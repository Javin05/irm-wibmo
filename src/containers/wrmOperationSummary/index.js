import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WrmOperationSummaryAWS from '../../components/dashboard/WrmOperationSummaryAWS'

function WRMOperationSummary(props) {
    return (
        <Switch>
            <Route path='/wrm-operation'>
                <WrmOperationSummaryAWS />
            </Route>
        </Switch>
    )
}

export default WRMOperationSummary