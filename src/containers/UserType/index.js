import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AddUserType from '../../components/UserType/userType/add/AddUserType'
import UsersTypeComponent from '../../components/UserType/UsersType'
import { PageTitle } from '../../theme/layout/core'

function UsersType () {
  return (
    <Switch>
      <Route path='/user-type'>
        <PageTitle breadcrumbs={[]}>Users Type</PageTitle>
        <UsersTypeComponent />
      </Route>
      <Route path='/update-user-type/:id'>
        <PageTitle breadcrumbs={[]}>Update Users Type</PageTitle>
        <AddUserType />
      </Route>
      <Route path='/add-user-type'>
        <PageTitle breadcrumbs={[]}>Add Users Type</PageTitle>
        <AddUserType />
      </Route>
    </Switch>
  )
}

export default UsersType
