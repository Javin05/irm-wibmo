import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom'
import WatchList from '../../components/watch-list'

function WatchlistContainer (props) {

    return (
        <Switch>
            <Route path='/watch-list'>
                <WatchList />
            </Route>
        </Switch>
    )
}

export default WatchlistContainer