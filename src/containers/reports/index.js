import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Reports from '../../components/reports/Reports'
import { PageTitle } from '../../theme/layout/core'

const merchant = [
  {
    title: 'Home',
    // path: '/merchant',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function Report () {
  return (
    <Switch>
      <Route path='/reports'>
        <PageTitle breadcrumbs={[]}>Reports</PageTitle>
        <Reports />
      </Route>
    </Switch>
  )
}

export default Report
