import React from 'react'
import { Switch, Route } from 'react-router-dom'
import OGMAnalysisSummaryAWS from '../../components/dashboard/OGMAnalysisSummaryAWS'

function OGMAnalysisSummary(props) {
    return (
        <Switch>
            <Route path='/ogmaudit-dashboard'>
                <OGMAnalysisSummaryAWS />
            </Route>
        </Switch>
    )
}

export default OGMAnalysisSummary