import { Switch, Route } from 'react-router-dom'
import WebAnalysis from '../../components/web-analysis'

function WebAnalysisContainer (props) {

    return (
        <Switch>
            <Route path='/web-analysis'>
                <WebAnalysis />
            </Route>
        </Switch>
    )
}

export default WebAnalysisContainer