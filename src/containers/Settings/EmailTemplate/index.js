import React from 'react'
import { Switch, Route } from 'react-router-dom'
import TemplateList from '../../../components/Settings/EmailTemplate/TemplateList'
import AddTemplate from '../../../components/Settings/EmailTemplate/EmailTemplateForm'
import { PageTitle } from '../../../theme/layout/core'

function TemplateContainer () {
  return (
    <Switch>
      <Route path='/email-templates/update/:id'>
        <PageTitle breadcrumbs={[]}>Update Template</PageTitle>
        <AddTemplate />
      </Route>
      <Route path='/email-templates/add-template'>
        <PageTitle breadcrumbs={[]}>Add Template</PageTitle>
        <AddTemplate />
      </Route>
      <Route path='/email-templates'>
        <PageTitle breadcrumbs={[]}>Email Templates</PageTitle>
        <TemplateList />
      </Route>
    </Switch>
  )
}

export default TemplateContainer
