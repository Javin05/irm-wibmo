import React from 'react'
import { Switch, Route } from 'react-router-dom'
import DashboardPowerBI from '../../components/dashboard/dashboardPowerBI'
// import { PageTitle } from '../../theme/layout/core'
// import { UnderConstruction } from '../error'

function Dashboard (props) {
  return (
    <Switch>
      <Route path='/dashboard'>
        {/* <PageTitle breadcrumbs={[]}>Under Construction</PageTitle>
        <UnderConstruction /> */}
        <DashboardPowerBI />
      </Route>
    </Switch>
  )
}

export default Dashboard
