import React from 'react'
import { Switch, Route } from 'react-router-dom'
import KYCList from '../../components/uaeKyc/KYCPage'
import { PageTitle } from '../../theme/layout/core'


function Homes () {
  return (
    <>
    <Switch>
      <Route path='/uae-kyc'>
        <PageTitle>Uae Kyc</PageTitle>
        <KYCList />
      </Route>
    </Switch>
    </>
  )
}

export default Homes
