import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AdminForm from '../../components/admin/AdminForm'
import { PageTitle } from '../../theme/layout/core'


const merchant = [
  {
    title: 'Merchant',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function QueuesSelect() {

  return (
    <Switch>
      <Route path='/update/:id'>
        <PageTitle breadcrumbs={merchant}> Edit Admin </PageTitle>
        <AdminForm />
      </Route>
    </Switch>
  )
}

export default QueuesSelect
