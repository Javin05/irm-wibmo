import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Admin from '../../components/admin/Admin'
import { PageTitle } from '../../theme/layout/core'

const merchant = [
  {
    title: 'Manage Input Fields',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function RulesSelect() {
  return (
    <Switch>
      <Route path='/admin'>
        <PageTitle breadcrumbs={{}}>High Risk List</PageTitle>
        <Admin />
      </Route>
    </Switch>
  )
}

export default RulesSelect
