import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import DemoListCharts from '../../components/demo-dashboard'

function DemoDashboard (props) {

    return (
        <Switch>
            <Route path='/new-dashboard'>
                <DemoListCharts />
            </Route>
        </Switch>
    )
}

export default DemoDashboard