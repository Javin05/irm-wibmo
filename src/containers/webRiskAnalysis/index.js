import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WebRiskAnalysis from '../../components/webRiskAnalysis/WebRiskAnalysis'

function Dashboard (props) {
  return (
    <Switch>
      <Route path='/web-risk'>
        <WebRiskAnalysis />
      </Route>
    </Switch>
  )
}

export default Dashboard
