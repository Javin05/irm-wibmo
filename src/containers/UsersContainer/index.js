import React from 'react'
import { Switch, Route } from 'react-router-dom'
import UsersComponent from '../../components/Users/UsersComponent'
import AddUsers from '../../components/Users/userRole/add/AddUser'
import { PageTitle } from '../../theme/layout/core'

function UsersContainer () {
  return (
    <Switch>
      {/* <Route path='/add-user-role'>
        <PageTitle breadcrumbs={[]}>Add Users Role</PageTitle>
        <AddUsers />
      </Route>
      <Route path='/update-user-role/:id'>
        <PageTitle breadcrumbs={[]}>Update Users Role</PageTitle>
        <AddUsers />
      </Route> */}
      <Route path='/user-role'>
        <PageTitle breadcrumbs={[]}>Users Role</PageTitle>
        <UsersComponent />
      </Route>
    </Switch>
  )
}

export default UsersContainer
