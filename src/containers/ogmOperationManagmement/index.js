import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import OgmOperationManagement from '../../components/ogmOperationManagmement'
import OgmOperationDetails from '../../components/ogmOperationDetails/index'

function OgmOperationManage() {
  return (
    <Switch>
       <Route path='/ogm-management'>
        <PageTitle breadcrumbs={{}}>OGM Queue</PageTitle>
        <OgmOperationManagement />
      </Route>
        <Route path="/ogmmanagement/update/:id">
        <PageTitle breadcrumbs={[]}>OGM Operation Managment</PageTitle>
        <OgmOperationDetails/>
      </Route>
    </Switch>
  )
}

export default OgmOperationManage