import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import RiskManagementList from '../../components/riskManagement/analyzeRiskManangement'
import AccountRiskSummary from '../../components/accountRiskSummary/riskSummary'

const issuerBankBreadCrumbs = [
  {
    title: 'Risk Management',
    path: '/accounts-riskmanagement',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]
function IssuerBank () {
  return (
    <Switch>
      {/* <Route path='/risk-management-search'>
        <PageTitle breadcrumbs={{}}>Cases</PageTitle>
        <RiskManagementList />
      </Route> */}
      <Route path='/accounts-riskmanagement'>
        <PageTitle breadcrumbs={{}}>Account Risk Management</PageTitle>
        <RiskManagementList />
      </Route>
      <Route path='/account-risk-summary/update/:id'>
        <PageTitle breadcrumbs={{}}>Account Risk Management</PageTitle>
        <AccountRiskSummary />
      </Route>
    </Switch>
  )
}

export default IssuerBank
