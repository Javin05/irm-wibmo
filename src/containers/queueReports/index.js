import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AmLQueueList from '../../components/amlQueue/AMLlist'
import Home from '../../components/queueReports/Home'
import { PageTitle } from '../../theme/layout/core'


function Dashboard(props) {
  return (
    <Switch>
      <Route path='/queue-reports'>
        <PageTitle breadcrumbs={{}}></PageTitle>
        <Home />
      </Route>
    </Switch>
  )
}

export default Dashboard
