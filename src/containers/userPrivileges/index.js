import React from 'react'
import { Switch, Route } from 'react-router-dom'
import UserPrivilege from '../../components/userPrivileges/UserPrivileges'
import { PageTitle } from '../../theme/layout/core'

const UsersPrivileges = () => {
  return (
    <Switch>
      <Route path='/user-privileges'>
        <PageTitle breadcrumbs={[]}>Users Privileges</PageTitle>
        <UserPrivilege />
      </Route>
      {/* <Route path='/update-user-privileges/:id'>
        <PageTitle breadcrumbs={[]}>Update Users Privilege</PageTitle>
        <UserPrivilegesForm />
      </Route> */}
      {/* <Route path='/add-user-privileges'>
        <PageTitle breadcrumbs={[]}>Add Users Privilege</PageTitle>
        <UserPrivilegesForm />
      </Route> */}
    </Switch>
  )
}

export default UsersPrivileges
