import { Switch, Route } from 'react-router-dom'
import TransactionLaundring from '../../components/transactionLaundring/index'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/transaction-lundering'>
        <PageTitle breadcrumbs={{}}>Transaction Laundring </PageTitle>
        <TransactionLaundring />
      </Route>
    </Switch>
  )
}

export default Home
