import React from 'react'
import { Switch, Route } from 'react-router-dom'
import APIUsageBillingSummaryAWS from '../../components/dashboard/APIUsageBillingSummaryAWS'

function APIUsageBillingSummary(props) {
    return (
        <Switch>
            <Route path='/apiusage-billing'>
                <APIUsageBillingSummaryAWS />
            </Route>
        </Switch>
    )
}

export default APIUsageBillingSummary