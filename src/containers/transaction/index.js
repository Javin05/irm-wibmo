import React from 'react'
import { Switch, Route } from 'react-router-dom'
import TransactionList from '../../components/transaction/Transaction'
import { PageTitle } from '../../theme/layout/core'

function Dashboard (props) {
  return (
    <Switch>
      <Route path='/transactions'>
         <PageTitle breadcrumbs={[]}>Transactions</PageTitle>
        <TransactionList />
      </Route>
      <Route path='/transaction-search'>
         <PageTitle breadcrumbs={[]}>Transactions</PageTitle>
        <TransactionList />
      </Route>
    </Switch>
  )
}

export default Dashboard
