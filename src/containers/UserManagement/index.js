import React from 'react'
import { Switch, Route } from 'react-router-dom'
import UserList from '../../components/UserManagement/UserList'
import ViewUser from '../../components/UserManagement/viewUser'
import AssignPartner from '../../components/UserManagement/assignPartner'
import AssignClient from '../../components/UserManagement/assignClient'
import { PageTitle } from '../../theme/layout/core'

function UsersContainer () {
  return (
    <Switch>
      <Route path='/assign-partner'>
        <PageTitle breadcrumbs={[]}>Assign Partner</PageTitle>
        <AssignPartner />
      </Route>
      <Route path='/assign-client'>
        <PageTitle breadcrumbs={[]}>Assign Client</PageTitle>
        <AssignClient />
      </Route>
      <Route path='/user-management/update-user'>
        <PageTitle breadcrumbs={[]}>Users</PageTitle>
        <ViewUser />
      </Route>
      <Route path='/user-management'>
        <PageTitle breadcrumbs={[]}>User Management</PageTitle>
        <UserList />
      </Route>
    </Switch>
  )
}

export default UsersContainer
