import React from 'react'
import { Switch, Route } from 'react-router-dom'
import MonitorList from '../../components/monitoring/Monitor'
import Websites from '../../components/monitoring/dashboard/website/index'
import routeConfig from "../../routing/routeConfig"

function Monitor (props) {
  return (
    <Switch>
      <Route path={routeConfig.monitar}>
        <MonitorList />
      </Route>
      {/* <Route path={routeConfig.monitarSearch}>
        <MonitorList />
      </Route> */}
      <Route path={routeConfig.monitarDashboard}>
        <Websites />
      </Route>
    </Switch>
  )
}

export default Monitor
