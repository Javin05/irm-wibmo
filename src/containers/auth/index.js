import React, { useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import { Registration } from '../../components/auth/Registration'
import ForgotPassword from '../../components/auth/ForgotPassword'
import Login from '../../components/auth/Login'
import { toAbsoluteUrl } from '../../theme/helpers'
import routeConfig from '../../routing/routeConfig'
import { colors } from '../../utils/constants'
import ResetPassword from '../../components/auth/ResetPassword'

export function AuthPage(props) {
  const query = useLocation().search
  const pathName = useLocation().pathname

  useEffect(() => {
    document.body.classList.add('bg-white')
    return () => {
      document.body.classList.remove('bg-white')
    }
  }, [])

  const GetComponent = () => {
    const pathName = useLocation().pathname
    if (pathName === routeConfig.registration) {
      return <Registration />
    }
    if (pathName === routeConfig.forgotPassword) {
      return <ForgotPassword query={query} />
    }
    if (pathName === routeConfig.resetPassword) {
      return <ResetPassword />
    }
    return <Login />
  }

  console.log('forgotPassword', pathName)

  return (
    <>
     
          <div
            className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed'
            style={{
              backgroundImage: `url(${toAbsoluteUrl(
                '/media/illustrations/sketchy-1/14.png'
              )})`,
              backgroundColor: colors.oxfordBlue
            }}
          >
            {/* begin::Content */}
            <div className='d-flex flex-center flex-column flex-column-fluid p-10'>
              {/* begin::Logo */}
              <a href='#' className='mb-12'>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
                  className='h-65px'
                />
              </a>
              <div className='w-lg-500px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto'>
                {GetComponent()}
              </div>
            </div>
            {/* end::Logo */}
            {/* begin::Wrapper */}
            {/* end::Wrapper */}
            {/* end::Content */}
          </div>
    </>
  )
}
