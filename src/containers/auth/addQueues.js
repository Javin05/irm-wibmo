import React, { useState, useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import { Registration } from '../../components/auth/Registration'
import ForgotPassword from '../../components/auth/ForgotPassword'
import Login from '../../components/auth/Login'
import { toAbsoluteUrl } from '../../theme/helpers'
import routeConfig from '../../routing/routeConfig'
import { colors } from '../../utils/constants'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { LoginActions, ownerActions, transactionActions, MonthlyActions, MccActions, MerchantLoginAction, queuesAction, queuesGetIdActions } from '../../store/actions'
import { USER_ERROR, REGEX, RESPONSE_STATUS, SESSION, STATUS_RESPONSE } from '../../utils/constants'
import _ from 'lodash'
import { setLocalStorage } from '../../utils/helper'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import color from "../../utils/colors"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import { successAlert, warningAlert } from "../../utils/alerts"

function AddQueue(props) {
  const {
    loginDispatch,
    loading,
    loginData,
    clearLogin,
    getOwnerDispatch,
    ownerlistsData,
    getTransactionDispatch,
    transactionListData,
    getMonthlyDispatch,
    monthlyListData,
    getMccDispatch,
    mccListData,
    queuesAddDispatch,
    queueStatus,
    clearAddQueue,
    queueMessage,
    queuesAddData
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]

  const [formData, setFormData] = useState({
    queueName: '',
    serviceUrl: '',
    slaHigh: '',
    slaMedium: '',
    slaLow: '',
  })
  const [errors, setErrors] = useState({
    queueName: '',
    serviceUrl: '',
    slaHigh: '',
    slaMedium: '',
    slaLow: '',
  })
  const [showBanner, setShowBanner] = useState(false)
  const [show, setShow] = useState(false)
  const countryCodes = require('country-codes-list')
  const myCountryCodesObject = countryCodes.customList('countryCode', '[{countryCode}] {countryNameEn}: +{countryCallingCode}')

  useEffect(() => {
    if (currentId) {
      getOwnerDispatch()

    }
    // getTransactionDispatch()
    // getMonthlyDispatch()
    // getMccDispatch()
  }, [currentId])

  const handleSubmit = (e) => {
    const errors = {}
    if (_.isEmpty(formData.queueName)) {
      errors.queueName = USER_ERROR.QUEUE_NAME
    }
    if (_.isEmpty(formData.slaHigh)) {
      errors.slaHigh = USER_ERROR.SLA_HIGH
    }
    if (_.isEmpty(formData.slaMedium)) {
      errors.slaMedium = USER_ERROR.SLA_MEDIUM
    }
    if (_.isEmpty(formData.slaLow)) {
      errors.slaLow = USER_ERROR.SLA_LOW
    }
    if (_.isEmpty(formData.serviceUrl)) {
      errors.serviceUrl = USER_ERROR.WEBSITE
    } else if (formData.serviceUrl && !REGEX.WEBSITE_URL.test(formData.serviceUrl)) {
      errors.serviceUrl = 'Website is InValid'
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      queuesAddDispatch(formData)
    }
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    formData[name] = value
    setFormData(formData)
    setErrors({ ...errors, [name]: '' })
  }

  const showSubmit = () => {
    setShow(false)
    clearLogin()
    setFormData({
      email: '',
      phone: '',
      address: '',
      ipAddress: '',
      deviceID: ''
    })
  }

  useEffect(() => {
    if (queueStatus === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        queueMessage,
        )      
      setTimeout(() => {
        clearAddQueue()
        window.location = '/manage-queues'
      }, 3500)
    } else if (queueStatus === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        queueMessage,
        '',
        'Try again',
        '',
        () => { }
      )
      clearAddQueue()
    }
  }, [queueStatus])
  

  return (
    <>

      <div
        className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed overflow-hidden'
        style={{
          backgroundImage: `url(${toAbsoluteUrl(
            '/media/illustrations/sketchy-1/14.png'
          )})`,
          backgroundColor: colors.oxfordBlue
        }}
      >
        {/* begin::Wrapper */}
        <div className='row mb-10'>
          {
            !show
              ? (
                <>
                  {/* begin::Content */}
                  <div className='d-flex flex-center flex-column flex-column-fluid p-10'>
                    {/* begin::Logo */}
                    <a href='#' className='mb-12'>
                      <img
                        alt='Logo'
                        // src={toAbsoluteUrl('/media/loginImage/MicrosoftTeams-image.png')}
                        src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
                        className='h-65px'
                      />
                    </a>
                    <div className='w-lg-500px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto'>
                      {/* <MerchantLogin1 /> */}
                      {/* begin::Banner */}
                      {showBanner &&
                        <div className='mb-10 bg-light-info p-8 rounded'>
                          <div className='text-center text-danger'>
                            {loginData.message}
                          </div>
                        </div>}
                      {/* end::Banner */}
                      <div className='text-center mb-10'>
                        <h1 className='text-dark mb-4'>Add Queue</h1>
                      </div>
                      <>
                        <div className='fv-row mb-10'>
                          <label className='form-label fs-6 fw-bolder text-dark'>Queue Name</label>
                          <input
                            placeholder='Queue Name'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.queueName && errors.queueName },
                              {
                                'is-valid': formData.queueName && !errors.queueName
                              }
                            )}
                            onChange={(e) => handleChange(e)}
                            onKeyPress={(e) => {
                              if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                            type='text'
                            name='queueName'
                            autoComplete='off'
                          />
                          {errors.queueName && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.queueName}</span>
                            </div>
                          )}
                        </div>
                        <div className='fv-row mb-10'>
                          <label className='form-label fs-6 fw-bolder text-dark'>Service Url</label>
                          <input
                            placeholder='Service Url'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.serviceUrl && errors.serviceUrl },
                              {
                                'is-valid': formData.serviceUrl && !errors.serviceUrl
                              }
                            )}
                            onChange={(e) => handleChange(e)}
                            type='text'
                            name='serviceUrl'
                            autoComplete='off'
                          />
                          {errors.serviceUrl && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.serviceUrl}</span>
                            </div>
                          )}
                        </div>
                        <div className='fv-row mb-10'>
                          <label className='form-label fs-6 fw-bolder text-dark'>SlaHigh</label>
                          <input
                            placeholder='SlaHigh'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.slaHigh && errors.slaHigh },
                              {
                                'is-valid': formData.slaHigh && !errors.slaHigh
                              }
                            )}
                            onChange={(e) => handleChange(e)}
                            type='text'
                            name='slaHigh'
                            autoComplete='off'
                            maxLength={6}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors.slaHigh && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.slaHigh}</span>
                            </div>
                          )}
                        </div>
                        <div className='fv-row mb-10'>
                          <label className='form-label fs-6 fw-bolder text-dark'>SlaMedium</label>
                          <input
                            placeholder='SlaMedium'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.slaMedium && errors.slaMedium },
                              {
                                'is-valid': formData.slaMedium && !errors.slaMedium
                              }
                            )}
                            onChange={(e) => handleChange(e)}
                            type='text'
                            name='slaMedium'
                            autoComplete='off'
                            maxLength={6}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors.slaMedium && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.slaMedium}</span>
                            </div>
                          )}
                        </div>
                        <div className='fv-row mb-10'>
                          <label className='form-label fs-6 fw-bolder text-dark'>SlaLow</label>
                          <input
                            placeholder='SlaLow'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.slaLow && errors.slaLow },
                              {
                                'is-valid': formData.slaLow && !errors.slaLow
                              }
                            )}
                            onChange={(e) => handleChange(e)}
                            type='text'
                            name='slaLow'
                            autoComplete='off'
                            maxLength={6}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors.slaLow && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.slaLow}</span>
                            </div>
                          )}
                        </div>
                        {/* end::Action */}
                        <div className='d-flex justify-content-end '>
                          <Link
                            to='/manage-queues'
                            disabled={loading}
                            className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                          >
                            Back
                          </Link>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                            onClick={(e) => handleSubmit(e)}
                            disabled={loading}
                          >
                            {!loading && <span className='indicator-label'>Submit</span>}
                            {loading && (
                              <span className='indicator-progress' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                      </>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className='row mt-50 m-top-7'>
                    <div className='col-lg-5' />
                    <div className='col-lg-6 ' style={{ marginTop: '14%' }}>
                      <div className='card w-450px '
                      >
                        <div className='text-center text-success fw-bolder fs-3 mb-4 mt-4'>
                          Thanks For Submitting The Request. We Are Processing your Request.
                        </div>
                        <div className='text-center mb-4'>
                          <div className='row'>
                            <div className='col-sm-4 col-md-4 col-lg-4' />
                            <div className='col-sm-3 col-md-3 col-lg-3'>
                              <button
                                type='button'
                                id='kt_sign_in_submit'
                                className='btn btn-sm btn-info w-100'
                                onClick={() => showSubmit()}
                                disabled={loading}
                              >
                                {!loading && <span className='indicator-label'>Back</span>}
                                {loading && (
                                  <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </>
              )
          }
        </div>
        {/* end::Wrapper */}
        {/* end::Content */}
      </div>

    </>
  )
}

const mapStateToProps = state => {
  const { queuesAddStore, merchantLoginStore, ownerlistStore, transactionlistStore, MonthlylistStore, McclistStore } = state
  return {
    queueStatus: queuesAddStore && queuesAddStore.queueStatus ? queuesAddStore.queueStatus : '',
    queueMessage: queuesAddStore && queuesAddStore.queueMessage ? queuesAddStore.queueMessage : '',
    queuesAddData: queuesAddStore && queuesAddStore.queuesAdd ? merchantLoginStore.queuesAdd : {},
    ownerlistsData: ownerlistStore && ownerlistStore.ownerlists ? ownerlistStore.ownerlists : '',
    transactionListData: transactionlistStore && transactionlistStore.transactionList ? transactionlistStore.transactionList : '',
    monthlyListData: MonthlylistStore && MonthlylistStore.MonthlyList ? MonthlylistStore.MonthlyList : '',
    mccListData: McclistStore && McclistStore.MccList ? McclistStore.MccList : '',
    loading: merchantLoginStore && merchantLoginStore.loading ? merchantLoginStore.loading : false
  }
}

const mapDispatchToProps = dispatch => ({
  queuesAddDispatch: (data) => dispatch(queuesAction.queuesAdd(data)),
  getOwnerDispatch: () => dispatch(ownerActions.getOwnerlist()),
  getTransactionDispatch: () => dispatch(transactionActions.gettransactionlist()),
  getMonthlyDispatch: () => dispatch(MonthlyActions.getMonthlylist()),
  getMccDispatch: () => dispatch(MccActions.getMcclist()),
  clearAddQueue: () => dispatch(queuesAction.clearqueues())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddQueue)
