import React, { useState, useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import { Registration } from '../../components/auth/Registration'
import ForgotPassword from '../../components/auth/ForgotPassword'
import Login from '../../components/auth/Login'
import { toAbsoluteUrl } from '../../theme/helpers'
import routeConfig from '../../routing/routeConfig'
import { colors } from '../../utils/constants'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { MerchantLoginAction, ownerActions, transactionActions, MonthlyActions, MccActions, clientIdLIstActions } from '../../store/actions'
import { USER_ERROR, REGEX, STATUS_RESPONSE, SECRETKYEY } from '../../utils/constants'
import _, { values } from 'lodash'
import { setLocalStorage } from '../../utils/helper'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import color from "../../utils/colors"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import { warningAlert, confirmationAlert } from "../../utils/alerts"
import { getLocalStorage, removeLocalStorage } from '../../utils/helper';
import DeviceFingerprint from "node_js_ipqs_device_tracker";

function MerchantDetails(props) {
  const {
    loginDispatch,
    loading,
    merchantLoginData,
    clearMerchantLogin,
    getOwnerDispatch,
    ownerlistsData,
    getTransactionDispatch,
    transactionListData,
    getMonthlyDispatch,
    monthlyListData,
    getMccDispatch,
    mccListData,
    clinetIdLists,
    clientIdDispatch
  } = props

  const query = useLocation().search
  const pathName = useLocation().pathname
  useEffect(() => {
    document.body.classList.add('bg-white')
    return () => {
      document.body.classList.remove('bg-white')
    }
  }, [])

  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [individualPhone, setIndividualPhone] = useState('')
  const [selectedbusinessOwnershipTypeOption, setSelectedbusinessOwnershipTypeOption] = useState('')
  const [BusinessOption, setBusiness] = useState()
  const [selectedTransactionIdOption, setSelectedTransactionIdOption] = useState('')
  const [TransactionOption, setTransactionOption] = useState()
  const [selectedMonthlyIdOption, setSelectedMonthlyIdOption] = useState('')
  const [MonthlyOption, setMonthlyOption] = useState()
  const [selectedMcIdOption, setSelectedMcIdOption] = useState('')
  const [MccOption, setMccOption] = useState()
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [ipqvalue, setIpqvalueValue] = useState()

  var format = /[!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]+/;

  const [clienIdData, setClienIdData] = useState({
    clientId: Role === 'Client User' ? ClinetId : ''
  })
  const [formData, setFormData] = useState({
    personalEmail: '',
    phone: '',
    address: '',
    ipAddress: '',
    deviceID: '',
    firstName: '',
    lastName: '',
    businessName: '',
    businessEmail: '',
    businessPhone: '',
    businessAddress: '',
    businessOwnershipType: '',
    gmvId: '',
    volId: '',
    mccId: '',
    // gstIn: '',
    // cin: '',
    // pan: '',
    panOwnerName: '',
    website: '',
    monthlyVolume: '',
    mccCode: '',
    clientId: ''
  })

  const [errors, setErrors] = useState({
    personalEmail: '',
    phone: '',
    address: '',
    ipAddress: '',
    deviceID: '',
    firstName: '',
    lastName: '',
    businessName: '',
    businessEmail: '',
    businessPhone: '',
    businessAddress: '',
    website: '',
    clientId: ''
  })
  const [showBanner, setShowBanner] = useState(false)
  const [show, setShow] = useState(false)
  const countryCodes = require('country-codes-list')
  const myCountryCodesObject = countryCodes.customList('countryCode', '[{countryCode}] {countryNameEn}: +{countryCallingCode}')

  useEffect(() => {
    getOwnerDispatch()
    getTransactionDispatch()
    getMonthlyDispatch()
    getMccDispatch()
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
  }, [])

  const match = [
    'http',
    'https'
  ]

  const handleSubmit = (e) => {
    const errors = {}
    if (_.isEmpty(formData.website)) {
      errors.website = USER_ERROR.WEBSITE
    } else if (formData.website && !REGEX.WEBSITE_URL.test(formData.website)) {
      errors.website = 'Website is InValid'
    }
    if (Role === "Admin" && _.isEmpty(formData.clientId)) {
      errors.clientId = USER_ERROR.CLIENT_ID
    }
    if (formData.phone && !REGEX.PHONEPLUS.test(formData.phone)) {
      errors.phone = USER_ERROR.PHONE_NUMBER_INVALID
    }
    if (formData.businessPhone && !REGEX.PHONEPLUS.test(formData.businessPhone)) {
      errors.businessPhone = USER_ERROR.PHONE_NUMBER_INVALID
    }
    // if (formData.businessEmail && !REGEX.EMAIL.test(formData.businessEmail)) {
    //   errors.businessEmail = USER_ERROR.EMAIL_INVALID
    // }
    // if (formData.personalEmail && !REGEX.EMAIL.test(formData.personalEmail)) {
    //   errors.personalEmail = USER_ERROR.EMAIL_INVALID
    // }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      if (!_.isEmpty(clienIdData && clienIdData.clientId)) {
        const demo = {
          ...formData,
          ...clienIdData
        }
        loginDispatch(demo)
      } else {
        const data = {
          ...formData,
          deviceFingerprint: ipqvalue
        }
        loginDispatch(data)
      }
    }


    // const errors = {}
    // if (_.isEmpty(formData.personalEmail)) {
    //   errors.personalEmail = USER_ERROR.P_EMAIL
    // } else if (formData.personalEmail && !REGEX.EMAIL.test(formData.personalEmail)) {
    //   errors.personalEmail = USER_ERROR.EMAIL_INVALID
    // }
    // if (_.isEmpty(formData.businessEmail)) {
    //   errors.businessEmail = USER_ERROR.B_EMAIL
    // } else if (formData.businessEmail && !REGEX.EMAIL.test(formData.businessEmail)) {
    //   errors.businessEmail = USER_ERROR.EMAIL_INVALID
    // }
    // if (_.isEmpty(formData.phone)) {
    //   errors.phone = USER_ERROR.PHONE
    // }
    // if (_.isEmpty(formData.address)) {
    //   errors.address = USER_ERROR.ADDRESS
    // }
    // if (_.isEmpty(formData.ipAddress)) {
    //   errors.ipAddress = USER_ERROR.IP_ADDRESS
    // }
    // if (_.isEmpty(formData.deviceID)) {
    //   errors.deviceID = USER_ERROR.DEVICE_ID
    // }
    // if (_.isEmpty(formData.firstName)) {
    //   errors.firstName = USER_ERROR.F_NAME
    // }
    // if (_.isEmpty(formData.lastName)) {
    //   errors.lastName = USER_ERROR.L_NAME
    // }
    // if (_.isEmpty(formData.businessName)) {
    //   errors.businessName = USER_ERROR.COMPANY_NAME
    // }
    // if (_.isEmpty(formData.businessPhone)) {
    //   errors.businessPhone = USER_ERROR.BUSINESS_PHONE
    // }
    // if (_.isEmpty(formData.businessAddress)) {
    //   errors.businessAddress = USER_ERROR.BUSINESS_ADDRESS
    // }
    // if (_.isEmpty(formData.website)) {
    //   errors.website = USER_ERROR.WEBSITE
    // } else if (formData.website && !REGEX.WEBSITE_URL.test(formData.website)) {
    //   errors.website = 'Website is InValid'
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
    //   loginDispatch(formData)
    //   e.target.reset()
    // }
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    formData[name] = value
    setFormData(formData)
    setErrors({ ...errors, [name]: '' })
  }

  const onConfirm = () => {
    clearMerchantLogin()
    props.history.push('/accounts-riskmanagement')
    setFormData({
      personalEmail: '',
      phone: '',
      address: '',
      ipAddress: '',
      deviceID: '',
      firstName: '',
      lastName: '',
      businessName: '',
      businessEmail: '',
      businessPhone: '',
      businessAddress: '',
      businessOwnershipType: '',
      gmvId: '',
      volId: '',
      mccId: '',
      // gstIn: '',
      // cin: '',
      // pan: '',
      panOwnerName: '',
      website: '',
      monthlyVolume: ''
    })
  }

  const clear = () => {
    clearMerchantLogin()
    setFormData({
      personalEmail: '',
      phone: '',
      address: '',
      ipAddress: '',
      deviceID: '',
      firstName: '',
      lastName: '',
      businessName: '',
      businessEmail: '',
      businessPhone: '',
      businessAddress: '',
      businessOwnershipType: '',
      gmvId: '',
      volId: '',
      mccId: '',
      // gstIn: '',
      // cin: '',
      // pan: '',
      panOwnerName: '',
      website: ''
    })
  }

  useEffect(() => {
    if (merchantLoginData && merchantLoginData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        merchantLoginData && merchantLoginData.message,
        'success',
        'Back to Admin',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      clearMerchantLogin()
    } else if (merchantLoginData && merchantLoginData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        merchantLoginData && merchantLoginData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearMerchantLogin()
    }
  }, [merchantLoginData])

  const handleOnChange = value => {
    const phonevalue = `+${value} `
    setFormData((values) => ({ ...values, phone: phonevalue.trim() }));
    setErrors({ ...errors, phone: '' })
  };
  const handleBusinessChange = value => {
    const phonevalue = `+${value} `
    setFormData((values) => ({ ...values, businessPhone: phonevalue.trim() }));
    setErrors({ ...errors, businessPhone: '' })
  };

  useEffect(() => {
    const Business = getDefaultOption(ownerlistsData)
    setBusiness(Business)
    if (!_.isEmpty(formData.businessOwnershipType)) {
      const selOption = _.filter(Business, function (x) { if (_.includes(formData.businessOwnershipType._id, x.value)) { return x } })
      setSelectedbusinessOwnershipTypeOption(selOption)
    }
  }, [ownerlistsData])

  const getDefaultOption = (ownerlistsData) => {
    const defaultOptions = []
    for (const item in ownerlistsData) {
      defaultOptions.push({ label: ownerlistsData[item].businessType, value: ownerlistsData[item]._id })
    }
    return defaultOptions
  }

  const handleChangeBusiness = selectedOption => {
    if (selectedOption !== null) {
      setSelectedbusinessOwnershipTypeOption(selectedOption)
      setFormData(values => ({ ...values, businessOwnershipType: selectedOption.value }))
    }
  }


  // useEffect(() => {
  //   const Transaction = getDefaultTransactionOption(transactionListData)
  //   setTransactionOption(Transaction)
  //   if (!_.isEmpty(formData.gmvId)) {
  //     const selOption = _.filter(Transaction, function (x) { if (_.includes(formData.gmvId._id, x.value)) { return x } })
  //     setSelectedTransactionIdOption(selOption)
  //   }
  // }, [transactionListData])

  // const getDefaultTransactionOption = (transactionListData) => {
  //   const defaultOptions = []
  //   for (const item in transactionListData) {
  //     defaultOptions.push({ label: transactionListData[item].avgMonthlyGmv, value: transactionListData[item]._id })
  //   }
  //   return defaultOptions
  // }

  // const handleChangeTransaction = selectedOption => {
  //   if (selectedOption !== null) {
  //     setSelectedTransactionIdOption(selectedOption)
  //     setFormData(values => ({ ...values, gmvId: selectedOption.value }))
  //   }
  // }

  useEffect(() => {
    const Transaction = getDefaultMonthlyOption(monthlyListData)
    setMonthlyOption(Transaction)
    if (!_.isEmpty(formData.volId)) {
      const selOption = _.filter(Transaction, function (x) { if (_.includes(formData.volId._id, x.value)) { return x } })
      setSelectedMonthlyIdOption(selOption)
    }
  }, [monthlyListData])

  const getDefaultMonthlyOption = (monthlyListData) => {
    const defaultOptions = []
    for (const item in monthlyListData) {
      defaultOptions.push({ label: monthlyListData[item].avgMonthlyVolume, value: monthlyListData[item]._id })
    }
    return defaultOptions
  }

  const handleChangeMonthly = selectedOption => {
    if (selectedOption !== null) {
      setSelectedMonthlyIdOption(selectedOption)
      setFormData(values => ({ ...values, volId: selectedOption.value }))
    }
  }

  useEffect(() => {
    const Transaction = getDefaultMccOption(mccListData)
    setMccOption(Transaction)
    if (!_.isEmpty(formData.mccId)) {
      const selOption = _.filter(Transaction, function (x) { if (_.includes(formData.mccId._id, x.value)) { return x } })
      setSelectedMcIdOption(selOption)
    }
  }, [mccListData])

  const getDefaultMccOption = (mccListData) => {
    const defaultOptions = []
    for (const item in mccListData) {
      defaultOptions.push({ label: mccListData[item].description, value: mccListData[item]._id })
    }
    return defaultOptions
  }

  const handleChangeMcc = selectedOption => {
    if (selectedOption !== null) {
      setSelectedMcIdOption(selectedOption)
      setFormData(values => ({ ...values, mccId: selectedOption.value }))
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOptions(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOptions = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({...errors,clientId: ''})
    }
  }


  useEffect(() => {
    DeviceFingerprint.initializeScriptAsync(SECRETKYEY.FINGERPRINT)
      .then(() => {
        const callback = (result) => {
          setIpqvalueValue(result)
          localStorage.setItem("deviceData", result)
          console.log("DeviceFingerprint success result : ", result)
        }
        DeviceFingerprint.AfterResult(callback)
        DeviceFingerprint.Init()
      })
      .catch((err) => {
        console.log("DeviceFingerprint errpr result : ", err)
        localStorage.setItem("deviceData", err)
        setIpqvalueValue(err)
      })
  }, [])

  return (
    <>

      <div
        className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed overflow-hidden'
        style={{
          backgroundImage: `url(${toAbsoluteUrl(
            '/media/illustrations/sketchy-1/14.png'
          )})`,
          backgroundColor: colors.oxfordBlue
        }}
      >
        {/* begin::Wrapper */}
        <div className='row mb-10'>
          {
            !show
              ? (
                <>
                  {/* begin::Content */}
                  <div className='d-flex flex-center flex-column flex-column-fluid p-10'>
                    {/* begin::Logo */}
                    <a href='#' className='mb-12'>
                      <img
                        alt='Logo'
                        // src={toAbsoluteUrl('/media/loginImage/MicrosoftTeams-image.png')}
                        src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
                        className='h-65px'
                      />
                    </a>
                  </div>
                  {/* end::Logo */}
                  <div className='card card-xl-stretch'>
                    <div className='row mt-10'>
                      <div className='col-lg-8'>
                        <h1 className='text-dark mb-3 d-flex justify-content-center'>Business Information</h1>
                      </div>
                      <div className='col-lg-4'>
                        <h1 className='text-dark mb-3 d-flex justify-content-center'>Personal Information</h1>
                      </div>
                    </div>
                    <div className='card-body pt-0'>
                      <div className='row'>
                        <div className='col-lg-4 d-flex justify-content-end'>
                          <div className='w-lg-450px bg-white rounded p-10 p-lg-15'>
                            <>
                              <>
                                {
                                  Role === 'Admin' ? (
                                    <div className='fv-row mb-10'>
                                      <label className="form-label fs-6 fw-bolder text-dark required">
                                        Client
                                      </label>
                                      <ReactSelect
                                        styles={customStyles}
                                        isMulti={false}
                                        name='AppUserId'
                                        className='select2'
                                        classNamePrefix='select'
                                        handleChangeReactSelect={handleChangeAsignees}
                                        options={AsigneesOption}
                                        value={SelectedAsigneesOption}
                                        isDisabled={!AsigneesOption}
                                      />
                                      {errors.clientId && (
                                        <div className='fv-plugins-message-container text-danger'>
                                          <span role='alert text-danger'>{errors.clientId}</span>
                                        </div>
                                      )}
                                    </div>
                                  ) : (
                                    null
                                  )
                                }
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Business Name</label>
                                  <input
                                    placeholder='Business Name'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.businessName && errors.businessName },
                                      {
                                        'is-valid': formData.businessName && !errors.businessName
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    onKeyPress={(e) => {
                                      if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    value={formData.businessName || ''}
                                    type='text'
                                    name='businessName'
                                    autoComplete='off'
                                  />
                                  {errors.businessName && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.businessName}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Business Phone</label>
                                  <PhoneInput
                                    // inputProps={{
                                    //   required: true,
                                    //   autoFocus: true
                                    // }}
                                    country={'in'}
                                    className='react-tel-input'
                                    value={formData.businessPhone || ''}
                                    onChange={(e) => handleBusinessChange(e)}
                                  />
                                  {errors.businessPhone && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.businessPhone}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Business Email</label>
                                  <input
                                    placeholder='Business Email'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.businessEmail && errors.businessEmail },
                                      {
                                        'is-valid': formData.businessEmail && !errors.businessEmail
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='businessEmail'
                                    name='businessEmail'
                                    autoComplete='off'
                                    value={formData.businessEmail || ''}
                                  />
                                  {errors.businessEmail && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.businessEmail}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Business Address</label>
                                  <textarea
                                    placeholder='Address'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.businessAddress && errors.businessAddress },
                                      {
                                        'is-valid': formData.businessAddress && !errors.businessAddress
                                      }
                                    )}
                                    value={formData.businessAddress || ''}
                                    onChange={(e) => handleChange(e)}
                                    type='businessAddress'
                                    name='businessAddress'
                                    autoComplete='off'
                                  />
                                  {errors.businessAddress && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.businessAddress}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark required'>Website</label>
                                  <input
                                    placeholder='Website'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.website && errors.website },
                                      {
                                        'is-valid': formData.website && !errors.website
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='website'
                                    name='website'
                                    autoComplete='off'
                                    value={formData.website || ''}
                                  />
                                  {errors.website && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.website}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Business Ownership Type</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='AppUserId'
                                    className='select2'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeBusiness}
                                    options={BusinessOption}
                                    value={selectedbusinessOwnershipTypeOption}
                                    isDisabled={!BusinessOption} s
                                  />
                                  {errors.businessType && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.businessType}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Merchant Category Code</label>
                                  <input
                                    placeholder='Mcc Code'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.mccCode && errors.mccCode },
                                      {
                                        'is-valid': formData.mccCode && !errors.mccCode
                                      }
                                    )}
                                    value={formData.mccCode || ''}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='mccCode'
                                    autoComplete='off'
                                  />
                                  {errors.mcCode && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.mcCode}</span>
                                    </div>
                                  )}
                                </div>
                                {/* end::Action */}
                              </>
                            </>
                          </div>
                        </div>

                        <div className='col-lg-4 d-flex justify-content-start'>
                          <div className='w-lg-500px bg-white rounded p-10 p-lg-15'>

                            <>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>Average Transaction Amount</label>
                                <select
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.gmvId && errors.gmvId },
                                    {
                                      'is-valid': formData.gmvId && !errors.gmvId
                                    }
                                  )}
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e)}
                                  name="gmvId"
                                  value={formData.gmvId || ''}
                                >
                                  <option value='select'>select</option>
                                  <option value='1 to 50'>1 to 50</option>
                                  <option value='101 to 250'>101 to 250</option>
                                  <option value='251 to 500'>251 to 500</option>
                                  <option value='501 to 1000'>501 to 1000</option>
                                </select>
                                {/* <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='AppUserId'
                                    className='select2'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeTransaction}
                                    options={TransactionOption}
                                    value={selectedTransactionIdOption}
                                    isDisabled={!TransactionOption}s
                                  /> */}

                                {errors.transactionAmount && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.transactionAmount}</span>
                                  </div>
                                )}
                              </div>

                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>Average Monthly Volume</label>
                                <select
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.monthlyVolume && errors.monthlyVolume },
                                    {
                                      'is-valid': formData.monthlyVolume && !errors.monthlyVolume
                                    }
                                  )}
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e)}
                                  name="monthlyVolume"
                                  value={formData.monthlyVolume || ''}
                                >
                                  <option value=''>select</option>
                                  <option value='1-1000'>1-1000</option>
                                  <option value='1001-5000'>1001-5000</option>
                                  <option value='5001-10000'>5001-10000</option>
                                  <option value='10001-50000'>10001-50000</option>
                                  <option value='Greater than 100,000'>Greater than 100,000</option>

                                </select>
                                {/* <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='AppUserId'
                                    className='select2'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeMonthly}
                                    options={MonthlyOption}
                                    value={selectedMonthlyIdOption}
                                    isDisabled={!MonthlyOption} s
                                  /> */}
                                {errors.monthlyVolume && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.monthlyVolume}</span>
                                  </div>
                                )}
                              </div>
                              {/* <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>GSTIN</label>
                                <select
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.gstIn && errors.gstIn },
                                    {
                                      'is-valid': formData.gstIn && !errors.gstIn
                                    }
                                  )}
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e)}
                                  name="gstIn"
                                  value={formData.gstIn || ''}
                                >
                                  <option value=''>select</option>
                                  <option value='Yes'>Yes</option>
                                  <option value='No'>No</option>
                                </select>
                                {errors.gstIn && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.gstIn}</span>
                                  </div>
                                )}
                              </div>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>CIN</label>
                                <select
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.cin && errors.cin },
                                    {
                                      'is-valid': formData.cin && !errors.cin
                                    }
                                  )}
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e)}
                                  name="cin"
                                  value={formData.cin || ''}
                                >
                                  <option value=''>select</option>
                                  <option value='Yes'>Yes</option>
                                  <option value='No'>No</option>
                                </select>
                                {errors.cin && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.cin}</span>
                                  </div>
                                )}
                              </div>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>PAN</label>
                                <select
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.pan && errors.pan },
                                    {
                                      'is-valid': formData.pan && !errors.pan
                                    }
                                  )}
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e)}
                                  name="pan"
                                  value={formData.pan || ''}
                                >
                                  <option value=''>select</option>
                                  <option value='Yes'>Yes</option>
                                  <option value='No'>No</option>
                                </select>
                                {errors.pan && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.pan}</span>
                                  </div>
                                )}
                              </div> */}
                            </>
                          </div>
                        </div>
                        <div className='col-lg-4 d-flex justify-content-center'>
                          <div className='w-lg-450px bg-white rounded p-10 p-lg-15'>
                            <>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>First Name</label>
                                <input
                                  placeholder='First Name'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.firstName && errors.firstName },
                                    {
                                      'is-valid': formData.firstName && !errors.firstName
                                    }
                                  )}
                                  value={formData.firstName || ''}
                                  onChange={(e) => handleChange(e)}
                                  onKeyPress={(e) => {
                                    if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                      e.preventDefault()
                                    }
                                  }}
                                  type='text'
                                  name='firstName'
                                  autoComplete='off'
                                />
                                {errors.firstName && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.firstName}</span>
                                  </div>
                                )}
                              </div>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>Last Name</label>
                                <input
                                  placeholder='Last Name'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.lastName && errors.lastName },
                                    {
                                      'is-valid': formData.lastName && !errors.lastName
                                    }
                                  )}
                                  value={formData.lastName || ''}
                                  onChange={(e) => handleChange(e)}
                                  onKeyPress={(e) => {
                                    if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                      e.preventDefault()
                                    }
                                  }}
                                  type='text'
                                  name='lastName'
                                  autoComplete='off'
                                />
                                {errors.lastName && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.lastName}</span>
                                  </div>
                                )}
                              </div>
                              <div className='fv-row mb-10 react-tel-input'>
                                <label className='form-label fs-6 fw-bolder text-dark'>Phone</label>
                                <PhoneInput
                                  inputProps={{
                                    required: true,
                                    autoFocus: true
                                  }}
                                  country={'in'}
                                  className='react-tel-input'
                                  value={formData.phone || ''}
                                  onChange={(e) => handleOnChange(e)}
                                  searchPlaceholder='Phone Number'
                                />
                                {errors.phone && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.phone}</span>
                                  </div>
                                )}
                              </div>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>Personal Email</label>
                                <input
                                  placeholder='Personal Email'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.personalEmail && errors.personalEmail },
                                    {
                                      'is-valid': formData.personalEmail && !errors.personalEmail
                                    }
                                  )}
                                  onChange={(e) => handleChange(e)}
                                  type='personalEmail'
                                  name='personalEmail'
                                  autoComplete='off'
                                  value={formData.personalEmail || ''}
                                />
                                {errors.personalEmail && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.personalEmail}</span>
                                  </div>
                                )}
                              </div>
                              <div className='fv-row mb-10'>
                                <label className='form-label fs-6 fw-bolder text-dark'>Address</label>
                                <textarea
                                  placeholder='Address'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.address && errors.address },
                                    {
                                      'is-valid': formData.address && !errors.address
                                    }
                                  )}
                                  value={formData.address || ''}
                                  onChange={(e) => handleChange(e)}
                                  type='address'
                                  name='address'
                                  autoComplete='off'
                                />
                                {errors.address && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <span role='alert text-danger'>{errors.address}</span>
                                  </div>
                                )}
                              </div>
                              {/* <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Device ID</label>
                                  <input
                                    placeholder='Device ID'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.deviceID && errors.deviceID },
                                      {
                                        'is-valid': formData.deviceID && !errors.deviceID
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='deviceID'
                                    name='deviceID'
                                    autoComplete='off'
                                    onKeyPress={(e) => {
                                      if (!/^[0-9 .]+$/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                  />
                                  {errors.deviceID && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.deviceID}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>IP Address</label>
                                  <textarea
                                    placeholder='Ip Address'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.ipAddress && errors.ipAddress },
                                      {
                                        'is-valid': formData.ipAddress && !errors.ipAddress
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='ipAddress'
                                    name='ipAddress'
                                    autoComplete='off'
                                  />
                                  {errors.ipAddress && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.ipAddress}</span>
                                    </div>
                                  )}
                                </div> */}
                            </>
                          </div>
                        </div>
                        <div className='form-group row mb-4'>
                          <div className='col-lg-6' />
                          <div className='col-lg-6'>
                            <div className='col-lg-11'>
                              <button
                                type='button'
                                className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                                onClick={(e) => handleSubmit(e)}
                                disabled={loading}
                              >
                                {!loading && <span className='indicator-label'>Submit</span>}
                                {loading && (
                                  <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                              <Link
                                to='/accounts-riskmanagement'
                                disabled={loading}
                                className='btn btn-sm btn-light-danger m-2 fa-pull-right close'

                              >
                                Back To Accounts Queue
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className='row mt-50 m-top-7'>
                    <div className='col-lg-5' />
                    <div className='col-lg-6 ' style={{ marginTop: '14%' }}>
                      <div className='card w-450px '
                      >
                        <div className='text-center text-success fw-bolder fs-3 mb-4 mt-4'>
                          Thanks For Submitting The Request. We Are Processing your Request.
                        </div>
                        <div className='text-center mb-4'>
                          <div className='row'>
                            <div className='col-sm-4 col-md-4 col-lg-4' />
                            <div className='col-sm-3 col-md-3 col-lg-3'>
                              <button
                                type='button'
                                id='kt_sign_in_submit'
                                className='btn btn-sm btn-info w-100'
                                // onClick={() => showSubmit()}
                                disabled={loading}
                              >
                                {!loading && <span className='indicator-label'>Back</span>}
                                {loading && (
                                  <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </>
              )
          }
        </div>
        {/* end::Wrapper */}
        {/* end::Content */}
      </div>

    </>
  )
}

const mapStateToProps = state => {
  const { merchantLoginStore, ownerlistStore, transactionlistStore, MonthlylistStore, McclistStore, clinetListStore } = state
  return {
    merchantLoginData: merchantLoginStore && merchantLoginStore.merchantLogin ? merchantLoginStore.merchantLogin : {},
    ownerlistsData: ownerlistStore && ownerlistStore.ownerlists ? ownerlistStore.ownerlists : '',
    transactionListData: transactionlistStore && transactionlistStore.transactionList ? transactionlistStore.transactionList : '',
    monthlyListData: MonthlylistStore && MonthlylistStore.MonthlyList ? MonthlylistStore.MonthlyList : '',
    mccListData: McclistStore && McclistStore.MccList ? McclistStore.MccList : '',
    loading: merchantLoginStore && merchantLoginStore.loading ? merchantLoginStore.loading : false,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
  }
}

const mapDispatchToProps = dispatch => ({
  loginDispatch: (data) => dispatch(MerchantLoginAction.login(data)),
  getOwnerDispatch: () => dispatch(ownerActions.getOwnerlist()),
  getTransactionDispatch: () => dispatch(transactionActions.gettransactionlist()),
  getMonthlyDispatch: () => dispatch(MonthlyActions.getMonthlylist()),
  getMccDispatch: () => dispatch(MccActions.getMcclist()),
  clearMerchantLogin: () => dispatch(MerchantLoginAction.clearMerchantLogin()),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantDetails)
