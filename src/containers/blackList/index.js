import React from 'react'
import { Switch, Route } from 'react-router-dom'
import BlackList from '../../components/blackList/BlackList'

function Dashboard (props) {
  return (
    <Switch>
      <Route path='/blacklist'>
        <BlackList />
      </Route>
    </Switch>
  )
}

export default Dashboard
