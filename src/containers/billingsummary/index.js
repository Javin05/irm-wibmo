import React from 'react'
import { Switch, Route } from 'react-router-dom'
import BillingSummaryAWS from '../../components/dashboard/BillingSummaryAWS'

function BillingSummary(props) {
    return (
        <Switch>
            <Route path='/billing-dashboard'>
                <BillingSummaryAWS />
            </Route>
        </Switch>
    )
}

export default BillingSummary