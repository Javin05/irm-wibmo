import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AddComponents from '../../components/userComponents/add/AddComponents'
import UserComponents from '../../components/userComponents/UserComponents'
import { PageTitle } from '../../theme/layout/core'

const Components = () => {
  return (
    <Switch>
      <Route path='/user-components'>
        <PageTitle breadcrumbs={[]}>Components</PageTitle>
        <UserComponents />
      </Route>
      <Route path='/update-user-components/:id'>
        <PageTitle breadcrumbs={[]}>Update Components</PageTitle>
        <AddComponents />
      </Route>
      <Route path='/add-user-components'>
        <PageTitle breadcrumbs={[]}>Add Components</PageTitle>
        <AddComponents />
      </Route>
    </Switch>
  )
}

export default Components
