import React from "react";
import { Switch, Route } from "react-router-dom";
import UserProfileKYC from "../../components/KYC/UserProfileKYC";
import { PageTitle } from "../../theme/layout/core";

function Homes() {
  return (
    <>
      <Switch>
        <Route path="/kyc-dashboard/update/:id">
          <PageTitle>UserProfileKYC</PageTitle>
          <UserProfileKYC />
        </Route>
      </Switch>
    </>
  );
}

export default Homes;
