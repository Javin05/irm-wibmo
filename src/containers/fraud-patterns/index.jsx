import { Switch, Route } from 'react-router-dom'
import FraudPatterns from '../../components/fraud-patterns'

function FraudPatternsContainer (props) {

    return (
        <Switch>
            <Route path='/fraud-patterns'>
                <FraudPatterns />
            </Route>
        </Switch>
    )
}

export default FraudPatternsContainer