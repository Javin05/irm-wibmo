import React from 'react'
import { Switch, Route } from 'react-router-dom'
import SiteConfig from '../../components/siteConfig/siteConfig'
import { PageTitle } from '../../theme/layout/core'

const SiteConfigBreadCrumbs = [
  {
    title: 'Site Configuration',
    path: '/site-configuration',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]
function SiteConfiguration () {
  return (
    <Switch>
      <Route path='/site-configuration'>
        <PageTitle breadcrumbs={SiteConfigBreadCrumbs}>Site Configuration</PageTitle>
        <SiteConfig />
      </Route>
    </Switch>
  )
}

export default SiteConfiguration
