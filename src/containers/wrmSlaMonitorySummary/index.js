import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WRMSlaMonitorSummaryAWS from '../../components/dashboard/WRMSlaMonitorSummaryAWS'

function WRMSlaMonitorSummary(props) {
    return (
        <Switch>
            <Route path='/wrm-sla'>
                <WRMSlaMonitorSummaryAWS />
            </Route>
        </Switch>
    )
}

export default WRMSlaMonitorSummary