import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'

const merchant = [
  {
    title: 'Merchant',
    // path: '/merchant',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function FraudAnalyzer () {
  return (
    <Switch>
      <Route path='/merchant-demo'>
        <PageTitle breadcrumbs={[]}>Dashboard</PageTitle>
      </Route>
    </Switch>
  )
}

export default FraudAnalyzer
