import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Merchant from '../../components/merchant/merchant'
import { PageTitle } from '../../theme/layout/core'

const merchant = [
  {
    title: 'Merchant',
    // path: '/merchant',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function FraudAnalyzer () {
  return (
    <Switch>
      <Route path='/merchant/:id'>
        <PageTitle breadcrumbs={[]}>Dashboard</PageTitle>
        <Merchant />
      </Route>
    </Switch>
  )
}

export default FraudAnalyzer
