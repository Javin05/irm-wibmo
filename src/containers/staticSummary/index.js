import React from "react";
import { Switch, Route } from "react-router-dom";
import StaticSummary from "../../components/kycDashboard/StaticComponent";

function StaticSummaryPage(props) {
  return (
    <Switch>
      <Route path="/static-summary/:id">
        <StaticSummary />
      </Route>
    </Switch>
  );
}

export default StaticSummaryPage;
