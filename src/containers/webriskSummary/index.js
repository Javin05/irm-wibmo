import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WebriskSummaryAWS from '../../components/dashboard/WebriskSummaryAWS'

function WebriskSummary(props) {
    return (
        <Switch>
            <Route path='/webrisk-dashboard'>
                <WebriskSummaryAWS />
            </Route>
        </Switch>
    )
}

export default WebriskSummary