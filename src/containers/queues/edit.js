import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AddQueue from '../../components/queues/addQueues'
import { PageTitle } from '../../theme/layout/core'
import { useLocation, Link, useParams } from 'react-router-dom'

const merchant = [
  {
    title: 'Merchant',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function QueuesSelect() {

  return (
    <Switch>
      <Route path='/queues/update/:id'>
        <PageTitle breadcrumbs={[]}> Update Queues </PageTitle>
        <AddQueue />
      </Route>
    </Switch>
  )
}

export default QueuesSelect
