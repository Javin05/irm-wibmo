import React from 'react'
import { Switch, Route } from 'react-router-dom'
import RiskDetails from '../../components/transaction/RiskDetails'
import { PageTitle } from '../../theme/layout/core'

function TransactionDashboards (props) {

  return (
    <Switch>
      <Route path='/transaction-dashboard/update/:id'>
      <PageTitle breadcrumbs={[]}/> 
        <RiskDetails />
      </Route>
    </Switch>
  )
}

export default TransactionDashboards