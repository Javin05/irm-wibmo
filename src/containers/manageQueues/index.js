import React from 'react'
import { Switch, Route } from 'react-router-dom'
import ManageQueues from '../../components/queues/ManageQueues '
import { PageTitle } from '../../theme/layout/core'
import { useLocation, Link, useParams } from 'react-router-dom'

const merchant = [
  {
    title: 'Merchant',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function QueuesSelect() {
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('queues/')
  const currentId = url && url[1]



  return (
    <Switch>
      <Route path='/manage-queues'>
         <PageTitle breadcrumbs={[]}> Manage - Queues </PageTitle>
        <ManageQueues />
      </Route>
    </Switch>
  )
}

export default QueuesSelect
