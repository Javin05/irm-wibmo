import React from 'react'
import { Switch, Route } from 'react-router-dom'
import OGMAuditSummaryAWS from '../../components/dashboard/OGMAuditSummaryAWS'

function OGMAuditSummary(props) {
    return (
        <Switch>
            <Route path='/ogmanalysis-dashboard'>
                <OGMAuditSummaryAWS />
            </Route>
        </Switch>
    )
}

export default OGMAuditSummary