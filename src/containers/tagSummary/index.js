import React from 'react'
import { Switch, Route } from 'react-router-dom'
import TagSummaryList from '../../components/tagSummary/index'
import { PageTitle } from '../../theme/layout/core'


function Dashboard(props) {
  return (
    <Switch>
      <Route path='/tag-summary'>
        <PageTitle breadcrumbs={{}}>Batch Summary</PageTitle>
        <TagSummaryList />
      </Route>
    </Switch>
  )
}

export default Dashboard
