import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import OnBoardingManagementList from '../../components/onBoarding/onBoardingManagement'
import OnBoardingSummary from '../../components/OnBoardingSummary/OnBoardingSummary'

function IssuerBank () {

  return (
    <Switch>
        <Route path='/onboarding-search'>
        <PageTitle breadcrumbs={{}}>Cases</PageTitle>
        <OnBoardingManagementList />
      </Route>
      <Route path='/onboarding'>
        <PageTitle breadcrumbs={{}}>On-Boarding</PageTitle>
        <OnBoardingManagementList />
      </Route>
      <Route path='/onboarding/update/:id'>
        <PageTitle breadcrumbs={{}}>On-Boarding</PageTitle>
        <OnBoardingSummary />
      </Route>
    </Switch>
  )
}

export default IssuerBank
