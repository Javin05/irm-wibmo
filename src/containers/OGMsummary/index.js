import React from 'react'
import { Switch, Route } from 'react-router-dom'
import OgmSummary from '../../components/OGMsummary'
import { PageTitle } from '../../theme/layout/core'


function Dashboard(props) {
  return (
    <Switch>
      <Route path='/ogm-summary'>
        <PageTitle breadcrumbs={{}}>OGM Summary</PageTitle>
        <OgmSummary />
      </Route>
    </Switch>
  )
}

export default Dashboard
