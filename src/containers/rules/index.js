import React from "react";
import { Switch, Route } from "react-router-dom";
import Rules from "../../components/rules/Rules";
import { PageTitle } from "../../theme/layout/core";
import RuleForm from "../../components/rules/ruleForm";

const merchant = [
  {
    title: "Manage Input Fields",
    isSeparator: false,
    isActive: false,
  },
  {
    title: "",
    path: "",
    isSeparator: true,
    isActive: false,
  },
];

function RulesSelect() {
  return (
    <Switch>
      <Route path="/rules">
        <PageTitle breadcrumbs={[]}>Rules</PageTitle>
        <Rules />
      </Route>
      <Route path="/rule/update/:id">
        <PageTitle breadcrumbs={[]}>Update Rules</PageTitle>
        <RuleForm />
      </Route>
      <Route path="/rule-form/add">
        <PageTitle breadcrumbs={[]}> Add Rules</PageTitle>
        <RuleForm />
      </Route>
    </Switch>
  );
}

export default RulesSelect;
