import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AmLQueueList from '../../components/amlQueue/AMLlist'
import AMLdashboard from '../../components/amlQueue/AmlDashboard'
import { PageTitle } from '../../theme/layout/core'


function Dashboard(props) {
  return (
    <Switch>
      <Route path='/aml-queue'>
        <PageTitle breadcrumbs={{}}>AML Queue </PageTitle>
        <AmLQueueList />
      </Route>
      <Route path='/aml-queue-search'>
        <PageTitle breadcrumbs={{}}>AML Queue </PageTitle>
        <AmLQueueList />
      </Route>
      <Route path='/aml-dashboard/update/:id'>
        <PageTitle breadcrumbs={{}}></PageTitle>
        <AMLdashboard />
      </Route>
    </Switch>
  )
}

export default Dashboard
