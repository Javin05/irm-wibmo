import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WRMSimplBillingSummaryAWS from '../../components/dashboard/WRMSimplBillingSummaryAWS'

function WRMSimplBillingSummary(props) {
    return (
        <Switch>
            <Route path='/simpl-billing'>
                <WRMSimplBillingSummaryAWS />
            </Route>
        </Switch>
    )
}

export default WRMSimplBillingSummary