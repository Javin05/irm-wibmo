import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import WrmOperationManagement from '../../components/wrmOperationManagmement'
import WrmOperationDetails from '../../components/wrmOperationDetails/index'

function WrmOperationManage() {
  return (
    <Switch>
       <Route path='/wrm-management'>
        <PageTitle breadcrumbs={{}}>Wrm Queue</PageTitle>
        <WrmOperationManagement />
      </Route>
        <Route path="/wrmmanagement/update/:id">
        <PageTitle breadcrumbs={[]}>WRM Operation Managment</PageTitle>
        <WrmOperationDetails/>
      </Route>
    </Switch>
  )
}

export default WrmOperationManage