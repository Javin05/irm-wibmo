import { 
    PriceTypes 
} from '../actions'

const PopupInits = {
    priceSuccess: null,
    priceCheckList: null,
}

export const PriceStore = 'PriceStore'

export const PriceReducer = (state = PopupInits, action) => {

    switch (action.type) {

        case PriceTypes.FETCH_PRICE_POPUP_INIT:
        case PriceTypes.PRICE_POPUP_INIT:
            return { 
                ...state, 
                loading: true 
            }

        case PriceTypes.PRICE_POPUP_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                priceSuccess: action
            }

        case PriceTypes.FETCH_PRICE_POPUP_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                priceCheckList: action.data?.data
            }

            case PriceTypes.PRICE_POPUP_CLEAR:
            return { 
                ...state, 
                loading: false, 
                priceSuccess: null
            }
            
        case PriceTypes.FETCH_PRICE_POPUP_ERROR:
        case PriceTypes.PRICE_POPUP_ERROR:
            return { 
                ...state, 
                loading: false, 
                error: action 
            }

        default:
            return state
    }
}