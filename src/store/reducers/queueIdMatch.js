import { QueueIdActionsTypes } from '../actions'

export const QueueIdInitialState = {
  data: null
}

export const QueueIdStoreKey = 'QueueIdStore'

export const QueueIdReducer = (state = QueueIdInitialState, action) => {
  switch (action.type) {
    case QueueIdActionsTypes.QUEUEID:
      return { ...state, loading: true }
    case QueueIdActionsTypes.SAVE_QUEUEID_RESPONSE:
      return {
        ...state,
        QueueIdResponce: action.data,
        loading: false
      }
    case QueueIdActionsTypes.CLEAR_QUEUEID:
      return { ...state, QueueIdResponce: null }
    default:
      return state
  }
}