import { 
  riskManagementActionsTypes, 
  merchantIdDetailsTypes, 
  linkAnalyticsActionsTypes,
  ExportListActionsTypes,
  clientIdLIstActionsTypes,
  BWListActionsTypes,
  PlayStoreExportTypes,
  PlayStoreDashboardTypes,
  exportFullReportActionsTypes
} from '../actions'

export const riskManagementlistInitialState = {
  list: null
}

export const riskManagementlistStoreKey = 'riskManagementlistStore'

export const riskManagementlistReducer = (state = riskManagementlistInitialState, action) => {
  switch (action.type) {
    case riskManagementActionsTypes.GET_RISK_MANAGEMENT_LIST:
      return { ...state, loading: true }
    case riskManagementActionsTypes.SAVE_RISK_MANAGEMENT_LIST_RESPONSE:
      return { ...state, riskmgmtlists: action.data, loading: false }
    case riskManagementActionsTypes.CLEAR_RISK_MANAGEMENT_LIST:
      return { ...state, riskmgmtlists: null }
    default:
      return state
  }
}

export const editMerchantInitialState = {
  list: null
}

export const editMerchantStoreKey = 'editMerchantStore'

export const editMerchantReducer = (state = editMerchantInitialState, action) => {
  switch (action.type) {
    case merchantIdDetailsTypes.GET_MERCHANT_ID:
      return { ...state, loadingEA: true }
    case merchantIdDetailsTypes.MERCHANT_ID_RESPONSE:
      return { ...state, merchantIddetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case merchantIdDetailsTypes.CLEAR_MERCHANT_ID:
      return { ...state, merchantIddetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const linkAnalyticslistInitialState = {
  list: null
}

export const linkAnalyticslistStoreKey = 'linkAnalyticslistStore'

export const linkAnalyticslistReducer = (state = linkAnalyticslistInitialState, action) => {
  switch (action.type) {
    case linkAnalyticsActionsTypes.GETLINK_ANALYTIS_LIST:
      return { ...state, loading: true }
    case linkAnalyticsActionsTypes.SAVELINK_ANALYTIS_LIST_RESPONSE:
      return { ...state, linkAnalyticallists: action.data, loading: false }
    case linkAnalyticsActionsTypes.CLEARLINK_ANALYTIS_LIST:
      return { ...state, linkAnalyticallists: null }
    default:
      return state
  }
}

export const exportlistInitialState = {
  list: null
}

export const exportlistStoreKey = 'exportlistStore'

export const exportlistReducer = (state = exportlistInitialState, action) => {
  switch (action.type) {
    case ExportListActionsTypes.GET_EXPORT_LIST:
      return { ...state, loading: true }
    case ExportListActionsTypes.SAVE_EXPORT_LIST_RESPONSE:
      return { ...state, exportLists: action, loading: false }
    case ExportListActionsTypes.CLEAR_EXPORT_LIST:
      return { ...state, exportLists: null }
    default:
      return state
  }
}

export const clinetListInitialState = {
  list: null
}

export const clinetIDListStoreKey = 'clinetListStore'

export const clinetdListReducer = (state = clinetListInitialState, action) => {
  switch (action.type) {
    case clientIdLIstActionsTypes.GET_CLIENT_ID_LIST:
      return { ...state, loading: true }
    case clientIdLIstActionsTypes.SAVE_CLIENT_ID_LIST_RESPONSE:
      return { ...state, clinetIdLists: action.data, loading: false }
    case clientIdLIstActionsTypes.CLEAR_CLIENT_ID_LIST:
      return { ...state, clinetIdLists: null }
    default:
      return state
  }
}

export const BWlistInitialState = {
  list: null
}

export const BWlistStoreKey = 'BWlistStore'

export const BWlistReducer = (state = BWlistInitialState, action) => {
  switch (action.type) {
    case BWListActionsTypes.GET_BW_LIST:
      return { ...state, loading: true }
    case BWListActionsTypes.SAVE_BW_LIST_RESPONSE:
      return { ...state, BWLists: action, loading: false }
    case BWListActionsTypes.CLEAR_BW_LIST:
      return { ...state, BWLists: null }
    default:
      return state
  }
}

export const PlayStoreExportInitialState = {
  list: null
}

export const PlayStoreExportStoreKey = 'PlayStoreExportStore'

export const PlayStoreExportReducer = (state = PlayStoreExportInitialState, action) => {
  switch (action.type) {
    case PlayStoreExportTypes.GET_PLAYSTORE_EXPORT:
      return { ...state, loading: true }
    case PlayStoreExportTypes.SAVE_PLAYSTORE_EXPORT_RESPONSE:
      return { ...state, PlayStoreExportResponse: action, loading: false }
    case PlayStoreExportTypes.CLEAR_PLAYSTORE_EXPORT:
      return { ...state, PlayStoreExportResponse: null }
    default:
      return state
  }
}

export const PlayStoreDashboardInitialState = {
  list: null
}

export const PlayStoreDashboardStoreKey = 'PlayStoreDashboardStore'

export const PlayStoreDashboardReducer = (state = PlayStoreDashboardInitialState, action) => {
  switch (action.type) {
    case PlayStoreDashboardTypes.GET_PLAYSTORE_DASHBOARD:
      return { ...state, loading: true }
    case PlayStoreDashboardTypes.SAVE_PLAYSTORE_DASHBOARD_RESPONSE:
      return { ...state, PlayStoreDashboardResponse: action, loading: false }
    case PlayStoreDashboardTypes.CLEAR_PLAYSTORE_DASHBOARD:
      return { ...state, PlayStoreDashboardResponse: null }
    default:
      return state
  }
}

export const exportFullReportlistInitialState = {
  list: null
}

export const exportFullReportlistStoreKey = 'exportFullReportlistStore'

export const exportFullReportlistReducer = (state = exportFullReportlistInitialState, action) => {
  switch (action.type) {
    case exportFullReportActionsTypes.GET_EXPORT_FULLREPORT_LIST:
      return { ...state, loading: true }
    case exportFullReportActionsTypes.SAVE_EXPORT_FULLREPORT_LIST_RESPONSE:
      return { ...state, exportFullReportlists: action.data, loading: false }
    case exportFullReportActionsTypes.CLEAR_EXPORT_FULLREPORT_LIST:
      return { ...state, exportFullReportlists: null }
    default:
      return state
  }
}