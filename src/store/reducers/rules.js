import { rulesActionTypes, updateRulesActionsTypes, RulesGetId } from '../actions'

export const rulesInitialState = {
  list: null
}

export const rulesStoreKey = 'rulesStore'

export const rulesReducer = (state = rulesInitialState, action) => {
  switch (action.type) {
    case rulesActionTypes.GET_RULES:
      return { ...state, loading: true }
    case rulesActionTypes.POST_RULES:
      return { ...state, loadingARG: true, }
    case rulesActionTypes.POST_RULES_RESPONSE:
      return { ...state, addRules: action.data, statusAR: action.data.status, messageAR: action.data.message, loadingARG: false }
    case rulesActionTypes.POST_CLEAR_RULES:
      return { ...state, addRules: null, statusAR: null, messageAR: null }
    case rulesActionTypes.DELETE_RULES:
      return { ...state, loading: true }
    case rulesActionTypes.CLEAR_RULES:
      return { ...state, DeleteRules: null }
    case rulesActionTypes.DELETE_RULES_RESPONSE:
      return { ...state, DeleteRules: action.data, loading: false }
    case rulesActionTypes.SAVE_RULES_RESPONSE:
      return { ...state, rules: action.data, loading: false }
    case rulesActionTypes.CLEAR_RULES:
      return { ...state, rules: null }
    default:
      return state
  }
}

export const editRulesInitialState = {
  list: null
}

export const editRulesStoreKey = 'editRulesStore'

export const editRulesReducer = (state = editRulesInitialState, action) => {
  switch (action.type) { 
    case RulesGetId.GET_RULES_DETAILS:
      return { ...state, loadingGR: true }
    case RulesGetId.RULES_DETAILS_RESPONSE:
      return { ...state, RulesIdDetail: action.data && action.data.data, statusEA: action.data.status, messageEA: action.data.message, loadingGR: false }
    case RulesGetId.CLEAR_RULES_DETAILS:
      return { ...state, RulesIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const updateRulesInitialState = {
  data: null
}

export const updateRulesStoreKey = 'updateRulesStore'

export const updateRulesReducer = (state = updateRulesInitialState, action) => {
  switch (action.type) {
    case updateRulesActionsTypes.UPDATE_RULES:
      return { ...state, loadingUR: true }
    case updateRulesActionsTypes.SAVE_UPDATE_RULES_RESPONSE:
      return { ...state, updateRulesResponce: action.data, statusUR: action.data.status, messageUR: action.data.message, loadingUR: false }
    case updateRulesActionsTypes.CLEAR_UPDATE_RULES:
      return { ...state, updateRulesResponce: null, statusUR: '', messageUR:'' }
    default:
      return state
  }
}
