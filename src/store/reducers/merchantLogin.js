import { MerchantLoginTypes } from '../actions'

export const loginInitialState = {
  data: null
}

export const merchantLoginStoreKey = 'merchantLoginStore'

export const merchantLoginReducer = (state = loginInitialState, action) => {
  switch (action.type) {
    case MerchantLoginTypes.MERCHANT_LOGIN:
      return { ...state, loading: true }
    case MerchantLoginTypes.MERCHANT_LOGIN_RESPONSE:
      return { ...state, merchantLogin: action.data, loading: false }
    case MerchantLoginTypes.MERCHANT_CLEAR_LOGIN:
      return { ...state, merchantLogin: null }
    default:
      return state
  }
}

export default merchantLoginReducer
