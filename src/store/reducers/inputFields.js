import { inputFieldActionTypes } from '../actions'

export const inputFieldsInitialState = {
  inputfields: null
}

export const inputFieldsStoreKey = 'inputFieldsStore'

export const inputFieldsReducer = (state = inputFieldsInitialState, action) => {
  switch (action.type) {
    
    case inputFieldActionTypes.GET_INPUT_FIELDS:
      return { ...state, loading: true }
   
    case inputFieldActionTypes.SAVE_INPUT_FIELDS_RESPONSE:
      return { ...state, inputfields: action.data, loading: false }
  
    default:
      return state
  }
}
