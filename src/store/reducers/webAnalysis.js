import { 
    WebAnalysisTypes,
    getWebAnalysisTypes,
    ManualWebAnalysisTypes,
    DeleteWebAnalysisTypes,
    EditWebAnalysisTypes,
  updateWebAnalysisTypes,
} from '../actions'

export const WebAnalysisInitialState = {
  list: null
}

export const WebAnalysisStoreKey = 'WebAnalysisStore'

export const WebAnalysisReducer = (state = WebAnalysisInitialState, action) => {
  switch (action.type) {
    case WebAnalysisTypes.POST_WEBANALYSIS_LIST:
      return { ...state, loading: true }
    case WebAnalysisTypes.SAVE_WEBANALYSIS_LIST_RESPONSE:
      return { ...state, postCSVWebAnalysis: action.data, loading: false }
    case WebAnalysisTypes.CLEAR_WEBANALYSIS_LIST:
      return { ...state, postCSVWebAnalysis: null }
    default:
      return state
  }
}

export const ManualWebAnalysisInitialState = {
  list: null
}

export const ManualWebAnalysisStoreKey = 'ManualWebAnalysisStore'

export const ManualWebAnalysisReducer = (state = ManualWebAnalysisInitialState, action) => {
  switch (action.type) {
    case ManualWebAnalysisTypes.POST_MANUAL_WEBANALYSIS_LIST:
      return { ...state, loading: true }
    case ManualWebAnalysisTypes.SAVE_MANUAL_WEBANALYSIS_LIST_RESPONSE:
      return { ...state, postManualWebAnalysis: action.data, loading: false }
    case ManualWebAnalysisTypes.CLEAR_MANUAL_WEBANALYSIS_LIST:
      return { ...state, postManualWebAnalysis: null }
    default:
      return state
  }
}
  
export const getWebAnalysisInitialState = {
  list: null
}

export const getWebAnalysisStoreKey = 'getWebAnalysisStore'

export const getWebAnalysisReducer = (state = getWebAnalysisInitialState, action) => {
  switch (action.type) {
    case getWebAnalysisTypes.GET_WEBANALYSIS_LIST:
      return { ...state, loading: true }
    case getWebAnalysisTypes.SAVE_GET_WEBANALYSIS_LIST_RESPONSE:
      return { ...state, getWebAnalysis: action.data, loading: false }
    case getWebAnalysisTypes.CLEAR_GET_WEBANALYSIS_LIST:
      return { ...state, getWebAnalysis: null }
    default:
      return state
  }
}

export const DeleteWebAnalysisInitialState = {
  list: null
}

export const DeleteWebAnalysisStoreKey = 'DeleteWebAnalysisStore'

export const DeleteWebAnalysisReducer = (state = DeleteWebAnalysisInitialState, action) => {
  switch (action.type) {
    case DeleteWebAnalysisTypes.DELETE_WEBANALYSIS_DETAILS:
      return { ...state, loading: true }
    case DeleteWebAnalysisTypes.WEBANALYSIS_DETAILS_RESPONSE:
      return { ...state, deleteWebAnalysis: action.data, loading: false }
    case DeleteWebAnalysisTypes.CLEAR_WEBANALYSIS_DETAILS:
      return { ...state, deleteWebAnalysis: null }
    default:
      return state
  }
}

export const EditWebAnalysisInitialState = {
  list: null
}

export const EditWebAnalysisStoreKey = 'EditWebAnalysisStore'

export const EditWebAnalysisReducer = (state = EditWebAnalysisInitialState, action) => {
  switch (action.type) {
    case EditWebAnalysisTypes.EDIT_WEBANALYSIS_DETAILS:
      return { ...state, loading: true }
    case EditWebAnalysisTypes.EDIT_WEBANALYSIS_DETAILS_RESPONSE:
      return { ...state, EditWebAnalysis: action.data, loading: false }
    case EditWebAnalysisTypes.EDIT_CLEAR_WEBANALYSIS_DETAILS:
      return { ...state, EditWebAnalysis: null }
    default:
      return state
  }
}

export const UpdateWebAnalysisInitialState = {
  list: null
}

export const UpdateWebAnalysisStoreKey = 'UpdateWebAnalysisStore'

export const UpdateWebAnalysisReducer = (state = UpdateWebAnalysisInitialState, action) => {
  switch (action.type) {
    case updateWebAnalysisTypes.UPADTE_WEBANALYSIS_DETAILS:
      return { ...state, loading: true }
    case updateWebAnalysisTypes.UPDATE_WEBANALYSIS_DETAILS_RESPONSE:
      return { ...state, UpdateWebAnalysis: action.data, loading: false }
    case updateWebAnalysisTypes.UPDATE_CLEAR_WEBANALYSIS_DETAILS:
      return { ...state, UpdateWebAnalysis: null }
    default:
      return state
  }
}