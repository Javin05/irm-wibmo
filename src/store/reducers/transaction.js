import { TransactionTypes, transactionDashboardTypes, TransactionAddTypes, TransactionGetByIdTypes, dropdownTransactionTypes, CVVTransactionTypes,
  AVSTransactionTypes, PaymentTransactionTypes, LinkAnlyticsTransactionTypes, transactionDistanceTypes, merchantIdTypes
 } from '../actions'

export const TransactionlistInitialState = {
  list: null
}

export const TransactionlistStoreKeys = 'TransactionlistStore'

export const TransactionlistReducers = (state = TransactionlistInitialState, action) => {
  switch (action.type) {
    case TransactionTypes.GET_TRANSACTION_LIST:
      return { ...state, loading: true }
    case TransactionTypes.SAVE_TRANSACTION_LIST_RESPONSE:
      return { ...state, Transactionlists: action.data, loading: false }
    case TransactionTypes.CLEAR_TRANSACTION_LIST:
      return { ...state, Transactionlists: null }
    default:
      return state
  }
}


export const TransactionDashboardInitialState = {
  list: null
}

export const TransactionDashboardStoreKey = 'TransactionDashboardStore'

export const TransactionDashboardReducer = (state = TransactionDashboardInitialState, action) => {
  switch (action.type) {
    case transactionDashboardTypes.GET_TRANSACTIONDASHBOARD_DETAILS:
      return { ...state, loadingEA: true }
    case transactionDashboardTypes.TRANSACTIONDASHBOARD_DETAILS_RESPONSE:
      return { ...state, TransactionDashboardIdDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case transactionDashboardTypes.CLEAR_TRANSACTIONDASHBOARD_DETAILS:
      return { ...state, TransactionDashboardIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const TransactionAddInitialState = {
  data: null
}

export const TransactionAddStoreKey = 'TransactionAddStore'

export const TransactionAddReducer = (state = TransactionAddInitialState, action) => {
  switch (action.type) {
    case TransactionAddTypes.TRANSACTION_POST:
      return { ...state, loading: true }
    case TransactionAddTypes.TRANSACTION_POST_RESPONSE:
      return { ...state, TransactionAdd: action.data, Transactiontatus: action.data.status, queueMessage: action.data.message, loading: false }
    case TransactionAddTypes.TRANSACTION_POST_CLEAR:
      return { ...state, TransactionAdd: null }
    default:
      return state
  }
}

export const TransactionGetByIdInitialState = {
  data: null
}

export const TransactionGetByIdStoreKey = 'TransactionGetByIdStore'

export const TransactionGetByIdReducer = (state = TransactionGetByIdInitialState, action) => {
  switch (action.type) {
    case TransactionGetByIdTypes.TRANSACTION_GET_BY_ID:
      return { ...state, loading: true }
    case TransactionGetByIdTypes.TRANSACTION_GET_BY_ID_RESPONSE:
      return { ...state, TransactionGetById: action.data, Transactiontatus: action.data.status, queueMessage: action.data.message, loading: false }
    case TransactionGetByIdTypes.TRANSACTION_GET_BY_ID_CLEAR:
      return { ...state, TransactionGetById: null }
    default:
      return state
  }
}

export const DropDownTransactionInitialState = {
  list: null
}

export const DropDownTransactionStoreKeys = 'DropDownTransactionStore'

export const DropDownTransactionReducers = (state = DropDownTransactionInitialState, action) => {
  switch (action.type) {
    case dropdownTransactionTypes.GET_DROPDOWN_TRANSACTION_LIST:
      return { ...state, loading: true }
    case dropdownTransactionTypes.SAVE_DROPDOWN_TRANSACTION_LIST_RESPONSE:
      return { ...state, DropDownTransactionlists:  action.data && action.data.data, loading: false }
    case dropdownTransactionTypes.CLEAR_DROPDOWN_TRANSACTION_LIST:
      return { ...state, DropDownTransactionlists: null }
    default:
      return state
  }
} 

export const AVSTransactionInitialState = {
  list: null
}

export const AVSTransactionStoreKeys = 'AVSTransactionStore'

export const AVSTransactionReducers = (state = AVSTransactionInitialState, action) => {
  switch (action.type) {
    case AVSTransactionTypes.GET_AVS_TRANSACTION_LIST:
      return { ...state, loading: true }
    case AVSTransactionTypes.SAVE_AVS_TRANSACTION_LIST_RESPONSE:
      return { ...state, AVSTransactionlists:  action.data && action.data.data, loading: false }
    case AVSTransactionTypes.CLEAR_AVS_TRANSACTION_LIST:
      return { ...state, AVSTransactionlists: null }
    default:
      return state
  }
} 

export const CVVTransactionInitialState = {
  list: null
}

export const CVVTransactionStoreKeys = 'CVVTransactionStore'

export const CVVTransactionReducers = (state = CVVTransactionInitialState, action) => {
  switch (action.type) {
    case CVVTransactionTypes.GET_CVV_TRANSACTION_LIST:
      return { ...state, loading: true }
    case CVVTransactionTypes.SAVE_CVV_TRANSACTION_LIST_RESPONSE:
      return { ...state, CVVTransactionlists:  action.data && action.data.data, loading: false }
    case CVVTransactionTypes.CLEAR_CVV_TRANSACTION_LIST:
      return { ...state, CVVTransactionlists: null }
    default:
      return state
  }
} 

export const PaymentTransactionInitialState = {
  list: null
}

export const PaymentTransactionStoreKeys = 'PaymentTransactionStore'

export const PaymentTransactionReducers = (state = PaymentTransactionInitialState, action) => {
  switch (action.type) {
    case PaymentTransactionTypes.GET_PAYMENY_TRANSACTION_LIST:
      return { ...state, loading: true }
    case PaymentTransactionTypes.SAVE_PAYMENY_TRANSACTION_LIST_RESPONSE:
      return { ...state, PaymentTransactionlists:  action.data && action.data.data, loading: false }
    case PaymentTransactionTypes.CLEAR_PAYMENY_TRANSACTION_LIST:
      return { ...state, PaymentTransactionlists: null }
    default:
      return state
  }
}

export const LinkAnlyticsTransactionInitialState = {
  list: null
}

export const LinkAnlyticsTransactionStoreKeys = 'LinkAnlyticsTransactionStore'

export const LinkAnlyticsTransactionReducers = (state = LinkAnlyticsTransactionInitialState, action) => {
  switch (action.type) {
    case LinkAnlyticsTransactionTypes.GET_LINKANALYTICS_TRANSACTION_LIST:
      return { ...state, loading: true }
    case LinkAnlyticsTransactionTypes.SAVE_LINKANALYTICS_TRANSACTION_LIST_RESPONSE:
      return { ...state, LinkAnlyticslists:  action.data && action.data.data, loading: false }
    case LinkAnlyticsTransactionTypes.CLEAR_LINKANALYTICS_TRANSACTION_LIST:
      return { ...state, LinkAnlyticsTransactionlists: null }
    default:
      return state
  }
}

export const TransactionDistanceInitialState = {
  list: null
}

export const TransactionDistanceStoreKey = 'TransactionDistanceStore'

export const TransactionDistanceReducer = (state = TransactionDistanceInitialState, action) => {
  switch (action.type) {
    case transactionDistanceTypes.GET_TRANSACTION_DISTANCE_DETAILS:
      return { ...state, loadingEA: true }
    case transactionDistanceTypes.TRANSACTION_DISTANCE_DETAILS_RESPONSE:
      return { ...state, TransactionDistance: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case transactionDistanceTypes.CLEAR_TRANSACTION_DISTANCE_DETAILS:
      return { ...state, TransactionDistance: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const MerchantIDInitialState = {
  list: null
}

export const MerchantIDStoreKey = 'MerchantIDStore'

export const MerchantIDReducer = (state = MerchantIDInitialState, action) => {
  switch (action.type) {
    case merchantIdTypes.GET_MERCHANT_ID_DETAILS:
      return { ...state, loadingEA: true }
    case merchantIdTypes.MERCHANT_ID_DETAILS_RESPONSE:
      return { ...state, MerchantID: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case merchantIdTypes.CLEAR_MERCHANT_ID_DETAILS:
      return { ...state, MerchantID: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}