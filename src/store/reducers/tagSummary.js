import {
  TagSummaryTypes,
  UpdatetagstatusTypes,
  TagSummaryIdTypes,
  TagSummaryUpdateTypes,
  PostSendEmailTypes,
  BatchSummaryExportTypes,
  SummaryPlaytoreExportTypes,
  SummaryWebExportTypes
} from "../actions"

export const TagSummaryStoreKey = "TagSummaryStore"

export const TagSummaryInitialState = {
  list: null,
}

export const TagSummaryReducers = (
  state = TagSummaryInitialState,
  action
) => {
  switch (action.type) {
    case TagSummaryTypes.TAG_SUMMARYLIST:
      return { ...state, loading: true }
    case TagSummaryTypes.TAG_SUMMARYLIST_SUCCESS:
      return { ...state, TagSummaryData: action.data, loading: false }
    case TagSummaryTypes.TAG_SUMMARYLIST_CLEAR:
      return { ...state, TagSummaryData: null }
    default:
      return state
  }
}

export const UpdatetagstatusKey = "UpdatetagstatusStore"

export const UpdatetagstatusInitialState = {
  list: null,
}

export const UpdatetagstatusReducers = ( state = UpdatetagstatusInitialState, action ) => {
  switch (action.type) {
    case UpdatetagstatusTypes.UPDATE_TAGSTATUS:
      return { ...state, loading: true }
    case UpdatetagstatusTypes.UPDATETAGSTATUS_SUCCESS:
      return { ...state, UpdatetagstatusData: action.data, loading: false }
    case UpdatetagstatusTypes.UPDATETAGSTATUS_CLEAR:
      return { ...state, UpdatetagstatusData: null }
    default:
      return state
  }
}

export const TagSummaryIDStoreKey = "TagSummaryIDStore"

export const TagSummaryIDInitialState = {
  list: null,
}

export const TagSummaryIDReducers = (
  state = TagSummaryIDInitialState,
  action
) => {
  switch (action.type) {
    case TagSummaryIdTypes.TAG_SUMMARY_ID:
      return { ...state, loading: true }
    case TagSummaryIdTypes.TAG_SUMMARY_ID_SUCCESS:
      return { ...state, TagSummaryIdData: action.data, loading: false }
    case TagSummaryIdTypes.TAG_SUMMARY_ID_CLEAR:
      return { ...state, TagSummaryIdData: null }
    default:
      return state
  }
}

export const TagSummaryUpdateStoreKey = "TagSummaryUpdateStore"

export const TagSummaryUpdateInitialState = {
  list: null,
}

export const TagSummaryUpdateReducers = (
  state = TagSummaryUpdateInitialState,
  action
) => {
  switch (action.type) {
    case TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES:
      return { ...state, loading: true }
    case TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES_SUCCESS:
      return { ...state, TagSummaryUpdateData: action.data, loading: false }
    case TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES_CLEAR:
      return { ...state, TagSummaryUpdateData: null }
    default:
      return state
  }
}

export const PostSendEmailStoreKey = "PostSendEmailStore"

export const PostSendEmailInitialState = {
  list: null,
}

export const PostSendEmailReducers = (
  state = PostSendEmailInitialState,
  action
) => {
  switch (action.type) {
    case PostSendEmailTypes.POSE_SEND_EMAIL:
      return { ...state, loading: true }
    case PostSendEmailTypes.POSE_SEND_EMAIL_SUCCESS:
      return { ...state, PostSendEmailData: action.data, loading: false }
    case PostSendEmailTypes.POSE_SEND_EMAIL_CLEAR:
      return { ...state, PostSendEmailData: null }
    default:
      return state
  }
}

export const BatchSummaryExportStoreKey = "BatchSummaryExportStore"

export const BatchSummaryExportInitialState = {
  list: null,
}

export const BatchSummaryExportReducers = (
  state = BatchSummaryExportInitialState,
  action
) => {
  switch (action.type) {
    case BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT:
      return { ...state, loading: true }
    case BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT_SUCCESS:
      return { ...state, BatchSummaryExportData: action.data, loading: false }
    case BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT_CLEAR:
      return { ...state, BatchSummaryExportData: null }
    default:
      return state
  }
}

export const SummaryWebExportStoreKey = "SummaryWebExportStore"

export const SummaryWebExportInitialState = {
  list: null,
}

export const SummaryWebExportReducers = (
  state = SummaryWebExportInitialState,
  action
) => {
  switch (action.type) {
    case SummaryWebExportTypes.SUMMARY_WEB_EXPORT:
      return { ...state, loading: true }
    case SummaryWebExportTypes.SUMMARY_WEB_EXPORT_SUCCESS:
      return { ...state, summaryWebExportData: action.data, loading: false }
    case SummaryWebExportTypes.SUMMARY_WEB_EXPORT_CLEAR:
      return { ...state, summaryWebExportData: null }
    default:
      return state
  }
}

export const SummaryPlayExportStoreKey = "SummaryPlayExportStore"

export const SummaryPlayExportInitialState = {
  list: null,
}

export const SummaryPlayExportReducers = (
  state = SummaryPlayExportInitialState,
  action
) => {
  switch (action.type) {
    case SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT:
      return { ...state, loading: true }
    case SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT_SUCCESS:
      return { ...state, summaryPlayExportData: action.data, loading: false }
    case SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT_CLEAR:
      return { ...state, summaryPlayExportData: null }
    default:
      return state
  }
}