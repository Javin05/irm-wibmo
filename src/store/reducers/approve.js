import { ApproveActionsTypes, ApproveTXnActionsTypes } from '../actions'

export const ApproveInitialState = {
  data: null
}

export const ApproveStoreKey = 'ApproveStore'

export const ApproveReducer = (state = ApproveInitialState, action) => {
  switch (action.type) {
    case ApproveActionsTypes.APPROVE:
      return { ...state, loading: true }
    case ApproveActionsTypes.SAVE_APPROVE_RESPONSE:
      return {
        ...state,
        approveResponce: action.data,
        loading: false
      }
    case ApproveActionsTypes.CLEAR_APPROVE:
      return { ...state, approveResponce: null }
    default:
      return state
  }
}

export const ApproveTXnInitialState = {
  data: null
}

export const ApproveTXnStoreKey = 'ApproveTXnStore'

export const ApproveTXnReducer = (state = ApproveTXnInitialState, action) => {
  switch (action.type) {
    case ApproveTXnActionsTypes.APPROVE_TXN:
      return { ...state, loading: true }
    case ApproveTXnActionsTypes.SAVE_APPROVE_TXN_RESPONSE:
      return {
        ...state,
        ApproveTXnResponce: action.data,
        loading: false
      }
    case ApproveTXnActionsTypes.CLEAR_APPROVE_TXN:
      return { ...state, ApproveTXnResponce: null }
    default:
      return state
  }
}