import { AccountsQueueApproveActionsTypes } from '../actions'

export const AccountQueueApproveInitialState = {
  data: null
}

export const AccountsQueueApproveStoreKey = 'AccountsQueueApproveStore'

export const AccountsQueueApproveReducer = (state = AccountQueueApproveInitialState, action) => {
  switch (action.type) {
    case AccountsQueueApproveActionsTypes.APPROVE:
      return { ...state, loading: true }
    case AccountsQueueApproveActionsTypes.SAVE_APPROVE_RESPONSE:
      return {
        ...state,
        approveResponce: action.data,
        loading: false
      }
    case AccountsQueueApproveActionsTypes.CLEAR_APPROVE:
      return { ...state, approveResponce: null }
    default:
      return state
  }
}
