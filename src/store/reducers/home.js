import { HomeActionsTypes, QueueValuesActionsTypes } from '../actions'

export const HomeIdInitialState = {
  data: null
}

export const HomeIdStoreKey = 'HomeIdStore'

export const HomeIdReducer = (state = HomeIdInitialState, action) => {
  switch (action.type) {
    case HomeActionsTypes.GET_HOME_LIST:
      return { ...state, loading: true }
    case HomeActionsTypes.SAVE_HOME_LIST_RESPONSE:
      return {
        ...state,
        HomeIdResponce: action.data,
        loading: false
      }
    case HomeActionsTypes.CLEAR_HOME_LIST:
      return { ...state, HomeIdResponce: null }
    default:
      return state
  }
}

export const QueueValuesIdInitialState = {
  data: null
}

export const QueueValuesActionsIdStoreKey = 'QueueValuesActionsIdStore'

export const QueueValuesActionsIdReducer = (state = QueueValuesIdInitialState, action) => {
  switch (action.type) {
    case QueueValuesActionsTypes.GET_QUEUEVALUES_LIST:
      return { ...state, loading: true }
    case QueueValuesActionsTypes.SAVE_QUEUEVALUES_LIST_RESPONSE:
      return {
        ...state,
        QueueValuesResponce: action.data,
        loading: false
      }
    case QueueValuesActionsTypes.CLEAR_QUEUEVALUES_LIST:
      return { ...state, QueueValuesResponce: null }
    default:
      return state
  }
}
