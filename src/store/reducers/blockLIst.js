import { 
    BlockListTypes, 
    BlockListEmailTypes, 
    BlockListPhoneTypes, 
    BlockListUpdateTypes, 
    BlockListEditTypes,
    EmailBlocklistsTypes,
    BlockListUploadTypes,
    BlockListDeleteTypes,
    DeleteClientTypes
} from '../actions'
  
  export const BlockListlistInitialState = {
    list: null
  }
  
  export const BlockListlistStoreKeys = 'BlockListlistStore'
  
  export const BlockListlistReducers = (state = BlockListlistInitialState, action) => {
    switch (action.type) {
      case BlockListTypes.GET_BLOCKLIST_LIST:
        return { ...state, loading: true }
      case BlockListTypes.SAVE_BLOCKLIST_LIST_RESPONSE:
        return { ...state, BlockListlists: action.data, loading: false }
      case BlockListTypes.CLEAR_BLOCKLIST_LIST:
        return { ...state, BlockListlists: null }
      default:
        return state
    }
  }

  export const BlockListEmaillistInitialState = {
    list: null
  }
  
  export const BlockListEmaillistStoreKeys = 'BlockListTypeStore'
  
  export const BlockListEmaillistReducers = (state = BlockListEmaillistInitialState, action) => {
    switch (action.type) {
      case BlockListEmailTypes.GET_BLOCKLISTEMAIL_LIST:
        return { ...state, loading: true }
      case BlockListEmailTypes.SAVE_BLOCKLISTEMAIL_LIST_RESPONSE:
        return { ...state, BlockListType: action, loading: false }
      case BlockListEmailTypes.CLEAR_BLOCKLISTEMAIL_LIST:
        return { ...state, BlockListType: null }
      default:
        return state
    }
  }

  export const BlockListUploadlistInitialState = {
    list: null
  }
  
  export const BlockListUploadlistStoreKeys = 'BlockListUploadlistStore'
  
  export const BlockListUploadlistReducers = (state = BlockListUploadlistInitialState, action) => {
    switch (action.type) {
      case BlockListUploadTypes.GET_BLOCKLISTUPLOAD_LIST:
        return { 
            ...state, 
            loading: true,
            BlocklistsUploads: null  
        }
      case BlockListUploadTypes.SAVE_BLOCKLISTUPLOAD_LIST_RESPONSE:
        return { 
            ...state, 
            loading: false, 
            BlocklistsUploads: action.data, 
        }
      case BlockListUploadTypes.CLEAR_BLOCKLISTUPLOAD_LIST:
        return { 
            ...state, 
            loading: false, 
            BlocklistsUploads: null 
        }
      default:
        return state
    }
  }

  export const BlockListUpdatelistInitialState = {
    list: null
  }
  
  export const BlockListUpdatelistStoreKeys = 'BlockListUpdatelistStore'

export const BlockListUpdatelistReducers = (state = BlockListUpdatelistInitialState, action) => {
    switch (action.type) {
        case BlockListUpdateTypes.GET_BLOCKLIST_UPDATE_LIST:
            return { 
                ...state, 
                loading: true,
                BlockListUpdatelists:null 
            }
        case BlockListUpdateTypes.SAVE_BLOCKLIST_UPDATE_LIST_RESPONSE:
            return { 
                ...state, 
                loading: false,
                BlockListUpdatelists: action.data, 
            }
        case BlockListUpdateTypes.CLEAR_BLOCKLIST_UPDATE_LIST:
            return { 
                ...state, 
                loading: false,
                BlockListUpdatelists: null 
            }
        default:
            return state
    }
}


  export const BlockListEditlistInitialState = {
    list: null
  }
  
  export const BlockListEditlistStoreKeys = 'BlockListEditlistStore'
  
export const BlockListEditlistReducers = (state = BlockListEditlistInitialState, action) => {
    switch (action.type) {
        case BlockListEditTypes.GET_BLOCKLIST_EDIT_LIST:
            return { 
                ...state, 
                loading: true,
                BlockListlists: null,
                BlockListEditlists: null 
            }
        case BlockListEditTypes.SAVE_BLOCKLIST_EDIT_LIST_RESPONSE:
            return { 
                ...state, 
                loading: false, 
                BlockListEditlists: action.data
            }
        case BlockListEditTypes.CLEAR_BLOCKLIST_EDIT_LIST:
            return { 
                ...state, 
                loading: false,
                BlockListlists: null,
                BlockListEditlists: null 
            }
        default:
            return state
    }
}

  export const BlockListDeletelistInitialState = {
    list: null
  }
  
  export const BlockListDeleteStoreKeys = 'BlockListDeletelStore'
  
  export const BlockListDeleteReducers = (state = BlockListDeletelistInitialState, action) => {
    switch (action.type) {
        case BlockListDeleteTypes.GET_BLOCKLIST_DELETE_LIST:
            return { 
                ...state, 
                loading: true,
                DelteBlockList: null 
            }
        case BlockListDeleteTypes.SAVE_BLOCKLIST_DELETE_LIST_RESPONSE:
            return { 
                ...state, 
                loading: false, 
                DelteBlockList: action.data, 
            }
        case BlockListDeleteTypes.CLEAR_BLOCKLIST_DELETE_LIST:
            return { 
                ...state, 
                loading: false,
                DelteBlockList: null 
            }
        default:
            return state
    }
  }

    export const AddEmailToBlacklistKeysDefault = {
        emailSuccess: null
    }
    export const AddEmailToBlacklistKeys = 'AddEmailToBlacklistKeysone'
    export const EmailBlocklistReducers = (state = AddEmailToBlacklistKeysDefault, action) => {
    switch (action.type) {
        case EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_INIt:
            return { 
                ...state, 
                loading: true,
                emailSuccess: null 
            }
        case EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                emailSuccess: action.data, 
            }
        case EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_ERROR:
            return { 
                ...state, 
                loading: false, 
                emailSuccess: null 
            }
        case EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_CLEAR:
            return { 
                ...state, 
                loading: false, 
                emailSuccess: null 
            }
        default:
            return state
        }
  }

  export const DeleteClientInitialState = {
    list: null
  }
  
  export const DeleteClientStoreKeys = 'DeleteClientStore'
  
  export const DeleteClientReducers = (state = DeleteClientInitialState, action) => {
    switch (action.type) {
      case DeleteClientTypes.DELETE_CLIENT:
        return { ...state, loading: true }
      case DeleteClientTypes.DELETE_CLIENT_SUCCESS:
        return { ...state, DeleteBlockListClient: action, loading: false }
      case DeleteClientTypes.DELETE_CLIENT_CLEAR:
        return { ...state, DeleteBlockListClient: null }
      default:
        return state
    }
  }