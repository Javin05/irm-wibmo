import { 
  WRMOperatorsListTypes,
  WrmOperationManagementDetailTypes,
  WrmOperationManagementBackendAPIStatusTypes,
  WrmOperationManagementTypes,
  WrmOperatorActionTypes,
  WrmReportEditTypes,
  WrmOperationBackendApiAllScheduleTypes,
} from '../actions'

export const WrmOperationManagementInitialState = {
  data: null
}

export const WrmOperationManagementStoreKey = 'WrmOperationManagementStore'

export const WrmOperationManagementReducer = (state = WrmOperationManagementInitialState, action) => {
  switch (action.type) {
    case WrmOperationManagementTypes.GET_WRMOPERATIONMANAGEMENT_LIST:
      return { ...state, loading: true }
    case WrmOperationManagementTypes.SAVE_WRMOPERATIONMANAGEMENT_LIST_RESPONSE:
      return {
        ...state,
        WrmOperationManagement: action.data,
        loading: false
      }
    case WrmOperationManagementTypes.CLEAR_WRMOPERATIONMANAGEMENT_LIST:
      return { ...state, WrmRiskManagement: null }
    default:
      return state
  }
}

export const WrmOperatorsListInitialState = {
  data: null
}

export const WrmOperatorsListsStoreKey = 'WrmOperatorsListStore'

export const WrmOperatorsListReducer = (state = WrmOperatorsListInitialState, action) => {
  switch (action.type) {
    case WRMOperatorsListTypes.GET_WRMOPERATOR_LIST:
      return { ...state, loading: true }
    case WRMOperatorsListTypes.SAVE_WRMOPERATOR_LIST_RESPONSE:
      return {
        ...state,
        WrmOperators: action.data,
        loading: false
      }
    case WRMOperatorsListTypes.CLEAR_WRMOPERATOR_LIST:
      return { ...state, WrmOperators: null }
    default:
      return state
  }
}

export const WrmOperationManagementDetailStoreKey = 'WrmOperationManagementDetailStore'

export const WrmOperationManagementDetailReducer = (state = {}, action) => {
  switch (action.type) {
    case WrmOperationManagementDetailTypes.GET_WRMOPERATIONMANAGEMENT_DETAIL:
      return { ...state, loading: true }
    case WrmOperationManagementDetailTypes.SAVE_WRMOPERATIONMANAGEMENT_DETAIL_RESPONSE:
      return {
        ...state,
        WrmOperationManagementDetail: action.data.data,
        loading: false
      }
    case WrmOperationManagementDetailTypes.CLEAR_WRMOPERATIONMANAGEMENT_DETAIL:
      return { ...state, WrmOperationManagementDetail: null }
    default:
      return state
  }
}

export const WrmOperationManagementActionStoreKey = 'wrmOperationManagementActionStore'


export const WrmOperationManagementActionReducer = (state = {}, action) => {
  switch (action.type) {
    case WrmOperatorActionTypes.WRM_OPERATION_ACTION_INIT:
      return { ...state, loading: true }
    case WrmOperatorActionTypes.WRM_OPERATION_ACTION_RESPONSE_SUCCESS:
      return { ...state, dataEUR: action.data && action.data, statusEUR: action.data.status, messageEUR: action.data.message, loadingEUR: false }
    case WrmOperatorActionTypes.WRM_OPERATION_ACTION_CLEAR:
      return { ...state, dataEUR: null, messageEUR: '', statusEUR: ''}
    default:
      return state
  }
}

export const WrmOperationManagementBackendAPIStatusInitialState = {
  data: null
}

export const WrmOperationManagementBackendAPIStatusStoreKey = 'WrmOperationManagementBackendAPIStatusStore'

export const WrmOperationManagementBackendAPIStatusReducer = (state = WrmOperationManagementBackendAPIStatusInitialState, action) => {
  switch (action.type) {
    case WrmOperationManagementBackendAPIStatusTypes.GET_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS:
      return { ...state, loading: true }
    case WrmOperationManagementBackendAPIStatusTypes.SAVE_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS_RESPONSE:
      return {
        ...state,
        WrmOperationManagementBackendAPIStatus: action.data,
        loading: false
      }
    case WrmOperationManagementBackendAPIStatusTypes.CLEAR_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS:
      return { ...state, WrmOperationManagementBackendAPIStatus: null }
    default:
      return state
  }
}
export const WrmReportEditStoreKey = 'WrmReportEditStore'

export const WrmReportEditReducer = (state = {}, action) => {
  switch (action.type) {
    case WrmReportEditTypes.REQUEST:
      return { ...state, loading: true }
    case WrmReportEditTypes.RESPONSE:
      return {
        ...state,
        WrmEditRerportMessage: action?.data?.status,
        loading: false
      }
    case WrmReportEditTypes.CLEAR:
      return { ...state, WrmEditRerportMessage: null }
    default:
      return state
  }
}

export const WrmBackendApiSchedulerKey = 'WrmBackendApiSchedulerStore'

export const WrmBackendApiSchedulerReducer = (state = {}, action) => {
  switch (action.type) {
    case WrmOperationBackendApiAllScheduleTypes.REQUEST:
      return { ...state, loading: true }
    case WrmOperationBackendApiAllScheduleTypes.RESPONSE:
      return {
        ...state,
        WRM_API_MESSAGE: action.data.message,
        WRM_API_STATUS:action.data.status,
        loading: false
      }
    case WrmOperationBackendApiAllScheduleTypes.CLEAR:
      return { ...state, WRM_API_MESSAGE: "",WRM_API_STATUS:"" }
    default:
      return state
  }
}
