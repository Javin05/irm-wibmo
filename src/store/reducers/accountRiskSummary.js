import { 
    accountriskSummaryTypes, 
    accountmerchantIdDetailsTypes, 
    accountdashboardGetDetailsTypes, 
    accountriskScoreTypes,
    accountmatrixActionTypes,
    accountClientIdActionsTypes
} from '../actions'

// import { accountClientIdActionsTypes } from '../actions/accountRiskSummary'
export const accountriskSummaryInitialState = {
  list: null
}
export const accountriskSummaryStoreKey = 'accountriskSummaryStore'
export const accountriskSummaryReducer = (state = accountriskSummaryInitialState, action) => {
  switch (action.type) {
    case accountriskSummaryTypes.GET_ACCOUNT_RISK_SUMMARY:
      return { ...state, loading: true }
    case accountriskSummaryTypes.SAVE_ACCOUNT_RISK_SUMMARY_RESPONSE:
      return { ...state, accountriskSummarys: action.data, loading: false }
    case accountriskSummaryTypes.CLEAR_ACCOUNT_RISK_SUMMARY:
      return { ...state, accountriskSummarys: null }
    default:
      return state
  }
} 

export const accountmerchantIdDetailsInitialState = {
    list: null
  }
  export const accountmerchantIdDetailsStoreKey = 'accountmerchantIdDetailsStore'
  export const aaccountmerchantIdDetailsReducer = (state = accountmerchantIdDetailsInitialState, action) => {
    switch (action.type) {
      case accountmerchantIdDetailsTypes.GET_ACCOUNT_MERCHANT_ID:
        return { ...state, loading: true }
      case accountmerchantIdDetailsTypes.ACCOUNT_MERCHANT_ID_RESPONSE:
        return { ...state, aaccountmerchantIdDetails: action.data, loading: false }
      case accountmerchantIdDetailsTypes.CLEAR_ACCOUNT_MERCHANT_ID:
        return { ...state, aaccountmerchantIdDetails: null }
      default:
        return state
    }
  }

  export const accountdashboardGetDetailsInitialState = {
    list: null
  }
  export const accountdashboardGetDetailsStoreKey = 'accountdashboardGetDetailsStore'
  export const aaccountdashboardGetDetailsReducer = (state = accountdashboardGetDetailsInitialState, action) => {
    switch (action.type) {
      case accountdashboardGetDetailsTypes.GET_ACCOUNT_DASHBOARD_DETAILS:
        return { ...state, loading: true }
      case accountdashboardGetDetailsTypes.ACCOUNT_DASHBOARD_DETAILS_RESPONSE:
        return { ...state, aaccountdashboardGetDetails: action.data, loading: false }
      case accountdashboardGetDetailsTypes.CLEAR_ACCOUNT_DASHBOARD_DETAILS:
        return { ...state, aaccountdashboardGetDetails: null }
      default:
        return state
    }
  }

  export const accountriskScoreInitialState = {
    list: null
  }
  export const accountriskScoreStoreKey = 'accountriskScoreStore'
  export const aaccountriskScoreReducer = (state = accountriskScoreInitialState, action) => {
    switch (action.type) {
      case accountriskScoreTypes.GET_ACCOUNT_RISK_SCORE:
        return { ...state, loading: true }
      case accountriskScoreTypes.SAVE_ACCOUNT_RISK_SCORE_RESPONSE:
        return { ...state, aaccountriskScoreData: action.data, loading: false }
      case accountriskScoreTypes.CLEAR_ACCOUNT_RISK_SCORE:
        return { ...state, aaccountriskScoreData: null }
      default:
        return state
    }
  }

  export const accountmatrixInitialState = {
    list: null
  }
  export const accountmatrixStoreKey = 'accountmatrixStore'
  export const accountmatrixReducer = (state = accountmatrixInitialState, action) => {
    switch (action.type) {
      case accountmatrixActionTypes.GET_ACCOUNT_MATRIX_DETAILS:
        return { ...state, loading: true }
      case accountmatrixActionTypes.ACCOUNT_MATRIX_DETAILS_RESPONSE:
        return { ...state, accountmatrixData: action.data, loading: false }
      case accountmatrixActionTypes.CLEAR_ACCOUNT_MATRIX_DETAILS:
        return { ...state, accountmatrixData: null }
      default:
        return state
    }
  }

  export const ClientIdInitialState = {
    list: null
  }
  export const ClientIdStoreKey = 'ClientIdStore'
  export const ClientIdReducer = (state = ClientIdInitialState, action) => {
    switch (action.type) {
      case accountClientIdActionsTypes.GET_CLIENT_ID_DETAILS:
        return { ...state, loading: true }
      case accountClientIdActionsTypes.CLIENT_ID_DETAILS_RESPONSE:
        return { ...state, clientIdData: action.data, loading: false }
      case accountClientIdActionsTypes.CLEAR_CLIENT_ID_DETAILS:
        return { ...state, clientIdData: null }
      default:
        return state
    }
  }