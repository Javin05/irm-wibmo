import { AdminActionsTypes, AdminAddTypes, AdminGetId, updateAdminActionsTypes } from '../actions'
  
export const AdminslistInitialState = {
  list: null
}

export const AdminslistStoreKey = 'AdminslistStore'

export const AdminslistReducer = (state = AdminslistInitialState, action) => {
  switch (action.type) {
    case AdminActionsTypes.GET_ADMIN_LIST:
      return { ...state, loading: true }
    case AdminActionsTypes.SAVE_ADMIN_LIST_RESPONSE:
      return { ...state, Adminslists: action.data, loading: false }
    case AdminActionsTypes.CLEAR_ADMIN_LIST:
      return { ...state, Adminslists: null }
    default:
      return state
  }
} 

export const AdminsAddInitialState = {
  data: null
} 

export const AdminsAddStoreKey = 'AdminsAddStore'

export const AdminsAddReducer = (state = AdminsAddInitialState, action) => {
  switch (action.type) {
    case AdminAddTypes.ADMIN_POST:
      return { ...state, loading: true }
    case AdminAddTypes.ADMIN_POST_RESPONSE:
      return { ...state, AdminsAdd: action.data, AdminStatus:  action.data.status, AdminMessage:  action.data.message, loading: false }
    case AdminAddTypes.ADMIN_POST_CLEAR:
      return { ...state, AdminsAdd: null }
    default:
      return state
  }
}

export default AdminsAddReducer


export const editAdminsInitialState = {
  list: null
}

export const editAdminsStoreKey = 'editAdminsStore'

export const editAdminsReducer = (state = editAdminsInitialState, action) => {
  switch (action.type) {
    case AdminGetId.GET_ADMIN_DETAILS:
      return { ...state, loadingEA: true }
    case AdminGetId.ADMIN_DETAILS_RESPONSE:
      return { ...state, AdminsIdDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case AdminGetId.CLEAR_ADMIN_DETAILS:
      return { ...state, AdminsIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const updateAdminInitialState = {
  data: null
}

export const updateAdminStoreKey = 'updateAdminStore'

export const updateAdminReducer = (state = updateAdminInitialState, action) => {
  switch (action.type) {
    case updateAdminActionsTypes.UPDATE_ADMIN:
      return { ...state, loading: true }
    case updateAdminActionsTypes.SAVE_UPDATE_ADMIN_RESPONSE:
      return {
        ...state,
        updateAdminResponce: action.data,
        loading: false
      }
    case updateAdminActionsTypes.CLEAR_UPDATE_ADMIN:
      return { ...state, updateAdminResponce: null }
    default:
      return state
  }
}