import { 
    WatchListTypes 
} from '../actions'

const WatchListInitials = {
    watchlistCount: 0,
    watchlistData: null,
    watchlistTypeData : null,
    createWatchlistTypeResponse : null,
    updateWatchlistTypeResponse : null,
    deleteWatchlistTypeResponse : null,
    error: null
}

export const WatchListStore = 'WatchListStore'

export const watchlistReducer = (state = WatchListInitials, action) => {

    switch (action.type) {
        case WatchListTypes.FETCH_WATCHLIST_INIT:
        case WatchListTypes.FETCH_WATCHLISTTYPE_INIT:
        case WatchListTypes.CREATE_WATCHLISTTYPE_INIT:
        case WatchListTypes.UPDATE_WATCHLISTTYPE_INIT:
        case WatchListTypes.DELETE_WATCHLISTTYPE_INIT:
            return { 
                ...state, 
                loading: true 
            }

        case WatchListTypes.FETCH_WATCHLIST_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                watchlistData: action.data?.data
            }

        case WatchListTypes.FETCH_WATCHLISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                watchlistTypeData: action.data?.data,
                watchlistCount: action.data?.count,
                createWatchlistTypeResponse : null,
                updateWatchlistTypeResponse : null,
                deleteWatchlistTypeResponse : null,
            }

        case WatchListTypes.CREATE_WATCHLISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                createWatchlistTypeResponse: action.data?.status,
                updateWatchlistTypeResponse : null,
                deleteWatchlistTypeResponse : null
            }

        case WatchListTypes.UPDATE_WATCHLISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                updateWatchlistTypeResponse: action.data?.status,
                createWatchlistTypeResponse : null,
                deleteWatchlistTypeResponse : null
            }

        case WatchListTypes.DELETE_WATCHLISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                deleteWatchlistTypeResponse: action.data?.status,
                createWatchlistTypeResponse : null,
                updateWatchlistTypeResponse : null
            }

        case WatchListTypes.FETCH_WATCHLIST_ERROR:
        case WatchListTypes.FETCH_WATCHLISTTYPE_ERROR:
        case WatchListTypes.CREATE_WATCHLISTTYPE_ERROR:
        case WatchListTypes.UPDATE_WATCHLISTTYPE_ERROR:
        case WatchListTypes.DELETE_WATCHLISTTYPE_ERROR:
            return { 
                ...state, 
                error: action 
            }

        default:
            return state
    }
}