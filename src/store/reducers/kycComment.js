import { KYCCommentTypes, updateKYCCommentActionsTypes, KYCCommentdeleteActionsTypes, KYCDocumentdeleteActionsTypes} from '../actions'
  
  export const KYCCommentlistInitialState = {
    list: null
  }
  
  export const KYCCommentlistStoreKeys = 'KYCCommentlistStore'
  
  export const KYCCommentlistReducers = (state = KYCCommentlistInitialState, action) => {
    switch (action.type) {
      case KYCCommentTypes.GET_KYC_COMMENT_LIST:
        return { ...state, loading: true }
      case KYCCommentTypes.SAVE_KYC_COMMENT_LIST_RESPONSE:
        return { ...state, KYCCommentlists: action.data, loading: false }
      case KYCCommentTypes.CLEAR_KYC_COMMENT_LIST:
        return { ...state, KYCCommentlists: null }
      default:
        return state
    }
  }

  export const updateKYCCommentInitialState = {
    data: null
  }
  
  export const updateKYCCommentStoreKey = 'updateKYCCommentStore'
  
  export const updateKYCCommentReducer = (state = updateKYCCommentInitialState, action) => {
    switch (action.type) {
      case updateKYCCommentActionsTypes.UPDATE_KYC_COMMENT:
        return { ...state, loading: true }
      case updateKYCCommentActionsTypes.SAVE_UPDATE_KYC_COMMENT_RESPONSE:
        return {
          ...state,
          saveupdateKYCCommentResponse: action.data,
          loading: false
        }
      case updateKYCCommentActionsTypes.CLEAR_UPDATE_KYC_COMMENT:
        return { ...state, saveupdateKYCCommentResponse: null }
      default:
        return state
    }
  }

  export const DeleteKYCCommentInitialState = {
    DeleteKYCCommentData: null
  }
  export const KYCCommentdeleteStoreKey = 'KYCCommentdeleteStore'
  
  export const KYCCommentdeleteReducer = (state = DeleteKYCCommentInitialState, action) => {
    switch (action.type) {
      case KYCCommentdeleteActionsTypes.REQUEST:
        return { ...state, loading: true }
      case KYCCommentdeleteActionsTypes.RESPONSE:
        return { ...state, DeleteKYCCommentData: action.data, loading: false }
      case KYCCommentdeleteActionsTypes.CLEAR:
        return { ...state, DeleteKYCCommentData: null }
      default:
        return state
    }
  }

  export const DeleteKYCDocumentInitialState = {
    DeleteKYCDocumentData: null
  }
  export const KYCDocumentdeleteStoreKey = 'KYCDocumentdeleteStore'
  
  export const KYCDocumentdeleteReducer = (state = DeleteKYCDocumentInitialState, action) => {
    switch (action.type) {
      case KYCDocumentdeleteActionsTypes.REQUEST:
        return { ...state, loading: true }
      case KYCDocumentdeleteActionsTypes.RESPONSE:
        return { ...state, DeleteKYCDocumentData: action.data, loading: false }
      case KYCDocumentdeleteActionsTypes.CLEAR:
        return { ...state, DeleteKYCDocumentData: null }
      default:
        return state
    }
  }