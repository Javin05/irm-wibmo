import { 
  OgmOperationManagementDetailTypes,
  OgmOperationManagementTypes,
  OgmOperatorActionTypes,
} from '../actions'

export const OgmOperationManagementInitialState = {
  data: null
}

export const OgmOperationManagementStoreKey = 'OgmOperationManagementStore'

export const OgmOperationManagementReducer = (state = OgmOperationManagementInitialState, action) => {
  switch (action.type) {
    case OgmOperationManagementTypes.GET_OGMOPERATIONMANAGEMENT_LIST:
      return { ...state, loading: true }
    case OgmOperationManagementTypes.SAVE_OGMOPERATIONMANAGEMENT_LIST_RESPONSE:
      return {
        ...state,
        OgmOperationManagement: action.data,
        loading: false
      }
    case OgmOperationManagementTypes.CLEAR_OGMOPERATIONMANAGEMENT_LIST:
      return { ...state, OgmRiskManagement: null }
    default:
      return state
  }
}

export const OgmOperatorsListInitialState = {
  data: null
}


export const OgmOperationManagementDetailStoreKey = 'OgmOperationManagementDetailStore'

export const OgmOperationManagementDetailReducer = (state = {}, action) => {
  switch (action.type) {
    case OgmOperationManagementDetailTypes.GET_OGMOPERATIONMANAGEMENT_DETAIL:
      return { ...state, loading: true }
    case OgmOperationManagementDetailTypes.SAVE_OGMOPERATIONMANAGEMENT_DETAIL_RESPONSE:
      return {
        ...state,
        OgmOperationManagementDetail: action.data.data,
        loading: false
      }
    case OgmOperationManagementDetailTypes.CLEAR_OGMOPERATIONMANAGEMENT_DETAIL:
      return { ...state, OgmOperationManagementDetail: null }
    default:
      return state
  }
}

export const OgmOperationManagementActionStoreKey = 'OgmOperationManagementActionStore'


export const OgmOperationManagementActionReducer = (state = {}, action) => {
  switch (action.type) {
    case OgmOperatorActionTypes.OGM_OPERATION_ACTION_INIT:
      return { ...state, loading: true }
    case OgmOperatorActionTypes.OGM_OPERATION_ACTION_RESPONSE_SUCCESS:
      return { ...state, dataEUR: action.data && action.data, statusEUR: action.data.status, messageEUR: action.data.message, loadingEUR: false }
    case OgmOperatorActionTypes.OGM_OPERATION_ACTION_CLEAR:
      return { ...state, dataEUR: null, messageEUR: '', statusEUR: ''}
    default:
      return state
  }
}

