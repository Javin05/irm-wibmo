import { ClientManagementActionsTypes} from '../actions'
  
export const ClientManagementInitialState = {
  list: null
}

export const ClientManagementStoreKey = 'ClientManagementStore'

export const ClientManagementReducer = (state = ClientManagementInitialState, action) => {
  switch (action.type) {
    case ClientManagementActionsTypes.GET_CLIENTMANAGEMENT_LIST:
      return { ...state, loading: true }
    case ClientManagementActionsTypes.SAVE_CLIENTMANAGEMENT_LIST_RESPONSE:
      return { ...state, ClientManagements: action.data, loading: false }
    case ClientManagementActionsTypes.CLEAR_CLIENTMANAGEMENT_LIST:
      return { ...state, ClientManagements: null }
    default:
      return state
  }
} 