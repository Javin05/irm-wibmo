import {
  getClientActionTypes
} from '../actions'

export const clientCrudFilterStoreKey = 'clientCrudFilterStore'

export const clientCrudFilterReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case getClientActionTypes.GET_FILTERS_CLIENT:
      return { ...state, loadingGetClient: true }
    case getClientActionTypes.SAVE_FILTERS_CLIENT_RESPONSE:
      return {
        ...state,
        getClient: res.data && res.data.result,
        loadingGetClient: false,
        count: res.data && res.data.count,
        messageGetClient: res.message,
        statusGetClient: res.status
      }
    case getClientActionTypes.CLEAR_FILTERS_CLIENT:
      return {
        ...state,
        getClient: null,
        count: 0,
        messageGetClient: '',
        statusGetClient: ''
      }
    case getClientActionTypes.SET_FILTERS_FUNCTION:
      return {
        ...state,
        setFilterFunction: action.data
      }
    case getClientActionTypes.SET_FILTERS_PARAMS:
      return {
        ...state,
        setCredFilterParams: action.data
      }
    default:
      return state
  }
}