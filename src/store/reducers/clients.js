import {
  clientActionsTypes,
  usersRoleTypes,
  industryTypes,
  saveClientIdTypes,
  RiskweightageTypes,
  RiskweightagePostTypes,
  RiskweightageEditTypes,
  RiskweightageUpdateTypes,
  RiskweightageDeleteTypes,
  RiskyDomainTypes,
  RiskyDomainPostTypes,
  RiskyDomainEditTypes,
  RiskyDomainUpdateTypes,
  RiskyDomainDeleteTypes,
  GroupScoreWeightAgeTypes,
  GroupScoreWeightAgePostTypes,
  GroupScoreWeightAgeEditTypes,
  GroupScoreWeightAgeUpdateTypes,
  GroupScoreWeightAgeDeleteTypes,
  RiskScoreWeightAgeTypes,
  RiskScoreWeightAgePostTypes,
  RiskScoreWeightAgeEditTypes,
  RiskScoreWeightAgeUpdateTypes,
  RiskScoreWeightAgeDeleteTypes,
  GroupIdDrpdnTypes,
  ReportDrpdnTypes,
  ExportWRMEditTypes,
  ExportWRMTypes,
  ExportPlaystoreEditTypes,
  ExportPlaystoreTypes,
  CloneScoreTypes
 } from '../actions'

export const clientIdStoreKey = 'clientIdStore'

export const clientIdStoreReducer = (state = {}, action) => {
  switch (action.type) {
    case saveClientIdTypes.SAVE:
      return { ...state, getCurrentClientId: action.data }
    case saveClientIdTypes.CLEAR:
      return { ...state, getCurrentClientId: null }
    default:
      return state
  }
}

export const clientInitialState = {
  list: null
}

export const clientStoreKey = 'clientStore'

export const clientReducer = (state = clientInitialState, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case clientActionsTypes.GET_CLIENT:
      return { ...state, loading: true }
    case clientActionsTypes.SAVE_CLIENT_RESPONSE:
      return { ...state, getClient: res.data, loading: false }
    case clientActionsTypes.CLEAR_CLIENT:
      return { ...state, getClient: null }
    default:
      return state
  }
}

export const userRoleStoreKey = 'userRoleStore'

export const userRoleReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case usersRoleTypes.REQUEST:
      return { ...state, loading: true }
    case usersRoleTypes.RESPONSE:
      return { ...state, gerUserRole: res.data, loading: false }
    case usersRoleTypes.CLEAR:
      return { ...state, gerUserRole: null }
    default:
      return state
  }
}

export const industryStoreKey = 'industryStore'

export const industryReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case industryTypes.REQUEST:
      return { ...state, loading: true }
    case industryTypes.RESPONSE:
      return { ...state, getIndustry: res.data, count: res.count, loading: false }
    case industryTypes.CLEAR:
      return { ...state, getIndustry: null }
    default:
      return state
  }
}

export const RiskweightageStoreKey = 'RiskweightageStore'

export const RiskweightageReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskweightageTypes.REQUEST:
      return { ...state, loading: true }
    case RiskweightageTypes.RESPONSE:
      return { ...state, getRiskweightage: res, count: res, loading: false }
    case RiskweightageTypes.CLEAR:
      return { ...state, getRiskweightage: null }
    default:
      return state
  }
}

export const RiskweightagePostStoreKey = 'RiskweightagePostStore'

export const RiskweightagePostReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskweightagePostTypes.REQUEST:
      return { ...state, loading: true }
    case RiskweightagePostTypes.RESPONSE:
      return { ...state, RiskweightagePost: res, count: res.count, loading: false }
    case RiskweightagePostTypes.CLEAR:
      return { ...state, RiskweightagePost: null }
    default:
      return state
  }
}

export const RiskweightageEditStoreKey = 'RiskweightageEditStore'

export const RiskweightageEditReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskweightageEditTypes.REQUEST:
      return { ...state, loading: true }
    case RiskweightageEditTypes.RESPONSE:
      return { ...state, RiskweightageEdit: res, count: res.count, loading: false }
    case RiskweightageEditTypes.CLEAR:
      return { ...state, RiskweightageEdit: null }
    default:
      return state
  }
}

export const RiskweightageUpdateStoreKey = 'RiskweightageUpdateStore'

export const RiskweightageUpdateReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskweightageUpdateTypes.REQUEST:
      return { ...state, loading: true }
    case RiskweightageUpdateTypes.RESPONSE:
      return { ...state, RiskweightageUpdate: res, count: res.count, loading: false }
    case RiskweightageUpdateTypes.CLEAR:
      return { ...state, RiskweightageUpdate: null }
    default:
      return state
  }
}

export const RiskweightageDeleteStoreKey = 'RiskweightageDeleteStore'

export const RiskweightageDeleteReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskweightageDeleteTypes.REQUEST:
      return { ...state, loading: true }
    case RiskweightageDeleteTypes.RESPONSE:
      return { ...state, RiskweightageDelete: res, count: res.count, loading: false }
    case RiskweightageDeleteTypes.CLEAR:
      return { ...state, RiskweightageDelete: null }
    default:
      return state
  }
}

export const RiskyDomaingetStoreKey = 'RiskyDomaingetStore'

export const RiskyDomaingetReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskyDomainTypes.REQUEST:
      return { ...state, loading: true }
    case RiskyDomainTypes.RESPONSE:
      return { ...state, RiskyDomain: res.data, count: res.count, loading: false }
    case RiskyDomainTypes.CLEAR:
      return { ...state, RiskyDomain: null }
    default:
      return state
  }
}

export const RiskyDomainPostStoreKey = 'RiskyDomainPostStore'

export const RiskyDomainPostReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskyDomainPostTypes.REQUEST:
      return { ...state, loading: true }
    case RiskyDomainPostTypes.RESPONSE:
      return { ...state, RiskyDomainPost: res, count: res.count, loading: false }
    case RiskyDomainPostTypes.CLEAR:
      return { ...state, RiskyDomainPost: null }
    default:
      return state
  }
}

export const RiskyDomainEditStoreKey = 'RiskyDomainEditStore'

export const RiskyDomainEditReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskyDomainEditTypes.REQUEST:
      return { ...state, loading: true }
    case RiskyDomainEditTypes.RESPONSE:
      return { ...state, RiskyDomainEdit: res, count: res.count, loading: false }
    case RiskyDomainEditTypes.CLEAR:
      return { ...state, RiskyDomainEdit: null }
    default:
      return state
  }
}

export const RiskyDomainUpdateStoreKey = 'RiskyDomainUpdateStore'

export const RiskyDomainUpdateReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskyDomainUpdateTypes.REQUEST:
      return { ...state, loading: true }
    case RiskyDomainUpdateTypes.RESPONSE:
      return { ...state, RiskyDomainUpdate: res, count: res.count, loading: false }
    case RiskyDomainUpdateTypes.CLEAR:
      return { ...state, RiskyDomainUpdate: null }
    default:
      return state
  }
}

export const RiskyDomainDeleteStoreKey = 'RiskyDomainDeleteStore'

export const RiskyDomainDeleteReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskyDomainDeleteTypes.REQUEST:
      return { ...state, loading: true }
    case RiskyDomainDeleteTypes.RESPONSE:
      return { ...state, RiskyDomainDelete: res, count: res.count, loading: false }
    case RiskyDomainDeleteTypes.CLEAR:
      return { ...state, RiskyDomainDelete: null }
    default:
      return state
  }
}

export const GroupScoreWeightAgegetStoreKey = 'GroupScoreWeightAgegetStore'

export const GroupScoreWeightAgegetReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case GroupScoreWeightAgeTypes.REQUEST:
      return { ...state, loading: true }
    case GroupScoreWeightAgeTypes.RESPONSE:
      return { ...state, GroupScoreWeightAge: res.data, count: res.count, loading: false }
    case GroupScoreWeightAgeTypes.CLEAR:
      return { ...state, GroupScoreWeightAge: null }
    default:
      return state
  }
}

export const GroupScoreWeightAgePostStoreKey = 'GroupScoreWeightAgePostStore'

export const GroupScoreWeightAgePostReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case GroupScoreWeightAgePostTypes.REQUEST:
      return { ...state, loading: true }
    case GroupScoreWeightAgePostTypes.RESPONSE:
      return { ...state, GroupScoreWeightAgePost: res, count: res.count, loading: false }
    case GroupScoreWeightAgePostTypes.CLEAR:
      return { ...state, GroupScoreWeightAgePost: null }
    default:
      return state
  }
}

export const GroupScoreWeightAgeEditStoreKey = 'GroupScoreWeightAgeEditStore'

export const GroupScoreWeightAgeEditReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case GroupScoreWeightAgeEditTypes.REQUEST:
      return { ...state, loading: true }
    case GroupScoreWeightAgeEditTypes.RESPONSE:
      return { ...state, GroupScoreWeightAgeEdit: res, count: res.count, loading: false }
    case GroupScoreWeightAgeEditTypes.CLEAR:
      return { ...state, GroupScoreWeightAgeEdit: null }
    default:
      return state
  }
}

export const GroupScoreWeightAgeUpdateStoreKey = 'GroupScoreWeightAgeUpdateStore'

export const GroupScoreWeightAgeUpdateReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case GroupScoreWeightAgeUpdateTypes.REQUEST:
      return { ...state, loading: true }
    case GroupScoreWeightAgeUpdateTypes.RESPONSE:
      return { ...state, GroupScoreWeightAgeUpdate: res, count: res.count, loading: false }
    case GroupScoreWeightAgeUpdateTypes.CLEAR:
      return { ...state, GroupScoreWeightAgeUpdate: null }
    default:
      return state
  }
}

export const GroupScoreWeightAgeDeleteStoreKey = 'GroupScoreWeightAgeDeleteStore'

export const GroupScoreWeightAgeDeleteReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case GroupScoreWeightAgeDeleteTypes.REQUEST:
      return { ...state, loading: true }
    case GroupScoreWeightAgeDeleteTypes.RESPONSE:
      return { ...state, GroupScoreWeightAgeDelete: res, count: res.count, loading: false }
    case GroupScoreWeightAgeDeleteTypes.CLEAR:
      return { ...state, GroupScoreWeightAgeDelete: null }
    default:
      return state
  }
}

export const RiskScoreWeightAgegetStoreKey = 'RiskScoreWeightAgegetStore'

export const RiskScoreWeightAgegetReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskScoreWeightAgeTypes.REQUEST:
      return { ...state, loading: true }
    case RiskScoreWeightAgeTypes.RESPONSE:
      return { ...state, RiskScoreWeightAge: res.data, count: res.count, loading: false }
    case RiskScoreWeightAgeTypes.CLEAR:
      return { ...state, RiskScoreWeightAge: null }
    default:
      return state
  }
}

export const RiskScoreWeightAgePostStoreKey = 'RiskScoreWeightAgePostStore'

export const RiskScoreWeightAgePostReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskScoreWeightAgePostTypes.REQUEST:
      return { ...state, loading: true }
    case RiskScoreWeightAgePostTypes.RESPONSE:
      return { ...state, RiskScoreWeightAgePost: res, count: res.count, loading: false }
    case RiskScoreWeightAgePostTypes.CLEAR:
      return { ...state, RiskScoreWeightAgePost: null }
    default:
      return state
  }
}

export const RiskScoreWeightAgeEditStoreKey = 'RiskScoreWeightAgeEditStore'

export const RiskScoreWeightAgeEditReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskScoreWeightAgeEditTypes.REQUEST:
      return { ...state, loading: true }
    case RiskScoreWeightAgeEditTypes.RESPONSE:
      return { ...state, RiskScoreWeightAgeEdit: res, count: res.count, loading: false }
    case RiskScoreWeightAgeEditTypes.CLEAR:
      return { ...state, RiskScoreWeightAgeEdit: null }
    default:
      return state
  }
}

export const RiskScoreWeightAgeUpdateStoreKey = 'RiskScoreWeightAgeUpdateStore'

export const RiskScoreWeightAgeUpdateReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskScoreWeightAgeUpdateTypes.REQUEST:
      return { ...state, loading: true }
    case RiskScoreWeightAgeUpdateTypes.RESPONSE:
      return { ...state, RiskScoreWeightAgeUpdate: res, count: res.count, loading: false }
    case RiskScoreWeightAgeUpdateTypes.CLEAR:
      return { ...state, RiskScoreWeightAgeUpdate: null }
    default:
      return state
  }
}

export const RiskScoreWeightAgeDeleteStoreKey = 'RiskScoreWeightAgeDeleteStore'

export const RiskScoreWeightAgeDeleteReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case RiskScoreWeightAgeDeleteTypes.REQUEST:
      return { ...state, loading: true }
    case RiskScoreWeightAgeDeleteTypes.RESPONSE:
      return { ...state, RiskScoreWeightAgeDelete: res, count: res.count, loading: false }
    case RiskScoreWeightAgeDeleteTypes.CLEAR:
      return { ...state, RiskScoreWeightAgeDelete: null }
    default:
      return state
  }
}

export const GroupIdDrpdngetStoreKey = 'GroupIdDrpdngetStore'

export const GroupIdDrpdngetReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case GroupIdDrpdnTypes.REQUEST:
      return { ...state, loading: true }
    case GroupIdDrpdnTypes.RESPONSE:
      return { ...state, GroupIdDrpdn: res.data, count: res.count, loading: false }
    case GroupIdDrpdnTypes.CLEAR:
      return { ...state, GroupIdDrpdn: null }
    default:
      return state
  }
}

export const ReportDrpdngetStoreKey = 'ReportDrpdngetStore'

export const ReportDrpdngetReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case ReportDrpdnTypes.REQUEST:
      return { ...state, loading: true }
    case ReportDrpdnTypes.RESPONSE:
      return { ...state, ReportDrpdn: res.data, count: res.count, loading: false }
    case ReportDrpdnTypes.CLEAR:
      return { ...state, ReportDrpdn: null }
    default:
      return state
  }
}

export const ExportWRMEditStoreKey = 'ExportWRMEditStore'

export const ExportWRMEditReducer = (state = {}, action) => {
  switch (action.type) {
    case ExportWRMEditTypes.REQUEST:
      return { ...state, loadingEWEAWL: true }
    case ExportWRMEditTypes.RESPONSE:
      return { ...state, statusEWEAWL: action.data.status, messageEWEAWL: action.data.message, loadingEWEAWL: false }
    case ExportWRMEditTypes.CLEAR:
      return { ...state,statusEWEAWL:"",messageEWEAWL:"",loadingEWEAWL:false }
    default:
      return state
  }
}

export const ExportWRMStoreKey = 'ExportWRMStore'

export const ExportWRMReducer = (state = {}, action) => {
  switch (action.type) {
    case ExportWRMTypes.REQUEST:
      return { ...state, loadingEWAWL: true }
    case ExportWRMTypes.RESPONSE:
      return { ...state, dataEAWL: action.data && action.data.data, loadingEAWL: false }
    case ExportWRMTypes.CLEAR:
      return { ...state, dataEAWL: null,statusEAWL:"",messageEAWL:"",loadingEAWL:false }
    default:
      return state
  }
}

export const ExportPlaystoreEditStoreKey = 'ExportPlaystoreEditStore'

export const ExportPlaystoreEditReducer = (state = {}, action) => {
  switch (action.type) {
    case ExportPlaystoreEditTypes.REQUEST:
      return { ...state, loadingEWEAWL: true }
    case ExportPlaystoreEditTypes.RESPONSE:
      return { ...state, statusPSEE: action.data.status, messagePSEE: action.data.message, loadingPSEE: false }
    case ExportPlaystoreEditTypes.CLEAR:
      return { ...state,statusPSEE:"",messagePSEE:"",loadingPSEE:false }
    default:
      return state
  }
}

export const ExportPlaystoreStoreKey = 'ExportPlaystoreStore'

export const ExportPlaystoreReducer = (state = {}, action) => {
  switch (action.type) {
    case ExportPlaystoreTypes.REQUEST:
      return { ...state, loadingEWAWL: true }
    case ExportPlaystoreTypes.RESPONSE:
      return { ...state, dataPSEL: action.data && action.data.data, loadingPSEL: false }
    case ExportPlaystoreTypes.CLEAR:
      return { ...state, dataPSEL: null,statusPSEL:"",messagePSEL:"",loadingPSEL:false }
    default:
      return state
  }
}

export const CloneScoreTypesStoreKey = 'CloneScoreTypesStore'

export const CloneScoreTypesReducer = (state = {}, action) => {
  switch (action.type) {
    case CloneScoreTypes.REQUEST:
      return { ...state, loadingEWAWL: true }
    case CloneScoreTypes.RESPONSE:
      return { ...state, cloneScoreSetting: action.data, loading: false }
    case CloneScoreTypes.CLEAR:
      return { ...state, cloneScoreSetting: null,loadingPSEL:false }
    default:
      return state
  }
}