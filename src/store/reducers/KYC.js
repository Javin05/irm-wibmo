import {
  KYCTypes,
  KYCAddTypes,
  KYCphoneVerifyTypes,
  KYCphoneOtpTypes,
  KYCgstinTypes,
  KYCpanTypes,
  KYCcinTypes,
  KYCPersonalpanTypes,
  KYCemailOtpTypes,
  KYCemailTypes,
  KYCAddAllTypes,
  KYCaccountDetailsType,
  KYCAdharNumberType,
  KYCUSerTypes,
  FullKycValueType,
  AadhaarBackType,
  AadhaarFrontType,
  CommomFileType,
  DistanceType,
  AllDasboardDataType,
  DasboardAadharType,
  DasboardPanType,
  DasboardCinType,
  DasboardAPCType,
  DasboardAPCTwoType,
  KycStatusType,
  KycScoreType,
  IndidualPanUploadType,
  form80UploadType,
  form12UploadType,
  DashboardDropdownType,
  linkAnalyticsType,
  VideoKYCType,
  UENType,
  UENdashboardType,
  EntityType,
  EntityValueType,
  AddressDocType,
  CategoryValueType,
  SubCategoryValueType,
  PinCodeValueType,
  ExportReportValueType
} from "../actions"

export const KYClistInitialState = {
  list: null,
}

export const KYClistStoreKeys = "KYClistStore"

export const KYClistReducers = (state = KYClistInitialState, action) => {
  switch (action.type) {
    case KYCTypes.GET_KYC_LIST:
      return { ...state, loading: true }
    case KYCTypes.SAVE_KYC_LIST_RESPONSE:
      return { ...state, KYClists: action.data, loading: false }
    case KYCTypes.CLEAR_KYC_LIST:
      return { ...state, KYClists: null }
    default:
      return state
  }
}
export const KYCUserStoreKey = "KYCUserStoreKey"

export const KYCUserReducers = (state = KYClistInitialState, action) => {
  switch (action.type) {
    case KYCUSerTypes.KYC_USER_INIT:
      return { ...state, loading: true }
    case KYCUSerTypes.KYC_USER_RESPONSE_SUCCESS:
      return { ...state, KYCUser: action && action.data, loading: false }
    case KYCUSerTypes.KYC_USER_CLEAR:
      return { ...state, KYCUser: null }
    default:
      return state
  }
}

export const KYCAddInitialState = {
  data: null,
}

export const KYCAddStoreKey = "KYCAddStore"

export const KYCAddReducer = (state = KYCAddInitialState, action) => {
  switch (action.type) {
    case KYCAddTypes.KYC_POST:
      return { ...state, loading: true }
    case KYCAddTypes.KYC_POST_RESPONSE:
      return { ...state, KYCAddResponse: action.data, loading: false }
    case KYCAddTypes.KYC_POST_CLEAR:
      return { ...state, KYCAddResponse: null }
    default:
      return state
  }
}

export const KYCAddAllInitialState = {
  data: null,
}

export const KYCAddAllStoreKey = "KYCAddAllStore"

export const KYCAddAllReducer = (state = KYCAddAllInitialState, action) => {
  switch (action.type) {
    case KYCAddAllTypes.KYC_PUT:
      return { ...state, loading: true }
    case KYCAddAllTypes.KYC_PUT_RESPONSE:
      return { ...state, KYCAllDataResponse: action.data, loading: false }
    case KYCAddAllTypes.KYC_PUT_CLEAR:
      return { ...state, KYCAllDataResponse: null }
    default:
      return state
  }
}

export const KYCphoneInitialState = {
  data: null,
}
export const KYCphoneStoreKey = "KYCphoneStore"

export const KYCphoneReducer = (state = KYCphoneInitialState, action) => {
  switch (action.type) {
    case KYCphoneVerifyTypes.KYC_PHONE_VERIFY:
      return { ...state, loading: true }
    case KYCphoneVerifyTypes.KYC_PHONE_VERIFY_RESPONSE:
      return { ...state, KycPhoneVerify: action.data, loading: false }
    case KYCphoneVerifyTypes.KYC_PHONE_VERIFY_CLEAR:
      return { ...state, KycPhoneVerify: null }
    default:
      return state
  }
}

export const KYCphoneOtpInitialState = {
  data: null,
}
export const KYCphoneOtpeKey = "KYCphoneOtpStore"

export const KYCphoneOtpReducer = (state = KYCphoneOtpInitialState, action) => {
  switch (action.type) {
    case KYCphoneOtpTypes.KYC_PHONE_OTP:
      return { ...state, loading: true }
    case KYCphoneOtpTypes.KYC_PHONE_OTP_RESPONSE:
      return { ...state, KycPhoneOtp: action.data, loading: false }
    case KYCphoneOtpTypes.KYC_PHONE_OTP_CLEAR:
      return { ...state, KycPhoneOtp: null }
    default:
      return state
  }
}

export const KYCpanInitialState = {
  data: null,
}
export const KYCpanKey = "KYCbusinessPanStore"

export const KYCpanReducer = (state = KYCpanInitialState, action) => {
  switch (action.type) {
    case KYCpanTypes.KYC_PAN_VERIFY:
      return { ...state, loading: true }
    case KYCpanTypes.KYC_PAN_VERIFY_RESPONSE:
      return { ...state, KybusinessPan: action.data, loading: false }
    case KYCpanTypes.KYC_PAN_VERIFY_CLEAR:
      return { ...state, KybusinessPan: null }
    default:
      return state
  }
}

export const KYCgstinInitialState = {
  data: null,
}
export const KYCgstinKey = "KYCgstinStore"
export const KYCgstinReducer = (state = KYCgstinInitialState, action) => {
  switch (action.type) {
    case KYCgstinTypes.KYC_GSTIN_VERIFY:
      return { ...state, loading: true }
    case KYCgstinTypes.KYC_GSTIN_VERIFY_RESPONSE:
      return { ...state, Kycgstin: action.data, loading: false }
    case KYCgstinTypes.KYC_GSTIN_VERIFY_CLEAR:
      return { ...state, Kycgstin: null }
    default:
      return state
  }
}

export const KYCcinInitialState = {
  data: null,
}
export const KYCcinKey = "KYCcinStore"
export const KYCcinReducer = (state = KYCcinInitialState, action) => {
  switch (action.type) {
    case KYCcinTypes.KYC_CIN_VERIFY:
      return { ...state, loading: true }
    case KYCcinTypes.KYC_CIN_VERIFY_RESPONSE:
      return { ...state, KycCIN: action.data, loading: false }
    case KYCcinTypes.KYC_CIN_VERIFY_CLEAR:
      return { ...state, KycCIN: null }
    default:
      return state
  }
}

export const personalpanInitialState = {
  data: null,
}
export const personalpanKey = "PersonalPanStore"

export const personalpanReducer = (state = personalpanInitialState, action) => {
  switch (action.type) {
    case KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY:
      return { ...state, loading: true }
    case KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY_RESPONSE:
      return { ...state, personalPan: action.data, loading: false }
    case KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY_CLEAR:
      return { ...state, personalPan: null }
    default:
      return state
  }
}

export const EmailVerifyInitialState = {
  data: null,
}
export const EmailVerifyKey = "EmailVerifyStore"

export const EmailVerifyReducer = (state = EmailVerifyInitialState, action) => {
  switch (action.type) {
    case KYCemailTypes.KYC_EMAIL_VERIFY:
      return { ...state, loading: true }
    case KYCemailTypes.KYC_EMAIL_VERIFY_RESPONSE:
      return { ...state, emialVerify: action.data, loading: false }
    case KYCemailTypes.KYC_EMAIL_VERIFY_CLEAR:
      return { ...state, emialVerify: null }
    default:
      return state
  }
}

export const EmailVerifyOtpInitialState = {
  data: null,
}
export const EmailVerifyOtpKey = "EmailVerifyOtpStore"

export const EmailVerifyOtpReducer = (
  state = EmailVerifyInitialState,
  action
) => {
  switch (action.type) {
    case KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY:
      return { ...state, loading: true }
    case KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY_RESPONSE:
      return { ...state, emialOtpVerify: action.data, loading: false }
    case KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY_CLEAR:
      return { ...state, emialOtpVerify: null }
    default:
      return state
  }
}

export const accountDetailInitialState = {
  data: null,
}
export const accountDetailKey = "accountDetailStore"

export const accountDetailReducer = (
  state = accountDetailInitialState,
  action
) => {
  switch (action.type) {
    case KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY:
      return { ...state, loading: true }
    case KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY_RESPONSE:
      return { ...state, accountDetail: action.data, loading: false }
    case KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY_CLEAR:
      return { ...state, accountDetail: null }
    default:
      return state
  }
}

export const AdharNumberInitialState = {
  data: null,
}
export const AdharNumberKey = "AdharNumberStore"

export const AdharNumberReducer = (state = AdharNumberInitialState, action) => {
  switch (action.type) {
    case KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY:
      return { ...state, loading: true }
    case KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY_RESPONSE:
      return { ...state, AadharVerify: action.data, loading: false }
    case KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY_CLEAR:
      return { ...state, AadharVerify: null }
    default:
      return state
  }
}

export const FullKycValueInitialState = {
  data: null,
}
export const FullKycValueKey = "FullKycValueStore"

export const FullKycValueReducer = (state = FullKycValueInitialState, action) => {
  switch (action.type) {
    case FullKycValueType.FULL_KYC_VERIFY:
      return { ...state, loading: true }
    case FullKycValueType.FULL_KYC_VERIFY_RESPONSE:
      return { ...state, FullKycValue: action.data, loading: false }
    case FullKycValueType.FULL_KYC_VERIFY_CLEAR:
      return { ...state, FullKycValue: null }
    default:
      return state
  }
}

export const AadharUploadInitialState = {
  data: null,
}
export const AAdhaarUploadKey = "AAdhaarUploadStore"

export const AAdhaarUploadReducer = (state = AadharUploadInitialState, action) => {
  switch (action.type) {
    case AadhaarFrontType.AADHAAR_FRONT_VERIFY:
      return { ...state, loading: true }
    case AadhaarFrontType.AADHAAR_FRONT_VERIFY_RESPONSE:
      return { ...state, AadharFrontValue: action.data, loading: false }
    case AadhaarFrontType.AADHAAR_FRONT_VERIFY_CLEAR:
      return { ...state, AadharFrontValue: null }
    default:
      return state
  }
}

export const AAdhaarBackUploadInitialState = {
  data: null,
}
export const AAdhaarBackUploadKey = "AAdhaarBackUploadStore"

export const AAdhaarBackUploadReducer = (state = AAdhaarBackUploadInitialState, action) => {
  switch (action.type) {
    case AadhaarBackType.AADHAAR_BACK_VERIFY:
      return { ...state, loading: true }
    case AadhaarBackType.AADHAAR_BACK_VERIFY_RESPONSE:
      return { ...state, AadharBackValue: action.data, loading: false }
    case AadhaarBackType.AADHAAR_BACK_VERIFY_CLEAR:
      return { ...state, AadharBackValue: null }
    default:
      return state
  }
}

export const CommonFileInitialState = {
  data: null,
}
export const CommonFileKey = "CommonFileStore"

export const CommonFileReducer = (state = CommonFileInitialState, action) => {
  switch (action.type) {
    case CommomFileType.COMMON_FILE_VERIFY:
      return { ...state, loading: true }
    case CommomFileType.COMMON_FILE_VERIFY_RESPONSE:
      return { ...state, commonFileRes: action.data, loading: false }
    case CommomFileType.COMMON_FILE_VERIFY_CLEAR:
      return { ...state, commonFileRes: null }
    default:
      return state
  }
}

export const DistanceInitialState = {
  data: null,
}
export const DistanceKey = "DistanceStore"

export const DistanceReducer = (state = DistanceInitialState, action) => {
  switch (action.type) {
    case DistanceType.DISTANCE_DATA:
      return { ...state, loading: true }
    case DistanceType.DISTANCE_DATA_RESPONSE:
      return { ...state, DistanceRes: action.data, loading: false }
    case DistanceType.DISTANCE_DATA_CLEAR:
      return { ...state, DistanceRes: null }
    default:
      return state
  }
}

export const AllDashboardInitialState = {
  data: null,
}
export const AllDashboardKey = "AllDashboardStore"

export const AllDashboardReducer = (state = AllDashboardInitialState, action) => {
  switch (action.type) {
    case AllDasboardDataType.ALL_DASHBOARD:
      return { ...state, loading: true }
    case AllDasboardDataType.ALL_DASHBOARD_RESPONSE:
      return { ...state, AllDashboardRes: action.data, loading: false }
    case AllDasboardDataType.ALL_DASHBOARD_CLEAR:
      return { ...state, AllDashboardRes: null }
    default:
      return state
  }
}

export const DashBoardAadharInitialState = {
  data: null,
}
export const DashBoardAadharKey = "DashBoardAadharStore"

export const DashBoardAadharReducer = (state = DashBoardAadharInitialState, action) => {
  switch (action.type) {
    case DasboardAadharType.DAHBOARD_AADHAAR:
      return { ...state, loading: true }
    case DasboardAadharType.DAHBOARD_AADHAAR_RESPONSE:
      return { ...state, DashboardAadharRes: action.data, loading: false }
    case DasboardAadharType.DAHBOARD_AADHAAR_CLEAR:
      return { ...state, DashboardAadharRes: null }
    default:
      return state
  }
}

export const DashBoardPANInitialState = {
  data: null,
}
export const DashBoardPANKey = "DashBoardPANStore"

export const DashBoardPANReducer = (state = DashBoardPANInitialState, action) => {
  switch (action.type) {
    case DasboardPanType.DAHBOARD_PAN:
      return { ...state, loading: true }
    case DasboardPanType.DAHBOARD_PAN_RESPONSE:
      return { ...state, DashboardPANRes: action.data, loading: false }
    case DasboardPanType.DAHBOARD_PAN_CLEAR:
      return { ...state, DashboardPANRes: null }
    default:
      return state
  }
}

export const DashBoardCINInitialState = {
  data: null,
}
export const DashBoardCINKey = "DashBoardCINStore"

export const DashBoardCINReducer = (state = DashBoardCINInitialState, action) => {
  switch (action.type) {
    case DasboardCinType.DAHBOARD_CIN:
      return { ...state, loading: true }
    case DasboardCinType.DAHBOARD_CIN_RESPONSE:
      return { ...state, DashboardCINres: action.data, loading: false }
    case DasboardCinType.DAHBOARD_CIN_CLEAR:
      return { ...state, DashboardCINres: null }
    default:
      return state
  }
}

export const DashBoardAPCInitialState = {
  data: null,
}
export const DashBoardAPCKey = "DashBoardAPCStore"

export const DashBoardAPCReducer = (state = DashBoardAPCInitialState, action) => {
  switch (action.type) {
    case DasboardAPCType.DAHBOARD_APC:
      return { ...state, loading: true }
    case DasboardAPCType.DAHBOARD_APC_RESPONSE:
      return { ...state, DashboardAPCres: action.data, loading: false }
    case DasboardAPCType.DAHBOARD_APC_CLEAR:
      return { ...state, DashboardAPCres: null }
    default:
      return state
  }
}

export const DashBoardAPCTwoInitialState = {
  data: null,
}
export const DashBoardAPCTwoKey = "DashBoardAPCTwoStore"

export const DashBoardAPCTwoReducer = (state = DashBoardAPCInitialState, action) => {
  switch (action.type) {
    case DasboardAPCTwoType.DAHBOARD_APCTWO:
      return { ...state, loading: true }
    case DasboardAPCTwoType.DAHBOARD_APCTWO_RESPONSE:
      return { ...state, DashboardAPCTwores: action.data, loading: false }
    case DasboardAPCTwoType.DAHBOARD_APCTWO_CLEAR:
      return { ...state, DashboardAPCTwores: null }
    default:
      return state
  }
}

export const KycStatusInitialState = {
  data: null,
}
export const KycStatusKey = "KycStatusStore"

export const KycStatusReducer = (state = DashBoardAPCInitialState, action) => {
  switch (action.type) {
    case KycStatusType.KYC_STATUS:
      return { ...state, loading: true }
    case KycStatusType.KYC_STATUS_RESPONSE:
      return { ...state, KycStatusres: action.data, loading: false }
    case KycStatusType.KYC_STATUS_CLEAR:
      return { ...state, KycStatusres: null }
    default:
      return state
  }
}

export const KycScoreInitialState = {
  data: null,
}
export const KycScoreKey = "KycScoreStore"

export const KycScoreReducer = (state = DashBoardAPCInitialState, action) => {
  switch (action.type) {
    case KycScoreType.KYC_SCORE:
      return { ...state, loading: true }
    case KycScoreType.KYC_SCORE_RESPONSE:
      return { ...state, KycScoreres: action.data, loading: false }
    case KycScoreType.KYC_SCORE_CLEAR:
      return { ...state, KycScoreres: null }
    default:
      return state
  }
}

export const IndidualPanInitialState = {
  data: null,
}
export const IndidualPanKey = "IndidualPanStore"

export const IndidualPanReducer = (state = IndidualPanInitialState, action) => {
  switch (action.type) {
    case IndidualPanUploadType.INDIDUAL_PAN_UPLOAD:
      return { ...state, loading: true }
    case IndidualPanUploadType.INDIDUAL_PAN_UPLOAD_RESPONSE:
      return { ...state, IndidualPanUploadRes: action.data, loading: false }
    case IndidualPanUploadType.INDIDUAL_PAN_UPLOAD_CLEAR:
      return { ...state, IndidualPanUploadRes: null }
    default:
      return state
  }
}

export const Form80InitialState = {
  data: null,
}
export const Form80Key = "Form80Store"

export const Form80Reducer = (state = Form80InitialState, action) => {
  switch (action.type) {
    case form80UploadType.FPORM_80_G:
      return { ...state, loading: true }
    case form80UploadType.FPORM_80_G_RESPONSE:
      return { ...state, Form80Res: action.data, loading: false }
    case form80UploadType.FPORM_80_G_CLEAR:
      return { ...state, Form80Res: null }
    default:
      return state
  }
}

export const AddressDocInitialState = {
  data: null,
}
export const AddressDocKey = "AddressDocStore"

export const AddressDocReducer = (state = AddressDocInitialState, action) => {
  switch (action.type) {
    case AddressDocType.ADDRESS_DOC_UPLOAD:
      return { ...state, loading: true }
    case AddressDocType.ADDRESS_DOC_UPLOAD_RESPONSE:
      return { ...state, AdressUploadRes: action.data, loading: false }
    case AddressDocType.ADDRESS_DOC_UPLOAD_CLEAR:
      return { ...state, AdressUploadRes: null }
    default:
      return state
  }
}

export const Form12InitialState = {
  data: null,
}
export const Form12Key = "Form12Store"

export const Form12Reducer = (state = Form12InitialState, action) => {
  switch (action.type) {
    case form12UploadType.FPORM_12_G:
      return { ...state, loading: true }
    case form12UploadType.FPORM_12_G_RESPONSE:
      return { ...state, Form12Res: action.data, loading: false }
    case form12UploadType.FPORM_12_G_CLEAR:
      return { ...state, Form12Res: null }
    default:
      return state
  }
}

export const DashboardDropdownInitialState = {
  data: null,
}
export const DashboardDropdownKey = "DashboardDropdownStore"

export const DashboardDropdownReducer = (state = DashboardDropdownInitialState, action) => {
  switch (action.type) {
    case DashboardDropdownType.DASHBOARD_DROPDOWN:
      return { ...state, loading: true }
    case DashboardDropdownType.DASHBOARD_DROPDOWN_RESPONSE:
      return { ...state, DashBoardDropsownRes: action.data, loading: false }
    case DashboardDropdownType.DASHBOARD_DROPDOWN_CLEAR:
      return { ...state, DashBoardDropsownRes: null }
    default:
      return state
  }
}

export const LinkAnalyticsInitialState = {
  data: null,
}
export const LinkAnalyticsKey = "LinkAnalyticsStore"

export const LinkAnalyticsReducer = (state = LinkAnalyticsInitialState, action) => {
  switch (action.type) {
    case linkAnalyticsType.LINK_ANALYTICS:
      return { ...state, loading: true }
    case linkAnalyticsType.LINK_ANALYTICS_RESPONSE:
      return { ...state, LinkAnalyticsRes: action.data, loading: false }
    case linkAnalyticsType.LINK_ANALYTICS_CLEAR:
      return { ...state, LinkAnalyticsRes: null }
    default:
      return state
  }
}

export const VideoKYCInitialState = {
  data: null,
}
export const VideoKYCKey = "VideoKYCStore"

export const VideoKYCReducer = (state = VideoKYCInitialState, action) => {
  switch (action.type) {
    case VideoKYCType.VIDEO_KYC:
      return { ...state, loading: true }
    case VideoKYCType.VIDEO_KYC_RESPONSE:
      return { ...state, VideoKycRes: action.data, loading: false }
    case VideoKYCType.VIDEO_KYC_CLEAR:
      return { ...state, VideoKycRes: null }
    default:
      return state
  }
}

export const UENInitialState = {
  data: null,
}
export const UENKey = "UENStore"

export const UENReducer = (state = UENInitialState, action) => {
  switch (action.type) {
    case UENType.UEN_NUMBER:
      return { ...state, loading: true }
    case UENType.UEN_NUMBER_RESPONSE:
      return { ...state, UENres: action.data, loading: false }
    case UENType.UEN_NUMBER_CLEAR:
      return { ...state, UENres: null }
    default:
      return state
  }
}

export const UENdashboardInitialState = {
  data: null,
}
export const UENdashboardKey = "UENdashboardStore"

export const UENdashboardReducer = (state = UENdashboardInitialState, action) => {
  switch (action.type) {
    case UENdashboardType.UEN_NUMBER_DASHBOARD:
      return { ...state, loading: true }
    case UENdashboardType.UEN_NUMBER_DASHBOARD_RESPONSE:
      return { ...state, UENdashboardRes: action.data, loading: false }
    case UENdashboardType.UEN_NUMBER_DASHBOARD_CLEAR:
      return { ...state, UENdashboardRes: null }
    default:
      return state
  }
}

export const EntityInitialState = {
  data: null,
}
export const EntityStoreKey = "EntityStore"

export const EntityReducer = (state = EntityInitialState, action) => {
  switch (action.type) {
    case EntityType.ENTITY_GET:
      return { ...state, loading: true }
    case EntityType.ENTITY_GET_RESPONSE:
      return { ...state, EntityRes: action.data, loading: false }
    case EntityType.ENTITY_GET_CLEAR:
      return { ...state, EntityRes: null }
    default:
      return state
  }
}

export const EntityValueTypeInitialState = {
  data: null,
}
export const EntityValueTypeStoreKey = "EntityValueTypeStore"

export const EntityValueTypeReducer = (state = EntityValueTypeInitialState, action) => {
  switch (action.type) {
    case EntityValueType.ENTITY_VALUE_GET:
      return { ...state, loading: true }
    case EntityValueType.ENTITY_VALUE_GET_RESPONSE:
      return { ...state, EntityValue: action.data, loading: false }
    case EntityValueType.ENTITY_VALUE_GET_CLEAR:
      return { ...state, EntityValue: null }
    default:
      return state
  }
}

export const CategoryValueTypeInitialState = {
  data: null,
}
export const CategoryValueTypeStoreKey = "CategoryValueTypeStore"

export const CategoryValueTypeReducer = (state = CategoryValueTypeInitialState, action) => {
  switch (action.type) {
    case CategoryValueType.CATEGORY_VALUE_GET:
      return { ...state, loading: true }
    case CategoryValueType.CATEGORY_VALUE_GET_RESPONSE:
      return { ...state, CategoryValue: action.data, loading: false }
    case CategoryValueType.CATEGORY_VALUE_GET_CLEAR:
      return { ...state, CategoryValue: null }
    default:
      return state
  }
}

export const SubCategoryValueTypeInitialState = {
  data: null,
}
export const SubCategoryValueTypeStoreKey = "SubCategoryValueTypeStore"

export const SubCategoryValueTypeReducer = (state = SubCategoryValueTypeInitialState, action) => {
  switch (action.type) {
    case SubCategoryValueType.SUB_CATEGORY_VALUE_GET:
      return { ...state, loading: true }
    case SubCategoryValueType.SUB_CATEGORY_VALUE_GET_RESPONSE:
      return { ...state, subCategoryValue: action.data, loading: false }
    case SubCategoryValueType.SUB_CATEGORY_VALUE_GET_CLEAR:
      return { ...state, subCategoryValue: null }
    default:
      return state
  }
}

export const PinCodeValueTypeInitialState = {
  data: null,
}
export const PinCodeValueTypeStoreKey = "PinCodeValueTypeStore"

export const PinCodeValueTypeReducer = (state = PinCodeValueTypeInitialState, action) => {
  switch (action.type) {
    case PinCodeValueType.PINCODE_VALUE_GET:
      return { ...state, loading: true }
    case PinCodeValueType.PINCODE_VALUE_GET_RESPONSE:
      return { ...state, pincodeData: action.data, loading: false }
    case PinCodeValueType.PINCODE_VALUE_GET_CLEAR:
      return { ...state, pincodeData: null }
    default:
      return state
  }
}

export const ExportReportValueTypeInitialState = {
  data: null,
}
export const ExportReportValueTypeStoreKey = "ExportReportValueTypeStore"

export const ExportReportValueTypeReducer = (state = ExportReportValueTypeInitialState, action) => {
  switch (action.type) {
    case ExportReportValueType.EXPORT_REPORT_VALUE_GET:
      return { ...state, loading: true }
    case ExportReportValueType.EXPORT_REPORT_VALUE_GET_RESPONSE:
      return { ...state, exportData: action.data, loading: false }
    case ExportReportValueType.EXPORT_REPORT_VALUE_GET_CLEAR:
      return { ...state, exportData: null }
    default:
      return state
  }
}