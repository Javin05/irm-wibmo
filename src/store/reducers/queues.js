import { queuesActionsTypes, queuesTypes,
  queuesAction, queuesGetId, queuesGetIdActions,  updateQueuesActionsTypes } from '../actions'

export const queueslistInitialState = {
  list: null
}

export const queueslistStoreKey = 'queueslistStore'

export const queueslistReducer = (state = queueslistInitialState, action) => {
  switch (action.type) {
    case queuesActionsTypes.GET_QUEUES_LIST:
      return { ...state, loading: true }
    case queuesActionsTypes.SAVE_QUEUES_LIST_RESPONSE:
      return { ...state, queueslists: action.data, loading: false }
    case queuesActionsTypes.CLEAR_QUEUES_LIST:
      return { ...state, queueslists: null }
    default:
      return state
  }
}

export const queuesAddInitialState = {
  data: null
} 

export const queuesAddStoreKey = 'queuesAddStore'

export const queuesAddReducer = (state = queuesAddInitialState, action) => {
  switch (action.type) {
    case queuesTypes.QUEUES_POST:
      return { ...state, loading: true }
    case queuesTypes.QUEUES_POST_RESPONSE:
      return { ...state, queuesAdd: action.data, queueStatus:  action.data.status, queueMessage:  action.data.message, loading: false }
    case queuesTypes.QUEUES_POST_CLEAR:
      return { ...state, queuesAdd: null }
    default:
      return state
  }
}

export default queuesAddReducer


export const editqueuesInitialState = {
  list: null
}

export const editqueuesStoreKey = 'editqueuesStore'

export const editqueuesReducer = (state = editqueuesInitialState, action) => {
  switch (action.type) {
    case queuesGetId.GET_QUEUES_DETAILS:
      return { ...state, loadingEA: true }
    case queuesGetId.QUEUES_DETAILS_RESPONSE:
      return { ...state, queuesIdDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case queuesGetId.CLEAR_QUEUES_DETAILS:
      return { ...state, queuesIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const updateQueueInitialState = {
  data: null
}

export const updateQueueStoreKey = 'updateQueueStore'

export const updateQueueReducer = (state = updateQueueInitialState, action) => {
  switch (action.type) {
    case updateQueuesActionsTypes.UPDATE_QUEUES:
      return { ...state, loading: true }
    case updateQueuesActionsTypes.SAVE_UPDATE_QUEUES_RESPONSE:
      return {
        ...state,
        updateQueueResponce: action.data,
        loading: false
      }
    case updateQueuesActionsTypes.CLEAR_UPDATE_QUEUES:
      return { ...state, updateQueueResponce: null }
    default:
      return state
  }
}