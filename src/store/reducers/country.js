import { CountryActionsTypes, StateActionsTypes, CityActionsTypes, AreaActionsTypes } from '../actions'
  
export const CountrylistInitialState = {
  list: null
}

export const CountrylistStoreKey = 'CountrylistStore'

export const CountrylistReducer = (state = CountrylistInitialState, action) => {
  switch (action.type) {
    case CountryActionsTypes.GET_COUNTRY_LIST:
      return { ...state, loading: true }
    case CountryActionsTypes.SAVE_COUNTRY_LIST_RESPONSE:
      return { ...state, Countrylists: action.data && action.data.data, loading: false }
    case CountryActionsTypes.CLEAR_COUNTRY_LIST:
      return { ...state, Countrylists: null }
    default:
      return state
  }
}

export const StatelistInitialState = {
    list: null
  }
  
  export const StatelistStoreKey = 'StatelistStore'
  
  export const StatelistReducer = (state = StatelistInitialState, action) => {
    switch (action.type) {
      case StateActionsTypes.GET_STATE_LIST:
        return { ...state, loading: true }
      case StateActionsTypes.SAVE_STATE_LIST_RESPONSE:
        return { ...state, Statelists: action.data && action.data.data, loading: false }
      case StateActionsTypes.CLEAR_STATE_LIST:
        return { ...state, Statelists: null }
      default:
        return state
    }
  }

  export const CitylistInitialState = {
    list: null
  }
  
  export const CitylistStoreKey = 'CitylistStore'
  
  export const CitylistReducer = (state = CitylistInitialState, action) => {
    switch (action.type) {
      case CityActionsTypes.GET_CITY_LIST:
        return { ...state, loading: true }
      case CityActionsTypes.SAVE_CITY_LIST_RESPONSE:
        return { ...state, Citylists: action.data && action.data.data, loading: false }
      case CityActionsTypes.CLEAR_CITY_LIST:
        return { ...state, Citylists: null }
      default:
        return state
    }
  }

  export const ArealistInitialState = {
    list: null
  }
  
  export const ArealistStoreKey = 'ArealistStore'
  
  export const ArealistReducer = (state = ArealistInitialState, action) => {
    switch (action.type) {
      case AreaActionsTypes.GET_AREA_LIST:
        return { ...state, loading: true }
      case AreaActionsTypes.SAVE_AREA_LIST_RESPONSE:
        return { ...state, Arealists: action.data && action.data.data, loading: false }
      case AreaActionsTypes.CLEAR_AREA_LIST:
        return { ...state, Arealists: null }
      default:
        return state
    }
  }