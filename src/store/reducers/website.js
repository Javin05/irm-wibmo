import { WebsiteActionsTypes } from '../actions'

export const WebsiteInitialState = {
  data: null
}

export const WebsiteStoreKey = 'WebsiteStore'

export const WebsiteReducer = (state = WebsiteInitialState, action) => {
  switch (action.type) {
    case WebsiteActionsTypes.GET_WEBSITE_LIST:
      return { ...state, loading: true }
    case WebsiteActionsTypes.SAVE_WEBSITE_LIST_RESPONSE:
      return {
        ...state,
        WebsiteResponce: action.data,
        loading: false
      }
    case WebsiteActionsTypes.CLEAR_WEBSITE_LIST:
      return { ...state, WebsiteResponce: null }
    default:
      return state
  }
}