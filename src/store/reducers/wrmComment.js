import { WRMCommentTypes, updateWRMCommentActionsTypes, WRMCommentdeleteActionsTypes, WrmStatusType } from '../actions'
  
  export const WRMlistInitialState = {
    list: null
  }
  
  export const WRMlistStoreKeys = 'WRMlistStore'
  
  export const WRMlistReducers = (state = WRMlistInitialState, action) => {
    switch (action.type) {
      case WRMCommentTypes.GET_WRM_COMMENT_LIST:
        return { ...state, loading: true }
      case WRMCommentTypes.SAVE_WRM_COMMENT_LIST_RESPONSE:
        return { ...state, WRMlists: action.data, loading: false }
      case WRMCommentTypes.CLEAR_WRM_COMMENT_LIST:
        return { ...state, WRMlists: null }
      default:
        return state
    }
  }

  export const updateWRMInitialState = {
    data: null
  }
  
  export const updateWRMStoreKey = 'updateWRMStore'
  
  export const updateWRMReducer = (state = updateWRMInitialState, action) => {
    switch (action.type) {
      case updateWRMCommentActionsTypes.UPDATE_WRM_COMMENT:
        return { ...state, loading: true }
      case updateWRMCommentActionsTypes.SAVE_UPDATE_WRM_COMMENT_RESPONSE:
        return {
          ...state,
          saveupdateWRMResponse: action.data,
          loading: false
        }
      case updateWRMCommentActionsTypes.CLEAR_UPDATE_WRM_COMMENT:
        return { ...state, saveupdateWRMResponse: null }
      default:
        return state
    }
  }

  export const DeleteWRMInitialState = {
    DeleteWRMData: null
  }
  export const WRMdeleteStoreKey = 'WRMdeleteStore'
  
  export const WRMdeleteReducer = (state = DeleteWRMInitialState, action) => {
    switch (action.type) {
      case WRMCommentdeleteActionsTypes.REQUEST:
        return { ...state, loading: true }
      case WRMCommentdeleteActionsTypes.RESPONSE:
        return { ...state, DeleteWRMData: action.data, loading: false }
      case WRMCommentdeleteActionsTypes.CLEAR:
        return { ...state, DeleteWRMData: null }
      default:
        return state
    }
  }

  export const WrmStatusInitialState = {
    data: null,
  }
  export const WrmStatusKey = "WrmStatusStores"
  
  export const WrmStatusReducer = (state = WrmStatusInitialState, action) => {
    switch (action.type) {
      case WrmStatusType.WRM_STATUS:
        return { ...state, loading: true }
      case WrmStatusType.WRM_STATUS_RESPONSE:
        return { ...state, WrmStatusres: action.data, loading: false }
      case WrmStatusType.WRM_STATUS_CLEAR:
        return { ...state, WrmStatusres: null }
      default:
        return state
    }
  }