import {
  BusinessUploadDocType,
  chequeDetailsDocType,
  categoryEntityType,
  BusinessUpload4DocType,
  AccountUploadDocType,
  DashComUploadDocType,
  HufDeedUploadDocType,
  SolePropUploadDocType,
  DashReUploadDocType,
  DashVideoApproveType,
  uploadViewType,
  UpdateKycFvType,
  addKycFvType,
  kycStatusType,
  KycStatusChangeActionsTypes,
  KycUpdateQueueType,
} from "../actions"

export const uploadViewTypeInitialState = {
  data: null,
};
export const uploadViewTypeKey = "uploadViewTypeStore"

export const uploadViewTypeReducer = (state = uploadViewTypeInitialState, action) => {
  switch (action.type) {
    case uploadViewType.VIEW_UPLOAD:
      return { ...state, loading: true }
    case uploadViewType.VIEW_UPLOAD_RESPONSE:
      return { ...state, uploadViewRes: action.data, loading: false };
    case uploadViewType.VIEW_UPLOAD_CLEAR:
      return { ...state, uploadViewRes: null }
    default:
      return state
  }
}

export const BusinessDocInitialState = {
  data: null,
};
export const BusinessDocKey = "BusinessDocStore"

export const BusinessDocReducer = (state = BusinessDocInitialState, action) => {
  switch (action.type) {
    case BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD:
      return { ...state, loading: true }
    case BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD_RESPONSE:
      return { ...state, BusinessUploadRes: action.data, loading: false };
    case BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD_CLEAR:
      return { ...state, BusinessUploadRes: null }
    default:
      return state
  }
}

export const chequeUploadInitialState = {
  data: null,
};
export const chequeUploadKey = "chequeUploadStore"

export const chequeUploadReducer = (state = chequeUploadInitialState, action) => {
  switch (action.type) {
    case chequeDetailsDocType.CHEQUE_DOC_UPLOAD:
      return { ...state, loading: true }
    case chequeDetailsDocType.CHEQUE_DOC_UPLOAD_RESPONSE:
      return { ...state, chequeUpload: action.data, loading: false };
    case chequeDetailsDocType.CHEQUE_DOC_UPLOAD_CLEAR:
      return { ...state, chequeUpload: null }
    default:
      return state
  }
}

export const categoryEntityInitialState = {
  data: null,
};
export const categoryEntityKey = "categoryEntityStore"

export const categoryEntityReducer = (state = categoryEntityInitialState, action) => {
  switch (action.type) {
    case categoryEntityType.CATEGORY_ENTITY:
      return { ...state, loading: true }
    case categoryEntityType.CATEGORY_ENTITY_RESPONSE:
      return { ...state, categoryEntityData: action.data, loading: false };
    case categoryEntityType.CATEGORY_ENTITY_CLEAR:
      return { ...state, categoryEntityData: null }
    default:
      return state
  }
}

export const BusinessDoc4InitialState = {
  data: null,
};
export const BusinessDoc4Key = "BusinessDoc4Store"

export const BusinessDoc4Reducer = (state = BusinessDoc4InitialState, action) => {
  switch (action.type) {
    case BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD:
      return { ...state, loading: true }
    case BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD_RESPONSE:
      return { ...state, BusinessUpload4Res: action.data, loading: false };
    case BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD_CLEAR:
      return { ...state, BusinessUpload4Res: null }
    default:
      return state
  }
}

export const AccountUploadInitialState = {
  data: null,
}
export const AccountUploaStoredKey = "AccountUploadStore"

export const AccountUploadReducer = (state = AccountUploadInitialState, action) => {
  switch (action.type) {
    case AccountUploadDocType.ACCOUNT_DOC_UPLOAD:
      return { ...state, loading: true }
    case AccountUploadDocType.ACCOUNT_DOC_UPLOAD_RESPONSE:
      return { ...state, AccountUploadRes: action.data, loading: false };
    case AccountUploadDocType.ACCOUNT_DOC_UPLOAD_CLEAR:
      return { ...state, AccountUploadRes: null }
    default:
      return state
  }
}

export const HufDeedUploadInitialState = {
  data: null,
}
export const HufDeedUploaStoredKey = "HufDeedUploadStore"

export const HufDeedUploadReducer = (state = HufDeedUploadInitialState, action) => {
  switch (action.type) {
    case HufDeedUploadDocType.HUFDEED_DOC_UPLOAD:
      return { ...state, loading: true }
    case HufDeedUploadDocType.HUFDEED_DOC_UPLOAD_RESPONSE:
      return { ...state, HufDeedUploadRes: action.data, loading: false };
    case HufDeedUploadDocType.HUFDEED_DOC_UPLOAD_CLEAR:
      return { ...state, HufDeedUploadRes: null }
    default:
      return state
  }
}

export const SOlePropUploadInitialState = {
  data: null,
}
export const SolePropUploaStoredKey = "SolePropUploadStore"

export const SolePropUploadReducer = (state = SOlePropUploadInitialState, action) => {
  switch (action.type) {
    case SolePropUploadDocType.SOLEPROP_DOC_UPLOAD:
      return { ...state, loading: true }
    case SolePropUploadDocType.SOLEPROP_DOC_UPLOAD_RESPONSE:
      return { ...state, SolePropUploadRes: action.data, loading: false };
    case SolePropUploadDocType.SOLEPROP_DOC_UPLOAD_CLEAR:
      return { ...state, SolePropUploadRes: null }
    default:
      return state
  }
}

export const DashCommonUploadInitialState = {
  data: null,
}
export const DashCommonUploaStoredKey = "DashCommonUploadStore"

export const DashCommonUploadReducer = (state = DashCommonUploadInitialState, action) => {
  switch (action.type) {
    case DashComUploadDocType.DASHCOMMON_DOC_UPLOAD:
      return { ...state, loading: true }
    case DashComUploadDocType.DASHCOMMON_DOC_UPLOAD_RESPONSE:
      return { ...state, DashCommonUploadRes: action.data, loading: false };
    case DashComUploadDocType.DASHCOMMON_DOC_UPLOAD_CLEAR:
      return { ...state, DashCommonUploadRes: null }
    default:
      return state
  }
}

export const DashReUploadInitialState = {
  data: null,
}
export const DashReUploaStoredKey = "DashReUploadStore"

export const DashReUploadReducer = (state = DashReUploadInitialState, action) => {
  switch (action.type) {
    case DashReUploadDocType.DASHRE_DOC_UPLOAD:
      return { ...state, loading: true }
    case DashReUploadDocType.DASHRE_DOC_UPLOAD_RESPONSE:
      return { ...state, DashReUploadRes: action.data, loading: false };
    case DashReUploadDocType.DASHRE_DOC_UPLOAD_CLEAR:
      return { ...state, DashReUploadRes: null }
    default:
      return state
  }
}

export const DashVideoApproveInitialState = {
  data: null,
}
export const DashVideoApproveStoredKey = "DashVideoApproveStore"

export const DashVideoApproveReducer = (state = DashVideoApproveInitialState, action) => {
  switch (action.type) {
    case DashVideoApproveType.DASHRE_VIDEO_APPROVE:
      return { ...state, loading: true }
    case DashVideoApproveType.DASHRE_VIDEO_APPROVE_RESPONSE:
      return { ...state, DashVideoApproveRes: action.data, loading: false };
    case DashVideoApproveType.DASHRE_VIDEO_APPROVE_CLEAR:
      return { ...state, DashVideoApproveRes: null }
    default:
      return state
  }
}

export const addKycFvInitialState = {
  data: null,
}
export const addKycFvStoredKey = "addKycFvStore"

export const addKycFvReducer = (state = addKycFvInitialState, action) => {
  switch (action.type) {
    case addKycFvType.ADD_KYC_FV:
      return { ...state, loading: true }
    case addKycFvType.ADD_KYC_FV_RESPONSE:
      return { ...state, addKycFvRes: action.data, loading: false };
    case addKycFvType.ADD_KYC_FV_CLEAR:
      return { ...state, addKycFvRes: null }
    default:
      return state
  }
}

export const updateKycFvInitialState = {
  data: null,
}
export const updateKycFvStoredKey = "updateKycFvStore"

export const updateKycFvReducer = (state = updateKycFvInitialState, action) => {
  switch (action.type) {
    case UpdateKycFvType.UPDATE_KYC_FV:
      return { ...state, loading: true }
    case UpdateKycFvType.UPDATE_KYC_FV_RESPONSE:
      return { ...state, updateKycFvRes: action.data, loading: false };
    case UpdateKycFvType.UPDATE_KYC_FV_CLEAR:
      return { ...state, updateKycFvRes: null }
    default:
      return state
  }
}

export const kycStatusInitialState = {
  data: null,
}
export const kycStatusStoredKey = "kycStatusStore"

export const kycStatusReducer = (state = kycStatusInitialState, action) => {
  switch (action.type) {
    case kycStatusType.KYC_STATUS_GET:
      return { ...state, loading: true }
    case kycStatusType.KYC_STATUS_GET_RESPONSE:
      return { ...state, kycStatusRes: action.data, loading: false };
    case kycStatusType.KYC_STATUS_GET_CLEAR:
      return { ...state, kycStatusRes: null }
    default:
      return state
  }
}

export const kycStatusARInitialState = {
  data: null,
}
export const kycStatusARStoredKey = "kycStatusARStore"

export const kycStatusARReducer = (state = kycStatusARInitialState, action) => {
  switch (action.type) {
    case KycStatusChangeActionsTypes.KYC_STATUS_CHANGE:
      return { ...state, loading: true }
    case KycStatusChangeActionsTypes.SAVE_KYC_STATUS_CHANGE_RESPONSE:
      return { ...state, KycStatusChangeResponce: action.data, loading: false };
    case KycStatusChangeActionsTypes.CLEAR_KYC_STATUS_CHANGE:
      return { ...state, KycStatusChangeResponce: null }
    default:
      return state
  }
}

export const KycUpdateQueueInitialState = {
  data: null,
}
export const KycUpdateQueueStoredKey = "KycUpdateQueueStore"

export const KycUpdateQueueReducer = (state = KycUpdateQueueInitialState, action) => {
  switch (action.type) {
    case KycUpdateQueueType.KYC_UPDATE_QUEUE:
      return { ...state, loading: true }
    case KycUpdateQueueType.KYC_UPDATE_QUEUE_RESPONSE:
      return { ...state, KycUpdateQueueRes: action.data, loading: false };
    case KycUpdateQueueType.KYC_UPDATE_QUEUE_CLEAR:
      return { ...state, KycUpdateQueueRes: null }
    default:
      return state
  }
}