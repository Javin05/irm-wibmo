import { 
    WhiteListTypes ,
    WhiteListUploadTypes,
    WhiteListDeleteClientTypes
} from '../actions'

const WhiteListInitials = {
    whitelistCount: 0,
    whitelistData: null,
    whitelistTypeData: null,
    createWhitelistTypeResponse : null,
    updateWhitelistTypeResponse : null,
    deleteWhitelistTypeResponse : null,
    error: null
}

export const WhiteListStore = 'WhiteListStore'

export const whitelistReducer = (state = WhiteListInitials, action) => {

    switch (action.type) {
        case WhiteListTypes.FETCH_WHITELIST_INIT:
        case WhiteListTypes.FETCH_WHITELISTTYPE_INIT:
        case WhiteListTypes.CREATE_WHITELISTTYPE_INIT:
        case WhiteListTypes.UPDATE_WHITELISTTYPE_INIT:
        case WhiteListTypes.DELETE_WHITELISTTYPE_INIT:
            return { 
                ...state, 
                loading: true 
            }

        case WhiteListTypes.FETCH_WHITELIST_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                whitelistData: action.data?.data
            }

        case WhiteListTypes.FETCH_WHITELISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                whitelistTypeData: action.data?.data,
                whitelistCount: action.data?.count,
                createWhitelistTypeResponse:null,
                updateWhitelistTypeResponse:null,
                deleteWhitelistTypeResponse:null
            }

        case WhiteListTypes.CREATE_WHITELISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                createWhitelistTypeResponse: action.data?.status,
                updateWhitelistTypeResponse:null,
                deleteWhitelistTypeResponse:null
            }

        case WhiteListTypes.UPDATE_WHITELISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                updateWhitelistTypeResponse: action.data?.status,
                createWhitelistTypeResponse:null,
                deleteWhitelistTypeResponse:null
            }

        case WhiteListTypes.DELETE_WHITELISTTYPE_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                deleteWhitelistTypeResponse: action.data?.status,
                createWhitelistTypeResponse:null,
                updateWhitelistTypeResponse:null
            }

        case WhiteListTypes.FETCH_WHITELIST_ERROR:
        case WhiteListTypes.FETCH_WHITELISTTYPE_ERROR:
        case WhiteListTypes.CREATE_WHITELISTTYPE_ERROR:
        case WhiteListTypes.UPDATE_WHITELISTTYPE_ERROR:
        case WhiteListTypes.DELETE_WHITELISTTYPE_ERROR:
            return { 
                ...state, 
                error: action 
            }

        default:
            return state
    }
}

export const WhiteListInitialState = {
    list: null
  }

export const WhiteListStoreKeys = 'whiteListStore'
  
export const WhiteListReducers = (state = WhiteListInitialState, action) => {
  switch (action.type) {
    case WhiteListUploadTypes.POST_WHITELISTUPLOAD_LIST:
      return { ...state, loading: true }
    case WhiteListUploadTypes.SAVE_WHITELISTUPLOAD_LIST_RESPONSE:
      return { ...state, WhiteList: action.data, loading: false }
    case WhiteListUploadTypes.CLEAR_WHITELISTUPLOAD_LIST:
      return { ...state, WhiteList: null }
    default:
      return state
  }
}

export const WhiteListDeleteClientInitialState = {
    list: null
  }
  
  export const WhiteListDeleteClientStoreKeys = 'WhiteListDeleteClientStore'
  
  export const WhiteListDeleteClientReducers = (state = WhiteListDeleteClientInitialState, action) => {
    switch (action.type) {
      case WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT:
        return { ...state, loading: true }
      case WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT_SUCCESS:
        return { ...state, WhiteDeletListClient: action, loading: false }
      case WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT_CLEAR:
        return { ...state, WhiteDeletListClient: null }
      default:
        return state
    }
  }