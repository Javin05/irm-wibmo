import { TransactionLaundringTypes} from '../actions'

export const TransactionLaundringlistInitialState = {
  list: null
}

export const TransactionLaundringlistStoreKey = 'TransactionLaundringlistStore'

export const TransactionLaundringlistReducers = (state = TransactionLaundringlistInitialState, action) => {
  switch (action.type) {
    case TransactionLaundringTypes.GET_TRANSACTIONLAUNDRING_LIST:
      return { ...state, loading: true }
    case TransactionLaundringTypes.SAVE_TRANSACTIONLAUNDRING_LIST_RESPONSE:
      return { ...state, TransactionLaundringData: action.data, loading: false }
    case TransactionLaundringTypes.CLEAR_TRANSACTIONLAUNDRING_LIST:
      return { ...state, TransactionLaundringData: null }
    default:
      return state
  }
}