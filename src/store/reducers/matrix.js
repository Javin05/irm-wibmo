import {
   matrixActionTypes
} from '../actions'

export const MatrixInitialState = {
  list: null
}

export const MatrixStoreKey = 'MatrixStore'

export const MatrixReducer = (state = MatrixInitialState, action) => {
  switch (action.type) {
    case matrixActionTypes.GET_MATRIX_DETAILS:
      return { ...state, loadingEA: true }
    case matrixActionTypes.MATRIX_DETAILS_RESPONSE:
      return { ...state, matrixDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case matrixActionTypes.CLEAR_MATRIX_DETAILS:
      return { ...state, matrixDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}
