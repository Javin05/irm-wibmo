import { 
  playStoreReportSettingActionsTypes, playStoreReportSettingAddTypes, playStoreReportSettingGetId, updateplayStoreReportSettingActionsTypes, playStoreReportSettingDelete
 } from '../actions'
  
export const playStoreReportSettingslistInitialState = {
  list: null
}

export const playStoreReportSettingslistStoreKey = 'playStoreReportSettingslistStore'

export const playStoreReportSettingslistReducer = (state = playStoreReportSettingslistInitialState, action) => {
  switch (action.type) {
    case playStoreReportSettingActionsTypes.GET_PLAYSTOREREPROT_SEETING_LIST:
      return { ...state, loading: true }
    case playStoreReportSettingActionsTypes.SAVE_PLAYSTOREREPROT_SEETING_LIST_RESPONSE:
      return { ...state, playStoreReportSettingslists: action.data, loading: false }
    case playStoreReportSettingActionsTypes.CLEAR_PLAYSTOREREPROT_SEETING_LIST:
      return { ...state, playStoreReportSettingslists: null }
    default:
      return state
  }
} 

export const playStoreReportSettingsAddInitialState = {
  data: null
} 

export const playStoreReportSettingsAddStoreKey = 'playStoreReportSettingsAddStore'

export const playStoreReportSettingsAddReducer = (state = playStoreReportSettingsAddInitialState, action) => {
  switch (action.type) {
    case playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST:
      return { ...state, loading: true }
    case playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST_RESPONSE:
      return { ...state, playStoreReportSettingsAdd: action.data, playStoreReportSettingStatus:  action.data.status, playStoreReportSettingMessage:  action.data.message, loading: false }
    case playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST_CLEAR:
      return { ...state, playStoreReportSettingsAdd: null }
    default:
      return state
  }
}

export default playStoreReportSettingsAddReducer


export const editplayStoreReportSettingsInitialState = {
  list: null
}

export const editplayStoreReportSettingsStoreKey = 'editplayStoreReportSettingsStore'

export const editplayStoreReportSettingsReducer = (state = editplayStoreReportSettingsInitialState, action) => {
  switch (action.type) {
    case playStoreReportSettingGetId.GET_PLAYSTOREREPROT_SEETING_DETAILS:
      return { ...state, loading: true }
    case playStoreReportSettingGetId.PLAYSTOREREPROT_SEETING_DETAILS_RESPONSE:
      return { ...state, playStoreReportSettingsIdDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case playStoreReportSettingGetId.CLEAR_PLAYSTOREREPROT_SEETING_DETAILS:
      return { ...state, playStoreReportSettingsIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const updateplayStoreReportSettingInitialState = {
  data: null
}

export const updateplayStoreReportSettingStoreKey = 'updateplayStoreReportSettingStore'

export const updateplayStoreReportSettingReducer = (state = updateplayStoreReportSettingInitialState, action) => {
  switch (action.type) {
    case updateplayStoreReportSettingActionsTypes.UPDATE_PLAYSTOREREPROT_SEETING:
      return { ...state, loading: true }
    case updateplayStoreReportSettingActionsTypes.SAVE_UPDATE_PLAYSTOREREPROT_SEETING_RESPONSE:
      return {
        ...state,
        updateplayStoreReportSettingResponce: action.data,
        loading: false
      }
    case updateplayStoreReportSettingActionsTypes.CLEAR_UPDATE_PLAYSTOREREPROT_SEETING:
      return { ...state, updateplayStoreReportSettingResponce: null }
    default:
      return state
  }
}

export const DeleteplayStoreReportSettingInitialState = {
  data: null
}

export const DeleteplayStoreReportSettingStoreKey = 'DeleteplayStoreReportSettingStore'

export const DeleteplayStoreReportSettingReducer = (state = DeleteplayStoreReportSettingInitialState, action) => {
  switch (action.type) {
    case playStoreReportSettingDelete.PLAYSTOREREPROT_SEETING_DELETE:
      return { ...state, loading: true }
    case playStoreReportSettingDelete.PLAYSTOREREPROT_SEETING_DELETE_RESPONSE:
      return {
        ...state,
        DeleteplayStoreReportSetting: action.data,
        loading: false
      }
    case playStoreReportSettingDelete.CLEAR_PLAYSTOREREPROT_SEETING_DELETE:
      return { ...state, DeleteplayStoreReportSetting: null }
    default:
      return state
  }
}