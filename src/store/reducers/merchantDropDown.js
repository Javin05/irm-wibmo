import { businessOwnerActionsTypes,  ownerActions, transactionAmountActionsTypes, MonthlyActionsTypes, MccActionsTypes} from '../actions'

export const ownerInitialState = {
  list: null
}

export const ownerlistStoreKey = 'ownerlistStore'

export const ownerlistReducer = (state = ownerInitialState, action) => {
  switch (action.type) {
    case businessOwnerActionsTypes.GET_OWNER_LIST:
      return { ...state, loading: true }
    case businessOwnerActionsTypes.SAVE_OWNER_LIST_RESPONSE:
      return { 
        ...state, 
        ownerlists: action.data && action.data.data, 
        loading: false }
    case businessOwnerActionsTypes.CLEAR_OWNER_LIST:
      return { ...state, ownerlists: null }
    default:
      return state
  }
}

export const transactionInitialState = {
  list: null
}

export const transactionlistStoreKey = 'transactionlistStore'

export const transactionlistReducer = (state = transactionInitialState, action) => {
  switch (action.type) {
    case transactionAmountActionsTypes.GET_TRANSACTION_AMOUNT_LIST:
      return { ...state, loading: true }
    case transactionAmountActionsTypes.SAVE_TRANSACTION_LIST_RESPONSE:
      return { 
        ...state, 
        transactionList: action.data && action.data.data, 
        loading: false }
    case transactionAmountActionsTypes.CLEAR_TRANSACTION_LIST:
      return { ...state, transactionList: null }
    default:
      return state
  }
}

export const MonthlyInitialState = {
  list: null
}

export const MonthlylistStoreKey = 'MonthlylistStore'

export const MonthlylistReducer = (state = MonthlyInitialState, action) => {
  switch (action.type) {
    case MonthlyActionsTypes.GET_MONTH_LIST:
      return { ...state, loading: true }
    case MonthlyActionsTypes.SAVE_MONTH_LIST_RESPONSE:
      return { 
        ...state, 
        MonthlyList: action.data && action.data.data, 
        loading: false }
    case MonthlyActionsTypes.CLEAR_MONTH_LIST:
      return { ...state, MonthlyList: null }
    default:
      return state
  }
}

export const MccInitialState = {
  list: null
}

export const McclistStoreKey = 'McclistStore'

export const McclistReducer = (state = MccInitialState, action) => {
  switch (action.type) {
    case MccActionsTypes.GET_MCC_LIST:
      return { ...state, loading: true }
    case MccActionsTypes.SAVE_MCC_LIST_RESPONSE:
      return { 
        ...state, 
        MccList: action.data && action.data.data, 
        loading: false }
    case MccActionsTypes.CLEAR_MCC_LIST:
      return { ...state, MccList: null }
    default:
      return state
  }
}