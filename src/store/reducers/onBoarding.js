import {
    onBoardingActionsTypes,
    OnBoardingAddTypes,
    OnBoardingUpdateTypes,
    OnBoardingSummaryTypes
} from '../actions'

export const onBoardingSummaryInitialState = {
    list: null
}
export const onBoardingSummaryStoreKey = 'onBoardingSummaryStore'
export const onBoardingSummaryReducer = (state = onBoardingSummaryInitialState, action) => {
    switch (action.type) {
        case OnBoardingSummaryTypes.GET_ON_BOARDING_SUMMARY:
            return { ...state, loading: true }
        case OnBoardingSummaryTypes.SAVE_ON_BOARDING_SUMMARY_RESPONSE:
            return { ...state, onBoardingSummary: action.data, loading: false }
        case OnBoardingSummaryTypes.CLEAR_ON_BOARDING_SUMMARY:
            return { ...state, onBoardingSummary: null }
        default:
            return state
    }
}

export const onBoardingListInitialState = {
    list: null
}

export const onBoardinglistStoreKey = 'onBoardinglistStore'

export const onBoardinglistReducer = (state = onBoardingListInitialState, action) => {
    switch (action.type) {
        case onBoardingActionsTypes.GET_ON_BOARDING_LIST:
            return { ...state, loading: true }
        case onBoardingActionsTypes.SAVE_ON_BOARDING_LIST_RESPONSE:
            return { ...state, onBoardinglists: action.data, loading: false }
        case onBoardingActionsTypes.CLEAR_ON_BOARDING_LIST:
            return { ...state, onBoardinglists: null }
        default:
            return state
    }
}

export const OnBoardingAddInitialState = {
    data: null,
}

export const OnBoardingAddStoreKey = "OnBoardingAddStore"

export const OnBoardingAddReducer = (state = OnBoardingAddInitialState, action) => {
    switch (action.type) {
        case OnBoardingAddTypes.ON_BOARDING_POST:
            return { ...state, loading: true }
        case OnBoardingAddTypes.ON_BOARDING_POST_RESPONSE:
            return { ...state, OnBoardingAddResponse: action.data, loading: false }
        case OnBoardingAddTypes.ON_BOARDING_POST_CLEAR:
            return { ...state, OnBoardingAddResponse: null }
        default:
            return state
    }
}

export const OnBoardingUpdateInitialState = {
    data: null,
}

export const OnBoardingUpdateStoreKey = "OnBoardingUpdateStore"

export const OnBoardingUpdateReducer = (state = OnBoardingUpdateInitialState, action) => {
    switch (action.type) {
        case OnBoardingUpdateTypes.ON_BOARDING_UPDATE:
            return { ...state, loading: true }
        case OnBoardingUpdateTypes.ON_BOARDING_UPDATE_RESPONSE:
            return { ...state, OnBoardingUpdateResponse: action.data, loading: false }
        case OnBoardingUpdateTypes.ON_BOARDING_UPDATE_CLEAR:
            return { ...state, OnBoardingUpdateResponse: null }
        default:
            return state
    }
}