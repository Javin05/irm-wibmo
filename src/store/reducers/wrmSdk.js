import { 
  SdkExportListActionsTypes,
  postSdkWebAnalysisTypes,
  SdkManualWebAnalysisTypes,
  getSdkWebAnalysisTypes,
  SdkBlockListEmailTypes,
  SdkBWListActionsTypes,
  PostSdkCategoryTypes,
  PostSdkCategory1Types,
  WrmListStatusChangeActionsTypes,
  WrmListUpdateQueueActionsTypes,
} from '../actions'

export const SdkexportlistInitialState = {
  list: null
}

export const SdkexportlistStoreKey = 'SdkexportlistStore'

export const SdkExportlistReducer = (state = SdkexportlistInitialState, action) => {
  switch (action.type) {
    case SdkExportListActionsTypes.GET_SDK_EXPORT_LIST:
      return { ...state, loading: true }
    case SdkExportListActionsTypes.SAVE_SDK_EXPORT_LIST_RESPONSE:
      return { ...state, SdkExportLists: action, loading: false }
    case SdkExportListActionsTypes.CLEAR_SDK_EXPORT_LIST:
      return { ...state, SdkExportLists: null }
    default:
      return state
  }
}

export const PostSdkWebAnalysisInitialState = {
list: null
}

export const PostSdkWebAnalysisStoreKey = 'PostSdkWebAnalysisStore'

export const PostCsvSdkWebAnalysisReducer = (state = PostSdkWebAnalysisInitialState, action) => {
switch (action.type) {
  case postSdkWebAnalysisTypes.POST_SDK_WEBANALYSIS_LIST:
    return { ...state, loading: true }
  case postSdkWebAnalysisTypes.SAVE_SDK_WEBANALYSIS_LIST_RESPONSE:
    return { ...state, postSdkCSVWebAnalysis: action.data, loading: false }
  case postSdkWebAnalysisTypes.CLEAR_SDK_WEBANALYSIS_LIST:
    return { ...state, postSdkCSVWebAnalysis: null }
  default:
    return state
}
}

export const SdkManualWebAnalysisInitialState = {
list: null
}

export const SdkManualWebAnalysisStoreKey = 'SdkManualWebAnalysisStore'

export const SdkManualWebAnalysisReducer = (state = SdkManualWebAnalysisInitialState, action) => {
switch (action.type) {
  case SdkManualWebAnalysisTypes.POST_SDK_MANUAL_WEBANALYSIS_LIST:
    return { ...state, loading: true }
  case SdkManualWebAnalysisTypes.SAVE_SDK_MANUAL_WEBANALYSIS_LIST_RESPONSE:
    return { ...state, postManualSdkWebAnalysis: action.data, loading: false }
  case SdkManualWebAnalysisTypes.CLEAR_SDK_MANUAL_WEBANALYSIS_LIST:
    return { ...state, postManualSdkWebAnalysis: null }
  default:
    return state
}
}

export const getSdkWebAnalysisInitialState = {
list: null
}

export const getSdkWebAnalysisStoreKey = 'getSdkWebAnalysisStore'

export const getSdkWebAnalysisReducer = (state = getSdkWebAnalysisInitialState, action) => {
switch (action.type) {
  case getSdkWebAnalysisTypes.GET_SDK_WEBANALYSIS_LIST:
    return { ...state, loading: true }
  case getSdkWebAnalysisTypes.SAVE_GET_SDK_WEBANALYSIS_LIST_RESPONSE:
    return { ...state, getWebAnalysis: action.data, loading: false }
  case getSdkWebAnalysisTypes.CLEAR_GET_SDK_WEBANALYSIS_LIST:
    return { ...state, getWebAnalysis: null }
  default:
    return state
}
}

export const SdkBlockListEmaillistInitialState = {
  list: null
}

export const SdkBlockListEmaillistStoreKeys = 'SdkBlockListTypeStore'

export const SdkBlockListEmaillistReducers = (state = SdkBlockListEmaillistInitialState, action) => {
  switch (action.type) {
    case SdkBlockListEmailTypes.GET_SDK_BLOCKLISTEMAIL_LIST:
      return { ...state, loading: true }
    case SdkBlockListEmailTypes.SAVE_SDK_BLOCKLISTEMAIL_LIST_RESPONSE:
      return { ...state, BlockListType: action, loading: false }
    case SdkBlockListEmailTypes.CLEAR_SDK_BLOCKLISTEMAIL_LIST:
      return { ...state, BlockListType: null }
    default:
      return state
  }
}

export const SdkBWlistInitialState = {
  list: null
}

export const SdkBWlistStoreKey = 'SdkBWlistStore'

export const SdkBWlistReducer = (state = SdkBWlistInitialState, action) => {
  switch (action.type) {
    case SdkBWListActionsTypes.GET_SDK_BW_LIST:
      return { ...state, loading: true }
    case SdkBWListActionsTypes.SAVE_SDK_BW_LIST_RESPONSE:
      return { ...state, BWLists: action, loading: false }
    case SdkBWListActionsTypes.CLEAR_SDK_BW_LIST:
      return { ...state, BWLists: null }
    default:
      return state
  }
}
export const PostSdkCategoryInitialState = {
  list: null
}

export const PostSdkCategoryStoreKey = 'PostSdkCategoryStore'

export const PostSdkCategoryReducer = (state = PostSdkCategoryInitialState, action) => {
  switch (action.type) {
    case PostSdkCategoryTypes.POST_SDK_CATEGORY:
      return { ...state, loading: true }
    case PostSdkCategoryTypes.SAVE_SDK_CATEGORY_RESPONSE:
      return { ...state, PostSdkCategory: action, loading: false }
    case PostSdkCategoryTypes.CLEAR_SDK_CATEGORY:
      return { ...state, PostSdkCategory: null }
    default:
      return state
  }
}

export const PostSdkCategory1InitialState = {
  list: null
}

export const PostSdkCategory1StoreKey = 'PostSdkCategoryStore1'

export const PostSdkCategory1Reducer = (state = PostSdkCategory1InitialState, action) => {
  switch (action.type) {
    case PostSdkCategory1Types.POST_SDK_CATEGORY1:
      return { ...state, loading: true }
    case PostSdkCategory1Types.SAVE_SDK_CATEGORY1_RESPONSE:
      return { ...state, postSdkCategory1Res: action, loading: false }
    case PostSdkCategory1Types.CLEAR_SDK_CATEGORY1:
      return { ...state, postSdkCategory1Res: null }
    default:
      return state
  }
}

export const WrmListStatusARInitialState = {
  data: null,
}
export const WrmListStatusARStoredKey = "WrmListStatusARStore"

export const WrmListStatusARReducer = (state = WrmListStatusARInitialState, action) => {
  switch (action.type) {
    case WrmListStatusChangeActionsTypes.WRM_LIST_STATUS_CHANGE:
      return { ...state, loading: true }
    case WrmListStatusChangeActionsTypes.SAVE_WRM_LIST_STATUS_CHANGE_RESPONSE:
      return { ...state, WrmListStatusChangeResponce: action.data, loading: false };
    case WrmListStatusChangeActionsTypes.CLEAR_WRM_LIST_STATUS_CHANGE:
      return { ...state, WrmListStatusChangeResponce: null }
    default:
      return state
  }
}

export const WrmListUpdateQueueInitialState = {
  data: null,
}
export const WrmListUpdateQueueStoredKey = "WrmListUpdateQueueStore"

export const WrmListUpdateQueueReducer = (state = WrmListUpdateQueueInitialState, action) => {
  switch (action.type) {
    case WrmListUpdateQueueActionsTypes.WRM_LIST_UPDATE_QUEUE:
      return { ...state, loading: true }
    case WrmListUpdateQueueActionsTypes.SAVE_WRM_LIST_UPDATE_QUEUE_RESPONSE:
      return { ...state, WrmListUpdateQueueResponce: action.data, loading: false };
    case WrmListUpdateQueueActionsTypes.CLEAR_WRM_LIST_UPDATE_QUEUE:
      return { ...state, WrmListUpdateQueueResponce: null }
    default:
      return state
  }
}