import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { matrixActions, matrixActionTypes   } from '../actions'
import serviceList from '../../services/serviceList'

function * MatrixMerchant (actions) {

  const { id } = actions
  const endPointUrl = `${serviceList.Matrix}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(matrixActions.saveMatrixDetailsResponse(json.data))
  } else {
    yield put(matrixActions.saveMatrixDetailsResponse([]))
  }
}

export function * fetchMatrixtWatcher () {
  yield all([
    yield takeLatest(matrixActionTypes.GET_MATRIX_DETAILS, MatrixMerchant)
  ])
}