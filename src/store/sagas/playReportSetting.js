import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  playStoreReportSettingActionsTypes,playStoreReportSettingActions, playStoreReportSettingAddAction, playStoreReportSettingAddTypes, playStoreReportSettingGetId, playStoreReportSettingGetIdActions, updateplayStoreReportSettingActionsTypes, updateplayStoreReportSettingActions, playStoreReportSettingDeleteActions, playStoreReportSettingDelete
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchplayStoreReportSettinglist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.playStoreReport, { params })
  if (json.status === 200) {
    yield put(playStoreReportSettingActions.saveplayStoreReportSettinglistResponse(json.data))
  } else {
    yield put(playStoreReportSettingActions.saveplayStoreReportSettinglistResponse([]))
  }
}

function* fetchplayStoreReportSettingLogin(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.playStoreReportCreate, payload)
    if (data && data.data) {
      yield put(playStoreReportSettingAddAction.saveplayStoreReportSettingResponse(data.data))
    }
  } catch (error) {
    yield put(playStoreReportSettingAddAction.saveplayStoreReportSettingResponse(error))
  }
}

function* getEditplayStoreReportSetting(actions) {
  const { id } = actions
  const idValue = id && id.id
  const params = {
    report_type: "Playstore Report",
  }
  const endPointUrl = `${serviceList.playStoreReportAdd}/${idValue}`
  const json = yield axiosInstance.get(endPointUrl, { params })
  if (json.status === 200) {
    yield put(playStoreReportSettingGetIdActions.saveplayStoreReportSettingIdDetailsResponse(json.data))

  } else {  
    yield put(playStoreReportSettingGetIdActions.saveplayStoreReportSettingIdDetailsResponse([]))
  }
}

function * fetchupdate (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.playStoreReportAdd}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateplayStoreReportSettingActions.saveupdateplayStoreReportSettingResponse(json.data))
  } else {
    yield put(updateplayStoreReportSettingActions.saveupdateplayStoreReportSettingResponse([]))
  }
}

function * fetchDelete (actions) {
  const { payload } = actions
  const idValue = payload && payload.id
  const params = {
    report_type: "Playstore Report",
  }
  const endPointUrl = `${serviceList.playStoreReportAdd}/${idValue}`
  const json = yield axiosInstance.delete(endPointUrl, params)
  if (json.status === 200) {
    yield put(playStoreReportSettingDeleteActions.saveplayStoreReportSettingDeleteResponse(json.data))
  } else {
    yield put(playStoreReportSettingDeleteActions.saveplayStoreReportSettingDeleteResponse([]))
  }
}

export function* fetchplayStoreReportSettinglistWatcher() {
  yield all([
    yield takeLatest(playStoreReportSettingActionsTypes.GET_PLAYSTOREREPROT_SEETING_LIST, fetchplayStoreReportSettinglist),
    yield all([yield takeLatest(playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST, fetchplayStoreReportSettingLogin)]),
    yield takeLatest(playStoreReportSettingGetId.GET_PLAYSTOREREPROT_SEETING_DETAILS, getEditplayStoreReportSetting),
    yield takeLatest(updateplayStoreReportSettingActionsTypes.UPDATE_PLAYSTOREREPROT_SEETING, fetchupdate),
    yield takeLatest(playStoreReportSettingDelete.PLAYSTOREREPROT_SEETING_DELETE, fetchDelete)
  ])
}