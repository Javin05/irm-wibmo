import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  WrmOperationManagementTypes,
  WrmOperationManagementActions,
  WRMOperatorsListActions,
  WRMOperatorsListTypes,
  WrmOperatorActions,
  WrmOperatorActionTypes,
  WrmOperationManagementDetailTypes,
  WrmOperationManagementDetailActions,
  WrmOperationManagementBackendAPIStatusActions,
  WrmOperationManagementBackendAPIStatusTypes,
  WrmReportEditAction,
  WrmReportEditTypes,
  WrmOperationBackendApiAllScheduleTypes,
  WrmOperationBackendApiAllScheduleAction
  
} from '../actions/wrmOperationManagement'
import serviceList from '../../services/serviceList'
import { API_MESSAGES } from '../../utils/constants'


function* fetchWrmOperationManagement(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.wrmOperationManagement, { params })
  if (json.status === 200) {
    yield put(WrmOperationManagementActions.saveWrmOperationManagemntResponse(json.data))
  } else {
    yield put(WrmOperationManagementActions.saveWrmOperationManagemntResponse([]))
  }
}
function* fetchWrmOperatorsList(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.wrmOperatortList, { params })
  if (json.status === 200) {
    yield put(WRMOperatorsListActions.saveWRMOperatorsListResponse(json.data))
  } else {
    yield put(WRMOperatorsListActions.saveWRMOperatorsListResponse([]))
  }
}

function* actionWrmOperation(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.put(serviceList.wrmOperatorAction, params)
    if (json.status === 200) {
      yield put(WrmOperatorActions.saveWrmOperatorActionResponse(json.data))
    } else {
      yield put(WrmOperatorActions.saveWrmOperatorActionResponse([]))
    }
    }
catch (error) {
  const data = {status: 'error', message: API_MESSAGES.SOMETHING_WRONG}
  yield put({type: WrmOperatorActionTypes.RESPONSE, data})
}
}
function* fetchWrmOperationManagementDetail(params) {
  let {id} = params
  const json = yield axiosInstance.get(`${serviceList.wrmOperationManagementDetails}/${id.id}`)
  if (json.status === 200) {
    yield put(WrmOperationManagementDetailActions.saveWrmOperationDetailResponse(json.data))
  } else {
    yield put(WrmOperationManagementDetailActions.saveWrmOperationDetailResponse([]))
  }
}

function* fetchWrmOperationManagementBackendAPIStatus(actions) {
  const { id } = actions
  const json = yield axiosInstance.get(`${serviceList.wrmOperationBackendAPIStatus}/${id}`, )
  if (json.status === 200) {
    yield put(WrmOperationManagementBackendAPIStatusActions.saveWrmOperationManagemntBackendAPIStatusResponse(json.data))
  } else {
    yield put(WrmOperationManagementBackendAPIStatusActions.saveWrmOperationManagemntBackendAPIStatusResponse([]))
  }
}
function* fetchWrmEditReport(data) {
 let {payload:{id,params}} = data
  const json = yield axiosInstance.put(`${serviceList.wrmReportEdit}/${id}`, { ...params })
  if (json.status === 200) {
    yield put(WrmReportEditAction.saveEditWrmReportResponse(json.data))
  } else {
    yield put(WrmReportEditAction.saveEditWrmReportResponse([]))
  }
}
function* actionWrmBackendAPIRescheduleAll(data) {
  console.log(data)
  let {id,name} = data
  let queries = name? `api_names=${name}`:""
  const json = yield axiosInstance.get(`${serviceList.WrmAllBackendApiReschedule}/${id}?${queries}`)
  if (json.status === 200) {
    yield put(WrmOperationBackendApiAllScheduleAction.saveWrmApiScheduleAllResponse(json.data))
  } else {
    yield put(WrmOperationBackendApiAllScheduleAction.saveWrmApiScheduleAllResponse([]))
  }
}

export function* fetchWrmOperationManagementWatcher() {
  yield all([yield takeLatest(WrmOperationManagementTypes.GET_WRMOPERATIONMANAGEMENT_LIST, fetchWrmOperationManagement)])
  yield all([yield takeLatest(WRMOperatorsListTypes.GET_WRMOPERATOR_LIST, fetchWrmOperatorsList)])
  yield all([yield takeLatest(WrmOperatorActionTypes.WRM_OPERATION_ACTION_INIT, actionWrmOperation)])
  yield all([yield takeLatest(WrmOperationManagementDetailTypes.GET_WRMOPERATIONMANAGEMENT_DETAIL, fetchWrmOperationManagementDetail)])
  yield all([yield takeLatest(WrmOperationManagementBackendAPIStatusTypes.GET_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS, fetchWrmOperationManagementBackendAPIStatus)])
  yield all([yield takeLatest(WrmReportEditTypes.REQUEST, fetchWrmEditReport)])
  yield all([yield takeLatest(WrmOperationBackendApiAllScheduleTypes.REQUEST, actionWrmBackendAPIRescheduleAll)])

}