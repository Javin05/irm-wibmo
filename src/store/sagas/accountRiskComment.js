import { put, takeLatest, all } from "redux-saga/effects";
import axiosInstance from "../../services";
import { API_MESSAGES } from '../../utils/constants'
import { 
    AccountRiskCommentTypes,
    AccountRiskCommentActions,
    updateAccountRiskCommentActionsTypes,
    updateAccountRiskCommentActions,
    AccountRiskCommentdeleteActionsTypes,
    AccountRiskCommentdeleteActions
  } from '../actions'
import serviceList from "../../services/serviceList"

  function* fetchAccountCommentlist(actions) {
    try {
      const { params } = actions
      const json = yield axiosInstance.get(serviceList.accountComments, { params })
      if (json.status === 200) {
        yield put(AccountRiskCommentActions.saveAccountRiskCommentlistResponse(json.data))
      } else {
        yield put(AccountRiskCommentActions.saveAccountRiskCommentlistResponse([]))
      }
    } catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: AccountRiskCommentTypes.SAVE_ACCOUNTRISK_COMMENT_LIST_RESPONSE, data })
    }
  }

  function* fetchAccountCommentUpdate(actions) {
    const { payload } = actions
    const { id, params } = payload && payload
    const endPointUrl = `${serviceList.accountCommentsUpdate}/${id}`
    try {
      const json = yield axiosInstance.put(endPointUrl, params)
      if (json.status === 200) {
        yield put(updateAccountRiskCommentActions.saveupdateAccountRiskCommentResponse(json.data))
      } else {
        yield put(updateAccountRiskCommentActions.saveupdateAccountRiskCommentResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: updateAccountRiskCommentActionsTypes.SAVE_UPDATE_ACCOUNTRISK_COMMENT_RESPONSE, data })
    }
  }

  function* deleteAccountComment(actions) {
    const { params } = actions
    const deleteUrl = `${serviceList.accountCommentsDelete}/${params} `
    try {
      const json = yield axiosInstance.delete(deleteUrl)
      if (json.status === 200) {
        yield put(AccountRiskCommentdeleteActions.saveResponse(json.data))
      } else {
        yield put(AccountRiskCommentdeleteActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: AccountRiskCommentdeleteActionsTypes.RESPONSE, data })
    }
  }

  export function* fetchAccountComment() {
    yield all([
      yield takeLatest(AccountRiskCommentTypes.GET_ACCOUNTRISK_COMMENT_LIST, fetchAccountCommentlist),
      yield takeLatest(updateAccountRiskCommentActionsTypes.UPDATE_ACCOUNTRISK_COMMENT, fetchAccountCommentUpdate),
      yield takeLatest(AccountRiskCommentdeleteActionsTypes.REQUEST, deleteAccountComment)
    ])
  }