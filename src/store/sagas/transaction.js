import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {TransactionTypes,TransactionActions, transactionDashboardTypes, TransactiondashboardActions, 
  TransactionAddTypes, TransactionAddAction, TransactionGetByIdTypes, TransactionGetByIdActions, DropdownTransactionActions, dropdownTransactionTypes,
  AVSTransactionActions, AVSTransactionTypes, CVVTransactionActions, CVVTransactionTypes, PaymentTransactionActions, PaymentTransactionTypes,
  LinkAnlyticsTransactionActions, LinkAnlyticsTransactionTypes, TransactionDistanceActions, transactionDistanceTypes,   merchantIdTypes, merchantIdActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchTransactionrlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transaction, { params })
  if (json.status === 200) {
    yield put(TransactionActions.saveTransactionlistResponse(json.data))
  } else {
    yield put(TransactionActions.saveTransactionlistResponse([]))
  }
}

function* TransactionDashboard(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.transactionDashboard}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(TransactiondashboardActions.savedashboardDetailsResponse(json.data))
  } else {  
    yield put(TransactiondashboardActions.savedashboardDetailsResponse([]))
  }
}

function* fetchAddTransaction(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.transaction, payload)
    if (data && data.data) {
      yield put(TransactionAddAction.saveTransactionResponse(data.data))
    }
  } catch (error) {
    yield put(TransactionAddAction.saveTransactionResponse(error))
  }
}


function* TransactionGetById(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.transaction}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(TransactionGetByIdActions.saveGetByIdDetailsResponse(json.data))
  } else {  
    yield put(TransactionGetByIdActions.saveGetByIdDetailsResponse([]))
  }
}

function* fetchDropDownTransactionrlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionDropDown, { params })
  if (json.status === 200) {
    yield put(DropdownTransactionActions.saveDropDownTransactionlistResponse(json.data))
  } else {
    yield put(DropdownTransactionActions.saveDropDownTransactionlistResponse([]))
  }
}

function* fetchAVSTransactionrlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionAVS, { params })
  if (json.status === 200) {
    yield put(AVSTransactionActions.saveAVSTransactionlistResponse(json.data))
  } else {
    yield put(AVSTransactionActions.saveAVSTransactionlistResponse([]))
  }
}

function* fetchCVVTransactionrlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionCVV, { params })
  if (json.status === 200) {
    yield put(CVVTransactionActions.saveCVVTransactionlistResponse(json.data))
  } else {
    yield put(CVVTransactionActions.saveCVVTransactionlistResponse([]))
  }
}

function* fetchPaymentTransactionrlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionPayment, { params })
  if (json.status === 200) {
    yield put(PaymentTransactionActions.savePaymentTransactionlistResponse(json.data))
  } else {
    yield put(PaymentTransactionActions.savePaymentTransactionlistResponse([]))
  }
}

function* fetchLinkAnlyticsTransactionrlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionLinkAnlytics, { params })
  if (json.status === 200) {
    yield put(LinkAnlyticsTransactionActions.saveLinkAnlyticsTransactionlistResponse(json.data))
  } else {
    yield put(LinkAnlyticsTransactionActions.saveLinkAnlyticsTransactionlistResponse([]))
  }
}


function* TransactionDistance (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.transactionDistance}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(TransactionDistanceActions.saveDistanceDetailsResponse(json.data))
  } else {  
    yield put(TransactionDistanceActions.saveDistanceDetailsResponse([]))
  }
}

function* FetchMerchnatId (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.merchantDropdown}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(merchantIdActions.saveMerchantIdResponse(json.data))
  } else {  
    yield put(merchantIdActions.saveMerchantIdResponse([]))
  }
}



export function* fetchTransactionWatcher() {
    yield all([
      yield takeLatest(TransactionTypes.GET_TRANSACTION_LIST, fetchTransactionrlist),
      yield takeLatest(transactionDashboardTypes.GET_TRANSACTIONDASHBOARD_DETAILS, TransactionDashboard),
      yield takeLatest(TransactionAddTypes.TRANSACTION_POST, fetchAddTransaction),
      yield takeLatest(TransactionGetByIdTypes.TRANSACTION_GET_BY_ID, TransactionGetById),
      yield takeLatest(dropdownTransactionTypes.GET_DROPDOWN_TRANSACTION_LIST, fetchDropDownTransactionrlist),
      yield takeLatest(AVSTransactionTypes.GET_AVS_TRANSACTION_LIST, fetchAVSTransactionrlist),
      yield takeLatest(CVVTransactionTypes.GET_CVV_TRANSACTION_LIST, fetchCVVTransactionrlist),
      yield takeLatest(PaymentTransactionTypes.GET_PAYMENY_TRANSACTION_LIST, fetchPaymentTransactionrlist),
      yield takeLatest(LinkAnlyticsTransactionTypes.GET_LINKANALYTICS_TRANSACTION_LIST, fetchLinkAnlyticsTransactionrlist),
      yield takeLatest(transactionDistanceTypes.GET_TRANSACTION_DISTANCE_DETAILS, TransactionDistance),
      yield takeLatest(merchantIdTypes.GET_MERCHANT_ID_DETAILS, FetchMerchnatId)
    ])
  }