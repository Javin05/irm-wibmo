import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  WrmRiskManagementActionsTypes,
  WrmRiskManagementActions,
  wrmSdkActions,
  wrmSdkActionsTypes,
  WrmStatusActions,
  WrmStatusTypes,
  UpdateWebReportActions,
  UpdateWebReportTypes,
  DashboardListPmaActions,
  DashboardListPmaTypes,
  GetClientsActions,
  GetClientsTypes,
  riskLevelActions,
  riskLevelActionTypes,
  CategoryStatusActions,
  CategoryStatusTypes,
  updateImageUploadActionsTypes,
  updateImageUploadActions,
  updateStatusChangeActionsTypes,
  updateStatusChangeActions,
  wrmStatusChangeActionsTypes,
  wrmStatusChangeActions,
  WrmUpdateQueueActionsTypes,
  WrmUpdateQueueActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchWrmRiskManagement(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.WrmRiskManagement, { params })
  if (json.status === 200) {
    yield put(WrmRiskManagementActions.saveWrmRiskMangemntResponse(json.data))
  } else {
    yield put(WrmRiskManagementActions.saveWrmRiskMangemntResponse([]))
  }
}


function* fetchWrmStatus(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.WrmStatus, { params })
  if (json.status === 200) {
    yield put(WrmStatusActions.saveWrmStatusResponse(json.data))
  } else {
    yield put(WrmStatusActions.saveWrmStatusResponse([]))
  }
}

function* fetchWrmSdkManage(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.WrmRiskManagement, { params })
  if (json.status === 200) {
    yield put(wrmSdkActions.saveWrmSdkManageResponse(json.data))
  } else {
    yield put(wrmSdkActions.saveWrmSdkManageResponse([]))
  }
}

function* fetchUpdateWebReport(actions) {
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.UpdateWebReport, params)
  if (json.status === 200) {
    yield put(UpdateWebReportActions.saveUpdateWebReportResponse(json.data))
  } else {
    yield put(UpdateWebReportActions.saveUpdateWebReportResponse([]))
  }
}

function* fetchDashboardListPma(actions){
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.dashboardPma, {params})
  if (json.status === 200) {
    yield put(DashboardListPmaActions.saveDashboardListPmaResponse(json.data))
  } else {
    yield put(DashboardListPmaActions.saveDashboardListPmaResponse([]))
  }
}

function* GetClientsPma(actions){
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.getClientsWrm, {params})
  if (json.status === 200) {
    yield put(GetClientsActions.saveClientsWrmResponse(json.data))
  } else {
    yield put(GetClientsActions.saveClientsWrmResponse([]))
  }
}

function* fetchRiskLevel() {
  const json = yield axiosInstance.get(serviceList.riskLevelList);
  if (json.status === 200) {
    yield put(riskLevelActions.saveRiskLevelResponse(json.data));
  } else {
    yield put(riskLevelActions.saveRiskLevelResponse([]));
  }
}

function * fetchCategoryStatusManage (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.categoriestatus, { params })
  if (json.status === 200) {
    yield put(CategoryStatusActions.saveCategoryStatusManageResponse(json.data))
  } else {
    yield put(CategoryStatusActions.saveCategoryStatusManageResponse([]))
  }
}

function * fetchImageUpdate (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.UpdateImage}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateImageUploadActions.saveImageUploadResponse(json.data))
  } else {
    yield put(updateImageUploadActions.saveImageUploadResponse([]))
  }
}

function * fetchStatusUpdate (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.statusChange}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateStatusChangeActions.saveStatusChangeResponse(json.data))
  } else {
    yield put(updateStatusChangeActions.saveStatusChangeResponse([]))
  }
}

function * fetchWrmStatusUpdate (actions) {  
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.accountstatus, params)
  if (json.status === 200) {
    yield put(wrmStatusChangeActions.savewrmStatusChangeResponse(json.data))
  } else {
    yield put(wrmStatusChangeActions.savewrmStatusChangeResponse([]))
  }
}

function * fetchWrmUpdateQueue (actions) {  
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.updateAccountQueue, params)
  if (json.status === 200) {
    yield put(WrmUpdateQueueActions.saveWrmUpdateQueueResponse(json.data))
  } else {
    yield put(WrmUpdateQueueActions.saveWrmUpdateQueueResponse([]))
  }
}

export function* fetchWrmRiskManagementWatcher() {
  yield all([yield takeLatest(WrmRiskManagementActionsTypes.GET_WRMRISKMANAGEMENT_LIST, fetchWrmRiskManagement)])
  yield all([yield takeLatest(wrmSdkActionsTypes.GET_WRMSDKMANAGE_LIST, fetchWrmSdkManage)])
  yield all([yield takeLatest(WrmStatusTypes.GET_WRM_STATUS, fetchWrmStatus)])
  yield all([yield takeLatest(UpdateWebReportTypes.GET_UPDATEWEBREPORT, fetchUpdateWebReport)])
  yield all([yield takeLatest(DashboardListPmaTypes.GET_DASHBOARDLIST_PMA, fetchDashboardListPma)])
  yield all([yield takeLatest(GetClientsTypes.GET_CLIENTS, GetClientsPma)])
  yield all([yield takeLatest(riskLevelActionTypes.GET_RISKLEVEL_LIST, fetchRiskLevel),])
  yield all([yield takeLatest(CategoryStatusTypes.GET_CATEGORYSTATUS_LIST, fetchCategoryStatusManage)])
  yield all([yield takeLatest(updateImageUploadActionsTypes.UPDATE_IMAGE_UPLOAD, fetchImageUpdate)])
  yield all([yield takeLatest(updateStatusChangeActionsTypes.UPDATE_STATUS_CHANGE, fetchStatusUpdate)])
  yield all([yield takeLatest(wrmStatusChangeActionsTypes.WRM_STATUS_CHANGE, fetchWrmStatusUpdate)])
  yield all([yield takeLatest(WrmUpdateQueueActionsTypes.WRM_UPDATE_QUEUE, fetchWrmUpdateQueue)])
}
