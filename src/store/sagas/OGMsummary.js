import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { 
  getOGMsummaryTypes,
  OGMSummaryActions,
  OGMlinkAnalyticsTypes,
  OGMlinkAnalyticsActions
} from '../actions'
import serviceList from '../../services/serviceList'


function* fetchgetOGMsummarylist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.ogmSummary, { params })
  if (json.status === 200) {
    yield put(OGMSummaryActions.saveOGMSummarylistResponse(json.data))
  } else {
    yield put(OGMSummaryActions.saveOGMSummarylistResponse([]))
  }
}

function* fetchgetOGMlinkAnalyticslist(actions) {
  const { params } = actions
  const data = `${serviceList.linkanalysis}/${params}`
  const json = yield axiosInstance.get(data)
  if (json.status === 200) {
    yield put(OGMlinkAnalyticsActions.saveOGMlinkAnalyticslistResponse(json.data))
  } else {
    yield put(OGMlinkAnalyticsActions.saveOGMlinkAnalyticslistResponse([]))
  }
}

export function* fetchgetOGMsummarylistWatcher() {
  yield all([
    yield takeLatest(getOGMsummaryTypes.GET_OGM_SUMMARY_LIST, fetchgetOGMsummarylist),
    yield takeLatest(OGMlinkAnalyticsTypes.GET_OGM_LINKANALYTICS_LIST, fetchgetOGMlinkAnalyticslist)

  ])
}