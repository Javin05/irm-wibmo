import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import {
    WatchListTypes,
    WatchListActions
} from '../actions'

function * fetchWatchlistSaga (actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.fetchWatchlist, { params })
    if (json.status === 200) {
        yield put(WatchListActions.fetchWatchListSuccess(json.data))
    } else {
        yield put(WatchListActions.fetchWatchListError([]))
    }
}
function * fetchWatchlistTypeSaga (actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.fetchWatchlistType, { params })
    if (json.status === 200) {
        yield put(WatchListActions.fetchWatchListTypeSuccess(json.data))
    } else {
        yield put(WatchListActions.fetchWatchListTypeError([]))
    }
}
function * createWatchlistTypeSaga (actions) {
    const { formData } = actions
    const json = yield axiosInstance.post(serviceList.createWatchlistType, formData)
    if (json.status === 200) {
        yield put(WatchListActions.createWatchListTypeSuccess(json.data))
    } else {
        yield put(WatchListActions.createWatchListTypeError([]))
    }
}
function * updateWatchlistTypeSaga (actions) {
    const { id, formData } = actions
    const endPointUrl = `${serviceList.updateWatchlistType}/${id}`
    const json = yield axiosInstance.put(endPointUrl, formData)
    if (json.status === 200) {
        yield put(WatchListActions.updateWatchListTypeSuccess(json.data))
    } else {
        yield put(WatchListActions.updateWatchListTypeError([]))
    }
}
function * deleteWatchlistTypeSaga (actions) {
    const { id, params } = actions
    const endPointUrl = `${serviceList.deleteWatchlistType}/${id}`
    const json = yield axiosInstance.delete(endPointUrl)
    if (json.status === 200) {

        yield put(WatchListActions.deleteWatchListTypeSuccess(json.data))
        //yield put({type: WatchListTypes.FETCH_WATCHLISTTYPE_INIT, payload: params });
    } else {
        yield put(WatchListActions.deleteWatchListTypeError([]))
    }
}

export function * watchlisttWatcher () {
    yield all([yield takeLatest(WatchListTypes.FETCH_WATCHLIST_INIT, fetchWatchlistSaga)])
    yield all([yield takeLatest(WatchListTypes.FETCH_WATCHLISTTYPE_INIT, fetchWatchlistTypeSaga)])
    yield all([yield takeLatest(WatchListTypes.CREATE_WATCHLISTTYPE_INIT, createWatchlistTypeSaga)])
    yield all([yield takeLatest(WatchListTypes.UPDATE_WATCHLISTTYPE_INIT, updateWatchlistTypeSaga)])
    yield all([yield takeLatest(WatchListTypes.DELETE_WATCHLISTTYPE_INIT, deleteWatchlistTypeSaga)])
}
