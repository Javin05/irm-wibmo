import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  webReportSettingActionsTypes,webReportSettingActions, webReportSettingAddAction, webReportSettingAddTypes, webReportSettingGetId, webReportSettingGetIdActions, updatewebReportSettingActionsTypes, updatewebReportSettingActions, webReportSettingDeleteActions, webReportSettingDelete
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchwebReportSettinglist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.webReport, { params })
  if (json.status === 200) {
    yield put(webReportSettingActions.savewebReportSettinglistResponse(json.data))
  } else {
    yield put(webReportSettingActions.savewebReportSettinglistResponse([]))
  }
}

function* fetchwebReportSettingLogin(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.webReportAdd, payload)
    if (data && data.data) {
      yield put(webReportSettingAddAction.savewebReportSettingResponse(data.data))
    }
  } catch (error) {
    yield put(webReportSettingAddAction.savewebReportSettingResponse(error))
  }
}

function* getEditwebReportSetting(actions) {
  const { id } = actions
  const idValue = id && id.id
  const params = {
    report_type: "Web Report",
  }
  const endPointUrl = `${serviceList.webReportAdd}/${idValue}`
  const json = yield axiosInstance.get(endPointUrl, { params })
  if (json.status === 200) {
    yield put(webReportSettingGetIdActions.savewebReportSettingIdDetailsResponse(json.data))

  } else {  
    yield put(webReportSettingGetIdActions.savewebReportSettingIdDetailsResponse([]))
  }
}

function * fetchupdate (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.webReportAdd}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updatewebReportSettingActions.saveupdatewebReportSettingResponse(json.data))
  } else {
    yield put(updatewebReportSettingActions.saveupdatewebReportSettingResponse([]))
  }
}

function * fetchDelete (actions) {
  const { payload } = actions
  const idValue = payload && payload.id
  const params = {
    report_type: "Web Report",
  }
  const endPointUrl = `${serviceList.webReportAdd}/${idValue}`
  const json = yield axiosInstance.delete(endPointUrl, params)
  if (json.status === 200) {
    yield put(webReportSettingDeleteActions.savewebReportSettingDeleteResponse(json.data))
  } else {
    yield put(webReportSettingDeleteActions.savewebReportSettingDeleteResponse([]))
  }
}

export function* fetchwebReportSettinglistWatcher() {
  yield all([
    yield takeLatest(webReportSettingActionsTypes.GET_WEBREPROT_SEETING_LIST, fetchwebReportSettinglist),
    yield all([yield takeLatest(webReportSettingAddTypes.WEBREPROT_SEETING_POST, fetchwebReportSettingLogin)]),
    yield takeLatest(webReportSettingGetId.GET_WEBREPROT_SEETING_DETAILS, getEditwebReportSetting),
    yield takeLatest(updatewebReportSettingActionsTypes.UPDATE_WEBREPROT_SEETING, fetchupdate),
    yield takeLatest(webReportSettingDelete.WEBREPROT_SEETING_DELETE, fetchDelete)
  ])
}