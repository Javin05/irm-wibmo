import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
    HomeActionsTypes,HomeActions, QueueValuesActions , QueueValuesActionsTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchHomeId (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.homeIdGet, { params })
  if (json.status === 200) {
    yield put(HomeActions.saveHomelistResponse(json.data))
  } else {
    yield put(HomeActions.saveHomelistResponse([]))
  }

}

function * fetchQueueValues (actions) {

  const { params } = actions
  const json = yield axiosInstance.get(serviceList.Queues)
  if (json.status === 200) {
    yield put(QueueValuesActions.saveQueueValueslistResponse(json.data))
  } else {
    yield put(QueueValuesActions.saveQueueValueslistResponse([]))
  }
}

export function * fetchHomeIdWatcher () {
  yield all([yield takeLatest(HomeActionsTypes.GET_HOME_LIST, fetchHomeId)])
  yield all([yield takeLatest(QueueValuesActionsTypes.GET_QUEUEVALUES_LIST, fetchQueueValues)])
}
