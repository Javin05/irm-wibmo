import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import {
    FraudTypes,
    FraudActions
} from '../actions'

function * fraudStatusChangeSaga (actions) {
    const { formData } = actions
    const endPointUrl = `${serviceList.fraudpatternBulkStatus}`
    const json = yield axiosInstance.put(endPointUrl, formData)
    if (json.status === 200) {
        yield put(FraudActions.ChangeFruadStatusInitSuccess(json.data))
    } else {
        yield put(FraudActions.ChangeFruadStatusInitError([]))
    }
}

export function * fraudStatusWatcher () {
    yield all([yield takeLatest(FraudTypes.FRAUD_STATUS_INIT, fraudStatusChangeSaga)])
}
