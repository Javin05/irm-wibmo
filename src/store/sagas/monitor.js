import { put, takeLatest, all } from 'redux-saga/effects'
import { API_MESSAGES } from '../../utils/constants'
import axiosInstance from '../../services'
import {
  MonitorActions,
  MonitorActionsTypes,
  OGMactions,
  OGMactionsTypes,
  MonitorDashboardActionsTypes,
  MonitorDashboardActions,
  OGMintervalActions,
  OGMintervalActionsTypes,
  MonitorDashboardIdActions,
  MonitorDashboardIdTypes,
  MonitorDashboardDateActions,
  MonitorDashboardDateTypes,
  MonitorDashboardStatusTypes,
  MonitorDashboardStatusActions,
  MonitorCountsActions,
  MonitorCountsTypes,
  MonitorActionUpdateActions,
  MonitorActionUpdateTypes,
  MonitorViewsActions,
  MonitorViewsTypes,
  UpdateCurrentReportActions,
  UpdateCurrentReportTypes,
  monitorExportReportActions,
  monitorExportReportTypes,
  deleteMonitorReportActions,
  monitorDeleteTypes,
  viewBackendDashboardActions,
  viewBackendDashboardTypes,
  UpdateBackendDashboardActions,
  UpdateBackendDashboardTypes,
  MonitarStatusChangeActionsTypes,
  MonitarStatusChangeActions,
  MonitorUpdateQueueActionsTypes,
  MonitorUpdateQueueActions,
  UpdateViewNewCallbackTypes,
  UpdateViewNewCallbackActions,
  OGMEmailTemplateTypes,
  OGMEmailTemplateactions,
  UpdateOGMEmailTemplateTypes,
  UpdateOGMEmailTemplateActions,
  DashbordOGMEmailTemplateactions,
  DashbordOGMEmailTemplateTypes,
  EmailHistoryIdTypes,
  EmailHistoryIdActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchMonitor(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.ongoingmonitoring, { params })
    if (json.status === 200) {
      yield put(MonitorActions.saveMonitorlistResponse(json.data))
    } else {
      yield put(MonitorActions.saveMonitorlistResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorActionsTypes.SAVE_MONITOR_LIST_RESPONSE, data })
  }
}

function* OGMPOST(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.post(serviceList.ongoingmonitoringCreate, params)
    if (json.status === 200) {
      yield put(OGMactions.saveOGMpostResponse(json.data))
    } else {
      yield put(OGMactions.saveOGMpostResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: OGMactionsTypes.OGM_POST_SAVE_RESPONSE, data })
  }
}

function* MonitorDashboard(actions) {
  const { id, date } = actions && actions.payload
  const params = {ogm_last_run_date : date && date.ogm_last_run_date}
  const endPointUrl = `${serviceList.monitorDashboard}/${id}`
  try {
    const json = yield axiosInstance.get(endPointUrl, {params})
    if (json.status === 200) {
      yield put(MonitorDashboardActions.saveMonitorDashboardResponse(json.data))
    } else {
      yield put(MonitorDashboardActions.saveMonitorDashboardResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorDashboardActionsTypes.SAVE_MONITORDASHBOARD_RESPONSE, data })
  }
}

function* OGMinterval(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.createinterval, { params })
    if (json.status === 200) {
      yield put(OGMintervalActions.saveOGMintervalResponse(json.data))
    } else {
      yield put(OGMintervalActions.saveOGMintervalResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: OGMintervalActionsTypes.SAVE_OGM_INTERVAL_LIST_RESPONSE, data })
  }
}

function* MonitorDashboardId(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.ongoingmonitoring}/${id}`
  try {
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
      yield put(MonitorDashboardIdActions.saveMonitorDashboardIdResponse(json.data))
    } else {
      yield put(MonitorDashboardIdActions.saveMonitorDashboardIdResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorDashboardIdTypes.SAVE_MONITORDASHBOARDID_RESPONSE, data })
  }
}

function* MonitorDashboardDate(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.ongoingmonitoringDate}/${id}`
  try {
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
      yield put(MonitorDashboardDateActions.saveMonitorDashboardDateResponse(json.data))
    } else {
      yield put(MonitorDashboardDateActions.saveMonitorDashboardDateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorDashboardDateTypes.SAVE_MONITORDASHBOARD_DATE_RESPONSE, data })
  }
}

function* MonitorDashboardStatus(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.post(serviceList.ongoingmonitoringStatus, params)
    if (json.status === 200) {
      yield put(MonitorDashboardStatusActions.saveMonitorDashboardStatusResponse(json.data))
    } else {
      yield put(MonitorDashboardStatusActions.saveMonitorDashboardStatusResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorDashboardStatusTypes.SAVE_MONITORDASHBOARD_STATUS_RESPONSE, data })
  }
}

function* MonitorCounts(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.ongoingmonitoringCounts, { params })
    if (json.status === 200) {
      yield put(MonitorCountsActions.saveMonitorCountsResponse(json.data))
    } else {
      yield put(MonitorCountsActions.saveMonitorCountsResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorCountsTypes.SAVE_MONITOR_COUNTS_RESPONSE, data })
  }
}

function* ActionUpdate(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.post(serviceList.ongoingmonitoringAction, params)
    if (json.status === 200) {
      yield put(MonitorActionUpdateActions.saveMonitorActionUpdateResponse(json.data))
    } else {
      yield put(MonitorActionUpdateActions.saveMonitorActionUpdateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorActionUpdateTypes.SAVE_MONITOR_ACTIONUPDATE_RESPONSE, data })
  }
}

function* MonitorView(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.ongoingmonitoringviewchanges, { params })
    if (json.status === 200) {
      yield put(MonitorViewsActions.saveMonitorViewsResponse(json.data))
    } else {
      yield put(MonitorViewsActions.saveMonitorViewsResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: MonitorViewsTypes.SAVE_MONITOR_VIEWS_RESPONSE, data })
  }
}

function* UpdateCurrent(actions) {
  const { payload } = actions
  const id = payload && payload.id
  const params = payload && payload.params
  const endPointUrl = `${serviceList.ongoingmonitoringUpdateCurrent}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(UpdateCurrentReportActions.saveUpdateCurrentReportResponse(json.data))
    } else {
      yield put(UpdateCurrentReportActions.saveUpdateCurrentReportResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: UpdateCurrentReportTypes.SAVE_UPDATE_CURRENT_REPORT_RESPONSE, data })
  }
}

function* monitorExportReport(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.ongoingmonitorexport, { params })
    if (json.status === 200) {
      yield put(monitorExportReportActions.savemonitorExportReportResponse(json.data))
    } else {
      yield put(monitorExportReportActions.savemonitorExportReportResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: monitorExportReportTypes.SAVE_MONITOR_EXPORT_REPORT_RESPONSE, data })
  }
}


function* deletMonitorReport(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.post(serviceList.monitorDelete, params)
    if (json.status === 200) {
      yield put(deleteMonitorReportActions.saveDeletemonitorReportResponse(json.data))
    } else {
      yield put(deleteMonitorReportActions.saveDeletemonitorReportResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: monitorDeleteTypes.SAVE_DELETE_MONITOR_REPORT_RESPONSE, data })
  }
}

function* DashboardBackendView(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(`${serviceList.monitorBackendDashboard}/${params}`)
    if (json.status === 200) {
      yield put(viewBackendDashboardActions.savebackendDashboardViewResponse(json.data))
    } else {
      yield put(viewBackendDashboardActions.savebackendDashboardViewResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: viewBackendDashboardTypes.SAVE_GET_BACKEND_DASHBOARD_RESPONSE, data })
  }
}

function* UpdateBackendDashboard(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.monitorUpdateBackendDashboard}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(UpdateBackendDashboardActions.saveUpdateBackendDashboardtResponse(json.data))
    } else {
      yield put(UpdateBackendDashboardActions.saveUpdateBackendDashboardtResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: UpdateBackendDashboardTypes.SAVE_UPDATE_BACKEND_DASHBOARD_RESPONSE, data })
  }
}

function* fetchmonitarStatusAR(actions) {
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.monitorStatus, params);
  if (json.status === 200) {
    yield put(MonitarStatusChangeActions.saveMonitarStatusChangeResponse(json.data));
  } else {
    yield put(MonitarStatusChangeActions.saveMonitarStatusChangeResponse([]));
  }
}

function* fetchMonitorUpdateQueue(actions) {
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.updateOgmQueue, params);
  if (json.status === 200) {
    yield put(MonitorUpdateQueueActions.saveMonitorUpdateQueueResponse(json.data));
  } else {
    yield put(MonitorUpdateQueueActions.saveMonitorUpdateQueueResponse([]));
  }
}

function* UpdateViewNewCallback(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.monitorUpdateViewNewCallback}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(UpdateViewNewCallbackActions.saveUpdateViewNewCallbacktResponse(json.data))
    } else {
      yield put(UpdateViewNewCallbackActions.saveUpdateViewNewCallbacktResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: UpdateViewNewCallbackTypes.SAVE_UPDATE_VIEW_NEW_CALLBACK_RESPONSE, data })
  }
}

function* OGMEMAILTEMPLATEPOST(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.post(serviceList.ogmEmailTemplatePost, params)
    if (json.status === 200) {
      yield put(OGMEmailTemplateactions.saveOGMEmailTemplatepostResponse(json.data))
    } else {
      yield put(OGMEmailTemplateactions.saveOGMEmailTemplatepostResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: OGMEmailTemplateTypes.OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE, data })
  }
}

function* UpdateOGMEmailTemplate(actions) {
  const { payload } = actions
  const { params } = payload && payload
  const endPointUrl = `${serviceList.ogmEmailTemplateUpdate}`
  try {
    const json = yield axiosInstance.post(endPointUrl, params)
    if (json.status === 200) {
      yield put(UpdateOGMEmailTemplateActions.saveUpdateOGMEmailTemplateResponse(json.data))
    } else {
      yield put(UpdateOGMEmailTemplateActions.saveUpdateOGMEmailTemplateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: UpdateOGMEmailTemplateTypes.SAVE_UPDATE_OGM_EMAIL_TEMPLATE_RESPONSE, data })
  }
}

function* DashboardOGMemailTemplate(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.post(serviceList.ogmEmailSingleTemplatePost, params)
    if (json.status === 200) {
      yield put(DashbordOGMEmailTemplateactions.saveDashbordOGMEmailTemplatepostResponse(json.data))
    } else {
      yield put(DashbordOGMEmailTemplateactions.saveDashbordOGMEmailTemplatepostResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: DashbordOGMEmailTemplateTypes.DASHBOARD_OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE, data })
  }
}

function* EmailHistoryId(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.getEmailHistory}/${id}`
  try {
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
      yield put(EmailHistoryIdActions.saveEmailHistoryIdResponse(json.data))
    } else {
      yield put(EmailHistoryIdActions.saveEmailHistoryIdResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: EmailHistoryIdTypes.SAVE_EMAIL_HISTORY_ID_RESPONSE, data })
  }
}

export function* fetchMonitorWatcher() {
  yield all([
    yield takeLatest(DashbordOGMEmailTemplateTypes.DASHBOARD_OGM_EMAIL_TEMPLATE_POST, DashboardOGMemailTemplate),
    yield takeLatest(MonitorActionsTypes.GET_MONITOR_LIST, fetchMonitor),
    yield takeLatest(OGMactionsTypes.OGM_POST, OGMPOST),
    yield takeLatest(MonitorDashboardActionsTypes.GET_MONITORDASHBOARD, MonitorDashboard),
    yield takeLatest(OGMintervalActionsTypes.GET_OGM_INTERVAL_LIST, OGMinterval),
    yield takeLatest(MonitorDashboardIdTypes.GET_MONITORDASHBOARDID, MonitorDashboardId),
    yield takeLatest(MonitorDashboardDateTypes.GET_MONITORDASHBOARD_DATE, MonitorDashboardDate),
    yield takeLatest(MonitorDashboardStatusTypes.GET_MONITORDASHBOARD_STATUS, MonitorDashboardStatus),
    yield takeLatest(MonitorCountsTypes.GET_MONITOR_COUNTS, MonitorCounts),
    yield takeLatest(MonitorActionUpdateTypes.POST_MONITOR_ACTIONUPDATE, ActionUpdate),
    yield takeLatest(MonitorViewsTypes.GET_MONITOR_VIEWS, MonitorView),
    yield takeLatest(UpdateCurrentReportTypes.GET_UPDATE_CURRENT_REPORT, UpdateCurrent),
    yield takeLatest(monitorExportReportTypes.GET_MONITOR_EXPORT_REPORT, monitorExportReport),
    yield takeLatest(monitorDeleteTypes.DELETE_MONITOR_REPORT, deletMonitorReport),
    yield takeLatest(viewBackendDashboardTypes.GET_BACKEND_DASHBOARD, DashboardBackendView),
    yield takeLatest(UpdateBackendDashboardTypes.GET_UPDATE_BACKEND_DASHBOARD, UpdateBackendDashboard),
    yield takeLatest(MonitarStatusChangeActionsTypes.MONITAR_STATUS_CHANGE, fetchmonitarStatusAR),
    yield takeLatest(MonitorUpdateQueueActionsTypes.MONITOR_UPDATE_QUEUE, fetchMonitorUpdateQueue),
    yield takeLatest(UpdateViewNewCallbackTypes.GET_UPDATE_VIEW_NEW_CALLBACK, UpdateViewNewCallback),
    yield takeLatest(OGMEmailTemplateTypes.OGM_EMAIL_TEMPLATE_POST, OGMEMAILTEMPLATEPOST),
    yield takeLatest(UpdateOGMEmailTemplateTypes.GET_UPDATE_OGM_EMAIL_TEMPLATE, UpdateOGMEmailTemplate),
    yield takeLatest(EmailHistoryIdTypes.GET_EMAIL_HISTORY_ID, EmailHistoryId),
  ])
}