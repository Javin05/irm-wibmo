import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { ownerActions, businessOwnerActionsTypes, transactionActions, transactionAmountActionsTypes,
  MonthlyActionsTypes, MonthlyActions, MccActionsTypes, MccActions
  } from '../actions'
import serviceList from '../../services/serviceList'

function * fetchOwnerlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.ownerType, { params })
  if (json.status === 200) {
    yield put(ownerActions.saveOwnerlistResponse(json.data))
  } else {
    yield put(ownerActions.saveOwnerlistResponse([]))
  }
}

function * fetchTransactionlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionAmount, { params })
  if (json.status === 200) {
    yield put(transactionActions.savetransactionlistResponse(json.data))
  } else {
    yield put(transactionActions.savetransactionlistResponse([]))
  }
}

function * fetchMonthlylist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.Monthly, { params })
  if (json.status === 200) {
    yield put(MonthlyActions.saveMonthlylistResponse(json.data))
  } else {
    yield put(MonthlyActions.saveMonthlylistResponse([]))
  }
}

function * fetchMcclist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.Mcc, { params })
  if (json.status === 200) {
    yield put(MccActions.saveMcclistResponse(json.data))
  } else {
    yield put(MccActions.saveMcclistResponse([]))
  }
}

export function * fetchDropDownWatcher () {
  yield all([
    yield takeLatest(businessOwnerActionsTypes.GET_OWNER_LIST, fetchOwnerlist),
    yield takeLatest(transactionAmountActionsTypes.GET_TRANSACTION_AMOUNT_LIST, fetchTransactionlist),
    yield takeLatest(MonthlyActionsTypes.GET_MONTH_LIST, fetchMonthlylist),
    yield takeLatest(MccActionsTypes.GET_MCC_LIST, fetchMcclist)
  ])
}