import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { 
    onBoardingActions, 
    onBoardingActionsTypes, 
    OnBoardingAddTypes,
    OnBoardingAddAction,
    OnBoardingPutAction,
    OnBoardingUpdateTypes,
    OnBoardingSummaryActions,
    OnBoardingSummaryTypes
  } from '../actions'
import serviceList from '../../services/serviceList'
import _ from 'lodash'

function* onBoadingSummarylist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.onBoarding}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(OnBoardingSummaryActions.saveOnBoardingSummaryResponse(json.data))
	} else {
		yield put(OnBoardingSummaryActions.saveOnBoardingSummaryResponse([]))
	}
}

function * fetchOnBoardinglist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.onBoarding, { params })
  if (json.status === 200) {
    yield put(onBoardingActions.saveOnBoardinglistResponse(json.data))
  } else {
    yield put(onBoardingActions.saveOnBoardinglistResponse([]))
  }
}

function* fetchOnBoardingAdd(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.onBoardingAdd, payload);
    if (data && data.data) {
      yield put(OnBoardingAddAction.saveOnBoardingResponse(data.data));
    }
  } catch (error) {
    yield put(OnBoardingAddAction.saveOnBoardingResponse(error));
  }
}

function* fetchOnBoardingUpdate(action) {
  const { payload } = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.onBoardingAdd}/${id}`
  const data = yield axiosInstance.put(endPointUrl, params)
  try {
    if (data && data.data) {
      yield put(OnBoardingPutAction.saveOnBoardingUpdateResponse(data.data));
    }
  } catch (error) {
    yield put(OnBoardingPutAction.saveOnBoardingUpdateResponse(error));
  }
}

export function * fetchOnBoardinglistWatcher () {
    yield all([
      yield takeLatest(OnBoardingSummaryTypes.GET_ON_BOARDING_SUMMARY, onBoadingSummarylist),
      yield takeLatest(onBoardingActionsTypes.GET_ON_BOARDING_LIST, fetchOnBoardinglist),
      yield takeLatest(OnBoardingAddTypes.ON_BOARDING_POST, fetchOnBoardingAdd),
      yield takeLatest(OnBoardingUpdateTypes.ON_BOARDING_UPDATE, fetchOnBoardingUpdate)
    ])
  }