import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  AccountsQueueApproveActions,
  AccountsQueueApproveActionsTypes,
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchAccountsQueueApprove (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.accountsQueueApprove}/${id}`

  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(AccountsQueueApproveActions.saveApproveResponse(json.data))
  } else {
    yield put(AccountsQueueApproveActions.saveApproveResponse([]))
  }
}

export function * fetchAccountsQueueApproveWatcher () {
  yield all([
    yield all([yield takeLatest(AccountsQueueApproveActionsTypes.APPROVE, fetchAccountsQueueApprove)]),
  ])
}
