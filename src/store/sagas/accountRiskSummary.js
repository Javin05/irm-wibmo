import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
	accountriskSummaryTypes,
	accountriskSummaryActions,
	accountmerchantIdDetailsTypes,
	accountmerchantIdDetailsActions,
	accountdashboardGetDetailsTypes,
	accountdashboardDetailsActions,
	accountriskScoreTypes,
	accountriskScoreActions,
	accountmatrixActionTypes,
	accountmatrixActions,
	accountClientIdActionsTypes,
	accountClientIdActions, 
} from '../actions'
// import { 
// 	accountClientIdActionsTypes,
// 	accountClientIdActions, 
// } from '../actions/accountRiskSummary'
import serviceList from '../../services/serviceList'

function* accountriskSummarylist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.accountRiskSummary}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(accountriskSummaryActions.saveaccountRiskSummaryResponse(json.data))
	} else {
		yield put(accountriskSummaryActions.saveaccountRiskSummaryResponse([]))
	}
}

function* accountmerchantlist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.accountSuspectAccounts}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(accountmerchantIdDetailsActions.saveaccountmerchantIdDetailsResponse(json.data))
	} else {
		yield put(accountmerchantIdDetailsActions.saveaccountmerchantIdDetailsResponse([]))
	}
}

function* accountdashboardlist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.accountRiskManagementList}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(accountdashboardDetailsActions.saveaccountdashboardDetailsResponse(json.data))
	} else {
		yield put(accountdashboardDetailsActions.saveaccountdashboardDetailsResponse([]))
	}
}

function* accountriskScorelist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.accountriskScore}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(accountriskScoreActions.saveaccountRiskScoreResponse(json.data))
	} else {
		yield put(accountriskScoreActions.saveaccountRiskScoreResponse([]))
	}
}

function* accountmatrixlist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.accountMatrix}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(accountmatrixActions.saveMatrixDetailsResponse(json.data))
	} else {
		yield put(accountmatrixActions.saveMatrixDetailsResponse([]))
	}
}

function* clientIdlist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.accountClientId}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(accountClientIdActions.saveClientIdResponse(json.data))
	} else {
		yield put(accountClientIdActions.saveClientIdResponse([]))
	}
}

export function* accountRiskSummaryWatcher() {
	yield all([
		yield takeLatest(accountriskSummaryTypes.GET_ACCOUNT_RISK_SUMMARY, accountriskSummarylist),
		yield all([yield takeLatest(accountmerchantIdDetailsTypes.GET_ACCOUNT_MERCHANT_ID, accountmerchantlist)]),
		yield takeLatest(accountdashboardGetDetailsTypes.GET_ACCOUNT_DASHBOARD_DETAILS, accountdashboardlist),
		yield takeLatest(accountriskScoreTypes.GET_ACCOUNT_RISK_SCORE, accountriskScorelist),
		yield takeLatest(accountmatrixActionTypes.GET_ACCOUNT_MATRIX_DETAILS, accountmatrixlist),
		yield takeLatest(accountClientIdActionsTypes.GET_CLIENT_ID_DETAILS, clientIdlist)
	])
}