import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  OgmOperationManagementTypes,
  OgmOperationManagementActions,
  OgmOperatorActions,
  OgmOperatorActionTypes,
  OgmOperationManagementDetailTypes,
  OgmOperationManagementDetailActions,
} from '../actions/ogmOperationManagement'
import serviceList from '../../services/serviceList'
import { API_MESSAGES } from '../../utils/constants'


function* fetchOgmOperationManagement(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.ogmOperationManagement, { params })
  if (json.status === 200) {
    yield put(OgmOperationManagementActions.saveOgmOperationManagemntResponse(json.data))
  } else {
    yield put(OgmOperationManagementActions.saveOgmOperationManagemntResponse([]))
  }
}


function* actionOgmOperation(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.put(serviceList.ogmOperatorAction, params)
    if (json.status === 200) {
      yield put(OgmOperatorActions.saveOgmOperatorActionResponse(json.data))
    } else {
      yield put(OgmOperatorActions.saveOgmOperatorActionResponse([]))
    }
    }
catch (error) {
  const data = {status: 'error', message: API_MESSAGES.SOMETHING_WRONG}
  yield put({type: OgmOperatorActionTypes.RESPONSE, data})
}
}
function* fetchOgmOperationManagementDetail(params) {
  let {id, skip} = params
  const json = yield axiosInstance.get(`${serviceList.ogmOperationManagementDetails}/${id.id}?skip=${id.skip}`)
  console.log(json)
  if (json.status === 200) {
    yield put(OgmOperationManagementDetailActions.saveOgmOperationDetailResponse(json.data))
  } else {
    yield put(OgmOperationManagementDetailActions.saveOgmOperationDetailResponse([]))
  }
}


export function* fetchOgmOperationManagementWatcher() {
  yield all([yield takeLatest(OgmOperationManagementTypes.GET_OGMOPERATIONMANAGEMENT_LIST, fetchOgmOperationManagement)])
  yield all([yield takeLatest(OgmOperatorActionTypes.OGM_OPERATION_ACTION_INIT, actionOgmOperation)])
  yield all([yield takeLatest(OgmOperationManagementDetailTypes.GET_OGMOPERATIONMANAGEMENT_DETAIL, fetchOgmOperationManagementDetail)])
}