import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
    CountryActionsTypes, CountryActions, StateActionsTypes, StateActions, CityActions, CityActionsTypes, AreaActions, AreaActionsTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchCountrylist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.Country, { params })
  if (json.status === 200) {
    yield put(CountryActions.saveCountrylistResponse(json.data))
  } else {
    yield put(CountryActions.saveCountrylistResponse([]))
  }
}

function* fetchStatelist(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.State, { params })
    if (json.status === 200) {
      yield put(StateActions.saveStatelistResponse(json.data))
    } else {
      yield put(StateActions.saveStatelistResponse([]))
    }
  }

  function* fetchCitylist(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.City, { params })
    if (json.status === 200) {
      yield put(CityActions.saveCitylistResponse(json.data))
    } else {
      yield put(CityActions.saveCitylistResponse([]))
    }
  }

  function* fetchArealist(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.Area, { params })
    if (json.status === 200) {
      yield put(AreaActions.saveArealistResponse(json.data))
    } else {
      yield put(AreaActions.saveArealistResponse([]))
    }
  }

  export function* fetchCountrylistWatcher() {
    yield all([
      yield takeLatest(CountryActionsTypes.GET_COUNTRY_LIST, fetchCountrylist),
      yield all([yield takeLatest(StateActionsTypes.GET_STATE_LIST, fetchStatelist)]),
      yield takeLatest(CityActionsTypes.GET_CITY_LIST, fetchCitylist),
      yield takeLatest(AreaActionsTypes.GET_AREA_LIST, fetchArealist)
    ])
  }