import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { 
  riskManagementActions, 
  riskManagementActionsTypes, 
  merchantIdDetailsActions, 
  merchantIdDetailsTypes, 
  linkAnalyticsActions,
  linkAnalyticsActionsTypes,
  ExportListActionsTypes,
  ExportListActions,
  clientIdLIstActions,
  clientIdLIstActionsTypes,
  BWListActions,
  BWListActionsTypes,
  PlayStoreExportActions,
  PlayStoreExportTypes,
  PlayStoreDashboardActions,
  PlayStoreDashboardTypes,
  exportFullReportActions,
  exportFullReportActionsTypes,
  } from '../actions'
import serviceList from '../../services/serviceList'
import _ from 'lodash'
import instanceApi from '../../services/sdkHeader'
import { getLocalStorage } from '../../utils/helper'
import { Link, useLocation } from 'react-router-dom'
const slugs =[ "/wrm-riskmmanagement-sdk"]

function * fetchRiskmanagementlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.acountslistRiskManagement, { params })
  if (json.status === 200) {
    yield put(riskManagementActions.saveRiskManagementlistResponse(json.data))
  } else {
    yield put(riskManagementActions.saveRiskManagementlistResponse([]))
  }
}

function * getEditMerchant (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.suspectAccounts}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(merchantIdDetailsActions.savemerchantIdDetailsResponse(json.data))
  } else {
    yield put(merchantIdDetailsActions.savemerchantIdDetailsResponse([]))
  }
}

function * fetchlinkAnalyticslist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.irmLinkAnlytics, { params })
  if (json.status === 200) {
    yield put(linkAnalyticsActions.savelinkAnalyticslistResponse(json.data))
  } else {
    yield put(linkAnalyticsActions.savelinkAnalyticslistResponse([]))
  }
}

function * fetchexportlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.export, { params })
  if (json.status === 200) {
    yield put(ExportListActions.saveExportListResponse(json.data))
  } else {
    yield put(ExportListActions.saveExportListResponse([]))
  }
}

function * fetchClientlist (actions) {
  const { params } = actions
    const json = yield axiosInstance.get(serviceList.getClients, { params })
    if (json.status === 200) {
      yield put(clientIdLIstActions.saveclientIdLIstResponse(json.data))
    } else {
      yield put(clientIdLIstActions.saveclientIdLIstResponse([]))
    }
}

function * fetchBWlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.blackwhitelistType, { params })
  if (json.status === 200) {
    yield put(BWListActions.saveBWListResponse(json.data))
  } else {
    yield put(BWListActions.saveBWListResponse([]))
  }
}

function * fetchPlayStoreExport (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.playStoreExport, { params })
  if (json.status === 200) {
    yield put(PlayStoreExportActions.savePlayStoreExportResponse(json.data))
  } else {
    yield put(PlayStoreExportActions.savePlayStoreExportResponse([]))
  }
}

function * fetchPlayStoreDashboar (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.playStoreExport, { params })
  if (json.status === 200) {
    yield put(PlayStoreDashboardActions.savePlayStoreDashboardResponse(json.data))
  } else {
    yield put(PlayStoreDashboardActions.savePlayStoreDashboardResponse([]))
  }
}

function * fetchexportFullReportlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.export, { params })
  if (json.status === 200) {
    yield put(exportFullReportActions.saveExportFullReportlistResponse(json.data))
  } else {
    yield put(exportFullReportActions.saveExportFullReportlistResponse([]))
  }
}

export function * fetchRiskmanagementlistWatcher () {
  yield all([
    yield takeLatest(riskManagementActionsTypes.GET_RISK_MANAGEMENT_LIST, fetchRiskmanagementlist),
    yield takeLatest(merchantIdDetailsTypes.GET_MERCHANT_ID, getEditMerchant),
    yield takeLatest(linkAnalyticsActionsTypes.GETLINK_ANALYTIS_LIST, fetchlinkAnalyticslist),
    yield takeLatest(ExportListActionsTypes.GET_EXPORT_LIST, fetchexportlist),
    yield takeLatest(clientIdLIstActionsTypes.GET_CLIENT_ID_LIST, fetchClientlist),
    yield takeLatest(BWListActionsTypes.GET_BW_LIST, fetchBWlist),
    yield takeLatest(PlayStoreExportTypes.GET_PLAYSTORE_EXPORT, fetchPlayStoreExport),
    yield takeLatest(PlayStoreDashboardTypes.GET_PLAYSTORE_DASHBOARD, fetchPlayStoreDashboar),
    yield takeLatest(exportFullReportActionsTypes.GET_EXPORT_FULLREPORT_LIST, fetchexportFullReportlist)
  ])
}