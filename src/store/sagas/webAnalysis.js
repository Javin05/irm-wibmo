import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import {
  WebAnalysisActions,
  WebAnalysisTypes,
  getWebAnalysisActions,
  getWebAnalysisTypes,
  ManualWebAnalysisTypes,
  ManualWebAnalysisActions,
  DeleteWebAnalysisActions,
  DeleteWebAnalysisTypes,
  EditWebAnalysisActions,
  EditWebAnalysisTypes,
  updateWebAnalysisTypes,
  updateWebAnalysisActions
} from '../actions'

function* fetchWebAnalysis(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.csvUpload, payload)
    if (data && data.data) {
      yield put(WebAnalysisActions.saveWebAnalysisResponse(data.data))
    }
  } catch (error) {
    yield put(WebAnalysisActions.saveWebAnalysisResponse(error))
  }
}

function* fetchManualWebAnalysis(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.manulUpload, payload)
    if (data && data.data) {
      yield put(ManualWebAnalysisActions.saveManualWebAnalysisResponse(data.data))
    }
  } catch (error) {
    yield put(ManualWebAnalysisActions.saveManualWebAnalysisResponse(error))
  }
}

function* fetchgetWebAnalysis(action) {
  const { params } = action
  try {
    const data = yield axiosInstance.get(serviceList.getWebAnalysis, {params})
    if (data && data.data) {
      yield put(getWebAnalysisActions.savegetWebAnalysislistResponse(data.data))
    }
  } catch (error) {
    yield put(getWebAnalysisActions.savegetWebAnalysislistResponse(error))
  }
}

function* fetchDeleteWebAnalysis(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.manulUpload}/${id}`
  const json = yield axiosInstance.delete(endPointUrl)
  if (json.status === 200) {
    yield put(DeleteWebAnalysisActions.saveWebAnalysiDetailsResponse(json.data))
  } else {  
    yield put(DeleteWebAnalysisActions.saveWebAnalysiDetailsResponse([]))
  }
}

function* fetchEditWebAnalysis(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.manulUpload}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(EditWebAnalysisActions.EditsaveWebAnalysiDetailsResponse(json.data))
  } else {  
    yield put(EditWebAnalysisActions.EditsaveWebAnalysiDetailsResponse([]))
  }
}

function * fetchupdateWebAnalysis (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.manulUpload}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateWebAnalysisActions.updatesaveWebAnalysiDetailsResponse(json.data))
  } else {
    yield put(updateWebAnalysisActions.updatesaveWebAnalysiDetailsResponse([]))
  }
}

export function* WebAnalysislistWatcher() {
  yield all([
    yield all([yield takeLatest(WebAnalysisTypes.POST_WEBANALYSIS_LIST, fetchWebAnalysis)]),
    yield all([yield takeLatest(getWebAnalysisTypes.GET_WEBANALYSIS_LIST, fetchgetWebAnalysis)]),
    yield all([yield takeLatest(ManualWebAnalysisTypes.POST_MANUAL_WEBANALYSIS_LIST, fetchManualWebAnalysis)]),
    yield all([yield takeLatest(DeleteWebAnalysisTypes.DELETE_WEBANALYSIS_DETAILS, fetchDeleteWebAnalysis)]),
    yield all([yield takeLatest(EditWebAnalysisTypes.EDIT_WEBANALYSIS_DETAILS, fetchEditWebAnalysis)]),
    yield all([yield takeLatest(updateWebAnalysisTypes.UPADTE_WEBANALYSIS_DETAILS, fetchupdateWebAnalysis)])
  ])
}
