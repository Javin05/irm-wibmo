import { put, takeLatest, all } from "redux-saga/effects";
import axiosInstance from "../../services";
import {
  KYCTypes,
  KYCActions,
  KYCAddAction,
  KYCAddTypes,
  KYCphoneVerifyTypes,
  KYCphoneVerify,
  KYCphoneOtpAction,
  KYCphoneOtpTypes,
  KYCpanAction,
  KYCpanTypes,
  KYCgstinAction,
  KYCgstinTypes,
  KYCcinAction,
  KYCcinTypes,
  KYCPersonalpanAction,
  KYCPersonalpanTypes,
  KYCemailTypes,
  KYCemailAction,
  KYCemailOtpAction,
  KYCemailOtpTypes,
  KYCAddALLAction,
  KYCAddAllTypes,
  KYCaccountAction,
  KYCaccountDetailsType,
  KYCAdharNumberType,
  KYCAdharNumberAction,
  KYCUserAction,
  KYCUSerTypes,
  FullKycValueType,
  FullKycValueAction,
  AadhaarFrontAction,
  AadhaarFrontType,
  AadhaarBackAction,
  AadhaarBackType,
  CommomFileAction,
  CommomFileType,
  DistanceType,
  DistanceAction,
  AllDasboardDataAction,
  AllDasboardDataType,
  DasboardAadharAction,
  DasboardAadharType,
  DasboardPanAction,
  DasboardPanType,
  DasboardCinAction,
  DasboardCinType,
  DasboardAPCAction,
  DasboardAPCType,
  DasboardAPCtwoAction,
  DasboardAPCTwoType,
  KycStatusAction,
  KycStatusType,
  KycScoreType,
  KycScoreAction,
  IndidualPanUploadAction,
  IndidualPanUploadType,
  form80UploadAction,
  form80UploadType,
  form12UploadType,
  form12UploadAction,
  DashboardDropdownAction,
  DashboardDropdownType,
  linkAnalyticsAction,
  linkAnalyticsType,
  VideoKYCAction,
  VideoKYCType,
  UENAction,
  UENType,
  UENdashboardAction,
  UENdashboardType,
  EntityAction,
  EntityType,
  EntityValueAction,
  EntityValueType,
  AddressDocAction,
  AddressDocType,
  SubCategoryValueAction,
  SubCategoryValueType,
  CategoryValueAction,
  CategoryValueType,
  PinCodeValueAction,
  PinCodeValueType,
  ExportReportValueType,
  ExportReportValueAction
} from "../actions"
import serviceList from "../../services/serviceList"

function* fetchKYCrlist(actions) {
  const { params } = actions;
  const json = yield axiosInstance.get(serviceList.KYC, { params });
  if (json.status === 200) {
    yield put(KYCActions.saveKYClistResponse(json.data));
  } else {
    yield put(KYCActions.saveKYClistResponse([]));
  }
}

function* fetchKYCUserDetails(actions) {
  const { params } = actions;

  const url = serviceList.KYCUserDetails + `${actions.params}`;
  const response = yield axiosInstance.get(url, {
    // params,
  });
  if (response.status === 200) {
    yield put(KYCUserAction.KYCUser_SUCCESS(response.data));
  } else {
    yield put(KYCUserAction.KYCUser_ERROR([]));
  }
}

function* fetchKYCAdd(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.KYC, payload);
    if (data && data.data) {
      yield put(KYCAddAction.saveKYCResponse(data.data));
    }
  } catch (error) {
    yield put(KYCAddAction.saveKYCResponse(error));
  }
}

function* fetchKYCAllData(action) {
  const { payload } = action;
  const { id, params } = payload && payload;
  const endPointUrl = `${serviceList.MiniKYCAllData}/${id}`;
  try {
    const data = yield axiosInstance.put(endPointUrl, params);
    if (data && data.data) {
      yield put(KYCAddALLAction.saveKYCAlldataResponse(data.data));
    }
  } catch (error) {
    yield put(KYCAddALLAction.saveKYCAlldataResponse(error));
  }
}

function* KYCphoneverifysaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.sendOtp, payload);
    if (data && data.data) {
      yield put(KYCphoneVerify.KYCphoneVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCphoneVerify.KYCphoneVerifyResponse(error));
  }
}

function* KYCphoneOtpsaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.otpverification, payload);
    if (data && data.data) {
      yield put(KYCphoneOtpAction.KYCphoneOTPResponse(data.data));
    }
  } catch (error) {
    yield put(KYCphoneOtpAction.KYCphoneOTPResponse(error));
  }
}

function* KYCpansaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.panverification, payload);
    if (data && data.data) {
      yield put(KYCpanAction.KYCpanVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCpanAction.KYCpanVerifyResponse(error));
  }
}

function* KYCgstinaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.gstverification, payload);
    if (data && data.data) {
      yield put(KYCgstinAction.KYCgstinVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCgstinAction.KYCgstinVerifyResponse(error));
  }
}

function* KYCcinsaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.cinverification, payload);
    if (data && data.data) {
      yield put(KYCcinAction.KYCcinVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCcinAction.KYCcinVerifyResponse(error));
  }
}

function* KYCpesonalaSaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.panverification, payload);
    if (data && data.data) {
      yield put(KYCPersonalpanAction.KYCpersonalpanVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCPersonalpanAction.KYCpersonalpanVerifyResponse(error));
  }
}

function* KYCemailVerifySaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.sendotpmail, payload);
    if (data && data.data) {
      yield put(KYCemailAction.KYCemailVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCemailAction.KYCemailVerifyResponse(error));
  }
}

function* KYCemailOtpVerifySaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(serviceList.verifymailotp, payload);
    if (data && data.data) {
      yield put(KYCemailOtpAction.KYCemailOtpVerifyResponse(data.data));
    }
  } catch (error) {
    yield put(KYCemailOtpAction.KYCemailOtpVerifyResponse(error));
  }
}

function* KYCaccountDetailSaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(
      serviceList.AccountVerification,
      payload
    );
    if (data && data.data) {
      yield put(KYCaccountAction.KYCaccountDetailResponse(data.data));
    }
  } catch (error) {
    yield put(KYCaccountAction.KYCaccountDetailResponse(error));
  }
}

function* AdharNumberSaga(action) {
  const { payload } = action;
  try {
    const data = yield axiosInstance.post(
      serviceList.aadharverification,
      payload
    );
    if (data && data.data) {
      yield put(KYCAdharNumberAction.KYCAdhaarNumberResponse(data.data));
    }
  } catch (error) {
    yield put(KYCAdharNumberAction.KYCAdhaarNumberResponse(error));
  }
}

function* FullKYCAllData(action) {
  const { payload } = action;
  const { id, params } = payload && payload;
  const endPointUrl = `${serviceList.MiniKYCAllData}/${id}`;
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(FullKycValueAction.FullKycValueResponse(json.data))
  } else {
    yield put(FullKycValueAction.FullKycValueResponse([]))
  }
}

function* AadhaarFrontSaga(action) {
  const { payload } = action;
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(AadhaarFrontAction.AadhaarFrontValueResponse(data.data));
    }
  } catch (error) {
    yield put(AadhaarFrontAction.AadhaarFrontValueResponse(error));
  }
}

function* AadhaarBackSaga(action) {
  const { payload } = action;
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(AadhaarBackAction.AadhaarBackValueResponse(data.data));
    }
  } catch (error) {
    yield put(AadhaarBackAction.AadhaarBackValueResponse(error));
  }
}

function* CommonFileSaga(action) {
  const { payload } = action
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(CommomFileAction.CommonFileResponse(data.data))
    }
  } catch (error) {
    yield put(CommomFileAction.CommonFileResponse(error))
  }
}

function* DistanceSaga(action) {
  const url = `${serviceList.Distance}/${action.params}`
  try {const data = yield axiosInstance.get(url)
    if (data && data.data) {
      yield put(DistanceAction.DistanceResponse(data.data))
    }
  } catch (error) {
    yield put(DistanceAction.DistanceResponse(error))
  }
}

function* AllDashboardSaga(action) {
  const url = `${serviceList.ApiData}/${action.params}`
  try {const data = yield axiosInstance.get(url)
    if (data && data.data) {
      yield put(AllDasboardDataAction.AllDashboardResponse(data.data))
    }
  } catch (error) {
    yield put(AllDasboardDataAction.AllDashboardResponse(error))
  }
}

function* AllDashboardAadharSaga(action) {
  const url = `${serviceList.aadhardetails}/${action.params}`
  try {const data = yield axiosInstance.get(url)
    if (data && data.data) {
      yield put(DasboardAadharAction.DashboardAadhaarResponse(data.data))
    }
  } catch (error) {
    yield put(DasboardAadharAction.DashboardAadhaarResponse(error))
  }
}

function* AllDashboardPanSaga(action) {
  const url = `${serviceList.pandetails}/${action.params}`
  try {const data = yield axiosInstance.get(url)
    if (data && data.data) {
      yield put(DasboardPanAction.DashboardPANResponse(data.data))
    }
  } catch (error) {
    yield put(DasboardPanAction.DashboardPANResponse(error))
  }
}

function* AllDashboardCINSaga(action) {
  const url = `${serviceList.cindetails}/${action.params}`
  try {const data = yield axiosInstance.get(url)
    if (data && data.data) {
      yield put(DasboardCinAction.DashboardCINResponse(data.data))
    }
  } catch (error) {
    yield put(DasboardCinAction.DashboardCINResponse(error))
  }
}

function* AllDashboardAPCSaga(action) {
  const {payload} = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.comparedata}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl)
    if (data && data.data) {
      yield put(DasboardAPCAction.DashboardAPCResponse(data.data))
    }
  } catch (error) {
    yield put(DasboardAPCAction.DashboardAPCResponse(error))
  }
}

function* AllDashboardAPCTwoSaga(action) {
  const {payload} = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.comparedata}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl, {params})
    if (data && data.data) {
      yield put(DasboardAPCtwoAction.DashboardAPCTwoResponse(data.data))
    }
  } catch (error) {
    yield put(DasboardAPCtwoAction.DashboardAPCTwoResponse(error))
  }
}

function* KycStatusSaga(action) {
  const {payload} = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.kycStatus}/${id}`
  try {const data = yield axiosInstance.put(endPointUrl, params)
    if (data && data.data) {
      yield put(KycStatusAction.KycStatusResponse(data.data))
    }
  } catch (error) {
    yield put(KycStatusAction.KycStatusResponse(error))
  }
}

function* KycScoreSaga(action) {
  const {payload} = action
  const { id } = payload && payload
  const endPointUrl = `${serviceList.kycScore}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl)
    if (data && data.data) {
      yield put(KycScoreAction.KycScoreResponse(data.data))
    }
  } catch (error) {
    yield put(KycScoreAction.KycScoreResponse(error))
  }
}

function* IndidualPanSaga(action) {
  const { payload } = action
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(IndidualPanUploadAction.IndidualPanUploadResponse(data.data))
    }
  } catch (error) {
    yield put(IndidualPanUploadAction.IndidualPanUploadResponse(error))
  }
}

function* Form80Saga(action) {
  const { payload } = action
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(form80UploadAction.form80UploadResponse(data.data))
    }
  } catch (error) {
    yield put(form80UploadAction.form80UploadResponse(error))
  }
}

function* Form12Saga(action) {
  const { payload } = action
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(form12UploadAction.form12UploadResponse(data.data))
    }
  } catch (error) {
    yield put(form12UploadAction.form12UploadResponse(error))
  }
}

function* AddressDocSaga(action) {
  const { payload } = action
  try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
    if (data && data.data) {
      yield put(AddressDocAction.AddressDocResponse(data.data))
    }
  } catch (error) {
    yield put(AddressDocAction.AddressDocResponse(error))
  }
}

function* DashboardDropdownSaga(action) {
  const {payload} = action
  const { id } = payload && payload
  const endPointUrl = `${serviceList.kycdropdown}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl)
    if (data && data.data) {
      yield put(DashboardDropdownAction.DashboardDropdownResponse(data.data))
    }
  } catch (error) {
    yield put(DashboardDropdownAction.DashboardDropdownResponse(error))
  }
}

function* LinkAnalyticsSaga(action) {
  const {payload} = action
  const { id } = payload && payload
  const endPointUrl = `${serviceList.kyclinkanalytics}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl)
    if (data && data.data) {
      yield put(linkAnalyticsAction.linkAnalyticsResponse(data.data))
    }
  } catch (error) {
    yield put(linkAnalyticsAction.linkAnalyticsResponse(error))
  }
}

function* VideoKYSaga(action) {
  const {payload} = action
  const { id } = payload && payload
  const endPointUrl = `${serviceList.videokyc}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl)
    if (data && data.data) {
      yield put(VideoKYCAction.VideoKYCResponse(data.data))
    }
  } catch (error) {
    yield put(VideoKYCAction.VideoKYCResponse(error))
  }
}

function* UENSaga(action) {
  const {payload} = action
  const { params } = payload && payload
  const endPointUrl = `${serviceList.uenverification}`
  try {const data = yield axiosInstance.post(endPointUrl, params)
    if (data && data.data) {
      yield put(UENAction.UENResponse(data.data))
    }
  } catch (error) {
    yield put(UENAction.UENResponse(error))
  }
}

function* UENdashboardSaga(action) {
  const {payload} = action
  const { id } = payload && payload
  const endPointUrl = `${serviceList.uennumber}/${id}`
  try {const data = yield axiosInstance.get(endPointUrl)
    if (data && data.data) {
      yield put(UENdashboardAction.UENdashboardResponse(data.data))
    }
  } catch (error) {
    yield put(UENdashboardAction.UENdashboardResponse(error))
  }
}

function* fetchEntity(actions) {
  const { params } = actions;
  const json = yield axiosInstance.get(serviceList.kycEntity, { params });
  if (json.status === 200) {
    yield put(EntityAction.EntityResponse(json.data));
  } else {
    yield put(EntityAction.EntityResponse([]));
  }
}

function* fetchEntityValue(actions) {
  const { params } = actions;
  const json = yield axiosInstance.get(serviceList.kycEntityValues, { params });
  if (json.status === 200) {
    yield put(EntityValueAction.EntityValueResponse(json.data));
  } else {
    yield put(EntityValueAction.EntityValueResponse([]));
  }
}

function* fetchCategoryValue(actions) {
  const { params } = actions;
  const json = yield axiosInstance.get(serviceList.kycCategoryValues, { params });
  if (json.status === 200) {
    yield put(CategoryValueAction.CategoryValueResponse(json.data));
  } else {
    yield put(CategoryValueAction.CategoryValueResponse([]));
  }
}

function* fetchSubCategoryValue(actions) {
  const { params } = actions;
  const json = yield axiosInstance.get(serviceList.kycCategoryValues, { params });
  if (json.status === 200) {
    yield put(SubCategoryValueAction.SubCategoryValueResponse(json.data));
  } else {
    yield put(SubCategoryValueAction.SubCategoryValueResponse([]));
  }
}

function* fetchPinCodeValue(actions) {
  const { payload } = actions;
  const json = yield axiosInstance.post(serviceList.verifyPincode, payload );
  if (json.status === 200) {
    yield put(PinCodeValueAction.PinCodeValueResponse(json.data));
  } else {
    yield put(PinCodeValueAction.PinCodeValueResponse([]));
  }
}

function* fetchExportReportValue(actions) {
  const { params } = actions;
  const json = yield axiosInstance.get(serviceList.exportReport, { params } );
  if (json.status === 200) {
    yield put(ExportReportValueAction.ExportReportValueResponse(json.data));
  } else {
    yield put(ExportReportValueAction.ExportReportValueResponse([]));
  }
}

export function* fetchKYCWatcher() {
  yield all([
    yield takeLatest(PinCodeValueType.PINCODE_VALUE_GET, fetchPinCodeValue),
    yield takeLatest(ExportReportValueType.EXPORT_REPORT_VALUE_GET, fetchExportReportValue),
    yield takeLatest(SubCategoryValueType.SUB_CATEGORY_VALUE_GET, fetchSubCategoryValue),
    yield takeLatest(CategoryValueType.CATEGORY_VALUE_GET, fetchCategoryValue),
    yield takeLatest(KYCTypes.GET_KYC_LIST, fetchKYCrlist),
    yield takeLatest(KYCUSerTypes.KYC_USER_INIT, fetchKYCUserDetails),
    yield takeLatest(KYCAddTypes.KYC_POST, fetchKYCAdd),
    yield takeLatest(KYCphoneVerifyTypes.KYC_PHONE_VERIFY, KYCphoneverifysaga),
    yield takeLatest(KYCphoneOtpTypes.KYC_PHONE_OTP, KYCphoneOtpsaga),
    yield takeLatest(KYCpanTypes.KYC_PAN_VERIFY, KYCpansaga),
    yield takeLatest(KYCgstinTypes.KYC_GSTIN_VERIFY, KYCgstinaga),
    yield takeLatest(KYCcinTypes.KYC_CIN_VERIFY, KYCcinsaga),
    yield takeLatest(KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY, KYCpesonalaSaga),
    yield takeLatest(KYCemailTypes.KYC_EMAIL_VERIFY, KYCemailVerifySaga),
    yield takeLatest(KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY,KYCemailOtpVerifySaga),
    yield takeLatest(KYCAddAllTypes.KYC_PUT, fetchKYCAllData),
    yield takeLatest( KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY, KYCaccountDetailSaga),
    yield takeLatest(KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY,AdharNumberSaga),
    yield takeLatest(FullKycValueType.FULL_KYC_VERIFY,FullKYCAllData),
    yield takeLatest(AadhaarBackType.AADHAAR_BACK_VERIFY,AadhaarBackSaga),
    yield takeLatest(AadhaarFrontType.AADHAAR_FRONT_VERIFY,AadhaarFrontSaga),
    yield takeLatest(CommomFileType.COMMON_FILE_VERIFY,CommonFileSaga),
    yield takeLatest(DistanceType.DISTANCE_DATA,DistanceSaga),
    yield takeLatest(AllDasboardDataType.ALL_DASHBOARD,AllDashboardSaga),
    yield takeLatest(DasboardAadharType.DAHBOARD_AADHAAR,AllDashboardAadharSaga),
    yield takeLatest(DasboardPanType.DAHBOARD_PAN,AllDashboardPanSaga),
    yield takeLatest(DasboardCinType.DAHBOARD_CIN,AllDashboardCINSaga),
    yield takeLatest(DasboardAPCType.DAHBOARD_APC,AllDashboardAPCSaga),
    yield takeLatest(DasboardAPCTwoType.DAHBOARD_APCTWO,AllDashboardAPCTwoSaga),
    yield takeLatest(KycStatusType.KYC_STATUS,KycStatusSaga),
    yield takeLatest(KycScoreType.KYC_SCORE,KycScoreSaga),
    yield takeLatest(IndidualPanUploadType.INDIDUAL_PAN_UPLOAD,IndidualPanSaga),
    yield takeLatest(form80UploadType.FPORM_80_G,Form80Saga),
    yield takeLatest(form12UploadType.FPORM_12_G,Form12Saga),
    yield takeLatest(AddressDocType.ADDRESS_DOC_UPLOAD,AddressDocSaga),
    yield takeLatest(DashboardDropdownType.DASHBOARD_DROPDOWN,DashboardDropdownSaga),
    yield takeLatest(linkAnalyticsType.LINK_ANALYTICS,LinkAnalyticsSaga),
    yield takeLatest(VideoKYCType.VIDEO_KYC,VideoKYSaga),
    yield takeLatest(UENType.UEN_NUMBER,UENSaga),
    yield takeLatest(UENdashboardType.UEN_NUMBER_DASHBOARD,UENdashboardSaga),
    yield takeLatest(EntityType.ENTITY_GET,fetchEntity),
    yield takeLatest(EntityValueType.ENTITY_VALUE_GET,fetchEntityValue)
  ])
}
