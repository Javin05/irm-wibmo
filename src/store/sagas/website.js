import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  WebsiteActionsTypes,WebsiteActions
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchWebsite (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.website, { params })
  if (json.status === 200) {
    yield put(WebsiteActions.saveWebsitelistResponse(json.data))
  } else {
    yield put(WebsiteActions.saveWebsitelistResponse([]))
  }

}

export function * fetchWebsiteWatcher () {
  yield all([yield takeLatest(WebsiteActionsTypes.GET_WEBSITE_LIST, fetchWebsite)])
}
