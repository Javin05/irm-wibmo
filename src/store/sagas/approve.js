import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  ApproveActions,
  ApproveActionsTypes,
  ApproveTXnActions,
  ApproveTXnActionsTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchApprove (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.approve}/${id}`

  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(ApproveActions.saveApproveResponse(json.data))
  } else {
    yield put(ApproveActions.saveApproveResponse([]))
  }
}

function * fetchApproveTXN (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.txnApprove}/${id}`

  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(ApproveTXnActions.saveApproveTXnResponse(json.data))
  } else {
    yield put(ApproveTXnActions.saveApproveTXnResponse([]))
  }
}

export function * fetchApproveWatcher () {
  yield all([
    yield all([yield takeLatest(ApproveActionsTypes.APPROVE, fetchApprove)]),
    yield all([yield takeLatest(ApproveTXnActionsTypes.APPROVE_TXN, fetchApproveTXN)])
  ])
}
