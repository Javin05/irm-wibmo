import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { TransactionLaundringTypes, TransactionLaundringActions } from '../actions'
import serviceList from '../../services/serviceList'

function* fetchTransactionLaundringlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.transactionLaundring, { params })
  if (json.status === 200) {
    yield put(TransactionLaundringActions.saveTransactionLaundringlistResponse(json.data))
  } else {
    yield put(TransactionLaundringActions.saveTransactionLaundringlistResponse([]))
  }
}

export function* fetchTransactionLaundringWatcher() {
  yield all([
    yield takeLatest(TransactionLaundringTypes.GET_TRANSACTIONLAUNDRING_LIST, fetchTransactionLaundringlist)
  ])
}