import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  QueueIdActions,
  QueueIdActionsTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchQueueId (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.queueIdGet}/${id}`

  const json = yield axiosInstance.get(endPointUrl, params)
  if (json.status === 200) {
    yield put(QueueIdActions.saveQueueIdResponse(json.data))
  } else {
    yield put(QueueIdActions.saveQueueIdResponse([]))
  }

}

export function * fetchQueueIdWatcher () {
  yield all([yield takeLatest(QueueIdActionsTypes.QUEUEID, fetchQueueId)])
}
