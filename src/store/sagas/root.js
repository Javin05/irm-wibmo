import { all } from "redux-saga/effects";
import { fetchPreventionWatcher } from "./preventionAlert";
import { fetchLoginWatcher } from "./login";
import { fetchLogoutWatcher } from "./logout";
import { fetchForgotWatcher } from "./forgotPassword";
import { fetchRiskmanagementlistWatcher } from "./riskManagementList";
import { fetchRiskSummaryWatcher } from "./riskSummary";
import { fetchMerchantLoginWatcher } from "./merchantLogin";
import { fetchApproveWatcher } from "./approve";
import { fetchDropDownWatcher } from "./merchantDropDown";
import { fetchMatrixtWatcher } from "./matrix";
import { fetchqueueslistWatcher } from "./queues";
import { fetchRulelistWatcher } from "./rules";
import { fetchInputlistWatcher } from "./inputFields";
import { fetchQueueIdWatcher } from "./queueIdMatch";
import { fetchHomeIdWatcher } from "./home";
import { fetchAdminlistWatcher } from "./admin";
import { fetchdashboardWatcher } from "./dasboard";
import { fetchTransactionWatcher } from "./transaction";
import { fetchWebsiteWatcher } from "./website";
import { fetchCountrylistWatcher } from "./country";
import { fetchBlockListWatcher } from "./blocklist";
import { fetchAMLWatcher } from "./AMLqueue";
import { fetchAClientManagementWatcher } from "./clientManagements";
import { fetchFilterClientWatcher } from "./authBasedFilter";
import { fetchOnBoardinglistWatcher } from "./onBoarding"

//onboarding
import { fetchaddClientWatcher } from "./clientManagement";
import { fetchClientWatcher } from "./clients";
import { fetchCRMWatcher } from "./crm";
import { fetchComponentsWatcher } from "./userComponents";
import { fetchMenuCompWatcher } from "./MenuComponents";
import { fetchUserroleWatcher } from "./userRole";
import { fetchExportWatcher } from "./export";
import { fetchUsertypeWatcher } from "./userType";
import { fetchPrivilegesWatcher } from "./userPrevileges";
import { fetchKYCWatcher } from "./Kyc";
import { whitelistWatcher } from "./whiteList";
import { watchlisttWatcher } from "./watchList";
import { WebAnalysislistWatcher } from "./webAnalysis";
import { fetchUploadWatcher } from "../../utils/dropzone/redux/saga";
import { fetchClientFilterWatcher } from "./filterDropdown";
import { fetchPartnerWatcher } from "./partners";
import { priceWatcher } from "./priceCheck";
import { fetchUserProfileWatcher } from "./userProfile";
import { fraudStatusWatcher } from "./fraudSaga";
import { fetchSiteConfigWatcher } from "./siteConfig";
import { fetchKYCStaticSummaryWatcher } from "./staticSummarySaga";
import { fetchApiKeyWatcher } from './apiKey'
import { fetchMerchantWatcher } from './merchants'
import { fetchTagSummaryWatcher } from './tagSummary'
import { fetchWrmRiskManagementWatcher } from './WrmRiskManagement'
import { fetchWrmSdkWatcher } from './wrmSdk'
import { fetchMonitorWatcher } from './monitor'
import { fetchTransactionLaundringWatcher } from './transactionLaundring'
import { fetchBusinessUploadWatcher } from './kycUpload'
import { fetchwebReportSettinglistWatcher } from './webReportSetting'
import { fetchplayStoreReportSettinglistWatcher } from './playReportSetting'
import { fetchClientWebReportWatcher } from './clientReport'
import { fetchMerchantSMTPWatcher } from './SmtpConfig'
import { fetchNotifyWatcher } from './NotificationType'
import { fetchgetOGMsummarylistWatcher } from './OGMsummary'
import { accountRiskSummaryWatcher } from './accountRiskSummary'
import { fetchAccountsQueueApproveWatcher } from './accountsQueueApprove'
import { fetchKYCComment } from './kycComment'
import { fetchAccountComment } from './accountRiskComment'
import { fetchWrmOperationManagementWatcher } from "./wrmOperationManagement";
import { fetchOgmOperationManagementWatcher } from "./ogmOperationManagement";
import { fetchWRMComment } from './wrmComment'

export default function* rootSaga() {
  yield all([
    accountRiskSummaryWatcher(),
    fetchgetOGMsummarylistWatcher(),
    fetchNotifyWatcher(),
    fetchMerchantSMTPWatcher(),
    fetchClientWebReportWatcher(),
    fetchplayStoreReportSettinglistWatcher(),
    fetchwebReportSettinglistWatcher(),
    fetchPreventionWatcher(),
    fetchLoginWatcher(),
    fetchLogoutWatcher(),
    fetchForgotWatcher(),
    fetchRiskmanagementlistWatcher(),
    fetchRiskSummaryWatcher(),
    fetchMerchantLoginWatcher(),
    fetchApproveWatcher(),
    fetchDropDownWatcher(),
    fetchMatrixtWatcher(),
    fetchqueueslistWatcher(),
    fetchRulelistWatcher(),
    fetchInputlistWatcher(),
    fetchQueueIdWatcher(),
    fetchHomeIdWatcher(),
    fetchAdminlistWatcher(),
    fetchdashboardWatcher(),
    fetchTransactionWatcher(),
    fetchWebsiteWatcher(),
    fetchCountrylistWatcher(),
    fetchBlockListWatcher(),
    fetchAMLWatcher(),
    fetchAClientManagementWatcher(),
    fetchFilterClientWatcher(),
    //onboarding
    fetchaddClientWatcher(),
    fetchClientWatcher(),
    fetchCRMWatcher(),
    fetchComponentsWatcher(),
    fetchMenuCompWatcher(),
    fetchUserroleWatcher(),
    fetchExportWatcher(),
    fetchUsertypeWatcher(),
    fetchPrivilegesWatcher(),
    whitelistWatcher(),
    watchlisttWatcher(),
    fetchKYCWatcher(),
    WebAnalysislistWatcher(),
    fetchUploadWatcher(),
    fetchClientFilterWatcher(),
    fetchPartnerWatcher(),
    priceWatcher(),
    fetchUserProfileWatcher(),
    fraudStatusWatcher(),
    fetchSiteConfigWatcher(),
    fetchKYCStaticSummaryWatcher(),
    fetchApiKeyWatcher(),
    fetchMerchantWatcher(),
    fetchTagSummaryWatcher(),
    fetchWrmRiskManagementWatcher(),
    fetchWrmSdkWatcher(),
    fetchMonitorWatcher(),
    fetchTransactionLaundringWatcher(),
    fetchBusinessUploadWatcher(),
    fetchAccountsQueueApproveWatcher(),
    fetchOnBoardinglistWatcher(),
    fetchKYCComment(),
    fetchAccountComment(),
    fetchWrmOperationManagementWatcher(),
    fetchOgmOperationManagementWatcher(),
    fetchWRMComment()
  ])
}