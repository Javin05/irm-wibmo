import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  LogoutActions,
  LogoutActionsTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchLogout() {
  try {
    const data = yield axiosInstance.post(serviceList.logout)
    if (data && data.data) {
      yield put(LogoutActions.saveLogoutResponse(data.data))
    }
  } catch (error) {
    yield put(LogoutActions.saveLogoutResponse(error))
  }
}

export function* fetchLogoutWatcher() {
  yield all([yield takeLatest(LogoutActionsTypes.LOGOUT, fetchLogout)])
}