import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import {
    PriceTypes,
    PriceActions
} from '../actions'

function * fetchPriceCheck (actions) {
    const { id } = actions
    const endPointUrl = `${serviceList.fetchPriceCheck}/${id}`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
        yield put(PriceActions.fetchPopupSuccess(json.data))
    } else {
        yield put(PriceActions.fetchPopupError([]))
    }
}
function * createPriceCheck (actions) {
    const { id, formData } = actions
    const endPointUrl = `${serviceList.postPriceCheck}/${id}`
    const json = yield axiosInstance.post(endPointUrl, formData)
    if (json.status === 200) {
        yield put(PriceActions.postPopupSuccess(json.data))
    } else {
        yield put(PriceActions.postPopupError([]))
    }
}

export function * priceWatcher () {
    yield all([
        yield takeLatest(PriceTypes.FETCH_PRICE_POPUP_INIT, fetchPriceCheck),
        yield takeLatest(PriceTypes.PRICE_POPUP_INIT, createPriceCheck)
    ])
}
