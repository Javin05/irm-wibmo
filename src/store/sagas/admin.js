import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  AdminActionsTypes,AdminActions, AdminAddAction, AdminAddTypes, AdminGetId, AdminGetIdActions, updateAdminActionsTypes, updateAdminActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchAdminlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.admin, { params })
  if (json.status === 200) {
    yield put(AdminActions.saveAdminlistResponse(json.data))
  } else {
    yield put(AdminActions.saveAdminlistResponse([]))
  }
}

function* fetchAdminLogin(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.admin, payload)
    if (data && data.data) {
      yield put(AdminAddAction.saveAdminResponse(data.data))
    }
  } catch (error) {
    yield put(AdminAddAction.saveAdminResponse(error))
  }
}

function* getEditAdmin(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.admin}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(AdminGetIdActions.saveAdminIdDetailsResponse(json.data))

  } else {  
    yield put(AdminGetIdActions.saveAdminIdDetailsResponse([]))
  }
}

function * fetchupdateQueue (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.admin}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateAdminActions.saveupdateAdminResponse(json.data))
  } else {
    yield put(updateAdminActions.saveupdateAdminResponse([]))
  }
}

export function* fetchAdminlistWatcher() {
  yield all([
    yield takeLatest(AdminActionsTypes.GET_ADMIN_LIST, fetchAdminlist),
    yield all([yield takeLatest(AdminAddTypes.ADMIN_POST, fetchAdminLogin)]),
    yield takeLatest(AdminGetId.GET_ADMIN_DETAILS, getEditAdmin),
    yield takeLatest(updateAdminActionsTypes.UPDATE_ADMIN, fetchupdateQueue)
  ])
}