import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  BlockListTypes, 
  BlockListActions, 
  BlockListEmailActions, 
  BlockListEmailTypes, 
  BlockListPhoneActions, 
  BlockListPhoneTypes,
  BlockListUpdateActions, 
  BlockListUpdateTypes, 
  BlockListEditActions, 
  BlockListEditTypes,
  BlockListUploadActions,
  BlockListUploadTypes,
  EmailBlocklistsTypes,
  EmailBlockListActions,
  BlockListDeleteActions,
  BlockListDeleteTypes,
  DeleteClientActions,
  DeleteClientTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchBlockList(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.blacklist, { params })
  if (json.status === 200) {
    yield put(BlockListActions.saveBlockListlistResponse(json.data))
  } else {
    yield put(BlockListActions.saveBlockListlistResponse([]))
  }
}

function* fetchBlockListEmail(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.BlockListType, { params })
    if (json.status === 200) {
      yield put(BlockListEmailActions.saveBlockListTypeResponse(json.data))
    } else {
      yield put(BlockListEmailActions.saveBlockListTypeResponse([]))
    }
  }

  function* fetchBlockListUpload(actions) {
    const { payload } = actions
    const json = yield axiosInstance.post(serviceList.BlockListUpload, payload )
    if (json.status === 200) {
      yield put(BlockListUploadActions.saveBlockListUploadResponse(json.data))
    } else {
      yield put(BlockListUploadActions.saveBlockListUploadResponse([]))
    }
  }

  function* fetchBlockListUpdate(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.BlockListType}/${id}`

    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(BlockListUpdateActions.saveBlockListlistResponse(json.data))
    } else {
      yield put(BlockListUpdateActions.saveBlockListlistResponse([]))
    }
  }

  function* fetchBlockListEdit(actions) {
  const { id } = actions
    const endPointUrl = `${serviceList.BlockListType}/${id}`

    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
      yield put(BlockListEditActions.saveBlockListlistResponse(json.data))
    } else {
      yield put(BlockListEditActions.saveBlockListlistResponse([]))
    }
  }

  function* blockMultipleEmail(actions) {
      const { payload } = actions
      try {
        const data = yield axiosInstance.post(serviceList.BlockEmail, payload)
        if (data && data.data) {
          yield put(EmailBlockListActions.addEmailBlacklistSuccess(data.data))
        }
      } catch (error) {
        yield put(EmailBlockListActions.addEmailBlacklistError(error))
      }
  }

  function* blockListDelete(actions) {
    const { id } = actions
      const endPointUrl = `${serviceList.BlockListType}/${id}`
      const json = yield axiosInstance.delete(endPointUrl)
      if (json.status === 200) {
        yield put(BlockListDeleteActions.saveBlockListlistDeleteResponse(json.data))
      } else {
        yield put(BlockListDeleteActions.saveBlockListlistDeleteResponse([]))
      }
    }

    function* DeleteClient (actions) {
      const { payload } = actions
      try {
        const data = yield axiosInstance.post(serviceList.BlockListDeleteall, payload)
        if (data && data.data) {
          yield put(DeleteClientActions.DeleteClientSuccess(data.data))
        }
      } catch (error) {
        yield put(DeleteClientActions.DeleteClientError(error))
      }
      }

export function* fetchBlockListWatcher() {
    yield all([
      yield takeLatest(BlockListTypes.GET_BLOCKLIST_LIST, fetchBlockList),
      yield takeLatest(BlockListEmailTypes.GET_BLOCKLISTEMAIL_LIST, fetchBlockListEmail),
      yield takeLatest(BlockListUploadTypes.GET_BLOCKLISTUPLOAD_LIST, fetchBlockListUpload),
      yield takeLatest(BlockListUpdateTypes.GET_BLOCKLIST_UPDATE_LIST, fetchBlockListUpdate),
      yield takeLatest(BlockListEditTypes.GET_BLOCKLIST_EDIT_LIST, fetchBlockListEdit),
      yield takeLatest(EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_INIt, blockMultipleEmail),
      yield takeLatest(BlockListDeleteTypes.GET_BLOCKLIST_DELETE_LIST, blockListDelete),
      yield takeLatest(DeleteClientTypes.DELETE_CLIENT, DeleteClient)
    ])
  }