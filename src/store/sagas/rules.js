import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { rulesActionTypes, rulesActions, RulesGetId, RulesGetIdActions,
  updateRulesActionsTypes, updateRulesActions } from '../actions'
import serviceList from '../../services/serviceList'

function* fetchRuleslist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.ruleGroup, { params })
  if (json.status === 200) {
    yield put(rulesActions.saveRulesResponse(json.data))
  } else {
    yield put(rulesActions.saveRulesResponse([]))
  }
}

function * postRulesList (actions) {
  const { payload } = actions
  try {
    const data = yield axiosInstance.post(serviceList.ruleGroup, payload)
    if (data && data.data) {
      yield put(rulesActions.PostRulesResponse(data.data))
    }
  } catch (error) {
    yield put(rulesActions.PostRulesResponse(error))
  }
}

function * deleteRulesList (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.ruleGroup}/${id}`
  const json = yield axiosInstance.delete(endPointUrl)
  if (json.status === 200) {
    yield put(rulesActions.DeleteRulesResponse(json.data))
  } else {
    yield put(rulesActions.DeleteRulesResponse([]))
  }
}

function* getEditRules(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.ruleGroup}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(RulesGetIdActions.saveRulesIdDetailsResponse(json.data))
  } else {  
    yield put(RulesGetIdActions.saveRulesIdDetailsResponse([]))
  }
}

function * fetchupdateRules (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.ruleGroup}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateRulesActions.saveupdateRulesResponse(json.data))
  } else {
    yield put(updateRulesActions.saveupdateRulesResponse([]))
  }
}


export function* fetchRulelistWatcher() {
  yield all([
    yield takeLatest(rulesActionTypes.GET_RULES, fetchRuleslist),
    yield takeLatest(rulesActionTypes.POST_RULES, postRulesList),
    yield takeLatest(rulesActionTypes.DELETE_RULES, deleteRulesList),
    yield takeLatest(RulesGetId.GET_RULES_DETAILS, getEditRules),
    yield takeLatest(updateRulesActionsTypes.UPDATE_RULES, fetchupdateRules),
  ])
}