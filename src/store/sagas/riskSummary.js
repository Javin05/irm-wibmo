import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  riskSummaryActions,
  riskSummaryTypes,
  GetAsigneeActions, 
  GetAsigneeTypes,
  AsiggnActions,
  AsiggnActionsTypes,
  riskScoreActions,
  riskScoreTypes,
  PostCategoryActions,
  PostCategoryTypes,
  GetCategoryActions,
  GetCategoryTypes,
  PostCategory1Actions,
  PostCategory1Types,
  PostPMAaction,
  PostPMATypes
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchRiskSummary (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.riskSummary}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(riskSummaryActions.saveRiskSummaryResponse(json.data))
  } else {
    yield put(riskSummaryActions.saveRiskSummaryResponse([]))
  }
}

function * fetchAssignee (actions) {
  const params = actions.params
  const endPointUrl = `${serviceList.Assignee}`
  const json = yield axiosInstance.get(endPointUrl, {params} )
  if (json.status === 200) {
    yield put(GetAsigneeActions.GetSaveSummaryResponse(json.data))
  } else {
    yield put(GetAsigneeActions.GetSaveSummaryResponse([]))
  }
}

function * fetchUpadteAssign (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.merchantLogin}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(AsiggnActions.saveAsiggnResponse(json.data))
  } else {
    yield put(AsiggnActions.saveAsiggnResponse([]))
  }
}

function * fetchRiskScore (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.riskScore}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(riskScoreActions.saveRiskScoreResponse(json.data))
  } else {
    yield put(riskScoreActions.saveRiskScoreResponse([]))
  }
}

function * fetchCategory (actions) {
  const params = actions.params
  const endPointUrl = `${serviceList.riskCategoryStatus}`
  const json = yield axiosInstance.post(endPointUrl, params )
  if (json.status === 200) {
    yield put(PostCategoryActions.PostCategreeResponse(json.data))
  } else {
    yield put(PostCategoryActions.PostCategreeResponse([]))
  }
}

function * fetchCategory1 (actions) {
  const params = actions.params
  const endPointUrl = `${serviceList.riskCategory1Status}`
  const json = yield axiosInstance.post(endPointUrl, params )
  if (json.status === 200) {
    yield put(PostCategory1Actions.PostCategoryResponse(json.data))
  } else {
    yield put(PostCategory1Actions.PostCategoryResponse([]))
  }
}

function * fetchGetCategory (actions) {
  const id = actions.params
  const endPointUrl = `${serviceList.getCategory}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(GetCategoryActions.GetCategroyResponse(json.data))
  } else {
    yield put(GetCategoryActions.GetCategroyResponse([]))
  }
}

function * fetchPostPMA (actions) {
  const params = actions.params
  const endPointUrl = `${serviceList.PMAupdated}`
  const json = yield axiosInstance.post(endPointUrl, params )
  if (json.status === 200) {
    yield put(PostPMAaction.PostPMAResponse(json.data))
  } else {
    yield put(PostPMAaction.PostPMAResponse([]))
  }
}

export function * fetchRiskSummaryWatcher () {
  yield all([
    yield takeLatest(riskSummaryTypes.GET_RISK_SUMMARY, fetchRiskSummary),
    yield takeLatest(GetAsigneeTypes.GET_ASSINEE, fetchAssignee),
    yield takeLatest(AsiggnActionsTypes.ASSIGN, fetchUpadteAssign),
    yield takeLatest(riskScoreTypes.GET_RISK_SCORE, fetchRiskScore),
    yield takeLatest(PostCategoryTypes.POST_CATEGORY, fetchCategory),
    yield takeLatest(GetCategoryTypes.GET_CATEGORY, fetchGetCategory),
    yield takeLatest(PostCategory1Types.POST_CATEGORY1, fetchCategory1),
    yield takeLatest(PostPMATypes.POST_PMA, fetchPostPMA)
  ])
}
