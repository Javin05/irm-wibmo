import { put, takeLatest, all } from 'redux-saga/effects'
import instanceApi from '../../services/sdkHeader'
import { 
  SdkExportListActionsTypes,
  SdkExportListActions,
  postSdkWebAnalysisTypes,
  PostWebAnalysisActions,
  SdkManualWebAnalysisTypes,
  SdkManualWebAnalysisActions,
  getSdkWebAnalysisActions,
  getSdkWebAnalysisTypes,
  SdkBlockListEmailTypes,
  SDKBlockListEmailActions,
  SdkBWListActionsTypes,
  SdkBWListActions,
  PostSdkCategoryActions,
  PostSdkCategoryTypes,
  PostSdkCategory1Actions,
  PostSdkCategory1Types,
  WrmListStatusChangeActions,
  WrmListStatusChangeActionsTypes,
  WrmListUpdateQueueActionsTypes,
  WrmListUpdateQueueActions
} from '../actions'
import serviceList from '../../services/serviceList'
import instance from '../../services/index'

function * fetSdkchexportlist (actions) {
  const { params } = actions
  const json = yield instance.get(serviceList.export, { params })
  if (json.status === 200) {
    yield put(SdkExportListActions.saveSdkExportListResponse(json.data))
  } else {
    yield put(SdkExportListActions.saveSdkExportListResponse([]))
  }
}

function* fetchSdkPostWebAnalysis(action) {
  const { payload } = action
  try {
    const data = yield instance.post(serviceList.csvUpload, payload)
    if (data && data.data) {
      yield put(PostWebAnalysisActions.saveSdkWebAnalysisResponse(data.data))
    }
  } catch (error) {
    yield put(PostWebAnalysisActions.saveSdkWebAnalysisResponse(error))
  }
}

function* fetchSdkManualWebAnalysis(action) {
  const { payload } = action
  try {
    const data = yield instance.post(serviceList.manulUpload, payload)
    if (data && data.data) {
      yield put(SdkManualWebAnalysisActions.saveSdkManualWebAnalysisResponse(data.data))
    }
  } catch (error) {
    yield put(SdkManualWebAnalysisActions.saveSdkManualWebAnalysisResponse(error))
  }
}

function* fetchgetSdkWebAnalysis(action) {
  const { params } = action
  try {
    const data = yield instance.get(serviceList.getWebAnalysis, {params})
    if (data && data.data) {
      yield put(getSdkWebAnalysisActions.savegetSdkWebAnalysislistResponse(data.data))
    }
  } catch (error) {
    yield put(getSdkWebAnalysisActions.savegetSdkWebAnalysislistResponse(error))
  }
}

function* fetchSdkBlockList(action) {
  const { params } = action
  try {
    const data = yield instance.get(serviceList.BlockListType, {params})
    if (data && data.data) {
      yield put(SDKBlockListEmailActions.saveSdkBlockListTypeResponse(data.data))
    }
  } catch (error) {
    yield put(SDKBlockListEmailActions.saveSdkBlockListTypeResponse(error))
  }
}

function* fetchSdkBWlist(action) {
  const { params } = action
  try {
    const data = yield instance.get(serviceList.blackwhitelistType, {params})
    if (data && data.data) {
      yield put(SdkBWListActions.saveSdkBWListResponse(data.data))
    }
  } catch (error) {
    yield put(SdkBWListActions.saveSdkBWListResponse(error))
  }
}

function * fetchSdkPostCategory (actions) {
  const params = actions.params
  const endPointUrl = `${serviceList.riskCategoryStatus}`
  const json = yield instance.post(endPointUrl, params )
  if (json.status === 200) {
    yield put(PostSdkCategoryActions.PostSdkCategroyResponse(json.data))
  } else {
    yield put(PostSdkCategoryActions.PostSdkCategroyResponse([]))
  }
}

function * fetchSdkCategory1 (actions) {
  const params = actions.params
  const endPointUrl = `${serviceList.riskCategory1Status}`
  const json = yield instance.post(endPointUrl, params )
  if (json.status === 200) {
    yield put(PostSdkCategory1Actions.PostSdkCategoryResponse(json.data))
  } else {
    yield put(PostSdkCategory1Actions.PostSdkCategoryResponse([]))
  }
}

function* fetchWrmStatus(actions) {
  const { params } = actions
  const json = yield instance.post(serviceList.wrmStatus, params);
  if (json.status === 200) {
    yield put(WrmListStatusChangeActions.saveWrmListStatusChangeResponse(json.data));
  } else {
    yield put(WrmListStatusChangeActions.saveWrmListStatusChangeResponse([]));
  }
}

function* fetchWrmListUpdateQueue(actions) {
  const { params } = actions
  const json = yield instance.post(serviceList.updateWrmQueue, params);
  if (json.status === 200) {
    yield put(WrmListUpdateQueueActions.saveWrmListUpdateQueueResponse(json.data));
  } else {
    yield put(WrmListUpdateQueueActions.saveWrmListUpdateQueueResponse([]));
  }
}

export function * fetchWrmSdkWatcher () {
  yield all([
    yield takeLatest(SdkExportListActionsTypes.GET_SDK_EXPORT_LIST, fetSdkchexportlist),
    yield takeLatest(postSdkWebAnalysisTypes.POST_SDK_WEBANALYSIS_LIST, fetchSdkPostWebAnalysis),
    yield takeLatest(SdkManualWebAnalysisTypes.POST_SDK_MANUAL_WEBANALYSIS_LIST, fetchSdkManualWebAnalysis),
    yield takeLatest(getSdkWebAnalysisTypes.GET_SDK_WEBANALYSIS_LIST, fetchgetSdkWebAnalysis),
    yield takeLatest(SdkBlockListEmailTypes.GET_SDK_BLOCKLISTEMAIL_LIST, fetchSdkBlockList),
    yield takeLatest(SdkBWListActionsTypes.GET_SDK_BW_LIST, fetchSdkBWlist),
    yield takeLatest(PostSdkCategoryTypes.POST_SDK_CATEGORY, fetchSdkPostCategory),
    yield takeLatest(PostSdkCategory1Types.POST_SDK_CATEGORY1, fetchSdkCategory1),
    yield takeLatest(WrmListStatusChangeActionsTypes.WRM_LIST_STATUS_CHANGE, fetchWrmStatus),
    yield takeLatest(WrmListUpdateQueueActionsTypes.WRM_LIST_UPDATE_QUEUE, fetchWrmListUpdateQueue)
  ])
}