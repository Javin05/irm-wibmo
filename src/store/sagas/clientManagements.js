import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { ClientManagementActionsTypes, ClientManagementActions } from '../actions'
import serviceList from '../../services/serviceList'

function* fetchClientManagement(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.clienmanagement, { params })
  if (json.status === 200) {
    yield put(ClientManagementActions.saveClientManagementlistResponse(json.data))
  } else {
    yield put(ClientManagementActions.saveClientManagementlistResponse([]))
  }
}

export function* fetchAClientManagementWatcher() {
  yield all([
    yield takeLatest(ClientManagementActionsTypes.GET_CLIENTMANAGEMENT_LIST, fetchClientManagement),
  ])
}