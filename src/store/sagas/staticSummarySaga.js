import { put, takeLatest, all } from "redux-saga/effects";
import axiosInstance from "../../services";
import {
  KYCDashboardSummaryAction,
  KYCDashboardSummaryTypes,
} from "../actions";
import serviceList from "../../services/serviceList";

function* fetchKYCStaticSummary(actions) {
  const { params } = actions;
  const url = serviceList.KYCDashboardSummary + `${params}`;
  const response = yield axiosInstance.get(url, {
    // params,
  });
  if (response.status === 200) {
    yield put(
      KYCDashboardSummaryAction.KYCDashboardSummary_SUCCESS(response.data)
    );
  } else {
    yield put(KYCDashboardSummaryAction.KYCDashboardSummary_ERROR([]));
  }
}

export function* fetchKYCStaticSummaryWatcher() {
  yield all([
    yield takeLatest(
      KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_INIT,
      fetchKYCStaticSummary
    ),
  ]);
}
