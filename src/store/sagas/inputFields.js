import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { inputFieldActionTypes, inputFieldActions } from '../actions'
import serviceList from '../../services/serviceList'


function* fetchInputFieldsList(actions){
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.getInputFields, { params })
  if (json.status === 200) {
    yield put(inputFieldActions.saveInputFieldsResponse(json.data))
  } else {
    yield put(inputFieldActions.saveInputFieldsResponse([]))
  }
}

export function* fetchInputlistWatcher() {
  yield all([
    yield takeLatest(inputFieldActionTypes.GET_INPUT_FIELDS, fetchInputFieldsList),

  ])
}