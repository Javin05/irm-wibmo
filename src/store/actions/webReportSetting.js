export const webReportSettingActionsTypes = {
  GET_WEBREPROT_SEETING_LIST: 'GET_WEBREPROT_SEETING_LIST',
  SAVE_WEBREPROT_SEETING_LIST_RESPONSE: 'SAVE_WEBREPROT_SEETING_LIST_RESPONSE',
  CLEAR_WEBREPROT_SEETING_LIST: 'CLEAR_WEBREPROT_SEETING_LIST'
}

export const webReportSettingActions = {
  getwebReportSettinglist: (params) => ({
    type: webReportSettingActionsTypes.GET_WEBREPROT_SEETING_LIST,
    params
  }),
  savewebReportSettinglistResponse: (data) => ({
    type: webReportSettingActionsTypes.SAVE_WEBREPROT_SEETING_LIST_RESPONSE,
    data
  }),
  clearwebReportSettinglist: () => ({
    type: webReportSettingActionsTypes.CLEAR_WEBREPROT_SEETING_LIST
  })
}

export const webReportSettingAddTypes = {
  WEBREPROT_SEETING_POST: 'WEBREPROT_SEETING_POST',
  WEBREPROT_SEETING_POST_RESPONSE: 'WEBREPROT_SEETING_POST_RESPONSE',
  WEBREPROT_SEETING_POST_CLEAR: 'WEBREPROT_SEETING_POST_CLEAR'
}

export const webReportSettingAddAction = {
  webReportSettingAdd: (data) => {
    return {
      type: webReportSettingAddTypes.WEBREPROT_SEETING_POST,
      payload: data
    }
  },
  savewebReportSettingResponse: data => ({
    type: webReportSettingAddTypes.WEBREPROT_SEETING_POST_RESPONSE,
    data
  }),
  clearwebReportSetting: () => ({
    type: webReportSettingAddTypes.WEBREPROT_SEETING_POST_CLEAR
  })
}

export const webReportSettingGetId = {
  GET_WEBREPROT_SEETING_DETAILS: 'GET_WEBREPROT_SEETING_DETAILS',
  WEBREPROT_SEETING_DETAILS_RESPONSE: 'WEBREPROT_SEETING_DETAILS_RESPONSE',
  CLEAR_WEBREPROT_SEETING_DETAILS: 'CLEAR_WEBREPROT_SEETING_DETAILS'
}

export const webReportSettingGetIdActions = {
  getwebReportSettingIdDetails: (id) => ({
    type: webReportSettingGetId.GET_WEBREPROT_SEETING_DETAILS,
    id
  }),
  savewebReportSettingIdDetailsResponse: data => ({
    type: webReportSettingGetId.WEBREPROT_SEETING_DETAILS_RESPONSE,
    data
  }),
  clearwebReportSettingIdDetails: () => ({
    type: webReportSettingGetId.CLEAR_WEBREPROT_SEETING_DETAILS
  })
}

export const updatewebReportSettingActionsTypes = {
  UPDATE_WEBREPROT_SEETING: 'UPDATE_WEBREPROT_SEETING',
  SAVE_UPDATE_WEBREPROT_SEETING_RESPONSE: 'SAVE_UPDATE_WEBREPROT_SEETING_RESPONSE',
  CLEAR_UPDATE_WEBREPROT_SEETING: 'CLEAR_UPDATE_WEBREPROT_SEETING'
}

export const updatewebReportSettingActions = {
  updatewebReportSetting: (id, params) => {
    return {
      type: updatewebReportSettingActionsTypes.UPDATE_WEBREPROT_SEETING,
      payload:  { id, params }
    }
  },
  saveupdatewebReportSettingResponse: data => ({
    type: updatewebReportSettingActionsTypes.SAVE_UPDATE_WEBREPROT_SEETING_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearupdatewebReportSetting: () => ({
    type: updatewebReportSettingActionsTypes.CLEAR_UPDATE_WEBREPROT_SEETING
  })
}

export const webReportSettingDelete = {
  WEBREPROT_SEETING_DELETE: 'WEBREPROT_SEETING_DELETE',
  WEBREPROT_SEETING_DELETE_RESPONSE: 'WEBREPROT_SEETING_DELETE_RESPONSE',
  CLEAR_WEBREPROT_SEETING_DELETE: 'CLEAR_WEBREPROT_SEETING_DELETE'
}

export const webReportSettingDeleteActions = {
  webReportSettingDelete: (payload) => ({
    type: webReportSettingDelete.WEBREPROT_SEETING_DELETE,
    payload
  }),
  savewebReportSettingDeleteResponse: data => ({
    type: webReportSettingDelete.WEBREPROT_SEETING_DELETE_RESPONSE,
    data
  }),
  clearwebReportSettingDelete: () => ({
    type: webReportSettingDelete.CLEAR_WEBREPROT_SEETING_DELETE
  })
}