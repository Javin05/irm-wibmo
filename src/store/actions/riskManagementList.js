export const riskManagementActionsTypes = {
  GET_RISK_MANAGEMENT_LIST: 'GET_RISK_MANAGEMENT_LIST',
  SAVE_RISK_MANAGEMENT_LIST_RESPONSE: 'SAVE_RISK_MANAGEMENT_LIST_RESPONSE',
  CLEAR_RISK_MANAGEMENT_LIST: 'CLEAR_RISK_MANAGEMENT_LIST'
}

export const riskManagementActions = {
  getRiskManagementlist: (params) => ({
    type: riskManagementActionsTypes.GET_RISK_MANAGEMENT_LIST,
    params
  }),
  saveRiskManagementlistResponse: (data) => ({
    type: riskManagementActionsTypes.SAVE_RISK_MANAGEMENT_LIST_RESPONSE,
    data
  }),
  clearRiskManagementlist: () => ({
    type: riskManagementActionsTypes.CLEAR_RISK_MANAGEMENT_LIST
  })
}

export const merchantIdDetailsTypes = {
  GET_MERCHANT_ID: 'GET_MERCHANT_ID',
  MERCHANT_ID_RESPONSE: 'MERCHANT_ID_RESPONSE',
  CLEAR_MERCHANT_ID: 'CLEAR_MERCHANT_ID'
}

export const merchantIdDetailsActions = {
  getmerchantIdDetailsData: (id) => ({
    type: merchantIdDetailsTypes.GET_MERCHANT_ID,
    id
  }),
  savemerchantIdDetailsResponse: data => ({
    type: merchantIdDetailsTypes.MERCHANT_ID_RESPONSE,
    data
  }),
  clearmerchantIdData: () => ({
    type: merchantIdDetailsTypes.CLEAR_MERCHANT_ID
  })
}

export const linkAnalyticsActionsTypes = {
  GETLINK_ANALYTIS_LIST: 'GETLINK_ANALYTIS_LIST',
  SAVELINK_ANALYTIS_LIST_RESPONSE: 'SAVELINK_ANALYTIS_LIST_RESPONSE',
  CLEARLINK_ANALYTIS_LIST: 'CLEARLINK_ANALYTIS_LIST'
}

export const linkAnalyticsActions = {
  getlinkAnalyticslist: (params) => ({
    type: linkAnalyticsActionsTypes.GETLINK_ANALYTIS_LIST,
    params
  }),
  savelinkAnalyticslistResponse: (data) => ({
    type: linkAnalyticsActionsTypes.SAVELINK_ANALYTIS_LIST_RESPONSE,
    data
  }),
  clearlinkAnalyticslist: () => ({
    type: linkAnalyticsActionsTypes.CLEARLINK_ANALYTIS_LIST
  })
}

export const ExportListActionsTypes = {
  GET_EXPORT_LIST: 'GET_EXPORT_LIST',
  SAVE_EXPORT_LIST_RESPONSE: 'SAVE_EXPORT_LIST_RESPONSE',
  CLEAR_EXPORT_LIST: 'CLEAR_EXPORT_LIST'
}

export const ExportListActions = {
  getExportList: (params) => ({
    type: ExportListActionsTypes.GET_EXPORT_LIST,
    params
  }),
  saveExportListResponse: (data) => ({
    type: ExportListActionsTypes.SAVE_EXPORT_LIST_RESPONSE,
    data
  }),
  clearExportList: () => ({
    type: ExportListActionsTypes.CLEAR_EXPORT_LIST
  })
}

export const clientIdLIstActionsTypes = {
  GET_CLIENT_ID_LIST: 'GET_CLIENT_ID_LIST',
  SAVE_CLIENT_ID_LIST_RESPONSE: 'SAVE_CLIENT_ID_LIST_RESPONSE',
  CLEAR_CLIENT_ID_LIST: 'CLEAR_CLIENT_ID_LIST'
}

export const clientIdLIstActions = {
  getclientIdList: (params) => (
    {
    type: clientIdLIstActionsTypes.GET_CLIENT_ID_LIST,
    params
  }),
  saveclientIdLIstResponse: (data) => ({
    type: clientIdLIstActionsTypes.SAVE_CLIENT_ID_LIST_RESPONSE,
    data
  }),
  clearclientIdLIst: () => ({
    type: clientIdLIstActionsTypes.CLEAR_CLIENT_ID_LIST
  })
}

export const BWListActionsTypes = {
  GET_BW_LIST: 'GET_BW_LIST',
  SAVE_BW_LIST_RESPONSE: 'SAVE_BW_LIST_RESPONSE',
  CLEAR_BW_LIST: 'CLEAR_BW_LIST'
}

export const BWListActions = {
  getBWList: (params) => ({
    type: BWListActionsTypes.GET_BW_LIST,
    params
  }),
  saveBWListResponse: (data) => ({
    type: BWListActionsTypes.SAVE_BW_LIST_RESPONSE,
    data
  }),
  clearBWList: () => ({
    type: BWListActionsTypes.CLEAR_BW_LIST
  })
}

export const PlayStoreExportTypes = {
  GET_PLAYSTORE_EXPORT: 'GET_PLAYSTORE_EXPORT',
  SAVE_PLAYSTORE_EXPORT_RESPONSE: 'SAVE_PLAYSTORE_EXPORT_RESPONSE',
  CLEAR_PLAYSTORE_EXPORT: 'CLEAR_PLAYSTORE_EXPORT'
}

export const PlayStoreExportActions = {
  getPlayStoreExport: (params) => ({
    type: PlayStoreExportTypes.GET_PLAYSTORE_EXPORT,
    params
  }),
  savePlayStoreExportResponse: (data) => ({
    type: PlayStoreExportTypes.SAVE_PLAYSTORE_EXPORT_RESPONSE,
    data
  }),
  clearPlayStoreExport: () => ({
    type: PlayStoreExportTypes.CLEAR_PLAYSTORE_EXPORT
  })
}

export const PlayStoreDashboardTypes = {
  GET_PLAYSTORE_DASHBOARD: 'GET_PLAYSTORE_DASHBOARD',
  SAVE_PLAYSTORE_DASHBOARD_RESPONSE: 'SAVE_PLAYSTORE_DASHBOARD_RESPONSE',
  CLEAR_PLAYSTORE_DASHBOARD: 'CLEAR_PLAYSTORE_DASHBOARD'
}

export const PlayStoreDashboardActions = {
  getPlayStoreDashboard: (params) => ({
    type: PlayStoreDashboardTypes.GET_PLAYSTORE_DASHBOARD,
    params
  }),
  savePlayStoreDashboardResponse: (data) => ({
    type: PlayStoreDashboardTypes.SAVE_PLAYSTORE_DASHBOARD_RESPONSE,
    data
  }),
  clearPlayStoreDashboard: () => ({
    type: PlayStoreDashboardTypes.CLEAR_PLAYSTORE_DASHBOARD
  })
}

export const riskLevelActionTypes = {
  GET_RISKLEVEL_LIST: "GET_RISKLEVEL_LIST",
  SAVE_RISKLEVEL_LIST_RESPONSE: "SAVE_RISKLEVEL_LIST_RESPONSE",
  CLEAR_RISKLEVEL_LIST: "CLEAR_RISKLEVEL_LIST",
};

export const riskLevelActions = {
  getRiskLevelList: () => ({
    type: riskLevelActionTypes.GET_RISKLEVEL_LIST,
  }),
  saveRiskLevelResponse: (data) => ({
    type: riskLevelActionTypes.SAVE_RISKLEVEL_LIST_RESPONSE,
    data,
  }),
  clearRiskLevel: () => ({
    type: riskLevelActionTypes.CLEAR_RISKLEVEL_LIST,
  }),
};

export const exportFullReportActionsTypes = {
  GET_EXPORT_FULLREPORT_LIST: 'GET_EXPORT_FULLREPORT_LIST',
  SAVE_EXPORT_FULLREPORT_LIST_RESPONSE: 'SAVE_EXPORT_FULLREPORT_LIST_RESPONSE',
  CLEAR_EXPORT_FULLREPORT_LIST: 'CLEAR_EXPORT_FULLREPORT_LIST'
}

export const exportFullReportActions = {
  getExportFullReportlist: (params) => ({
    type: exportFullReportActionsTypes.GET_EXPORT_FULLREPORT_LIST,
    params
  }),
  saveExportFullReportlistResponse: (data) => ({
    type: exportFullReportActionsTypes.SAVE_EXPORT_FULLREPORT_LIST_RESPONSE,
    data
  }),
  clearExportFullReportlist: () => ({
    type: exportFullReportActionsTypes.CLEAR_EXPORT_FULLREPORT_LIST
  })
}
