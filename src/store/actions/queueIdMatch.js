export const QueueIdActionsTypes = {
  QUEUEID: 'QUEUEID',
  SAVE_QUEUEID_RESPONSE: 'SAVE_QUEUEID_RESPONSE',
  CLEAR_QUEUEID: 'CLEAR_QUEUEID'
}

export const QueueIdActions = {
  QueueId: (id, params) => {
    return {
      type: QueueIdActionsTypes.QUEUEID,
      payload:  { id, params }
    }
  },
  saveQueueIdResponse: data => ({
    type: QueueIdActionsTypes.SAVE_QUEUEID_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearQueueId: () => ({
    type: QueueIdActionsTypes.CLEAR_QUEUEID
  })
}
