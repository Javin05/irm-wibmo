export const WrmOperationManagementTypes = {
    GET_WRMOPERATIONMANAGEMENT_LIST: 'GET_WRMOPERATIONMANAGEMENT_LIST',
    SAVE_WRMOPERATIONMANAGEMENT_LIST_RESPONSE: 'SAVE_WRMOPERATIONMANAGEMENT_LIST_RESPONSE',
    CLEAR_WRMOPERATIONMANAGEMENT_LIST: 'CLEAR_WRMOPERATIONMANAGEMENT_LIST'
  }
  
  export const WrmOperationManagementActions = {
    getWrmOperationManagemnt: (params) => ({
      type: WrmOperationManagementTypes.GET_WRMOPERATIONMANAGEMENT_LIST,
      params
    }),
    saveWrmOperationManagemntResponse: (data) => ({
      type: WrmOperationManagementTypes.SAVE_WRMOPERATIONMANAGEMENT_LIST_RESPONSE,
      data
    }),
    clearWrmOperationManagemnt: () => ({
      type: WrmOperationManagementTypes.CLEAR_WRMOPERATIONMANAGEMENT_LIST
    })
  }

  export const WRMOperatorsListTypes = {
    GET_WRMOPERATOR_LIST: 'GET_GET_WRMOPERATOR_LIST_LIST',
    SAVE_WRMOPERATOR_LIST_RESPONSE: 'SAVE_GET_WRMOPERATOR_LIST_LIST_RESPONSE',
    CLEAR_WRMOPERATOR_LIST: 'CLEAR_GET_WRMOPERATOR_LIST_LIST'
  }
  
  export const WRMOperatorsListActions = {
    getWRMOperatorsList: (params) => ({
      type: WRMOperatorsListTypes.GET_WRMOPERATOR_LIST,
      params
    }),
    saveWRMOperatorsListResponse: (data) => ({
      type: WRMOperatorsListTypes.SAVE_WRMOPERATOR_LIST_RESPONSE,
      data
    }),
    clearWRMOperatorsList: () => ({
      type: WRMOperatorsListTypes.CLEAR_WRMOPERATOR_LIST
    })
  }
export const WrmOperatorActionTypes = {
  WRM_OPERATION_ACTION_INIT: " WRM_OPERATION_ACTION_INIT",
  WRM_OPERATION_ACTION_RESPONSE_SUCCESS: "WRM_OPERATION_ACTION_RESPONSE_SUCCESS",
  WRM_OPERATION_ACTION_CLEAR: "WRM_OPERATION_ACTION_CLEAR",
  WRM_OPERATION_ACTION_ERROR: "WRM_OPERATION_ACTION_ERROR",
};

export const WrmOperatorActions = {
  wrmOperatorActionInit: (params) => ({
    type: WrmOperatorActionTypes.WRM_OPERATION_ACTION_INIT,
    params,
  }),
  saveWrmOperatorActionResponse: (data) => ({
    type: WrmOperatorActionTypes.WRM_OPERATION_ACTION_RESPONSE_SUCCESS,
    data,
  }),
  clearWrmOperatorAction: () => ({
    type: WrmOperatorActionTypes.WRM_OPERATION_ACTION_CLEAR,
  }),
   errorWrmOperatorAction: () => ({
    type: WrmOperatorActionTypes. WRM_OPERATION_ACTION_ERROR,
  }),
};

export const WrmOperationManagementDetailTypes = {
    GET_WRMOPERATIONMANAGEMENT_DETAIL: 'GET_WRMOPERATIONMANAGEMENT_DETAIL',
    SAVE_WRMOPERATIONMANAGEMENT_DETAIL_RESPONSE: 'SAVE_WRMOPERATIONMANAGEMENT_DETAIL_RESPONSE',
    CLEAR_WRMOPERATIONMANAGEMENT_DETAIL: 'CLEAR_WRMOPERATIONMANAGEMENT_DETAIL'
  }
  
  export const WrmOperationManagementDetailActions = {
    getWrmOperationDetail: (id) => ({
      type: WrmOperationManagementDetailTypes.GET_WRMOPERATIONMANAGEMENT_DETAIL,
      id
    }),
    saveWrmOperationDetailResponse: (data) => ({
      type: WrmOperationManagementDetailTypes.SAVE_WRMOPERATIONMANAGEMENT_DETAIL_RESPONSE,
      data
    }),
    clearWrmOperationDetail: () => ({
      type: WrmOperationManagementDetailTypes.CLEAR_WRMOPERATIONMANAGEMENT_DETAIL
    })
  }

  export const WrmOperationManagementBackendAPIStatusTypes = {
    GET_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS: 'GET_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS',
    SAVE_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS_RESPONSE: 'SAVE_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS_RESPONSE',
    CLEAR_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS: 'CLEAR_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS'
  }
  
  export const WrmOperationManagementBackendAPIStatusActions = {
    getWrmOperationManagemntBackendAPIStatus: (id) => ({
      type: WrmOperationManagementBackendAPIStatusTypes.GET_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS,
      id
    }),
    saveWrmOperationManagemntBackendAPIStatusResponse: (data) => ({
      type: WrmOperationManagementBackendAPIStatusTypes.SAVE_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS_RESPONSE,
      data
    }),
    clearWrmOperationManagemntBackendAPIStatus: () => ({
      type: WrmOperationManagementBackendAPIStatusTypes.CLEAR_WRMOPERATIONMANAGEMENT_BACKEND_API_STATUS
    })
  }


  export const WrmReportEditTypes = {
  REQUEST: 'EDIT_WRM_REPORT_REQUEST',
  RESPONSE: 'EDIT_WRM_REPORT_RESPONSE',
  ERROR: 'EDIT_WRM_REPORT_ERROR',
  CLEAR: 'EDIT_WRM_REPORT_CLEAR'
}

export const WrmReportEditAction = {
  editWrmReportRequest: (id, params) => {
    return {
      type: WrmReportEditTypes.REQUEST,
      payload: { id, params }
    }
  },
  saveEditWrmReportResponse: (data) => {
    return {
      type: WrmReportEditTypes.RESPONSE,
      data
    }
  },
  cleareditWrmReport: () => ({
    type: WrmReportEditTypes.CLEAR
  })
}

  export const WrmOperationBackendApiAllScheduleTypes = {
  REQUEST: 'REQUEST',
  RESPONSE: 'RESPONSE',
  CLEAR: 'CLEAR'
}

export const WrmOperationBackendApiAllScheduleAction = {
  WrmApiScheduleAllRequest: (id,name) => {
    return {
      type: WrmOperationBackendApiAllScheduleTypes.REQUEST,
     id,name
    }
  },
  saveWrmApiScheduleAllResponse: (data) => {
    return {
      type: WrmOperationBackendApiAllScheduleTypes.RESPONSE,
      data
    }
  },
  clearWrmApiScheduleAll: () => ({
    type: WrmOperationBackendApiAllScheduleTypes.CLEAR
  })
}

