
export const TransactionTypes = {
  GET_TRANSACTION_LIST: 'GET_TRANSACTION_LIST',
  SAVE_TRANSACTION_LIST_RESPONSE: 'SAVE_TRANSACTION_LIST_RESPONSE',
  CLEAR_TRANSACTION_LIST: 'CLEAR_TRANSACTION_LIST'
}

export const TransactionActions = {
  getTransactionlist: (params) => ({
    type: TransactionTypes.GET_TRANSACTION_LIST,
    params
  }),
  saveTransactionlistResponse: (data) => ({
    type: TransactionTypes.SAVE_TRANSACTION_LIST_RESPONSE,
    data
  }),
  clearTransactionlist: () => ({
    type: TransactionTypes.CLEAR_TRANSACTION_LIST
  })
}

export const transactionDashboardTypes = {
    GET_TRANSACTIONDASHBOARD_DETAILS: 'GET_TRANSACTIONDASHBOARD_DETAILS',
    TRANSACTIONDASHBOARD_DETAILS_RESPONSE: 'TRANSACTIONDASHBOARD_DETAILS_RESPONSE',
    CLEAR_TRANSACTIONDASHBOARD_DETAILS: 'CLEAR_TRANSACTIONDASHBOARD_DETAILS'
  }
  
  export const TransactiondashboardActions = {
    getdashboardDetails: (id) => ({
      type: transactionDashboardTypes.GET_TRANSACTIONDASHBOARD_DETAILS,
      id
    }),
    savedashboardDetailsResponse: data => ({
      type: transactionDashboardTypes.TRANSACTIONDASHBOARD_DETAILS_RESPONSE,
      data
    }),
    cleardashboardDetails: () => ({
      type: transactionDashboardTypes.CLEAR_TRANSACTIONDASHBOARD_DETAILS
    })
  }
  
  export const TransactionAddTypes = {
    TRANSACTION_POST: 'TRANSACTION_POST',
    TRANSACTION_POST_RESPONSE: 'TRANSACTION_POST_RESPONSE',
    TRANSACTION_POST_CLEAR: 'TRANSACTION_POST_CLEAR'
  }
  
  export const TransactionAddAction = {
    TransactionAdd: (data) => {
      return {
        type: TransactionAddTypes.TRANSACTION_POST,
        payload: data
      }
    },
    saveTransactionResponse: data => ({
      type: TransactionAddTypes.TRANSACTION_POST_RESPONSE,
      data
    }),
    clearTransaction: () => ({
      type: TransactionAddTypes.TRANSACTION_POST_CLEAR
    })
  }

  export const TransactionGetByIdTypes = {
    TRANSACTION_GET_BY_ID: 'TRANSACTION_GET_BY_ID',
    TRANSACTION_GET_BY_ID_RESPONSE: 'TRANSACTION_GET_BY_ID_RESPONSE',
    TRANSACTION_GET_BY_ID_CLEAR: 'TRANSACTION_GET_BY_ID_CLEAR'
  }

  export const TransactionGetByIdActions = {
    getGetByIdDetails: (id) => ({
      type: TransactionGetByIdTypes.TRANSACTION_GET_BY_ID,
      id
    }),
    saveGetByIdDetailsResponse: data => ({
      type: TransactionGetByIdTypes.TRANSACTION_GET_BY_ID_RESPONSE,
      data
    }),
    clearGetByIdDetails: () => ({
      type: TransactionGetByIdTypes.TRANSACTION_GET_BY_ID_CLEAR
    })
  }
  
  export const dropdownTransactionTypes = {
    GET_DROPDOWN_TRANSACTION_LIST: 'GET_DROPDOWN_TRANSACTION_LIST',
    SAVE_DROPDOWN_TRANSACTION_LIST_RESPONSE: 'SAVE_DROPDOWN_TRANSACTION_LIST_RESPONSE',
    CLEAR_DROPDOWN_TRANSACTION_LIST: 'CLEAR_DROPDOWN_TRANSACTION_LIST'
  }
  
  export const DropdownTransactionActions = {
    getDropDownTransactionlist: (params) => ({
      type: dropdownTransactionTypes.GET_DROPDOWN_TRANSACTION_LIST,
      params
    }),
    saveDropDownTransactionlistResponse: (data) => ({
      type: dropdownTransactionTypes.SAVE_DROPDOWN_TRANSACTION_LIST_RESPONSE,
      data
    }),
    clearDropDownTransactionlist: () => ({
      type: dropdownTransactionTypes.CLEAR_DROPDOWN_TRANSACTION_LIST
    })
  }

  export const AVSTransactionTypes = {
    GET_AVS_TRANSACTION_LIST: 'GET_AVS_TRANSACTION_LIST',
    SAVE_AVS_TRANSACTION_LIST_RESPONSE: 'SAVE_AVS_TRANSACTION_LIST_RESPONSE',
    CLEAR_AVS_TRANSACTION_LIST: 'CLEAR_AVS_TRANSACTION_LIST'
  }
  
  export const AVSTransactionActions = {
    getAVSTransactionlist: (params) => ({
      type: AVSTransactionTypes.GET_AVS_TRANSACTION_LIST,
      params
    }),
    saveAVSTransactionlistResponse: (data) => ({
      type: AVSTransactionTypes.SAVE_AVS_TRANSACTION_LIST_RESPONSE,
      data
    }),
    clearAVSTransactionlist: () => ({
      type: AVSTransactionTypes.CLEAR_AVS_TRANSACTION_LIST
    })
  }

  export const CVVTransactionTypes = {
    GET_CVV_TRANSACTION_LIST: 'GET_CVV_TRANSACTION_LIST',
    SAVE_CVV_TRANSACTION_LIST_RESPONSE: 'SAVE_CVV_TRANSACTION_LIST_RESPONSE',
    CLEAR_CVV_TRANSACTION_LIST: 'CLEAR_CVV_TRANSACTION_LIST'
  }
  
  export const CVVTransactionActions = {
    getCVVTransactionlist: (params) => ({
      type: CVVTransactionTypes.GET_CVV_TRANSACTION_LIST,
      params
    }),
    saveCVVTransactionlistResponse: (data) => ({
      type: CVVTransactionTypes.SAVE_CVV_TRANSACTION_LIST_RESPONSE,
      data
    }),
    clearCVVTransactionlist: () => ({
      type: CVVTransactionTypes.CLEAR_CVV_TRANSACTION_LIST
    })
  }

  export const PaymentTransactionTypes = {
    GET_PAYMENY_TRANSACTION_LIST: 'GET_PAYMENY_TRANSACTION_LIST',
    SAVE_PAYMENY_TRANSACTION_LIST_RESPONSE: 'SAVE_PAYMENY_TRANSACTION_LIST_RESPONSE',
    CLEAR_PAYMENY_TRANSACTION_LIST: 'CLEAR_PAYMENY_TRANSACTION_LIST'
  }
  
  export const PaymentTransactionActions = {
    getPaymentTransactionlist: (params) => ({
      type: PaymentTransactionTypes.GET_PAYMENY_TRANSACTION_LIST,
      params
    }),
    savePaymentTransactionlistResponse: (data) => ({
      type: PaymentTransactionTypes.SAVE_PAYMENY_TRANSACTION_LIST_RESPONSE,
      data
    }),
    clearPaymentTransactionlist: () => ({
      type: PaymentTransactionTypes.CLEAR_PAYMENY_TRANSACTION_LIST
    })
  }

  export const LinkAnlyticsTransactionTypes = {
    GET_LINKANALYTICS_TRANSACTION_LIST: 'GET_LINKANALYTICS_TRANSACTION_LIST',
    SAVE_LINKANALYTICS_TRANSACTION_LIST_RESPONSE: 'SAVE_LINKANALYTICS_TRANSACTION_LIST_RESPONSE',
    CLEAR_LINKANALYTICS_TRANSACTION_LIST: 'CLEAR_LINKANALYTICS_TRANSACTION_LIST'
  }
  
  export const LinkAnlyticsTransactionActions = {
    getLinkAnlyticsTransactionlist: (params) => ({
      type: LinkAnlyticsTransactionTypes.GET_LINKANALYTICS_TRANSACTION_LIST,
      params
    }),
    saveLinkAnlyticsTransactionlistResponse: (data) => ({
      type: LinkAnlyticsTransactionTypes.SAVE_LINKANALYTICS_TRANSACTION_LIST_RESPONSE,
      data
    }),
    clearLinkAnlyticsTransactionlist: () => ({
      type: LinkAnlyticsTransactionTypes.CLEAR_LINKANALYTICS_TRANSACTION_LIST
    })
  }

  export const transactionDistanceTypes = {
    GET_TRANSACTION_DISTANCE_DETAILS: 'GET_TRANSACTION_DISTANCE_DETAILS',
    TRANSACTION_DISTANCE_DETAILS_RESPONSE: 'TRANSACTION_DISTANCE_DETAILS_RESPONSE',
    CLEAR_TRANSACTION_DISTANCE_DETAILS: 'CLEAR_TRANSACTION_DISTANCE_DETAILS'
  }
  
  export const TransactionDistanceActions = {
    getDistanceDetails: (id) => ({
      type: transactionDistanceTypes.GET_TRANSACTION_DISTANCE_DETAILS,
      id
    }),
    saveDistanceDetailsResponse: data => ({
      type: transactionDistanceTypes.TRANSACTION_DISTANCE_DETAILS_RESPONSE,
      data
    }),
    clearDistanceDetails: () => ({
      type: transactionDistanceTypes.CLEAR_TRANSACTION_DISTANCE_DETAILS
    })
  }

  export const merchantIdTypes = {
    GET_MERCHANT_ID_DETAILS: 'GET_MERCHANT_ID_DETAILS',
    MERCHANT_ID_DETAILS_RESPONSE: 'MERCHANT_ID_DETAILS_RESPONSE',
    CLEAR_MERCHANT_ID_DETAILS: 'CLEAR_MERCHANT_ID_DETAILS'
  }
  
  export const merchantIdActions = {
    getMerchantIdDetails: (id) => ({
      type: merchantIdTypes.GET_MERCHANT_ID_DETAILS,
      id
    }),
    saveMerchantIdResponse: data => ({
      type: merchantIdTypes.MERCHANT_ID_DETAILS_RESPONSE,
      data
    }),
    clearMerchantIdDetails: () => ({
      type: merchantIdTypes.CLEAR_MERCHANT_ID_DETAILS
    })
  }