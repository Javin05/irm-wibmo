export const AccountsQueueApproveActionsTypes = {
  APPROVE: 'ACCOUNTS_QUEUE_APPROVE',
  SAVE_APPROVE_RESPONSE: 'ACCOUNTS_QUEUE_SAVE_APPROVE_RESPONSE',
  CLEAR_APPROVE: 'ACCOUNTS_QUEUE_CLEAR_APPROVE'
}

export const AccountsQueueApproveActions = {
  approve: (id, params) => {
    return {
      type: AccountsQueueApproveActionsTypes.APPROVE,
      payload:  { id, params }
    }
  },
  saveApproveResponse: data => ({
    type: AccountsQueueApproveActionsTypes.SAVE_APPROVE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearApprove: () => ({
    type: AccountsQueueApproveActionsTypes.CLEAR_APPROVE
  })
}