export const riskSummaryTypes = {
  GET_RISK_SUMMARY: 'GET_RISK_SUMMARY',
  SAVE_RISK_SUMMARY_RESPONSE: 'SAVE_RISK_SUMMARY_RESPONSE',
  CLEAR_RISK_SUMMARY: 'CLEAR_RISK_SUMMARY'
}

export const riskSummaryActions = {

  getRiskSummary: (id) => (
    {
      type: riskSummaryTypes.GET_RISK_SUMMARY,
      id
    }),
  saveRiskSummaryResponse: data => ({
    type: riskSummaryTypes.SAVE_RISK_SUMMARY_RESPONSE,
    data
  }),
  clearRiskSummary: () => ({
    type: riskSummaryTypes.CLEAR_RISK_SUMMARY
  })
}

export const postRiskSummaryTypes = {
  GET_RISK_SUMMARY: 'POST_RISK_SUMMARY',
  SAVE_RISK_SUMMARY_RESPONSE: 'SAVE_POST_SUMMARY_RESPONSE',
  CLEAR_RISK_SUMMARY: 'CLEAR_POST_SUMMARY'
}

export const postRiskSummaryActions = {
  postRiskSummary: (params) => ({
    type: postRiskSummaryTypes.GET_RISK_SUMMARY,
    params
  }),
  postSaveSummaryResponse: data => ({
    type: postRiskSummaryTypes.SAVE_RISK_SUMMARY_RESPONSE,
    data
  }),
  postClearRiskSummary: () => ({
    type: postRiskSummaryTypes.CLEAR_RISK_SUMMARY
  })
}

export const GetAsigneeTypes = {
  GET_ASSINEE: 'GET_ASSINEE',
  SAVE_ASSINEE_RESPONSE: 'SAVE_GET_SUMMARY_RESPONSE',
  CLEAR_ASSINEE: 'CLEAR_GET_SUMMARY'
}

export const GetAsigneeActions = {
  GetAsignee: (params) => ({
    type: GetAsigneeTypes.GET_ASSINEE,
    params
  }),
  GetSaveSummaryResponse: data => ({
    type: GetAsigneeTypes.SAVE_ASSINEE_RESPONSE,
    data
  }),
  GetClearAsignee: () => ({
    type: GetAsigneeTypes.CLEAR_ASSINEE
  })
}

export const AsiggnActionsTypes = {
  ASSIGN: 'ASSIGN',
  SAVE_ASSIGN_RESPONSE: 'SAVE_ASSIGN_RESPONSE',
  CLEAR_ASSIGN: 'CLEAR_ASSIGN'
}

export const AsiggnActions = {
  Asiggn: (id, params) => {
    return {
      type: AsiggnActionsTypes.ASSIGN,
      payload:  { id, params }
    }
  },
  saveAsiggnResponse: data => ({
    type: AsiggnActionsTypes.SAVE_ASSIGN_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearAsiggn: () => ({
    type: AsiggnActionsTypes.CLEAR_ASSIGN
  })
}

export const riskScoreTypes = {
  GET_RISK_SCORE: 'GET_RISK_SCORE',
  SAVE_RISK_SCORE_RESPONSE: 'SAVE_RISK_SCORE_RESPONSE',
  CLEAR_RISK_SCORE: 'CLEAR_RISK_SCORE'
}

export const riskScoreActions = {
  getRiskScore: (id) => (
    {
      type: riskScoreTypes.GET_RISK_SCORE,
      id
    }),
  saveRiskScoreResponse: data => ({
    type: riskScoreTypes.SAVE_RISK_SCORE_RESPONSE,
    data
  }),
  clearRiskScore: () => ({
    type: riskScoreTypes.CLEAR_RISK_SCORE
  })
}

export const PostCategoryTypes = {
  POST_CATEGORY: 'POST_CATEGORY',
  SAVE_CATEGORY_RESPONSE: 'SAV_ASSIGNEE_RESPONSE',
  CLEAR_CATEGORY: 'CLEAR_ASSIGNEE_SUMMARY'
}

export const PostCategoryActions = {
  PostCategroy: (params) => ({
    type: PostCategoryTypes.POST_CATEGORY,
    params
  }),
  PostCategreeResponse: data => ({
    type: PostCategoryTypes.SAVE_CATEGORY_RESPONSE,
    data
  }),
  ClearPostCategroy: () => ({
    type: PostCategoryTypes.CLEAR_CATEGORY
  })
}

export const PostCategory1Types = {
  POST_CATEGORY1: 'POST_CATEGORY1',
  SAVE_CATEGORY1_RESPONSE: 'SAV_CATEGORY1_RESPONSE',
  CLEAR_CATEGORY: 'CLEAR_CATEGORY1_SUMMARY'
}

export const PostCategory1Actions = {
  PostCategory1: (params) => ({
    type: PostCategory1Types.POST_CATEGORY1,
    params
  }),
  PostCategoryResponse: data => ({
    type: PostCategory1Types.SAVE_CATEGORY1_RESPONSE,
    data
  }),
  ClearPostCategory: () => ({
    type: PostCategory1Types.CLEAR_CATEGORY
  })
}

export const GetCategoryTypes = {
  GET_CATEGORY: 'GET_CATEGORY',
  SAVE_GET_CATEGORY_RESPONSE: 'SAV_GET_ASSIGNEE_RESPONSE',
  CLEAR_GET_CATEGORY: 'CLEAR_GET_ASSIGNEE_SUMMARY'
}

export const GetCategoryActions = {
  GetCategroy: (params) => ({
    type: GetCategoryTypes.GET_CATEGORY,
    params
  }),
  GetCategroyResponse: data => ({
    type: GetCategoryTypes.SAVE_GET_CATEGORY_RESPONSE,
    data
  }),
  ClearGetCategroy: () => ({
    type: GetCategoryTypes.CLEAR_GET_CATEGORY
  })
}

export const PostPMATypes = {
  POST_PMA: 'POST_PMA',
  SAVE_PMA_RESPONSE: 'SAV_PMA_RESPONSE',
  CLEAR_PMA: 'CLEAR_PMA_SUMMARY'
}

export const PostPMAaction = {
  PostPMA: (params) => ({
    type: PostPMATypes.POST_PMA,
    params
  }),
  PostPMAResponse: data => ({
    type: PostPMATypes.SAVE_PMA_RESPONSE,
    data
  }),
  ClearPostPMA: () => ({
    type: PostPMATypes.CLEAR_PMA
  })
}