export const TagSummaryTypes = {
  TAG_SUMMARYLIST: "TAG_SUMMARYLIST",
  TAG_SUMMARYLIST_SUCCESS: "TAG_SUMMARYLIST_SUCCESS",
  TAG_SUMMARYLIST_CLEAR: "TAG_SUMMARYLIST_CLEAR",
  TAG_SUMMARYLIST_ERROR: "TAG_SUMMARYLIST_ERROR"
}

export const TagSummaryAction = {
  TagSummary: (params) => ({
    type: TagSummaryTypes.TAG_SUMMARYLIST,
    params,
  }),
  TagSummary_SUCCESS: (data) => ({
    type: TagSummaryTypes.TAG_SUMMARYLIST_SUCCESS,
    data,
  }),
  TagSummary_CLEAR: () => ({
    type: TagSummaryTypes.TAG_SUMMARYLIST_CLEAR,
  }),
  TagSummary_ERROR: () => ({
    type: TagSummaryTypes.TAG_SUMMARYLIST_ERROR,
  })
}

export const UpdatetagstatusTypes = {
  UPDATE_TAGSTATUS: "UPDATETAGSTATUS",
  UPDATETAGSTATUS_SUCCESS: "UPDATETAGSTATUS_SUCCESS",
  UPDATETAGSTATUS_CLEAR: "UPDATETAGSTATUS_CLEAR",
  UPDATETAGSTATUS_ERROR: "UPDATETAGSTATUS_ERROR"
}

export const UpdatetagstatusAction = {
  Updatetagstatus: (params) => ({
    type: UpdatetagstatusTypes.UPDATE_TAGSTATUS,
    payload: params,
  }),
  UpdatetagstatusSuccess: (data) => ({
    type: UpdatetagstatusTypes.UPDATETAGSTATUS_SUCCESS,
    data,
  }),
  UpdatetagstatusClear: () => ({
    type: UpdatetagstatusTypes.UPDATETAGSTATUS_CLEAR,
  }),
  UpdatetagstatusError: () => ({
    type: UpdatetagstatusTypes.UPDATETAGSTATUS_ERROR,
  })
}

export const TagSummaryIdTypes = {
  TAG_SUMMARY_ID: "TAG_SUMMARY_ID",
  TAG_SUMMARY_ID_SUCCESS: "TAG_SUMMARY_ID_SUCCESS",
  TAG_SUMMARY_ID_CLEAR: "TAG_SUMMARY_ID_CLEAR",
  TAG_SUMMARY_ID_ERROR: "TAG_SUMMARY_ID_ERROR"
};

export const TagSummaryIdAction = {
  TagSummaryID: (id) => ({
    type: TagSummaryIdTypes.TAG_SUMMARY_ID,
    id,
  }),
  TagSummaryIdSuccess: (data) => ({
    type: TagSummaryIdTypes.TAG_SUMMARY_ID_SUCCESS,
    data,
  }),
  TagSummaryIdClear: () => ({
    type: TagSummaryIdTypes.TAG_SUMMARY_ID_CLEAR,
  }),
  TagSummaryIdError: () => ({
    type: TagSummaryIdTypes.TAG_SUMMARY_ID_ERROR,
  })
}

export const TagSummaryUpdateTypes = {
  TAG_SUMMARY_UPDATES: "TAG_SUMMARY_UPDATES",
  TAG_SUMMARY_UPDATES_SUCCESS: "TAG_SUMMARY_UPDATES_SUCCESS",
  TAG_SUMMARY_UPDATES_CLEAR: "TAG_SUMMARY_UPDATES_CLEAR",
  TAG_SUMMARY_UPDATES_ERROR: "TAG_SUMMARY_UPDATES_ERROR"
};

export const TagSummaryUpdateAction = {
  TagSummaryUpdate: (id, params) => ({
    type: TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES,
    payload: {id, params}
  }),
  TagSummaryUpdateSuccess: (data) => ({
    type: TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES_SUCCESS,
    data,
  }),
  TagSummaryUpdateClear: () => ({
    type: TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES_CLEAR,
  }),
  TagSummaryUpdateError: () => ({
    type: TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES_ERROR,
  })
}

export const PostSendEmailTypes = {
  POSE_SEND_EMAIL: "POSE_SEND_EMAIL",
  POSE_SEND_EMAIL_SUCCESS: "POSE_SEND_EMAIL_SUCCESS",
  POSE_SEND_EMAIL_CLEAR: "POSE_SEND_EMAIL_CLEAR",
  POSE_SEND_EMAIL_ERROR: "POSE_SEND_EMAIL_ERROR"
}

export const PostSendEmailAction = {
  PostSendEmail: (params) => ({
    type: PostSendEmailTypes.POSE_SEND_EMAIL,
    payload: params,
  }),
  PostSendEmailSuccess: (data) => ({
    type: PostSendEmailTypes.POSE_SEND_EMAIL_SUCCESS,
    data,
  }),
  PostSendEmailClear: () => ({
    type: PostSendEmailTypes.POSE_SEND_EMAIL_CLEAR,
  }),
  PostSendEmailError: () => ({
    type: PostSendEmailTypes.POSE_SEND_EMAIL_ERROR,
  })
}

export const BatchSummaryExportTypes = {
  BATCH_SUMMARY_EXPORT: "BATCH_SUMMARY_EXPORT",
  BATCH_SUMMARY_EXPORT_SUCCESS: "BATCH_SUMMARY_EXPORT_SUCCESS",
  BATCH_SUMMARY_EXPORT_CLEAR: "BATCH_SUMMARY_EXPORT_CLEAR",
  BATCH_SUMMARY_EXPORT_ERROR: "BATCH_SUMMARY_EXPORT_ERROR"
}

export const BatchSummaryExportAction = {
  BatchSummaryExport: (params) => ({
    type: BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT,
    params,
  }),
  BatchSummaryExportSuccess: (data) => ({
    type: BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT_SUCCESS,
    data,
  }),
  BatchSummaryExportClear: () => ({
    type: BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT_CLEAR,
  }),
  BatchSummaryExportError: () => ({
    type: BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT_ERROR,
  })
}

export const SummaryWebExportTypes = {
  SUMMARY_WEB_EXPORT: "SUMMARY_WEB_EXPORT",
  SUMMARY_WEB_EXPORT_SUCCESS: "SUMMARY_WEB_EXPORT_SUCCESS",
  SUMMARY_WEB_EXPORT_CLEAR: "SUMMARY_WEB_EXPORT_CLEAR",
  SUMMARY_WEB_EXPORT_ERROR: "SUMMARY_WEB_EXPORT_ERROR"
}

export const SummaryWebExportAction = {
  SummaryWebExport: (params) => ({
    type: SummaryWebExportTypes.SUMMARY_WEB_EXPORT,
    params,
  }),
  SummaryWebExportSuccess: (data) => ({
    type: SummaryWebExportTypes.SUMMARY_WEB_EXPORT_SUCCESS,
    data,
  }),
  SummaryWebExportClear: () => ({
    type: SummaryWebExportTypes.SUMMARY_WEB_EXPORT_CLEAR,
  }),
  SummaryWebExportError: () => ({
    type: SummaryWebExportTypes.SUMMARY_WEB_EXPORT_ERROR,
  })
}

export const SummaryPlaytoreExportTypes = {
  SUMMARY_PLAYSTORE_EXPORT: "SUMMARY_PLAYSTORE_EXPORT",
  SUMMARY_PLAYSTORE_EXPORT_SUCCESS: "SUMMARY_PLAYSTORE_EXPORT_SUCCESS",
  SUMMARY_PLAYSTORE_EXPORT_CLEAR: "SUMMARY_PLAYSTORE_EXPORT_CLEAR",
  SUMMARY_PLAYSTORE_EXPORT_ERROR: "SUMMARY_PLAYSTORE_EXPORT_ERROR"
}

export const SummaryPlaytoreExportAction = {
  SummaryPlaytoreExport: (params) => ({
    type: SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT,
    params,
  }),
  SummaryPlaytoreExportSuccess: (data) => ({
    type: SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT_SUCCESS,
    data,
  }),
  SummaryPlaytoreExportClear: () => ({
    type: SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT_CLEAR,
  }),
  SummaryPlaytoreExportError: () => ({
    type: SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT_ERROR,
  })
}