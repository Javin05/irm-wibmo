
export const matrixActionTypes = {
  GET_MATRIX_DETAILS: 'GET_MATRIX_DETAILS',
  MATRIX_DETAILS_RESPONSE: 'MATRIX_DETAILS_RESPONSE',
  CLEAR_MATRIX_DETAILS: 'CLEAR_MATRIX_DETAILS'
}

export const matrixActions = {
  getMatrixDetails: (id) => ({
    type: matrixActionTypes.GET_MATRIX_DETAILS,
    id
  }),
  saveMatrixDetailsResponse: data => ({
    type: matrixActionTypes.MATRIX_DETAILS_RESPONSE,
    data
  }),
  clearMatrixDetails: () => ({
    type: matrixActionTypes.CLEAR_MATRIX_DETAILS
  })
}
