export const accountriskSummaryTypes = {
    GET_ACCOUNT_RISK_SUMMARY: 'GET_ACCOUNT_RISK_SUMMARY',
    SAVE_ACCOUNT_RISK_SUMMARY_RESPONSE: 'SAVE_ACCOUNT_RISK_SUMMARY_RESPONSE',
    CLEAR_ACCOUNT_RISK_SUMMARY: 'CLEAR_ACCOUNT_RISK_SUMMARY'
  }
  
  export const accountriskSummaryActions = {
  
    getaccountRiskSummary: (id) => (
      {
        type: accountriskSummaryTypes.GET_ACCOUNT_RISK_SUMMARY,
        id
      }),
    saveaccountRiskSummaryResponse: data => ({
      type: accountriskSummaryTypes.SAVE_ACCOUNT_RISK_SUMMARY_RESPONSE,
      data
    }),
    clearaccountRiskSummary: () => ({
      type: accountriskSummaryTypes.CLEAR_ACCOUNT_RISK_SUMMARY
    })
  }

  export const accountmerchantIdDetailsTypes = {
    GET_ACCOUNT_MERCHANT_ID: 'GET_ACCOUNT_MERCHANT_ID',
    ACCOUNT_MERCHANT_ID_RESPONSE: 'ACCOUNT_MERCHANT_ID_RESPONSE',
    CLEAR_ACCOUNT_MERCHANT_ID: 'CLEAR_ACCOUNT_MERCHANT_ID'
  }
  
  export const accountmerchantIdDetailsActions = {
    getaccountmerchantIdDetailsData: (id) => ({
      type: accountmerchantIdDetailsTypes.GET_ACCOUNT_MERCHANT_ID,
      id
    }),
    saveaccountmerchantIdDetailsResponse: data => ({
      type: accountmerchantIdDetailsTypes.ACCOUNT_MERCHANT_ID_RESPONSE,
      data
    }),
    clearaccountmerchantIdData: () => ({
      type: accountmerchantIdDetailsTypes.CLEAR_ACCOUNT_MERCHANT_ID
    })
  }

  export const accountdashboardGetDetailsTypes = {
    GET_ACCOUNT_DASHBOARD_DETAILS: 'GET_ACCOUNT_DASHBOARD_DETAILS',
    ACCOUNT_DASHBOARD_DETAILS_RESPONSE: 'ACCOUNT_DASHBOARD_DETAILS_RESPONSE',
    CLEAR_ACCOUNT_DASHBOARD_DETAILS: 'CLEAR_ACCOUNT_DASHBOARD_DETAILS'
  }
  
  export const accountdashboardDetailsActions = {
    getaccountdashboardDetails: (id) => ({
      type: accountdashboardGetDetailsTypes.GET_ACCOUNT_DASHBOARD_DETAILS,
      id
    }),
    saveaccountdashboardDetailsResponse: data => ({
      type: accountdashboardGetDetailsTypes.ACCOUNT_DASHBOARD_DETAILS_RESPONSE,
      data
    }),
    clearaccountdashboardDetails: () => ({
      type: accountdashboardGetDetailsTypes.CLEAR_ACCOUNT_DASHBOARD_DETAILS
    })
}
  
export const accountriskScoreTypes = {
    GET_ACCOUNT_RISK_SCORE: 'GET_ACCOUNT_RISK_SCORE',
    SAVE_ACCOUNT_RISK_SCORE_RESPONSE: 'SAVE_ACCOUNT_RISK_SCORE_RESPONSE',
    CLEAR_ACCOUNT_RISK_SCORE: 'CLEAR_ACCOUNT_RISK_SCORE'
  }
  
  export const accountriskScoreActions = {
    getaccountRiskScore: (id) => (
      {
        type: accountriskScoreTypes.GET_ACCOUNT_RISK_SCORE,
        id
      }),
    saveaccountRiskScoreResponse: data => ({
      type: accountriskScoreTypes.SAVE_ACCOUNT_RISK_SCORE_RESPONSE,
      data
    }),
    clearaccountRiskScore: () => ({
      type: accountriskScoreTypes.CLEAR_ACCOUNT_RISK_SCORE
    })
  }

  
export const accountmatrixActionTypes = {
    GET_ACCOUNT_MATRIX_DETAILS: 'GET_ACCOUNT_MATRIX_DETAILS',
    ACCOUNT_MATRIX_DETAILS_RESPONSE: 'ACCOUNT_MATRIX_DETAILS_RESPONSE',
    CLEAR_ACCOUNT_MATRIX_DETAILS: 'CLEAR_ACCOUNT_MATRIX_DETAILS'
  }
  
  export const accountmatrixActions = {
    getMatrixDetails: (id) => ({
      type: accountmatrixActionTypes.GET_ACCOUNT_MATRIX_DETAILS,
      id
    }),
    saveMatrixDetailsResponse: data => ({
      type: accountmatrixActionTypes.ACCOUNT_MATRIX_DETAILS_RESPONSE,
      data
    }),
    clearMatrixDetails: () => ({
      type: accountmatrixActionTypes.CLEAR_ACCOUNT_MATRIX_DETAILS
    })
  }

export const accountClientIdActionsTypes = {
    GET_CLIENT_ID_DETAILS: 'GET_CLIENT_ID_DETAILS',
    CLIENT_ID_DETAILS_RESPONSE: 'CLIENT_ID_DETAILS_RESPONSE',
    CLEAR_CLIENT_ID_DETAILS: 'CLEAR_CLIENT_ID_DETAILS'
  }
  
  export const accountClientIdActions = {
    getClientIdDetails: (id) => ({
      type: accountClientIdActionsTypes.GET_CLIENT_ID_DETAILS,
      id
    }),
    saveClientIdResponse: data => ({
      type: accountClientIdActionsTypes.CLIENT_ID_DETAILS_RESPONSE,
      data
    }),
    clearClientIdDetails: () => ({
      type: accountClientIdActionsTypes.CLEAR_CLIENT_ID_DETAILS
    })
  }