export const AMLqueueTypes = {
  GET_AML_LIST: 'GET_AML_LIST',
  SAVE_AML_RESPONSE: 'SAVE_AML_LIST_RESPONSE',
  CLEAR_AML_LIST: 'CLEAR_AML_LIST'
}
export const AMLqueueActions = {
  getAMLqueuelist: (params) => ({
    type: AMLqueueTypes.GET_AML_LIST,
    params
  }),
  saveAMLqueuelistResponse: (data) => ({
    type: AMLqueueTypes.SAVE_AML_RESPONSE,
    data
  }),
  clearAMLqueuelist: () => ({
    type: AMLqueueTypes.CLEAR_AML_LIST
  })
}

export const AMLPostTypes = {
  AMLPOST_LIST: 'AMLPost_LIST',
  SAVE_AMLPOST_LIST_RESPONSE: 'SAVE_AMLPost_LIST_RESPONSE',
  CLEAR_AMLPOST_LIST: 'CLEAR_AMLPost_LIST'
}

export const AMLPostActions = {
  getAMLPost: (data) => ({
    type: AMLPostTypes.AMLPOST_LIST,
    payload: data
  }),
  saveAMLPostResponse: (data) => ({
    type: AMLPostTypes.SAVE_AMLPOST_LIST_RESPONSE,
    data
  }),
  clearAMLPost: () => ({
    type: AMLPostTypes.CLEAR_AMLPOST_LIST
  })
}

export const ApproveAMLActionsTypes = {
  APPROVE_AML: 'APPROVE_AML',
  SAVE_APPROVE_AML_RESPONSE: 'SAVE_APPROVE_AML_RESPONSE',
  CLEAR_APPROVE_AML: 'CLEAR_APPROVE_AML'
}

export const ApproveAMLActions = {
  ApproveAML: (id, params) => {
    return {
      type: ApproveAMLActionsTypes.APPROVE_AML,
      payload: { id, params }
    }
  },
  saveApproveAMLResponse: data => ({
    type: ApproveAMLActionsTypes.SAVE_APPROVE_AML_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearApproveAML: () => ({
    type: ApproveAMLActionsTypes.CLEAR_APPROVE_AML
  })
}

export const AMLGetIdTypes = {
  GET_AML_DETAILS: 'GET_AML_DETAILS',
  AML_DETAILS_RESPONSE: 'AML_DETAILS_RESPONSE',
  CLEAR_AML_DETAILS: 'CLEAR_AML_DETAILS'
}

export const AMLGetIdActions = {
  getAMLIdDetails: (id) => ({
    type: AMLGetIdTypes.GET_AML_DETAILS,
    id
  }),
  saveAMLIdDetailsResponse: data => ({
    type: AMLGetIdTypes.AML_DETAILS_RESPONSE,
    data
  }),
  clearAMLIdDetails: () => ({
    type: AMLGetIdTypes.CLEAR_AML_DETAILS
  })
}

export const TransactionAMLActionsTypes = {
  TRANSACTION_AML: 'TRANSACTION_AML',
  SAVE_TRANSACTION_AML_RESPONSE: 'SAVE_TRANSACTION_AML_RESPONSE',
  CLEAR_TRANSACTION_AML: 'CLEAR_TRANSACTION_AML'
}

export const TransactionAMLActions = {
  TransactionAML: (id , params) => {
    return {
      type: TransactionAMLActionsTypes.TRANSACTION_AML,
      payload: { id, params }
    }
  },
  saveTransactionAMLResponse: data => ({
    type: TransactionAMLActionsTypes.SAVE_TRANSACTION_AML_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearTransactionAML: () => ({
    type: TransactionAMLActionsTypes.CLEAR_TRANSACTION_AML
  })
}

export const AMLCountActionsTypes = {
  COUNT_AML: 'COUNT_AML',
  SAVE_COUNT_AML_RESPONSE: 'SAVE_COUNT_AML_RESPONSE',
  CLEAR_COUNT_AML: 'CLEAR_COUNT_AML'
}

export const AMLCountActions = {
  CountAML: (params) => {
    return {
      type: AMLCountActionsTypes.COUNT_AML,
      params
    }
  },
  saveCountAMLResponse: data => ({
    type: AMLCountActionsTypes.SAVE_COUNT_AML_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearCountAML: () => ({
    type: AMLCountActionsTypes.CLEAR_COUNT_AML
  })
}

export const AMLPercentageActionsTypes = {
  PERCENTAGE_AML: 'PERCENTAGE_AML',
  SAVE_PERCENTAGE_AML_RESPONSE: 'SAVE_PERCENTAGE_AML_RESPONSE',
  CLEAR_PERCENTAGE_AML: 'CLEAR_PERCENTAGE_AML'
}

export const AMLPercentageActions = {
  PercentageAML: (id) => {
    return {
      type: AMLPercentageActionsTypes.PERCENTAGE_AML,
      id
    }
  },
  savePercentageAMLResponse: data => ({
    type: AMLPercentageActionsTypes.SAVE_PERCENTAGE_AML_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearPercentageAML: () => ({
    type: AMLPercentageActionsTypes.CLEAR_PERCENTAGE_AML
  })
}

export const AMLWMYActionsTypes = {
  WMY_AML: 'WMY_AML',
  SAVE_WMY_AML_RESPONSE: 'SAVE_WMY_AML_RESPONSE',
  CLEAR_WMY_AML: 'CLEAR_WMY_AML'
}

export const AMLWMYActions = {
  WMYAML: (id) => {
    return {
      type: AMLWMYActionsTypes.WMY_AML,
      id
    }
  },
  saveWMYAMLResponse: data => ({
    type: AMLWMYActionsTypes.SAVE_WMY_AML_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearWMYAML: () => ({
    type: AMLWMYActionsTypes.CLEAR_WMY_AML
  })
}

export const AMLAnnatationActionsTypes = {
  ANNATATION_AML: 'ANNATATION_AML',
  SAVE_ANNATATION_AML_RESPONSE: 'SAVE_ANNATATION_AML_RESPONSE',
  CLEAR_ANNATATION_AML: 'CLEAR_ANNATATION_AML'
}

export const AMLAnnatationActions = {
  AnnatationAML: (id) => {
    return {
      type: AMLAnnatationActionsTypes.ANNATATION_AML,
      id
    }
  },
  saveAnnatationAMLResponse: data => ({
    type: AMLAnnatationActionsTypes.SAVE_ANNATATION_AML_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearAnnatationAML: () => ({
    type: AMLAnnatationActionsTypes.CLEAR_ANNATATION_AML
  })
}