export const getClientActionTypes = {
  GET_FILTERS_CLIENT: 'GET_FILTERS_CLIENT',
  SAVE_FILTERS_CLIENT_RESPONSE: 'SAVE_FILTERS_CLIENT_RESPONSE',
  CLEAR_FILTERS_CLIENT: 'CLEAR_FILTERS_CLIENT',
  SET_FILTERS_FUNCTION: 'SET_FILTERS_FUNCTION',
  SET_FILTERS_PARAMS: 'SET_FILTERS_PARAMS'
}

export const clientCredFilterActions = {
  getAuthClient: (params) => ({
    type: getClientActionTypes.GET_FILTERS_CLIENT,
    params
  }),
  saveAuthClientResponse: data => ({
    type: getClientActionTypes.SAVE_FILTERS_CLIENT_RESPONSE,
    data
  }),
  clearAuthClient: () => ({
    type: getClientActionTypes.CLEAR_FILTERS_CLIENT
  }),
  setFilterFunction: (data) => ({
    type: getClientActionTypes.SET_FILTERS_FUNCTION,
    data
  }),
  setCredFilterParams: (data) => ({
    type: getClientActionTypes.SET_FILTERS_PARAMS,
    data
  })
}