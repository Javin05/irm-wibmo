export const AccountRiskCommentTypes = {
    GET_ACCOUNTRISK_COMMENT_LIST: 'GET_ACCOUNTRISK_COMMENT_LIST',
    SAVE_ACCOUNTRISK_COMMENT_LIST_RESPONSE: 'SAVE_ACCOUNTRISK_COMMENT_LIST_RESPONSE',
    CLEAR_ACCOUNTRISK_COMMENT_LIST: 'CLEAR_ACCOUNTRISK_COMMENT_LIST'
  }
  
  export const AccountRiskCommentActions = {
    getAccountRiskCommentlist: (params) => ({
      type: AccountRiskCommentTypes.GET_ACCOUNTRISK_COMMENT_LIST,
      params
    }),
    saveAccountRiskCommentlistResponse: (data) => ({
      type: AccountRiskCommentTypes.SAVE_ACCOUNTRISK_COMMENT_LIST_RESPONSE,
      data
    }),
    clearAccountRiskCommentlist: () => ({
      type: AccountRiskCommentTypes.CLEAR_ACCOUNTRISK_COMMENT_LIST
    })
  }

  export const updateAccountRiskCommentActionsTypes = {
    UPDATE_ACCOUNTRISK_COMMENT: 'UPDATE_ACCOUNTRISK_COMMENT',
    SAVE_UPDATE_ACCOUNTRISK_COMMENT_RESPONSE: 'SAVE_UPDATE_ACCOUNTRISK_COMMENT_RESPONSE',
    CLEAR_UPDATE_ACCOUNTRISK_COMMENT: 'CLEAR_UPDATE_ACCOUNTRISK_COMMENT'
  }
  
  export const updateAccountRiskCommentActions = {
    updateAccountRiskComment: (id, params) => {
      return {
        type: updateAccountRiskCommentActionsTypes.UPDATE_ACCOUNTRISK_COMMENT,
        payload: { id, params }
      }
    },
    saveupdateAccountRiskCommentResponse: data => ({
      type: updateAccountRiskCommentActionsTypes.SAVE_UPDATE_ACCOUNTRISK_COMMENT_RESPONSE,
      data
    }),
    clearupdateAccountRiskComment: () => ({
      type: updateAccountRiskCommentActionsTypes.CLEAR_UPDATE_ACCOUNTRISK_COMMENT
    })
  }

  export const AccountRiskCommentdeleteActionsTypes = {
    REQUEST: 'ACCOUNTRISKCOMMENT_DELETE_REQUEST',
    RESPONSE: 'ACCOUNTRISKCOMMENT_DELETE_RESPONSE',
    ERROR: 'ACCOUNTRISKCOMMENT_DELETE_ERROR',
    CLEAR: 'ACCOUNTRISKCOMMENT_DELETE_CLEAR'
  }
  
  export const AccountRiskCommentdeleteActions = {
    delete: (params) => ({
      type: AccountRiskCommentdeleteActionsTypes.REQUEST,
      params
    }),
    saveResponse: (data) => ({
      type: AccountRiskCommentdeleteActionsTypes.RESPONSE,
      data
    }),
    clear: () => ({
      type: AccountRiskCommentdeleteActionsTypes.CLEAR
    })
  }