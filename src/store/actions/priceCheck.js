export const PriceTypes = {
    FETCH_PRICE_POPUP_INIT: 'FETCH_PRICE_POPUP_INIT',
    FETCH_PRICE_POPUP_SUCCESS: 'FETCH_PRICE_POPUP_SUCCESS',
    FETCH_PRICE_POPUP_ERROR: 'FETCH_PRICE_POPUP_ERROR',

    PRICE_POPUP_INIT: 'PRICE_POPUP_INIT',
    PRICE_POPUP_SUCCESS: 'PRICE_POPUP_SUCCESS',
    PRICE_POPUP_ERROR: 'PRICE_POPUP_ERROR',
    PRICE_POPUP_CLEAR: 'PRICE_POPUP_CLEAR'
}
export const PriceActions = {

    fetchPopupInit: (id) => ({
        type: PriceTypes.FETCH_PRICE_POPUP_INIT,
        id
    }),
    fetchPopupSuccess: (data) => ({
        type: PriceTypes.FETCH_PRICE_POPUP_SUCCESS,
        data
    }),
    fetchPopupError: () => ({
        type: PriceTypes.FETCH_PRICE_POPUP_ERROR
    }),

    postPopupInit: (id, formData) => ({
        type: PriceTypes.PRICE_POPUP_INIT,
        id,
        formData
    }),
    postPopupSuccess: (data) => ({
        type: PriceTypes.PRICE_POPUP_SUCCESS,
        data
    }),
    postPopupError: () => ({
        type: PriceTypes.PRICE_POPUP_ERROR
    }),
    postPopupClear: () => ({
        type: PriceTypes.PRICE_POPUP_CLEAR
    })
}