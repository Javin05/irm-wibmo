export const inputFieldActionTypes = {
    GET_INPUT_FIELDS: 'GET_INPUT_FIELDS',
    SAVE_INPUT_FIELDS_RESPONSE: 'SAVE_INPUT_FIELDS_RESPONSE',
}

export const inputFieldActions = {
    getInputFields: (params) => ({
        type: inputFieldActionTypes.GET_INPUT_FIELDS,
        params
    }),

    saveInputFieldsResponse: (data) => ({
        type: inputFieldActionTypes.SAVE_INPUT_FIELDS_RESPONSE,
        data
    }),
}
