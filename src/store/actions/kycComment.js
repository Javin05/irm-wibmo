export const KYCCommentTypes = {
    GET_KYC_COMMENT_LIST: 'GET_KYC_COMMENT_LIST',
    SAVE_KYC_COMMENT_LIST_RESPONSE: 'SAVE_KYC_COMMENT_LIST_RESPONSE',
    CLEAR_KYC_COMMENT_LIST: 'CLEAR_KYC_COMMENT_LIST'
  }
  
  export const KYCCommentActions = {
    getKYCCommentlist: (params) => ({
      type: KYCCommentTypes.GET_KYC_COMMENT_LIST,
      params
    }),
    saveKYCCommentlistResponse: (data) => ({
      type: KYCCommentTypes.SAVE_KYC_COMMENT_LIST_RESPONSE,
      data
    }),
    clearKYCCommentlist: () => ({
      type: KYCCommentTypes.CLEAR_KYC_COMMENT_LIST
    })
  }

  export const updateKYCCommentActionsTypes = {
    UPDATE_KYC_COMMENT: 'UPDATE_KYC_COMMENT',
    SAVE_UPDATE_KYC_COMMENT_RESPONSE: 'SAVE_UPDATE_KYC_COMMENT_RESPONSE',
    CLEAR_UPDATE_KYC_COMMENT: 'CLEAR_UPDATE_KYC_COMMENT'
  }
  
  export const updateKYCCommentActions = {
    updateKYCComment: (id, params) => {
      return {
        type: updateKYCCommentActionsTypes.UPDATE_KYC_COMMENT,
        payload: { id, params }
      }
    },
    saveupdateKYCCommentResponse: data => ({
      type: updateKYCCommentActionsTypes.SAVE_UPDATE_KYC_COMMENT_RESPONSE,
      data
    }),
    clearupdateKYCComment: () => ({
      type: updateKYCCommentActionsTypes.CLEAR_UPDATE_KYC_COMMENT
    })
  }

  export const KYCCommentdeleteActionsTypes = {
    REQUEST: 'KYCCOMMENT_DELETE_REQUEST',
    RESPONSE: 'KYCCOMMENT_DELETE_RESPONSE',
    ERROR: 'KYCCOMMENT_DELETE_ERROR',
    CLEAR: 'KYCCOMMENT_DELETE_CLEAR'
  }
  
  export const KYCCommentdeleteActions = {
    delete: (params) => ({
      type: KYCCommentdeleteActionsTypes.REQUEST,
      params
    }),
    saveResponse: (data) => ({
      type: KYCCommentdeleteActionsTypes.RESPONSE,
      data
    }),
    clear: () => ({
      type: KYCCommentdeleteActionsTypes.CLEAR
    })
  }

  export const KYCDocumentdeleteActionsTypes = {
    REQUEST: 'KYC_DOCUMENT_DELETE_REQUEST',
    RESPONSE: 'KYC_DOCUMENT_DELETE_RESPONSE',
    ERROR: 'KYC_DOCUMENT_DELETE_ERROR',
    CLEAR: 'KYC_DOCUMENT_DELETE_CLEAR'
  }
  
  export const KYCDocumentdeleteActions = {
    delete: (params) => ({
      type: KYCDocumentdeleteActionsTypes.REQUEST,
      params
    }),
    saveResponse: (data) => ({
      type: KYCDocumentdeleteActionsTypes.RESPONSE,
      data
    }),
    clear: () => ({
      type: KYCDocumentdeleteActionsTypes.CLEAR
    })
  }
  