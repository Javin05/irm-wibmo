export const ApproveActionsTypes = {
  APPROVE: 'APPROVE',
  SAVE_APPROVE_RESPONSE: 'SAVE_APPROVE_RESPONSE',
  CLEAR_APPROVE: 'CLEAR_APPROVE'
}

export const ApproveActions = {
  approve: (id, params) => {
    return {
      type: ApproveActionsTypes.APPROVE,
      payload:  { id, params }
    }
  },
  saveApproveResponse: data => ({
    type: ApproveActionsTypes.SAVE_APPROVE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearApprove: () => ({
    type: ApproveActionsTypes.CLEAR_APPROVE
  })
}

export const ApproveTXnActionsTypes = {
  APPROVE_TXN: 'APPROVE_TXN',
  SAVE_APPROVE_TXN_RESPONSE: 'SAVE_APPROVE_TXN_RESPONSE',
  CLEAR_APPROVE_TXN: 'CLEAR_APPROVE_TXN'
}

export const ApproveTXnActions = {
  ApproveTXn: (id, params) => {
    return {
      type: ApproveTXnActionsTypes.APPROVE_TXN,
      payload:  { id, params }
    }
  },
  saveApproveTXnResponse: data => ({
    type: ApproveTXnActionsTypes.SAVE_APPROVE_TXN_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearApproveTXn: () => ({
    type: ApproveTXnActionsTypes.CLEAR_APPROVE_TXN
  })
}
