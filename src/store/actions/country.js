export const CountryActionsTypes = {
    GET_COUNTRY_LIST: 'GET_COUNTRY_LIST',
    SAVE_COUNTRY_LIST_RESPONSE: 'SAVE_COUNTRY_LIST_RESPONSE',
    CLEAR_COUNTRY_LIST: 'CLEAR_COUNTRY_LIST'
}

export const CountryActions = {
    getCountrylist: (params) => ({
        type: CountryActionsTypes.GET_COUNTRY_LIST,
        params
    }),
    saveCountrylistResponse: (data) => ({
        type: CountryActionsTypes.SAVE_COUNTRY_LIST_RESPONSE,
        data
    }),
    clearCountrylist: () => ({
        type: CountryActionsTypes.CLEAR_COUNTRY_LIST
    })
}

export const StateActionsTypes = {
    GET_STATE_LIST: 'GET_STATE_LIST',
    SAVE_STATE_LIST_RESPONSE: 'SAVE_STATE_LIST_RESPONSE',
    CLEAR_STATE_LIST: 'CLEAR_STATE_LIST'
}

export const StateActions = {
    getStatelist: (params) => ({
        type: StateActionsTypes.GET_STATE_LIST,
        params
    }),
    saveStatelistResponse: (data) => ({
        type: StateActionsTypes.SAVE_STATE_LIST_RESPONSE,
        data
    }),
    clearStatelist: () => ({
        type: StateActionsTypes.CLEAR_STATE_LIST
    })
}

export const CityActionsTypes = {
    GET_CITY_LIST: 'GET_CITY_LIST',
    SAVE_CITY_LIST_RESPONSE: 'SAVE_CITY_LIST_RESPONSE',
    CLEAR_CITY_LIST: 'CLEAR_CITY_LIST'
}

export const CityActions = {
    getCitylist: (params) => ({
        type: CityActionsTypes.GET_CITY_LIST,
        params
    }),
    saveCitylistResponse: (data) => ({
        type: CityActionsTypes.SAVE_CITY_LIST_RESPONSE,
        data
    }),
    clearCitylist: () => ({
        type: CityActionsTypes.CLEAR_CITY_LIST
    })
}

export const AreaActionsTypes = {
    GET_AREA_LIST: 'GET_AREA_LIST',
    SAVE_AREA_LIST_RESPONSE: 'SAVE_AREA_LIST_RESPONSE',
    CLEAR_AREA_LIST: 'CLEAR_AREA_LIST'
}

export const AreaActions = {
    getArealist: (params) => ({
        type: AreaActionsTypes.GET_AREA_LIST,
        params
    }),
    saveArealistResponse: (data) => ({
        type: AreaActionsTypes.SAVE_AREA_LIST_RESPONSE,
        data
    }),
    clearArealist: () => ({
        type: AreaActionsTypes.CLEAR_AREA_LIST
    })
}