export const businessOwnerActionsTypes = {
  GET_OWNER_LIST: 'GET_OWNER_LIST',
  SAVE_OWNER_LIST_RESPONSE: 'SAVE_OWNER_LIST_RESPONSE',
  CLEAR_OWNER_LIST: 'CLEAR_OWNER_LIST'
}

export const ownerActions = {
  getOwnerlist: (params) => ({
    type: businessOwnerActionsTypes.GET_OWNER_LIST,
    params
  }),
  saveOwnerlistResponse: (data) => ({
    type: businessOwnerActionsTypes.SAVE_OWNER_LIST_RESPONSE,
    data
  }),
  clearOwnerlist: () => ({
    type: businessOwnerActionsTypes.CLEAR_OWNER_LIST
  })
}

export const transactionAmountActionsTypes = {
  GET_TRANSACTION_AMOUNT_LIST: 'GET_TRANSACTION_AMOUNT_LIST',
  SAVE_TRANSACTION_LIST_RESPONSE: 'SAVE_TRANSACTION_LIST_RESPONSE',
  CLEAR_TRANSACTION_LIST: 'CLEAR_TRANSACTION_LIST'
}

export const transactionActions = {
  gettransactionlist: (params) => ({
    type: transactionAmountActionsTypes.GET_TRANSACTION_AMOUNT_LIST,
    params
  }),
  savetransactionlistResponse: (data) => ({
    type: transactionAmountActionsTypes.SAVE_TRANSACTION_LIST_RESPONSE,
    data
  }),
  cleartransactionlist: () => ({
    type: transactionAmountActionsTypes.CLEAR_TRANSACTION_LIST
  })
}

export const MonthlyActionsTypes = {
  GET_MONTH_LIST: 'GET_MONTH_LIST',
  SAVE_MONTH_LIST_RESPONSE: 'SAVE_MONTH_LIST_RESPONSE',
  CLEAR_MONTH_LIST: 'CLEAR_MONTH_LIST'
}

export const  MonthlyActions = {
  getMonthlylist: (params) => ({
    type: MonthlyActionsTypes.GET_MONTH_LIST,
    params
  }),
  saveMonthlylistResponse: (data) => ({
    type: MonthlyActionsTypes.SAVE_MONTH_LIST_RESPONSE,
    data
  }),
  clearMonthlylist: () => ({
    type: MonthlyActionsTypes.CLEAR_MONTH_LIST
  })
}

export const MccActionsTypes = {
  GET_MCC_LIST: 'GET_MCC_LIST',
  SAVE_MCC_LIST_RESPONSE: 'SAVE_MCC_LIST_RESPONSE',
  CLEAR_MCC_LIST: 'CLEAR_MCC_LIST'
}

export const  MccActions = {
  getMcclist: (params) => ({
    type: MccActionsTypes.GET_MCC_LIST,
    params
  }),
  saveMcclistResponse: (data) => ({
    type: MccActionsTypes.SAVE_MCC_LIST_RESPONSE,
    data
  }),
  clearMcclist: () => ({
    type: MccActionsTypes.CLEAR_MCC_LIST
  })
}