export const ClientExportTypes = {
    GET_CLIENT_EXPORT_LIST: 'GET_CLIENT_EXPORT_LIST',
    SAVE_CLIENT_EXPORT_LIST_RESPONSE: 'SAVE_CLIENT_EXPORT_LIST_RESPONSE',
    CLEAR_CLIENT_EXPORT_LIST: 'CLEAR_CLIENT_EXPORT_LIST'
  }
  
  export const ClientExportActions = {
    getClientExportList: (params) => ({
      type: ClientExportTypes.GET_CLIENT_EXPORT_LIST,
      params
    }),
    saveClientExportListResponse: (data) => ({
      type: ClientExportTypes.SAVE_CLIENT_EXPORT_LIST_RESPONSE,
      data
    }),
    clearClientExportList: () => ({
      type: ClientExportTypes.CLEAR_CLIENT_EXPORT_LIST
    })
  }