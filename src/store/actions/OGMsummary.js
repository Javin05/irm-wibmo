export const getOGMsummaryTypes = {
    GET_OGM_SUMMARY_LIST: 'GET_OGM_SUMMARY_LIST',
    SAVE_OGM_SUMMARY_LIST_RESPONSE: 'SAVE_OGM_SUMMARY_LIST_RESPONSE',
    CLEAR_OGM_SUMMARY_LIST: 'CLEAR_OGM_SUMMARY_LIST'
}
export const OGMSummaryActions = {
    getOGMSummarylist: (params) => ({
        type: getOGMsummaryTypes.GET_OGM_SUMMARY_LIST,
        params
    }),
    saveOGMSummarylistResponse: (data) => ({
        type: getOGMsummaryTypes.SAVE_OGM_SUMMARY_LIST_RESPONSE,
        data
    }),
    clearOGMSummarylist: () => ({
        type: getOGMsummaryTypes.CLEAR_OGM_SUMMARY_LIST
    })
}

export const OGMlinkAnalyticsTypes = {
    GET_OGM_LINKANALYTICS_LIST: 'GET_OGM_LINKANALYTICS_LIST',
    SAVE_OGM_LINKANALYTICS_LIST_RESPONSE: 'SAVE_OGM_LINKANALYTICS_LIST_RESPONSE',
    CLEAR_OGM_LINKANALYTICS_LIST: 'CLEAR_OGM_LINKANALYTICS_LIST'
}
export const OGMlinkAnalyticsActions = {
    getOGMlinkAnalyticslist: (params) => ({
        type: OGMlinkAnalyticsTypes.GET_OGM_LINKANALYTICS_LIST,
        params
    }),
    saveOGMlinkAnalyticslistResponse: (data) => ({
        type: OGMlinkAnalyticsTypes.SAVE_OGM_LINKANALYTICS_LIST_RESPONSE,
        data
    }),
    clearOGMlinkAnalyticslist: () => ({
        type: OGMlinkAnalyticsTypes.CLEAR_OGM_LINKANALYTICS_LIST
    })
}