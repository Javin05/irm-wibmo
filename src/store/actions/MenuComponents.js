export const getMenuCompActionsTypes = {
  REQUEST: 'GET_MENU_COMP_REQUEST',
  RESPONSE: 'GET_MENU_COMP_RESPONSE',
  ERROR: 'GET_MENU_COMP_ERROR',
  CLEAR: 'GET_MENU_COMP_CLEAR'
}

export const getMenuCompActions = {
  get: (params) => ({
    type: getMenuCompActionsTypes.REQUEST,
    params
  }),
  saveResponse: (data) => ({
    type: getMenuCompActionsTypes.RESPONSE,
    data
  }),
  clear: () => ({
    type: getMenuCompActionsTypes.CLEAR
  })
}