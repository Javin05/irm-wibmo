export const FraudTypes = {
    FRAUD_STATUS_INIT: 'FRAUD_STATUS_INIT',
    FRAUD_STATUS_SUCCESS: 'FRAUD_STATUS_SUCCESS',
    FRAUD_STATUS_ERROR: 'FRAUD_STATUS_ERROR',
    FRAUD_STATUS_CLEAR: 'FRAUD_STATUS_CLEAR'
}
export const FraudActions = {
    ChangeFruadStatusInit: (formData) => ({
        type: FraudTypes.FRAUD_STATUS_INIT,
        formData
    }),
    ChangeFruadStatusInitSuccess: (data) => ({
        type: FraudTypes.FRAUD_STATUS_SUCCESS,
        data
    }),
    ChangeFruadStatusInitError: () => ({
        type: FraudTypes.FRAUD_STATUS_ERROR
    }),
    ChangeFruadStatusInitClear: () => ({
        type: FraudTypes.FRAUD_STATUS_CLEAR
    })
}