export const MerchantLoginTypes = {
  MERCHANT_LOGIN: 'MERCHANT_LOGIN',
  MERCHANT_LOGIN_RESPONSE: 'MERCHANT_LOGIN_RESPONSE',
  MERCHANT_CLEAR_LOGIN: 'MERCHANT_CLEAR_LOGIN'
}

export const MerchantLoginAction = {
  login: (data) => {
    return {
      type: MerchantLoginTypes.MERCHANT_LOGIN,
      payload: data
    }
  },
  saveMerchantLoginResponse: data => ({
    type: MerchantLoginTypes.MERCHANT_LOGIN_RESPONSE,
    data
  }),
  clearMerchantLogin: () => ({
    type: MerchantLoginTypes.MERCHANT_CLEAR_LOGIN
  })
}
