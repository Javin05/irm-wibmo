export const saveClientIdTypes = {
  SAVE: 'SAVE_CLIENT_ID',
  CLEAR: 'CLEAR_CLIENT_Id'
}

export const getClientIdActions = {
  save: data => ({
    type: saveClientIdTypes.SAVE,
    data
  }),
  clear: () => ({
    type: saveClientIdTypes.CLEAR
  })
}

export const clientActionsTypes = {
  GET_CLIENT: 'GET_CLIENT',
  SAVE_CLIENT_RESPONSE: 'SAVE_CLIENT_RESPONSE',
  CLEAR_CLIENT: 'CLEAR_CLIENT'
}

export const clientActions = {
  getClient: (params) => ({
    type: clientActionsTypes.GET_CLIENT,
    params
  }),
  saveclientResponse: data => ({
    type: clientActionsTypes.SAVE_CLIENT_RESPONSE,
    data
  }),
  clearClient: () => ({
    type: clientActionsTypes.CLEAR_CLIENT
  })
}

export const usersRoleTypes = {
  REQUEST: 'GET_USER_ROLES',
  RESPONSE: 'SAVE_USER_ROLE_RESPONSE',
  CLEAR: 'CLEAR_USER_ROLE'
}

export const userRoleActions = {
  getUserRole: (params) => ({
    type: usersRoleTypes.REQUEST,
    params
  }),
  saveUserRoleResponse: data => ({
    type: usersRoleTypes.RESPONSE,
    data
  }),
  clearUserRole: () => ({
    type: usersRoleTypes.CLEAR
  })
}

export const industryTypes = {
  REQUEST: 'GET_INDUSTRY',
  RESPONSE: 'SAVE_INDUSTRY_RESPONSE',
  CLEAR: 'CLEAR_INDUSTRY'
}

export const industryActions = {
  getIndustry: (params) => ({
    type: industryTypes.REQUEST,
    params
  }),
  saveIndustryResponse: data => ({
    type: industryTypes.RESPONSE,
    data
  }),
  clearIndustry: () => ({
    type: industryTypes.CLEAR
  })
}

export const RiskweightageTypes = {
  REQUEST: 'GET_RISKWEIGHTAGE',
  RESPONSE: 'SAVE_RISKWEIGHTAGE_RESPONSE',
  CLEAR: 'CLEAR_RISKWEIGHTAGE'
}

export const RiskweightageActions = {
  getRiskweightage: (params) => ({
    type: RiskweightageTypes.REQUEST,
    params
  }),
  saveRiskweightageResponse: data => ({
    type: RiskweightageTypes.RESPONSE,
    data
  }),
  clearRiskweightage: () => ({
    type: RiskweightageTypes.CLEAR
  })
}

export const RiskweightagePostTypes = {
  REQUEST: 'RISKWEIGHTAGE_POST',
  RESPONSE: 'SAVE_RISKWEIGHTAGE_POST_RESPONSE',
  CLEAR: 'CLEAR_RISKWEIGHTAGE_POST'
}

export const RiskweightagePostActions = {
  getRiskweightagePost: (params) => ({
    type: RiskweightagePostTypes.REQUEST,
    params
  }),
  saveRiskweightagePostResponse: data => ({
    type: RiskweightagePostTypes.RESPONSE,
    data
  }),
  clearRiskweightagePost: () => ({
    type: RiskweightagePostTypes.CLEAR
  })
}

export const RiskweightageEditTypes = {
  REQUEST: 'RISKWEIGHTAGE_EDIT',
  RESPONSE: 'SAVE_RISKWEIGHTAGE_EDIT_RESPONSE',
  CLEAR: 'CLEAR_RISKWEIGHTAGE_EDIT'
}

export const RiskweightageEditActions = {
  getRiskweightageEdit: (params) => ({
    type: RiskweightageEditTypes.REQUEST,
    params
  }),
  saveRiskweightageEditResponse: data => ({
    type: RiskweightageEditTypes.RESPONSE,
    data
  }),
  clearRiskweightageEdit: () => ({
    type: RiskweightageEditTypes.CLEAR
  })
}

export const RiskweightageUpdateTypes = {
  REQUEST: 'RISKWEIGHTAGE_UPDATE',
  RESPONSE: 'SAVE_RISKWEIGHTAGE_UPDATE_RESPONSE',
  CLEAR: 'CLEAR_RISKWEIGHTAGE_UPDATE'
}

export const RiskweightageUpdateActions = {
  getRiskweightageUpdate: (id, params) => ({
    type: RiskweightageUpdateTypes.REQUEST,
    payload: { id, params }
  }),
  saveRiskweightageUpdateResponse: data => ({
    type: RiskweightageUpdateTypes.RESPONSE,
    data
  }),
  clearRiskweightageUpdate: () => ({
    type: RiskweightageUpdateTypes.CLEAR
  })
}

export const RiskweightageDeleteTypes = {
  REQUEST: 'RISKWEIGHTAGE_DELETE',
  RESPONSE: 'SAVE_RISKWEIGHTAGE_DELETE_RESPONSE',
  CLEAR: 'CLEAR_RISKWEIGHTAGE_DELETE'
}

export const RiskweightageDeleteActions = {
  getRiskweightageDelete: (params) => ({
    type: RiskweightageDeleteTypes.REQUEST,
    params
  }),
  saveRiskweightageDeleteResponse: data => ({
    type: RiskweightageDeleteTypes.RESPONSE,
    data
  }),
  clearRiskweightageDelete: () => ({
    type: RiskweightageDeleteTypes.CLEAR
  })
}

export const RiskyDomainTypes = {
  REQUEST: 'GET_RISKYDOMAIN',
  RESPONSE: 'SAVE_RISKYDOMAIN_RESPONSE',
  CLEAR: 'CLEAR_RISKYDOMAIN'
}

export const RiskyDomainActions = {
  getRiskyDomain: (params) => ({
    type: RiskyDomainTypes.REQUEST,
    params
  }),
  saveRiskyDomainResponse: data => ({
    type: RiskyDomainTypes.RESPONSE,
    data
  }),
  clearRiskyDomain: () => ({
    type: RiskyDomainTypes.CLEAR
  })
}

export const RiskyDomainPostTypes = {
  REQUEST: 'RISKDOMAIN_POST',
  RESPONSE: 'SAVE_RISKDOMAIN_POST_RESPONSE',
  CLEAR: 'CLEAR_RISKDOMAIN_POST'
}

export const RiskyDomainPostActions = {
  postRiskyDomain: (params) => ({
    type: RiskyDomainPostTypes.REQUEST,
    params
  }),
  saveRiskyDomainPostResponse: data => ({
    type: RiskyDomainPostTypes.RESPONSE,
    data
  }),
  clearRiskyPostDomain: () => ({
    type: RiskyDomainPostTypes.CLEAR
  })
}

export const RiskyDomainEditTypes = {
  REQUEST: 'RISKDOMAIN_EDIT',
  RESPONSE: 'SAVE_RISKDOMAIN_EDIT_RESPONSE',
  CLEAR: 'CLEAR_RISKDOMAIN_EDIT'
}

export const RiskyDomainEditActions = {
  EditRiskyDomain: (params) => ({
    type: RiskyDomainEditTypes.REQUEST,
    params
  }),
  saveRiskyDomainEditResponse: data => ({
    type: RiskyDomainEditTypes.RESPONSE,
    data
  }),
  clearRiskyEditDomain: () => ({
    type: RiskyDomainEditTypes.CLEAR
  })
}

export const RiskyDomainUpdateTypes = {
  REQUEST: 'RISKDOMAIN_UPDATE',
  RESPONSE: 'SAVE_RISKDOMAIN_UPDATE_RESPONSE',
  CLEAR: 'CLEAR_RISKDOMAIN_UPDATE'
}

export const RiskyDomainUpdateActions = {
  UpdateRiskyDomain: (id, params) => ({
    type: RiskyDomainUpdateTypes.REQUEST,
    payload: {id, params}
  }),
  saveRiskyDomainUpdateResponse: data => ({
    type: RiskyDomainUpdateTypes.RESPONSE,
    data
  }),
  clearRiskyUpdateDomain: () => ({
    type: RiskyDomainUpdateTypes.CLEAR
  })
}

export const RiskyDomainDeleteTypes = {
  REQUEST: 'RISKDOMAIN_DELETE',
  RESPONSE: 'SAVE_RISKDOMAIN_DELETE_RESPONSE',
  CLEAR: 'CLEAR_RISKDOMAIN_DELETE'
}

export const RiskyDomainDeleteActions = {
  DeleteRiskyDomain: (params) => ({
    type: RiskyDomainDeleteTypes.REQUEST,
    params
  }),
  saveRiskyDomainDeleteResponse: data => ({
    type: RiskyDomainDeleteTypes.RESPONSE,
    data
  }),
  clearRiskyDeleteDomain: () => ({
    type: RiskyDomainDeleteTypes.CLEAR
  })
}

export const GroupScoreWeightAgeTypes = {
  REQUEST: 'GET_GROUPSCORE_WEIGHTAGE',
  RESPONSE: 'SAVE_GROUPSCORE_WEIGHTAGE_RESPONSE',
  CLEAR: 'CLEAR_GROUPSCORE_WEIGHTAGE'
}

export const GroupScoreWeightAgeActions = {
  getGroupScoreWeightAge: (params) => ({
    type: GroupScoreWeightAgeTypes.REQUEST,
    params
  }),
  saveGroupScoreWeightAgeResponse: data => ({
    type: GroupScoreWeightAgeTypes.RESPONSE,
    data
  }),
  clearGroupScoreWeightAge: () => ({
    type: GroupScoreWeightAgeTypes.CLEAR
  })
}

export const GroupScoreWeightAgePostTypes = {
  REQUEST: 'GROUPSCORE_WEIGHTAGEPOST',
  RESPONSE: 'SAVE_GROUPSCORE_WEIGHTAGEPOST_RESPONSE',
  CLEAR: 'CLEAR_GROUPSCORE_WEIGHTAGEPOST'
}

export const GroupScoreWeightAgePostActions = {
  postGroupScoreWeightAge: (params) => ({
    type: GroupScoreWeightAgePostTypes.REQUEST,
    params
  }),
  saveGroupScoreWeightAgePostResponse: data => ({
    type: GroupScoreWeightAgePostTypes.RESPONSE,
    data
  }),
  clearGroupScoreWeightAgePost: () => ({
    type: GroupScoreWeightAgePostTypes.CLEAR
  })
}

export const GroupScoreWeightAgeEditTypes = {
  REQUEST: 'GROUPSCORE_WEIGHTAGEEDIT',
  RESPONSE: 'SAVE_GROUPSCORE_WEIGHTAGEEDIT_RESPONSE',
  CLEAR: 'CLEAR_GROUPSCORE_WEIGHTAGEEDIT'
}

export const GroupScoreWeightAgeEditActions = {
  EditGroupScoreWeightAge: (params) => ({
    type: GroupScoreWeightAgeEditTypes.REQUEST,
    params
  }),
  saveGroupScoreWeightAgeEditResponse: data => ({
    type: GroupScoreWeightAgeEditTypes.RESPONSE,
    data
  }),
  clearGroupScoreWeightAgeEdit: () => ({
    type: GroupScoreWeightAgeEditTypes.CLEAR
  })
}

export const GroupScoreWeightAgeUpdateTypes = {
  REQUEST: 'GROUPSCORE_WEIGHTAGEUPDATE',
  RESPONSE: 'SAVE_GROUPSCORE_WEIGHTAGEUPDATE_RESPONSE',
  CLEAR: 'CLEAR_GROUPSCORE_WEIGHTAGEUPDATE'
}

export const GroupScoreWeightAgeUpdateActions = {
  UpdateGroupScoreWeightAge: (id, params) => ({
    type: GroupScoreWeightAgeUpdateTypes.REQUEST,
    payload: {id, params}
  }),
  saveGroupScoreWeightAgeUpdateResponse: data => ({
    type: GroupScoreWeightAgeUpdateTypes.RESPONSE,
    data
  }),
  clearGroupScoreWeightAgeUpdate: () => ({
    type: GroupScoreWeightAgeUpdateTypes.CLEAR
  })
}

export const GroupScoreWeightAgeDeleteTypes = {
  REQUEST: 'GROUPSCORE_WEIGHTAGEDELETE',
  RESPONSE: 'SAVE_GROUPSCORE_WEIGHTAGEDELETE_RESPONSE',
  CLEAR: 'CLEAR_GROUPSCORE_WEIGHTAGEDELETE'
}

export const GroupScoreWeightAgeDeleteActions = {
  DeleteGroupScoreWeightAge: (params) => ({
    type: GroupScoreWeightAgeDeleteTypes.REQUEST,
    params
  }),
  saveGroupScoreWeightAgeDeleteResponse: data => ({
    type: GroupScoreWeightAgeDeleteTypes.RESPONSE,
    data
  }),
  clearGroupScoreWeightAgeDelete: () => ({
    type: GroupScoreWeightAgeDeleteTypes.CLEAR
  })
}




export const RiskScoreWeightAgeTypes = {
  REQUEST: 'GET_RISKSCORE_WEIGHTAGE',
  RESPONSE: 'SAVE_RISKSCORE_WEIGHTAGE_RESPONSE',
  CLEAR: 'CLEAR_RISKSCORE_WEIGHTAGE'
}

export const RiskScoreWeightAgeActions = {
  getRiskScoreWeightAge: (params) => ({
    type: RiskScoreWeightAgeTypes.REQUEST,
    params
  }),
  saveRiskScoreWeightAgeResponse: data => ({
    type: RiskScoreWeightAgeTypes.RESPONSE,
    data
  }),
  clearRiskScoreWeightAge: () => ({
    type: RiskScoreWeightAgeTypes.CLEAR
  })
}

export const RiskScoreWeightAgePostTypes = {
  REQUEST: 'RISKSCORE_WEIGHTAGEPOST',
  RESPONSE: 'SAVE_RISKSCORE_WEIGHTAGEPOST_RESPONSE',
  CLEAR: 'CLEAR_RISKSCORE_WEIGHTAGEPOST'
}

export const RiskScoreWeightAgePostActions = {
  postRiskScoreWeightAge: (params) => ({
    type: RiskScoreWeightAgePostTypes.REQUEST,
    params
  }),
  saveRiskScoreWeightAgePostResponse: data => ({
    type: RiskScoreWeightAgePostTypes.RESPONSE,
    data
  }),
  clearRiskScoreWeightAgePost: () => ({
    type: RiskScoreWeightAgePostTypes.CLEAR
  })
}

export const RiskScoreWeightAgeEditTypes = {
  REQUEST: 'RISKSCORE_WEIGHTAGEEDIT',
  RESPONSE: 'SAVE_RISKSCORE_WEIGHTAGEEDIT_RESPONSE',
  CLEAR: 'CLEAR_RISKSCORE_WEIGHTAGEEDIT'
}

export const RiskScoreWeightAgeEditActions = {
  EditRiskScoreWeightAge: (params) => ({
    type: RiskScoreWeightAgeEditTypes.REQUEST,
    params
  }),
  saveRiskScoreWeightAgeEditResponse: data => ({
    type: RiskScoreWeightAgeEditTypes.RESPONSE,
    data
  }),
  clearRiskScoreWeightAgeEdit: () => ({
    type: RiskScoreWeightAgeEditTypes.CLEAR
  })
}

export const RiskScoreWeightAgeUpdateTypes = {
  REQUEST: 'RISKSCORE_WEIGHTAGEUPDATE',
  RESPONSE: 'SAVE_RISKSCORE_WEIGHTAGEUPDATE_RESPONSE',
  CLEAR: 'CLEAR_RISKSCORE_WEIGHTAGEUPDATE'
}

export const RiskScoreWeightAgeUpdateActions = {
  UpdateRiskScoreWeightAge: (id, params) => ({
    type: RiskScoreWeightAgeUpdateTypes.REQUEST,
    payload: {id, params}
  }),
  saveRiskScoreWeightAgeUpdateResponse: data => ({
    type: RiskScoreWeightAgeUpdateTypes.RESPONSE,
    data
  }),
  clearRiskScoreWeightAgeUpdate: () => ({
    type: RiskScoreWeightAgeUpdateTypes.CLEAR
  })
}

export const RiskScoreWeightAgeDeleteTypes = {
  REQUEST: 'RISKSCORE_WEIGHTAGEDELETE',
  RESPONSE: 'SAVE_RISKSCORE_WEIGHTAGEDELETE_RESPONSE',
  CLEAR: 'CLEAR_RISKSCORE_WEIGHTAGEDELETE'
}

export const RiskScoreWeightAgeDeleteActions = {
  DeleteRiskScoreWeightAge: (params) => ({
    type: RiskScoreWeightAgeDeleteTypes.REQUEST,
    params
  }),
  saveRiskScoreWeightAgeDeleteResponse: data => ({
    type: RiskScoreWeightAgeDeleteTypes.RESPONSE,
    data
  }),
  clearRiskScoreWeightAgeDelete: () => ({
    type: RiskScoreWeightAgeDeleteTypes.CLEAR
  })
}

export const GroupIdDrpdnTypes = {
  REQUEST: 'GET_GROUP_ID_DRPDN',
  RESPONSE: 'SAVE_GROUP_ID_DRPDN_RESPONSE',
  CLEAR: 'CLEAR_GROUP_ID_DRPDN'
}

export const GroupIdDrpdnActions = {
  getGroupIdDrpdn: (params) => ({
    type: GroupIdDrpdnTypes.REQUEST,
    params
  }),
  saveGroupIdDrpdnResponse: data => ({
    type: GroupIdDrpdnTypes.RESPONSE,
    data
  }),
  clearGroupIdDrpdn: () => ({
    type: GroupIdDrpdnTypes.CLEAR
  })
}

export const ReportDrpdnTypes = {
  REQUEST: 'GET_REPORT_DRPDN',
  RESPONSE: 'SAVE_REPORT_DRPDN_RESPONSE',
  CLEAR: 'CLEAR_REPORT_DRPDN'
}

export const ReportDrpdnActions = {
  getReportDrpdn: (params) => ({
    type: ReportDrpdnTypes.REQUEST,
    params
  }),
  saveReportDrpdnResponse: data => ({
    type: ReportDrpdnTypes.RESPONSE,
    data
  }),
  clearReportDrpdn: () => ({
    type: ReportDrpdnTypes.CLEAR
  })
}

export const ExportWRMEditTypes = {
  REQUEST: 'EXPORTWRM_EDIT',
  RESPONSE: 'SAVE_EXPORTWRM__EDIT_RESPONSE',
  CLEAR: 'CLEAR_EXPORTWRM__EDIT'
}

export const ExportWRMEditActions = {
  getExportWRMEdit: (id,data) => ({
    type: ExportWRMEditTypes.REQUEST,
    id,
    data
  }),
  saveExportWRMEditResponse: data => ({
    type: ExportWRMEditTypes.RESPONSE,
    data
  }),
  clearExportWRMEdit: () => ({
    type: ExportWRMEditTypes.CLEAR
  })
}

export const ExportWRMTypes = {
  REQUEST: 'EXPORTWRM_GET',
  RESPONSE: 'SAVE_EXPORTWRM__GET_RESPONSE',
  CLEAR: 'CLEAR_EXPORTWRM__GET'
}

export const ExportWRMActions = {
  getExportWRM: (id,query) => ({
    type: ExportWRMTypes.REQUEST,
    id,
    query
  }),
  saveExportWRMResponse: data => ({
    type: ExportWRMTypes.RESPONSE,
    data
  }),
  clearExportWRM: () => ({
    type: ExportWRMTypes.CLEAR
  })
}

export const ExportPlaystoreEditTypes = {
  REQUEST: 'EXPORT_PLAYSTORE_EDIT',
  RESPONSE: 'SAVE_EXPORT_PLAYSTORE__EDIT_RESPONSE',
  CLEAR: 'CLEAR_EXPORT_PLAYSTORE__EDIT'
}

export const ExportPlaystoreEditActions = {
  getExportPlaystoreEdit: (id,data) => ({
    type: ExportPlaystoreEditTypes.REQUEST,
    id,
    data
  }),
  saveExportPlaystoreEditResponse: data => ({
    type: ExportPlaystoreEditTypes.RESPONSE,
    data
  }),
  clearExportPlaystoreEdit: () => ({
    type: ExportPlaystoreEditTypes.CLEAR
  })
}
export const ExportPlaystoreTypes = {
  REQUEST: 'EXPORT_PLAYSTORE_GET',
  RESPONSE: 'SAVE_EXPORT_PLAYSTORE__GET_RESPONSE',
  CLEAR: 'CLEAR_EXPORT_PLAYSTORE__GET'
}

export const ExportPlaystoreActions = {
  getExportPlaystore: (id,query) => ({
    type: ExportPlaystoreTypes.REQUEST,
    id,
    query
  }),
  saveExportPlaystoreResponse: data => ({
    type: ExportPlaystoreTypes.RESPONSE,
    data
  }),
  clearExportPlaystore: () => ({
    type: ExportPlaystoreTypes.CLEAR
  })
}

export const CloneScoreTypes = {
  REQUEST: 'CLONE_SCORE_GET',
  RESPONSE: 'SAVE_CLONE_SCORE__GET_RESPONSE',
  CLEAR: 'CLEAR_CLONE_SCORE__GET'
}

export const CloneScoreActions = {
  getCloneScoreRequest: (params) => ({
    type: CloneScoreTypes.REQUEST,
    params,
  }),
  saveCloneScoreResponse: data => ({
    type: CloneScoreTypes.RESPONSE,
    data
  }),
  clearCloneScore: () => ({
    type: CloneScoreTypes.CLEAR
  })
}