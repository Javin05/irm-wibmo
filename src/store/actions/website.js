export const WebsiteActionsTypes = {
  GET_WEBSITE_LIST: 'GET_WEBSITE_LIST',
  SAVE_WEBSITE_LIST_RESPONSE: 'SAVE_WEBSITE_LIST_RESPONSE',
  CLEAR_WEBSITE_LIST: 'CLEAR_WEBSITE_LIST'
}

export const WebsiteActions = {
  getWebsitelist: (params) => ({
    type: WebsiteActionsTypes.GET_WEBSITE_LIST,
    params
  }),
  saveWebsitelistResponse: (data) => ({
    type: WebsiteActionsTypes.SAVE_WEBSITE_LIST_RESPONSE,
    data
  }),
  clearWebsitelist: () => ({
    type: WebsiteActionsTypes.CLEAR_WEBSITE_LIST
  })
}
