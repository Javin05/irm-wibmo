export const WhiteListTypes = {
    FETCH_WHITELIST_INIT: 'FETCH_WHITELIST_INIT',
    FETCH_WHITELIST_SUCCESS: 'FETCH_WHITELIST_SUCCESS',
    FETCH_WHITELIST_ERROR: 'FETCH_WHITELIST_ERROR',

    FETCH_WHITELISTTYPE_INIT: 'FETCH_WHITELISTTYPE_INIT',
    FETCH_WHITELISTTYPE_SUCCESS: 'FETCH_WHITELISTTYPE_SUCCESS',
    FETCH_WHITELISTTYPE_ERROR: 'FETCH_WHITELISTTYPE_ERROR',

    CREATE_WHITELISTTYPE_INIT: 'CREATE_WHITELISTTYPE_INIT',
    CREATE_WHITELISTTYPE_SUCCESS: 'CREATE_WHITELISTTYPE_SUCCESS',
    CREATE_WHITELISTTYPE_ERROR: 'CREATE_WHITELISTTYPE_ERROR',

    UPDATE_WHITELISTTYPE_INIT: 'UPDATE_WHITELISTTYPE_INIT',
    UPDATE_WHITELISTTYPE_SUCCESS: 'UPDATE_WHITELISTTYPE_SUCCESS',
    UPDATE_WHITELISTTYPE_ERROR: 'UPDATE_WHITELISTTYPE_ERROR',

    DELETE_WHITELISTTYPE_INIT: 'DELETE_WHITELISTTYPE_INIT',
    DELETE_WHITELISTTYPE_SUCCESS: 'DELETE_WHITELISTTYPE_SUCCESS',
    DELETE_WHITELISTTYPE_ERROR: 'DELETE_WHITELISTTYPE_ERROR',
}
export const WhiteListActions = {

    fetchWhitelist: (params) => ({
        type: WhiteListTypes.FETCH_WHITELIST_INIT,
        params
    }),
    fetchWhitelistSuccess: (data) => ({
        type: WhiteListTypes.FETCH_WHITELIST_SUCCESS,
        data
    }),
    fetchWhitelistError: () => ({
        type: WhiteListTypes.FETCH_WHITELIST_ERROR
    }),

    fetchWhitelistType: (params) => ({
        type: WhiteListTypes.FETCH_WHITELISTTYPE_INIT,
        params
    }),
    fetchWhitelistTypeSuccess: (data) => ({
        type: WhiteListTypes.FETCH_WHITELISTTYPE_SUCCESS,
        data
    }),
    fetchWhitelisttypeError: () => ({
        type: WhiteListTypes.FETCH_WHITELISTTYPE_ERROR
    }),

    createWhitelistType: (formData) => ({
        type: WhiteListTypes.CREATE_WHITELISTTYPE_INIT,
        formData
    }),
    createWhitelistTypeSuccess: (data) => ({
        type: WhiteListTypes.CREATE_WHITELISTTYPE_SUCCESS,
        data
    }),
    createWhitelistTypeError: () => ({
        type: WhiteListTypes.CREATE_WHITELISTTYPE_ERROR
    }),

    updateWhitelistType: (id, formData) => ({
        type: WhiteListTypes.UPDATE_WHITELISTTYPE_INIT,
        id,
        formData
    }),
    updateWhitelistTypeSuccess: (data) => ({
        type: WhiteListTypes.UPDATE_WHITELISTTYPE_SUCCESS,
        data
    }),
    updateWhitelistTypeError: () => ({
        type: WhiteListTypes.UPDATE_WHITELISTTYPE_ERROR
    }),

    deleteWhitelistType: (id) => ({
        type: WhiteListTypes.DELETE_WHITELISTTYPE_INIT,
        id
    }),
    deleteWhitelistTypeSuccess: (data) => ({
        type: WhiteListTypes.DELETE_WHITELISTTYPE_SUCCESS,
        data
    }),
    deleteWhitelistTypeError: () => ({
        type: WhiteListTypes.DELETE_WHITELISTTYPE_ERROR
    }),
}

export const WhiteListUploadTypes = {
    POST_WHITELISTUPLOAD_LIST: 'POST_WHITELISTUPLOAD_LIST',
    SAVE_WHITELISTUPLOAD_LIST_RESPONSE: 'SAVE_WHITELISTUPLOAD_LIST_RESPONSE',
    CLEAR_WHITELISTUPLOAD_LIST: 'CLEAR_WHITELISTUPLOAD_LIST'
  }

export const whiteListUploadActions = {
    postwhiteListUpload: (data) => ({
      type: WhiteListUploadTypes.POST_WHITELISTUPLOAD_LIST,
      payload: data
    }),
    savewhiteListUploadResponse: (data) => ({
      type: WhiteListUploadTypes.SAVE_WHITELISTUPLOAD_LIST_RESPONSE,
      data
    }),
    clearwhiteListUpload: () => ({
      type: WhiteListUploadTypes.CLEAR_WHITELISTUPLOAD_LIST
    })
}

export const WhiteListDeleteClientTypes = {
    WHITE_LIST_DELETE_CLIENT: 'WHITE_LIST_DELETE_CLIENT',
    WHITE_LIST_DELETE_CLIENT_SUCCESS: 'WHITE_LIST_DELETE_CLIENT_SUCCESS',
    WHITE_LIST_DELETE_CLIENT_ERROR: 'WHITE_LIST_DELETE_CLIENT_ERROR',
    WHITE_LIST_DELETE_CLIENT_CLEAR: 'WHITE_LIST_DELETE_CLIENT_CLEAR'
}
export const WhiteListDeleteClientActions = {
    WhiteListDeleteClientInit: (data) => ({
        type: WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT,
        payload: data
    }),
    WhiteListDeleteClientSuccess: (data) => ({
        type: WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT_SUCCESS,
        data
    }),
    WhiteListDeleteClientError: () => ({
        type: WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT_ERROR
    }),
    WhiteListDeleteClientClear: () => ({
        type: WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT_CLEAR
    })
}