export const preventionAlertActionsTypes = {
  GET_PREVENTIONALERT: 'GET_PREVENTIONALERT',
  SAVE_PREVENTIONALERT_RESPONSE: 'SAVE_PREVENTIONALERT_RESPONSE',
  CLEAR_PREVENTIONALERT: 'CLEAR_PREVENTIONALERT'
}
export const preventionAlertActions = {
  getPreventionAlert: (params) => ({
    type: preventionAlertActionsTypes.GET_PREVENTIONALERT,
    params
  }),
  savePreventionAlertResponse: data => ({
    type: preventionAlertActionsTypes.SAVE_PREVENTIONALERT_RESPONSE,
    data
  }),
  clearPosts: () => ({
    type: preventionAlertActionsTypes.CLEAR_PREVENTIONALERT
  })
}

export const dashboardGetDetailsTypes = {
  GET_DASHBOARD_DETAILS: 'GET_DASHBOARD_DETAILS',
  DASHBOARD_DETAILS_RESPONSE: 'DASHBOARD_DETAILS_RESPONSE',
  CLEAR_DASHBOARD_DETAILS: 'CLEAR_DASHBOARD_DETAILS'
}

export const dashboardDetailsActions = {
  getdashboardDetails: (id) => ({
    type: dashboardGetDetailsTypes.GET_DASHBOARD_DETAILS,
    id
  }),
  savedashboardDetailsResponse: data => ({
    type: dashboardGetDetailsTypes.DASHBOARD_DETAILS_RESPONSE,
    data
  }),
  cleardashboardDetails: () => ({
    type: dashboardGetDetailsTypes.CLEAR_DASHBOARD_DETAILS
  })
}

export const editAlertsTypes = {
  REQUEST: 'EDIT_PREVENTIONALERT_REQUEST',
  RESPONSE: 'EDIT_PREVENTIONALERT_RESPONSE',
  ERROR: 'EDIT_PREVENTIONALERT_ERROR',
  CLEAR: 'EDIT_PREVENTIONALERT_CLEAR'
}

export const editAlertsActions = {
  editAlerts: (id, params) => {
    return {
      type: editAlertsTypes.REQUEST,
      payload: { id, params }
    }
  },
  saveeditAlertsResponse: (data) => {
    return {
      type: editAlertsTypes.RESPONSE,
      data
    }
  },
  cleareditAlerts: () => ({
    type: editAlertsTypes.CLEAR
  })
}

export const dashboardFilterTypes = {
  DASHBOARD_FILTER_INIT: 'DASHBOARD_FILTER_INIT',
  DASHBOARD_FILTER_SUCCESS: 'DASHBOARD_FILTER_SUCCESS',
  DASHBOARD_FILTER_ERROR: 'DASHBOARD_FILTER_ERROR',
  DASHBOARD_FILTER_CLEAR: 'DASHBOARD_FILTER_CLEAR'
}
export const dashboardFilterActions = {
  dashboardFilterInit: (params) => {
    return {
      type: dashboardFilterTypes.DASHBOARD_FILTER_INIT,
      params
    }
  },
  dashboardFilterSuccess: (data) => {
    return {
      type: dashboardFilterTypes.DASHBOARD_FILTER_SUCCESS,
      data
    }
  },
  dashboardFilterError: (data) => {
    return {
      type: dashboardFilterTypes.DASHBOARD_FILTER_ERROR,
      data
    }
  },
  dashboardFilterClear: () => ({
    type: dashboardFilterTypes.DASHBOARD_FILTER_CLEAR
  })
}

export const dashboardTransactionTypes = {
  DASHBOARD_TRANSACTION_INIT: 'DASHBOARD_TRANSACTION_INIT',
  DASHBOARD_TRANSACTION_SUCCESS: 'DASHBOARD_TRANSACTION_SUCCESS',
  DASHBOARD_TRANSACTION_ERROR: 'DASHBOARD_TRANSACTION_ERROR'
}
export const dashboardTransactionActions = {
  dashboardTransactionInit: (params) => {
    return {
      type: dashboardTransactionTypes.DASHBOARD_TRANSACTION_INIT,
      params
    }
  },
  dashboardTransactionSuccess: (data) => {
    return {
      type: dashboardTransactionTypes.DASHBOARD_TRANSACTION_SUCCESS,
      data
    }
  },
  dashboardTransactionError: (data) => {
    return {
      type: dashboardTransactionTypes.DASHBOARD_TRANSACTION_ERROR,
      data
    }
  }
}

export const getWebsiteIdTypes = {
  GET_WEBSITEID: 'GET_WEBSITEID',
  GET_WEBSITEID_SUCCESS: 'GET_WEBSITEID_SUCCESS',
  GET_WEBSITEID_ERROR: 'GET_WEBSITEID_ERROR'
}
export const getWebsiteIdActions = {
  getWebsiteId: (params) => {
    return {
      type: getWebsiteIdTypes.GET_WEBSITEID,
      params
    }
  },
  getWebsiteIdSuccess: (data) => {
    return {
      type: getWebsiteIdTypes.GET_WEBSITEID_SUCCESS,
      data
    }
  },
  getWebsiteIdError: (data) => {
    return {
      type: getWebsiteIdTypes.GET_WEBSITEID_ERROR,
      data
    }
  }
}