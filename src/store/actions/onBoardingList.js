export const onBoardingActionsTypes = {
    GET_ON_BOARDING_LIST: 'GET_ON_BOARDING_LIST',
    SAVE_ON_BOARDING_LIST_RESPONSE: 'SAVE_ON_BOARDING_LIST_RESPONSE',
    CLEAR_ON_BOARDING_LIST: 'CLEAR_ON_BOARDING_LIST'
}

export const onBoardingActions = {
    getOnBoardinglist: (params) => ({
        type: onBoardingActionsTypes.GET_ON_BOARDING_LIST,
        params
    }),
    saveOnBoardinglistResponse: (data) => ({
        type: onBoardingActionsTypes.SAVE_ON_BOARDING_LIST_RESPONSE,
        data
    }),
    clearOnBoardinglist: () => ({
        type: onBoardingActionsTypes.CLEAR_ON_BOARDING_LIST
    })
}

export const OnBoardingAddTypes = {
    ON_BOARDING_POST: "ON_BOARDING_POST",
    ON_BOARDING_POST_RESPONSE: "ON_BOARDING_POST_RESPONSE",
    ON_BOARDING_POST_CLEAR: "ON_BOARDING_POST_CLEAR",
};

export const OnBoardingAddAction = {
    OnBoardingAdd: (data) => {
        return {
            type: OnBoardingAddTypes.ON_BOARDING_POST,
            payload: data,
        };
    },
    saveOnBoardingResponse: (data) => ({
        type: OnBoardingAddTypes.ON_BOARDING_POST_RESPONSE,
        data,
    }),
    clearOnBoarding: () => ({
        type: OnBoardingAddTypes.ON_BOARDING_POST_CLEAR,
    }),
};

export const OnBoardingUpdateTypes = {
    ON_BOARDING_UPDATE: "ON_BOARDING_UPDATE",
    ON_BOARDING_UPDATE_RESPONSE: "ON_BOARDING_UPDATE_RESPONSE",
    ON_BOARDING_UPDATE_CLEAR: "ON_BOARDING_UPDATE_CLEAR"
};

export const OnBoardingPutAction = {
    OnBoardingUpdate: (id, params) => {
        return {
            type: OnBoardingUpdateTypes.ON_BOARDING_UPDATE,
            payload: {id, params},
        };
    },
    saveOnBoardingUpdateResponse: (data) => ({
        type: OnBoardingUpdateTypes.ON_BOARDING_UPDATE_RESPONSE,
        data,
    }),
    clearOnBoardingUpdate: () => ({
        type: OnBoardingUpdateTypes.ON_BOARDING_UPDATE_CLEAR,
    }),
};

export const OnBoardingSummaryTypes = {
    GET_ON_BOARDING_SUMMARY: 'GET_ON_BOARDING_SUMMARY',
    SAVE_ON_BOARDING_SUMMARY_RESPONSE: 'SAVE_ON_BOARDING_SUMMARY_RESPONSE',
    CLEAR_ON_BOARDING_SUMMARY: 'CLEAR_ON_BOARDING_SUMMARY'
  }
  
  export const OnBoardingSummaryActions = {
  
    getOnBoardingSummary: (id) => (
      {
        type: OnBoardingSummaryTypes.GET_ON_BOARDING_SUMMARY,
        id
      }),
    saveOnBoardingSummaryResponse: data => ({
      type: OnBoardingSummaryTypes.SAVE_ON_BOARDING_SUMMARY_RESPONSE,
      data
    }),
    clearOnBoardingSummary: () => ({
      type: OnBoardingSummaryTypes.CLEAR_ON_BOARDING_SUMMARY
    })
  }