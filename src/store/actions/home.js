export const HomeActionsTypes = {
  GET_HOME_LIST: 'GET_HOME_LIST',
  SAVE_HOME_LIST_RESPONSE: 'SAVE_HOME_LIST_RESPONSE',
  CLEAR_HOME_LIST: 'CLEAR_HOME_LIST'
}

export const HomeActions = {
  getHomelist: (params) => ({
    type: HomeActionsTypes.GET_HOME_LIST,
    params
  }),
  saveHomelistResponse: (data) => ({
    type: HomeActionsTypes.SAVE_HOME_LIST_RESPONSE,
    data
  }),
  clearHomelist: () => ({
    type: HomeActionsTypes.CLEAR_HOME_LIST
  })
}

export const QueueValuesActionsTypes = {
  GET_QUEUEVALUES_LIST: 'GET_QUEUEVALUES_LIST',
  SAVE_QUEUEVALUES_LIST_RESPONSE: 'SAVE_QUEUEVALUES_LIST_RESPONSE',
  CLEAR_QUEUEVALUES_LIST: 'CLEAR_QUEUEVALUES_LIST'
}

export const QueueValuesActions = {
  getQueueValueslist: (params) => ({
    type: QueueValuesActionsTypes.GET_QUEUEVALUES_LIST,
    params
  }),
  saveQueueValueslistResponse: (data) => ({
    type: QueueValuesActionsTypes.SAVE_QUEUEVALUES_LIST_RESPONSE,
    data
  }),
  clearQueueValueslist: () => ({
    type: QueueValuesActionsTypes.CLEAR_QUEUEVALUES_LIST
  })
}