export const WrmRiskManagementActionsTypes = {
    GET_WRMRISKMANAGEMENT_LIST: 'GET_WRMRISKMANAGEMENT_LIST',
    SAVE_WRMRISKMANAGEMENT_LIST_RESPONSE: 'SAVE_WRMRISKMANAGEMENT_LIST_RESPONSE',
    CLEAR_WRMRISKMANAGEMENT_LIST: 'CLEAR_WRMRISKMANAGEMENT_LIST'
  }
  
  export const WrmRiskManagementActions = {
    getWrmRiskMangemnt: (params) => ({
      type: WrmRiskManagementActionsTypes.GET_WRMRISKMANAGEMENT_LIST,
      params
    }),
    saveWrmRiskMangemntResponse: (data) => ({
      type: WrmRiskManagementActionsTypes.SAVE_WRMRISKMANAGEMENT_LIST_RESPONSE,
      data
    }),
    clearWrmRiskMangemnt: () => ({
      type: WrmRiskManagementActionsTypes.CLEAR_WRMRISKMANAGEMENT_LIST
    })
  }

  export const WrmStatusTypes = {
    GET_WRM_STATUS: 'GET_WRM_STATUS',
    SAVE_WRM_STATUS_RESPONSE: 'SAVE_WRM_STATUS_RESPONSE',
    CLEAR_WRM_STATUS: 'CLEAR_WRM_STATUS'
  }
  
  export const WrmStatusActions = {
    getWrmStatus: (params) => ({
      type: WrmStatusTypes.GET_WRM_STATUS,
      params
    }),
    saveWrmStatusResponse: (data) => ({
      type: WrmStatusTypes.SAVE_WRM_STATUS_RESPONSE,
      data
    }),
    clearWrmStatus: () => ({
      type: WrmStatusTypes.CLEAR_WRM_STATUS
    })
  }
  
  export const wrmSdkActionsTypes = {
    GET_WRMSDKMANAGE_LIST: 'GET_WRMSDKMANAGE_LIST',
    SAVE_WRMSDKMANAGE_LIST_RESPONSE: 'SAVE_WRMSDKMANAGE_LIST_RESPONSE',
    CLEAR_WRMSDKMANAGE_LIST: 'CLEAR_WRMSDKMANAGE_LIST'
  }
  
  export const wrmSdkActions = {
    getWrmSdkManage: (params) => ({
      type: wrmSdkActionsTypes.GET_WRMSDKMANAGE_LIST,
      params
    }),
    saveWrmSdkManageResponse: (data) => ({
      type: wrmSdkActionsTypes.SAVE_WRMSDKMANAGE_LIST_RESPONSE,
      data
    }),
    clearWrmSdkManage: () => ({
      type: wrmSdkActionsTypes.CLEAR_WRMSDKMANAGE_LIST
    })
  }

  export const UpdateWebReportTypes = {
    GET_UPDATEWEBREPORT: 'GET_UPDATEWEBREPORT',
    SAVE_UPDATEWEBREPORT_RESPONSE: 'SAVE_UPDATEWEBREPORT_RESPONSE',
    CLEAR_UPDATEWEBREPORT: 'CLEAR_UPDATEWEBREPORT'
  }
  
  export const UpdateWebReportActions = {
    getUpdateWebReport: (params) => ({
      type: UpdateWebReportTypes.GET_UPDATEWEBREPORT,
      params
    }),
    saveUpdateWebReportResponse: (data) => ({
      type: UpdateWebReportTypes.SAVE_UPDATEWEBREPORT_RESPONSE,
      data
    }),
    clearUpdateWebReport: () => ({
      type: UpdateWebReportTypes.CLEAR_UPDATEWEBREPORT
    })
  }

  export const DashboardListPmaTypes = {
    GET_DASHBOARDLIST_PMA: 'GET_DASHBOARDLIST_PMA',
    SAVE_DASHBOARDLIST_PMA_RESPONSE: 'SAVE_DASHBOARDLIST_PMA_RESPONSE',
    CLEAR_DASHBOARDLIST_PMA: 'CLEAR_DASHBOARDLIST_PMA'
  }
  
  export const DashboardListPmaActions = {
    getDashboardListPma: (params) => ({
      type: DashboardListPmaTypes.GET_DASHBOARDLIST_PMA,
      params
    }),
    saveDashboardListPmaResponse: (data) => ({
      type: DashboardListPmaTypes.SAVE_DASHBOARDLIST_PMA_RESPONSE,
      data
    }),
    clearDashboardListPma: () => ({
      type: DashboardListPmaTypes.CLEAR_DASHBOARDLIST_PMA
    })
  }

  export const GetClientsTypes = {
    GET_CLIENTS: 'GET_CLIENTS',
    SAVE_GET_CLIENTS_RESPONSE: 'SAVE_GET_CLIENTS_RESPONSE',
    CLEAR_GET_CLIENTS: 'CLEAR_GET_CLIENTS'
  }
  
  export const GetClientsActions = {
    getClientsWrm: (params) => ({
      type: GetClientsTypes.GET_CLIENTS,
      params
    }),
    saveClientsWrmResponse: (data) => ({
      type: GetClientsTypes.SAVE_GET_CLIENTS_RESPONSE,
      data
    }),
    clearClientsWrm: () => ({
      type: GetClientsTypes.CLEAR_GET_CLIENTS
    })
  }
  
  export const CategoryStatusTypes = {
    GET_CATEGORYSTATUS_LIST: 'GET_CATEGORYSTATUS_LIST',
    SAVE_CATEGORYSTATUS_LIST_RESPONSE: 'SAVE_CATEGORYSTATUS_LIST_RESPONSE',
    CLEAR_CATEGORYSTATUS_LIST: 'CLEAR_CATEGORYSTATUS_LIST'
  }
  
  export const CategoryStatusActions = {
    getCategoryStatusManage: (params) => ({
      type: CategoryStatusTypes.GET_CATEGORYSTATUS_LIST,
      params
    }),
    saveCategoryStatusManageResponse: (data) => ({
      type: CategoryStatusTypes.SAVE_CATEGORYSTATUS_LIST_RESPONSE,
      data
    }),
    clearCategoryStatusManage: () => ({
      type: CategoryStatusTypes.CLEAR_CATEGORYSTATUS_LIST
    })
  }

  export const updateImageUploadActionsTypes = {
    UPDATE_IMAGE_UPLOAD: 'UPDATE_IMAGE_UPLOAD',
    SAVE_IMAGE_UPLOAD_RESPONSE: 'SAVE_IMAGE_UPLOAD_RESPONSE',
    CLEAR_IMAGE_UPLOAD: 'CLEAR_IMAGE_UPLOAD'
  }
  
  export const updateImageUploadActions = {
    updateImageUpload: (id, params) => {
      return {
        type: updateImageUploadActionsTypes.UPDATE_IMAGE_UPLOAD,
        payload:  { id, params }
      }
    },
    saveImageUploadResponse: data => ({
      type: updateImageUploadActionsTypes.SAVE_IMAGE_UPLOAD_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearImageUpload: () => ({
      type: updateImageUploadActionsTypes.CLEAR_IMAGE_UPLOAD
    })
  }

  export const updateStatusChangeActionsTypes = {
    UPDATE_STATUS_CHANGE: 'UPDATE_STATUS_CHANGE',
    SAVE_STATUS_CHANGE_RESPONSE: 'SAVE_STATUS_CHANGE_RESPONSE',
    CLEAR_STATUS_CHANGE: 'CLEAR_STATUS_CHANGE'
  }
  
  export const updateStatusChangeActions = {
    updateStatusChange: (id, params) => {
      return {
        type: updateStatusChangeActionsTypes.UPDATE_STATUS_CHANGE,
        payload:  { id, params }
      }
    },
    saveStatusChangeResponse: data => ({
      type: updateStatusChangeActionsTypes.SAVE_STATUS_CHANGE_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearStatusChange: () => ({
      type: updateStatusChangeActionsTypes.CLEAR_STATUS_CHANGE
    })
  }

  export const wrmStatusChangeActionsTypes = {
    WRM_STATUS_CHANGE: 'WRM_STATUS_CHANGE',
    SAVE_WRM_STATUS_CHANGE_RESPONSE: 'SAVE_WRM_STATUS_CHANGE_RESPONSE',
    CLEAR_WRM_STATUS_CHANGE: 'CLEAR_WRM_STATUS_CHANGE'
  }
  
  export const wrmStatusChangeActions = {
    wrmStatusChange: (params) => {
      return {
        type: wrmStatusChangeActionsTypes.WRM_STATUS_CHANGE,
        params
      }
    },
    savewrmStatusChangeResponse: data => ({
      type: wrmStatusChangeActionsTypes.SAVE_WRM_STATUS_CHANGE_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearwrmStatusChange: () => ({
      type: wrmStatusChangeActionsTypes.CLEAR_WRM_STATUS_CHANGE
    })
  }

  export const WrmUpdateQueueActionsTypes = {
    WRM_UPDATE_QUEUE: 'WRM_UPDATE_QUEUE',
    SAVE_WRM_UPDATE_QUEUE_RESPONSE: 'SAVE_WRM_UPDATE_QUEUE_RESPONSE',
    CLEAR_WRM_UPDATE_QUEUE: 'CLEAR_WRM_UPDATE_QUEUE'
  }
  
  export const WrmUpdateQueueActions = {
    WrmUpdateQueue: (params) => {
      return {
        type: WrmUpdateQueueActionsTypes.WRM_UPDATE_QUEUE,
        params
      }
    },
    saveWrmUpdateQueueResponse: data => ({
      type: WrmUpdateQueueActionsTypes.SAVE_WRM_UPDATE_QUEUE_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearWrmUpdateQueue: () => ({
      type: WrmUpdateQueueActionsTypes.CLEAR_WRM_UPDATE_QUEUE
    })
  }