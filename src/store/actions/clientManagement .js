export const addClientMgmtTypes = {
  REQUEST: 'ADD_CLIENT_MGMT_REQUEST',
  RESPONSE: 'ADD_CLIENT_MGMT_RESPONSE',
  ERROR: 'ADD_CLIENT_MGMT_ERROR',
  CLEAR: 'ADD_CLIENT_MGMT_CLEAR'
}

export const clientMgmtActions = {
  addClientMgmt: (data) => {
    return {
      type: addClientMgmtTypes.REQUEST,
      payload: data
    }
  },
  saveaddClientMgmtResponse: (data) => {
    return {
      type: addClientMgmtTypes.RESPONSE,
      data
    }
  },
  clearaddClientMgmt: () => ({
    type: addClientMgmtTypes.CLEAR
  })
}

export const clientDetailsTypes = {
  REQUEST: 'CLIENT_DETAILS_REQUEST_',
  RESPONSE: 'CLIENT_DETAILS_RESPONSE_',
  ERROR: 'CLIENT_DETAILS_ERROR_',
  CLEAR: 'CLIENT_DETAILS_CLEAR_'
}

export const clientDetailsActions = {
  getClientDetails: (data) => {
    return {
      type: clientDetailsTypes.REQUEST,
      payload: data
    }
  },
  saveClientDetailsResponse: (data) => {
    return {
      type: clientDetailsTypes.RESPONSE,
      data
    }
  },
  clearClientDetails: () => ({
    type: clientDetailsTypes.CLEAR
  })
}

export const clientGetDetailsTypes = {
  GET_CLIENT_DETAILS: 'GET_CLIENT_DETAILS',
  CLIENT_DETAILS_RESPONSE: 'CLIENT_DETAILS_RESPONSE',
  CLEAR_CLIENT_DETAILS: 'CLEAR_CLIENT_DETAILS'
}
export const clientGetDetailsActions = {
  getClientDetails: (id) => ({
    type: clientGetDetailsTypes.GET_CLIENT_DETAILS,
    id
  }),
  saveClientDetailsResponse: data => ({
    type: clientGetDetailsTypes.CLIENT_DETAILS_RESPONSE,
    data
  }),
  clearClientDetails: () => ({
    type: clientGetDetailsTypes.CLEAR_CLIENT_DETAILS
  })
}

export const editClientTypes = {
  REQUEST: 'EDIT_CLIENT_REQUEST',
  RESPONSE: 'EDIT_CLIENT_RESPONSE',
  ERROR: 'EDIT_CLIENT_ERROR',
  CLEAR: 'EDIT_CLIENT_CLEAR'
}

export const editClientsActions = {
  editClients: (id, params) => {
    return {
      type: editClientTypes.REQUEST,
      payload: { id, params }
    }
  },
  saveeditClientsResponse: (data) => {
    return {
      type: editClientTypes.RESPONSE,
      data
    }
  },
  cleareditClients: () => ({
    type: editClientTypes.CLEAR
  })
}
