export const SdkExportListActionsTypes = {
    GET_SDK_EXPORT_LIST: 'GET_SDK_EXPORT_LIST',
    SAVE_SDK_EXPORT_LIST_RESPONSE: 'SAVE_SDK_EXPORT_LIST_RESPONSE',
    CLEAR_SDK_EXPORT_LIST: 'CLEAR_EXPORT_LIST'
  }
  
  export const SdkExportListActions = {
    getSdkExportList: (params) => ({
      type: SdkExportListActionsTypes.GET_SDK_EXPORT_LIST,
      params
    }),
    saveSdkExportListResponse: (data) => ({
      type: SdkExportListActionsTypes.SAVE_SDK_EXPORT_LIST_RESPONSE,
      data
    }),
    clearSdkExportList: () => ({
      type: SdkExportListActionsTypes.CLEAR_SDK_EXPORT_LIST
    })
  }

  export const postSdkWebAnalysisTypes = {
    POST_SDK_WEBANALYSIS_LIST: 'POST_SDK_WEBANALYSIS_LIST',
    SAVE_SDK_WEBANALYSIS_LIST_RESPONSE: 'SAVE_SDK_WEBANALYSIS_LIST_RESPONSE',
    CLEAR_SDK_WEBANALYSIS_LIST: 'CLEAR_SDK_WEBANALYSIS_LIST'
  }
  
export const PostWebAnalysisActions = {
    postSdkWebAnalysis: (data) => ({
      type: postSdkWebAnalysisTypes.POST_SDK_WEBANALYSIS_LIST,
      payload: data
    }),
    saveSdkWebAnalysisResponse: (data) => ({
      type: postSdkWebAnalysisTypes.SAVE_SDK_WEBANALYSIS_LIST_RESPONSE,
      data
    }),
    clearSdkWebAnalysis: () => ({
      type: postSdkWebAnalysisTypes.CLEAR_SDK_WEBANALYSIS_LIST
    })
}


export const SdkManualWebAnalysisTypes = {
  POST_SDK_MANUAL_WEBANALYSIS_LIST: 'POST_SDK_MANUAL_WEBANALYSIS_LIST',
  SAVE_SDK_MANUAL_WEBANALYSIS_LIST_RESPONSE: 'SAVE_SDK_MANUAL_WEBANALYSIS_LIST_RESPONSE',
  CLEAR_SDK_MANUAL_WEBANALYSIS_LIST: 'CLEAR_SDK_MANUAL_WEBANALYSIS_LIST'
}

export const SdkManualWebAnalysisActions = {
  postSdkManualWebAnalysis: (data) => ({
    type: SdkManualWebAnalysisTypes.POST_SDK_MANUAL_WEBANALYSIS_LIST,
    payload: data
  }),
  saveSdkManualWebAnalysisResponse: (data) => ({
    type: SdkManualWebAnalysisTypes.SAVE_SDK_MANUAL_WEBANALYSIS_LIST_RESPONSE,
    data
  }),
  clearSdkManualWebAnalysis: () => ({
    type: SdkManualWebAnalysisTypes.CLEAR_SDK_MANUAL_WEBANALYSIS_LIST
  })
}

export const getSdkWebAnalysisTypes = {
  GET_SDK_WEBANALYSIS_LIST: 'GET_SDK_WEBANALYSIS_LIST',
  SAVE_GET_SDK_WEBANALYSIS_LIST_RESPONSE: 'SAVE_GET_SDK_WEBANALYSIS_LIST_RESPONSE',
  CLEAR_GET_SDK_WEBANALYSIS_LIST: 'CLEAR_GET_SDK_WEBANALYSIS_LIST'
}

export const getSdkWebAnalysisActions = {
  getSdkWebAnalysislist: (params) => ({
    type: getSdkWebAnalysisTypes.GET_SDK_WEBANALYSIS_LIST,
    params
  }),
  savegetSdkWebAnalysislistResponse: (data) => ({
    type: getSdkWebAnalysisTypes.SAVE_GET_SDK_WEBANALYSIS_LIST_RESPONSE,
    data
  }),
  cleargetSdkWebAnalysislist: () => ({
    type: getSdkWebAnalysisTypes.CLEAR_GET_SDK_WEBANALYSIS_LIST
  })
}

export const SdkBlockListEmailTypes = {
  GET_SDK_BLOCKLISTEMAIL_LIST: 'GET_SDK_BLOCKLISTEMAIL_LIST',
  SAVE_SDK_BLOCKLISTEMAIL_LIST_RESPONSE: 'SAVE_SDK_BLOCKLISTEMAIL_LIST_RESPONSE',
  CLEAR_SDK_BLOCKLISTEMAIL_LIST: 'CLEAR_SDK_BLOCKLISTEMAIL_LIST'
}

export const SDKBlockListEmailActions = {
  getSdkBlockListType: (params) => ({
      type: SdkBlockListEmailTypes.GET_SDK_BLOCKLISTEMAIL_LIST,
      params
  }),
  saveSdkBlockListTypeResponse: (data) => ({
      type: SdkBlockListEmailTypes.SAVE_SDK_BLOCKLISTEMAIL_LIST_RESPONSE,
      data
  }),
  clearSdkBlockList: () => ({
      type: SdkBlockListEmailTypes.CLEAR_SDK_BLOCKLISTEMAIL_LIST
  })
}

export const SdkBWListActionsTypes = {
  GET_SDK_BW_LIST: 'GET_SDK_BW_LIST',
  SAVE_SDK_BW_LIST_RESPONSE: 'SAVE_SDK_BW_LIST_RESPONSE',
  CLEAR_SDK_BW_LIST: 'CLEAR_SDK_BW_LIST'
}

export const SdkBWListActions = {
  getSdkBWList: (params) => ({
    type: SdkBWListActionsTypes.GET_SDK_BW_LIST,
    params
  }),
  saveSdkBWListResponse: (data) => ({
    type: SdkBWListActionsTypes.SAVE_SDK_BW_LIST_RESPONSE,
    data
  }),
  clearSdkBWList: () => ({
    type: SdkBWListActionsTypes.CLEAR_SDK_BW_LIST
  })
}

export const PostSdkCategoryTypes = {
  POST_SDK_CATEGORY: 'POST_SDK_CATEGORY',
  SAVE_SDK_CATEGORY_RESPONSE: 'SAV_SDK_CATEGORY_RESPONSE',
  CLEAR_SDK_CATEGORY: 'CLEAR_SDK_CATEGORY_SUMMARY'
}

export const PostSdkCategoryActions = {
  PostSdkCategroy: (params) => ({
    type: PostSdkCategoryTypes.POST_SDK_CATEGORY,
    params
  }),
  PostSdkCategroyResponse: data => ({
    type: PostSdkCategoryTypes.SAVE_SDK_CATEGORY_RESPONSE,
    data
  }),
  ClearPostSdkCategroy: () => ({
    type: PostSdkCategoryTypes.CLEAR_SDK_CATEGORY
  })
}

export const PostSdkCategory1Types = {
  POST_SDK_CATEGORY1: 'POST_SDK_CATEGORY1',
  SAVE_SDK_CATEGORY1_RESPONSE: 'SAV_SDK_CATEGORY1_RESPONSE',
  CLEAR_SDK_CATEGORY1: 'CLEAR_SDK_CATEGORY1_SUMMARY'
}

export const PostSdkCategory1Actions = {
  PostSdkCategory1: (params) => ({
    type: PostSdkCategory1Types.POST_SDK_CATEGORY1,
    params
  }),
  PostSdkCategoryResponse: data => ({
    type: PostSdkCategory1Types.SAVE_SDK_CATEGORY1_RESPONSE,
    data
  }),
  ClearSdkPostCategory1: () => ({
    type: PostSdkCategory1Types.CLEAR_SDK_CATEGORY1
  })
}

export const WrmListStatusChangeActionsTypes = {
  WRM_LIST_STATUS_CHANGE: 'WRM_LIST_STATUS_CHANGE',
  SAVE_WRM_LIST_STATUS_CHANGE_RESPONSE: 'SAVE_WRM_LIST_STATUS_CHANGE_RESPONSE',
  CLEAR_WRM_LIST_STATUS_CHANGE: 'CLEAR_WRM_LIST_STATUS_CHANGE'
}

export const WrmListStatusChangeActions = {
  WrmListStatusChange: (params) => {
    return {
      type: WrmListStatusChangeActionsTypes.WRM_LIST_STATUS_CHANGE,
      params
    }
  },
  saveWrmListStatusChangeResponse: data => ({
    type: WrmListStatusChangeActionsTypes.SAVE_WRM_LIST_STATUS_CHANGE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearWrmListStatusChange: () => ({
    type: WrmListStatusChangeActionsTypes.CLEAR_WRM_LIST_STATUS_CHANGE
  })
}

export const WrmListUpdateQueueActionsTypes = {
  WRM_LIST_UPDATE_QUEUE: 'WRM_LIST_UPDATE_QUEUE',
  SAVE_WRM_LIST_UPDATE_QUEUE_RESPONSE: 'SAVE_WRM_LIST_UPDATE_QUEUE_RESPONSE',
  CLEAR_WRM_LIST_UPDATE_QUEUE: 'CLEAR_WRM_LIST_UPDATE_QUEUE'
}

export const WrmListUpdateQueueActions = {
  WrmListUpdateQueue: (params) => {
    return {
      type: WrmListUpdateQueueActionsTypes.WRM_LIST_UPDATE_QUEUE,
      params
    }
  },
  saveWrmListUpdateQueueResponse: data => ({
    type: WrmListUpdateQueueActionsTypes.SAVE_WRM_LIST_UPDATE_QUEUE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearWrmListUpdateQueue: () => ({
    type: WrmListUpdateQueueActionsTypes.CLEAR_WRM_LIST_UPDATE_QUEUE
  })
}