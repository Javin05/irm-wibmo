export const addMerchantCLTypes = {
  REQUEST: 'ADD_MERCHANT_MGMT_REQUEST',
  RESPONSE: 'ADD_MERCHANT_MGMT_RESPONSE',
  ERROR: 'ADD_MERCHANT_MGMT_ERROR',
  CLEAR: 'ADD_MERCHANT_MGMT_CLEAR'
}

export const addMerchantCLActions = {
  addMerchantCL: (data) => {
    return {
      type: addMerchantCLTypes.REQUEST,
      payload: data
    }
  },
  saveaddMerchantCLResponse: (data) => {
    return {
      type: addMerchantCLTypes.RESPONSE,
      data
    }
  },
  clearaddMerchantCL: () => ({
    type: addMerchantCLTypes.CLEAR
  })
}

export const saveMerchantTypes = {
  REQUEST: 'SAVE_MERCHANT_REQUEST',
  RESPONSE: 'SAVE_MERCHANT_DETAILS_RESPONSE',
  ERROR: 'SAVE_MERCHANT_DETAILS_ERROR',
  CLEAR: 'SAVE_MERCHANT_DETAILS_CLEAR'
}

export const saveMerchantActions = {
  saveMerchant: (data) => {
    return {
      type: saveMerchantTypes.REQUEST,
      payload: data
    }
  },
  saveMerchantResponse: (data) => {
    return {
      type: saveMerchantTypes.RESPONSE,
      data
    }
  },
  clearMerchant: () => ({
    type: saveMerchantTypes.CLEAR
  })
}

export const merchantActionsTypes = {
  GET_MERCHANT: 'GET_MERCHANT',
  SAVE_MERCHANT_RESPONSE: 'SAVE_MERCHANT_RESPONSE',
  CLEAR_MERCHANT: 'CLEAR_MERCHANT'
}

export const merchantActions = {
  getMerchant: (params) => ({
    type: merchantActionsTypes.GET_MERCHANT,
    params
  }),
  savemerchantResponse: data => ({
    type: merchantActionsTypes.SAVE_MERCHANT_RESPONSE,
    data
  }),
  clearMerchant: () => ({
    type: merchantActionsTypes.CLEAR_MERCHANT
  })
}

export const merchantDeleteTypes = {
  DELETE_MERCHANT: 'DELETE_MERCHANT',
  SAVE_DELETE_MERCHANT_RESPONSE: 'SAVE_DELETE_MERCHANT_RESPONSE',
  CLEAR_DELETE_MERCHANT: 'CLEAR_DELETE_MERCHANT'
}

export const deleteMerchantActions = {
  deleteMerchant: (params) => ({
    type: merchantDeleteTypes.DELETE_MERCHANT,
    params
  }),
  savemResponseDeleteMerchant: data => ({
    type: merchantDeleteTypes.SAVE_DELETE_MERCHANT_RESPONSE,
    data
  }),
  clearDeleteMerchant: () => ({
    type: merchantDeleteTypes.CLEAR_DELETE_MERCHANT
  })
}

export const merchantGetDetailTypes = {
  GET_MERCHANT_DETAILS: 'GET_MERCHANT_DETAILS',
  MERCHANT_DETAILS_RESPONSE: 'MERCHANT_DETAILS_RESPONSE',
  CLEAR_MERCHANT_DETAILS: 'CLEAR_MERCHANT_DETAILS',
  SET_CURRENT_CLIENT_ID: 'SET_CURRENT_CLIENT_ID'
}

export const merchantGetDetailsActions = {
  getMerchantDetails: (id) => ({
    type: merchantGetDetailTypes.GET_MERCHANT_DETAILS,
    id
  }),
  saveMerchantDetailsResponse: data => ({
    type: merchantGetDetailTypes.MERCHANT_DETAILS_RESPONSE,
    data
  }),
  clearMerchantDetails: () => ({
    type: merchantGetDetailTypes.CLEAR_MERCHANT_DETAILS
  })
}

export const editMerchantTypes = {
  REQUEST: 'EDIT_MERCHANT_REQUEST',
  RESPONSE: 'EDIT_MERCHANT_RESPONSE',
  ERROR: 'EDIT_MERCHANT_ERROR',
  CLEAR: 'EDIT_MERCHANT_CLEAR'
}

export const editMerchantActions = {
  editMerchant: (id, params) => {
    return {
      type: editMerchantTypes.REQUEST,
      payload: { id, params }
    }
  },
  saveeditMerchantResponse: (data) => {
    return {
      type: editMerchantTypes.RESPONSE,
      data
    }
  },
  cleareditMerchant: () => ({
    type: editMerchantTypes.CLEAR
  })
}

export const addMerchantUploadTypes = {
  REQUEST: 'ADD_MERCHANT_UPLOAD_REQUEST',
  RESPONSE: 'ADD_MERCHANT_UPLOAD_RESPONSE',
  ERROR: 'ADD_MERCHANT_UPLOAD_ERROR',
  CLEAR: 'ADD_MERCHANT_UPLOAD_CLEAR'
}

export const addMerchantUploadActions = {
  addMerchantUpload: (data) => {
    return {
      type: addMerchantUploadTypes.REQUEST,
      payload: data
    }
  },
  saveaddMerchantUploadResponse: (data) => {
    return {
      type: addMerchantUploadTypes.RESPONSE,
      data
    }
  },
  clearaddMerchantUpload: () => ({
    type: addMerchantUploadTypes.CLEAR
  })
}