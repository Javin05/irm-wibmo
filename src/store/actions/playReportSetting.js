export const playStoreReportSettingActionsTypes = {
  GET_PLAYSTOREREPROT_SEETING_LIST: 'GET_PLAYSTOREREPROT_SEETING_LIST',
  SAVE_PLAYSTOREREPROT_SEETING_LIST_RESPONSE: 'SAVE_PLAYSTOREREPROT_SEETING_LIST_RESPONSE',
  CLEAR_PLAYSTOREREPROT_SEETING_LIST: 'CLEAR_PLAYSTOREREPROT_SEETING_LIST'
}

export const playStoreReportSettingActions = {
  getplayStoreReportSettinglist: (params) => ({
    type: playStoreReportSettingActionsTypes.GET_PLAYSTOREREPROT_SEETING_LIST,
    params
  }),
  saveplayStoreReportSettinglistResponse: (data) => ({
    type: playStoreReportSettingActionsTypes.SAVE_PLAYSTOREREPROT_SEETING_LIST_RESPONSE,
    data
  }),
  clearplayStoreReportSettinglist: () => ({
    type: playStoreReportSettingActionsTypes.CLEAR_PLAYSTOREREPROT_SEETING_LIST
  })
}

export const playStoreReportSettingAddTypes = {
  PLAYSTOREREPROT_SEETING_POST: 'PLAYSTOREREPROT_SEETING_POST',
  PLAYSTOREREPROT_SEETING_POST_RESPONSE: 'PLAYSTOREREPROT_SEETING_POST_RESPONSE',
  PLAYSTOREREPROT_SEETING_POST_CLEAR: 'PLAYSTOREREPROT_SEETING_POST_CLEAR'
}

export const playStoreReportSettingAddAction = {
  playStoreReportSettingAdd: (data) => {
    return {
      type: playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST,
      payload: data
    }
  },
  saveplayStoreReportSettingResponse: data => ({
    type: playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST_RESPONSE,
    data
  }),
  clearplayStoreReportSetting: () => ({
    type: playStoreReportSettingAddTypes.PLAYSTOREREPROT_SEETING_POST_CLEAR
  })
}

export const playStoreReportSettingGetId = {
  GET_PLAYSTOREREPROT_SEETING_DETAILS: 'GET_PLAYSTOREREPROT_SEETING_DETAILS',
  PLAYSTOREREPROT_SEETING_DETAILS_RESPONSE: 'PLAYSTOREREPROT_SEETING_DETAILS_RESPONSE',
  CLEAR_PLAYSTOREREPROT_SEETING_DETAILS: 'CLEAR_PLAYSTOREREPROT_SEETING_DETAILS'
}

export const playStoreReportSettingGetIdActions = {
  getplayStoreReportSettingIdDetails: (id) => ({
    type: playStoreReportSettingGetId.GET_PLAYSTOREREPROT_SEETING_DETAILS,
    id
  }),
  saveplayStoreReportSettingIdDetailsResponse: data => ({
    type: playStoreReportSettingGetId.PLAYSTOREREPROT_SEETING_DETAILS_RESPONSE,
    data
  }),
  clearplayStoreReportSettingIdDetails: () => ({
    type: playStoreReportSettingGetId.CLEAR_PLAYSTOREREPROT_SEETING_DETAILS
  })
}

export const updateplayStoreReportSettingActionsTypes = {
  UPDATE_PLAYSTOREREPROT_SEETING: 'UPDATE_PLAYSTOREREPROT_SEETING',
  SAVE_UPDATE_PLAYSTOREREPROT_SEETING_RESPONSE: 'SAVE_UPDATE_PLAYSTOREREPROT_SEETING_RESPONSE',
  CLEAR_UPDATE_PLAYSTOREREPROT_SEETING: 'CLEAR_UPDATE_PLAYSTOREREPROT_SEETING'
}

export const updateplayStoreReportSettingActions = {
  updateplayStoreReportSetting: (id, params) => {
    return {
      type: updateplayStoreReportSettingActionsTypes.UPDATE_PLAYSTOREREPROT_SEETING,
      payload:  { id, params }
    }
  },
  saveupdateplayStoreReportSettingResponse: data => ({
    type: updateplayStoreReportSettingActionsTypes.SAVE_UPDATE_PLAYSTOREREPROT_SEETING_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearupdateplayStoreReportSetting: () => ({
    type: updateplayStoreReportSettingActionsTypes.CLEAR_UPDATE_PLAYSTOREREPROT_SEETING
  })
}

export const playStoreReportSettingDelete = {
  PLAYSTOREREPROT_SEETING_DELETE: 'PLAYSTOREREPROT_SEETING_DELETE',
  PLAYSTOREREPROT_SEETING_DELETE_RESPONSE: 'PLAYSTOREREPROT_SEETING_DELETE_RESPONSE',
  CLEAR_PLAYSTOREREPROT_SEETING_DELETE: 'CLEAR_PLAYSTOREREPROT_SEETING_DELETE'
}

export const playStoreReportSettingDeleteActions = {
  playStoreReportSettingDelete: (payload) => ({
    type: playStoreReportSettingDelete.PLAYSTOREREPROT_SEETING_DELETE,
    payload
  }),
  saveplayStoreReportSettingDeleteResponse: data => ({
    type: playStoreReportSettingDelete.PLAYSTOREREPROT_SEETING_DELETE_RESPONSE,
    data
  }),
  clearplayStoreReportSettingDelete: () => ({
    type: playStoreReportSettingDelete.CLEAR_PLAYSTOREREPROT_SEETING_DELETE
  })
}