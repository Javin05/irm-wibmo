export const queuesActionsTypes = {
  GET_QUEUES_LIST: 'GET_QUEUES_LIST',
  SAVE_QUEUES_LIST_RESPONSE: 'SAVE_QUEUES_LIST_RESPONSE',
  CLEAR_QUEUES_LIST: 'CLEAR_QUEUES_LIST'
}

export const queuesActions = {
  getqueueslist: (params) => ({
    type: queuesActionsTypes.GET_QUEUES_LIST,
    params
  }),
  savequeueslistResponse: (data) => ({
    type: queuesActionsTypes.SAVE_QUEUES_LIST_RESPONSE,
    data
  }),
  clearqueueslist: () => ({
    type: queuesActionsTypes.CLEAR_QUEUES_LIST
  })
}

export const queuesTypes = {
  QUEUES_POST: 'QUEUES_POST',
  QUEUES_POST_RESPONSE: 'QUEUES_POST_RESPONSE',
  QUEUES_POST_CLEAR: 'QUEUES_POST_CLEAR'
}

export const queuesAction = {
  queuesAdd: (data) => {
    return {
      type: queuesTypes.QUEUES_POST,
      payload: data
    }
  },
  savequeuesResponse: data => ({
    type: queuesTypes.QUEUES_POST_RESPONSE,
    data
  }),
  clearqueues: () => ({
    type: queuesTypes.QUEUES_POST_CLEAR
  })
}


export const queuesGetId = {
  GET_QUEUES_DETAILS: 'GET_QUEUES_DETAILS',
  QUEUES_DETAILS_RESPONSE: 'QUEUES_DETAILS_RESPONSE',
  CLEAR_QUEUES_DETAILS: 'CLEAR_QUEUES_DETAILS'
}

export const queuesGetIdActions = {
  getqueuesIdDetails: (id) => ({
    type: queuesGetId.GET_QUEUES_DETAILS,
    id
  }),
  savequeuesIdDetailsResponse: data => ({
    type: queuesGetId.QUEUES_DETAILS_RESPONSE,
    data
  }),
  clearqueuesIdDetails: () => ({
    type: queuesGetId.CLEAR_QUEUES_DETAILS
  })
}

export const updateQueuesActionsTypes = {
  UPDATE_QUEUES: 'UPDATE_QUEUES',
  SAVE_UPDATE_QUEUES_RESPONSE: 'SAVE_UPDATE_QUEUES_RESPONSE',
  CLEAR_UPDATE_QUEUES: 'CLEAR_UPDATE_QUEUES'
}

export const updateQueuesActions = {
  updateQueues: (id, params) => {
    return {
      type: updateQueuesActionsTypes.UPDATE_QUEUES,
      payload:  { id, params }
    }
  },
  saveupdateQueuesResponse: data => ({
    type: updateQueuesActionsTypes.SAVE_UPDATE_QUEUES_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearupdateQueues: () => ({
    type: updateQueuesActionsTypes.CLEAR_UPDATE_QUEUES
  })
}
