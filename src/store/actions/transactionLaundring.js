
export const TransactionLaundringTypes = {
    GET_TRANSACTIONLAUNDRING_LIST: 'GET_TRANSACTIONLAUNDRING_LIST',
    SAVE_TRANSACTIONLAUNDRING_LIST_RESPONSE: 'SAVE_TRANSACTIONLAUNDRING_LIST_RESPONSE',
    CLEAR_TRANSACTIONLAUNDRING_LIST: 'CLEAR_TRANSACTIONLAUNDRING_LIST'
  }
  
  export const TransactionLaundringActions = {
    getTransactionLaundringlist: (params) => ({
      type: TransactionLaundringTypes.GET_TRANSACTIONLAUNDRING_LIST,
      params
    }),
    saveTransactionLaundringlistResponse: (data) => ({
      type: TransactionLaundringTypes.SAVE_TRANSACTIONLAUNDRING_LIST_RESPONSE,
      data
    }),
    clearTransactionLaundringlist: () => ({
      type: TransactionLaundringTypes.CLEAR_TRANSACTIONLAUNDRING_LIST
    })
  }