export const OgmOperationManagementTypes = {
    GET_OGMOPERATIONMANAGEMENT_LIST: 'GET_OGMOPERATIONMANAGEMENT_LIST',
    SAVE_OGMOPERATIONMANAGEMENT_LIST_RESPONSE: 'SAVE_OGMOPERATIONMANAGEMENT_LIST_RESPONSE',
    CLEAR_OGMOPERATIONMANAGEMENT_LIST: 'CLEAR_OGMOPERATIONMANAGEMENT_LIST'
  }
  
  export const OgmOperationManagementActions = {
    getOgmOperationManagemnt: (params) => ({
      type: OgmOperationManagementTypes.GET_OGMOPERATIONMANAGEMENT_LIST,
      params
    }),
    saveOgmOperationManagemntResponse: (data) => ({
      type: OgmOperationManagementTypes.SAVE_OGMOPERATIONMANAGEMENT_LIST_RESPONSE,
      data
    }),
    clearOgmOperationManagemnt: () => ({
      type: OgmOperationManagementTypes.CLEAR_OGMOPERATIONMANAGEMENT_LIST
    })
  }

export const OgmOperatorActionTypes = {
  OGM_OPERATION_ACTION_INIT: " OGM_OPERATION_ACTION_INIT",
  OGM_OPERATION_ACTION_RESPONSE_SUCCESS: "OGM_OPERATION_ACTION_RESPONSE_SUCCESS",
  OGM_OPERATION_ACTION_CLEAR: "OGM_OPERATION_ACTION_CLEAR",
  OGM_OPERATION_ACTION_ERROR: "OGM_OPERATION_ACTION_ERROR",
};

export const OgmOperatorActions = {
  ogmOperatorActionInit: (params) => ({
    type: OgmOperatorActionTypes.OGM_OPERATION_ACTION_INIT,
    params,
  }),
  saveOgmOperatorActionResponse: (data) => ({
    type: OgmOperatorActionTypes.OGM_OPERATION_ACTION_RESPONSE_SUCCESS,
    data,
  }),
  clearOgmOperatorAction: () => ({
    type: OgmOperatorActionTypes.OGM_OPERATION_ACTION_CLEAR,
  }),
   errorOgmOperatorAction: () => ({
    type: OgmOperatorActionTypes.OGM_OPERATION_ACTION_ERROR,
  }),
};

export const OgmOperationManagementDetailTypes = {
    GET_OGMOPERATIONMANAGEMENT_DETAIL: 'GET_OGMOPERATIONMANAGEMENT_DETAIL',
    SAVE_OGMOPERATIONMANAGEMENT_DETAIL_RESPONSE: 'SAVE_OGMOPERATIONMANAGEMENT_DETAIL_RESPONSE',
    CLEAR_OGMOPERATIONMANAGEMENT_DETAIL: 'CLEAR_OGMOPERATIONMANAGEMENT_DETAIL'
  }
  
  export const OgmOperationManagementDetailActions = {
    getOgmOperationDetail: (id) => ({
      type: OgmOperationManagementDetailTypes.GET_OGMOPERATIONMANAGEMENT_DETAIL,
      id
    }),
    saveOgmOperationDetailResponse: (data) => ({
      type: OgmOperationManagementDetailTypes.SAVE_OGMOPERATIONMANAGEMENT_DETAIL_RESPONSE,
      data
    }),
    clearOgmOperationDetail: () => ({
      type: OgmOperationManagementDetailTypes.CLEAR_OGMOPERATIONMANAGEMENT_DETAIL
    })
  }



