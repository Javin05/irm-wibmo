export const KYCDashboardSummaryTypes = {
  KYC_SUMMARY_DASHBOARD_INIT: "KYC_SUMMARY_DASHBOARD_INIT",
  KYC_SUMMARY_DASHBOARD_SUCCESS: "KYC_SUMMARY_DASHBOARD_SUCCESS",
  KYC_SUMMARY_DASHBOARD_CLEAR: "KYC_SUMMARY_DASHBOARD_CLEAR",
  KYC_SUMMARY_DASHBOARD_ERROR: "KYC_SUMMARY_DASHBOARD_ERROR",
};

export const KYCDashboardSummaryAction = {
  KYCDashboardSummary_INIT: (params) => ({
    type: KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_INIT,
    params,
  }),
  KYCDashboardSummary_SUCCESS: (data) => ({
    type: KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_SUCCESS,
    data,
  }),
  KYCDashboardSummary_CLEAR: () => ({
    type: KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_CLEAR,
  }),
  KYCDashboardSummary_ERROR: () => ({
    type: KYCDashboardSummaryTypes.ERROR_KYC_SUMMARY_DASHBOARD,
  }),
};
