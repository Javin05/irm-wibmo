export const BlockListTypes = {
    GET_BLOCKLIST_LIST: 'GET_BLOCKLIST_LIST',
    SAVE_BLOCKLIST_LIST_RESPONSE: 'SAVE_BLOCKLIST_LIST_RESPONSE',
    CLEAR_BLOCKLIST_LIST: 'CLEAR_BLOCKLIST_LIST'
}
export const BlockListActions = {
    getBlockListlist: (params) => ({
        type: BlockListTypes.GET_BLOCKLIST_LIST,
        params
    }),
    saveBlockListlistResponse: (data) => ({
        type: BlockListTypes.SAVE_BLOCKLIST_LIST_RESPONSE,
        data
    }),
    clearBlockListlist: () => ({
        type: BlockListTypes.CLEAR_BLOCKLIST_LIST
    })
}

export const BlockListEmailTypes = {
    GET_BLOCKLISTEMAIL_LIST: 'GET_BLOCKLISTEMAIL_LIST',
    SAVE_BLOCKLISTEMAIL_LIST_RESPONSE: 'SAVE_BLOCKLISTEMAIL_LIST_RESPONSE',
    CLEAR_BLOCKLISTEMAIL_LIST: 'CLEAR_BLOCKLISTEMAIL_LIST'
}
export const BlockListEmailActions = {
    getBlockListType: (params) => ({
        type: BlockListEmailTypes.GET_BLOCKLISTEMAIL_LIST,
        params
    }),
    saveBlockListTypeResponse: (data) => ({
        type: BlockListEmailTypes.SAVE_BLOCKLISTEMAIL_LIST_RESPONSE,
        data
    }),
    clearBlockList: () => ({
        type: BlockListEmailTypes.CLEAR_BLOCKLISTEMAIL_LIST
    })
}

export const BlockListPhoneTypes = {
    GET_BLOCKLISTPHONE_LIST: 'GET_BLOCKLISTPHONE_LIST',
    SAVE_BLOCKLISTPHONE_LIST_RESPONSE: 'SAVE_BLOCKLISTPHONE_LIST_RESPONSE',
    CLEAR_BLOCKLISTPHONE_LIST: 'CLEAR_BLOCKLISTPHONE_LIST'
}
export const BlockListPhoneActions = {
    getBlockListlist: (params) => ({
        type: BlockListPhoneTypes.GET_BLOCKLISTPHONE_LIST,
        params
    }),
    saveBlockListlistResponse: (data) => ({
        type: BlockListPhoneTypes.SAVE_BLOCKLISTPHONE_LIST_RESPONSE,
        data
    }),
    clearBlockListlist: () => ({
        type: BlockListPhoneTypes.CLEAR_BLOCKLISTPHONE_LIST
    })
}

  export const BlockListUploadTypes = {
    GET_BLOCKLISTUPLOAD_LIST: 'GET_BLOCKLISTUPLOAD_LIST',
    SAVE_BLOCKLISTUPLOAD_LIST_RESPONSE: 'SAVE_BLOCKLISTUPLOAD_LIST_RESPONSE',
    CLEAR_BLOCKLISTUPLOAD_LIST: 'CLEAR_BLOCKLISTUPLOAD_LIST'
  }
  
export const BlockListUploadActions = {
    getBlockListUpload: (data) => ({
      type: BlockListUploadTypes.GET_BLOCKLISTUPLOAD_LIST,
      payload: data
    }),
    saveBlockListUploadResponse: (data) => ({
      type: BlockListUploadTypes.SAVE_BLOCKLISTUPLOAD_LIST_RESPONSE,
      data
    }),
    clearBlockListUpload: () => ({
      type: BlockListUploadTypes.CLEAR_BLOCKLISTUPLOAD_LIST
    })
}

export const BlockListUpdateTypes = {
    GET_BLOCKLIST_UPDATE_LIST: 'GET_BLOCKLIST_UPDATE_LIST',
    SAVE_BLOCKLIST_UPDATE_LIST_RESPONSE: 'SAVE_BLOCKLIST_UPDATE_LIST_RESPONSE',
    CLEAR_BLOCKLIST_UPDATE_LIST: 'CLEAR_BLOCKLIST_UPDATE_LIST'
}
export const BlockListUpdateActions = {
    getBlockListlist: (id, params) => ({
        type: BlockListUpdateTypes.GET_BLOCKLIST_UPDATE_LIST,
        payload: {id, params}
    }),
    saveBlockListlistResponse: (data) => ({
        type: BlockListUpdateTypes.SAVE_BLOCKLIST_UPDATE_LIST_RESPONSE,
        data
    }),
    clearUpdateBlockList: () => ({
        type: BlockListUpdateTypes.CLEAR_BLOCKLIST_UPDATE_LIST
    })
}

export const BlockListEditTypes = {
    GET_BLOCKLIST_EDIT_LIST: 'GET_BLOCKLIST_EDIT_LIST',
    SAVE_BLOCKLIST_EDIT_LIST_RESPONSE: 'SAVE_BLOCKLIST_EDIT_LIST_RESPONSE',
    CLEAR_BLOCKLIST_EDIT_LIST: 'CLEAR_BLOCKLIST_EDIT_LIST'
}
export const BlockListEditActions = {
    getBlockListlist: (id) => ({
        type: BlockListEditTypes.GET_BLOCKLIST_EDIT_LIST,
        id
    }),
    saveBlockListlistResponse: (data) => ({
        type: BlockListEditTypes.SAVE_BLOCKLIST_EDIT_LIST_RESPONSE,
        data
    }),
    clearBlockListlist: () => ({
        type: BlockListEditTypes.CLEAR_BLOCKLIST_EDIT_LIST
    })
}

export const BlockListDeleteTypes = {
    GET_BLOCKLIST_DELETE_LIST: 'GET_BLOCKLIST_DELETE_LIST',
    SAVE_BLOCKLIST_DELETE_LIST_RESPONSE: 'SAVE_BLOCKLIST_DELETE_LIST_RESPONSE',
    CLEAR_BLOCKLIST_DELETE_LIST: 'CLEAR_BLOCKLIST_DELETE_LIST',
    BLOCKLIST_DELETE_ERROR: 'BLOCKLIST_DELETE_ERROR',
}
export const BlockListDeleteActions = {
    DeletegetBlocklist: (id) => ({
        type: BlockListDeleteTypes.GET_BLOCKLIST_DELETE_LIST,
        id
    }),
    saveBlockListlistDeleteResponse: (data) => ({
        type: BlockListDeleteTypes.SAVE_BLOCKLIST_DELETE_LIST_RESPONSE,
        data
    }),
    DelteBlacklistError: () => ({
        type: BlockListDeleteTypes.BLOCKLIST_DELETE_ERROR
    }),
    clearBlockListlist: () => ({
        type: BlockListDeleteTypes.CLEAR_BLOCKLIST_DELETE_LIST
    })
}

export const EmailBlocklistsTypes = {
    ADD_EMAIL_BLACKLIST_INIt: 'ADD_BLOCKLIST_LIST',
    ADD_EMAIL_BLACKLIST_SUCCESS: 'ADD_EMAIL_BLACKLIST_SUCCESS',
    ADD_EMAIL_BLACKLIST_ERROR: 'ADD_EMAIL_BLACKLIST_ERROR',
    ADD_EMAIL_BLACKLIST_CLEAR: 'ADD_EMAIL_BLACKLIST_CLEAR'
}
export const EmailBlockListActions = {
    addEmailBlacklistInit: (data) => ({
        type: EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_INIt,
        payload: data
    }),
    addEmailBlacklistSuccess: (data) => ({
        type: EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_SUCCESS,
        data
    }),
    addEmailBlacklistError: () => ({
        type: EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_ERROR
    }),
    addEmailBlacklistClear: () => ({
        type: EmailBlocklistsTypes.ADD_EMAIL_BLACKLIST_CLEAR
    })
}

export const DeleteClientTypes = {
    DELETE_CLIENT: 'DELETE_CLIENT',
    DELETE_CLIENT_SUCCESS: 'DELETE_CLIENT_SUCCESS',
    DELETE_CLIENT_ERROR: 'DELETE_CLIENT_ERROR',
    DELETE_CLIENT_CLEAR: 'DELETE_CLIENT_CLEAR'
}
export const DeleteClientActions = {
    DeleteClientInit: (data) => ({
        type: DeleteClientTypes.DELETE_CLIENT,
        payload: data
    }),
    DeleteClientSuccess: (data) => ({
        type: DeleteClientTypes.DELETE_CLIENT_SUCCESS,
        data
    }),
    DeleteClientError: () => ({
        type: DeleteClientTypes.DELETE_CLIENT_ERROR
    }),
    DeleteClientClear: () => ({
        type: DeleteClientTypes.DELETE_CLIENT_CLEAR
    })
}