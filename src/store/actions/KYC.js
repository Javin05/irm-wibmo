export const KYCTypes = {
  GET_KYC_LIST: "GET_KYC_LIST",
  SAVE_KYC_LIST_RESPONSE: "SAVE_KYC_LIST_RESPONSE",
  CLEAR_KYC_LIST: "CLEAR_KYC_LIST",
};
export const KYCUSerTypes = {
  KYC_USER_INIT: "KYC_USER_INIT",
  KYC_USER_RESPONSE_SUCCESS: "KYC_USER_RESPONSE_SUCCESS",
  KYC_USER_CLEAR: "KYC_USER_CLEAR",
  KYC_USER_ERROR: "KYC_USER_ERROR",
};

export const KYCActions = {
  getKYClist: (params) => ({
    type: KYCTypes.GET_KYC_LIST,
    params,
  }),
  saveKYClistResponse: (data) => ({
    type: KYCTypes.SAVE_KYC_LIST_RESPONSE,
    data,
  }),
  clearKYClist: () => ({
    type: KYCTypes.CLEAR_KYC_LIST,
  }),
};
export const KYCUserAction = {
  KYCUser_INIT: (params) => (
    {
    type: KYCUSerTypes.KYC_USER_INIT,
    params,
  }),
  KYCUser_SUCCESS: (data) => ({
    type: KYCUSerTypes.KYC_USER_RESPONSE_SUCCESS,
    data,
  }),
  KYCUser_CLEAR: () => ({
    type: KYCUSerTypes.KYC_USER_CLEAR,
  }),
  KYCUser_ERROR: () => ({
    type: KYCUSerTypes.KYC_USER_ERROR,
  }),
};

export const KYCAddTypes = {
  KYC_POST: "KYC_POST",
  KYC_POST_RESPONSE: "KYC_POST_RESPONSE",
  KYC_POST_CLEAR: "KYC_POST_CLEAR",
};

export const KYCAddAction = {
  KYCAdd: (data) => {
    return {
      type: KYCAddTypes.KYC_POST,
      payload: data,
    };
  },
  saveKYCResponse: (data) => ({
    type: KYCAddTypes.KYC_POST_RESPONSE,
    data,
  }),
  clearKYC: () => ({
    type: KYCAddTypes.KYC_POST_CLEAR,
  }),
};

export const KYCAddAllTypes = {
  KYC_PUT: "KYC_PUT",
  KYC_PUT_RESPONSE: "KYC_PUT_RESPONSE",
  KYC_PUT_CLEAR: "KYC_PUT_CLEAR",
};

export const KYCAddALLAction = {
  KYCAddAllData: (id, params) => {
    return {
      type: KYCAddAllTypes.KYC_PUT,
      payload: { id, params },
    };
  },
  saveKYCAlldataResponse: (data) => ({
    type: KYCAddAllTypes.KYC_PUT_RESPONSE,
    data,
  }),
  clearKYCAllData: () => ({
    type: KYCAddAllTypes.KYC_PUT_CLEAR,
  }),
};

export const KYCphoneVerifyTypes = {
  KYC_PHONE_VERIFY: "KYC_PHONE_VERIFY",
  KYC_PHONE_VERIFY_RESPONSE: "KYC_KYC_PHONE_VERIFY_RESPONSE",
  KYC_PHONE_VERIFY_CLEAR: "KYC_KYC_PHONE_VERIFY_CLEAR",
};

export const KYCphoneVerify = {
  KYCphoneVerifyList: (data) => {
    return {
      type: KYCphoneVerifyTypes.KYC_PHONE_VERIFY,
      payload: data,
    };
  },
  KYCphoneVerifyResponse: (data) => ({
    type: KYCphoneVerifyTypes.KYC_PHONE_VERIFY_RESPONSE,
    data,
  }),
  clearKYCphoneVerify: () => ({
    type: KYCphoneVerifyTypes.KYC_PHONE_VERIFY_CLEAR,
  }),
};

export const KYCphoneOtpTypes = {
  KYC_PHONE_OTP: "KYC_PHONE_OTP",
  KYC_PHONE_OTP_RESPONSE: "KYC_KYC_PHONE_OTP_RESPONSE",
  KYC_PHONE_OTP_CLEAR: "KYC_KYC_PHONE_OTP_CLEAR",
};

export const KYCphoneOtpAction = {
  KYCphoneOTP: (data) => {
    return {
      type: KYCphoneOtpTypes.KYC_PHONE_OTP,
      payload: data,
    };
  },
  KYCphoneOTPResponse: (data) => ({
    type: KYCphoneOtpTypes.KYC_PHONE_OTP_RESPONSE,
    data,
  }),
  clearKYCphoneOTP: () => ({
    type: KYCphoneOtpTypes.KYC_PHONE_OTP_CLEAR,
  }),
};

export const KYCpanTypes = {
  KYC_PAN_VERIFY: "KYC_PAN_VERIFY",
  KYC_PAN_VERIFY_RESPONSE: "KYC_PAN_VERIFY_RESPONSE",
  KYC_PAN_VERIFY_CLEAR: "KYC_PAN_VERIFY_CLEAR",
};

export const KYCpanAction = {
  KYCpanVerify: (data) => {
    return {
      type: KYCpanTypes.KYC_PAN_VERIFY,
      payload: data,
    };
  },
  KYCpanVerifyResponse: (data) => ({
    type: KYCpanTypes.KYC_PAN_VERIFY_RESPONSE,
    data,
  }),
  clearKYCpanVerify: () => ({
    type: KYCpanTypes.KYC_PAN_VERIFY_CLEAR,
  }),
};

export const KYCgstinTypes = {
  KYC_GSTIN_VERIFY: "KYC_GSTIN_VERIFY",
  KYC_GSTIN_VERIFY_RESPONSE: "KYC_GSTIN_VERIFY_RESPONSE",
  KYC_GSTIN_VERIFY_CLEAR: "KYC_GSTIN_VERIFY_CLEAR",
};

export const KYCgstinAction = {
  KYCgstinVerify: (data) => {
    return {
      type: KYCgstinTypes.KYC_GSTIN_VERIFY,
      payload: data,
    };
  },
  KYCgstinVerifyResponse: (data) => ({
    type: KYCgstinTypes.KYC_GSTIN_VERIFY_RESPONSE,
    data,
  }),
  clearKYCgstinVerify: () => ({
    type: KYCgstinTypes.KYC_GSTIN_VERIFY_CLEAR,
  }),
};

export const KYCcinTypes = {
  KYC_CIN_VERIFY: "KYC_CIN_VERIFY",
  KYC_CIN_VERIFY_RESPONSE: "KYC_CIN_VERIFY_RESPONSE",
  KYC_CIN_VERIFY_CLEAR: "KYC_CIN_VERIFY_CLEAR",
};

export const KYCcinAction = {
  KYCcinVerify: (data) => {
    return {
      type: KYCcinTypes.KYC_CIN_VERIFY,
      payload: data,
    };
  },
  KYCcinVerifyResponse: (data) => ({
    type: KYCcinTypes.KYC_CIN_VERIFY_RESPONSE,
    data,
  }),
  clearKYCcinVerify: () => ({
    type: KYCcinTypes.KYC_CIN_VERIFY_CLEAR,
  }),
};

export const KYCPersonalpanTypes = {
  KYC_PERSONAL_PAN_VERIFY: "KYC_PERSONAL_PAN_VERIFY",
  KYC_PERSONAL_PAN_VERIFY_RESPONSE: "KYC_PERSONAL_PAN_VERIFY_RESPONSE",
  KYC_PERSONAL_PAN_VERIFY_CLEAR: "KYC_PERSONAL_PAN_VERIFY_CLEAR",
};

export const KYCPersonalpanAction = {
  KYCpersonalpanVerify: (data) => {
    return {
      type: KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY,
      payload: data,
    };
  },
  KYCpersonalpanVerifyResponse: (data) => ({
    type: KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY_RESPONSE,
    data,
  }),
  clearKYCpersonalpanVerify: () => ({
    type: KYCPersonalpanTypes.KYC_PERSONAL_PAN_VERIFY_CLEAR,
  }),
};

export const KYCemailTypes = {
  KYC_EMAIL_VERIFY: "KYC_EMAIL_VERIFY",
  KYC_EMAIL_VERIFY_RESPONSE: "KYC_EMAIL_VERIFY_RESPONSE",
  KYC_EMAIL_VERIFY_CLEAR: "KYC_EMAIL_VERIFY_CLEAR",
};

export const KYCemailAction = {
  KYCemailVerify: (data) => {
    return {
      type: KYCemailTypes.KYC_EMAIL_VERIFY,
      payload: data,
    };
  },
  KYCemailVerifyResponse: (data) => ({
    type: KYCemailTypes.KYC_EMAIL_VERIFY_RESPONSE,
    data,
  }),
  clearKYCemailVerify: () => ({
    type: KYCemailTypes.KYC_EMAIL_VERIFY_CLEAR,
  }),
};

export const KYCemailOtpTypes = {
  KYC_EMAIL_OTP_VERIFY: "KYC_EMAIL_OTP_VERIFY",
  KYC_EMAIL_OTP_VERIFY_RESPONSE: "KYC_EMAIL_OTP_VERIFY_RESPONSE",
  KYC_EMAIL_OTP_VERIFY_CLEAR: "KYC_EMAIL_OTP_VERIFY_CLEAR",
};

export const KYCemailOtpAction = {
  KYCemailOtpVerify: (data) => {
    return {
      type: KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY,
      payload: data,
    };
  },
  KYCemailOtpVerifyResponse: (data) => ({
    type: KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY_RESPONSE,
    data,
  }),
  clearKYCemailOtpVerify: () => ({
    type: KYCemailOtpTypes.KYC_EMAIL_OTP_VERIFY_CLEAR,
  }),
};

export const KYCaccountDetailsType = {
  KYC_ACCOUNT_DETAIL_VERIFY: "KYC_ACCOUNT_DETAIL_VERIFY",
  KYC_ACCOUNT_DETAIL_VERIFY_RESPONSE: "KYC_ACCOUNT_DETAIL_VERIFY_RESPONSE",
  KYC_ACCOUNT_DETAIL_VERIFY_CLEAR: "KYC_ACCOUNT_DETAIL_VERIFY_CLEAR",
};

export const KYCaccountAction = {
  KYCaccountDetail: (data) => {
    return {
      type: KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY,
      payload: data,
    };
  },
  KYCaccountDetailResponse: (data) => ({
    type: KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY_RESPONSE,
    data,
  }),
  clearKYCaccountDetail: () => ({
    type: KYCaccountDetailsType.KYC_ACCOUNT_DETAIL_VERIFY_CLEAR,
  }),
};

export const KYCAdharNumberType = {
  KYC_ADHAARNUMBER_VERIFY: "KYC_ADHAARNUMBER_VERIFY",
  KYC_ADHAARNUMBER_VERIFY_RESPONSE: "KYC_ADHAARNUMBER_VERIFY_RESPONSE",
  KYC_ADHAARNUMBER_VERIFY_CLEAR: "KYC_ADHAARNUMBER_VERIFY_CLEAR",
};

export const KYCAdharNumberAction = {
  KYCAdhaarNumber: (data) => {
    return {
      type: KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY,
      payload: data,
    };
  },
  KYCAdhaarNumberResponse: (data) => ({
    type: KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY_RESPONSE,
    data,
  }),
  clearKYCAdhaarNumber: () => ({
    type: KYCAdharNumberType.KYC_ADHAARNUMBER_VERIFY_CLEAR,
  }),
};

export const FullKycValueType = {
  FULL_KYC_VERIFY: "FULL_KYC_VERIFY",
  FULL_KYC_VERIFY_RESPONSE: "FULL_KYC_VERIFY_RESPONSE",
  FULL_KYC_VERIFY_CLEAR: "KYC_ADHAARNUMBER_VERIFY_CLEAR",
};

export const FullKycValueAction = {
  FullKycValue: (id , params) => {
    return {
      type: FullKycValueType.FULL_KYC_VERIFY,
      payload: { id, params },
    };
  },
  FullKycValueResponse: (data) => ({
    type: FullKycValueType.FULL_KYC_VERIFY_RESPONSE,
    data,
  }),
  clearFullKycValue: () => ({
    type: FullKycValueType.FULL_KYC_VERIFY_CLEAR,
  }),
}

export const AadhaarFrontType = {
  AADHAAR_FRONT_VERIFY: "AADHAAR_FRONT_VERIFY",
  AADHAAR_FRONT_VERIFY_RESPONSE: "AADHAAR_FRONT_VERIFY_RESPONSE",
  AADHAAR_FRONT_VERIFY_CLEAR: "AADHAAR_FRONT_VERIFY_CLEAR"
};

export const AadhaarFrontAction = {
  AadhaarFrontValue: (params) => {
    return {
      type: AadhaarFrontType.AADHAAR_FRONT_VERIFY,
      payload: params ,
    };
  },
  AadhaarFrontValueResponse: (data) => ({
    type: AadhaarFrontType.AADHAAR_FRONT_VERIFY_RESPONSE,
    data,
  }),
  clearAadhaarFrontValue: () => ({
    type: AadhaarFrontType.AADHAAR_FRONT_VERIFY_CLEAR,
  }),
}

export const AadhaarBackType = {
  AADHAAR_BACK_VERIFY: "AADHAAR_BACK_VERIFY",
  AADHAAR_BACK_VERIFY_RESPONSE: "AADHAAR_BACK_VERIFY_RESPONSE",
  AADHAAR_BACK_VERIFY_CLEAR: "AADHAAR_BACK_VERIFY_CLEAR"
};

export const AadhaarBackAction = {
  AadhaarBackValue: (params) => {
    return {
      type: AadhaarBackType.AADHAAR_BACK_VERIFY,
      payload: params ,
    };
  },
  AadhaarBackValueResponse: (data) => ({
    type: AadhaarBackType.AADHAAR_BACK_VERIFY_RESPONSE,
    data,
  }),
  clearAadhaarBackValue: () => ({
    type: AadhaarBackType.AADHAAR_BACK_VERIFY_CLEAR,
  }),
}

export const CommomFileType = {
  COMMON_FILE_VERIFY: "COMMON_FILE_VERIFY",
  COMMON_FILE_VERIFY_RESPONSE: "COMMON_FILE_VERIFY_RESPONSE",
  COMMON_FILE_VERIFY_CLEAR: "COMMON_FILE_VERIFY_CLEAR"
};

export const CommomFileAction = {
  CommonFile: (params) => {
    return {
      type: CommomFileType.COMMON_FILE_VERIFY,
      payload: params ,
    };
  },
  CommonFileResponse: (data) => ({
    type: CommomFileType.COMMON_FILE_VERIFY_RESPONSE,
    data,
  }),
  clearCommonFile: () => ({
    type: CommomFileType.COMMON_FILE_VERIFY_CLEAR,
  })
}

export const DistanceType = {
  DISTANCE_DATA: "DISTANCE_DATA",
  DISTANCE_DATA_RESPONSE: "DISTANCE_DATA_RESPONSE",
  DISTANCE_DATA_CLEAR: "DISTANCE_DATA_CLEAR"
};

export const DistanceAction = {
  Distance: (params) => {
    return {
      type: DistanceType.DISTANCE_DATA,
      params ,
    };
  },
  DistanceResponse: (data) => ({
    type: DistanceType.DISTANCE_DATA_RESPONSE,
    data,
  }),
  clearDistance: () => ({
    type: DistanceType.DISTANCE_DATA_CLEAR,
  })
}

export const AllDasboardDataType = {
  ALL_DASHBOARD: "ALL_DASHBOARD",
  ALL_DASHBOARD_RESPONSE: "ALL_DASHBOARD_RESPONSE",
  ALL_DASHBOARD_CLEAR: "ALL_DASHBOARD_CLEAR"
};

export const AllDasboardDataAction = {
  AllDashboard: (params) => {
    return {
      type: AllDasboardDataType.ALL_DASHBOARD,
      params ,
    };
  },
  AllDashboardResponse: (data) => ({
    type: AllDasboardDataType.ALL_DASHBOARD_RESPONSE,
    data,
  }),
  clearAllDashboard: () => ({
    type: AllDasboardDataType.ALL_DASHBOARD_CLEAR,
  })
}

export const DasboardAadharType = {
  DAHBOARD_AADHAAR: "DAHBOARD_AADHAAR",
  DAHBOARD_AADHAAR_RESPONSE: "DAHBOARD_AADHAAR_RESPONSE",
  DAHBOARD_AADHAAR_CLEAR: "DAHBOARD_AADHAAR_CLEAR"
};

export const DasboardAadharAction = {
  DashboardAadhaar: (params) => {
    return {
      type: DasboardAadharType.DAHBOARD_AADHAAR,
      params ,
    }
  },
  DashboardAadhaarResponse: (data) => ({
    type: DasboardAadharType.DAHBOARD_AADHAAR_RESPONSE,
    data,
  }),
  clearDashboardAadhaar: () => ({
    type: DasboardAadharType.DAHBOARD_AADHAAR_CLEAR,
  })
}

export const DasboardPanType = {
  DAHBOARD_PAN: "DAHBOARD_PAN",
  DAHBOARD_PAN_RESPONSE: "DAHBOARD_PAN_RESPONSE",
  DAHBOARD_PAN_CLEAR: "DAHBOARD_PAN_CLEAR"
};

export const DasboardPanAction = {
  DashboardPAN: (params) => {
    return {
      type: DasboardPanType.DAHBOARD_PAN,
      params ,
    }
  },
  DashboardPANResponse: (data) => ({
    type: DasboardPanType.DAHBOARD_PAN_RESPONSE,
    data,
  }),
  clearDashboardPAN: () => ({
    type: DasboardPanType.DAHBOARD_PAN_CLEAR,
  })
}

export const DasboardCinType = {
  DAHBOARD_CIN: "DAHBOARD_CIN",
  DAHBOARD_CIN_RESPONSE: "DAHBOARD_CIN_RESPONSE",
  DAHBOARD_CIN_CLEAR: "DAHBOARD_CIN_CLEAR"
};

export const DasboardCinAction = {
  DashboardCIN: (params) => {
    return {
      type: DasboardCinType.DAHBOARD_CIN,
      params ,
    }
  },
  DashboardCINResponse: (data) => ({
    type: DasboardCinType.DAHBOARD_CIN_RESPONSE,
    data,
  }),
  clearDashboardCIN: () => ({
    type: DasboardCinType.DAHBOARD_CIN_CLEAR,
  })
}

export const DasboardAPCType = {
  DAHBOARD_APC: "DAHBOARD_APC",
  DAHBOARD_APC_RESPONSE: "DAHBOARD_APC_RESPONSE",
  DAHBOARD_APC_CLEAR: "DAHBOARD_APC_CLEAR"
};

export const DasboardAPCAction = {
  DashboardAPC: (id, params) => {
    return {
      type: DasboardAPCType.DAHBOARD_APC,
      payload:{id, params} ,
    }
  },
  DashboardAPCResponse: (data) => ({
    type: DasboardAPCType.DAHBOARD_APC_RESPONSE,
    data,
  }),
  clearDashboardAPC: () => ({
    type: DasboardAPCType.DAHBOARD_APC_CLEAR,
  })
}

export const DasboardAPCTwoType = {
  DAHBOARD_APCTWO: "DAHBOARD_APCTWO",
  DAHBOARD_APCTWO_RESPONSE: "DAHBOARD_APCTWO_RESPONSE",
  DAHBOARD_APCTWO_CLEAR: "DAHBOARD_APCTWO_CLEAR"
};

export const DasboardAPCtwoAction = {
  DashboardAPCTwo: (id, params) => {
    return {
      type: DasboardAPCTwoType.DAHBOARD_APCTWO,
      payload:{id, params}
    }
  },
  DashboardAPCTwoResponse: (data) => ({
    type: DasboardAPCTwoType.DAHBOARD_APCTWO_RESPONSE,
    data,
  }),
  clearDashboardAPCTwo: () => ({
    type: DasboardAPCTwoType.DAHBOARD_APCTWO_CLEAR,
  })
}

export const KycStatusType = {
  KYC_STATUS: "KYC_STATUS",
  KYC_STATUS_RESPONSE: "KYC_STATUS_RESPONSE",
  KYC_STATUS_CLEAR: "KYC_STATUS_CLEAR"
};

export const KycStatusAction = {
  KycStatus: (id, params) => {
    return {
      type: KycStatusType.KYC_STATUS,
      payload:{id, params}
    }
  },
  KycStatusResponse: (data) => ({
    type: KycStatusType.KYC_STATUS_RESPONSE,
    data,
  }),
  clearKycStatus: () => ({
    type: KycStatusType.KYC_STATUS_CLEAR,
  })
}

export const KycScoreType = {
  KYC_SCORE: "KYC_SCORE",
  KYC_SCORE_RESPONSE: "KYC_SCORE_RESPONSE",
  KYC_SCORE_CLEAR: "KYC_SCORE_CLEAR"
};

export const KycScoreAction = {
  KycScore: (id) => {
    return {
      type: KycScoreType.KYC_SCORE,
      payload:{id}
    }
  },
  KycScoreResponse: (data) => ({
    type: KycScoreType.KYC_SCORE_RESPONSE,
    data,
  }),
  clearKycScore: () => ({
    type: KycScoreType.KYC_SCORE_CLEAR,
  })
}

export const IndidualPanUploadType = {
  INDIDUAL_PAN_UPLOAD: "INDIDUAL_PAN_UPLOAD",
  INDIDUAL_PAN_UPLOAD_RESPONSE: "INDIDUAL_PAN_UPLOAD_RESPONSE",
  INDIDUAL_PAN_UPLOAD_CLEAR: "INDIDUAL_PAN_UPLOAD_CLEAR"
};

export const IndidualPanUploadAction = {
  IndidualPanUpload: (params) => {
    return {
      type: IndidualPanUploadType.INDIDUAL_PAN_UPLOAD,
      payload: params
    }
  },
  IndidualPanUploadResponse: (data) => ({
    type: IndidualPanUploadType.INDIDUAL_PAN_UPLOAD_RESPONSE,
    data,
  }),
  clearIndidualPanUpload: () => ({
    type: IndidualPanUploadType.INDIDUAL_PAN_UPLOAD_CLEAR,
  })
}

export const form80UploadType = {
  FPORM_80_G: "FPORM_80_G",
  FPORM_80_G_RESPONSE: "FPORM_80_G_RESPONSE",
  FPORM_80_G_CLEAR: "FPORM_80_G_CLEAR"
};

export const form80UploadAction = {
  form80Upload: (params) => {
    return {
      type: form80UploadType.FPORM_80_G,
      payload: params
    }
  },
  form80UploadResponse: (data) => ({
    type: form80UploadType.FPORM_80_G_RESPONSE,
    data,
  }),
  clearform80Upload: () => ({
    type: form80UploadType.FPORM_80_G_CLEAR,
  })
}

export const form12UploadType = {
  FPORM_12_G: "FPORM_12_G",
  FPORM_12_G_RESPONSE: "FPORM_12_G_RESPONSE",
  FPORM_12_G_CLEAR: "FPORM_12_G_CLEAR"
};

export const form12UploadAction = {
  form12Upload: (params) => {
    return {
      type: form12UploadType.FPORM_12_G,
      payload: params
    }
  },
  form12UploadResponse: (data) => ({
    type: form12UploadType.FPORM_12_G_RESPONSE,
    data,
  }),
  clearform12Upload: () => ({
    type: form12UploadType.FPORM_12_G_CLEAR,
  })
}

export const AddressDocType = {
  ADDRESS_DOC_UPLOAD: "ADDRESS_DOC_UPLOAD",
  ADDRESS_DOC_UPLOAD_RESPONSE: "ADDRESS_DOC_UPLOAD_RESPONSE",
  ADDRESS_DOC_UPLOAD_CLEAR: "ADDRESS_DOC_UPLOAD_CLEAR"
};

export const AddressDocAction = {
  AddressDoc: (params) => {
    return {
      type: AddressDocType.ADDRESS_DOC_UPLOAD,
      payload: params
    }
  },
  AddressDocResponse: (data) => ({
    type: AddressDocType.ADDRESS_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearAddressDoc: () => ({
    type: AddressDocType.ADDRESS_DOC_UPLOAD_CLEAR,
  })
}

export const DashboardDropdownType = {
  DASHBOARD_DROPDOWN: "DASHBOARD_DROPDOWN",
  DASHBOARD_DROPDOWN_RESPONSE: "DASHBOARD_DROPDOWN_RESPONSE",
  DASHBOARD_DROPDOWN_CLEAR: "DASHBOARD_DROPDOWN_CLEAR"
}

export const DashboardDropdownAction = {
  DashboardDropdown: (id) => {
    return {
      type: DashboardDropdownType.DASHBOARD_DROPDOWN,
      payload:{id}
    }
  },
  DashboardDropdownResponse: (data) => ({
    type: DashboardDropdownType.DASHBOARD_DROPDOWN_RESPONSE,
    data,
  }),
  clearDashboardDropdown: () => ({
    type: DashboardDropdownType.DASHBOARD_DROPDOWN_CLEAR,
  })
}

export const linkAnalyticsType = {
  LINK_ANALYTICS: "LINK_ANALYTICS",
  LINK_ANALYTICS_RESPONSE: "LINK_ANALYTICS_RESPONSE",
  LINK_ANALYTICS_CLEAR: "LINK_ANALYTICS_CLEAR"
}

export const linkAnalyticsAction = {
  linkAnalytics: (id) => {
    return {
      type: linkAnalyticsType.LINK_ANALYTICS,
      payload:{id}
    }
  },
  linkAnalyticsResponse: (data) => ({
    type: linkAnalyticsType.LINK_ANALYTICS_RESPONSE,
    data,
  }),
  clearlinkAnalytics: () => ({
    type: linkAnalyticsType.LINK_ANALYTICS_CLEAR,
  })
}

export const VideoKYCType = {
  VIDEO_KYC: "VIDEO_KYC",
  VIDEO_KYC_RESPONSE: "VIDEO_KYC_RESPONSE",
  VIDEO_KYC_CLEAR: "VIDEO_KYC_CLEAR"
}

export const VideoKYCAction = {
  VideoKYC: (id) => {
    return {
      type: VideoKYCType.VIDEO_KYC,
      payload:{id}
    }
  },
  VideoKYCResponse: (data) => ({
    type: VideoKYCType.VIDEO_KYC_RESPONSE,
    data,
  }),
  clearVideoKYC: () => ({
    type: VideoKYCType.VIDEO_KYC_CLEAR,
  })
}

export const UENType = {
  UEN_NUMBER: "UEN_NUMBER",
  UEN_NUMBER_RESPONSE: "UEN_NUMBER_RESPONSE",
  UEN_NUMBER_CLEAR: "UEN_NUMBER_CLEAR"
}

export const UENAction = {
  UEN: (params) => {
    return {
      type: UENType.UEN_NUMBER,
      payload:{params}
    }
  },
  UENResponse: (data) => ({
    type: UENType.UEN_NUMBER_RESPONSE,
    data,
  }),
  clearUEN: () => ({
    type: UENType.UEN_NUMBER_CLEAR,
  })
}

export const UENdashboardType = {
  UEN_NUMBER_DASHBOARD: "UEN_NUMBER_DASHBOARD",
  UEN_NUMBER_DASHBOARD_RESPONSE: "UEN_NUMBER_DASHBOARD_RESPONSE",
  UEN_NUMBER_DASHBOARD_CLEAR: "UEN_NUMBER_DASHBOARD_CLEAR"
}

export const UENdashboardAction = {
  UENdashboard: (id) => {
    return {
      type: UENdashboardType.UEN_NUMBER_DASHBOARD,
      payload:{id}
    }
  },
  UENdashboardResponse: (data) => ({
    type: UENdashboardType.UEN_NUMBER_DASHBOARD_RESPONSE,
    data,
  }),
  clearUENdashboard: () => ({
    type: UENdashboardType.UEN_NUMBER_DASHBOARD_CLEAR,
  })
}

export const EntityType = {
  ENTITY_GET: "ENTITY_GET",
  ENTITY_GET_RESPONSE: "ENTITY_GET_RESPONSE",
  ENTITY_GET_CLEAR: "ENTITY_GET_CLEAR"
}

export const EntityAction = {
  Entity: () => {
    return {
      type: EntityType.ENTITY_GET,
    }
  },
  EntityResponse: (data) => ({
    type: EntityType.ENTITY_GET_RESPONSE,
    data,
  }),
  clearEntity: () => ({
    type: EntityType.ENTITY_GET_CLEAR,
  })
}

export const EntityValueType = {
  ENTITY_VALUE_GET: "ENTITY_VALUE_GET",
  ENTITY_VALUE_GET_RESPONSE: "ENTITY_VALUE_GET_RESPONSE",
  ENTITY_VALUE_GET_CLEAR: "ENTITY_VALUE_GET_CLEAR"
}

export const EntityValueAction = {
  EntityValueData: (params) => {
    return {
      type: EntityValueType.ENTITY_VALUE_GET,
      params
    }
  },
  EntityValueResponse: (data) => ({
    type: EntityValueType.ENTITY_VALUE_GET_RESPONSE,
    data,
  }),
  clearEntityValue: () => ({
    type: EntityValueType.ENTITY_VALUE_GET_CLEAR,
  })
}

export const CategoryValueType = {
  CATEGORY_VALUE_GET: "CATEGORY_VALUE_GET",
  CATEGORY_VALUE_GET_RESPONSE: "CATEGORY_VALUE_GET_RESPONSE",
  CATEGORY_VALUE_GET_CLEAR: "CATEGORY_VALUE_GET_CLEAR"
}

export const CategoryValueAction = {
  CategoryValue: (params) => {
    return {
      type: CategoryValueType.CATEGORY_VALUE_GET,
      params
    }
  },
  CategoryValueResponse: (data) => ({
    type: CategoryValueType.CATEGORY_VALUE_GET_RESPONSE,
    data,
  }),
  clearCategoryValue: () => ({
    type: CategoryValueType.CATEGORY_VALUE_GET_CLEAR,
  })
}

export const SubCategoryValueType = {
  SUB_CATEGORY_VALUE_GET: "SUB_CATEGORY_VALUE_GET",
  SUB_CATEGORY_VALUE_GET_RESPONSE: "SUB_CATEGORY_VALUE_GET_RESPONSE",
  SUB_CATEGORY_VALUE_GET_CLEAR: "SUB_CATEGORY_VALUE_GET_CLEAR"
}

export const SubCategoryValueAction = {
  SubCategoryValue: (params) => {
    return {
      type: SubCategoryValueType.SUB_CATEGORY_VALUE_GET,
      params
    }
  },
  SubCategoryValueResponse: (data) => ({
    type: SubCategoryValueType.SUB_CATEGORY_VALUE_GET_RESPONSE,
    data,
  }),
  clearSubCategoryValue: () => ({
    type: SubCategoryValueType.SUB_CATEGORY_VALUE_GET_CLEAR,
  })
}

export const PinCodeValueType = {
  PINCODE_VALUE_GET: "PINCODE_VALUE_GET",
  PINCODE_VALUE_GET_RESPONSE: "PINCODE_VALUE_GET_RESPONSE",
  PINCODE_VALUE_GET_CLEAR: "PINCODE_VALUE_GET_CLEAR"
}

export const PinCodeValueAction = {
  PinCodeValue: (payload) => {
    return {
      type: PinCodeValueType.PINCODE_VALUE_GET,
      payload
    }
  },
  PinCodeValueResponse: (data) => ({
    type: PinCodeValueType.PINCODE_VALUE_GET_RESPONSE,
    data,
  }),
  clearPinCodeValue: () => ({
    type: PinCodeValueType.PINCODE_VALUE_GET_CLEAR,
  })
}

export const ExportReportValueType = {
  EXPORT_REPORT_VALUE_GET: "EXPORT_REPORT_VALUE_GET",
  EXPORT_REPORT_VALUE_GET_RESPONSE: "EXPORT_REPORT_VALUE_GET_RESPONSE",
  EXPORT_REPORT_VALUE_GET_CLEAR: "EXPORT_REPORT_VALUE_GET_CLEAR"
}

export const ExportReportValueAction = {
  ExportReportValue: (params) => {
    return {
      type: ExportReportValueType.EXPORT_REPORT_VALUE_GET,
      params
    }
  },
  ExportReportValueResponse: (data) => ({
    type: ExportReportValueType.EXPORT_REPORT_VALUE_GET_RESPONSE,
    data,
  }),
  clearExportReportValue: () => ({
    type: ExportReportValueType.EXPORT_REPORT_VALUE_GET_CLEAR,
  })
}