
export const WebAnalysisTypes = {
    POST_WEBANALYSIS_LIST: 'POST_WEBANALYSIS_LIST',
    SAVE_WEBANALYSIS_LIST_RESPONSE: 'SAVE_WEBANALYSIS_LIST_RESPONSE',
    CLEAR_WEBANALYSIS_LIST: 'CLEAR_WEBANALYSIS_LIST'
  }
  
export const WebAnalysisActions = {
    getWebAnalysis: (data) => ({
      type: WebAnalysisTypes.POST_WEBANALYSIS_LIST,
      payload: data
    }),
    saveWebAnalysisResponse: (data) => ({
      type: WebAnalysisTypes.SAVE_WEBANALYSIS_LIST_RESPONSE,
      data
    }),
    clearWebAnalysis: () => ({
      type: WebAnalysisTypes.CLEAR_WEBANALYSIS_LIST
    })
}


export const ManualWebAnalysisTypes = {
  POST_MANUAL_WEBANALYSIS_LIST: 'POST_MANUAL_WEBANALYSIS_LIST',
  SAVE_MANUAL_WEBANALYSIS_LIST_RESPONSE: 'SAVE_MANUAL_WEBANALYSIS_LIST_RESPONSE',
  CLEAR_MANUAL_WEBANALYSIS_LIST: 'CLEAR_MANUAL_WEBANALYSIS_LIST'
}

export const ManualWebAnalysisActions = {
  postWebAnalysis: (data) => ({
    type: ManualWebAnalysisTypes.POST_MANUAL_WEBANALYSIS_LIST,
    payload: data
  }),
  saveManualWebAnalysisResponse: (data) => ({
    type: ManualWebAnalysisTypes.SAVE_MANUAL_WEBANALYSIS_LIST_RESPONSE,
    data
  }),
  clearManualWebAnalysis: () => ({
    type: ManualWebAnalysisTypes.CLEAR_MANUAL_WEBANALYSIS_LIST
  })
}

export const getWebAnalysisTypes = {
  GET_WEBANALYSIS_LIST: 'GET_WEBANALYSIS_LIST',
  SAVE_GET_WEBANALYSIS_LIST_RESPONSE: 'SAVE_GET_WEBANALYSIS_LIST_RESPONSE',
  CLEAR_GET_WEBANALYSIS_LIST: 'CLEAR_GET_WEBANALYSIS_LIST'
}

export const getWebAnalysisActions = {
  getgetWebAnalysislist: (params) => ({
    type: getWebAnalysisTypes.GET_WEBANALYSIS_LIST,
    params
  }),
  savegetWebAnalysislistResponse: (data) => ({
    type: getWebAnalysisTypes.SAVE_GET_WEBANALYSIS_LIST_RESPONSE,
    data
  }),
  cleargetWebAnalysislist: () => ({
    type: getWebAnalysisTypes.CLEAR_GET_WEBANALYSIS_LIST
  })
}

export const DeleteWebAnalysisTypes = {
  DELETE_WEBANALYSIS_DETAILS: 'DELETE_WEBANALYSIS_DETAILS',
  WEBANALYSIS_DETAILS_RESPONSE: 'WEBANALYSIS_DETAILS_RESPONSE',
  CLEAR_WEBANALYSIS_DETAILS: 'CLEAR_WEBANALYSIS_DETAILS'
}

export const DeleteWebAnalysisActions = {
  deleteWebAnalysisDetails: (id) => ({
    type: DeleteWebAnalysisTypes.DELETE_WEBANALYSIS_DETAILS,
    id
  }),
  saveWebAnalysiDetailsResponse: data => ({
    type: DeleteWebAnalysisTypes.WEBANALYSIS_DETAILS_RESPONSE,
    data
  }),
  clearWebAnalysisDetails: () => ({
    type: DeleteWebAnalysisTypes.WEBANALYSIS_DETAILS_RESPONSE
  })
}

export const EditWebAnalysisTypes = {
  EDIT_WEBANALYSIS_DETAILS: 'EDIT_WEBANALYSIS_DETAILS',
  EDIT_WEBANALYSIS_DETAILS_RESPONSE: 'EDIT_WEBANALYSIS_DETAILS_RESPONSE',
  EDIT_CLEAR_WEBANALYSIS_DETAILS: 'EDIT_CLEAR_WEBANALYSIS_DETAILS'
}

export const EditWebAnalysisActions = {
  EditWebAnalysisDetails: (id) => ({
    type: EditWebAnalysisTypes.EDIT_WEBANALYSIS_DETAILS,
    id
  }),
  EditsaveWebAnalysiDetailsResponse: data => ({
    type: EditWebAnalysisTypes.EDIT_WEBANALYSIS_DETAILS_RESPONSE,
    data
  }),
  EditclearWebAnalysisDetails: () => ({
    type: EditWebAnalysisTypes.EDIT_CLEAR_WEBANALYSIS_DETAILS
  })
}

export const updateWebAnalysisTypes = {
  UPADTE_WEBANALYSIS_DETAILS: 'UPADTE_WEBANALYSIS_DETAILS',
  UPDATE_WEBANALYSIS_DETAILS_RESPONSE: 'UPDATE_WEBANALYSIS_DETAILS_RESPONSE',
  UPDATE_CLEAR_WEBANALYSIS_DETAILS: 'UPDATE_CLEAR_WEBANALYSIS_DETAILS'
}

export const updateWebAnalysisActions = {
  updateWebAnalysisDetails: (id,params) => ({
    type: updateWebAnalysisTypes.UPADTE_WEBANALYSIS_DETAILS,
    payload:{id, params}
  }),
  updatesaveWebAnalysiDetailsResponse: data => ({
    type: updateWebAnalysisTypes.UPDATE_WEBANALYSIS_DETAILS_RESPONSE,
    data
  }),
  updateclearWebAnalysisDetails: () => ({
    type: updateWebAnalysisTypes.UPDATE_CLEAR_WEBANALYSIS_DETAILS
  })
}