import axios from 'axios'
import { getLocalStorage } from '../utils/helper'
import { HEADER, SESSION } from '../utils/constants'

const instanceApi = axios.create({
  headers: {
     [`${"api-key"}`]: getLocalStorage('SDKAPI'),
    'Content-Type': HEADER.CONTENT_TYPE
  },
  timeout: HEADER.TIMEOUT
})

// Add a request interceptor
instanceApi.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
instanceApi.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    const { data, status } = response
    return { data, status }
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error)
  }
)

export default instanceApi
