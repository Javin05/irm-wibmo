import axios from 'axios'
import { getLocalStorage, setLocalStorage } from '../utils/helper'
import { HEADER, SESSION } from '../utils/constants'
import { useLocation } from 'react-router-dom'

let url = window.location
let pathName = url && url.pathname
const slugs = [
  "/add-kyc"
]

const instance = axios.create({
  headers: {
    [`${slugs.includes(pathName) ? "api-key" : HEADER.TOKEN}`]: ` ${slugs.includes(pathName) ? "ESWfwbTHiHgjiGPBL9B0KcFTM"
    : `${HEADER.BEARER} ${getLocalStorage(SESSION.TOKEN)}`}`,
    'Content-Type': HEADER.CONTENT_TYPE
  },
  timeout: HEADER.TIMEOUT,
})


// Add a request interceptor
instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
instance.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    const { data, status } = response
    return { data, status }
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error)
  }
)

export default instance
