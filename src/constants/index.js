export const DROPZONE_IMAGE_NAME_TYPES = {
    IMAGE: 'IMAGE',
    VIDEO: 'VIDEO',
    AUDIO: 'AUDIO',
    APPUSER: 'appuser',
    DOCUMENTS: 'FILES',
    USER_PROFILE:'userprofile',
    EXCUTIVE_IMAGE: 'executive',
    ADVERTISER_IMAGE: 'advertiser',
    COMPANYDETAIL_IMAGE: 'company',
    FRANCHISE_IMAGE: 'franchise',
    PLAYER_IMAGE: 'player',
    ADVERTISEMENT_IMAGE: 'advertisement',
    ADVERTISEMENT_VIDEO: 'advertisement',
    PROFILE: 'PROFILE',
    FEED_IMAGE: 'FEEDS_IMAGE',
    USER_ROLE: 'USER_ROLE',
    MANAGER_IMAGE: 'image'
  
  }
  
  export const FILE_ORGANIZATION = {
    Partnership : 'Partnership Deed/Certificate',
    Proprietorship : 'Business Registration',
    "Public Limited" : 'Certificate of Incorporation',
    LLP: 'Certificate of Incorporation or LLPIN Certificate',
    Trust: 'Trust Deed or Registration Certificate',
    Society: 'Society Registration Certificate',
    NGO: 'NGO Registration Certificate',
    HUF: 'HUF Deed',
    "Private Limited": 'Certificate of InCorporation',
    SoleProp: 'sole Propreitor RegistrationProof',
  }

  export const COMMON_PROOF_TYPE = {
    Partnership : 'partnershipDeedCertificate',
    Proprietorship : 'registrationProof',
    "Public Limited" : 'businessCinImage',
    LLP: 'llpinCertificate',
    Trust: 'trust_RegistrationProof',
    Society: 'society_RegistrationProof',
    NGO: 'ngo_RegistrationProof',
    HUF: 'huf_RegistrationProof',
    "Private Limited": 'businessCinImage',
    SoleProp: 'registrationProof'
  }
  
  export const RESTRICTED_FILE_FORMAT_TYPE = [
    'image/jpeg',
    'image/png',
    'image/jpg',
    'image/svg+xml',
    'image/JPEG',
    'image/PNG',
    'image/JPG'
  ]

  export const IMAGEA_AND_PDF = [
    'image/jpeg',
    'image/png',
    'image/jpg',
    'image/JPEG',
    'image/PNG',
    'image/JPG',
    'application/pdf',
  ]

  export const PDF = [
    'application/pdf',
  ]
  
  export const FILE_FORMAT_TYPE = [
    'image/jpeg',
    'image/png',
    'image/jpg',
    'image/svg+xml',
    'image/JPEG',
    'image/PNG',
    'image/JPG',
    'video/MP4',
    'video/mp4',
    'video/MOV',
    'video/mov',
    'video/WMV',
    'video/wmv',
    'video/AVI',
    'video/avi',
    'video/AVCHD',
    'video/avchd',
    'video/FLV',
    'video/flv',
    'video/F4V',
    'video/f4v',
    'video/SWF',
    'video/swf',
    'video/WEBM',
    'video/webm',
    'video/HTML5',
    'video/html5',
    'video/MPEG-2',
    'video/mpeg-2',
  ]

  export const FILE_FORMAT_TYPE_DOCUMENT = [
    'application/msword',
    'application/vnd.ms-excel',
    'text/plain',
    'image/gif',
    'image/vnd.adobe.photoshop',
    'application/pdf',
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.google-apps.site",
    "application/vnd.google-apps.spreadsheet",
    "application/vnd.google-apps.unknown",
    "application/vnd.google-apps.video"
  ]

  export const FILE_FORMAT_TYPE_DOCUMEN_IMAGE =[
    'image/jpeg',
    'image/png',
    'image/jpg',
    'image/svg+xml',
    'image/JPEG',
    'image/PNG',
    'image/JPG',
    'image/gif',
    'image/vnd.adobe.photoshop',
    'application/pdf',
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.google-apps.site",
    "application/vnd.google-apps.spreadsheet",
    "application/vnd.google-apps.unknown",
    "application/vnd.google-apps.video"
  ]