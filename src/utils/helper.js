import { SESSION, SET_STORAGE, API, HEADER } from "../utils/constants"
import _ from "lodash"

export const setLocalStorage = (key, varToSet) =>
  localStorage.setItem(key, btoa(varToSet))

export const getLocalStorage = (key) => {
  const getStorage = localStorage.getItem(key)
  try {
    return getStorage ? atob(getStorage) : false
  } catch (e) {
    return false
  }
}
export const unsetLocalStorage = () => localStorage.clear()
export const removeLocalStorage = (key) => localStorage.removeItem(key)

export const getLocalStorageMenu = (key) => {
  const getStorage = localStorage.getItem(key)
  try {
    return getStorage ? atob(getStorage) : ""
  } catch (e) {
    return ""
  }
}

export const addEllipsis = (string, sliceUpTo) => {
  if (string && string.length >= 30) {
    return <span>{string.substring(0, sliceUpTo) + "..."}</span>
  } else {
    return <span>{string}</span>
  }
}

export const setDynamicPasswordRegex = (min, max) => {
  const variable = `(?=^.{${min || 7},${max || 14}}$)`
  const exp = `${variable}(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$`
  const regex = new RegExp(exp)
  return regex
}

export const handleAuthFail = (error) => {
  if (error === SESSION.AUTH_FAILED) {
    window.location.href = "/authorization-failed"
  }
}

export const getUserPermissions = (slug) => {
  const getUserMenuDetails = getLocalStorageMenu(SET_STORAGE.USER_MENU_DETAILS)
  if (getUserMenuDetails && getUserMenuDetails) {
    const getMenuList = JSON.parse(getUserMenuDetails)
    if (getMenuList && getMenuList.length > 0) {
      let val = []
      _.forEach(getMenuList, (parent) => {
        const currentSlug = `/${parent.slug}`
        if (parent && parent.submenu && parent.submenu.length > 0) {
          _.forEach(parent.submenu, (child) => {
            const currentSlug = `/${child.slug}`
            if (currentSlug === slug) {
              const permissions =
                child.permissions && child.permissions.length > 0
                  ? child.permissions
                  : []
              val = permissions
            } else if (child && child.submenu && child.submenu.length > 0) {
              _.forEach(child.submenu, (grand) => {
                const currentSlug = `/${grand.slug}`
                if (currentSlug === slug) {
                  const permissions =
                    grand.permissions && grand.permissions.length > 0
                      ? grand.permissions
                      : []
                  val = permissions
                }
              })
            }
          })
        } else if (currentSlug === slug) {
          const permissions =
            parent.permissions && parent.permissions.length > 0
              ? parent.permissions
              : []
          val = permissions
        }
      })
      return val
    }
  } else {
    window.location.href = "/session-expired"
    unsetLocalStorage()
  }
}

const checkIfParentExist = (tempSlug, arr) => {
  switch (tempSlug) {
    case "/webrisk-dashboard":
      arr.push("/webrisk-dashboard")
      return
    case "/main-dashboard":
      arr.push("/main-dashboard")
      return
    case "/home":
      arr.push("/home")
      return
    case "/web-report-settings":
      arr.push("/web-report-settings")
      return
    case "/playstore-report-settings":
      arr.push("/playstore-report-settings")
      return
    case "/accounts-riskmanagement":
      arr.push(
        "/accounts-riskmanagement",
        "/risk-summary/update/:id",
        "/risk-summary/new-update/:id",
        "/static-summary/update/:id",
        "/merchant-login",
        "/risk-management-search",
        "/KYC",
        "/kyc-dashboard/update/:id",
        "/tag-summary",
        "/uae-kyc",
        "/onboarding-search"
      )
      return
    case "/transactions":
      arr.push(
        "/transactions",
        "/transaction-dashboard/update/:id",
        "/transaction-search",
        "/add-transaction"
      )
      return
    case "/tag-summary":
      arr.push(
        "/tag-summary"
      )
      return
    case "/wrm-riskmmanagement":
      arr.push(
        "/wrm-riskmmanagement",
        "/wrm-sdk",
        "/wrm-riskmmanagement-sdk",
        "/sdk-summary/update/:id",
        "/ongoing-monitoring",
        "/onboarding"
      )
    case "/ongoing-monitoring":
      arr.push(
        "/ongoing-monitoring-dashboard/update/:id", "/ongoing-monitoring-search"
      )
      return
    case "/onboarding":
      arr.push(
        "/onboarding/update/:id"
      )
      return
    case "/aml-queue":
      arr.push("/aml-queue", "/aml-dashboard/update/:id", "/aml-queue-search")
      return
    case "/transaction-lundering":
      arr.push("/transaction-lundering")
      return
    case "/create":
      arr.push("/create")
      return
    case "/manage-queues":
      arr.push("/manage-queues", "queues/update/:id", "/add-queues")
      return
    case "/rules":
      arr.push("/rules", "/rules/update/:id", "/rule-form/add")
      return
    case "/blacklist":
      arr.push("/blacklist")
      return
    case "/white-list":
      arr.push("/white-list")
      return
    case "/fraud-patterns":
      arr.push("/fraud-patterns")
      return
    case "/reports":
      arr.push("/reports")
      return
    case "/queue-reports":
      arr.push("/queue-reports")
      return
    case "/filters":
      arr.push("/filters")
      return
    case "/admin":
      arr.push("/admin")
      return
    case "/web-risk":
      arr.push("/web-risk", "/client-onboarding/update/:id")
      return
    case "/prevention-alerts":
      arr.push(
        "/prevention-alerts",
        "/prevention-alerts/update/:id",
        "/prevention-alerts"
      )
      return
    case "/client-management":
      arr.push(
        "/client-management",
        "/client-onboarding",
        "/client-onboarding/update/:id",
        "/client-onboarding/update/:id/add-template",
        "/client-onboarding/update/:id/update-template/:id"
      )
      return
    case "/user-components":
      arr.push(
        "/user-components",
        "/add-user-components",
        "/update-user-components/:id"
      )
      return
    case "/email-templates":
      arr.push(
        "/email-templates",
        "/email-templates/add-template",
        "/email-templates/update/:id"
      );
    case "/user-management":
      arr.push(
        "/user-management",
        "/user-management/update-user/:id",
        "/assign-partner/:id",
        "/assign-client/:id"
      )
    case "/site-configuration":
      arr.push("/site-configuration")
    case "/wrm-management":
      arr.push(
        "/wrm-management",
        "/wrmmanagement/update/:id"
      )
    default:
      arr.push(tempSlug)
      return
  }
}

export const getUserMatrixSlugs = () => {
  const getUserMenuDetails = getLocalStorageMenu(SET_STORAGE.USER_MENU_DETAILS)

  if (getUserMenuDetails && getUserMenuDetails) {
    const getMenuList = JSON.parse(getUserMenuDetails)
    if (getMenuList && getMenuList.length > 0) {
      let slugs = [
        "/risk-management', '/user-profile', '/user-change-password", "/accounts-queues"
      ]
      _.forEach(getMenuList, (parent) => {
        if (parent.submenu && parent.submenu.length > 0) {
          _.forEach(parent.submenu, (child) => {
            if (child.submenu && child.submenu.length > 0) {
              _.forEach(child.submenu, (grand) => {
                if (grand.slug) {
                  const prepSlug = grand.slug
                    .replace(/\s+/g, "-")
                    .toLowerCase()
                  checkIfParentExist(`/${prepSlug}`, slugs)
                }
              })
            } else {
              const prepSlug = child.slug.replace(/\s+/g, "-")
              checkIfParentExist(`/${prepSlug}`, slugs)
            }
          })
        } else {
          const prepSlug = parent.slug.replace(/\s+/g, "-").toLowerCase()
          checkIfParentExist(`/${prepSlug}`, slugs)
        }
      })
      return slugs
    }
  }
}

const events = ["click", "load", "keydown"]

export const addEvents = (eventHandler) => {
  events.forEach((eventName) => {
    window.addEventListener(eventName, eventHandler)
  })
}

export const removeEvents = (eventHandler) => {
  events.forEach((eventName) => {
    window.removeEventListener(eventName, eventHandler)
  })
}

export const setLastInteractionTime = (key, valueToSet) => {
  localStorage.setItem(key, valueToSet)
}

export const getLastInteractionTime = (key) => {
  const getStorage = localStorage.getItem(key)
  try {
    return getStorage || ""
  } catch (e) {
    return ""
  }
}

export const headersWithAuth = {
  'X-Custom-Header': 'foobar',
  [`${HEADER.TOKEN}`]: `Bearer ${getLocalStorage(SESSION.TOKEN)}`
}

export const headerWithApiKey = {
  'X-Custom-Header': 'foobar',
  'api-key': `${getLocalStorage(API.API_KEY)}`
}