import { put, takeLatest, all } from 'redux-saga/effects'
import {
  UPLOAD_IMAGE,
  UploadActions,
  UploadDocumentActions,
  UPLOAD_DOCUMENT
} from '../../../store/actions/index'
import { SESSION } from '../../constants'
import { getLocalStorage } from '../../../utils/helper'

import serviceList from '../../../services/serviceList'
import axiosInstance from '../../../services'

const headers = {
  'x-auth-token': getLocalStorage(SESSION.TOKEN),
  'Content-Type': 'multipart/form-data,boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL'
}

function* fetchUpload(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.upload, payload)
    if (data && data.data) {
      yield put(UploadActions.uploadImageSuccess(data.data))
    }
  } catch (error) {
    yield put(UploadActions.uploadImageError(error))
  }
}

function* fetchUploadDocument(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.upload, payload)
    if (data && data.data) {
      yield put(UploadDocumentActions.uploadDocumentSuccess(data.data))
    }
  } catch (error) {
    yield put(UploadDocumentActions.uploadDocumentSuccess(error))
  }
}

export function* fetchUploadWatcher() {
  yield all([
    yield takeLatest(UPLOAD_IMAGE.GET_LIST, fetchUpload),
    yield takeLatest(UPLOAD_DOCUMENT.GET_LIST, fetchUploadDocument)
  ])
}
