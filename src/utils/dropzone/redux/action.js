import { ERROR } from '../../constants'

export const UPLOAD_IMAGE = {
  GET_LIST: 'GET_REQUEST_UPLOAD',
  REQUEST: 'REQUEST_UPLOAD_IMAGE',
  SUCCESS: 'SUCCESS_UPLOAD_IMAGE',
  ERROR: 'ERROR_UPLOAD_IMAGE',
  CLEAR: 'CLEAR_UPLOAD_IMAGE'
}

export const UploadActions = {
  uploadImage: (data, name) => (
    {
    type: UPLOAD_IMAGE.GET_LIST,
    payload: data,
    name: name
  }),
  uploadImageSuccess: (data) => ({
    type: UPLOAD_IMAGE.SUCCESS,
    uploadImage: data,
  }),
  uploadImageError: () => ({
    type: UPLOAD_IMAGE.ERROR
  })
}

export const UPLOAD_DOCUMENT = {
  GET_LIST: 'GET_REQUEST_UPLOAD_DOCUMENT',
  REQUEST: 'REQUEST_UPLOAD_DOCUMENT',
  SUCCESS: 'SUCCESS_UPLOAD_DOCUMENT',
  ERROR: 'ERROR_UPLOAD_DOCUMENT',
  CLEAR: 'CLEAR_UPLOAD_DOCUMENT'
}

export const UploadDocumentActions = {
  uploadDocument: (data, name) => (
    {
    type: UPLOAD_DOCUMENT.GET_LIST,
    payload: data,
    name: name
  }),
  uploadDocumentSuccess: (data) => ({
    type: UPLOAD_DOCUMENT.SUCCESS,
    uploadDocument: data,
  }),
  uploadDocumentError: () => ({
    type: UPLOAD_DOCUMENT.ERROR
  }),
  ClearDocument: () => ({
    type: UPLOAD_DOCUMENT.CLEAR
  })
}
