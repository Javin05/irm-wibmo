import React, { useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useDropzone } from "react-dropzone";
import _ from "lodash";
import {
  DROPZONE_IMAGE_NAME_TYPES,
  DELETE_THUMBNAIL_IMAGE,
  EMPTY_DROPZONE,
  DROPZONE_MESSAGES,
  ENV,
} from "../constants";
import { KTSVG } from '../../theme/helpers'

// import { uploadImage, uploadImageWithThumbnail } from "./redux/action";
// import userPlaceholder from '../../assets/images/userPlaceholder.png'
import { UploadActions } from "../../store/actions/index";

const Dropzone = (props) => {
  const {
    defaultImages,
    multiple,
    thumbnail,
    width,
    height,
    type,
    name,
    flag,
    maxFileSize,
    formatType,
    showRemove,
    dropzoneError,
    dropzoneImage,
    postUploadDispatch
  } = props;
  const imagesArray =
    defaultImages && defaultImages.length > 0
      ? _.filter(defaultImages, (i) => {
        return i.key;
      })
      : [];
  const [files, setFiles] = useState(imagesArray);
  const [uploadingSize, setUploadingSize] = useState(0);
  const [totalSize, setTotalSize] = useState(0);
  const [status, setStatus] = useState(
    "Drag 'n' drop some files here, or click to select files"
  );
  const [error, setError] = useState(false);
  const isJsonFormat = _.includes(formatType, "application/json");

  const setFormatTypeError = (type, setStatus) => {
    switch (type) {
      case DROPZONE_IMAGE_NAME_TYPES.IMAGE:
        return setStatus(DROPZONE_MESSAGES.IMAGE_INVALID);
      case DROPZONE_IMAGE_NAME_TYPES.VIDEO:
        return setStatus(DROPZONE_MESSAGES.VIDEO_INVALID);
      case DROPZONE_IMAGE_NAME_TYPES.AUDIO:
        return setStatus(DROPZONE_MESSAGES.AUDIO_INVALID);
      case DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS:
        return setStatus(DROPZONE_MESSAGES.DOCUMENT_INVALID);
    }
  };

  const onDrop = useCallback((acceptedFiles) => {
    setError(false);
    setStatus("Uploading...");
    let isValidFileFormat = false;
    const formData = new FormData();
    formData.append("type", type);
    if (thumbnail) {
      formData.append("width", width);
      formData.append("height", height);
    }
    acceptedFiles.forEach((item) => {
      const fileType = item && item.type;
      const uploadedFileSize = item && item.size;
      isValidFileFormat = _.includes(formatType, fileType);
      const fileSize = Number(maxFileSize) * 1024 * 1024;
      setTotalSize(uploadedFileSize);
      if (isValidFileFormat) {
        if (uploadedFileSize < fileSize) {
          formData.append("file_to_upload", item);
        } else {
          setError(true);
          setStatus(
            `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`
          );
        }
      } else {
        setError(true);
        setFormatTypeError(type, setStatus);
      }
    });

    const config = {
      onUploadProgress: (progressEvent) => {
        setUploadingSize(progressEvent.loaded);
      },
    };

    if (isValidFileFormat) {

      if (thumbnail) {
        // dispatch(uploadImageWithThumbnail(formData, name, flag));
      } else {
        postUploadDispatch(formData, name, flag)
        // dispatch(uploadImage(formData, name, flag, config));
      }
    }
  }, []);

  const getProgressPercentage = (currentSize, ttlSize) => {
    return (currentSize / ttlSize) * 100;
  };

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  const removeDropzoneImage = (key, flag) => {
    const array = _.filter(files, (x) => {
      if (key !== x.key) {
        return x;
      }
    });
    setFiles(array);
    setStatus(EMPTY_DROPZONE.MSG);
    // dispatch({
    //   type: DELETE_THUMBNAIL_IMAGE.RESPONSE_SUCCESS,
    //   flag,
    //   imageName: array,
    // });
  };

  const previewFiles = files.map((file, i) => (
    <div
      className={
        multiple
          ? `col-3 col-sm-3 col-md-3 p-2`
          : ` col-12 col-sm-12 col-md-12 p-2 mx-auto`
      }
      key={i}
    >
      <i
        className="zmdi zmdi-close-circle zmdi-hc-fw cursor-pointer position-top-rigth text-danger icon-2"
        onClick={() => {
          removeDropzoneImage(file.key, flag);
        }}
      />
      {type === DROPZONE_IMAGE_NAME_TYPES.IMAGE ? (
        <img
          className="preview-image img-fit"
          src={`${ENV.IMAGE_PREVIEW_URL}/${file.key}`}
          alt={file.originalname || ""}
          // onError={(e) => {
          //   e.target.src = userPlaceholder;
          // }}
          key={file.location}
          width="100%"
          height="100%"
        />
      ) : (
        <div className="bg-dark d-flex justify-content-center">
          {type === DROPZONE_IMAGE_NAME_TYPES.AUDIO ? (
            <audio
              className=""
              style={{ backgroundColor: "transparent" }}
              controls
              id=""
              key={file.location}
              width="100%"
              height="100%"
              autoPlay={false}
              src={file.location}
            />
          ) : (
            <>
              {type === DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS ? (
                <>
                  <div className="docs-icon-card d-flex justify-content-center align-items-center">
                    <div className="text-center">
                      <i
                        data-toggle="dropdown"
                        className="zmdi zmdi-file-text text-danger icon-5"
                      />
                      <p
                        className="text-muted mb-0"
                        controls
                        id=""
                        key={file.location}
                        width="100%"
                        height="100%"
                        src={file.location}
                      >
                      </p>
                    </div>
                  </div>
                </>
              ) : (
                <video
                  className=""
                  controls
                  id=""
                  key={file.location}
                  width="100%"
                  height="100%"
                  autoPlay={false}
                  src={file.location}
                />
              )}
            </>
          )}
        </div>
      )}
    </div>
  ));

  useEffect(() => {
    const imagesArray =
      defaultImages && defaultImages.length > 0
        ? _.filter(defaultImages, (i) => {
          return i.key;
        })
        : [];
    if (_.isEmpty(imagesArray)) {
      setStatus(
        `${isJsonFormat ? EMPTY_DROPZONE.FILE_MSG : EMPTY_DROPZONE.MSG}`
      );
    }
    setFiles([...imagesArray]);
  }, [defaultImages]);

  useEffect(() => {
    if (files && files.length > 0) {
      setStatus(
        `${isJsonFormat ? EMPTY_DROPZONE.FILE_MSG : EMPTY_DROPZONE.MSG}`
      );
    }
  }, [files]);

  useEffect(() => {
    if (dropzoneError) {
      setStatus(dropzoneError);
    }
  }, [dropzoneError]);

  // useEffect(() => {
  //   return () => {
  //     dispatch({ type: DELETE_THUMBNAIL_IMAGE.RESPONSE_SUCCESS, flag });
  //     setFiles([]);
  //   };
  // }, []);

  const percent = getProgressPercentage(uploadingSize, totalSize);
  const showProgress = () => {
    const sp = Math.floor(percent - 100);
    return Math.abs(sp);
  };

  return (
    <div className="btn btn-outline btn-outline-dashed btn-outline-primary btn-active-light-primary">
      <div className="row">{previewFiles}</div>

      {!multiple && files && files.length === 0 ? (
        <div
          {...getRootProps({
            className: `dropzone ${error && "dropzone-error"}`,
          })}
        >
          <input {...getInputProps({ multiple })} />
          <p className="m-0 text-center mx-3">
            <i
              className="bi bi-file-earmark-arrow-down-fill text-primary"
              style={{ fontSize: "1.4rem" }}
            />
            {" "}
            {status}
          </p>
        </div>
      ) : null}
      {multiple && (
        <div
          {...getRootProps({
            className: `dropzone ${error && "dropzone-error"}`,
          })}
        >
          <input {...getInputProps({ multiple })} />
          <p className="m-0 text-center mx-3">
          <i
              className="bi bi-file-earmark-arrow-down-fill text-primary"
              style={{ fontSize: "1.4rem" }}
            />{" "}
            {status}
          </p>
        </div>
      )}
      {/* {
      showProgress() ? (
        <>
          <div className="row ">
            <div className="col-12 col-sm-12 col-md-12">
              <div className="progress mt-3">
                <div
                  className="progress-bar progress-bar-striped bg-success"
                  role="progressbar"
                  label={`${Math.floor(percent)}%`}
                  style={{ width: `${Math.floor(percent)}%` }}
                  aria-valuenow="25"
                  aria-valuemin="0"
                  aria-valuemax="100"
                />
              </div>
              <div className="d-flex justify-content-center">
              <span className="percentage">{`${Math.floor(percent) < 100 ? Math.floor(percent) : 100}%`}</span>
            </div>
            </div>
          </div>
        </>
      ) : null} */}
    </div>
  );
};


const mapStateToProps = (state) => {
  const { dropzoneStore } = state;
  return {
    dropzoneImage:
      dropzoneStore && dropzoneStore.image ? dropzoneStore.image : [],
    dropzoneImageThumbNail:
      dropzoneStore && dropzoneStore.imageThumbNail
        ? dropzoneStore.imageThumbNail
        : [],
    dropzoneStatus:
      dropzoneStore && dropzoneStore.status ? dropzoneStore.status : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  postUploadDispatch: (formData, name, flag) => dispatch(UploadActions.uploadImage(formData, name, flag)),
})

Dropzone.propTypes = {
  dropzoneImage: PropTypes.array,
  dropzoneImageThumbNail: PropTypes.array,
  dropzoneStatus: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dropzone);
