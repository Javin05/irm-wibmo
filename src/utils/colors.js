const color = {
  tangerine: '#00a2ff',
  peach: '#00a2ff',
  redSand: '#00a2ff',
  black: '#000000',
  gray: 'gray',
  white: '#ffffff'
}

export default color
