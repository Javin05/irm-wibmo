import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { riskManagementActions, queuesActions, clientCredFilterActions } from '../../store/actions'
import { connect } from 'react-redux'
import SearchManageQueues from './SearchManageQueues'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage, getLocalStorage } from '../../utils/helper';
import { useLocation, Link, useParams } from 'react-router-dom'
import { QUEUE_ROUTING, SET_FILTER } from '../../utils/constants'

function ManageQueues(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading,
    merchantIddetails,
    getQueueslistDispatch,
    queuesLists,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction
  } = props
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false,
    clientId: false
  })

  const didMount = React.useRef(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('queues/')
  const currentId = url && url[1]

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params);
    getQueueslistDispatch(pickByParams)
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1);
      const params = {
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getQueueslistDispatch(pickByParams)
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: 1,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    getQueueslistDispatch(params)
    setActivePageNumber(1);
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    setActivePageNumber(pageNumber)
    getRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = '/merchant-login';
  };

  const merchantIdDetails = merchantIddetails && merchantIddetails.data ? merchantIddetails.data : '--'

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  return (
    <>
      <div className={`card ${className}`}>
        <SearchManageQueues currentId={currentId} />
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {riskmgmtlists && riskmgmtlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto'>
              <div className='my-auto'>
                {/* <SearchList /> */}
              </div>
              <div className='my-auto me-3'>
                {/* <a
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                  onClick={logout}
                >
                  <KTSVG path="/media/icons/duotune/files/fil003.svg" 
                  />
                  Add Case
                </a> */}
                {/* <Link
                  to='/merchant-login'
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                  <KTSVG path="/media/icons/duotune/files/fil003.svg"
                  />
                  Add Case
                </Link> */}
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className="min-w-70px">Action</th>
                  <th>
                    <div className="d-flex">
                      <span>Queue</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.queue
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Client</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("clientId")}
                        >
                          <i
                            className={`bi ${sorting.queue
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Sla</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.sla
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                  !loading
                    ? (
                      queuesLists &&
                        queuesLists.data
                        ? (
                          queuesLists.data.map((item, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="min-width-150px text-start">
                                  <div>
                                    <Link
                                      to={`/queues/update/${item._id}`}
                                      className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4" href="/client-onboarding/update/621c7764174b5b0004719991">
                                      <span className="svg-icon svg-icon-3">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px">
                                          <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black">
                                          </path>
                                          <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black">
                                          </path>
                                        </svg>
                                      </span>
                                    </Link>
                                  </div>
                                </td>
                                <td className="ellipsis">
                                  <Link
                                    to={`${QUEUE_ROUTING[item.queueName && item.queueName]}`}
                                  >
                                    {item.queueName ? item.queueName : "--"}
                                  </Link>
                                </td>
                                <td className="ellipsis">
                                  {item?.clientId?.company
                                    ? item.clientId?.company
                                    : "--"}
                                </td>
                                <td className="ellipsis">
                                  {item.sla ? item.sla : "--"}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore, editMerchantStore, queueslistStore } = state;
  return {
    riskmgmtlists:
      state &&
      state.riskManagementlistStore &&
      state.riskManagementlistStore.riskmgmtlists,
    loading: queueslistStore && queueslistStore.loading ? queueslistStore.loading : false,
    merchantIddetails: editMerchantStore && editMerchantStore.merchantIddetail ? editMerchantStore.merchantIddetail : {},
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params)),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageQueues);
