import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { riskManagementActions } from '../../store/actions'
import { useLocation, Link } from 'react-router-dom'

function SearchManageQueues(props) {
  const {
    getRiskMgmtColumns,
    currentId
  } = props
  const [, setShow] = useState(false)
  const [formData, setFormData] = useState({
    deviceID: '',
    phone: '',
    email: '',
    ipAddress: '',
    address: '',
    status: ''
  })

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSearch = () => {
    setShow(false)
    const params = {}
    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key] !== '') {
        params[key] = formData[key]
      }
    }
    getRiskMgmtColumns(params)
  }

  const handleReset = () => {
    setFormData({
      deviceID: '',
      phone: '',
      email: '',
      ipAddress: '',
      address: '',
      status: ''
    })
    const params = {
      limit: 25,
      page: 1
    }
    getRiskMgmtColumns(params)
  }

  return (
    <>
      <div className='row mt-10'>
        <div className='col-lg-8 '>
        </div>
        <div className='col-lg-4 '>
          <div className='d-flex justify-content-end my-auto'>
            <div className='my-auto'>
              {/* <SearchList /> */}
            </div>
            <div className='my-auto me-3 d-flex ' >
              <Link
                to='/add-queues'
                className='btn btn-sm btn-light-primary btn-responsive me-4'
              >
                <span className="svg-icon svg-icon-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                  </svg>
                </span>
                Add Queue
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => ({
  getRiskManagementlist: state && state.riskManagementlistStore && state.riskManagementlistStore.getRiskManagementlist,
  loading: state && state.riskManagementlistStore && state.riskManagementlistStore.loading,
})

const mapDispatchToProps = dispatch => ({
  getRiskMgmtColumns: (data) =>
    dispatch(riskManagementActions.getRiskManagementlist(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchManageQueues)