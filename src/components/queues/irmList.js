import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { riskManagementActions } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'

function IRMList(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading,
    value
  } = props
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    // getRiskManagementlistDispatch(params)
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    // getRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      // getRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      // getRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.count) / limit)
      : 1

      const queuesLists = [
        {
          qName: "caseId",
          count: "52",
          status: "Accepted",
          avgWait: "3 Hours",
          maxWait: "4 Hours",
          sla: "5 Hours"
        },
        {
          qName: "caseId",
          count: "56",
          status: "Rejected",
          avgWait: "3 Hours",
          maxWait: "4 Hours",
          sla: "5 Hours"
        },
        {
          qName: "caseId",
          count: "32",
          status: "Onhold",
          avgWait: "3 Hours",
          maxWait: "4 Hours",
          sla: "5 Hours"
        },
      ];

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {riskmgmtlists && riskmgmtlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>QName</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("qName")}
                        >
                          <i
                            className={`bi ${sorting.qName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Count</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("count")}
                        >
                          <i
                            className={`bi ${sorting.count
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Average Wait(hrs)</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("avgWait")}
                        >
                          <i
                            className={`bi ${sorting.avgWait
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Maximum Wait (hrs)</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("maxWait")}
                        >
                          <i
                            className={`bi ${sorting.maxWait
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>SLA (hrs)</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("sla")}
                        >
                          <i
                            className={`bi ${sorting.sla
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-7'>
                {
                  !loading
                    ? (
                      queuesLists &&
                        queuesLists
                        ? (
                          queuesLists.map((queuesList, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className='ellipsis'>
                                  {value}
                                  {/* {queuesList.qName ? queuesList.qName : "--"} */}
                                </td>
                                <td className='ellipsis'>
                                  {queuesList.count ? queuesList.count : "--"}
                                </td>
                                <td className='ellipsis'>
                                  {queuesList.status ? queuesList.status : "--"}
                                </td>
                                <td className='ellipsis'>
                                  {queuesList.avgWait ? queuesList.avgWait : "--"}
                                </td>
                                <td className='ellipsis'>
                                  {queuesList.maxWait ? queuesList.maxWait : "--"}
                                </td>
                                <td className='ellipsis'>
                                  {queuesList.sla ? queuesList.sla : "--"}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='6'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='6' className='text-center'>
                          <div className='spinner-border text-primary m-5' role='status' />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore } = state;
  return {
    riskmgmtlists:
      state &&
      state.riskManagementlistStore &&
      state.riskManagementlistStore.riskmgmtlists,
    loading:
      riskManagementlistStore && riskManagementlistStore.loading ? riskManagementlistStore.loading : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) =>
    dispatch(riskManagementActions.getRiskManagementlist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(IRMList)
