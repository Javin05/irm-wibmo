/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useMemo, useEffect, useState } from "react";
import objectPath from "object-path";
import ApexCharts from "apexcharts";
import { Dropdown } from "react-bootstrap";
import { toAbsoluteUrl } from "../../../theme/helpers";
import { useHtmlClassService } from "../../../theme/theme-settings/_core/MetronicLayout";
import { DropdownMenu2 } from "./DropdownMenu2";
import Chart from "react-apexcharts";

export function PrimeWidget1({ className, filterType, transData }) {

    let seriesData=[]
    let approved=[] 
    let rejected=[] 
    let hold=[] 
    let pending=[] 
    let label=[]

    if( transData && transData.length !== 0){
        Object.keys(transData).forEach(function(key) {
            approved.push(transData[key].totalApprovedCount);
            rejected.push(transData[key].totalRejectCount);
            hold.push(transData[key].totalHoldCount);
            pending.push(transData[key].totalPendingCount);

            if(filterType==="today"){
                label.push(transData[key].day);
            }
            else if(filterType==="monthly"){
                label.push(transData[key].monthName);
            }
            else if(filterType==="year"){
                label.push(transData[key].year);
            }
        });
        seriesData.push({
            name: "Approved",
            data: approved
        })        
        seriesData.push({
            name: "Rejected",
            data: rejected
        })        
        seriesData.push({
            name: "Hold",
            data: hold
        })        
        seriesData.push({
            name: "Pending",
            data: pending
        })
    }

    const strokeColor = "#D13647";
    const oneData = {
        series: seriesData,
        options: {
            chart: {
                type: "area",
                height: 260,
                toolbar: {
                    autoSelected: "pan",
                    show: true,
                    tools: {
                        download: true,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset : false,
                        //customIcons: []
                    },
                }
            },
            colors: ['#1BC5BD', '#F64E60', '#FFA800', '#8950FC'],
            dropShadow: {
                enabled: true,
                enabledOnSeries: undefined,
                top: 5,
                left: 0,
                blur: 3,
                color: strokeColor,
                opacity: 0.5
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: "smooth",
                show: true,
                width: 2,
            },
            fill: {
                type: "gradient",
                gradient: {
                    opacityFrom: 0.2,
                    opacityTo: 0.8
                }
            },
            legend: {
                position: "bottom",
                horizontalAlign: "center"
            },
            xaxis: {
                title: { text: "Transaction Status" },
                // type: "datetime",
                // labels: {
                //     format: 'MMM dd'
                // },
                categories : label,
            },
            yaxis: {
                title: { text: "Number of Transactions" }
            }
        }
    };
    
    return (
        <div className={`card card-custom bg-gray-100 ${className}`}>
            <div className="card-header border-0 py-5">
                <h3 className="card-title font-weight-bolder capitalize">{ filterType==="year"? "Yearly" : filterType} Transaction Status</h3>
                <p className="text-muted">...</p>
            </div>
            <div className="card-body p-0 position-relative overflow-hidden">
                { transData && <Chart
                    series={oneData.series}
                    options={oneData.options}
                    type="area"
                    height={340}
                  />}
            </div>
        </div>
    );
}

const getColorArray = (length) => {
    let array = [];
    for (let n = 0; n < length; n++) {
        array.push(
            "#" + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
        );
    }

    return array;
}
const generateDayWiseTimeSeries = function (baseval, count, yrange) {
    let i = 0;
    let series = [];
    while (i < count) {
        let x = baseval;
        let y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push([x, y]);
        baseval += 86400000;
        i++;
    }

    return series;
}
const generateTimeWiseTimeSeries = function (baseval, count, yrange) {
    let i = 0;
    let series = [];
    while (i < count) {
        let x = Math.floor(Math.random() * (750 - 1 + 1)) + 1;
        let y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
        let z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;

        series.push([x, y, z]);
        baseval += 86400000;
        i++;
    }

    return series;
}
const generateMonthWiseTimeSeries = function (count, yrange) {
    let i = 0;
    let series = [];
    while (i < count) {
        //let x = baseval;
        let y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push(y);
        //baseval += 86400000;
        i++;
    }
    return series;
}
