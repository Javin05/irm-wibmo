import React from 'react';
import {WidgetOne} from './widgets/WidgetOne';
import {WidgetTwo} from './widgets/WidgetTwo';
import {WidgetThree} from './widgets/WidgetThree';
import {WidgetFour} from './widgets/WidgetFour';
import {PrimeWidget1} from './widgets/PrimeWidget1';
import './index.css';

const MainDashboard = (props) => {
    const { loading, chartdata, transdata, filterType } = props
    let approved = chartdata && chartdata.approved !== null? chartdata.approved : 0
    let rejected = chartdata && chartdata.rejected !== null? chartdata.rejected : 0
    let pending = chartdata && chartdata.pending !== null? chartdata.pending : 0
    let hold = chartdata && chartdata.hold !== null? chartdata.hold : 0

    return (
        <>            
            <div className="row mb-4">
                <div className="col-md-3">
                    <WidgetThree 
                        className="card-stretch card-stretch-half gutter-b"
                        symbolShape="circle"
                        baseColor="success"
                        loading={loading}
                        count={approved}/>                   
                </div>
                <div className="col-md-3">
                    <WidgetTwo 
                        className="card-stretch card-stretch-half gutter-b"
                        symbolShape="circle"
                        baseColor="danger"
                        loading={loading}
                        count={rejected}/>
                </div>
                <div className="col-md-3">
                    <WidgetOne 
                        className="card-stretch card-stretch-half gutter-b"
                        symbolShape="circle"
                        baseColor="warning"
                        loading={loading}
                        count={hold}/> 
                </div>
                <div className="col-md-3">
                    <WidgetFour 
                        className="card-stretch card-stretch-half gutter-b"
                        symbolShape="circle"
                        baseColor="info"
                        loading={loading}
                        count={pending}/> 
                </div>
            </div>
            <div className="row mb-4">
                <div className="col-md-12">
                    <PrimeWidget1 
                        className="card-stretch gutter-b" 
                        filterType={filterType}
                        transData={transdata}
                        />
                </div>
            </div>
        </>
    );
}

export default MainDashboard;