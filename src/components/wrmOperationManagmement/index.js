import React, { useEffect, useState, useRef } from 'react'
import _ from 'lodash'
import {
  WrmOperationManagementActions,
  GetClientsActions,
  clientIdLIstActions,
  WRMOperatorsListActions
} from '../../store/actions'
import SearchList from "./searchList"
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage, removeLocalStorage, setLocalStorage } from '../../utils/helper'
import { SET_FILTER, STATUS_RESPONSE, } from '../../utils/constants'
import moment from 'moment'
import { useHistory,Link } from 'react-router-dom';
import { RISKSTATUS } from '../../utils/constants'


function WrmOperationManagementList(props) {
  const {
    getWrmOperationManagementlistDispatch,
    className,
    WrmOperationManagement,
    loading,
    WrmOperationActionStatus,
    clientIdDispatch,
    clinetIdLists,
    getWRMOperatorsDispatch,
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(100)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [Value, setValue] = useState(false)
  const paginationSearch = JSON.parse(getLocalStorage('WRM_OPS_SEARCH'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [formData, setFormData] = useState([])
  const [selectAllChecked, setSelectAllChecked] = useState(false);
  const [sorting, setSorting] = useState({
    clientId: false,
    taskId: false,
    caseId: false,
    workStatus: false,
    operationStatus: false,
    assignedFrom: false,
    assignedTo: false,
    tag: false,
    createdTime: false,
  })

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
      ...paginationSearch
    }
    let clientParams = { limit: 100 }
    clientIdDispatch(clientParams)
    const pickByParams = _.pickBy(params)
    getWrmOperationManagementlistDispatch(pickByParams)
    getWRMOperatorsDispatch()
    removeLocalStorage("OPS_ACTIVE_TAB")
  }, [WrmOperationActionStatus])

  const history = useHistory()
  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      tag: paginationSearch.tag ? paginationSearch.tag : '',
      clientId: paginationSearch.clientId ? paginationSearch.clientId : '',
      assignedTo: paginationSearch.assignedTo ? paginationSearch.assignedTo : '',
      operationStatus: paginationSearch.operationStatus ? paginationSearch.operationStatus : '',
      workStatus: paginationSearch.workStatus ? paginationSearch.workStatus : ''
    }
    setActivePageNumber(pageNumber)
    getWrmOperationManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getWrmOperationManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getWrmOperationManagementlistDispatch(params)
    }
  }

  const totalPages =
    WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.count
      ? Math.ceil(parseInt(WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.count) / limit)
      : 1

  const tagSearch = JSON.parse(getLocalStorage('WRM_OPS_SEARCH'))

  useEffect(() => {
    return (
      setValue(true),
      setTimeout(() => {
        setValue(false)
      }, 1500)
    )
  }, [])


  const hadelRefresh = (() => {
    getWrmOperationManagementlistDispatch(tagSearch)
  })

  const hadelReset = (() => {
    const params = {
      limit: limit,
      page: 1,
      ...searchParams,
    }
    getWrmOperationManagementlistDispatch(params)
    removeLocalStorage("WRM_OPS_SEARCH")
    setValue(true)  
  })

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }
  const handleChangeAll = (e) => {
    e.persist()
    if (e.target.checked === true) {
      let data = WrmOperationManagement.data.result.map(item => item._id)
      setFormData(data)
    } else if (e.target.checked === false) {
      setFormData([])
      setSelectAllChecked(false)
    }
  }

  useEffect(() => {
  const data =  WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.result
  if (!_.isEmpty(formData)){
    if(formData && formData.length === data.length){
      setSelectAllChecked(true)
    }else{
      setSelectAllChecked(false)
    }
  }
  },[formData])

  const handleSearch = (operationStatus,workStatus,caseAlert,taskAlert,caseId) => {
    const params = {
      operationStatus,workStatus,caseAlert,taskAlert,caseId
    }
    setLocalStorage('WRM_OPS_SEARCH', JSON.stringify(
      params
    ))
    getWrmOperationManagementlistDispatch(params)
  }
  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#categoryModal'
        onClick={() => { }}
      >
      </div>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='row'>
            <div className='d-flex justify-content-start col-md-12 col-lg-12'>
              <div className='col-md-6 mt-1 ms-2'>
                {WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='d-flex col-md-12 col-lg-12 justify-content-end my-auto mt-4'>
              <ul className="nav nav-tabs nav-line-tabs fs-6">
                <li className="nav-item">

                  <SearchList Value={Value}
                    clientList={clinetIdLists && clinetIdLists.data && clinetIdLists.data.result ? clinetIdLists.data.result : []}
                  />

                </li>
              </ul>
              <div className='my-auto'>
                <button
                  onClick={() => hadelReset()}
                  type='button'
                  className='btn btn-lg btn-primary btn-responsive font-6vw me-3 pull-right'
                >
                  Reset
                </button>

              </div>
              <div className='my-auto'>
                <button
                  onClick={() => hadelRefresh()}
                  type='button'
                    className='btn btn-lg btn-primary btn-responsive font-6vw me-3 pull-right'
                >
                  Refresh
                </button>
              </div>
            </div>
  
            <div className='d-flex col-md-12 justify-content col-lg-12 my-auto mt-4 pt-8'>
              <div className='col-md-4 mt-1 ms-2'>
                <Link onClick={()=>handleSearch("BACKEND PROCESSING","PENDING")}>
                <span className='text-muted'>
                    <span className="badge bg-light text-dark w-100 h-100  "><h6>{`Backend process pending : ${WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.countBackendProcessingPending ? WrmOperationManagement.data.countBackendProcessingPending : 0}`}</h6></span>   
                </span>
                </Link>
              </div>
              <div className='col-md-4 mt-1 ms-2'>
              <Link onClick={()=>handleSearch("ANALYST REVIEW","PENDING")}>
                <span className='text-muted'>
                      <span className="badge bg-light text-dark w-100 h-100 "><h6>{`Analyst review pending : ${WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.countAnalystPending ? WrmOperationManagement.data.countAnalystPending : 0}`}</h6></span>
                </span>
                </Link>
              </div>
               <div className='col-md-4 mt-1 ms-2'>
                <Link onClick={()=>handleSearch("QA REVIEW","PENDING")}>
                <span className='text-muted'>
                      <span className="badge bg-light text-dark w-100 h-100 "><h6>{`QA review Pending : ${WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.countQAnalystPending ? WrmOperationManagement.data.countQAnalystPending : 0}`}</h6></span>
                </span>
                </Link>
              </div>
            </div>
            
            <div className='d-flex col-md-12 justify-content col-lg-12 my-auto mt-4 pt-2'>
              <div className='col-md-4 mt-1 ms-2'>
              <Link onClick={()=>handleSearch("","ON HOLD")}>
                <span className='text-muted'>
                      <span className="badge bg-light text-dark w-100 h-100 "><h6>{`On hold : ${WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.countHold ? WrmOperationManagement.data.countHold : 0}`}</h6></span>
                </span>
              </Link>
              </div>
              <div className='col-md-4 mt-1 ms-2'>
              <Link onClick={()=>handleSearch("","","","RED")}>
                <span className='text-muted'>
                      <span className="badge bg-light text-dark w-100 h-100 "><h6>{`Task Urgent Attention : ${WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.countTaskRedAlert ? WrmOperationManagement.data.countTaskRedAlert : 0}`}</h6></span>
                </span>
                </Link>
              </div>
               <div className='col-md-4 mt-1 ms-2'>
               <Link onClick={()=>handleSearch("","","RED")}>
                <span className='text-muted'>
                      <span className="badge bg-light text-dark w-100 h-100 "><h6>{`Case Urgent Attention : ${WrmOperationManagement && WrmOperationManagement.data && WrmOperationManagement.data.countRedCaseAlert ? WrmOperationManagement.data.countRedCaseAlert : 0}`}</h6></span>
                </span>
              </Link>
              </div>
            </div>
          </div>

          <br />
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                 {(Role==="Admin"||Role==="Supervisor")&&  
                 <th>
                    <div className="d-flex">
                      <span>Select all</span>
                    </div>
                    <div className='mt-4'>
                      <div className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                        <input
                          className="form-check-input cursor-pointer "
                          type="Checkbox"
                          onChange={(e) => handleChangeAll(e)}
                          name='checkNameSelectAll'
                          checked={selectAllChecked}
                        />
                      </div>
                    </div>
                  </th>}
                  <th>
                    <div className="d-flex">
                      <span>Client name</span>
                    </div>
                  </th>
                   <th>
                    <div className="d-flex">
                      <span>Case status</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Task status</span>
                    </div>
                  </th>
                  <th >
                    <div className="d-flex">
                      <span>Task id</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("taskId")}
                        >
                          <i
                            className={`bi ${sorting.taskId
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>

                  <th >
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("clietnId")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Work status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("workStatus")}
                        >
                          <i
                            className={`bi ${sorting.workStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("operationStatus")}
                        >
                          <i
                            className={`bi ${sorting.operationStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>

                  {/* <th>
                    <div className="d-flex">
                      <span>Assigned from</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("assignedFrom")}
                        >
                          <i
                            className={`bi ${sorting.assignedFrom
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th> */}
                  <th>
                    <div className="d-flex">
                      <span>Assigned To</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${sorting.ReportStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Duration(Min)</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${sorting.ReportStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Case Duration</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${sorting.ReportStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Tag</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                    <th>
                    <div className="d-flex">
                      <span>Category Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                    <th>
                    <div className="d-flex">
                      <span>Risk score status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Report  status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>created time</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${sorting.ReportStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>

                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      WrmOperationManagement &&
                        WrmOperationManagement.data
                        ? (
                          WrmOperationManagement.data && WrmOperationManagement.data.result.map((riskoperationmgmtlist, i) => {
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                 {(Role==="Admin"||Role==="Supervisor")&&  
                                 <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                                  <input
                                    className="form-check-input cursor-pointer "
                                    type="Checkbox"
                                    name='checkName'
                                    value={riskoperationmgmtlist._id}
                                    onChange={(e) => handleChange(e)}
                                    checked={formData && formData.includes(riskoperationmgmtlist._id)}
                                  />
                                </td>}
                              
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.clientId?.company ? riskoperationmgmtlist.clientId?.company : "--"}
                                  </>
                                </td>
                                  <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.caseAlert==="RED" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"red"}}></div> : riskoperationmgmtlist.caseAlert==="GREEN" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"green"}}></div>:riskoperationmgmtlist.caseAlert==="ORANGE" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"orange"}}></div>:riskoperationmgmtlist.caseAlert==="GRAY" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"gray"}}></div>:""}
                                  </>
                                </td>
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.taskAlert==="RED" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"red"}}></div> : riskoperationmgmtlist.taskAlert==="GREEN" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"green"}}></div>:riskoperationmgmtlist.taskAlert==="ORANGE" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"orange"}}></div>:riskoperationmgmtlist.taskAlert==="GRAY" ? <div style={{margin:"0px", padding:'10px 40px',backgroundColor:"gray"}}></div>:""}
                                  </>
                                </td>
                                <td className="ellipsis" >
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => history.push(`/wrmmanagement/update/${riskoperationmgmtlist._id}`)}
                                    to={`/wrmmanagement/update/${riskoperationmgmtlist._id}`}
                                  >
                                    TIN{
                                      riskoperationmgmtlist.ticketId ? riskoperationmgmtlist.ticketId : "--"
                                    }
                                  </a>
                                </td>
                             
                                <td className="ellipsis" >
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={()=>handleSearch("","","","",riskoperationmgmtlist.caseId)}
                                    // onClick={() => window.open(`/risk-summary/update/${riskoperationmgmtlist._id}`, "_blank")}
                                    to={`/wrm-management`}
                                  >
                                    WRM{
                                      riskoperationmgmtlist.caseId ? riskoperationmgmtlist.caseId : "--"
                                    }
                                  </a>
                                </td>
                               
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.workStatus ? riskoperationmgmtlist.workStatus : "--"}
                                  </>
                                </td>
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.operationStatus ? riskoperationmgmtlist.operationStatus : "--"}
                                  </>
                                </td>

                                {/* <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.assignedFrom?.firstName ? riskoperationmgmtlist.assignedFrom?.firstName : "--"}
                                  </>
                                </td> */}
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.assignedTo?.firstName ? riskoperationmgmtlist.assignedTo?.firstName : "--"}
                                  </>
                                </td>
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.taskEndTime ? moment.duration(moment(riskoperationmgmtlist.taskEndTime).diff(moment(riskoperationmgmtlist.taskStartTime))).asMinutes().toFixed(2) : "--"}
                                  </>
                                </td>
                                 <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.caseEndTime ? moment.duration(moment(riskoperationmgmtlist?.caseEndTime).diff(moment(riskoperationmgmtlist?.caseStartTime))).asMinutes().toFixed(2) : moment(riskoperationmgmtlist?.caseStartTime).startOf('hour').fromNow()}
                                  </>
                                </td>
                                <td className="ellipsis" >
                                  {riskoperationmgmtlist.tag ? riskoperationmgmtlist.tag : "--"}
                                </td>
                                   <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.riskStatus]}`}>
                                    {riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.riskStatus ? riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.riskStatus: "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.riskScoreStatus]}`}>
                                    {riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.riskScoreStatus ? riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.riskScoreStatus : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.reportStatus]}`}>
                                    {riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.reportStatus ? riskoperationmgmtlist.wrmId&&riskoperationmgmtlist.wrmId.reportStatus : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <>
                                    {riskoperationmgmtlist.caseStartTime ? moment(riskoperationmgmtlist.caseStartTime).startOf('minute').fromNow() : "--"}
                                  </>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { WrmOperationManagementStore, clinetListStore } = state
  return {
    WrmOperationManagement: state && state.WrmOperationManagementStore && state.WrmOperationManagementStore.WrmOperationManagement,
    loading: WrmOperationManagementStore && WrmOperationManagementStore.loading ? WrmOperationManagementStore.loading : false,
    WrmOperationActionStatus: state && state.wrmOperationManagementActionStore && state.wrmOperationManagementActionStore,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',

  }
}

const mapDispatchToProps = (dispatch) => ({
  getWrmOperationManagementlistDispatch: (params) => dispatch(WrmOperationManagementActions.getWrmOperationManagemnt(params)),
  clearClientsWrmDispatch: (data) => dispatch(GetClientsActions.clearClientsWrm(data)),
  getClientsWrmDispatch: (data) => dispatch(GetClientsActions.getClientsWrm(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  getWRMOperatorsDispatch: () => dispatch(WRMOperatorsListActions.getWRMOperatorsList()),

})

export default connect(mapStateToProps, mapDispatchToProps)(WrmOperationManagementList)
