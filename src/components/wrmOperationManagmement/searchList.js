import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _, { values } from 'lodash'
import {
  GetClientsActions,
  WrmOperationManagementActions
} from '../../store/actions'
// import './styles.css';
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setLocalStorage, getLocalStorage, removeLocalStorage } from '../../utils/helper';
import { DateSelector } from '../../theme/layout/components/DateSelector'
import { DATE, SET_FILTER } from '../../utils/constants'
import moment from "moment"

function SearchList(props) {
  const {

    getWrmOperationManagementlistDispatch,
    Value,
    clientList,
    wrmOperatorsList
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [buil, setBUild] = useState(false);
  const [dataValue, setDataValue] = useState({});
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const headClientId = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const [manualFormData, setManualFormData] = useState({
    tag: '',
    createdAtFrom: '',
    createdAtTo: '',
    ticketId: '',
    operationStatus:'',
    workStatus:'',
    role:'',
    caseId:'',
    clientId:"",
    assignedTo:"",
    riskScoreStatus:"",
    reportStatus:"",
    riskStatus:"",
    caseAlert:'',
    taskAlert:''
  })
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const operationStatus = ["BACKEND PROCESSING","ANALYST REVIEW","QA REVIEW"]
  const workStatus=["PENDING","IN PROGRESS","COMPLETED","HOLD"]
  const riskScoreStatus =  ['PENDING', 'PROCESSING', 'COMPLETED']
  const reportStatus = ['PENDING', 'QUEUED', 'REJECTED', 'DATA CAPTURED', 'COMPLETED', 'TAG PROCESSING', 'TAG CAPTURED', 'WAITING FOR REPORT', ]
  const riskStatus = ['PENDING', 'APPROVED', 'REJECTED', 'MANUAL REVIEW']
  const alert = ["RED","ORANGE","GREEN","GRAY"]
  const handleSearch = () => {
    const UpDateFrom = moment(manualFormData.createdAtFrom).format("YYYY-MM-DD")
    const UpDateTo = moment(manualFormData.createdAtTo).format("YYYY-MM-DD")
    if (moment(UpDateFrom).isAfter(UpDateTo)) {
      setError({
        createdDate: "From Date Should Be Less Than To Date",
      });
      return setShow(true);
    }
    const params = {
      tag: manualFormData.tag,
      ticketId: manualFormData.ticketId,
      createdAtFrom: UpDateFrom === 'Invalid date' ? '' : UpDateFrom,
      createdAtTo: UpDateTo === 'Invalid date' ? moment(new Date()).format("YYYY/MM/DD") : UpDateTo,
      ticketId: manualFormData.ticketId,
      clientId: manualFormData.clientId ? manualFormData.clientId : '',
      assignedTo: manualFormData.assignedTo ? manualFormData.assignedTo : '',
      operationStatus:manualFormData.operationStatus,
      workStatus:manualFormData.workStatus,
      caseId:manualFormData.caseId,
      riskScoreStatus:manualFormData.riskScoreStatus,
      reportStatus:manualFormData.reportStatus,
      riskStatus:manualFormData.riskStatus,
      caseAlert:manualFormData.caseAlert,
      taskAlert:manualFormData.taskAlert,
    }
    setShow(false)
    getWrmOperationManagementlistDispatch(params)
    setDataValue(params)
    setLocalStorage('WRM_OPS_SEARCH', JSON.stringify(
      params
    ))
    const data = {
      tag: manualFormData.tag,
    }

  }
  const clearPopup = () => {
    setShow(false)
  }
  
  const tagSearch = JSON.parse(getLocalStorage('WRM_OPS_SEARCH'))
  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
      website: tagSearch.website,
      tag: tagSearch.tag,
      riskStatus: tagSearch.riskStatus,
      reportStatus: tagSearch.reportStatus,
      riskScoreStatus:tagSearch.riskScoreStatus,
      caseAlert:tagSearch.caseAlert,
      taskAlert:tagSearch.taskAlert,
      ticketId: tagSearch.ticketId,
      clientId:tagSearch.clientId,
      assignedTo:tagSearch.assignedTo,
      operationStatus:tagSearch.operationStatus,
      workStatus:tagSearch.workStatus,
      caseId:tagSearch.caseId
    }))
    show && getWrmOperationManagementlistDispatch(manualFormData)
  }, [show])
  useEffect(() => {
    if (Value === true) {
      setManualFormData(values => ({
        ...values,
        website: '',
        acquirer: '',
        tag: '',
        riskStatus: '',
        reportStatus: '',
        createdAtFrom: '',
        createdAtTo: '',
        riskScoreStatus:"",
        reportStatus:"",
        riskStatus:"",
        caseAlert:'',
        taskAlert:''
      }))
    } else {
      setBUild(true)
    }
  }, [Value])
  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }
 const hadleReset = (() => {
    setManualFormData(values => ({
           tag: '',
          createdAtFrom: '',
          createdAtTo: '',
          ticketId: '',
          operationStatus:'',
          workStatus:'',
          role:'',
          caseId:'',
          clientId:"",
          assignedTo:"",
          riskScoreStatus:"",
          reportStatus:"",
          riskStatus:"",
          caseAlert:'',
          taskAlert:''
      }))

    removeLocalStorage("WRM_OPS_SEARCH")

  })
  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-lg btn-primary btn-responsive font-6vw me-3 pull-right'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
                <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Client:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="clientId"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.clientId || ""}
                >
                  <option value="">Select...</option>
                  {clientList?.map((ele) => (
                    <option
                      value={ele._id}
                      key={ele._id}
                    >
                      {ele.company}
                    </option>
                  ))}
                </select>
              </div>
            </div>
                <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Operator:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="assignedTo"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.assignedTo || ""}
                >
                  <option value="">Select...</option>
                   {wrmOperatorsList?.map((user) => (
                    <option
                      value={user._id}
                      key={user._id}
                    >
                      {`${user.firstName} ${user.lastName}`}
                    </option>
                  ))}
                </select>
              </div>
            </div>      
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Operation Status:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="operationStatus"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.operationStatus || ""}
                >
                  <option value="">Select...</option>
                  {operationStatus.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Work Status:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="workStatus"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.workStatus || ""}
                >
                  <option value="">Select...</option>
                  {workStatus.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
             <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Case Alert:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="caseAlert"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.caseAlert || ""}
                >
                  <option value="">Select...</option>
                  {alert.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
             <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Task alert:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="taskAlert"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.taskAlert || ""}
                >
                  <option value="">Select...</option>
                  {alert.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
             <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Category Status:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="riskStatus"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.riskStatus || ""}
                >
                  <option value="">Select...</option>
                  {riskStatus.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report  Status:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="reportStatus"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.reportStatus || ""}
                >
                  <option value="">Select...</option>
                  {reportStatus.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Risk Score  Status:
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="riskScoreStatus"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.riskScoreStatus || ""}
                >
                  <option value="">Select...</option>
                  {riskScoreStatus.map((status) => (
                    <option
                      value={status}
                      key={status}
                    >
                      {status}
                    </option>
                  ))}
                </select>
              </div>
            </div>
                 <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Tag :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Tag'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.tag && error.tag },
                    {
                      'is-valid': manualFormData.tag && !error.tag
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  onBlur={e => handleTrimWhiteSpace(e, setManualFormData)}
                  type='text'
                  name='tag'
                  autoComplete='off'
                  value={manualFormData.tag || ''}
                />
              </div>
            </div>
           
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Ticket Id :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Tickt Id'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.ticketId && error.ticketId },
                    {
                      'is-valid': manualFormData.ticketId && !error.ticketId
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  onBlur={e => handleTrimWhiteSpace(e, setManualFormData)}
                  type='text'
                  name='ticketId'
                  autoComplete='off'
                  value={manualFormData.ticketId || ''}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Case id :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='caseId'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.caseId && error.caseId },
                    {
                      'is-valid': manualFormData.caseId && !error.caseId
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type='text'
                  name='caseId'
                  autoComplete='off'
                  value={manualFormData.caseId || ''}
                />
                {error && error.caseId && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.caseId}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Created date :
                </label>
              </div>
              <div className="col-md-4">
                <DateSelector
                  name="createdAt"
                  placeholder="Created Date From"
                  className="form-control"
                  selected={manualFormData.createdAtFrom || ""}
                  onChange={(date) => {
                    setManualFormData((values) => ({
                      ...values,
                      createdAtFrom: date,
                    }));
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  maxDate={new Date()}
                  isClearable={true}
                />
              </div>
              <div className="col-md-4">
                <DateSelector
                  name="createdAt"
                  placeholder="Created Date To"
                  className="form-control"
                  selected={manualFormData.createdAtTo || ""}
                  onChange={(date) => {
                    setManualFormData((values) => ({
                      ...values,
                      createdAtTo: date,
                    }));
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  maxDate={new Date()}
                  isClearable={true}
                />
              </div>
              {error && error.createdDate && (
                <div className="rr mt-1 text-justify">
                  <style>{".rr{color:red}"}</style>
                  {error.createdDate}
                </div>
              )}
            </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  className='btn btn-primary m-1 mt-8 font-5vw '
                  onClick={()=>hadleReset()}>
                  Reset
                </button>
                <button
                  className='btn btn-primary m-1 mt-8 font-5vw '
                  onClick={handleSearch}>
                  Search
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
WrmOperatorsListStore,
riskManagementlistStore,
RiskLevelStore
  } = state
  return {
    loading: riskManagementlistStore &&riskManagementlistStore.loading,
    loadingRL: RiskLevelStore &&RiskLevelStore.loadingRL,
    wrmOperatorsList:WrmOperatorsListStore&& WrmOperatorsListStore.WrmOperators?WrmOperatorsListStore.WrmOperators.data:[]

  }
}

const mapDispatchToProps = dispatch => ({
  getWrmOperationManagementlistDispatch: (params) => dispatch(WrmOperationManagementActions.getWrmOperationManagemnt(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)