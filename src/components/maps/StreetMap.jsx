import React, { useState, useEffect } from "react";
import { GoogleMap, StreetViewPanorama } from "@react-google-maps/api";
import { MAP } from '../../utils/constants'

function StreetMap(props) {
    const { mapData } = props
    const [viewDefault, setViewDefault] = useState("phone") // phone/personal/ip
    const mapContainerStyle = {
        height: "450px",
        width: "100%"
    }
    const center = {
      lat: 37.5247596,
      lng: -122.2583719
    }

    useEffect(() => {
        if (mapData) {
            let businessAddressLocation ={
                lat:parseFloat(mapData.businessAddressLocation?.lat),
                lng:parseFloat(mapData.businessAddressLocation?.long)
            }
            // let businessPhoneLocation ={
            //     lat:parseFloat(mapData.businessPhoneLocation?.lat),
            //     lng:parseFloat(mapData.businessPhoneLocation?.long)
            // }
            // let individualAddressLocation ={
            //     lat:parseFloat(mapData.individualAddressLocation?.lat),
            //     lng:parseFloat(mapData.individualAddressLocation?.long)
            // }
            // let phoneLocation ={
            //     lat:parseFloat(mapData.phoneLocation?.lat),
            //     lng:parseFloat(mapData.phoneLocation?.long)
            // }
            // let ipLocation ={
            //     lat:parseFloat(mapData.ipLocation?.lat),
            //     lng:parseFloat(mapData.ipLocation?.long)
            // }
            // console.log(mapData)
            // console.log(businessAddressLocation)
            setViewDefault(businessAddressLocation)
        }
    }, [mapData])

    return (
        <GoogleMap
            id="googlemap-streetview"
            mapContainerStyle={mapContainerStyle}
            zoom={17}
            //center={viewDefault}
            // onLoad={() => console.log("Loaded GoogleMap")}
            >
                <StreetViewPanorama
                    position={viewDefault}
                    enableCloseButton={false}
                    linksControl={false}
                    addressControl={true}
                    visible={true}
                    motionTracking={true}
                    motionTrackingControl={true}
                    // onLoad={() => console.log("Loaded StreetViewPanorama")}
                />
        </GoogleMap>
    );
}

export default StreetMap;