import React, { useState, Fragment } from "react";
import { GoogleMap, InfoWindow, Marker, Polyline } from "@react-google-maps/api";
import { MAP } from '../../utils/constants'

function MapGoogle(props) {
  const { mapData, mapMarkers } = props
  const [activeMarker, setActiveMarker] = useState(null);
  const [center, setCenter] = useState({ lat: 13.0405984, lng: 80.19304 });

  // const [center, setCenter] = useState({ 
  //   lat: centerPoint.lat ? centerPoint.lat : 13.0405984, 
  //   lng: centerPoint.lng ? centerPoint.lng : 80.19304
  // });

  const polyOptions = {
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    clickable: false,
    draggable: false,
    editable: false,
    visible: true,
    radius: 30000,
    zIndex: 1
  }

  const handleActiveMarker = (marker) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  }


  return (
    <GoogleMap
      zoom={4}
      center={center}
      onClick={() => setActiveMarker(null)}
      mapContainerStyle={{ width: "100%", height: "100vh" }}
      // defaultCenter={}
      >
      {mapMarkers && mapMarkers.map((mark, index) => (
        <Fragment key={"M_" + index}>
          <Marker
            position={{
              lat: parseFloat(mark.lat),
              lng: parseFloat(mark.lng)
            }}
            onClick={() => handleActiveMarker(index)}
            icon={`${MAP[mark.area]}`}>
            {activeMarker === index ? (
              <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                <div>{mark.area}</div>
              </InfoWindow>
            ) : null}
          </Marker>
          <Polyline
            path={mapMarkers}
            options={polyOptions}
          />
        </Fragment>
      ))}
    </GoogleMap>
  );
}

export default MapGoogle;