import React, { useState, useEffect, Fragment} from "react";
import { GoogleMap, InfoWindow, Marker, Polyline } from "@react-google-maps/api";
import { MAP } from '../../utils/constants'

function MapGoogleSingle(props) {
    const { mapData, address } = props
    const [activeMarker, setActiveMarker] = useState(1);
    const [bussinessAddressLocation, setBussinessAddressLocation] = useState(null)
    const [addressLocation, setAddressLocation] = useState(null)
    const [bussinessPhoneLocation, setBussinessPhoneLocation] = useState(null)
    const [ipLocation, setIplocation] = useState(null)
    const [phoneLocation, setPhoneLocation] = useState(null)
    const [defaultLocation, setDefaultLocation] = useState(null)

    console.log('addressLocation', addressLocation)
    console.log('bussinessAddressLocation', bussinessAddressLocation)


    useEffect(() => {
        if (mapData!==undefined) {
            if(mapData["businessAddressLocation"] !== undefined){
                let bal = mapData["businessAddressLocation"]
                if(bal.lat !== '' && bal.long !== ''){
                    let map_data = {
                        icon:'BUSINESS ADDRESS',
                        center:'',
                        position:{
                            lat:parseFloat(bal.lat),
                            lng:parseFloat(bal.long)
                        },
                        info: mapData?.businessAddress,
                    }

                    setBussinessAddressLocation(map_data)
                }                
            }
            if(mapData["individualAddressLocation"] !== undefined){
                let bal = mapData["individualAddressLocation"]
                if(bal.lat !== '' && bal.long !== ''){
                    let map_data = {
                        icon:'INDIVIDUALADDRESS',
                        center:'',
                        position:{
                            lat:parseFloat(bal.lat),
                            lng:parseFloat(bal.long)
                        },
                        info: mapData?.businessAddress,
                    }

                    setAddressLocation(map_data)
                }                
            }
            if(mapData["businessPhoneLocation"] !== undefined){
                let bpl = mapData["businessPhoneLocation"]
                if(bpl.lat !== '' && bpl.long !== ''){
                    let map_data = {
                        icon:'BUSINESS ADDRESS',
                        center:'',
                        position:{
                            lat:parseFloat(bpl.lat),
                            lng:parseFloat(bpl.long)
                        },
                        info: mapData?.businessPhoneAddress,
                    }

                    setBussinessPhoneLocation(map_data)
                }                
            }
            if(mapData["ipLocation"] !== undefined){
                let il = mapData["ipLocation"]
                if(il.lat !== '' && il.long !== ''){
                    let map_data = {
                        icon:'INDIVIDUALADDRESS',
                        center:'',
                        position:{
                            lat:parseFloat(il.lat),
                            lng:parseFloat(il.long)
                        },
                        info: '',
                    }

                    setIplocation(map_data)
                }                
            }
            if(mapData["phoneLocation"] !== undefined){
                let pl = mapData["phoneLocation"]
                if(pl.lat !== '' && pl.long !== ''){
                    let map_data = {
                        icon:'INDIVIDUALADDRESS',
                        center:'',
                        position:{
                            lat:parseFloat(pl.lat),
                            lng:parseFloat(pl.long)
                        },
                        info: mapData?.phoneAddress,
                    }

                    setPhoneLocation(map_data)
                }                
            }
        }
    }, [mapData])

    const handleOnLoad = (map) => {
        if(address==='address'){
            console.log()
            if(addressLocation!==null){
                setDefaultLocation(addressLocation)

                const bounds = new window.google.maps.LatLngBounds();
                bounds.extend(addressLocation?.position);
                map.fitBounds(bounds);
            }            
        }
        else if(address==='bussiness'){
            if(bussinessAddressLocation!==null){
                setDefaultLocation(bussinessAddressLocation)

                const bounds = new window.google.maps.LatLngBounds();
                bounds.extend(bussinessAddressLocation?.position);
                map.fitBounds(bounds);
            }
        }
    }

    console.log('defaultLocation', defaultLocation)

    return (
        <Fragment>
            { defaultLocation!== null? (
                <GoogleMap
                        zoom={1}
                        center={defaultLocation?.center}
                        onLoad={handleOnLoad}
                        mapContainerStyle={{ width: "100%", height: "100vh" }}>
                        <Marker
                            position={defaultLocation?.position}
                            icon={`${MAP[defaultLocation?.icon]}`}>
                            <InfoWindow>
                                <div>{defaultLocation?.info}</div>
                            </InfoWindow>
                        </Marker>
                    </GoogleMap>
                ) : (
                    <GoogleMap
                        zoom={1}
                        center={{
                            lat : 38.94902400133194, 
                            lng : -100.24342008381274
                        }}
                        mapContainerStyle={{ width: "100%", height: "100vh" }}>
                    </GoogleMap>
                )
            }            
            <div>
                { defaultLocation === null ? 'Lat & Lon Empty !' : null}
            </div>
        </Fragment>
    );
}

export default MapGoogleSingle;