import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import {
  PostCategory1Actions,
} from '../../store/actions'
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import { KTSVG } from '../../theme/helpers'
import Modal from 'react-bootstrap/Modal'
import _ from 'lodash'
import Select from 'react-select'

function BlockListCategory1 (props) {
  const {
    blacklistDropdown,
    BlockListType,
    postCategory1Res,
    PostCategroy1Dispatch,
    ClearPostCategroy1,
    Categoryloading,
    blockListValue,
    manualCategorys,
    riskIdValueId,
    showBlockListClient
  } = props

  const [categoryValue, setCategoryValue] = useState()
  const [show, setShow] = useState(false)
  const [formData, setFormData] = useState({
    category: '',
    type: 'whiteList',
    riskId: blockListValue ? blockListValue : '',
    status: 'APPROVED'
  })
  const [errors, setErrors] = useState({
    reason: '',
  })

  const onConfirmUpdate = ((value) => {
    const formValue = {
      category: value,
      riskId: blockListValue,
      status: 'REJECTED',
      type: 'blockList'
    }
    PostCategroy1Dispatch(formValue)
  })

  const onCategoryChange = (category) => {
    const { value } = category
    setCategoryValue(value)
    if (value !== 'Others') {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        `Want to update the Category ${value} to the Case #IRM${riskIdValueId}?`,
        'warning',
        'Yes',
        'No',
        () => { onConfirmUpdate(value) },
        () => { }
      )
    } else if (value === 'Others') {
      setShow(true)
    }
  }

  useEffect(() => {
    if (postCategory1Res && postCategory1Res.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        postCategory1Res && postCategory1Res.message,
        'success'
      )
      ClearPostCategroy1()
      setShow(false)
      setFormData({
        category: '',
      })
    } else if (postCategory1Res && postCategory1Res.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postCategory1Res && postCategory1Res.message,
        '',
        'Try again',
        '',
        () => { }
      )
      ClearPostCategroy1()
    }
  }, [postCategory1Res])

  const bloaklistDatas = BlockListType && BlockListType.data
  const clearPopup = () => {
    setShow(false)
  }

  const rejectChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const Submit = () => {
    const errors = {}
    if (_.isEmpty(formData.category)) {
      errors.category = "Category Is Required"
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const formvalue = {
        category: formData.category,
        type: 'whiteList',
        riskId: blockListValue,
        status: 'APPROVED'
      }
      PostCategroy1Dispatch(formvalue)
    }
  }

  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Override Category
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className='card-header label-four'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12 mb-3 mt-4'>

                      <div className='row mb-2 align-items-cente'>
                        <div className='col-lg-6 col-md-6 col-sm-6 mt-4'>
                          <div className='col-lg-11 col-md-11 col-sm-11 mb-2 w-300px'>
                            <input
                              name='category'
                              type='text'
                              className={'form-control form-control-lg form-control-solid'}
                              placeholder='Category'
                              onChange={(e) => rejectChange(e)}
                              autoComplete='off'
                              value={formData.category || ''}
                            // value={() => { categoryValue ? formData.category : '' }}
                            />
                            {errors.category && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert text-danger'>{errors.category}</span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='form-group row mb-4 mt-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11 d-flex justify-content-end me-4 mb-4 mt-4'>
                        <button type="button"
                          className="btn btn-light-primary me-10 btn-sm"
                          onClick={() => Submit()}
                        >
                          {!Categoryloading && <span className='indicator-label'>
                            <KTSVG path="/media/icons/duotune/finance/fin001.svg" />
                            Update Category
                          </span>}
                          {Categoryloading && (
                            <span className='indicator-progress' style={{ display: 'block' }}>
                              Please wait...
                              <span className='spinner-border spinner-border-sm align-middle ms-2' />
                            </span>
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <div className='flex-column w-240px'>
        { blacklistDropdown !== null ? (
          <Select 
              menuPortalTarget={document.body} 
              styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
              options={blacklistDropdown} 
              defaultValue={()=>{
                if(manualCategorys!==undefined){
                    if (blacklistDropdown.hasOwnProperty(manualCategorys)) {
                        let one = blacklistDropdown && blacklistDropdown.filter(b=>b.value === manualCategorys);
                        return one[0]
                    }
                    else{
                        return {
                          value:manualCategorys,
                          label:manualCategorys,
                        }                   
                    }                  
                }
                            
              }}
              onChange={onCategoryChange}                
            />
          ) : null }
          
        </div>
    </>
  )
}


const mapStateToProps = (state) => {
  const {
    BlockListTypeStore,
    PostCategoryStore1
    } = state

  return {
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType?.data : null,
    postCategory1Res: PostCategoryStore1 && PostCategoryStore1.postCategory1Res ? PostCategoryStore1.postCategory1Res?.data : '',
    Categoryloading: PostCategoryStore1 && PostCategoryStore1.loading ? PostCategoryStore1.loading : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  PostCategroy1Dispatch: (params) => dispatch(PostCategory1Actions.PostCategory1(params)),
  ClearPostCategroy1: () => dispatch(PostCategory1Actions.ClearPostCategory()),
})


export default connect(mapStateToProps, mapDispatchToProps)(BlockListCategory1)