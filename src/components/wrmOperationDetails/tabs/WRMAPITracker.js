import {  WrmOperationBackendApiAllScheduleAction, WrmOperationBackendNamedApiScheduleAction, updateStatusChangeActions } from "../../../store/actions"
import { connect } from 'react-redux'
import axiosInstance from "../../../services";
import serviceList from "../../../services/serviceList";

function WRMAPITracker(props) {
  const {
    WrmOperationManagementDetails,
    WrmOperationManagementBackendAPIStatus,
    postStatusChangeDispatch,
    urlId,
    backendApiScheduleDispatch,
  } = props
  
  const handleChangeStatus =(status)=>{
  let id = WrmOperationManagementDetails&&WrmOperationManagementDetails.wrmId&&WrmOperationManagementDetails.wrmId._id
  let updateStatus ={updateStatus:status} 
  postStatusChangeDispatch(id,updateStatus)
  }
  const handleResheduleAllBackendAPIs = (name)=>{
     backendApiScheduleDispatch(urlId,name)
  }
  return (
    <>
    <div className='row g-5 g-xl-8 mb-8' >
           <div className='col-xl-12'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-body pt-0'>
             <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                 WRM information
                </span>
              </h3>
            </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Report status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.reportStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
               <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Risk score status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.riskScoreStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
               <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Auto status

                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.autoStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Risk status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.riskStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Job id
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.jobId : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     API Key
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.backend_api_key : '--'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div> 
       <div className='row g-5 g-xl-8 mb-8' >
           <div className='col-xl-12'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-body pt-0'>
             <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                 WRM Report Status
                </span>
              </h3>
            </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     PENDING
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleChangeStatus("Apidata-ReScheduled")}>Click here</button>
                  </div>
                </div>
              </div>
               <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     QUEUED
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleChangeStatus("Apidata-Recapture")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     WAITING FOR REPORT
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleChangeStatus("Regenerate-Report")}>Click here</button>
                  </div>
                </div>
              </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     REGENERATE RISKSCORE
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleChangeStatus("Regenerate-Riskscore")}>Click here</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div> 
    <div className='row g-5 g-xl-8 mb-8' >
           <div className='col-xl-12'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-body pt-0'>
             <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                 WRM Backend API Status
                </span>
                 
              </h3>         
            </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Summary
                    </span>
                  </div>

                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Completed
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.summary.status_completed}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Failed
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.summary.status_failed}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     In progress
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.summary.status_inprogress}
                  </div>
                </div>
              </div>
                            <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Inqueue
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.summary.status_inqueue}
                  </div>
                </div>
              </div>
                            <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Not started
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.summary.status_not_started}
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    <b> Total</b>
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <b> {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.summary.status_total}</b>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     dns_lookup
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[0].status}
                  </div>
                </div>
              </div>
               <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     domain_info
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[1].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     website_categorization_v4
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[2].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     website_details
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[3].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    ssl_vulnerabilty_detection
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[4].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    ssl_check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[5].status}
                  </div>
                </div>
              </div>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     web_risk_analysis
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[6].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    website_image_detection
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[7].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    predict_medicine
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[8].status}
                  </div>
                </div>
              </div>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     safe_browsing
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[9].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    review_analysis
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[10].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    logo_check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[11].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    verify_domain_v3
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[12].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                   website_reachability_check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[13].status}
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    predict_fake_logo_name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    {WrmOperationManagementBackendAPIStatus&&WrmOperationManagementBackendAPIStatus.data&&WrmOperationManagementBackendAPIStatus.data.data&&WrmOperationManagementBackendAPIStatus.data.data[14].status}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
        <div className='row g-5 g-xl-8 mb-8' >
           <div className='col-xl-12'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-body pt-0'>
             <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                 WRM Backend API Action
                </span>
                 
              </h3>         
            </div>
            <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Reschedule all
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs()}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     dns_lookup
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("dns_lookup")}>Click here</button>
                  </div>
                </div>
              </div>
               <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     domain_info
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("domain_info")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     website_categorization_v4
                    </span>
                  </div>
                 <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("website_categorization_v4")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     website_details
                    </span>
                  </div>
                 <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("website_details")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    ssl_vulnerabilty_detection
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("ssl_vulnerabilty_detection")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    ssl_check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("ssl_check")}>Click here</button>
                  </div>
                </div>
              </div>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     web_risk_analysis
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("web_risk_analysis")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    website_image_detection
                    </span>
                  </div>
                 <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("website_image_detection")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    predict_medicine
                    </span>
                  </div>
                   <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("predict_medicine")}>Click here</button>
                  </div>
                </div>
              </div>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     safe_browsing
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("safe_browsing")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    review_analysis
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("review_analysis")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    logo_check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("logo_check")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    verify_domain_v3
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("verify_domain_v3")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                   website_reachability_check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("website_reachability_check")}>Click here</button>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    predict_fake_logo_name
                    </span>
                  </div>
                   <div className='col-lg-6 col-md-6 col-sm-6'>
                    <button onClick={()=>handleResheduleAllBackendAPIs("predict_fake_logo_name")}>Click here</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>       
    </>
  )
}
const mapStateToProps = (state) => {
  return ({
      loading: null
})}
const mapDispatchToProps = (dispatch) => ({
  postStatusChangeDispatch: (id, data) => dispatch(updateStatusChangeActions.updateStatusChange(id, data)),
  clearStatusChange: () => dispatch(updateStatusChangeActions.clearStatusChange()),
  backendApiScheduleDispatch:(id,name)=>dispatch(WrmOperationBackendApiAllScheduleAction.WrmApiScheduleAllRequest(id,name)),
})

export default connect(mapStateToProps, mapDispatchToProps)(WRMAPITracker)
