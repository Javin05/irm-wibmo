import { useState } from 'react'
import moment  from 'moment'
import PMA from "../PMA"
import BlockListCategory1 from '../blockListCategory1'
import BlockListCategory from '../blockListCategory'
import { getLocalStorage } from '../../../utils/helper'
import PriceCheckPopup from '../../shared-components/PriceCheckPopup'

function Summary(props) {
  const {
    WrmOperationManagementDetails,blacklistDropdown
  } = props
  const [blockListValue, setblockListValue] = useState()
    const [riskIdValue, setRiskIdValue] = useState()

  const getIdValue = ((value, riskId) => {
    setblockListValue(value)
    setRiskIdValue(riskId)
  })
const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))

  return (
    <>
       <div className='row g-5 g-xl-8 mb-8' >
           <div className='col-xl-12'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Ticket id
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-dark-700'>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.ticketId ?  `TIN${WrmOperationManagementDetails.ticketId}` : '--'}
                    </span>
                  </div>
                </div>
              </div>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Client Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        {WrmOperationManagementDetails && WrmOperationManagementDetails.clientId ?  WrmOperationManagementDetails.clientId?.company : '--'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Tag
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.tag ?  WrmOperationManagementDetails.tag : '--'}
                    </span>
                  </div>
                </div>
              </div>
              
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Case id
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.caseId ?  `WRM${WrmOperationManagementDetails.caseId}` : '--'}
                    </span>
                  </div>
                </div>
              </div>
            
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Website
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId?.website : '--'}                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Operation status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                     {WrmOperationManagementDetails && WrmOperationManagementDetails.operationStatus ?  WrmOperationManagementDetails.operationStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Work Status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                     {WrmOperationManagementDetails && WrmOperationManagementDetails.workStatus ?  WrmOperationManagementDetails.workStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Created time
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.caseStartTime ?  moment(WrmOperationManagementDetails.caseStartTime).startOf('minute').fromNow() : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Report status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.reportStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Risk score status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.riskScoreStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Auto status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.autoStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Category status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.riskStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Category reason
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.policyStatus : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Level1 category
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.categoryReason : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Level1 reason
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                     {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.level1Category : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Level 1 ReasonAuto
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                     {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.level1ReasonAuto : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Level 2 category
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.level2Category : '--'}
                    </span>
                  </div>
                </div>
              </div>
                  <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Level 2 reason
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.level2Reason : '--'}
                    </span>
                  </div>
                </div>
              </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Reason
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.reason : '--'}
                    </span>
                  </div>
                </div>
              </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     PMA
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.wrmId ?  WrmOperationManagementDetails.wrmId.pma : '--'}
                    </span>
                  </div>
                </div>
              </div>
           
              {(WrmOperationManagementDetails?.workStatus === "IN PROGRESS" && WrmOperationManagementDetails.taskQAAccepted||WrmOperationManagementDetails?.workStatus === "IN PROGRESS" && WrmOperationManagementDetails.taskAnalystAccepted) && 
              <>
                      <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Edit PMA
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                        <PMA
                        manualCategorys={WrmOperationManagementDetails?.wrmId?.pma}
                        blockListValue={WrmOperationManagementDetails?.wrmId?._id}
                        riskIdValueId={WrmOperationManagementDetails?.wrmId?.riskId}
                      />
                    </span>
                  </div>
                </div>
              </div>
                      <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Edit category
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                    <BlockListCategory
                      blacklistDropdown={blacklistDropdown}
                      manualCategorys={WrmOperationManagementDetails?.wrmId?.level2Category}
                      blockListValue={WrmOperationManagementDetails?.wrmId?._id}
                      riskIdValueId={WrmOperationManagementDetails?.wrmId?.riskId}

                      />                    </span>
                  </div>
                </div>
              </div>
                   <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Edit category1
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                    <BlockListCategory1
                      blacklistDropdown={blacklistDropdown}
                      manualCategorys={WrmOperationManagementDetails?.wrmId?.level1Category}
                      blockListValue={WrmOperationManagementDetails?.wrmId?._id}
                      riskIdValueId={WrmOperationManagementDetails?.wrmId?.riskId}
                      />                    </span>
                  </div>
                </div>
              </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Tag processing
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                     {
                                      // riskmgmtlist.policyComplainceChecks === true && 
                                      // riskmgmtlist.riskStatus === 'APPROVED' &&
                                      WrmOperationManagementDetails?.wrmId?.reportStatus === 'TAG PROCESSING'
                                        ?
                                        <span
                                          onClick={() => getIdValue( WrmOperationManagementDetails?.wrmId?._id,  WrmOperationManagementDetails?.wrmId?.riskId)}
                                        >
                                          <PriceCheckPopup
                                            blockListValue={blockListValue}
                                            tagSearch={tagSearch}
                                          />
                                        </span>
                                        : 'Processed'
                                    }
                  </div>
                </div>
              </div>
              </>}
              
            </div>
          </div>
        </div>
    </div>  
    </>
  )
}

export default Summary