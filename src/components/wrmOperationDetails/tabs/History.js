import React from 'react'
const History = ({WrmOperationManagementDetails}) => {
                return ( 
<>
    <div className='row g-5 g-xl-8 mb-8' >
        {WrmOperationManagementDetails?.taskHistory.map((task, index)=>{
                return (
 <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                {`Task Number ${task?.ticketId}` }
                </span>
              </h3>
            </div>

            <div className='card-body pt-0'>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      {task.note}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
                )
        })}
      
</div>
</>
                 );
}
 
export default History;