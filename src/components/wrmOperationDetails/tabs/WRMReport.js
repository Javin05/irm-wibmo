import wrmOperationDetails from ".."
import EditReport from "../EditReport"
function WRMReport(props) {
  const {
    WrmOperationManagementDetails
  } = props
const data = [
    {
        "name": "Web Url",
        "value": `${WrmOperationManagementDetails?.report?.webUrl}`
    },
    {
        "name": "Tag",
        "value": `${WrmOperationManagementDetails?.report?.tag}`
    },
    {
        "name": "Status",
        "value": `${WrmOperationManagementDetails?.report?.status}`
    },
    {
        "name": "Reason",
        "value": `${WrmOperationManagementDetails?.report?.reason}`
    },
    {
        "name": "Acquirer",
        "value": `${WrmOperationManagementDetails?.report?.acquirer}`
    },
    {
        "name": "Level1 Status",
        "value": `${WrmOperationManagementDetails?.report?.level1Status}`
    },
    {
        "name": "Level1 Category",
        "value": `${WrmOperationManagementDetails?.report?.levelCategory}`
    },
    {
        "name": "Level2 Status",
        "value": `${WrmOperationManagementDetails?.report?.level2Status}`
    },
    {
        "name": "Level2 Category",
        "value": `${WrmOperationManagementDetails?.report?.level2Category}`
    },
    {
        "name": "Website Working?",
        "value": `${WrmOperationManagementDetails?.report?.websiteWorking}`
    },
    {
        "name": "Legal Name",
        "value": `${WrmOperationManagementDetails?.report?.legalName}`
    },
    {
        "name": "Legal Name Input",
        "value": `${WrmOperationManagementDetails?.report?.legalNameInput}`
    },
    {
        "name": "Legal Name Scrapped",
        "value": `${WrmOperationManagementDetails?.report?.legalNameScrapped}`
    },
    {
        "name": "Legal Name Match",
        "value": `${WrmOperationManagementDetails?.report?.legalNameMatch}`
    },
    {
        "name": "Suspicious Domain",
        "value": `${WrmOperationManagementDetails?.report?.suspiciousDomain}`
    },
    {
        "name": "Malware Present",
        "value": `${WrmOperationManagementDetails?.report?.malwarePresent}`
    },
    {
        "name": "Malware Risk",
        "value": `${WrmOperationManagementDetails?.report?.malwareRisk}`
    },
    {
        "name": "Domain Registered",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistered}`
    },
    {
        "name": "Domain Registration Company",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrationCompany}`
    },
    {
        "name": "Domain Registration Date",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrationDate}`
    },
    {
        "name": "Domain Registration Expiry Date",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrationExpiryDate}`
    },
    {
        "name": "Domain Registrar Risk",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrarRisk}`
    },
    {
        "name": "Ssl Certificate Check",
        "value": `${WrmOperationManagementDetails?.report?.sslCertificateCheck}`
    },
    {
        "name": "Adult Content Monitoring",
        "value": `${WrmOperationManagementDetails?.report?.adultContentMonitoring}`
    },
    {
        "name": "Adult Content Monitoring Keyword",
        "value": `${WrmOperationManagementDetails?.report?.adultContentMonitoringKeyword}`
    },
    {
        "name": "Mcc Code Input",
        "value": `${WrmOperationManagementDetails?.report?.mccCodeInput}`
    },
    {
        "name": "Mcc Code Match",
        "value": `${WrmOperationManagementDetails?.report?.mccCodeMatch}`
    },
    {
        "name": "Mcc Code Scrapped",
        "value": `${WrmOperationManagementDetails?.report?.webUrl}`
    },
    {
        "name": "Product Category",
        "value": `${WrmOperationManagementDetails?.report?.productCategory}`
    },
    {
        "name": "Merchant Policy Link Work",
        "value": `${WrmOperationManagementDetails?.report?.merchantPolicyLinkWork}`
    },
    {
        "name": "Negative Keywords",
        "value": `${WrmOperationManagementDetails?.report?.negativeKeywords}`
    },
    {
        "name": "Readiness",
        "value": `${WrmOperationManagementDetails?.report?.readiness}`
    },
    {
        "name": "Transparency",
        "value": `${WrmOperationManagementDetails?.report?.transparency}`
    },
    {
        "name": "Contact Details Phone",
        "value": `${WrmOperationManagementDetails?.report?.contactDetailsPhone}`
    },
    {
        "name": "Contact Details Email",
        "value": `${WrmOperationManagementDetails?.report?.contactDetailsEmail}`
    },
    {
        "name": "Purchase Or Registration",
        "value": `${WrmOperationManagementDetails?.report?.purchaseOrRegistration}`
    },
    {
        "name": "Merchant Intelligence",
        "value": `${WrmOperationManagementDetails?.report?.merchantIntelligence}`
    },
    {
        "name": "Merchant Intelligence Rating",
        "value": `${WrmOperationManagementDetails?.report?.merchantIntelligenceRating}`
    },
    {
        "name": "Logo",
        "value": `${WrmOperationManagementDetails?.report?.logo}`
    },
    {
        "name": "Ip Address Of Server",
        "value": `${WrmOperationManagementDetails?.report?.ipAddressOfServer}`
    },
    {
        "name": "Suggestion",
        "value": `${WrmOperationManagementDetails?.report?.suggestion}`
    },
    {
        "name": "Content Repetition",
        "value": `${WrmOperationManagementDetails?.report?.contentRepetition}`
    },
    {
        "name": "Default Template like Loreal ipsum",
        "value": `${WrmOperationManagementDetails?.report?.webUrl}`
    },
    {
        "name": "Business Category",
        "value": `${WrmOperationManagementDetails?.report?.businessCategory}`
    },
    {
        "name": "Parked Domain",
        "value": `${WrmOperationManagementDetails?.report?.parkedDomain}`
    },
    {
        "name": "Spamming",
        "value": `${WrmOperationManagementDetails?.report?.spamming}`
    },
    {
        "name": "Http Status Code",
        "value": `${WrmOperationManagementDetails?.report?.httpStatusCode}`
    },
    {
        "name": "Page Size",
        "value": `${WrmOperationManagementDetails?.report?.pageSize}`
    },
    {
        "name": "Content Type",
        "value": `${WrmOperationManagementDetails?.report?.contentType}`
    },
    {
        "name": "Web Server",
        "value": `${WrmOperationManagementDetails?.report?.webServer}`
    },
    {
        "name": "Dns Sec Org",
        "value": `${WrmOperationManagementDetails?.report?.dnsSecOrg}`
    },
    {
        "name": "Domain Aliases",
        "value": `${WrmOperationManagementDetails?.report?.domainAliases}`
    },
    {
        "name": "Administrative Contact State",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactState}`
    },
    {
        "name": "Administrative Contact City",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactCity}`
    },
    {
        "name": "Administrative Contact Country",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactCountry}`
    },
    {
        "name": "Administrative Country Code",
        "value": `${WrmOperationManagementDetails?.report?.administrativeCountryCode}`
    },
    {
        "name": "Administrative Contact Email",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactEmail}`
    },
    {
        "name": "Administrative Contact Name",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactName}`
    },
    {
        "name": "Administrative Contact Telephone",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactTelephone}`
    },
    {
        "name": "Administrative Contact Organization",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactOrganization}`
    },
    {
        "name": "Administrative Contact Postal Code",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactPostalCode}`
    },
    {
        "name": "Administrative Contact Street1",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactStreet1}`
    },
    {
        "name": "Estimated Domain Age",
        "value": `${WrmOperationManagementDetails?.report?.estimatedDomainAge}`
    },
    {
        "name": "Domain Registered Date",
        "value": `${WrmOperationManagementDetails?.report?.domainRegisteredDate}`
    },
    {
        "name": "Domain Expiry Date",
        "value": `${WrmOperationManagementDetails?.report?.domainExpiryDate}`
    },
    {
        "name": "Domain Active For",
        "value": `${WrmOperationManagementDetails?.report?.domainActivefor}`
    },
    {
        "name": "Domain Expiring In",
        "value": `${WrmOperationManagementDetails?.report?.domainExpiringin}`
    },
    {
        "name": "Geo City",
        "value": `${WrmOperationManagementDetails?.report?.geoCity}`
    },
    {
        "name": "Geo Country",
        "value": `${WrmOperationManagementDetails?.report?.geoCountry}`
    },
    {
        "name": "Geo Country Code",
        "value": `${WrmOperationManagementDetails?.report?.geoCountryCode}`
    },
    {
        "name": "Geo Latitude",
        "value": `${WrmOperationManagementDetails?.report?.geoLatitude}`
    },
    {
        "name": "Geo Longitude",
        "value": `${WrmOperationManagementDetails?.report?.geoLongitude}`
    },
    {
        "name": "Geo Postal Code",
        "value": `${WrmOperationManagementDetails?.report?.geoPostalCode}`
    },
    {
        "name": "Geo State",
        "value": `${WrmOperationManagementDetails?.report?.geoState}`
    },
    {
        "name": "Geo State Code",
        "value": `${WrmOperationManagementDetails?.report?.geoStateCode}`
    },
    {
        "name": "Geo Street Address",
        "value": `${WrmOperationManagementDetails?.report?.geoStreetAddress}`
    },
    {
        "name": "Geo Street Name",
        "value": `${WrmOperationManagementDetails?.report?.geoStreetName}`
    },
    {
        "name": "Geo Street Number",
        "value": `${WrmOperationManagementDetails?.report?.geoStreetNumber}`
    },
    {
        "name": "Page Activity Check Mining",
        "value": `${WrmOperationManagementDetails?.report?.pageActivityCheckMining}`
    },
    {
        "name": "Page Activity Check Untrusted Downloads",
        "value": `${WrmOperationManagementDetails?.report?.pageActivityCheckUntrustedDownloads}`
    },
    {
        "name": "Page Availability Check Url Status",
        "value": `${WrmOperationManagementDetails?.report?.pageAvailabilityCheckURLStatus}`
    },
    {
        "name": "Page Health Check Content Accessibilty",
        "value": `${WrmOperationManagementDetails?.report?.pageHealthCheckContentAccessibilty}`
    },
    {
        "name": "Page Health Check Page Loading Time",
        "value": `${WrmOperationManagementDetails?.report?.pageHealthCheckPageLoadingTime}`
    },
    {
        "name": "Page Links Connectivity Check Success Rate",
        "value": `${WrmOperationManagementDetails?.report?.pageLinksConnectivityCheckSuccessRate}`
    },
    {
        "name": "Page Redirection Check Domain Redirection",
        "value": `${WrmOperationManagementDetails?.report?.pageRedirectionCheckDomainRedirection}`
    },
    {
        "name": "Malware Monitoring",
        "value": `${WrmOperationManagementDetails?.report?.malwareMonitoring}`
    },
    {
        "name": "Policy Compliance Status Contactus Page",
        "value": `${WrmOperationManagementDetails?.report?.policyComplianceStatusContactUsPage}`
    },
    {
        "name": "Policy Contains Valid Phone",
        "value": `${WrmOperationManagementDetails?.report?.policyContainsValidPhone}`
    },
    {
        "name": "Policy Contains Valid Email",
        "value": `${WrmOperationManagementDetails?.report?.policyContainsValidEmail}`
    },
    {
        "name": "Contact Form Status",
        "value": `${WrmOperationManagementDetails?.report?.contactsFormStatus}`
    },
    {
        "name": "Privacy Policy Status",
        "value": `${WrmOperationManagementDetails?.report?.privacyPolicyStatus}`
    },
    {
        "name": "Return Policy Status",
        "value": `${WrmOperationManagementDetails?.report?.returnPolicyStatus}`
    },
    {
        "name": "Shipping Policy Status",
        "value": `${WrmOperationManagementDetails?.report?.shippingPolicyStatus}`
    },
    {
        "name": "Terms And Condition Status",
        "value": `${WrmOperationManagementDetails?.report?.termsAndConditionStatus}`
    },
    {
        "name": "Website To Business Name Mcc Code Match",
        "value": `${WrmOperationManagementDetails?.report?.websiteToBusinessNameMccCodeMatch}`
    },
    {
        "name": "Website To Business Name Match Rating",
        "value": `${WrmOperationManagementDetails?.report?.websiteToBusinessNameMatchRating}`
    },
    {
        "name": "Content Keyword Risk",
        "value": `${WrmOperationManagementDetails?.report?.contentKeywordRisk}`
    },
    {
        "name": "Ssl Vulnerability Info",
        "value": `${WrmOperationManagementDetails?.report?.sslVulnerabilityInfo}`
    },
    {
        "name": "Ssl Check Info",
        "value": `${WrmOperationManagementDetails?.report?.sslCheckInfo}`
    },
    {
        "name": "User Interaction Checks Bank Detail Submission",
        "value": `${WrmOperationManagementDetails?.report?.userInteractionChecksBankDetailSubmission}`
    },
    {
        "name": "User Interaction Checks Id Proof Submission",
        "value": `${WrmOperationManagementDetails?.report?.userInteractionChecksIdProofSubmission}`
    },
    {
        "name": "User Interaction Checks Seller Redirection",
        "value": `${WrmOperationManagementDetails?.report?.userInteractionChecksSellerRedirection}`
    },
    {
        "name": "Predict Medicine",
        "value": `${WrmOperationManagementDetails?.report?.predictMedicine}`
    },
    {
        "name": "Shopify",
        "value": `${WrmOperationManagementDetails?.report?.shopify}`
    },
    {
        "name": "Woo Commerce",
        "value": `${WrmOperationManagementDetails?.report?.wooCommerce}`
    },
    {
        "name": "Safety",
        "value": `${WrmOperationManagementDetails?.report?.safety}`
    },
    {
        "name": "Suspicious",
        "value": `${WrmOperationManagementDetails?.report?.suspicious}`
    },
    {
        "name": "Phishing",
        "value": `${WrmOperationManagementDetails?.report?.phishing}`
    },
    {
        "name": "Domain Age",
        "value": `${WrmOperationManagementDetails?.report?.domainAge}`
    },
    {
        "name": "Domain Rank",
        "value": `${WrmOperationManagementDetails?.report?.domainRank}`
    },
    {
        "name": "Business Address",
        "value": `${WrmOperationManagementDetails?.report?.businessAddress}`
    },
    {
        "name": "Website Tags",
        "value": `${WrmOperationManagementDetails?.report?.websiteTags}`
    },
    {
        "name": "Facebook Url",
        "value": `${WrmOperationManagementDetails?.report?.facebookURL}`
    },
    {
        "name": "Facebook Likes",
        "value": `${WrmOperationManagementDetails?.report?.facebookLikes}`
    },
    {
        "name": "Twitter Url",
        "value": `${WrmOperationManagementDetails?.report?.twitterURL}`
    },
    {
        "name": "Twitter Followers",
        "value": `${WrmOperationManagementDetails?.report?.twitterFollowers}`
    },
    {
        "name": "Linkedin Url",
        "value": `${WrmOperationManagementDetails?.report?.linkedInURL}`
    },
    {
        "name": "Language Detected",
        "value": `${WrmOperationManagementDetails?.report?.languageDetected}`
    },
    {
        "name": "Contactus Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.contactUsPageScreenshot}`
    },
    {
        "name": "Privacy Policy Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.privacyPolicyPageScreenshot}`
    },
    {
        "name": "Shipping Policy Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.shippingPolicyPageScreenshot}`
    },
    {
        "name": "Return Policy Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.returnPolicyPageScreenshot}`
    },
    {
        "name": "Terms And Condition Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.termsAndConditionPageScreenshot}`
    },
    {
        "name": "Description",
        "value": `${WrmOperationManagementDetails?.report?.description}`
    },
    {
        "name": "Risk Classification",
        "value": `${WrmOperationManagementDetails?.report?.riskClassification}`
    },
    {
        "name": "Risk Level",
        "value": `${WrmOperationManagementDetails?.report?.riskLevel}`
    },
    {
        "name": "Risk Category Status",
        "value": `${WrmOperationManagementDetails?.report?.riskCategoryStatus}`
    },
    {
        "name": "Contactus Page Url",
        "value": `${WrmOperationManagementDetails?.report?.contactUsPageUrl}`
    },
    {
        "name": "Privacy Policy Page Url",
        "value": `${WrmOperationManagementDetails?.report?.privacyPolicyPageUrl}`
    },
    {
        "name": "Shipping Policy Page Url",
        "value": `${WrmOperationManagementDetails?.report?.shippingPolicyPageUrl}`
    },
    {
        "name": "Return Policy Page Url",
        "value": `${WrmOperationManagementDetails?.report?.returnPolicyPageUrl}`
    },
    {
        "name": "Terms And Condition Page Url",
        "value": `${WrmOperationManagementDetails?.report?.termsAndConditionPageUrl}`
    },
    {
        "name": "List Of Negative Keywords Found In Contact Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInContactPolicy}`
    },
    {
        "name": "List Of Negative Keywords Found In Privacy Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInPrivacyPolicy}`
    },
    {
        "name": "List Of Negative Keywords Found In Return Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInReturnPolicy}`
    },
    {
        "name": "List Of Negative Keywords Found In Shipping Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInShippingPolicy}`
    },
    {
        "name": "List Of Negative Keywords Found In Terms Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInTermsPolicy}`
    },
    {
        "name": "Merchant Address Being Mapped With Existing Urls",
        "value": `${WrmOperationManagementDetails?.report?.marchentAddressScrappedFromWebsite}`
    },
    {
        "name": "Emails Mapped With Emails From Existing Urls",
        "value": `${WrmOperationManagementDetails?.report?.emailsMappedWithEmailsFromExistingUrls}`
    },
    {
        "name": "Match Found With Existing Emails In System",
        "value": `${WrmOperationManagementDetails?.report?.matchFoundWithExistingEmailsInSystem}`
    },
    {
        "name": "Match Found With Phone Numbers In System",
        "value": `${WrmOperationManagementDetails?.report?.matchFoundWithPhoneNumbersInSystem}`
    },
    {
        "name": "Phone Numbers Mapped With Phone Numbers From Existing Urls",
        "value": `${WrmOperationManagementDetails?.report?.phoneNumbersMappedWithPhoneNumbersFromExistingUrls}`
    },
    {
        "name": "Whitelisted Keywords Found In Privacy Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInPrivacyPolicy}`
    },
    {
        "name": "Whitelisted Keywords Found In Return Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInReturnPolicy}`
    },
    {
        "name": "Whitelisted Keywords Found In Shipping Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInShippingPolicy}`
    },
    {
        "name": "Whitelisted Keywords Found In Terms And Conditions Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInTermsAndConditionsPolicy}`
    },
    {
        "name": "Currencies Found On Website",
        "value": `${WrmOperationManagementDetails?.report?.currenciesFoundOnWebsite}`
    },
    {
        "name": "Currencies Found Or Not",
        "value": `${WrmOperationManagementDetails?.report?.curreciesFoundOrNot}`
    },
    {
        "name": "Max Price Listed",
        "value": `${WrmOperationManagementDetails?.report?.maxPriceListedInHomePage}`
    },
    {
        "name": "Min Price Listed",
        "value": `${WrmOperationManagementDetails?.report?.minPriceListedInHomePage}`
    },
    {
        "name": "Average Product Price",
        "value": `${WrmOperationManagementDetails?.report?.averageProductPrice}`
    },
    {
        "name": "Product Price Page Links",
        "value": `${WrmOperationManagementDetails?.report?.productPricePageLinks}`
    },
    {
        "name": "Website Contains Unreasonable Price",
        "value": `${WrmOperationManagementDetails?.report?.websiteContainsUnreasonablePric}`
    },
    {
        "name": "Website Is Accessible Without Login Prompt",
        "value": `${WrmOperationManagementDetails?.report?.websiteIsAccessibleWithoutLoginPrompt}`
    },
    {
        "name": "Unreasonable Offers",
        "value": `${WrmOperationManagementDetails?.report?.unreasonableOffers}`
    },
    {
        "name": "Merchant Address Already Present",
        "value": `${WrmOperationManagementDetails?.report?.merchantAddressAlreadyPresent}`
    },
    {
        "name": "Website Redirection",
        "value": `${WrmOperationManagementDetails?.report?.websiteRedirection}`
    },
    {
        "name": "Website Redirection Url",
        "value": `${WrmOperationManagementDetails?.report?.websiteRedirectionURL}`
    },
    {
        "name": "Blacklist Advance Keyword",
        "value": `${WrmOperationManagementDetails?.report?.blackListAdvanceKeyword}`
    },
    {
        "name": "Website Reachability Description",
        "value": `${WrmOperationManagementDetails?.report?.websiteReachabilityDescription}`
    },
    {
        "name": "Domain Count",
        "value": `${WrmOperationManagementDetails?.report?.domainCount}`
    },
    {
        "name": "Reverse Ip Dns Analysis",
        "value": `${WrmOperationManagementDetails?.report?.reverseIpDnsAnalysis}`
    },
    {
        "name": "Orginal Brand Name",
        "value": `${WrmOperationManagementDetails?.report?.orginalBrandName}`
    },
    {
        "name": "Email Reachable",
        "value": `${WrmOperationManagementDetails?.report?.emailReachable}`
    },
    {
        "name": "Email Disposable",
        "value": `${WrmOperationManagementDetails?.report?.emailDisposable}`
    },
    {
        "name": "Mail Accepts",
        "value": `${WrmOperationManagementDetails?.report?.mailAccepts}`
    },
    {
        "name": "Email Records",
        "value": `${WrmOperationManagementDetails?.report?.emailRecords}`
    },
    {
        "name": "Email Smtp Connect",
        "value": `${WrmOperationManagementDetails?.report?.emailSmtpConnect}`
    },
    {
        "name": "Email Catch All",
        "value": `${WrmOperationManagementDetails?.report?.emailCatchAll}`
    },
    {
        "name": "Email Deliverable",
        "value": `${WrmOperationManagementDetails?.report?.emailDeliverable}`
    },
    {
        "name": "Email Disabled",
        "value": `${WrmOperationManagementDetails?.report?.emailDisabled}`
    },
    {
        "name": "Email Domain",
        "value": `${WrmOperationManagementDetails?.report?.emailDomain}`
    },
    {
        "name": "Email Valid Syntax",
        "value": `${WrmOperationManagementDetails?.report?.emailValidSyntax}`
    },
    {
        "name": "Address Scrapped From Website",
        "value": `${WrmOperationManagementDetails?.report?.addressScrappedFromWebsite}`
    },
    {
        "name": "Address From Google Map",
        "value": `${WrmOperationManagementDetails?.report?.addressFromGoogleMap}`
    },
    {
        "name": "Matched Address From Google Map",
        "value": `${WrmOperationManagementDetails?.report?.matchedAddressFromGoogleMap}`
    },
    {
        "name": "Blacklisted Address Pincode",
        "value": `${WrmOperationManagementDetails?.report?.blacklistedAddressPinCode}`
    },
    {
        "name": "Preshared Address",
        "value": `${WrmOperationManagementDetails?.report?.presharedAddress}`
    },
    {
        "name": "Banned Category",
        "value": `${WrmOperationManagementDetails?.report?.bannedCategory}`
    },
    {
        "name": "Banned Category State",
        "value": `${WrmOperationManagementDetails?.report?.bannedCategoryState}`
    },
    {
        "name": "Banned Category Status",
        "value": `${WrmOperationManagementDetails?.report?.bannedCategoryStatus}`
    },
    {
        "name": "Social Media Links",
        "value": `${WrmOperationManagementDetails?.report?.socialMediaLinks}`
    },
    {
        "name": "Blacklisted Merchant Address",
        "value": `${WrmOperationManagementDetails?.report?.blackListedMerchantAddress}`
    },
    {
        "name": "Blacklisted Buisness Details",
        "value": `${WrmOperationManagementDetails?.report?.blackListedBuisnessDetails}`
    },
    {
        "name": "Possible Categories",
        "value": `${WrmOperationManagementDetails?.report?.possibleCategories}`
    },
    {
        "name": "Is Refurbished",
        "value": `${WrmOperationManagementDetails?.report?.isRefurbished}`
    },
    {
        "name": "Risk Score",
        "value": `${WrmOperationManagementDetails?.report?.riskScore}`
    },
    {
        "name": "Category Risk Weightage",
        "value": `${WrmOperationManagementDetails?.report?.categoryRiskWeightage}`
    },
    {
        "name": "Business Risk",
        "value": `${WrmOperationManagementDetails?.report?.businessRisk}`
    },
    {
        "name": "Web Content Risk",
        "value": `${WrmOperationManagementDetails?.report?.webContentRisk}`
    },
    {
        "name": "Transparency Risk",
        "value": `${WrmOperationManagementDetails?.report?.transparencyRisk}`
    },
    {
        "name": "Website Health",
        "value": `${WrmOperationManagementDetails?.report?.websiteHealth}`
    },
    {
        "name": "Website Content Checkup Domain Health",
        "value": `${WrmOperationManagementDetails?.report?.websiteContentCheckupDomainHealth}`
    },
    {
        "name": "Online Website Reputation",
        "value": `${WrmOperationManagementDetails?.report?.onlineWebsiteReputation}`
    },
    {
        "name": "Web Security",
        "value": `${WrmOperationManagementDetails?.report?.webSecurity}`
    },
    {
        "name": "Website Is Not Working",
        "value": `${WrmOperationManagementDetails?.report?.websiteIsNotWorking}`
    },
    {
        "name": "Pricing Is Not Updated",
        "value": `${WrmOperationManagementDetails?.report?.pricingIsNotUpdated}`
    },
    {
        "name": "Login Credentials Are Required",
        "value": `${WrmOperationManagementDetails?.report?.loginCredentialsAreRequired}`
    },
    {
        "name": "Pricing Is In Dollars",
        "value": `${WrmOperationManagementDetails?.report?.pricingIsInDollars}`
    },
    {
        "name": "High Discounts",
        "value": `${WrmOperationManagementDetails?.report?.highDiscounts}`
    },
    {
        "name": "Missing Policy Links",
        "value": `${WrmOperationManagementDetails?.report?.missingPolicyLinks}`
    },
    {
        "name": "Website Redirection Pma",
        "value": `${WrmOperationManagementDetails?.report?.websiteRedirectionPMA}`
    },
    {
        "name": "Page Navigation Issue",
        "value": `${WrmOperationManagementDetails?.report?.pageNavigationIssue}`
    },
    {
        "name": "Pma Flag",
        "value": `${WrmOperationManagementDetails?.report?.pmaFlag}`
    },
    {
        "name": "Pma List",
        "value": `${WrmOperationManagementDetails?.report?.pmaList}`
    },
    {
        "name": "All Violations",
        "value": `${WrmOperationManagementDetails?.report?.allViolations}`
    },
    {
        "name": "Google Analytics Id Relation",
        "value": `${WrmOperationManagementDetails?.report?.googleAnalyticsIdRelation}`
    },
    {
        "name": "Paid Up Capital",
        "value": `${WrmOperationManagementDetails?.report?.paidUpCapital}`
    },
    {
        "name": "Product Pricing Home Page URL",
        "value": `${WrmOperationManagementDetails?.report?.productPricingHomePageUrl}`
    },
    {
        "name": "Product Pricing Product Page URL",
        "value": `${WrmOperationManagementDetails?.report?.productPricingProductPageUrl}`
    },
    {
        "name": "Email in Blacklisted",
        "value": `${WrmOperationManagementDetails?.report?.email_in_blacklisted}`
    },
    {
        "name": "Phone in Blacklisted",
        "value": `${WrmOperationManagementDetails?.report?.phone_in_blacklisted}`
    },
    {
        "name": "UPI in Blacklisted",
        "value": `${WrmOperationManagementDetails?.report?.upi_in_blacklisted}`
    },
    {
        "name": "Rejection Reason",
        "value": `${WrmOperationManagementDetails?.report?.rejection_reason}`
    },
    {
        "name": "Is Account Linked",
        "value": `${WrmOperationManagementDetails?.report?.is_account_linked}`
    },
    {
        "name": "Linked Accounts",
        "value": `${WrmOperationManagementDetails?.report?.linked_accounts}`
    },
    {
        "name": "No Accounts Linked",
        "value": `${WrmOperationManagementDetails?.report?.no_accounts_linked}`
    },
    {
        "name": "Linked Cases",
        "value": `${WrmOperationManagementDetails?.report?.linked_cases}`
    },
    {
        "name": "Linked to Rejected Cases",
        "value": `${WrmOperationManagementDetails?.report?.linked_to_rejected_cases}`
    },
    {
        "name": "No of Accounts Email",
        "value": `${WrmOperationManagementDetails?.report?.no_of_accounts_email}`
    },
    {
        "name": "No of Accounts Phone",
        "value": `${WrmOperationManagementDetails?.report?.no_of_accounts_phone}`
    },
    {
        "name": "No of Accounts UPI",
        "value": `${WrmOperationManagementDetails?.report?.no_of_accounts_upi}`
    },
    {
        "name": "Blacklist Status",
        "value": `${WrmOperationManagementDetails?.report?.blacklistStatus}`
    },
    {
        "name": "Email - Input",
        "value": `${WrmOperationManagementDetails?.report?.emailInput}`
    },
    {
        "name": "Phone - Input",
        "value": `${WrmOperationManagementDetails?.report?.phoneInput}`
    },
    {
        "name": "UPI - Input",
        "value": `${WrmOperationManagementDetails?.report?.upiInput}`
    },
    {
        "name": "Billing Descriptor",
        "value": `${WrmOperationManagementDetails?.report?.billingDescriptor}`
    },
    {
        "name": "Merchant DBA",
        "value": `${WrmOperationManagementDetails?.report?.merchantDBA}`
    },
    {
        "name": "ID 1",
        "value": `${WrmOperationManagementDetails?.report?.id1}`
    },
    {
        "name": "Master Card ICA",
        "value": `${WrmOperationManagementDetails?.report?.masterCardICA}`
    },
    {
        "name": "Visa BIN",
        "value": `${WrmOperationManagementDetails?.report?.visaBIN}`
    },
    {
        "name": "City",
        "value": `${WrmOperationManagementDetails?.report?.city}`
    },
    {
        "name": "State",
        "value": `${WrmOperationManagementDetails?.report?.state}`
    },
    {
        "name": "Country",
        "value": `${WrmOperationManagementDetails?.report?.country}`
    },
    {
        "name": "Zip",
        "value": `${WrmOperationManagementDetails?.report?.zip}`
    },
    {
        "name": "Primary Merchant Contact Name",
        "value": `${WrmOperationManagementDetails?.report?.primaryMerchantContactName}`
    },
    {
        "name": "Products Services Description",
        "value": `${WrmOperationManagementDetails?.report?.productsServicesDescription}`
    },
    {
        "name": "Mirror Website - Home Page",
        "value": `${WrmOperationManagementDetails?.report?.mirrorHomePage}`
    },
    {
        "name": "Mirror Website - About us",
        "value": `${WrmOperationManagementDetails?.report?.mirrorAboutUsPage}`
    },
    {
        "name": "Facebook - Followers",
        "value": `${WrmOperationManagementDetails?.report?.facebookFollowers}`
    },
    {
        "name": "Facebook - Likes",
        "value": `${WrmOperationManagementDetails?.report?.facebookTotalLikes}`
    },
    {
        "name": "Facebook - Joined On",
        "value": `${WrmOperationManagementDetails?.report?.facebookJoinedOn}`
    },
    {
        "name": "Instagram - Followers",
        "value": `${WrmOperationManagementDetails?.report?.instagramFollowers}`
    },
    {
        "name": "Instagram - Post",
        "value": `${WrmOperationManagementDetails?.report?.instagramPost}`
    },
    {
        "name": "Youtube - Subscribers",
        "value": `${WrmOperationManagementDetails?.report?.youtubeSubscribers}`
    },
    {
        "name": "Youtube - Videos",
        "value": `${WrmOperationManagementDetails?.report?.youtubeVideos}`
    },
    {
        "name": "Youtube - Joined On",
        "value": `${WrmOperationManagementDetails?.report?.youtubeJoinedOn}`
    },
    {
        "name": "Linked In - Followers",
        "value": `${WrmOperationManagementDetails?.report?.linkedFollowers}`
    },
    {
        "name": "Twitter - Followers",
        "value": `${WrmOperationManagementDetails?.report?.twitterTotalFollowers}`
    },
    {
        "name": "Twitter -Joined On",
        "value": `${WrmOperationManagementDetails?.report?.twitterJoinedOn}`
    },
    {
        "name": "Pinterest - Followers",
        "value": `${WrmOperationManagementDetails?.report?.pinterestFollowers}`
    }
]

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-6'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-body pt-0">
            {WrmOperationManagementDetails?.workStatus === "IN PROGRESS" &&WrmOperationManagementDetails.taskQAAccepted&& <EditReport report = {WrmOperationManagementDetails.report} id={WrmOperationManagementDetails._id}/>}
             {data?.map(item=> <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row' key={item.name}>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     {item.name}
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {item.value}                    
                    </span>
                  </div>
                </div>
              </div>).slice(0,125)}
            </div>
          </div>
        </div>

        <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-body pt-0'>
                {data?.map(item=> <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     {item.name}
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                   <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {item.value}                    
                    </span>
                  </div>
                </div>
              </div>).slice(125)}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default WRMReport