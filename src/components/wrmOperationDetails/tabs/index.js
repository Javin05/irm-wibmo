import React, { useEffect, useState } from "react"
import "react-circular-progressbar/dist/styles.css"
import {  useLocation } from "react-router-dom"
import { connect } from 'react-redux'
import WRMReport from './WRMReport'
import Summarry from './Summary'
import ScreenShot from './ScreebShot'
import _ from 'lodash'
import {  SET_FILTER } from '../../../utils/constants'
import { getLocalStorage, setLocalStorage } from '../../../utils/helper'
import { BWListActions, BlockListEmailActions, WrmStatusActions, updateStatusChangeActions } from "../../../store/actions"
import WRMAPITracker from "./WRMAPITracker"
import History from "./History"

function Websites(props) {
  const {
    loading=false,
    WrmOperationManagementDetails,
    BlackWhiteDataList,
    getBlackWhiteLDispatch,
    getWrmStatusDispatch,
    getBlockListTypeDispatch,
    WrmOperationManagementBackendAPIStatus,
  } = props
  let activeOPSTAB = getLocalStorage("OPS_ACTIVE_TAB")
  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [activestep, setActiveStep] = useState(activeOPSTAB?parseInt(activeOPSTAB):0)
  const [completed] = useState({})
  const steps = getSteps()

  function getSteps() {
    return [
      {
        label: "Summary",
        className: "btn web-label-six",
        stepCount: 0,
      },
      {
        label: "WRM Report",
        className: "btn web-label-six",
        stepCount: 1,
      },
      {
        label: "Screenshots",
        stepCount: 2,
        className: "btn web-label-six",
      },
      {
        label: "WRM API tracker",
        stepCount: 3,
        className: "btn web-label-six",
      },
      {
        label: "History",
        stepCount: 4,
        className: "btn web-label-six",
      },
    ]
  }

 
  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <Summarry
          WrmOperationManagementDetails={WrmOperationManagementDetails}
          blacklistDropdown={blacklistDropdown}
          />
        )
      case 1:
        return (
          <WRMReport
          WrmOperationManagementDetails={WrmOperationManagementDetails}
          />
        )
      case 2:
        return (
          <ScreenShot
          WrmOperationManagementDetails={WrmOperationManagementDetails}
          />
        )
      case 3:
        return (
          <WRMAPITracker
          WrmOperationManagementDetails={WrmOperationManagementDetails}
          WrmOperationManagementBackendAPIStatus={WrmOperationManagementBackendAPIStatus}
           urlId={id}
          />
        )
      case 4:
        return (
          <History
             WrmOperationManagementDetails={WrmOperationManagementDetails}
            id={id}
          />
        )
      default:
        return "unknown step"
    }
  }

  const handleStep = (step) => () => {
    setActiveStep(step)
    setLocalStorage("OPS_ACTIVE_TAB",step)
  }


  const [blacklistDropdown, setBlacklistDropdown] = useState(null)
  const [searchParams, setSearchParams] = useState({})
  const [limit, setLimit] = useState(25)

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
  const pickByParams = _.pickBy(params)
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const currentRoute = url && url[1]
    if (currentRoute === 'risk-management-search') {
    } else {
      getWrmStatusDispatch(pickByParams)
      const bloackListParams = {
        skipPagination: 'true',
        fieldType: 'Website_Category',
        queueId: "624fc67fae69dc1e03f47ebd"
      }
      getBlockListTypeDispatch(bloackListParams)
    }
    getBlackWhiteLDispatch()
  }, [])
 useEffect(() => {
    if (BlackWhiteDataList) {
      const blackWhiteData = BlackWhiteDataList && _.isArray(BlackWhiteDataList.data) ? BlackWhiteDataList.data : []
      let blacktemp = []
      Object.keys(blackWhiteData).forEach(function (key) {
        blacktemp.push({
          "value": blackWhiteData[key].fieldValue,
          "label": blackWhiteData[key].fieldValue,
        })
      })

      blacktemp.push({
        "value": "Others",
        "label": "Others",
      })
      setBlacklistDropdown(blacktemp)
    }
  }, [BlackWhiteDataList])
  return (
    <>
     <div className="mt-0">
              <>
                <div className="d-flex">
                  {steps.map((step, index) => (
                    <div
                      key={"A_" + index}
                      completed={completed[index]}
                      className={`my-10 mx-1 text mb-4 rounded-1 seven-label fs-8 fw-bolder text-gray-800 ${loading ? "event-disable" : ""
                        } ${step.stepCount === activestep
                          ? "btn web-label-sixActive fs-8 fw-bolder"
                          : `${step.className}`
                        }`}
                      onClick={handleStep(index)}
                    >
                      {step.label}
                    </div>
                  ))}
                </div>
                {activestep === steps.length ? null : (
                  <>
                    <div>{getStepContent(activestep)}</div>
                  </>
                )}
              </>
            </div>
    </>
  )
}

const mapStateToProps = (state) => {
  return ({
      loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
      WrmOperationManagementDetails: state && state.WrmOperationManagementDetailStore && state.WrmOperationManagementDetailStore.WrmOperationManagementDetail,
      BlackWhiteDataList: state.BWlistStore && state.BWlistStore.BWLists && state.BWlistStore.BWLists.data ? state.BWlistStore.BWLists.data : '',
      getWebsiteIdDataLists: state.getWebsiteIdStore && state.getWebsiteIdStore.getWebsiteIdData ? state.getWebsiteIdStore.getWebsiteIdData && state.getWebsiteIdStore.getWebsiteIdData.data : '',
      WrmOperationManagementBackendAPIStatus:state&&state.WrmOperationManagementBackendAPIStatusStore && state.WrmOperationManagementBackendAPIStatusStore.WrmOperationManagementBackendAPIStatus,
  })
}

const mapDispatchToProps = (dispatch) => ({
    getBlackWhiteLDispatch: (params) => dispatch(BWListActions.getBWList(params)),
    getWrmStatusDispatch: (params) => dispatch(WrmStatusActions.getWrmStatus(params)),
    getBlockListTypeDispatch: (params) => dispatch(BlockListEmailActions.getBlockListType(params)),
    getBlackWhiteLDispatch: (params) => dispatch(BWListActions.getBWList(params)),

})

export default connect(mapStateToProps, mapDispatchToProps)(Websites)
