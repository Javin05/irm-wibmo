import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _, { values } from 'lodash'
import {
  GetClientsActions,
  WrmOperationManagementActions,
  WrmOperationManagementDetailActions,
  WrmReportEditAction,
  getWrmOperationDetails
} from '../../store/actions'
// import './styles.css';
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setLocalStorage, getLocalStorage } from '../../utils/helper';
import { DateSelector } from '../../theme/layout/components/DateSelector'
import { DATE, SET_FILTER } from '../../utils/constants'
import moment from "moment"

function WrmReportEdit(props) {
  const {

    getWrmOperationManagementlistDispatch,
    Value,
    report,
    editWrmReport,
    id
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [buil, setBUild] = useState(false);
  const [dataValue, setDataValue] = useState({});
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const headClientId = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const [manualFormData, setManualFormData] = useState({
    websiteWorking: report?report.websiteWorking:'',
    legalNameScrapped: report?report.legalNameScrapped:'',
    legalNameMatch: report?report.legalNameMatch:'',
    domainRegistered: report?report.domainRegistered:'',
    domainRegistrationCompany:report?report.domainRegistrationCompany:'',
    domainRegistrationDate:report?report.domainRegistrationDate==="No Data"?"":new Date(report.domainRegistrationDate):'',
    domainRegistrationExpiryDate:report?report.domainRegistrationExpiryDate:'',
    sslCertificateCheck:report?report.sslCertificateCheck:'',
    contactDetailsPhone:report?report.contactDetailsPhone:'',
    contactDetailsEmail:report?report.contactDetailsEmail:'',
    merchantIntelligence:report?report.merchantIntelligence:'',
    policyComplianceStatusContactUsPage:report?report.policyComplianceStatusContactUsPage:'',
    policyContainsValidPhone:report?report.policyContainsValidPhone:'',
    policyContainsValidEmail:report?report.policyContainsValidEmail:'',
    contactsFormStatus:report?report.contactsFormStatus:'',
    privacyPolicyStatus:report?report.privacyPolicyStatus:'',
    returnPolicyStatus:report?report.returnPolicyStatus:'',
    shippingPolicyStatus:report?report.shippingPolicyStatus:'',
    termsAndConditionStatus:report?report.termsAndConditionStatus:'',
    contactUsPageUrl:report?report.contactUsPageUrl:'',
    privacyPolicyPageUrl:report?report.privacyPolicyPageUrl:'',
    shippingPolicyPageUrl:report?report.shippingPolicyPageUrl:'',
    returnPolicyPageUrl:report?report.returnPolicyPageUrl:'',
    termsAndConditionPageUrl:report?report.termsAndConditionPageUrl:'',
    currenciesFoundOnWebsite:report?report.currenciesFoundOnWebsite:'',
    maxPriceListedInHomePage:report?report.maxPriceListedInHomePage:'',
    minPriceListedInHomePage:report?report.minPriceListedInHomePage:'',
    productsPriceListedInWebsite:report?report.productsPriceListedInWebsite:'',
    websiteContainsUnreasonablePrice:report?report.websiteContainsUnreasonablePrice:'',
    websiteIsAccessibleWithoutLoginPrompt:report?report.websiteIsAccessibleWithoutLoginPrompt:'',
    websiteRedirection:report?report.websiteRedirection:'',
    websiteRedirectionURL:report?report.websiteRedirectionURL:'',
    domainRegistrarRisk:report?report.domainRegistrarRisk:'',
    merchantIntelligenceRating:report?report.merchantIntelligenceRating:'',
    domainActivefor:report?report.domainActivefor:'',
    domainExpiringin:report?report.domainExpiringin:'',
    websiteIsNotWorking:report?report.websiteIsNotWorking:'',
    pricingIsNotUpdated:report?report.pricingIsNotUpdated:'',
    loginCredentialsAreRequired:report?report.loginCredentialsAreRequired:'',
    pricingIsInDollars:report?report.pricingIsInDollars:'',
    highDiscounts:report?report.highDiscounts:'',
    missingPolicyLinks:report?report.highDiscounts:'',
    websiteRedirectionPMA:report?report.websiteRedirectionPMA:'',
    pageNavigationIssue:report?report.pageNavigationIssue:'',
    pmaFlag:report?report.pmaFlag:'',

  })
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  
  const handleSearch = () => {
    // const errorMsg = searchValidation(manualFormData, setError)
    // if (_.isEmpty(errorMsg)) {

    const UpDateFrom = moment(manualFormData.createdAtFrom).format("YYYY-MM-DD")
    const UpDateTo = moment(manualFormData.createdAtTo).format("YYYY-MM-DD")
    if (moment(UpDateFrom).isAfter(UpDateTo)) {
      setError({
        createdDate: "From Date Should Be Less Than To Date",
      });
      return setShow(true);
    }
    const params = {
      tag: manualFormData.tag,
      ticketId: manualFormData.ticketId,
      createdAtFrom: UpDateFrom === 'Invalid date' ? '' : UpDateFrom,
      createdAtTo: UpDateTo === 'Invalid date' ? moment(new Date()).format("YYYY/MM/DD") : UpDateTo,
      ticketId: manualFormData.ticketId,
      clientId: manualFormData.clientId ? manualFormData.clientId : '',
      assignedTo: manualFormData.assignedTo ? manualFormData.assignedTo : '',
      operationStatus:manualFormData.operationStatus,
      workStatus:manualFormData.workStatus,
      caseId:manualFormData.caseId
    }
    setShow(false)
    getWrmOperationManagementlistDispatch(params)
    setDataValue(params)
    setLocalStorage('ExportHide', JSON.stringify(
      !_.isEmpty(params && params) ? params : ''
    ))
    setLocalStorage('TAG', JSON.stringify(
      params
    ))
    setLocalStorage('WEBSITSEARCH', JSON.stringify(
      params
    ))
    const data = {
      tag: manualFormData.tag,
    }

  }

  const clearPopup = () => {
    setShow(false)
  }

  const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))
  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
      website: tagSearch.website,
      tag: tagSearch.tag,
      riskStatus: tagSearch.riskStatus,
      reportStatus: tagSearch.reportStatus,

    }))
    // show && getClientsWrmDispatch()
  }, [show])
  useEffect(() => {
    if (Value === true) {
      setManualFormData(values => ({
        ...values,
        website: '',
        acquirer: '',
        tag: '',
        riskStatus: '',
        reportStatus: '',
        createdAtFrom: '',
        createdAtTo: ''
      }))
    } else {
      setBUild(true)
    }
  }, [Value])
  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }
   const handleChange = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }
  const handleEdit = ()=>{
      editWrmReport(id,manualFormData)
      setShow(false)    
  }
  return (
    <>
      <div className='ml-3'>
        <button
          type='button'
          className='btn btn-sm btn-primary font-5vw me-3 pull-right'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          {/* <KTSVG path='/media/icons/duotune/general/gen021.svg' /> */}
          {/* eslint-disable */}
          Edit report
        </button>
      </div>

      <Modal
        show={show}
        size="xl"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
           Edit WRM Report
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className='col-lg-12  ' >
          <div className='card-header bg-skyBlue py-10 mb-8'>
               <div className='card-body ml-2'>
                 <div className='form-group row mb-6'>
                    <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Working?:</label>
                          <div className='col-lg-11'>
                            <select
                              name='websiteWorking'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteWorking || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                        </div>
                        <div className='col-lg-6 mb-2 '>
                          <label className='fs-xs fw-bold mb-2 form-label'>Legal Name Scrapped:</label>
                          <div className='col-lg-11'>
                            <input
                            placeholder='Legal Name Scrapped?'
                           className='form-control'
                            onChange={(e) => handleChange(e)}
                            type='text'
                            name='legalNameScrapped'
                            autoComplete='off'
                            value={manualFormData.legalNameScrapped || ''}
                          />
                          </div>
                        </div>
                        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Legal Name Match:</label>
                          <div className='col-lg-11'>
                            <select
                              name='legalNameMatch'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.legalNameMatch || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='true'>true</option>
                              <option value='false'>false</option>
                            </select>
                          </div>
                        </div>
                           <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Domain Registered:</label>
                          <div className='col-lg-11'>
                            <select
                              name='domainRegistered'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.domainRegistered || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                        </div>
                           <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Domain Registration Company:</label>
                          <div className='col-lg-11'>
                            <input
                              name='domainRegistrationCompany'
                              type='text'
                              className='form-control'
                              placeholder='ARN'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.domainRegistrationCompany || ''}
                              autoComplete='off'
                            />
                          </div>
                        </div>
                           {/* <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Domain Registration Date:</label>
                          <div className='col-lg-11'>
                            <DateSelector
                            name="domainRegistrationDate"
                            placeholder="Domain Registration Date"
                            className="form-control"
                            selected={manualFormData.domainRegistrationDate || ""}
                            onChange={(date) => {
                              setManualFormData((values) => ({
                                ...values,
                                domainRegistrationDate: date,
                              }));
                            }}
                            dateFormat={DATE.DATE_FOR_PICKER}
                            maxDate={new Date()}
                            isClearable={true}
                      />
                          </div>
                        </div> */}
                          <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Ssl Certificate Check:</label>
                          <div className='col-lg-11'>
                            <select
                              name='sslCertificateCheck'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.sslCertificateCheck || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                        </div>
                          <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Contact Details Phone:</label>
                          <div className='col-lg-11'>
                            <input
                              name='contactDetailsPhone'
                              type='text'
                              className='form-control'
                              placeholder='Contact Details Phone'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.contactDetailsPhone || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Contact Details Email:</label>
                          <div className='col-lg-11'>
                             <input
                              name='contactDetailsEmail'
                              type='text'
                              className='form-control'
                              placeholder='Contact Details Email'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.contactDetailsEmail || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Merchant Intelligence:</label>
                          <div className='col-lg-11'>
                              <input
                              name='merchantIntelligence'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.merchantIntelligence || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Policy Compliance Status Contactus Page:</label>
                          <div className='col-lg-11'>
                            <select
                              name='policyComplianceStatusContactUsPage'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.policyComplianceStatusContactUsPage || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='TRUE'>TRUE</option>
                              <option value='FALSE'>FALSE</option>
                            </select>
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Policy Contains Valid Phone:</label>
                          <div className='col-lg-11'>
                           <select
                              name='policyContainsValidPhone'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.policyContainsValidPhone || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='TRUE'>TRUE</option>
                              <option value='FALSE'>FALSE</option>
                            </select>
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Policy Contains Valid Email:</label>
                          <div className='col-lg-11'>
                              <select
                              name='policyContainsValidEmail'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.policyContainsValidEmail || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='TRUE'>TRUE</option>
                              <option value='FALSE'>FALSE</option>
                            </select>
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Contacts Form status:</label>
                          <div className='col-lg-11'>
                          <select
                              name='contactsFormStatus'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.contactsFormStatus || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='accessible'>accessible</option>
                              <option value='inaccessible'>inaccessible</option>
                            </select>
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Privacy Policy Status:</label>
                          <div className='col-lg-11'>
                             <select
                              name='privacyPolicyStatus'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.privacyPolicyStatus || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='accessible'>accessible</option>
                              <option value='inaccessible'>inaccessible</option>
                            </select>
                          </div>
                        </div>  <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Return Policy Status:</label>
                          <div className='col-lg-11'>
                            <select
                              name='returnPolicyStatus'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.returnPolicyStatus || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='accessible'>accessible</option>
                              <option value='inaccessible'>inaccessible</option>
                            </select>
                          </div>
                        </div>  
                        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Shipping Policy Status:</label>
                          <div className='col-lg-11'>
                           <select
                              name='shippingPolicyStatus'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.shippingPolicyStatus || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='accessible'>accessible</option>
                              <option value='inaccessible'>inaccessible</option>
                            </select>
                          </div>
                        </div>  
                        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Terms And Condition Status:</label>
                          <div className='col-lg-11'>
                            <select
                              name='termsAndConditionStatus'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.termsAndConditionStatus || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='accessible'>accessible</option>
                              <option value='inaccessible'>inaccessible</option>
                            </select>
                          </div>
                        </div>
                            <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Contact Us Page Url:</label>
                          <div className='col-lg-11'>
                            <input
                              name='contactUsPageUrl'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.contactUsPageUrl || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                            <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Privacy Policy Page Url:</label>
                          <div className='col-lg-11'>
                            <input
                              name='privacyPolicyPageUrl'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.privacyPolicyPageUrl || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                            <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Shipping Policy Page Url:</label>
                          <div className='col-lg-11'>
                             <input
                              name='shippingPolicyPageUrl'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.shippingPolicyPageUrl || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                            <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Return Policy Page Url:</label>
                          <div className='col-lg-11'>
                           <input
                              name='returnPolicyPageUrl'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.returnPolicyPageUrl || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                            <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Terms And Condition Page Url:</label>
                          <div className='col-lg-11'>
                            <input
                              name='termsAndConditionPageUrl'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.termsAndConditionPageUrl || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Max Price Listed:</label>
                          <div className='col-lg-11'>
                            <input
                              name='maxPriceListedInHomePage'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.maxPriceListedInHomePage || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Min Price Listed:</label>
                          <div className='col-lg-11'>
                            <input
                              name='minPriceListedInHomePage'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.minPriceListedInHomePage || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Product Price Page Links:</label>
                          <div className='col-lg-11'>
                          <input
                              name='productsPriceListedInWebsite'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.productsPriceListedInWebsite || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Contains Unreasonable Price:</label>
                          <div className='col-lg-11'>
                          <select
                              name='websiteContainsUnreasonablePrice'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteContainsUnreasonablePrice || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='TRUE'>TRUE</option>
                              <option value='FALSE'>FALSE</option>
                            </select>
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Is Accessible Without Login Prompt:</label>
                          <div className='col-lg-11'>
                           <input
                              name='websiteIsAccessibleWithoutLoginPrompt'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteIsAccessibleWithoutLoginPrompt || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Redirection:</label>
                          <div className='col-lg-11'>
                         <select
                              name='websiteRedirection'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteRedirection || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                        </div>
                             <div className='col-lg-6 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Redirection Url:</label>
                          <div className='col-lg-11'>
                           <input
                              name='websiteRedirectionURL'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteRedirectionURL || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Domain Registrar Risk:</label>
                          <div className='col-lg-11'>
                            <select
                              name='domainRegistrarRisk'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.domainRegistrarRisk || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                        </div>
                        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Merchant Intelligence Rating:</label>
                          <div className='col-lg-11'>
                            <input
                              name='merchantIntelligenceRating'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.merchantIntelligenceRating || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Domain Active For:</label>
                          <div className='col-lg-11'>
                            <input
                              name='merchadomainActiveforntIntelligenceRating'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.domainActivefor || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Domain Expiring In:</label>
                          <div className='col-lg-11'>
                               <input
                              name='domainExpiringin'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.domainExpiringin || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                                <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Is Not Working:</label>
                          <div className='col-lg-11'>
                               <input
                              name='websiteIsNotWorking'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteIsNotWorking || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Pricing Is Not Updated:</label>
                          <div className='col-lg-11'>
                               <input
                              name='pricingIsNotUpdated'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.pricingIsNotUpdated || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Login Credentials Are Required:</label>
                          <div className='col-lg-11'>
                              <input
                              name='loginCredentialsAreRequired'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.loginCredentialsAreRequired || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Pricing Is In Dollars:</label>
                          <div className='col-lg-11'>
                              <input
                              name='pricingIsInDollars'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.pricingIsInDollars || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>High Discounts:</label>
                          <div className='col-lg-11'>
                              <input
                              name='highDiscounts'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.highDiscounts || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>        
                        <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Missing Policy Links:</label>
                          <div className='col-lg-11'>
                              <input
                              name='missingPolicyLinks'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.missingPolicyLinks || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Website Redirection Pma:</label>
                          <div className='col-lg-11'>
                             <input
                              name='websiteRedirectionPMA'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.websiteRedirectionPMA || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            />
                          </div>
                        </div>
                             <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Page Navigation Issue:</label>
                          <div className='col-lg-11'>
                             <input
                              name='pageNavigationIssue'
                              type='text'
                              className='form-control'
                              placeholder='Merchant Intelligence'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.pageNavigationIssue || ''}
                              autoComplete='off'
                              // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                              //
                              />
                          </div>
                        </div>
                     
                          <div className='col-lg-3 mb-3'>
                          <label className='fs-xs fw-bold mb-2 form-label'>Pma Flag:</label>
                          <div className='col-lg-11'>
                            <select
                              name='pmaFlag'
                              className='form-select'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleChange(e)}
                              value={manualFormData.pmaFlag || ''}
                            >
                              <option value=''>Select...</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                          <br/>
                          <button 
                            type='button'
                            className='btn btn-sm btn-primary font-5vw me-3'
                            onClick={()=>setShow(false)}
                          >Cancel</button>
                          <button 
                            type='button'
                            className='btn btn-sm btn-primary font-5vw me-3'
                          onClick={()=>handleEdit()}>Submit</button>
                        </div>
                      </div>
               </div>
          </div>
        </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
WrmOperatorsListStore,
riskManagementlistStore,
RiskLevelStore
  } = state
  return {
    loading: riskManagementlistStore &&riskManagementlistStore.loading,
    loadingRL: RiskLevelStore &&RiskLevelStore.loadingRL,
    wrmOperatorsList: WrmOperatorsListStore.WrmOperators?WrmOperatorsListStore.WrmOperators.data:[]

  }
}

const mapDispatchToProps = dispatch => ({
  getWrmOperationManagementlistDispatch: (params) => dispatch(WrmOperationManagementActions.getWrmOperationManagemnt(params)),
  editWrmReport:(id,params)=>dispatch(WrmReportEditAction.editWrmReportRequest(id,params)),
  getWrmOperationDetails: (id)=>dispatch(WrmOperationManagementDetailActions.getWrmOperationDetail(id)),

})
  
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrmReportEdit)