import React, { useEffect, useState, Fragment, useRef, useCallback } from 'react'
import { Link } from 'react-router-dom'
import _, { includes } from 'lodash'
import { TagSummaryAction, SummaryWebExportAction, clientCredFilterActions, UpdatetagstatusAction, TagSummaryIdAction, TagSummaryUpdateAction, clientIdLIstActions, PostSendEmailAction, OGMactions, SummaryPlaytoreExportAction, exportClientWebActions, exportClientPlayStoreActions, GetClientsActions } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage, removeLocalStorage } from '../../utils/helper'
import { warningAlert, confirmationAlert, confirmAlert } from "../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { RISKSTATUS } from '../../utils/constants'
import SearchList from './searchList'
import moment from "moment"
import {
  SWEET_ALERT_MSG,
  STATUS_RESPONSE,
  DATE,
  FILE_FORMAT_CB_DOCUMENT,
  DROPZONE_MESSAGES,
  BUTTON_COLOR,
  TAGSTATUSNAME,
  REGEX,
  RISKAPPROVED
} from '../../utils/constants'
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'
import { DateSelector } from '../../theme/layout/components/DateSelector'
import { setTagSummaryData } from './formData'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import axiosInstance from "../../services"
import serviceList from "../../services/serviceList"

function TagSummaryList(props) {
  const {
    className,
    TagSummaryDispatch,
    TagSummaryData,
    loading,
    getExportDispatch,
    exportLoading,
    exportLists,
    clearExportListDispatch,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    UpdatetagstatusDispatch,
    TagSummaryIdDispatch,
    TagSummaryUpdateDispatch,
    UpdatetagstatusData,
    CleartagstatusDispatch,
    TagSummaryIdData,
    ClearTagSummaryUpdateDispatch,
    TagSummaryupdateLoading,
    TagSummaryUpdateData,
    TagSummaryIdDataloading,
    ClearIdDispatch,
    clinetIdLists,
    clientIdDispatch,
    PostSendEmailDispatch,
    PostSendEmailDataloading,
    PostSendEmailData,
    SendEmailClearDispatch,
    OGMpostDispatch,
    OGMpostDAta,
    clearOGMpostDispatch,
    OGMpostloading,
    playStoreExportResponse,
    getPlayStoreExportDispatch,
    clearPlayStoreExportDispatch,
    playStoreExportLoading,
    exportClientPlayStoreDispatch,
    exportClientWebDispatch,
    GetClientsRes,
    exportclientReports,
    exportClientPlayStoreReports,
    getClientsWrmDispatch,
  } = props

  const didMount = React.useRef(false)
  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [limit, setLimit] = useState(25)
  const hiddenFileInput = useRef(null);
  const [, setData] = useState({})
  const [show, setShow] = useState(false)
  const [showEmail, setShowEmail] = useState(false)
  const [showOGM, setShowOGM] = useState(false)
  const [Id, setId] = useState('')
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [searchParams, setSearchParams] = useState({})
  const [fileName, setFileName] = useState()
  const [sortingParams, setSortingParams] = useState()
  const [sorting, setSorting] = useState({
    tag: false,
    createdDate: false,
    rejected: false,
    total: false,
    manualreview: false
  })
  const [formData, setFormData] = useState([])
  const [searchData, setSearchData] = useState({})
  const [listIndex, setListIndex] = useState(undefined)
  const [listId, setListID] = useState()
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [selectShow, setSelectShow] = useState(false)
  // const [formData, setFormData] = useState({
  //   checkName: ''
  // })
  const [updateFormData, setupdateFormData] = useState({
    deliveryDate: '',
    rejectedCases: '',
    approvedCases: '',
    uploadedDate: '',
    file: '',
    tag: '',
    totalDeliveryCases: '',
  })
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [emailFormData, setEmailFormData] = useState({
    emailcc: '',
    emailbcc: '',
  })
  const [emailErrors, setEmailErrors] = useState({
    emailcc: '',
    emailbcc: ''
  })
  const [ogmFormData, setOgmFormData] = useState({
    tag: '',
    monitoringInterval: '',
    summaryId: ''
  })
  const [ogmErrors, setOgmErrors] = useState({
    tag: '',
    monitoringInterval: '',
  })

  const handleChanges = (e) => {
    setupdateFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleEmailChanges = (e) => {
    setEmailFormData((emailFormData) => ({ ...emailFormData, [e.target.name]: e.target.value }))
    setEmailErrors({ ...emailErrors, [e.target.name]: '' })
  }

  const handleOGMChanges = (e) => {
    setOgmFormData((ogmFormData) => ({ ...ogmFormData, [e.target.name]: e.target.value }))
    setOgmErrors({ ...ogmErrors, [e.target.name]: '' })
  }

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }

  useEffect(() => {
    TagSummaryDispatch()
    clientIdDispatch()
  }, [])

  useEffect(() => {
    if (GetClientsRes && !_.isEmpty(GetClientsRes && GetClientsRes._id)) {
      const data = {
        selecttag: "multiple"
      }
      exportClientWebDispatch(GetClientsRes && GetClientsRes._id, data)
      exportClientPlayStoreDispatch(GetClientsRes && GetClientsRes._id, data)
    }
  }, [GetClientsRes])

  useEffect(() => {
    if (!_.isEmpty(formData)) {
      const data = {
        tag: formData && formData[0]
      }
      getClientsWrmDispatch(data)
    }
  }, [formData])

  const TagId = (id) => {
    TagSummaryIdDispatch(id)
  }

  const onConfirm = (id) => {
    const params = { summaryId: id }
    UpdatetagstatusDispatch(params)
  }
  const postSummary = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      "You want to Resume this Batch?",
      'warning',
      'Yes',
      'No',
      () => {
        onConfirm(id)
      },
      () => { }
    )
  }

  const UpdatePostSendEmail = (id) => {
    const emailErrors = {}
    if (_.isEmpty(emailFormData.emailcc)) {
      emailErrors.emailcc = 'Email CC is required.'
    }
    // if (_.isEmpty(emailFormData.emailbcc)) {
    //   emailErrors.emailbcc = 'Email BCC is required.'
    // }
    setEmailErrors(emailErrors)
    if (_.isEmpty(emailErrors)) {
      const params = {
        summaryId: listId,
        emailcc: emailFormData.emailcc,
        emailbcc: emailFormData.emailbcc
      }
      PostSendEmailDispatch(params)
    }
  }

  const OGMUpdate = () => {
    const ogmErrors = {}
    if (_.isEmpty(ogmFormData.monitoringInterval)) {
      ogmErrors.monitoringInterval = 'Monitoring Interval is required.'
    }
    setOgmErrors(ogmErrors)
    if (_.isEmpty(ogmErrors)) {
      const params = {
        summaryId: ogmFormData.summaryId,
        ogm_interval: ogmFormData.monitoringInterval
      }
      OGMpostDispatch(params)
    }
  }

  const Update = (id) => {
    const errors = {}
    // if (_.isEmpty(updateFormData.deliveryDate)) {
    //   errors.deliveryDate = 'DeliveryDate is required.'
    // }
    if (_.isEmpty(updateFormData.rejectedCases)) {
      errors.rejectedCases = 'Rejected Cases is required.'
    }
    if (_.isEmpty(updateFormData.approvedCases)) {
      errors.approvedCases = 'Approved Cases is required.'
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      if (!_.isEmpty(fileName)) {
        const data = new FormData()
        data.append("deliveryDate", moment(updateFormData.deliveryDate).format("YYYY-MM-DD"))
        data.append("uploadedDate", moment(updateFormData.uploadedDate).format("YYYY-MM-DD"))
        data.append("rejectedCases", updateFormData.rejectedCases)
        data.append("file", updateFormData.file)
        data.append("approvedCases", updateFormData.approvedCases)
        data.append("totalDeliveryCases", updateFormData.totalDeliveryCases)
        TagSummaryUpdateDispatch(Id, data)
      } else {
        const data = new FormData()
        data.append("deliveryDate", moment(updateFormData.deliveryDate).format("YYYY-MM-DD"))
        data.append("uploadedDate", moment(updateFormData.uploadedDate).format("YYYY-MM-DD"))
        data.append("rejectedCases", updateFormData.rejectedCases)
        data.append("approvedCases", updateFormData.approvedCases)
        data.append("totalDeliveryCases", updateFormData.totalDeliveryCases)
        TagSummaryUpdateDispatch(Id, data)
      }
    }
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1)
      const params = {
        ...sortingParams,
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : '',
      }
      const pickByParams = _.pickBy(params)
      TagSummaryDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
    }
  }, [setFilterFunction, setCredFilterParams])


  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      ...sortingParams,
      limit: value,
      page: 1,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : "",
    }
    TagSummaryDispatch(params)
    setActivePageNumber(1)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      ...sortingParams,
      limit: limit,
      page: pageNumber,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : searchData.clientId ? searchData.clientId : "",
      deliveryDateFrom: searchData.deliveryDateFrom,
      deliveryDateTo: searchData.deliveryDateTo,
      status: searchData.status,
      tag: searchData.tag,
      uploadedDateFrom: searchData.uploadedDateFrom,
      uploadedDateTo: searchData.uploadedDateTo,
    }
    setActivePageNumber(pageNumber)
    TagSummaryDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC',
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : "",
      }
      setSortingParams(params)
      TagSummaryDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC',
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : "",
      }
      setSortingParams(params)
      TagSummaryDispatch(params)
    }
  }

  const exported = () => {
    const params = {
      tag: formData.toString()
    }
    // const params = {
    //   tag: formData.checkName
    // }
    getExportDispatch(params)
  }

  const clear = () => {
    setFormData([])
  }

  useEffect(() => {
    if (exportLists && exportLists.status === 'ok') {
      if (Array.isArray(exportLists && exportLists.data)) {
        const closeXlsx = document.getElementById('bulkCsvReport')
        closeXlsx.click()
        clearExportListDispatch()
      } else if (_.isEmpty(exportLists && exportLists.data)) {
        warningAlert(
          'error',
          exportLists && exportLists.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearExportListDispatch()
      } else if (!_.isEmpty(exportLists && exportLists.data.file)) {

        const DataUrl = exportLists && exportLists.data.file
        const data = DataUrl
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearExportListDispatch()
      }
    }else if (exportLists && exportLists.status === 'error') {
      warningAlert(
        'error',
        exportLists && exportLists.message,
        '',
        'Try again',
        '',
        () => { {} }
      )
      clearExportListDispatch()
    }
  }, [exportLists])

  useEffect(() => {
    if (UpdatetagstatusData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        UpdatetagstatusData.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      CleartagstatusDispatch()
    } else if (UpdatetagstatusData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        UpdatetagstatusData.message,
        '',
        'Ok'
      )
    }
    CleartagstatusDispatch()
  }, [UpdatetagstatusData])

  useEffect(() => {
    if (TagSummaryIdData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShow(true)
      const getIdData = TagSummaryIdData && TagSummaryIdData.data
      const data = setTagSummaryData(getIdData)
      setupdateFormData(data)
      setId(getIdData && getIdData._id)
    } else if (TagSummaryIdData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        TagSummaryIdData.message,
        '',
        'Ok'
      )
    }
  }, [TagSummaryIdData])

  useEffect(() => {
    if (TagSummaryUpdateData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        TagSummaryUpdateData.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      ClearTagSummaryUpdateDispatch()
      ClearIdDispatch()
      setShow(false)
      setupdateFormData({
        deliveryDate: '',
        rejectedCases: '',
        approvedCases: '',
        uploadedDate: '',
        file: ''
      })
      setFileName('')
    } else if (TagSummaryUpdateData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        TagSummaryUpdateData.message,
        '',
        'Ok'
      )
    }
    ClearTagSummaryUpdateDispatch()
  }, [TagSummaryUpdateData])

  useEffect(() => {
    if (PostSendEmailData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        PostSendEmailData.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      SendEmailClearDispatch()
      ClearIdDispatch()
      setShowEmail(false)
    } else if (PostSendEmailData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        PostSendEmailData.message,
        '',
        'Ok'
      )
    }
    SendEmailClearDispatch()
  }, [PostSendEmailData])

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setupdateFormData((values) => ({
          ...values,
          file: files,
        }));
        setErrors((values) => ({ ...values, file: "" }));
        setFileName(files && files.name);
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  }

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const queue = getDefaultOption(AsigneesNames)
    if (!_.isEmpty(listId)) {
      const selOption = _.filter(queue, function (x) {
        if (_.includes(listId, x.value)) {
          setAsignees(x.label)
          return x
        }
      })
      setSelectedAsigneesOption(selOption)
    } else {
      setSelectedAsigneesOption()
    }
  }, [listId])

  const totalPages =
    TagSummaryData && TagSummaryData.data && TagSummaryData.data.count
      ? Math.ceil(parseInt(TagSummaryData && TagSummaryData.data && TagSummaryData.data.count) / limit)
      : 1

  const OGMACTION = [
    "STOPPED",
    "NOT STARTERD",
    ""
  ]

  const OGMpostSubmit = (tagData, Id) => {
    setShowOGM(true)
    setOgmFormData({
      tag: tagData,
      summaryId: Id
    })
  }

  const onOGMconfirm = () => { console.log('sucessss') }

  const OGMstop = (tagData) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      "Are you sure, Want to Stop the Ongoing Monitoring in this Batch?",
      'warning',
      'Yes',
      'No',
      () => { onOGMconfirm() },
      () => { }
    )
  }

  useEffect(() => {
    if (OGMpostDAta.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        OGMpostDAta.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      clearOGMpostDispatch()
      setShowOGM(false)
    } else if (OGMpostDAta.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        OGMpostDAta.message,
        '',
        'Ok'
      )
    }
    clearOGMpostDispatch()
  }, [OGMpostDAta])

  useEffect(() => {
    if (playStoreExportResponse && playStoreExportResponse.status === 'ok') {
      if (Array.isArray(playStoreExportResponse && playStoreExportResponse.data )) {
        const closeXlsx = document.getElementById('PlayStoreReport')
        closeXlsx.click()
        clearPlayStoreExportDispatch()
      } else if (_.isEmpty(playStoreExportResponse && playStoreExportResponse.data )) {
        warningAlert(
          'error',
          playStoreExportResponse && playStoreExportResponse.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearPlayStoreExportDispatch()
      } else if (!_.isEmpty(playStoreExportResponse && playStoreExportResponse.data.file)) {
        const data = playStoreExportResponse && playStoreExportResponse.data.file
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearPlayStoreExportDispatch()
      }
    }else if (playStoreExportResponse && playStoreExportResponse.status === 'error') {
      warningAlert(
        'error',
        playStoreExportResponse && playStoreExportResponse.message,
        '',
        'Try again',
        '',
        () => { {} }
      )
      clearPlayStoreExportDispatch()
    }
  }, [playStoreExportResponse])

  const playStorExported = () => {
    const params = {
      tag: formData.toString()
    }
    getPlayStoreExportDispatch(params)
  }

  useEffect(() => {
    return () => {
      ClearIdDispatch()
    }
  }, [])

  const handleCheckboxChange = useCallback((e) => {
    e.persist()
    if(e.target.checked === true) {
      const params = {
        ...searchData,
        skipPagination: 'true',
      };
    
      const url = serviceList.TagSummary;
    
      axiosInstance.get(url, { params }).then((response) => {
        if (response.data && response.data.status === 'ok') {
          const data = response.data.data.result;
          const result = data && data.map((item) =>{
            return item.tag
          })
          setFormData(result);
        }
      });
    }else if(e.target.checked === false) {
      setFormData([])
    }
    
  }, [searchData, setFormData, serviceList.TagSummary, axiosInstance]);  

  return (
    <>

      <Modal
        show={showOGM}
        size="lg"
        centered
        onHide={() => setShowOGM(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => setShowOGM(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            OGM Update
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className='row'>
                    <div className='col-lg-3 mb-3'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Tag :
                      </label>
                    </div>
                    <div className='col-lg-6'>
                      <input
                        placeholder='Tag'
                        className={clsx(
                          'form-control form-control-lg form-control-solid'
                        )}
                        type='text'
                        name='tag'
                        autoComplete='off'
                        value={ogmFormData.tag || ''}
                        disabled
                      />
                      {ogmErrors && ogmErrors.tag && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {ogmErrors.tag}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='row mt-4'>
                    <div className='col-lg-3 mb-3'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Monitoring Interval :
                      </label>
                    </div>
                    <div className='col-lg-8'>
                      <div className="form-check form-check-custom form-check-solid mb-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleOGMChanges(e)}
                              value='WEEKLY'
                              name='monitoringInterval'
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Weekly
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleOGMChanges(e)}
                              value='BIWEEKLY'
                              name='monitoringInterval'
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Bi Weekly
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleOGMChanges(e)}
                              value='MONTHLY'
                              name='monitoringInterval'
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Monthly
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleOGMChanges(e)}
                              value='QUARTERLY'
                              name='monitoringInterval'
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Quarterly
                            </span>
                          </span>
                        </label>
                      </div>
                      {ogmErrors && ogmErrors.monitoringInterval && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {ogmErrors.monitoringInterval}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='form-group row mb-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => OGMUpdate()}
                          disabled={OGMpostloading}
                        >
                          {OGMpostloading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Submit'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => setShowOGM(false)}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <Modal
        show={showEmail}
        size="lg"
        centered
        onHide={() => {
          setShowEmail(false)
          setListID('')
        }} >
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => {
            setShowEmail(false)
            setListID('')
          }}
        >
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Send Email Update
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  {/* <div className="row mb-8">
                    <div className='col-md-2'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Client :
                      </label>
                    </div>
                    <div className='col-md-6'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                        isDisabled={AsigneesOption}
                      />
                      {emailErrors && emailErrors.client && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {emailErrors.client}
                        </div>
                      )}
                    </div>
                  </div> */}
                  <div className="row mb-8">
                    <div className='col-lg-2 mb-3'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Email CC :
                      </label>
                    </div>
                    <div className='col-lg-6'>
                      <textarea
                        placeholder='Email CC'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': emailFormData.emailcc && errors.emailcc },
                          {
                            'is-valid': emailFormData.emailcc && !errors.emailcc
                          }
                        )}
                        onChange={(e) => handleEmailChanges(e)}
                        type='text'
                        name='emailcc'
                        autoComplete='off'
                        value={emailFormData.emailcc || ''}
                        onKeyPress={(e) => {
                          if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                            e.preventDefault();
                          }
                        }}
                      />
                      {emailErrors && emailErrors.emailcc && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {emailErrors.emailcc}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row mb-8">
                    <div className='col-lg-2 mb-3'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Email BCC :
                      </label>
                    </div>
                    <div className='col-lg-6'>
                      <textarea
                        placeholder='Email BCC'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': emailFormData.emailbcc && errors.emailbcc },
                          {
                            'is-valid': emailFormData.emailbcc && !errors.emailbcc
                          }
                        )}
                        onChange={(e) => handleEmailChanges(e)}
                        type='text'
                        name='emailbcc'
                        autoComplete='off'
                        value={emailFormData.emailbcc || ''}
                        onKeyPress={(e) => {
                          if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                            e.preventDefault();
                          }
                        }}
                      />
                      {emailErrors && emailErrors.emailbcc && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {emailErrors.emailbcc}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className='form-group row mb-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => UpdatePostSendEmail()}
                          disabled={PostSendEmailDataloading}
                        >
                          {PostSendEmailDataloading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Update'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => setShowEmail(false)}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => setShow(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => setShow(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Update
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Tag :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Tag'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': updateFormData.tag && errors.tag },
                          {
                            'is-valid': updateFormData.tag && !errors.tag
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='tag'
                        autoComplete='off'
                        value={updateFormData.tag || ''}
                        disabled
                      />
                      {errors && errors.tag && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.tag}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Approved Cases :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Approved Cases'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': updateFormData.approvedCases && errors.approvedCases },
                          {
                            'is-valid': updateFormData.approvedCases && !errors.approvedCases
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='approvedCases'
                        autoComplete='off'
                        value={updateFormData.approvedCases || ''}
                      />
                      {errors && errors.approvedCases && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.approvedCases}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Rejected Cases :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Rejected Cases'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': updateFormData.rejectedCases && errors.rejectedCases },
                          {
                            'is-valid': updateFormData.rejectedCases && !errors.rejectedCases
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='rejectedCases'
                        autoComplete='off'
                        value={updateFormData.rejectedCases || ''}
                      />
                      {errors && errors.rejectedCases && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.rejectedCases}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Batch Uploaded Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='uploadedDate'
                        placeholder='Batch Uploaded Date'
                        className='form-control'
                        selected={updateFormData.uploadedDate || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, uploadedDate: '' })
                          setupdateFormData((values) => ({
                            ...values,
                            uploadedDate: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                      // isClearable={clear}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Batch Delivery Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='deliveryDate'
                        placeholder='Batch Delivery Date'
                        className='form-control'
                        selected={updateFormData.deliveryDate || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, deliveryDate: '' })
                          setupdateFormData((values) => ({
                            ...values,
                            deliveryDate: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      // isClearable={true}
                      />
                    </div>
                  </div>

                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Total Delivery Cases :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Total Delivery Cases'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': updateFormData.totalDeliveryCases && errors.totalDeliveryCases },
                          {
                            'is-valid': updateFormData.totalDeliveryCases && !errors.totalDeliveryCases
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='totalDeliveryCases'
                        autoComplete='off'
                        value={updateFormData.totalDeliveryCases || ''}
                        onKeyPress={(e) => {
                          if (!REGEX.NUMERIC.test(e.key)) {
                            e.preventDefault()
                          }
                        }}
                      />
                      {errors && errors.totalDeliveryCases && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.totalDeliveryCases}
                        </div>
                      )}
                    </div>
                  </div>

                  {/* <div className='col-md-4 mt-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Upload Document :
                    </label>
                    <div className='col-md-12'>
                      <input
                        type="file"
                        className="d-none"
                        name="file"
                        id="file"
                        multiple={true}
                        ref={hiddenFileInput}
                        onChange={handleFileChange}
                      />
                      <button
                        type="button"
                        style={{
                          width: "100%",
                        }}
                        className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                        onClick={handleClick}
                      >
                        <i className="bi bi-filetype-csv" />
                        Upload Document
                      </button>
                      {fileName && fileName}
                    </div>
                  </div> */}
                  <div className='form-group row mb-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => Update()}
                          disabled={TagSummaryupdateLoading}
                        >
                          {TagSummaryupdateLoading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Update'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => setShow(false)}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename={formData && formData.length > 1 ? 'web-report' :`${formData}-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="PlayStoreReport"
          className="download-table-xls-button"
          table="PlayStoreReport-table"
          filename={formData && formData.length > 1 ? 'playstore-report' : `${formData}-report`}
          sheet="tablexls"
        />
      </div>


      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="table-to-xls">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              {
                exportclientReports && exportclientReports.data.map((item, i) => {
                  return (
                    <th>{item.report_value}</th>
                  )
                })
              }
            </tr>
          </thead>
          <tbody className="fs-10">
            {
              _.isArray(exportLists && exportLists.data) ?
                exportLists && exportLists.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      {
                        exportclientReports && exportclientReports.data.map((itemReport, i) => {
                          let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                          return (
                            _.isArray(reportTdData) ? (
                              <td>
                                {item[itemReport.report_key].toString()}
                              </td>
                            ) :
                              <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                          )
                        })
                      }
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>

      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="PlayStoreReport-table">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              {
                exportClientPlayStoreReports && exportClientPlayStoreReports.data.map((item) => {
                  return (
                    <th>{item.report_value}</th>
                  )
                })
              }
            </tr>
          </thead>
          <tbody className="fs-10">
            {
              _.isArray(playStoreExportResponse && playStoreExportResponse.data) ?
                playStoreExportResponse && playStoreExportResponse.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      {
                        exportClientPlayStoreReports && exportClientPlayStoreReports.data.map((itemReport, i) => {
                          let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                          return (
                            _.isArray(reportTdData) ? (
                              <td>
                                {item[itemReport.report_key].toString()}
                              </td>
                            ) :
                              <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                          )
                        })
                      }
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      <div className={`card md-10 ${className}`} >
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1 ms-4'>
                {TagSummaryData && TagSummaryData.data && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3 ms-1'>
                      {TagSummaryData && TagSummaryData.data && TagSummaryData.data.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='col-lg-6'>
              <div className='d-flex justify-content-end mt-4 me-4'>
                <SearchList setCredFilterParams={setCredFilterParams} setSearchData={setSearchData} TagSummaryData={TagSummaryData} setSelectShow = {setSelectShow} setFormDatas = {setFormData}/>
                {
                  !_.isEmpty(formData) && Role !== 'Analyst' ? (
                    <>
                      <button
                        type='button'
                        className='btn btn-md btn-light-success btn-responsive pull-right w-150px'
                        onClick={(e) => exported(e)}
                        disabled={exportLoading}
                      >
                        {!exportLoading &&
                          <span className='indicator-label'>
                            <i className="bi bi-filetype-csv" />
                            Web Report
                          </span>
                        }
                        {exportLoading && (
                          <span className='indicator-progress text-success' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                      <button
                        type='button'
                        className='btn btn-sm btn-light-success btn-responsive font-5vw me-3 pull-right  ms-2'
                        onClick={(e) => playStorExported(e)}
                        disabled={playStoreExportLoading}
                      >
                        {!playStoreExportLoading &&
                          <span className='indicator-label'>
                            <i className="bi bi-filetype-csv" />
                            Play Store Report Export
                          </span>
                        }
                        {playStoreExportLoading && (
                          <span className='indicator-progress text-success' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    </>
                  )
                    : null
                }
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                {setCredFilterParams.clientId || selectShow ?
                  <th>
                    <div className="d-flex">
                      <span>Select all</span>
                    </div>
                    <div className='mt-4'>
                    {/* {selectShow ? */}
                    <div className="min-width-150px text-center form-check form-check-custom form-check-success form-check-solid">
                      <input
                        className="form-check-input cursor-pointer "
                        type="Checkbox"
                        onChange={(e) => handleCheckboxChange(e)}
                        name='checkNameSelectAll'
                      />
                    </div>
                      {/* : null} */}
                      </div>
                  </th> : null}
                  <th className="min-w-150px">
                    <div className="d-flex">
                      <span>Batch Uploaded Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("uploadedDate")}
                        >
                          <i
                            className={`bi ${sorting.uploadedDate
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-150px">
                    <div className="d-flex">
                      <span>Batch Delivery Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deliveryDate")}
                        >
                          <i
                            className={`bi ${sorting.deliveryDate
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Batch Name</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Cases</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("totalCases")}
                        >
                          <i
                            className={`bi ${sorting.totalCases
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Case Approved</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("approvedCases")}
                        >
                          <i
                            className={`bi ${sorting.approvedCases
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Case Rejected</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("rejectedCases")}
                        >
                          <i
                            className={`bi ${sorting.rejectedCases
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {/* <th>
                    <div className="d-flex">
                      <span>Approval Rate%</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("rejected")}
                        >
                          <i
                            className={`bi ${sorting.rejected
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Rejection Rate%</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("rejected")}
                        >
                          <i
                            className={`bi ${sorting.rejected
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th> */}
                  {/* {
                    _.includes(Role, 'Admin') ? (
                      <th>
                        <div className="d-flex w-150px">
                          <span>Tag Status</span>
                        </div>
                      </th>
                    ) : null
                  } */}
                  <th>
                    <div className="d-flex">
                      <span>Time Taken to Deliver</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("hoursConsumed")}
                        >
                          <i
                            className={`bi ${sorting.hoursConsumed
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Batch Type</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("batchType")}
                        >
                          <i
                            className={`bi ${sorting.batchType
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {/* <th>
                    <div className="d-flex">
                      <span>OGM Interval</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("OGM Interval")}
                        >
                          <i
                            className={`bi ${sorting.OGMInterval
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>OGM Last Run Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("OGM Last Run Date")}
                        >
                          <i
                            className={`bi ${sorting.OGMInterval
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>OGM Next Run Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("OGM Next Run Date")}
                        >
                          <i
                            className={`bi ${sorting.OGMInterval
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex w-150px">
                      <span>OGM Action</span>
                    </div>
                  </th> */}

                </tr>
              </thead>
              <tbody className='fs-7'>
                {
                  !loading
                    ? (
                      Array.isArray(TagSummaryData &&
                        TagSummaryData.data && TagSummaryData.data.result)
                        ? (
                          TagSummaryData && TagSummaryData.data && TagSummaryData.data.result.map((item, i) => {

                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  {setCredFilterParams.clientId || selectShow  ?
                                    <td className="min-width-150px text-center form-check form-check-custom form-check-success form-check-solid">
                                      <input
                                        className="form-check-input cursor-pointer "
                                        type="Checkbox"
                                        value={item.tag}
                                        onChange={(e) => handleChange(e)}
                                        checked={formData && formData.includes(item.tag)}
                                        name='checkName'
                                      />
                                    </td> : null}
                                  <td>
                                    {
                                      item.uploadedDate ? item.uploadedDate : "--"
                                    }
                                  </td>
                                  <td>
                                    {
                                      item.resolvedDate ? item.resolvedDate : "--"
                                    }
                                  </td>
                                  <td>
                                    {item && item.tag ? item.tag : '--'}
                                  </td>
                                  <td>
                                    {
                                      _.includes(Role, 'Client User') ? (
                                        <span className={`badge  ${RISKSTATUS[item.status === 'HOLD' ? 'PROCESSING' : item.status]}`}>
                                          {item.status === 'HOLD' ? 'PROCESSING' : item.status}
                                        </span>
                                      ) : (
                                        <span className={`badge  ${RISKSTATUS[item.status]}`}>
                                          {item.status ? item.status : "--"}
                                        </span>
                                      )
                                    }

                                  </td>
                                  <td>
                                    {item && item.totalCases ? item.totalCases : '0'}
                                  </td>
                                  <td>
                                    {item && item.approvedCases && item.approvalRate ?`${item.approvedCases}(${Number(item.approvalRate).toFixed(2)}%)` : '0(0%)'}
                                  </td>
                                  <td>
                                    {item && item.rejectedCases && item.rejectionRate ?`${item.rejectedCases}(${Number(item.rejectionRate).toFixed(2)}%)` : '0(0%)'}
                                  </td>
                                  <td>
                                    {item && item.hoursConsumed ? item.hoursConsumed : '--'}
                                  </td>
                                  {/* <td>
                                    {item && item.approvalRate ? Number(item.approvalRate).toFixed(2) : '0'}%
                                  </td>
                                  <td>
                                    {item && item.rejectionRate ? Number(item.rejectionRate).toFixed(2) : '0'}%
                                  </td> */}
                                  {/* <td>
                                    {
                                      _.includes(Role, 'Admin') ?
                                        (
                                          item && item.status === 'COMPLETED' ? (
                                            <>--</>
                                          ) :
                                            <button className={`btn btn-sm ${BUTTON_COLOR[item && item.status]}`}
                                              onClick={() => {
                                                if (item && item.status === 'HOLD') {
                                                  postSummary(item && item._id)
                                                  listIndex === i ? setListIndex(i) : setListIndex(i)
                                                }
                                                if (item && item.status === 'PROCESSING') {
                                                  TagId(item && item._id)
                                                  listIndex === i ? setListIndex(i) : setListIndex(i)
                                                }
                                                if (item && item.status === 'UPDATE STATUS') {
                                                  {
                                                    setShowEmail(true)
                                                    setListID(item && item._id)
                                                    listIndex === i ? setListIndex(i) : setListIndex(i)
                                                  }
                                                }
                                              }}
                                              disabled={TagSummaryIdDataloading}
                                            >
                                              {
                                                i === listIndex && TagSummaryIdDataloading ? (
                                                  <span
                                                    className='spinner-border spinner-border-sm mx-3'
                                                    role='status'
                                                    aria-hidden='true'
                                                  />
                                                ) : TAGSTATUSNAME[item && item.status]
                                              }

                                            </button>
                                        ) : null
                                    }
                                  </td> */}
                                  <td>
                                    {item && item.batchType ? item.batchType : "Live"}
                                  </td>
                                  {/* 
                                  <td>
                                    {item && item.ogm_interval ? item.ogm_interval : '--'}
                                  </td>
                                  <td>
                                    {
                                      LastRunDate === 'Invalid date' ? '--' : LastRunDate
                                    }
                                  </td>
                                  <td>
                                    {
                                      NextRunDate === 'Invalid date' ? '--' : NextRunDate
                                    }
                                  </td>
                                  <td>
                                    {
                                      _.includes(item && item.ogm, OGMACTION) ? (
                                        <button className='btn btn-sm btn-light-primary pull-right w-120px'
                                          onClick={() => {
                                            {
                                              OGMpostSubmit(item && item.tag, item && item._id)
                                            }
                                          }}
                                        >
                                          OGM START
                                        </button>
                                      ) :
                                        item && item.ogm === 'STARTED' ?
                                          <button className='btn btn-sm btn-light-danger pull-right w-120px'
                                            onClick={() => {
                                              {
                                                OGMstop(item && item.tag)
                                              }
                                            }}
                                          >
                                            OGM STOP
                                          </button>
                                          :
                                          <button className='btn btn-sm btn-light-primary pull-right w-120px'
                                            onClick={() => {
                                              {
                                                OGMpostSubmit(item && item.tag, item && item._id)
                                              }
                                            }}
                                          >
                                            OGM START
                                          </button>
                                    }
                                  </td> */}
                                </tr>
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { TagSummaryStore, SummaryWebExportStore, UpdatetagstatusStore, TagSummaryIDStore, TagSummaryUpdateStore, clinetListStore, PostSendEmailStore, OGMStore, SummaryPlayExportStore, GetClientsStore, DeletemonitorReportStore, BatchSummaryExportStore } = state
  return {
    loading: TagSummaryStore && TagSummaryStore.loading ? TagSummaryStore.loading : false,
    TagSummaryData: TagSummaryStore && TagSummaryStore.TagSummaryData ? TagSummaryStore.TagSummaryData : {},

    exportLists: SummaryWebExportStore && SummaryWebExportStore.summaryWebExportData ? SummaryWebExportStore.summaryWebExportData : '',
    exportLoading: SummaryWebExportStore && SummaryWebExportStore.loading ? SummaryWebExportStore.loading : '',
    playStoreExportResponse: SummaryPlayExportStore && SummaryPlayExportStore.summaryPlayExportData ? SummaryPlayExportStore.summaryPlayExportData : '',
    playStoreExportLoading: SummaryPlayExportStore && SummaryPlayExportStore.loading ? SummaryPlayExportStore.loading : '',

    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    UpdatetagstatusData: UpdatetagstatusStore && UpdatetagstatusStore.UpdatetagstatusData && UpdatetagstatusStore.UpdatetagstatusData.data ? UpdatetagstatusStore.UpdatetagstatusData.data : '',
    TagSummaryIdData: TagSummaryIDStore && TagSummaryIDStore.TagSummaryIdData ? TagSummaryIDStore.TagSummaryIdData : '',
    TagSummaryIdDataloading: TagSummaryIDStore && TagSummaryIDStore.loading ? TagSummaryIDStore.loading : false,
    TagSummaryupdateLoading: TagSummaryUpdateStore && TagSummaryUpdateStore.loading ? TagSummaryUpdateStore.loading : false,
    TagSummaryUpdateData: TagSummaryUpdateStore && TagSummaryUpdateStore.TagSummaryUpdateData ? TagSummaryUpdateStore.TagSummaryUpdateData : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    PostSendEmailData: PostSendEmailStore && PostSendEmailStore.PostSendEmailData ? PostSendEmailStore.PostSendEmailData : '',
    PostSendEmailDataloading: PostSendEmailStore && PostSendEmailStore.loading ? PostSendEmailStore.loading : false,
    OGMpostDAta: OGMStore && OGMStore.OGMpostDAta ? OGMStore.OGMpostDAta : '',
    OGMpostloading: OGMStore && OGMStore.loading ? OGMStore.loading : false,
    exportclientReports: state && state.webclientReportStore && state.webclientReportStore.exportclientReports,
    exportClientPlayStoreReports: state && state.exportClientPlayStoreReportStore && state.exportClientPlayStoreReportStore.exportClientPlayStoreReports,
    GetClientsRes: GetClientsStore && GetClientsStore.GetClientsRes ? GetClientsStore.GetClientsRes?.data : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  TagSummaryDispatch: (params) => dispatch(TagSummaryAction.TagSummary(params)),
  
  getExportDispatch: (data) => dispatch(SummaryWebExportAction.SummaryWebExport(data)),
  clearExportListDispatch: (data) => dispatch(SummaryWebExportAction.SummaryWebExportClear(data)),
  getPlayStoreExportDispatch: (data) => dispatch(SummaryPlaytoreExportAction.SummaryPlaytoreExport(data)),
  clearPlayStoreExportDispatch: (data) => dispatch(SummaryPlaytoreExportAction.SummaryPlaytoreExportClear(data)),


  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
  UpdatetagstatusDispatch: (data) => dispatch(UpdatetagstatusAction.Updatetagstatus(data)),
  CleartagstatusDispatch: (data) => dispatch(UpdatetagstatusAction.UpdatetagstatusClear(data)),
  TagSummaryIdDispatch: (params) => dispatch(TagSummaryIdAction.TagSummaryID(params)),
  ClearIdDispatch: (params) => dispatch(TagSummaryIdAction.TagSummaryIdClear(params)),
  TagSummaryUpdateDispatch: (id, params) => dispatch(TagSummaryUpdateAction.TagSummaryUpdate(id, params)),
  ClearTagSummaryUpdateDispatch: (params) => dispatch(TagSummaryUpdateAction.TagSummaryUpdateClear(params)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  PostSendEmailDispatch: (data) => dispatch(PostSendEmailAction.PostSendEmail(data)),
  SendEmailClearDispatch: (data) => dispatch(PostSendEmailAction.PostSendEmailClear(data)),
  OGMpostDispatch: (data) => dispatch(OGMactions.OGMpost(data)),
  clearOGMpostDispatch: (data) => dispatch(OGMactions.clearOGMpost(data)),
  exportClientWebDispatch: (id, params) => dispatch(exportClientWebActions.exportClientWeb(id, params)),
  exportClientPlayStoreDispatch: (id, params) => dispatch(exportClientPlayStoreActions.exportClientPlayStore(id, params)),
  getClientsWrmDispatch: (data) => dispatch(GetClientsActions.getClientsWrm(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(TagSummaryList)
