import React, { useEffect, useState } from 'react'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import _ from 'lodash'
import { Link } from 'react-router-dom'

function MerchantDetails(props) {
  const {
    merchantSummary
  } = props

  return (
    <>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='card-header border-0'>
          <h3 className='card-title align-items-start flex-column '>
            <span className='d-flex align-items-center fw-boldest my-1 fs-2'>Case Details - Case ID #{merchantSummary && merchantSummary.onboardingId ? merchantSummary.onboardingId : '--'}
            </span>
          </h3>
          <div className='col-lg-4'/>
        </div>
        <div className="separator separator-dashed my-3" />
        <div className="row">
          <div className="col-lg-3 ps-10">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">First Name</th>
                  <td>
                    {merchantSummary && merchantSummary.firstName ? merchantSummary.firstName: '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Last Name</th>
                  <td>
                    {merchantSummary && merchantSummary.lastName ? merchantSummary.lastName: '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Business Name</th>
                  <td>
                    {merchantSummary && merchantSummary.businessName ? merchantSummary.businessName : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Entity Name</th>
                  <td>
                    {merchantSummary && merchantSummary.entityName ? merchantSummary.entityName : '--'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-lg-5">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Comments</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.comments && merchantSummary.comments.length > 0 ? merchantSummary.comments.map(e => e) : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">EmailId</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.emailId && merchantSummary.emailId.emailId ? merchantSummary.emailId.emailId  : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Business Category</th>
                  <td>
                    {merchantSummary && merchantSummary.businessCategory ? merchantSummary.businessCategory : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Onboarding Updated Type</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.onboardingUpdatedType ? merchantSummary.onboardingUpdatedType : '--'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-lg-4">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Contact Name</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.primaryContactName ? merchantSummary.primaryContactName : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Phone Number</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.phoneNumber && merchantSummary.phoneNumber.number ?merchantSummary.phoneNumber.countryCode+' '+merchantSummary.phoneNumber.number : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Website</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.website ? merchantSummary.website : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Business Description</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.businessDescription ? merchantSummary.businessDescription : '--'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='card-header border-0 p-5'>
          <div className='col-lg-4 text-center'>
            <p>Accounts Queue</p>
            {merchantSummary && merchantSummary.accountsQueue === "YES" ?
              <Link to={`/account-risk-summary/update/${merchantSummary.accountQueueId}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
          </div>
          <div className='col-lg-4 text-center'>
            <p>KYC Queue</p>
            {merchantSummary && merchantSummary.kycQueue === "YES" ?
              <Link to={`/static-summary/update/${merchantSummary.kycId}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
          </div>
          <div className='col-lg-4 text-center'>
            <p>WRM Queue</p>
            {merchantSummary && merchantSummary.wrmQueue === "YES" ?
              <Link to={`/risk-summary/update/${merchantSummary.wrmId}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
          </div>
        </div>
      </div>
    </>
  )
}

export default MerchantDetails