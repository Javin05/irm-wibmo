import React, { useEffect, useState } from "react"
import moment from "moment"
import "react-circular-progressbar/dist/styles.css"
import Tabs from "react-bootstrap/Tabs"
import Tab from "react-bootstrap/Tab"
import MerchantDetails from "./merchantDetails"
import {
  OnBoardingSummaryActions,
} from "../../store/actions"
import { useLocation } from 'react-router-dom'
import { connect } from "react-redux"

function OnBoardingSummary(props) {
  const {
    onBoardingIdDetails,
    getOnBoardingSummary,
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [key, setKey] = useState("OnBoarding")
  const merchantSummary = onBoardingIdDetails && onBoardingIdDetails.data

  useEffect(() => {
    getOnBoardingSummary(id)
  },[])

  return (
    <>
      <div className="mt-0">
        <Tabs
          id="controlled-tab-example"
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className="ctab mb-3"
        >
          <Tab eventKey="OnBoarding" title="ONBOARDING">
            <MerchantDetails
              merchantSummary={merchantSummary}
            />
          </Tab>
        </Tabs>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { onBoardingSummaryStore } = state
  return {
    onBoardingIdDetails:
    onBoardingSummaryStore && onBoardingSummaryStore.onBoardingSummary
        ? onBoardingSummaryStore.onBoardingSummary
        : ""
  }
}

const mapDispatchToProps = (dispatch) => ({
  getOnBoardingSummary: (id) =>
    dispatch(OnBoardingSummaryActions.getOnBoardingSummary(id)),
})

export default connect(mapStateToProps, mapDispatchToProps) (OnBoardingSummary)