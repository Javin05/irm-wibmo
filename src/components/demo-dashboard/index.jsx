import React, { useState } from "react";
import {Dropdown} from "react-bootstrap";
import BarLineChart from "../chart-widgets/BarLineChart"
import DonutChart from "../chart-widgets/DonutChart"
import HorizontalBarChart from "../chart-widgets/HorizontalBarChart"
import MixedChart from "../chart-widgets/MixedChart"
import MultiVerticleBarChart from "../chart-widgets/MultiVerticleBarChart"
import MultiHorizontalBarChart from "../chart-widgets/MultiHorizontalBarChart"
import MultipleLineChart from "../chart-widgets/MultipleLineChart"
import SankeyChart from "../chart-widgets/SankeyChart"
import { Link, useLocation, useHistory } from "react-router-dom";

const DemoListCharts=()=>{

	const history = useHistory();
	const search = useLocation().search;
    const page = new URLSearchParams(search).get('page');

    const onPageChange = (page) => {
	    history.push({
            pathname: 'demo-dashboard',
            search: `?page=${page}`
        })
    }

	const caseSummaryData={
		"colors" :['#00596D', '#32BBBB', '#B95208', '#A22616', '#FFE089', '#B3B3B3'],
		"labels" :[
            'Approved - Auto', 
            'Approved - Manual', 
            'Rejected - Auto', 
            'Rejected - Manual', 
            'On Hold 109', 
            'Open 90'
        ],
		"series" :[44, 55, 13, 33, 88, 30]
	}
	const rejectedReasonData={
		"colors" :['#F76D0B', '#3DB0D3', '#4DEDC2', '#C83D95', '#FFBBED', '#42F9F9', '#00B2D9', '#FFD86C'],
		"labels" :[
            'Marked Fraud', 
            'Email Verification Failed', 
            'Device Fraud', 
            'High Risk Score', 
            'Address Verification Failed', 
            'Phone Verification',
            'Banned Bussiness',
            'Suyspicious Ip'
        ],
		"series" :[44, 55, 13, 33, 88, 30, 90, 20]
	}
	const caseVolumeChartData={
		"colors" :['#F76D0B', '#F9C19A', '#277761'],
		"labels" :[
            'Feb 14', 
            'Feb 15', 
            'Feb 16', 
            'Feb 17', 
            'Feb 18'
        ],
		"series" :[
            {
                name: 'Total Cases',
                type: 'column',
                data: [440, 505, 414, 671, 227]
            },            
            {
                name: 'One Cases',
                type: 'column',
                data: [300, 200, 100, 222, 401]
            }, 
            {
                name: 'Approved %',
                type: 'line',
                data: [23, 42, 35, 27, 43]
            }
        ]
	}
	const autoApprovedChartData={
		"colors" :['#00596D', '#AAE1E1', '#64C298'],
		"labels" :[
            'Feb 14', 
            'Feb 15', 
            'Feb 16', 
            'Feb 17', 
            'Feb 18'
        ],
		"series" :[
            {
                name: 'Approved Auto',
                type: 'column',
                data: [440, 505, 414, 671, 227]
            },            
            {
                name: 'Approved Manual',
                type: 'column',
                data: [300, 200, 100, 222, 401]
            }, 
            {
                name: 'Auto Approved %',
                type: 'line',
                data: [23, 42, 35, 27, 43]
            }
        ]
	}
	const rejectedCaseChartData={
		"colors" :['#666666', '#BFBFBF', '#A22616'],
		"labels" :[
            'Feb 14', 
            'Feb 15', 
            'Feb 16', 
            'Feb 17', 
            'Feb 18'
        ],
		"series" :[
            {
                name: 'Approved Auto',
                type: 'column',
                data: [440, 505, 414, 671, 227]
            },            
            {
                name: 'Approved Manual',
                type: 'column',
                data: [300, 200, 100, 222, 401]
            }, 
            {
                name: 'Auto Approved %',
                type: 'line',
                data: [23, 42, 35, 27, 43]
            }
        ]
	}
	const merchantRejectedData={
		"colors" :['#F9C19A'],
		"labels": ["Merchant 01", "Merchant 02", "Merchant 03", "Merchant 04", "Merchant 05", "Merchant 06"],
		"series": [
			{data: [44, 55, 10, 67, 80, 90]}
		]
	}
	const manualCaseResolutionChartData ={
		"colors" :['#666666', '#10A261', '#A22616', '#00596D'],
		"labels" :[
            'Feb 14', 
            'Feb 15', 
            'Feb 16', 
            'Feb 17', 
            'Feb 18'
        ],
		"series" :[
            {
                name: 'Total Case Handled',
                type: 'column',
                data: [440, 505, 414, 671, 227]
            },            
            {
                name: 'Approved',
                type: 'column',
                data: [300, 200, 100, 222, 401]
            },
            {
                name: 'Rejected',
                type: 'column',
                data: [300, 200, 100, 222, 401]
            },  
            {
                name: 'Resolution time in Mins',
                type: 'line',
                data: [23, 42, 35, 27, 43]
            }
        ]
	}
	const analystLevelChartData ={
		"colors" :['#666666', '#71F1CE', '#F98A3C'],
		"labels" :[
            'Bharathi', 
            'David', 
            'Priyanka', 
            'Jagan', 
            'Sanjay',
            'Praveen',
        ],
		"series" : [
		    {
		      name: 'Total Case Handled',
		      data: [4, 2, 4, 6, 2, 5]
		    },
		    {
		      name: 'Open',
		      data: [6, 3, 4, 3, 4, 3]
		    },
		    {
		      name: 'On Hold',
		      data: [3, 4, 1, 7, 4, 8]
		    }
		]
	}
	const analystLevelApprovalData ={
		"colors" :['#00596D', '#A22616'],
		"labels" :[
            'David', 
            'Bharathi', 
            'Sanjay',
            'Praveen',
            'Jagan', 
            'Priyanka', 
        ],
		"series" : [
		    {
		      name: 'Approved Manual',
		      data: [4, 2, 4, 6, 2, 5]
		    },
		    {
		      name: 'Rejected Manual',
		      data: [6, 3, 4, 3, 4, 3]
		    }
		]
	}
	const resolutionTimeData ={
		"colors" :['#F76D0B', '#3DB0D3', '#4DEDC2', '#C83D95', '#FFBBED', '#42F9F9'],
		"labels" :[
            '2/14/2022', 
            '2/15/2022', 
            '2/16/2022',
            '2/17/2022',
            '2/18/2022' 
        ],
		"series" : [
			{
	            name: "Bharathi",
	            data: [45, 52, 38, 24, 33]
            },
            {
                name: "David",
                data: [35, 41, 62, 42, 13]
            },
            {
                name: 'Jagan',
                data: [87, 57, 74, 99, 75]
            },
            {
	            name: "Praveen",
	            data: [12, 14, 18, 28, 56]
            },
            {
                name: "Priyanka",
                data: [45, 61, 72, 88, 30]
            },
            {
                name: 'Sanjay',
                data: [30, 40, 50, 99, 54]
            }
        ]
	}
	const rejectedReasonAnalysisData ={
		"colors" :"",
		"labels" :"",
		"series" :""
	}
	
	return (
		<>
			<div className="row mb-5">
				<div className="col-md-12">
					
	            </div>
			</div>
			{ page === null || page === "one"? (
					<>
						<div className="row mb-5">
				            <div className="col-md-6">
				            	<div className={`card card-custom gutter-b card-stretch`}>
				            		<div className="count-widget">
				            			<div className="count-widget-top">
				            				
				            			</div>
				            			<div className="count-widget-data">
				            				<div className="row m-0">
				            					<div className="col-md-6">
				            						<div className="widgetfy bg-light-warning">
					            						<h6>Total Cases</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            					<div className="col-md-6">
				            						<div className="widgetfy bg-light-primary">
					            						<h6>Closed Cases</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            				</div>
				            				<div className="row m-0">
				            					<div className="col-md-6">
													<div className="widgetfy bg-light-danger">
					            						<h6>Total Cases</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            					<div className="col-md-6">
				            						<div className="widgetfy bg-light-success">
					            						<h6>Total Cases</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            				</div>
				            			</div>
				            		</div>
				            	</div>
				            </div>
				           	<div className="col-md-6">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Case Summary 
												{/*<div className="font-size-sm text-muted mt-2">890,344 Summary</div>*/}
											</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
						                <DonutChart chartData={caseSummaryData} />
				            		</div>
			        			</div>
				            </div>
						</div>
						<div className="row mb-5">
				            <div className="col-md-6">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Case Volume Vs Approved %</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
				            			<MixedChart chartData={caseVolumeChartData}/>
				            		</div>
			        			</div>
				            </div>
				            <div className="col-md-6">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Manual Vs Auto Approved Cases Trend</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
				            			<MixedChart chartData={autoApprovedChartData}/>
				            		</div>
			        			</div>
				            </div>
						</div>
						<div className="row mb-5">
				            <div className="col-md-6">
				            	<div className={`card card-custom  gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Rejected Cases Analysis</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
				            			<MixedChart chartData={rejectedCaseChartData}/>
				            		</div>
			        			</div>
				            </div>
				            <div className="col-md-6">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Rejected Reason Analysis</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
						                <DonutChart chartData={rejectedReasonData}/>
				            		</div>
			        			</div>
				            </div>
						</div>
						<div className="row mb-5">
				            <div className="col-md-7">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Rejected Reason Analysis Trend</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0" style={{height:"320px"}}>
						            	<SankeyChart />
				            		</div>
			        			</div>	 
				            </div>
				            <div className="col-md-5">
				            	<div className={`card card-custom p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Merchants Rejected More Than Once</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
						                <HorizontalBarChart chartData={merchantRejectedData} stacked={false}/>
				            		</div>
			        			</div>	            	
				            </div>
						</div>
					</>
				) : (
					<>
						<div className="row mb-5">
				            <div className="col-md-4">
				            	<div className={`card card-custom gutter-b card-stretch`}>
				            		<div className="count-widget">
				            			<div className="count-widget-top">
				            				
				            			</div>
				            			<div className="count-widget-data">
				            				<div className="row m-0">
				            					<div className="col-md-12">
				            						<div className="widgetfy bg-light-warning">
					            						<h6>Total Active Cases</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            				</div>
				            				<div className="row m-0">
				            					<div className="col-md-6">
													<div className="widgetfy bg-light-danger">
					            						<h6>Cases On Hold</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            					<div className="col-md-6">
				            						<div className="widgetfy bg-light-success">
					            						<h6>Open Cases</h6>
					            						<h2>567</h2>
					            					</div>
				            					</div>
				            				</div>
				            			</div>
				            		</div>
				            	</div>
				            </div>
				           	<div className="col-md-8">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Manual Cases Resolution Trend</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
						                <MultiVerticleBarChart chartData={manualCaseResolutionChartData} />
				            		</div>
			        			</div>
				            </div>
						</div>
						<div className="row mb-5">
				            <div className="col-md-6">
				            	<div className={`card card-custom  gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Analyst Level Case Count</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">				            			
				            			<HorizontalBarChart chartData={analystLevelChartData} stacked={false}/>
				            		</div>
			        			</div>
				            </div>
				            <div className="col-md-6">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Analyst Level Approval Rates</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
						                <HorizontalBarChart chartData={analystLevelApprovalData} stacked={true}/>
				            		</div>
			        			</div>
				            </div>
						</div>
						<div className="row mb-5">
							<div className="col-md-12">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
				            		<div className="card-header border-0 pt-5">
										<div className="card-title font-weight-bolder">
											<div className="card-label">Avg Resolution Time (in Mins) for Manual Cases By Analyst</div>
										</div>
										<div className="card-toolbar"></div>
									</div>
						            <div className="card-body p-0">
						                <MultipleLineChart chartData={resolutionTimeData}/>
				            		</div>
			        			</div>
				            </div>
						</div>
						<div className="row mb-5">
							<div className="col-md-12">
				            	<div className={`card card-custom gutter-b card-stretch p-5`}>
						            <div className="card-body chart-table p-0">
						            	<section className="table-card">
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption className="txt-center">Date Analyst</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption className="txt-center">2/14/2022</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption className="txt-center">2/14/2022</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption className="txt-center">2/14/2022</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption className="txt-center">2/14/2022</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption>2/14/2022</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										  	<div className="table-content">
										  		<table className="mtable">
										  			<caption>2/14/2022</caption>
										  			<thead>
										  				<tr>
											  				<th>Total Cases</th>
											  				<th>Approved</th>
											  				<th>Rejected</th>
											  				<th>Open</th>
											  				<th>Hold</th>
											  				<th>Resolution Time in (Mins)</th>
											  			</tr>
										  			</thead>
										  			<tbody>
										  				<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
											  			<tr>
											  				<td>34</td>
											  				<td>3</td>
											  				<td>4</td>
											  				<td>23</td>
											  				<td>12</td>
											  				<td>28</td>
											  			</tr>
										  			</tbody>
										  			<tfoot>
										  				<tr>
											  				<th>34</th>
											  				<th>3</th>
											  				<th>4</th>
											  				<th>23</th>
											  				<th>12</th>
											  				<th>28</th>
											  			</tr>
										  			</tfoot>
										  		</table>
										  	</div>
										</section>
				            		</div>
			        			</div>
				            </div>
						</div>
					</>
				)}
			<div className="row">
				<div className="col-md-12">
					<div className="row">
						<div className="col-md-12">
							{ page==="one" || page===null? (
								<a
							    	onClick={()=>onPageChange("two")}
				                  	className='btn btn-sm btn-light-primary btn-responsive font-5vw'>
	                  					<span className="svg-icon svg-icon-3">
	                    					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
	                      						<rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
	                      						<rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
	                    					</svg>
	                  					</span>
				                  	Next
				                </a>
								) : (
									<a
								    	onClick={()=>onPageChange("one")}
					                  	className='btn btn-sm btn-light-primary btn-responsive font-5vw'>
		                  					<span className="svg-icon svg-icon-3">
		                    					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
		                      						<rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
		                      						<rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
		                    					</svg>
		                  					</span>
					                  	Back
					                </a>
								)
							}
			            </div>
					</div>
	            </div>
			</div>
		</>
	)
}


export default DemoListCharts;
