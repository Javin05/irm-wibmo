import { USER_MANAGEMENT_ERROR, REGEX } from "../../utils/constants";
import _ from 'lodash'

export const userRoleValidation = (values, setErrors) => {
  const errors = {};
  if (!values.ruleGroupName) {
    errors.ruleGroupName = 'Rule Group Name is required.'
  }
  if (!values.status) {
    errors.status = 'Status is required.'
  }

  if (!values.condition) {
    errors.condition = 'Condition is required.'
  }
  if (!values.description) {
    errors.description = 'Description is required.'
  }
  if (!values.action) {
    errors.action = 'Action is required.'
  }
//   if (!values.displayRuleLogic) {
//     errors.displayRuleLogic = 'Display Rule Logic is required.'
//   }
  if (_.isEmpty(values.customRules)) {
    errors.customRules = 'Custom Rules is required.'
  }

  setErrors(errors);
  return errors;
};
