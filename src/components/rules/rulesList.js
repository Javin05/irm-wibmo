import React, { useEffect, useState } from 'react'
import { rulesActions } from '../../store/actions'
import { connect } from 'react-redux'
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import { warningAlert, confirmationAlert } from "../../utils/alerts"


function RulesList(props) {
  const {
    getRuleslistDispatch,
    deleteRulesListDispatch,
    className,
    rules,
    loading,
    DeleteRules
  } = props
  const [limit, setLimit] = useState(25)

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getRuleslistDispatch(params)
  }, [])
  const queuesLists = [
    {
      _id: "1",
      number: "1",
      ruleId: "2748347",
      ruleField: "762",
      ruleDescription: "3",
      dateRecived: "thu may 4 2007",
      action: "20.0"
    }
  ];
  // useEffect(() => {
  //   if (DeleteRules && DeleteRules.status === STATUS_RESPONSE.SUCCESS_MSG) {
  //     confirmationAlert(
  //       'success',
  //       DeleteRules && DeleteRules.message,
  //       'success',
  //       'Back to Queues',
  //       'Ok',
  //       () => { onConfirm() },
  //       () => { clear()}
  //       )      
  //       PostclearRules()
  //   } else if (DeleteRules && DeleteRules.status === STATUS_RESPONSE.ERROR_MSG) {
  //     warningAlert(
  //       'error',
  //       DeleteRules && DeleteRules.message,
  //       '',
  //       'Try again',
  //       '',
  //       () => { clear()}
  //     )
  //     PostclearRules()
  //   }
  // }, [DeleteRules])


  return (
    <>
      <div>
        <div className="table-responsive mx-5">
          <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
            <thead>
              <tr className="fw-bold fs-6 text-gray-800">
                <th className=" text-start">
                  <div className="d-flex">
                    <span>S.No</span>

                  </div>
                </th>
                <th className=" text-start">
                  <div className="d-flex">
                    <span>Rule ID</span>
                    <div className="min-w-25px text-end">
                    </div>
                  </div>
                </th>
                <th className=" text-center">
                  <div className="d-flex">
                    <span>Rule Field</span>
                    <div className="min-w-25px text-end">

                    </div>
                  </div>
                </th>
                <th className=" text-start">
                  <div className="d-flex">
                    <span>Rule description</span>
                    <div className="min-w-25px text-end">

                    </div>
                  </div>
                </th>
                <th className=" text-start">
                  <div className="d-flex">
                    <span>Action</span>
                    <div className="min-w-25px text-end">

                    </div>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {
                !loading
                  ? (
                    rules &&
                      rules
                      ? (
                        rules.map((rule, _id) => {
                          return (
                            <tr
                              key={_id}
                              style={
                                _id === 0
                                  ? { borderColor: "black" }
                                  : { borderColor: "white" }
                              }
                            >
                              <td className="pb-0 pt-5  text-start">
                                {/* <Link to={`/merchant/${rule._id}`} className="pb-0 pt-5  text-start"> */}
                                {/* <Link to={`/risk-summary/${rule.number}`} className="pb-0 pt-5  text-start"> */}
                                {rule.number ? rule.number : "--"}
                              </td>
                              <td className="pb-0 pt-5  text-start">
                                {rule.ruleId ? rule.ruleId : "--"}
                              </td>
                              <td className="pb-0 pt-5  text-start">
                                {rule.ruleField ? rule.ruleField : "--"}
                              </td>
                              <td className="pb-0 pt-5  text-start">
                                {rule.ruleDescription ? rule.ruleDescription : "--"}
                              </td>
                              <td className="pb-0 pt-5  text-start">
                                {rule.action ? rule.action : "--"}
                              </td>
                              <td className="pb-0 pt-5  text-start">
                                <a title="Edit customer"
                                  className=" text-start btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                                >
                                  <span className="svg-icon svg-icon-danger svg-icon-2x">
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                      <g stroke="none" strokeWidth="1" fill="none" >
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) " />
                                        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1" />
                                      </g>
                                    </svg></span>
                                </a>

                                <a title="Delete customer"
                                  className="btn btn-icon btn-light btn-hover-danger btn-sm"
                                >
                                  <span className="svg-icon svg-icon-primary svg-icon-2x">
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                      <g stroke="none" strokeWidth="1" fill="none" >
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M6,8 L18,8 L17.106535,19.6150447 C17.04642,20.3965405 16.3947578,21 15.6109533,21 L8.38904671,21 C7.60524225,21 6.95358004,20.3965405 6.89346498,19.6150447 L6,8 Z M8,10 L8.45438229,14.0894406 L15.5517885,14.0339036 L16,10 L8,10 Z" fill="#000000" />
                                        <path d="M14,4.5 L14,3.5 C14,3.22385763 13.7761424,3 13.5,3 L10.5,3 C10.2238576,3 10,3.22385763 10,3.5 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                                      </g>
                                    </svg></span>
                                </a>
                              </td>

                            </tr>
                          )
                        })
                      )
                      : (
                        <tr className='text-center py-3'>
                          <td colSpan='6'>No record(s) found</td>
                        </tr>
                      )
                  )
                  :
                  (
                    <tr>
                      <td colSpan='6' className='text-center'>
                        <div className='spinner-border text-primary m-5' role='status' />
                      </td>
                    </tr>
                  )
              }
            </tbody>
          </table>
        </div >
      </div >
    </>
  );
}


const mapStateToProps = state => {
  const { queuesAddStore, inputFieldsStore, rulesStore
  } = state
  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    loading: state && state.rulesStore && state.rulesStore.loading,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : '',
  }
}

const mapDispatchToProps = dispatch => ({
  getRuleslistDispatch: (params) => dispatch(rulesActions.getRules(params))
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RulesList);
