import React, { useEffect, useState, useRef } from 'react'
import _, { values } from 'lodash'
import {
  playStoreReportSettingActions,
  playStoreReportSettingAddAction,
  playStoreReportSettingGetIdActions,
  updateplayStoreReportSettingActions,
  playStoreReportSettingDeleteActions
} from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper'
import { KTSVG } from '../../theme/helpers'
import FindRole from '../wrmManagement/Role'
import {
  SWEET_ALERT_MSG,
  STATUS_BADGE,
  STATUS_RESPONSE
} from '../../utils/constants'
import Modal from 'react-bootstrap/Modal'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"

function PlayStoreReport(props) {
  const {
    getplayStoreReportSettinglistDispatch,
    playStoreReportSettingslists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    playStoreReportSettingAddDispatch,
    playStoreReportSettingsAdd,
    addLoading,
    clearplayStoreReportSettingAddDispatch,
    getplayStoreReportSettingIdDetailsDispatch,
    playStoreReportSettingsIdDetail,
    clearplayStoreReportSettingIdDetailsDispatch,
    updateLoading,
    updateplayStoreReportSettingDispatch,
    updateplayStoreReportSettingResponce,
    clearupdateplayStoreReportSettingDispatch,
    playStoreReportSettingDeleteDispatch,
    clearplayStoreReportSettingDeleteDispatch,
    DeleteplayStoreReportSetting
  } = props

  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [show, setShow] = useState(false)
  const [searchShow, setSearchShow] = useState(false)
  const [editshow, seteditshow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
  })
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    report_key: "",
    report_value: "",
    report_type: "Playstore Report"
  })
  const [searchData, setSearchData] = useState({
    report_key: "",
    report_value: "",
    report_type: "Playstore Report"
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
      report_type: "Playstore Report"
    }
    getplayStoreReportSettinglistDispatch(params)
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      const pickByParams = _.pickBy(params)
      getplayStoreReportSettinglistDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
    }
  }, [setFilterFunction, setCredFilterParams])


  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      report_key: searchData.report_key ? searchData.report_key : '',
      report_value: searchData.report_value ? searchData.report_value : '',
      report_type: searchData.report_type ? searchData.report_type : '',
    }
    setActivePageNumber(pageNumber)
    getplayStoreReportSettinglistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getplayStoreReportSettinglistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getplayStoreReportSettinglistDispatch(params)
    }
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const handelChange = (event) => {
    const { name, value } = event.target
    setFormData(values => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const searchChange = (event) => {
    const { name, value } = event.target
    setSearchData(values => ({ ...values, [name]: value }))
  }

  const Search = () => {
    const params = {
      limit: limit,
      page: activePageNumber,
      report_type: searchData.report_type,
      report_key: searchData.report_key,
      report_value: searchData.report_value
    }
    getplayStoreReportSettinglistDispatch(params)
    setSearchShow(false)
  }

  const Postsubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.report_key)) {
      errors.report_key = "Report Key Is Required"
    }
    if (_.isEmpty(formData.report_value)) {
      errors.report_value = "Report Value Is Required"
    }
    if (editshow) {
      if (_.isEmpty(errors)) {
        const data = playStoreReportSettingsIdDetail && playStoreReportSettingsIdDetail.data
        updateplayStoreReportSettingDispatch(data && data._id, formData)
      }
    } else {
      if (_.isEmpty(errors)) {
        playStoreReportSettingAddDispatch(formData)
      }
    }
    setErrors(errors)
  }

  const onEditItm = (data) => {
    const params = {
      id: data,
      report_type: "Playstore Report",
    }
    getplayStoreReportSettingIdDetailsDispatch(params)
    setShow(true)
  }

  useEffect(() => {
    if (playStoreReportSettingsAdd && playStoreReportSettingsAdd.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        playStoreReportSettingsAdd && playStoreReportSettingsAdd.message,
        'success'
      )
      clearplayStoreReportSettingAddDispatch()
      setShow(false)
      setFormData({
        report_key: '',
        report_value: ''
      })
      const params = {
        limit: limit,
        page: activePageNumber,
        report_type: "Playstore Report"
      }
      getplayStoreReportSettinglistDispatch(params)
    } else if (playStoreReportSettingsAdd && playStoreReportSettingsAdd.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        playStoreReportSettingsAdd && playStoreReportSettingsAdd.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearplayStoreReportSettingAddDispatch()
    }
  }, [playStoreReportSettingsAdd])

  useEffect(() => {
    if (playStoreReportSettingsIdDetail && playStoreReportSettingsIdDetail.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = playStoreReportSettingsIdDetail && playStoreReportSettingsIdDetail.data
      setFormData(values => ({
        ...values,
        report_key: data && data.report_key,
        report_value: data && data.report_value
      }))
      seteditshow(true)
    }
  }, [playStoreReportSettingsIdDetail])


  useEffect(() => {
    if (updateplayStoreReportSettingResponce && updateplayStoreReportSettingResponce.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        updateplayStoreReportSettingResponce && updateplayStoreReportSettingResponce.message,
        'success'
      )
      clearupdateplayStoreReportSettingDispatch()
      setShow(false)
      seteditshow(false)
      setFormData({
        report_key: '',
        report_value: ''
      })
      const params = {
        limit: limit,
        page: activePageNumber,
        report_type: "Playstore Report"
      }
      getplayStoreReportSettinglistDispatch(params)
    } else if (updateplayStoreReportSettingResponce && updateplayStoreReportSettingResponce.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        updateplayStoreReportSettingResponce && updateplayStoreReportSettingResponce.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearupdateplayStoreReportSettingDispatch()
    }
  }, [updateplayStoreReportSettingResponce])


  const onConfirmUpdate = (params) => {
    const data = {
      id: params,
      report_type: "Playstore Report"
    }
    playStoreReportSettingDeleteDispatch(data)
  }

  const onDeleteItem = (params) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_REPORT,
      'warning',
      'Yes',
      'No',
      () => { onConfirmUpdate(params) },
      () => { }
    )
  }

  useEffect(() => {
    if (DeleteplayStoreReportSetting && DeleteplayStoreReportSetting.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        DeleteplayStoreReportSetting && DeleteplayStoreReportSetting.message,
        'success'
      )
      clearplayStoreReportSettingDeleteDispatch()
      setShow(false)
      seteditshow(false)
      setFormData({
        report_key: '',
        report_value: ''
      })
      const params = {
        limit: limit,
        page: activePageNumber,
        report_type: "Playstore Report"
      }
      getplayStoreReportSettinglistDispatch(params)
    } else if (DeleteplayStoreReportSetting && DeleteplayStoreReportSetting.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DeleteplayStoreReportSetting && DeleteplayStoreReportSetting.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearplayStoreReportSettingDeleteDispatch()
    }
  }, [DeleteplayStoreReportSetting])

  const clear = () => {
    setShow(false)
    setFormData(values => ({
      ...values,
      report_key: '',
      report_value: ''
    }))
    clearplayStoreReportSettingIdDetailsDispatch()
    seteditshow(false)
  }

  const totalPages =
    playStoreReportSettingslists && playStoreReportSettingslists.data && playStoreReportSettingslists.data.count
      ? Math.ceil(parseInt(playStoreReportSettingslists && playStoreReportSettingslists.data && playStoreReportSettingslists.data.count) / limit)
      : 1
  return (
    <>
      <Modal
        show={searchShow}
        size="lg"
        centered
        onHide={() => {
          setSearchShow(false)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(167 199 230), rgb(188 204 237))", "color": "#ffffff" }}
          closeButton={() => setSearchShow(false)}>
          <Modal.Title>Search</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Key :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => searchChange(e)}
                  name='report_key'
                  value={searchData.report_key}
                  placeholder='Report Key'
                  className="form-control form-control-solid" />
                {errors && errors.report_key && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_key}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Value :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => searchChange(e)}
                  name='report_value'
                  value={searchData.report_value}
                  placeholder='Report Value'
                  className="form-control form-control-solid" />
                {errors && errors.report_value && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_value}
                  </div>
                )}
              </div>
            </div>

            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <div className='d-flex justify-content-end'>
                  <button
                    className='btn btn-sm btn-light-primary btn-responsive me-2 mt-4'
                    onClick={Search}
                  >
                    Search
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => {
          setShow(false)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(167 199 230), rgb(188 204 237))", "color": "#ffffff" }}
          closeButton={() => clear()}>
          <Modal.Title>{editshow ? "Update" : "Add"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Key :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => handelChange(e)}
                  name='report_key'
                  value={formData.report_key}
                  placeholder='Report Key'
                  className="form-control form-control-solid" />
                {errors && errors.report_key && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_key}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Value :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => handelChange(e)}
                  name='report_value'
                  value={formData.report_value}
                  placeholder='Report Value'
                  className="form-control form-control-solid" />
                {errors && errors.report_value && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_value}
                  </div>
                )}
              </div>
            </div>

            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <div className='d-flex justify-content-end'>
                  {
                    editshow ? (
                      <button
                        className='btn btn-sm btn-light-primary btn-responsive me-2 mt-4'
                        onClick={Postsubmit}
                        disabled={updateLoading}
                      >
                        {!updateLoading && <span className='indicator-label'>
                          Update
                        </span>
                        }
                        {updateLoading && (
                          <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    ) : (
                      <button
                        className='btn btn-sm btn-light-primary btn-responsive me-2 mt-4'
                        onClick={Postsubmit}
                        disabled={addLoading}
                      >

                        {!addLoading && <span className='indicator-label'>
                          Submit
                        </span>
                        }
                        {addLoading && (
                          <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className='card'>
        <div className="row">
          <div className='col-md-3'>
            {playStoreReportSettingslists && playStoreReportSettingslists.data && playStoreReportSettingslists.data.count && (
              <span className='text-muted fw-bold d-flex fs-3 mt-2 ms-4'>
                Total:
                <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                  {playStoreReportSettingslists && playStoreReportSettingslists.data && playStoreReportSettingslists.data.count}
                </span>
              </span>
            )}
          </div>
          <div className='col-md-9 mt-4'>
            <div className="d-flex justify-content-end">
              {
                <FindRole
                  role={Role}
                >
                  <button
                    className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                    onClick={() => setShow(true)}
                  >
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    Add playStoreReport
                  </button>
                </FindRole>
              }
              <button
                className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                onClick={() => setSearchShow(true)}
              >
                <KTSVG path='/media/icons/duotune/general/gen021.svg' />
                Search
              </button>
            </div>
          </div>
        </div>
        <div className='card-body py-3'>
          <div className="table-responsive">
            <table className="table gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>S.No</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Report Key</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Report Value</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-200px text-start">
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                  </FindRole>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      playStoreReportSettingslists &&
                        playStoreReportSettingslists.data &&
                        _.isArray(playStoreReportSettingslists.data.result)
                        ? (
                          playStoreReportSettingslists.data.result.map((item, i) => {
                            return (
                              <tr
                                key={i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="pb-0 pt-5  text-start">
                                  {i + 1}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item && item.report_key ? item.report_key : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item && item.report_value ? item.report_value : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  <span className={`badge ${STATUS_BADGE[item.status]}`}>
                                    {item && item.status ? item.status : "--"}
                                  </span>
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td className="pb-0 pt-5  text-start">
                                    <button
                                      className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
                                      onClick={(e) => onEditItm(item._id)}
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                        title="Edit BlockList"
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                      onClick={() => onDeleteItem(item._id)}
                                      title="Delete BlockList"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                </FindRole>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { playStoreReportSettingslistStore, playStoreReportSettingsAddStore, updateplayStoreReportSettingStore, DeleteplayStoreReportSettingStore } = state
  return {
    playStoreReportSettingslists: state && state.playStoreReportSettingslistStore && state.playStoreReportSettingslistStore.playStoreReportSettingslists,
    loading: playStoreReportSettingslistStore && playStoreReportSettingslistStore.loading ? playStoreReportSettingslistStore.loading : false,
    playStoreReportSettingsAdd: state && state.playStoreReportSettingsAddStore && state.playStoreReportSettingsAddStore.playStoreReportSettingsAdd,
    addLoading: playStoreReportSettingsAddStore && playStoreReportSettingsAddStore.loading ? playStoreReportSettingsAddStore.loading : false,
    playStoreReportSettingsIdDetail: state && state.editplayStoreReportSettingsStore && state.editplayStoreReportSettingsStore.playStoreReportSettingsIdDetail,
    updateLoading: updateplayStoreReportSettingStore && updateplayStoreReportSettingStore.loading ? updateplayStoreReportSettingStore.loading : false,
    updateplayStoreReportSettingResponce: updateplayStoreReportSettingStore && updateplayStoreReportSettingStore.updateplayStoreReportSettingResponce ? updateplayStoreReportSettingStore.updateplayStoreReportSettingResponce : '',
    DeleteplayStoreReportSetting: DeleteplayStoreReportSettingStore && DeleteplayStoreReportSettingStore.DeleteplayStoreReportSetting ? DeleteplayStoreReportSettingStore.DeleteplayStoreReportSetting : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getplayStoreReportSettinglistDispatch: (params) => dispatch(playStoreReportSettingActions.getplayStoreReportSettinglist(params)),
  playStoreReportSettingAddDispatch: (params) => dispatch(playStoreReportSettingAddAction.playStoreReportSettingAdd(params)),
  clearplayStoreReportSettingAddDispatch: (params) => dispatch(playStoreReportSettingAddAction.clearplayStoreReportSetting(params)),
  getplayStoreReportSettingIdDetailsDispatch: (params) => dispatch(playStoreReportSettingGetIdActions.getplayStoreReportSettingIdDetails(params)),
  clearplayStoreReportSettingIdDetailsDispatch: (params) => dispatch(playStoreReportSettingGetIdActions.clearplayStoreReportSettingIdDetails(params)),
  updateplayStoreReportSettingDispatch: (id, params) => dispatch(updateplayStoreReportSettingActions.updateplayStoreReportSetting(id, params)),
  clearupdateplayStoreReportSettingDispatch: (id, params) => dispatch(updateplayStoreReportSettingActions.clearupdateplayStoreReportSetting(id, params)),
  playStoreReportSettingDeleteDispatch: (id) => dispatch(playStoreReportSettingDeleteActions.playStoreReportSettingDelete(id)),
  clearplayStoreReportSettingDeleteDispatch: (id) => dispatch(playStoreReportSettingDeleteActions.clearplayStoreReportSettingDelete(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(PlayStoreReport)
