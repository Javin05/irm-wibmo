import React, {useState, useEffect} from 'react';
import { Modal, Row, Col, Form } from 'react-bootstrap'
import { Formik, Field, Form as Formikform, ErrorMessage } from 'formik'
import Select from "react-select";
import * as Yup from 'yup';

const WatchlistModal=(props)=> {
    const {
        modalType, 
        editModal, 
        editModaltoggle, 
        QueueOptions, 
        WatchOptions,
        formSubmitHandler,
        editRow
    } = props

    const [fieldLabel, setFieldLabel] = useState("--")
    const [queueOption, setQueueOption] = useState(null)
    const [whiteOption, setWhiteOption] = useState(null)

    const initialValues = {
        queueId: editRow? editRow.queueId : '',
        fieldName: editRow? editRow.fieldName : '',
        fieldType: editRow? editRow.fieldType : '',
        watchItems: editRow? editRow.fieldValue : ''
    }

    const validationSchema = Yup.object().shape({
        queueId: Yup.string()
            .required('QueueID is required'),
        fieldName: Yup.string()
            .required('Field Name is required'),
        watchItems: Yup.string()
            .required('White Items is required')
    })

    const setFieldName = (e, fName) =>{
        setFieldLabel(e.label)
    }
    function onSubmit(fields, { setStatus, setSubmitting }) {
        setStatus();
        /*
        Multiple values
         */
        let filtered_array = [];
        if(fields.fieldType!=="shipping_address"){
            let items = fields.watchItems.split(',');
            items.forEach(element => {
                let value = element.trim();
                filtered_array.push({
                    fieldValue: value,
                });
            });
        }

        /*
        Submit Forms
        */
        let formData = {
            "queueId": fields.queueId,
            "fieldName": fields.fieldName,
            "fieldType": fields.fieldType, 
            "watchItems": fields.fieldType==="shipping_address"? [
                {
                  "fieldValue": fields.watchItems
                }
            ] : filtered_array
        }

        let data = {
           "fieldValue": fields.watchItems
        }

        let rowId = editRow? editRow._id : null;
        formSubmitHandler(rowId, editRow!==null? data : formData)
        setSubmitting(false)
    }

    useEffect(() => {
        if(editRow!==null){
            let QueueOption = QueueOptions.filter(t=>t.value === editRow.queueId);
            let WhiteOption = WatchOptions.filter(w=>w.label === editRow.fieldName);
            setQueueOption(QueueOption[0])
            setWhiteOption(WhiteOption[0])
            setFieldLabel(editRow.fieldName)
        }
        else{
            setQueueOption(null)
            setWhiteOption(null)
            setFieldLabel("--")
        }
    }, [editRow])

    return (
        <Modal
            show={editModal}
            onHide={() => {
                editModaltoggle(false)
                setQueueOption(null)
                setWhiteOption(null)
                setFieldLabel("--")
            }}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            className="custom-modal"
            centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                   { modalType==="create"? "New Watchlist" : "Update Watchlist"}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Formik 
                    //initialValues={initialValues} 
                    //validationSchema={validationSchema} 
                    onSubmit={onSubmit}>
                    {({ errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm }) => {                    
                        return (
                            <Formikform id="signUpForm" className="signinupForm" noValidate>
                                <Field type="hidden" name="fieldType" />
                                <Form.Group as={Row} className="mb-3" controlId="queueId">
                                    <Form.Label column sm="3">
                                        Queue ID :
                                    </Form.Label>
                                    <Col sm="9">
                                        <Select
                                            name="queueId"
                                            onBlur={() => setFieldTouched("queueId", true)}
                                            onChange={(opt, e) => {
                                                setFieldValue("queueId", opt.value);
                                                setQueueOption(opt)
                                            }}
                                            value={queueOption}
                                            options={QueueOptions}
                                            error={errors.queueId}
                                            touched={touched.queueId}
                                        />
                                        <ErrorMessage name="queueId" component="div" className="invalid-feedback" />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3" controlId="fieldName">
                                    <Form.Label column sm="3">
                                        Watch List : 
                                    </Form.Label>
                                    <Col sm="9">
                                        <Select
                                            name="fieldName"
                                            onBlur={() => setFieldTouched("fieldName", true)}
                                            onChange={(opt, e) => {
                                                setFieldValue("fieldName", opt.label);
                                                setFieldValue("fieldType", opt.fieldType);
                                                setFieldLabel(opt.label)
                                                setWhiteOption(opt)
                                            }}
                                            value={whiteOption}
                                            options={WatchOptions}
                                            error={errors.fieldName}
                                            touched={touched.fieldName}
                                        />
                                        <ErrorMessage name="fieldName" component="div" className="invalid-feedback" />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3" controlId="fieldName">
                                    <Form.Label column sm="3">
                                        {fieldLabel} : 
                                    </Form.Label>
                                    <Col sm="9">
                                        <Field 
                                        name="watchItems" 
                                        as="textarea" 
                                        className={'form-control' + (errors.watchItems && touched.watchItems ? ' is-invalid' : '')} />
                                        <ErrorMessage name="watchItems" component="div" className="invalid-feedback" />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3" controlId="submitBtn">
                                    <Form.Label column sm="3">
                                        &nbsp;
                                    </Form.Label>
                                    <Col sm="9">
                                        <button 
                                            type="submit" 
                                            disabled={isSubmitting} 
                                            className="btn btn-primary">
                                            {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            {modalType==="create"? "Add Watchlist" : "Update Watchlist"}
                                        </button>    
                                    </Col>
                                </Form.Group>
                            </Formikform>
                        );
                    }}
                </Formik>
            </Modal.Body>
        </Modal>
    );
}
 
export default WatchlistModal;