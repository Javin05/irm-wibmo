import React, { useState, useEffect } from 'react'
import { toAbsoluteUrl } from "../../theme/helpers"
import { connect } from 'react-redux'
import { STATUS_RESPONSE, SWEET_ALERT_MSG, STATUS_BADGE, DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT } from '../../utils/constants'
import {
    WatchListTypes,
    WatchListActions,
    queuesActions
} from '../../store/actions'
import ReactPaginate from 'react-paginate'
import Icofont from 'react-icofont'
import Select from 'react-select'
import WatchlistModal from './WatchlistModal'
import WatchListTable from './WatchListTable'
import { 
    successAlert, 
    warningAlert, 
    confirmationAlert 
} from "../../utils/alerts"
 
function Watchlist(props) {
    const {
        fetchQueueslistDispatch,
        fetchWatchlistDispatch, 
        fetchWatchlistTypeDispatch,
        postWatchlistTypeDispatch,
        updateWatchlistTypeDispatch,
        deleteWatchlistTypeDispatch,
        loading,
        watchlistData,
        watchlistCount,
        QueuesList,
        watchlistTypeData,
        createWatchlistTypeResponse,
        updateWatchlistTypeResponse,
        deleteWatchlistTypeResponse
    } = props

    const recordOptions = [
        { label:25, value: 25},
        { label:50, value: 50},
        { label:75, value: 75},
        { label:100, value: 100}
    ]
    const [currentPage, setCurrentPage] = useState(1)
    const [pageRecords, setPageRecords] = useState(25)
    const [records, setRecords] = useState(recordOptions)
    const [modalType, setModalType] = useState("create")
    const [editModal, setEditModal] = useState(false)
    const [queue, setQueue] = useState(localStorage.getItem('WatchQueue') !== null ? localStorage.getItem('WatchQueue') : "ATO_Q");
    const [watchlist, setWatchlist] = useState(localStorage.getItem('WatchList') !== null ? localStorage.getItem('WatchList') : "Email Address");
    const [watchOptions, setWatchOptions] = useState(null)
    const [queueOptions, setQueueOptions] = useState(null)
    const [defaultWatchlist, setDefaultWatchlist] = useState(null)
    const [defaultQueue, setDefaultQueue] = useState(null)
    const [editRow, setEditRow] = useState(null)
    const totalPages = watchlistCount && Math.ceil(parseInt(watchlistCount) / pageRecords)

    const editModaltoggle = (status) =>{
        setEditRow(null)
        setModalType("create")
        setEditModal(status)
    }
    const editHandler = (e, row) =>{
        setEditRow(row)
        setModalType("edit")
        setEditModal(true)
    }
    const getSearchParams = (label) =>{
        let oneWatchList = watchOptions && watchOptions.filter(wo=>wo.label === label);
        if(oneWatchList !== null && oneWatchList.length>0){
            return oneWatchList[0].fieldType
        }
    }
    const filterHandler = (e, fiterType) =>{
        if(fiterType==="queue"){
            setDefaultQueue(e)            
            localStorage.setItem('WatchQueue', e.label);

            const params = {
                queueId : e.value,
                fieldType : getSearchParams(defaultWatchlist.label),
                limit: pageRecords,
                page: currentPage
            }
            fetchWatchlistTypeDispatch(params)
        }
        else if(fiterType==="watchlist"){
            setDefaultWatchlist(e)
            localStorage.setItem('WatchList', e.label);
            let fieldType= getSearchParams(e.label)

            const params = {
                queueId : defaultQueue.value,
                fieldType : fieldType,
                limit: pageRecords,
                page: currentPage
            }
            fetchWatchlistTypeDispatch(params)
        }  
    }
    const formSubmitHandler = (id, formData) =>{
        if(id===null){
            postWatchlistTypeDispatch(formData)
        }
        else{
            updateWatchlistTypeDispatch(id, formData)
        }
    }
    const deleteHandler = (e, id) =>{
        e.preventDefault()
        confirmationAlert(
            SWEET_ALERT_MSG.CONFIRMATION_TEXT,
            SWEET_ALERT_MSG.DELETE_WATCHLIST,
            'warning',
            'Yes',
            'No',
            () => { 
                let label = getSearchParams(defaultWatchlist)
                const params = {
                    fieldType : label===undefined? 'Email' : label,
                    limit: pageRecords,
                    page: currentPage
                }
                deleteWatchlistTypeDispatch(id, params)
            }
        )
    }
    const handleRecordPerPage = (e) =>{
        e.preventDefault()
        setPageRecords(e.target.value)
    }
    const handlePageClick = (event) =>{
        const pageNumber = event.selected + 1
        setCurrentPage(pageNumber)
    }
    const getCurrentOption = (pageRecords) =>{
        let rec = records && records.filter(r=>r.value === pageRecords)
        return rec[0] 
    }
    const onReDispatch = () =>{
        const params = {
            fieldType : getSearchParams(defaultWatchlist.label),
            queueId : defaultQueue.value,
            limit: pageRecords,
            page: currentPage
        }
        fetchWatchlistTypeDispatch(params)
    }
    
    /*
    Dispatches
    */
    useEffect(() => {
        fetchWatchlistDispatch()
        fetchQueueslistDispatch()
    }, [])
    useEffect(() => {
        let fieldType = defaultWatchlist && getSearchParams(defaultWatchlist.label)
        let queueId = defaultQueue && defaultQueue.value
        const params = {
            fieldType : fieldType,
            queueId: queueId,
            limit: pageRecords,
            page: currentPage
        }
        fetchWatchlistTypeDispatch(params)
    }, [defaultWatchlist, defaultQueue, pageRecords, currentPage])

    /*
    Watchlist Data
    */
    useEffect(() => {
        let watchTemp = [];
        if(watchlistData){
            Object.keys(watchlistData).forEach(function(key) {
                watchTemp.push({
                    "value":watchlistData[key]._id,
                    "label":watchlistData[key].fieldName,
                    "fieldType":watchlistData[key].fieldType,
                });
            });

            setWatchOptions(watchTemp)

            // set selected option ... from localstorage
            if(watchlist){
                let oneWatchlist = watchTemp.filter(w=>w.label === watchlist);
                setDefaultWatchlist(oneWatchlist[0])
            }
            else{
                setDefaultWatchlist(watchTemp[0])
            }
        }
    }, [watchlistData])

    /*
    Queue Data
    */
    useEffect(() => {
        let queueTemp = [];
        if(QueuesList){
            Object.keys(QueuesList).forEach(function(key) {
                queueTemp.push({
                    "value":QueuesList[key]._id,
                    "label":QueuesList[key].queueName
                });
            });

            setQueueOptions(queueTemp)
            // set selected option ... from localstorage
            if(queue){
                let oneQueue = queueTemp.filter(q=>q.label === queue);
                setDefaultQueue(oneQueue[0])
            }
            else{
                setDefaultQueue(queueTemp[0])
            } 
        }
    }, [QueuesList])

    /*
    Create Success
    */
    useEffect(() => {
        if(createWatchlistTypeResponse && createWatchlistTypeResponse==="ok"){
            setEditModal(false)
            confirmationAlert(
                'success', 'Created Successfully',
                'success',
                'Back to Watchlist',
                'Ok',
                () => { 
                    onReDispatch()
                },
                () => { 
                    onReDispatch()
                }
            )
        }
    }, [createWatchlistTypeResponse])

    /*
    Update Success
    */
    useEffect(() => {
        if(updateWatchlistTypeResponse && updateWatchlistTypeResponse==="ok"){
            setEditModal(false)
            confirmationAlert(
                'success', 'Updated Successfully',
                'success',
                'Back to Watchlist',
                'Ok',
                () => { 
                    onReDispatch()
                },
                () => { 
                    onReDispatch()
                }
            )
        }
    }, [updateWatchlistTypeResponse])

    /*
    delete Success
    */
    useEffect(() => {
        if(deleteWatchlistTypeResponse && deleteWatchlistTypeResponse==="ok"){
            onReDispatch()
        }
    }, [deleteWatchlistTypeResponse])

    return (
        <>
            <div className={`card card-custom card-stretch gutter-b`}>
                <div className="card-header border-0 pt-5">
                    <h3 className="card-title align-items-start flex-column">
                        <span className="text-muted font-size-sm">Record(s) per Page : &nbsp;{' '}</span>
                        <div>
                            <select
                                className='form-select w-6rem'
                                data-control='select'
                                data-placeholder='Select an option'
                                data-allow-clear='true'
                                onChange={(e) => handleRecordPerPage(e)}>
                                <option value='25' selected={pageRecords===25? true : false}>25</option>
                                <option value='50' selected={pageRecords===50? true : false}>50</option>
                                <option value='75' selected={pageRecords===75? true : false}>75</option>
                                <option value='100' selected={pageRecords===100? true : false}>100</option>
                            </select>
                        </div>     
                    </h3>
                    <div className="card-toolbar">
                        <div className="mr-2">
                            <span className="text-muted font-size-sm">Queue</span>
                            <Select 
                                placeholder="Queue" 
                                options={queueOptions} 
                                value={defaultQueue}
                                onChange={(e)=>filterHandler(e, "queue")}
                                style={{width:"150px"}}
                                />
                        </div>
                        <div className="mr-2">
                        <span className="text-muted font-size-sm">Watchlist</span>
                            <Select 
                                placeholder="Watchlist" 
                                options={watchOptions} 
                                value={defaultWatchlist}
                                onChange={(e)=>filterHandler(e, "watchlist")}
                                style={{width:"150px"}}
                                />
                        </div>
                        <div>
                        <div className="text-muted font-size-sm">&nbsp;</div>
                            <button
                                className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
                                onClick={() =>editModaltoggle(true)}>
                                <span className="svg-icon svg-icon-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                                        <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                Add Watchlist
                            </button>
                        </div>
                    </div>
                </div>
                <div className="card-body pt-2 pb-0 pl-5 pr-5">
                    {
                        (() => {
                            if (!loading) {
                                if (watchlistTypeData && watchlistTypeData.length > 0) {
                                    return (
                                        <div className="table-responsive white-wrapper">
                                            <WatchListTable 
                                            tableData={watchlistTypeData}
                                            editHandler={editHandler}
                                            deleteHandler={deleteHandler}
                                            pageRecords={pageRecords}
                                            currentPage={currentPage}/>
                                            <ReactPaginate
                                              nextLabel="Next >"
                                              onPageChange={handlePageClick}
                                              pageRangeDisplayed={3}
                                              marginPagesDisplayed={2}
                                              pageCount={totalPages}
                                              forcePage={currentPage - 1}
                                              previousLabel="< Prev"
                                              pageClassName="page-item"
                                              pageLinkClassName="page-link"
                                              previousClassName="page-item"
                                              previousLinkClassName="page-link"
                                              nextClassName="page-item"
                                              nextLinkClassName="page-link"
                                              breakLabel="..."
                                              breakClassName="page-item"
                                              breakLinkClassName="page-link"
                                              containerClassName="pagination"
                                              activeClassName="active"
                                              activeLinkClassName="active_a"
                                              renderOnZeroPageCount={null}
                                            />                 
                                        </div>
                                    )
                                }
                                else{
                                    return (
                                        <p className="text-center">No record(s) found</p>
                                    )
                                } 
                            } else {
                                return <div className="spinner-wrapper">
                                    <div className='spinner-border text-primary m-5'role='status'></div>
                                </div>
                            }
                        })()
                    }
                </div>
            </div>
            <WatchlistModal
                modalType={modalType} 
                editModal={editModal}
                QueueOptions={queueOptions}
                WatchOptions={watchOptions}
                editModaltoggle={editModaltoggle}
                formSubmitHandler={formSubmitHandler}
                editRow={editRow}
                />
        </>
    )
}

const mapStateToProps = state => {
    const {
        WatchListStore,
        queueslistStore
    } = state

    return {
        loading: state && state.WatchListStore && state.WatchListStore.loading,
        watchlistData: WatchListStore && WatchListStore.watchlistData ? WatchListStore.watchlistData : null,
        watchlistCount: WatchListStore && WatchListStore.watchlistCount ? WatchListStore.watchlistCount : 0,
        watchlistTypeData: WatchListStore && WatchListStore.watchlistTypeData ? WatchListStore.watchlistTypeData : null,
        createWatchlistTypeResponse: WatchListStore && WatchListStore.createWatchlistTypeResponse ? WatchListStore.createWatchlistTypeResponse : null,
        updateWatchlistTypeResponse: WatchListStore && WatchListStore.updateWatchlistTypeResponse ? WatchListStore.updateWatchlistTypeResponse : null,
        deleteWatchlistTypeResponse: WatchListStore && WatchListStore.deleteWatchlistTypeResponse ? WatchListStore.deleteWatchlistTypeResponse : null,
        QueuesList: queueslistStore && queueslistStore.queueslists?.data ? queueslistStore.queueslists?.data : null,
    }
}
const mapDispatchToProps = dispatch => ({
    fetchWatchlistDispatch: (params) => dispatch(WatchListActions.fetchWatchList(params)),
    fetchWatchlistTypeDispatch: (params) => dispatch(WatchListActions.fetchWatchListType(params)),
    postWatchlistTypeDispatch: (formData) => dispatch(WatchListActions.createWatchListType(formData)),
    updateWatchlistTypeDispatch: (id, formData) => dispatch(WatchListActions.updateWatchListType(id, formData)),
    deleteWatchlistTypeDispatch: (id, formData) => dispatch(WatchListActions.deleteWatchListType(id, formData)),
    fetchQueueslistDispatch: () => dispatch(queuesActions.getqueueslist()),
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Watchlist);