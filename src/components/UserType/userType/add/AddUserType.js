import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { useLocation, useHistory } from 'react-router-dom'
import { STATUS_RESPONSE, REGEX } from '../../../../utils/constants'
import { KTSVG } from '../../../../theme/helpers'
import {
  userTypesActions,
  addUsertypeActions,
  editUsertypeActions
} from '../../../../store/actions'
import {
  warningAlert,
  confirmAlert
} from '../../../../utils/alerts'
import { userTypeValidation } from '../../validation'
import _ from 'lodash'
import { Modal } from '../../../../theme/layout/components/modal'
import { setTypeData } from '../../formData'

const AddUsersType = (props) => {
  const {
    getUserTypeDispatch,
    addUsertypeDispatch,
    clearaddUsertypeDispatch,
    messageAddUsertype,
    statusUType,
    loadingAddUType,
    cleareditUsertypesDispatch,
    usertypeDetails,
    statusUserType,
    loadingUserType,
    editUsertypesDispatch,
    statusUpdateUsertype,
    messagesUpdateUsertype,
    loadingUpdateUserType,
    setShow,
    show,
    editMode,
    dataEUR
  } = props

  const searchName = useLocation().pathname
  const userParam = searchName && searchName.split('update-user-type/')
  const id = userParam && userParam[1]
  const [errors, setErrors] = useState({})
  const [formData, setFormData] = useState({
    userType: '',
    tag:"IRM"
  })

  const resetState = () => {
    setFormData({
      userType: ''
    })
  }

  const handleChange = (e) => {
    e.persist()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = userTypeValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
      const Userdata = usertypeDetails && usertypeDetails.data
        editUsertypesDispatch(Userdata._id, formData)
      } else {
        addUsertypeDispatch(formData)
      }
    }
  }

  useEffect(() => {
    const params = {
      tag:"IRM"
    }
    getUserTypeDispatch(params)
  }, [])

  const onConfirm = () => {
    const params = {
      tag:"IRM"
    }
    getUserTypeDispatch(params)
    setShow(false);
    resetState()
  }


  useEffect(() => {
    if (statusUpdateUsertype === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messagesUpdateUsertype,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      cleareditUsertypesDispatch()
    } else if (statusUpdateUsertype === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messagesUpdateUsertype,
        'error',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      cleareditUsertypesDispatch()
    }
  }, [statusUpdateUsertype])

  useEffect(() => {
    if (statusUType === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAddUsertype,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearaddUsertypeDispatch()
      getUserTypeDispatch()
    } else if (statusUType === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAddUsertype,
        'error',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearaddUsertypeDispatch()
    }
  }, [statusUType])

  useEffect(() => {
    if (statusUserType === STATUS_RESPONSE.SUCCESS_MSG) {
      const Userdata = usertypeDetails && usertypeDetails.data
      if ( Userdata && Userdata._id) {
        const data = setTypeData(Userdata)
        setFormData(data)
      }
    }
  }, [statusUserType])

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }


  return (
    <>
      <Modal
        showModal={show}
        modalWidth={650}
      >
        <div className="" id="userModal">
          <div>
            <div className='modal-dialog modal-dialog-centered mw-800px'>
              <div className='modal-content'>
                <div className='modal-header'>
                  <h2 className='me-8 ms-4 mt-4 mb-4'>{editMode ? 'Update' : 'Add'} User Type</h2>
                  <button
                    type='button'
                    className='btn btn-lg btn-icon btn-active-light-primary close m-4'
                    data-dismiss='modal'
                    onClick={() => {
                      setShow(false)
                      resetState()
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                    {/* eslint-disable */}
                  </button>
                </div>
                {loadingUserType ? (
                  <div className="d-flex justify-content-center py-5">
                    <div className="spinner-border text-primary m-5" role="status" />
                  </div>
                ) : (
                  <>
                    <div className='container-fixed'>
                      <div className='card-header bg-lightBlue'>
                        <div className='card-body'>
                          <div className="fv-row mb-10 fv-plugins-icon-container ms-4">
                            <label className="fs-5 fw-bolder form-label mt-4">
                              <span className="required">User Type :</span>
                            </label>
                            <div className='row'>
                              <div className='col-lg-2' />
                              <div className='col-lg-7'>
                                <input
                                  name='userType'
                                  type='text'
                                  className='form-control'
                                  placeholder='User Type'
                                  onChange={(e) => handleChange(e)}
                                  value={formData.userType || ''}
                                  autoComplete='off'
                                  onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                                  onKeyPress={(e) => {
                                    if (!REGEX.ALPHA_NUMERIC.test(e.key)) {
                                      e.preventDefault()
                                    }
                                  }} />
                              </div>
                              {errors && errors.userType && (
                                <div className='rr mt-1'>
                                  <style>{'.rr{color:red}'}</style>
                                  {errors.userType}
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='form-group row mt-4'>
                            <div className='col-lg-5' />
                            <div className='col-lg-7'>
                              <div className='col-lg-10'>
                                <button
                                  className='btn btn-blue mt-7 fa-pull-right mb-4'
                                  onClick={(event) => {
                                    handleSubmit()
                                  }}
                                >
                                  {loadingAddUType || loadingUpdateUserType
                                    ? (
                                      <span
                                        className='spinner-border spinner-border-sm mx-3'
                                        role='status'
                                        aria-hidden='true'
                                      />
                                    )
                                    : (
                                      'Submit'
                                    )}
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div >
        </div>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    usertypeStore,
    addUsertypeStore,
    editUsertypeStore,
    getByIdUsertypeStore
  } = state
  console.log('getByIdUsertypeStore', getByIdUsertypeStore)
  return {
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
    messageAddUsertype:
      addUsertypeStore && addUsertypeStore.messageAddUsertype
        ? addUsertypeStore.messageAddUsertype
        : 0,
    statusUType:
      addUsertypeStore && addUsertypeStore.statusUType
        ? addUsertypeStore.statusUType
        : {},
    loadingAddUType:
      addUsertypeStore && addUsertypeStore.loadingAddUType
        ? addUsertypeStore.loadingAddUType
        : false,
    statusUpdateUsertype:
      editUsertypeStore && editUsertypeStore.statusUpdateUsertype
        ? editUsertypeStore.statusUpdateUsertype
        : '',
    messagesUpdateUsertype:
      editUsertypeStore && editUsertypeStore.messagesUpdateUsertype
        ? editUsertypeStore.messagesUpdateUsertype
        : '',
        dataEUR:
        editUsertypeStore && editUsertypeStore.dataEUR
          ? editUsertypeStore.dataEUR
          : '',
    loadingUpdateUserType:
      editUsertypeStore && editUsertypeStore.loadingUpdateUserType
        ? editUsertypeStore.loadingUpdateUserType
        : false,
    usertypeDetails:
      getByIdUsertypeStore && getByIdUsertypeStore.usertypeDetails
        ? getByIdUsertypeStore.usertypeDetails
        : {},
    statusUserType:
      getByIdUsertypeStore && getByIdUsertypeStore.statusUserType ? getByIdUsertypeStore.statusUserType : '',
    loadingUserType:
      getByIdUsertypeStore && getByIdUsertypeStore.loadingUserType
        ? getByIdUsertypeStore.loadingUserType
        : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserTypeDispatch: (data) =>
    dispatch(userTypesActions.getUserType(data)),
  addUsertypeDispatch: (data) => dispatch(addUsertypeActions.addUsertype(data)),
  clearaddUsertypeDispatch: () =>
    dispatch(addUsertypeActions.clearaddUsertype()),
  editUsertypesDispatch: (id, data) =>
    dispatch(editUsertypeActions.getEditUsertypes(id, data)),
  cleareditUsertypesDispatch: () =>
    dispatch(editUsertypeActions.cleareditUsertypes()),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddUsersType)
