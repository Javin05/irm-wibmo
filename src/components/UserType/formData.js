import _ from 'lodash'

export const setTypeData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      userType:  _.startCase(data.userType),
      tag:"IRM"
    }
  }
}
