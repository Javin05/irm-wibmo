import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import {
   OgmOperatorActions
} from '../../store/actions'
// import './styles.css';
import Modal from 'react-bootstrap/Modal'

function AssigntoUser(props) {
  const {
    wrmOperatorsList,
    cases,
    ogmOperatorActionDispatch,
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [dataValue, setDataValue] = useState({});
  const [role,setRole] = useState("")
  const [manualFormData, setManualFormData] = useState({
     userId:"",
     name:""
  })
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }
  
  const handleSearch = async() => {
    if(role ==="ANALYST"){
       const params = {
      cases:cases,
      name:"assigned to analyst",
      assignedTo:manualFormData.userId,
    }
     await ogmOperatorActionDispatch(params)
    setShow(false)
    setDataValue(params)
    }else if(role ==="QA") {
        const params = {
      cases:cases,
      name:"assign to qa review",
      assignedTo:manualFormData.userId,
    }
     await ogmOperatorActionDispatch(params)
    setShow(false)
    setDataValue(params)
    }
  
  
  }

  const clearPopup = () => {
    setShow(false)
  }

  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
    }))

  }, [show])

 const roles = ["ANALYST","QA"]
  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          onClick={() => { setShow(true) }}
          disabled={cases.length===0}
        >
          {/* eslint-disable */}
          {/* <KTSVG path='/media/icons/duotune/general/gen021.svg' /> */}
          {/* eslint-disable */}
          Assign to users
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Assign task
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Role :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="role"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => setRole(e.target.value)}
                  value={role || ""}
                >
                  <option value="">Select...</option>
                  {roles.map((role) => (
                    <option
                      value={role}
                      key={role}
                    >
                      {role}
                    </option>
                  ))}
                </select>
              </div>
            </div>
             {(role==="ANALYST"||role==="QA") && 
             <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Operators :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="userId"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.userId || ""}
                >
                  <option value="">Select...</option>
                  {wrmOperatorsList?.map((user) => (
                    <option
                      value={user._id}
                      key={user._id}
                    >
                      {`${user.firstName} ${user.lastName}`}
                    </option>
                  ))}
                </select>
              </div>
            </div>}
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  className='btn btn-light-primary m-1 mt-8 font-5vw '
                  onClick={()=>handleSearch()}>
                  Assign
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    WrmOperatorsListStore
  } = state
  return {
    wrmOperatorsList:WrmOperatorsListStore&& WrmOperatorsListStore.WrmOperators?WrmOperatorsListStore.WrmOperators.data:[]
  }
}

const mapDispatchToProps = dispatch => ({
  ogmOperatorActionDispatch:(params)=>dispatch(OgmOperatorActions.ogmOperatorActionInit(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssigntoUser)