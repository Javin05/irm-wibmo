import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  TransactionLaundringActions,
} from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper'
import { RISKSTATUS } from '../../utils/constants'
import moment from "moment"

function TransactionLaundring(props) {
  const {
    TransactionLaundringlistDispatch,
    className,
    TransactionLaundringData,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [searchData, setSearchData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentRoute = url && url[1]
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
    }
    TransactionLaundringlistDispatch(params)
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      const pickByParams = _.pickBy(params)
      TransactionLaundringlistDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
    }
  }, [setFilterFunction, setCredFilterParams])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: activePageNumber,
      ...searchParams,
    }
    TransactionLaundringlistDispatch(params)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      tag: searchData.tag ? searchData.tag : '',
      status: searchData.status ? searchData.status : '',
      website: searchData.website ? searchData.website : '',
    }
    setActivePageNumber(pageNumber)
    TransactionLaundringlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      TransactionLaundringlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      TransactionLaundringlistDispatch(params)
    }
  }


  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const totalPages =
    TransactionLaundringData && TransactionLaundringData.data && TransactionLaundringData.data.count
      ? Math.ceil(parseInt(TransactionLaundringData && TransactionLaundringData.data && TransactionLaundringData.data.count) / limit)
      : 1

  const test = JSON.stringify(TransactionLaundringData, null, 3)
  return (

    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-3'>
              <div className='col-md-6 mt-1 ms-2'>
                {TransactionLaundringData && TransactionLaundringData.data && TransactionLaundringData.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {TransactionLaundringData && TransactionLaundringData.data && TransactionLaundringData.data.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-6 d-flex mt-4'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Case Id</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Account Holder Name</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Account Number</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Brand</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>IFSC Code</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Category</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Website</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      TransactionLaundringData &&
                        TransactionLaundringData.data
                        ? (
                          TransactionLaundringData.data.map((item, i) => {
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => window.open(`${item.screenshot}`, "_blank")}
                                  >
                                    TXNLNG{item.caseId ? item.caseId : "--"}
                                  </a>
                                </td>
                                <td className="ellipsis">
                                  {item.account_holder_name ? item.account_holder_name : "--"}
                                </td>
                                <td className="ellipsis">
                                  {item.account_number ? item.account_number : "--"}
                                </td>
                                <td className="ellipsis">
                                  {item.brand ? item.brand : "--"}
                                </td>
                                <td className="ellipsis">
                                  {item.ifsc_code ? item.ifsc_code : "--"}
                                </td>
                                <td className="ellipsis">
                                  {item.category ? item.category : "--"}
                                </td>
                                <td className="ellipsis">
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => window.open(`${item && item.Website}`, "_blank")}
                                  >
                                    {item.Website ? item.Website : "--"}
                                  </a>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { TransactionLaundringlistStore } = state
  return {
    TransactionLaundringData: state && state.TransactionLaundringlistStore && state.TransactionLaundringlistStore.TransactionLaundringData,
    loading: TransactionLaundringlistStore && TransactionLaundringlistStore.loading ? TransactionLaundringlistStore.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  TransactionLaundringlistDispatch: (params) => dispatch(TransactionLaundringActions.getTransactionLaundringlist(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(TransactionLaundring)
