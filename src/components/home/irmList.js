import React, { useEffect, useState, Fragment } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { riskManagementActions, queuesActions, HomeActions, QueueValuesActions } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { DATE, QUEUE_ROUTING } from '../../utils/constants'
import { useHistory } from 'react-router-dom'
import { DateSelector } from '../../theme/layout/components/DateSelector'

function IRMList(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading,
    value,
    queuesLoading,
    HomeIdResponce,
    HomeIdResponceLoading,
    getHomeDispatch,
    QueueValuesResponce,
    getQueueHomeDispatch
  } = props
  const history = useHistory()
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sortColumn, setSortColumn] = useState('')
  const [sortOrder, setSortOrder] = useState('asc')
  const [sorting, setSorting] = useState({
    queueName: false,
    count: false,
    locked: false,
    onhold: false,
    avgWaitHrs: false,
    maxWaitHrs: false,
    closed: false,
    sla: false,
    open: false
  })
  const [errors, setErrors] = useState()
  const [formData, setFormData] = useState({
    fromDate:'',
    toDate:''
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getHomeDispatch()
    getQueueHomeDispatch()
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    // getRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getHomeDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getHomeDispatch(params)
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.count) / limit)
      : 1

  const handlePageRouting = (item) => {
    const params = {
      queueId: item && item._id
    }
    history.push(`${QUEUE_ROUTING[item && item.queueName]}?queueId=${item && item._id}`, params)
  }

  const handleSubmit = () => {
    const errors = {};
    if (!formData.fromDate) {
      errors.fromDate = 'From date is required.'
    }
    setErrors(errors);
    if (_.isEmpty(errors)) {
      getHomeDispatch(formData)
    }
  }

  const handleReset = () => {
    setFormData('')
    getHomeDispatch()
  }

  const sortData = (data) => {
    if (!sortColumn) return data

    const sortedData = [...data].sort((a, b) => {
      const valueA = a[sortColumn] || ''
      const valueB = b[sortColumn] || ''
    if (sorting && sorting.queueName === true) {
      const stringA = String(valueA || '');
      const stringB = String(valueB || '');
      return sortOrder === 'asc' ? stringA.localeCompare(stringB) : stringB.localeCompare(stringA);
    } else {
      if (sortOrder === 'asc') {
        return a.result[sortColumn] - b.result[sortColumn];
      } else {
        return b.result[sortColumn] - a.result[sortColumn];
      }
    }
    })

    return sortedData
  }

  const handleSort = (column) => {
    sorting[column] = !sorting[column]
    setSorting(sorting)
    if (sortColumn === column) {
      setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc')
    } else {
      setSortColumn(column)
      setSortOrder('asc')
    }
  }

  const sortedData = sortData(HomeIdResponce && HomeIdResponce.data ? HomeIdResponce.data : [])

  return (
    <>
      <div className={`card md-10 ${className}`} >
        <div className='card-body py-3'>
          <div className='d-flex px - 2'>
            <div className='d-flex justify-content-start m-3 mt-7 col-md-12'>
              <div className='col-md-4'>
                <div className='row justify-content-start'>
                  <div className='col-md-4 col-lg-3 mt-3'>
                    From Date:
                  </div>
                  <div className='col-8'>
                    <DateSelector
                      name='fromDate'
                      placeholder='From Date'
                      className='form-control'
                      selected={formData.fromDate || ''}
                      onChange={(date) => {
                        setErrors('')
                        setFormData((values) => ({
                          ...values,
                          fromDate: date
                        }))
                      }}
                      dateFormat={DATE.DATE_ONLY}
                      peek={true}
                      monthDropdown={true}
                      yearDropdown={true}
                      showYear={true} 
                      maxDate={new Date()}
                    />
                    {errors && errors.fromDate && (
                      <div className='fv-plugins-message-container text-danger ms-2 mt-1'>
                        <div className='fv-help-block'>
                          <span role='alert'>{errors && errors.fromDate}</span>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className='col-md-4'>
                <div className='row justify-content-start'>
                  <div className='col-3 mt-3'>
                    To Date :
                  </div>
                  <div className='col-8'>
                    <DateSelector
                      name='toDate'
                      placeholder='To Date'
                      className='form-control'
                      selected={formData.toDate || ''}
                      onChange={(date) => {
                        setFormData((values) => ({
                          ...values,
                          toDate: date
                        }))
                      }}
                      dateFormat={DATE.DATE_ONLY}
                      peek={true}
                      monthDropdown={true}
                      yearDropdown={true}
                      showYear={true} 
                      maxDate={new Date()}
                    />
                  </div>
                </div>
              </div>
              <div className='col-md-2 mt-2'>
                <button className='btn btn-secondary btn-sm me-2' onClick={handleReset} disabled={!formData.fromDate}>Reset</button>
                <button className='btn btn-primary btn-sm' onClick={handleSubmit}>Search</button>
              </div>
              {/* <div className='col-md-3 mt-1'>
                {riskmgmtlists && riskmgmtlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists.count}
                    </span>
                  </span>
                )}
              </div> */}
              {/* <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div> */}
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>S.No</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>QName</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("queueName")}
                        >
                          <i
                            className={`bi ${sorting.queueName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Count</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("count")}
                        >
                          <i
                            className={`bi ${sorting.count
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Open</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("open")}
                        >
                          <i
                            className={`bi ${sorting.open
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Locked</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("locked")}
                        >
                          <i
                            className={`bi ${sorting.locked
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>On Hold</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("onhold")}
                        >
                          <i
                            className={`bi ${sorting.onhold
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Average Wait(hrs)</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("avgWaitHrs")}
                        >
                          <i
                            className={`bi ${sorting.avgWaitHrs
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Maximum Wait (hrs)</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("maxWaitHrs")}
                        >
                          <i
                            className={`bi ${sorting.maxWaitHrs
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>SLA (hrs)</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("sla")}
                        >
                          <i
                            className={`bi ${sorting.sla
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Closed</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSort("closed")}
                        >
                          <i
                            className={`bi ${sorting.closed
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-7'>
                {
                  HomeIdResponceLoading ?  (
                    <tr>
                      <td colSpan='100%' className='text-center'>
                        <div
                          className='spinner-border text-primary m-5'
                          role='status'
                        />
                      </td>
                    </tr>
                  ) :
                     (
                      HomeIdResponce &&
                        sortedData
                        ? (
                          sortedData.map((item, i) => {
                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  <td className='ellipsis text-center'>
                                    {i + 1}
                                  </td>
                                  <td className='ellipsis'>
                                    <button className='btn text-primary btn-outline p-0 border-0 fs-7' onClick={() => handlePageRouting(item)}>{item.queueName ? item.queueName : "--"}</button>
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.count ? item.result.count : "0"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.open ? item.result.open : "0"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.locked ? item.result.locked : "0"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.onhold ? item.result.onhold : "0"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.avgWaitHrs ? item.result.avgWaitHrs : "0"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.maxWaitHrs ? item.result.maxWaitHrs : "--"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item.sla ? item.sla : "0"}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.result && item.result.closed ? item.result.closed : "0"}
                                  </td>
                                </tr>
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                }
              </tbody>
            </table>
          </div>
          {/* <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore, queueslistStore, HomeIdStore, QueueValuesActionsIdStore } = state;
  return {
    riskmgmtlists: state && state.riskManagementlistStore && state.riskManagementlistStore.riskmgmtlists,
    loading: riskManagementlistStore && riskManagementlistStore.loading ? riskManagementlistStore.loading : false,
    HomeIdResponce: HomeIdStore && HomeIdStore.HomeIdResponce ? HomeIdStore.HomeIdResponce : {},
    HomeIdResponceLoading: HomeIdStore && HomeIdStore.loading ? HomeIdStore.loading : false,
    queuesLoading: queueslistStore && queueslistStore.loading ? queueslistStore.loading : false,
    QueueValuesResponce: QueueValuesActionsIdStore && QueueValuesActionsIdStore.QueueValuesResponce ? QueueValuesActionsIdStore.QueueValuesResponce : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params)),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  getHomeDispatch: (params) => dispatch(HomeActions.getHomelist(params)),
  getQueueHomeDispatch: (params) => dispatch(QueueValuesActions.getQueueValueslist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(IRMList)
