import React, { useEffect, useState, useCallback } from 'react'
import clsx from 'clsx'
import { Link, useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import routeConfig from '../../routing/routeConfig'
import { LoginActions } from '../../store/actions'
import { USER_ERROR, REGEX, RESPONSE_STATUS, SESSION, SET_STORAGE } from '../../utils/constants'
import _ from 'lodash'
import { getLocalStorage, setLocalStorage } from '../../utils/helper'
import CryptoJS from "crypto-js"
import { toAbsoluteUrl } from '../../theme/helpers'
// import {
//   GoogleReCaptchaProvider,
//   GoogleReCaptcha
// } from 'react-google-recaptcha-v3'

/*
  Formik+YUP+Typescript:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
  https://medium.com/@maurice.de.beijer/yup-validation-and-typescript-and-formik-6c342578a20e
*/
const SITE_KEY = "6LehHRAoAAAAACYvcKUlObyEnD5nLBOhhcbjQ3rS";

function Login(props) {
  const { loginDispatch, loading, loginData, clearLogin } = props
  const history = useHistory();

  const [formData, setFormData] = useState({
    email: '',
    password: '',
    tag: "IRM"
  })
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [showBanner, setShowBanner] = useState(false)
  const ClientName = JSON.parse(getLocalStorage("CLIENTNAME"))



  const [token, setToken] = useState();
  const [refreshReCaptcha, setRefreshReCaptcha] = useState(false);

  const onVerify = useCallback((token) => {
    setToken(token);
  });

  const doSomething = () => {
    /* do something like submit a form and then refresh recaptcha */
    setRefreshReCaptcha(r => !r);
  }

  const handleOnClick = e => {
    e.preventDefault();

    window.grecaptcha.ready(() => {
      window.grecaptcha.execute(SITE_KEY, { action: 'submit' }).then(token => {
      });
    });
  }
  const submitData = token => {
    console.log("token", token)
  }

  const handleSubmit = (e) => {
    window.grecaptcha.ready(() => {
      window.grecaptcha.execute(SITE_KEY, { action: 'submit' }).then(token => {
        submitData(token)
      })
    })
    const errors = {}
    if (_.isEmpty(formData.email)) {
      errors.email = USER_ERROR.EMAIL_REQUIRED
    } else if (formData.email && !REGEX.EMAIL.test(formData.email)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
    if (_.isEmpty(formData.password)) {
      errors.password = USER_ERROR.PASSWORD_REQUIRED
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      var data = CryptoJS.AES.encrypt(formData.password, 'I4M2OBW').toString()
      const params = {
        email: formData.email,
        password: data,
        tag: "IRM"
      }
      loginDispatch(params)
    }
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    formData[name] = value
    setFormData(formData)
    setErrors({ ...errors, [name]: '' })
  }

  useEffect(() => {
    if (loginData && loginData.data && loginData.data.token && loginData.status === RESPONSE_STATUS.SUCCESS) {
      setLocalStorage(SESSION.TOKEN, loginData.data.token)
      const menuList = loginData && loginData.menulist &&
        loginData.menulist ? loginData.menulist : []
      setLocalStorage(SET_STORAGE.USER_MENU_DETAILS, JSON.stringify(menuList))
      const RouteData = menuList && menuList[0]
      const MainRoute = RouteData && !_.isEmpty(RouteData.submenu) ? RouteData.submenu[0] : RouteData
      window.location = `/${MainRoute && MainRoute.slug}`
      setLocalStorage('ROLEDATA', JSON.stringify(loginData && loginData.data && loginData.data.role))
      setLocalStorage('CLIENTID', JSON.stringify(
        !_.isEmpty(loginData && loginData.data && loginData.data.clientId) ? loginData.data.clientId
          : 'Admin'
      ))
      setLocalStorage('CLIENTNAME', JSON.stringify(loginData && loginData.data && loginData.data.clientName ? loginData.data.clientName : '--'))
      clearLogin()
      // clientId
    }
    else if (loginData.status === RESPONSE_STATUS.ERROR) {
      setShowBanner(true)
      setTimeout(() => {
        setShowBanner(false)
        clearLogin()
      }, 3000)
    }
  }, [loginData])

  useEffect(() => {
    const loadScriptByURL = (id, url, callback) => {
      const isScriptExist = document.getElementById(id)

      if (!isScriptExist) {
        var script = document.createElement("script")
        script.type = "text/javascript"
        script.src = url
        script.id = id
        script.onload = function () {
          if (callback) callback()
        }
        document.body.appendChild(script)
      }

      if (isScriptExist && callback) callback()
    }

    // load the script by passing the URL
    loadScriptByURL("recaptcha-key", `https://www.google.com/recaptcha/api.js?render=${SITE_KEY}`, function () {
      console.log("Script loaded!")
    })
  }, [])

  return (
    <>
      {/* begin::Heading */}
      <div className='text-center mb-10'
      // style={{ backgroundColor: ClientName === 'Ippopay' ? 'transparent' : 'cornflowerblue' }}
      >
        <img
          alt='Logo'
          // src={toAbsoluteUrl('/media/loginImage/MicrosoftTeams-image.png')}
          src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
          className='h-65px mb-4'
        />
        <h1 className='text-dark mb-4'>Sign In</h1>
      </div>

      {/* end::Heading */}

      {/* begin::Banner */}
      {showBanner &&
        <div className='mb-10 bg-light-info p-8 rounded'>
          <div className='text-center text-danger'>
            {loginData.message}
          </div>
        </div>}
      {/* end::Banner */}

      {/* begin::Form group */}
      <div className='fv-row mb-10'>
        <label className='form-label fs-6 fw-bolder text-dark'>Email</label>
        <input
          placeholder='Email'
          className={clsx(
            'form-control form-control-lg form-control-solid',
            { 'is-invalid': formData.email && errors.email },
            {
              'is-valid': formData.email && !errors.email
            }
          )}
          onChange={(e) => handleChange(e)}
          type='email'
          name='email'
          autoComplete='off'
        />
        {errors.email && (
          <div className='fv-plugins-message-container text-danger'>
            <span role='alert text-danger'>{errors.email}</span>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='fv-row mb-10'>
        <div className='d-flex justify-content-between mt-n5'>
          <div className='d-flex flex-stack mb-2'>
            {/* begin::Label */}
            <label className='form-label fw-bolder text-dark fs-6 mb-0'>
              Password
            </label>
            {/* end::Label */}
            {/* begin::Link */}
            <Link
              to={routeConfig.forgotPassword}
              className='link-primary fs-6 fw-bolder'
              style={{ marginLeft: '5px' }}
            >
              Forgot Password ?
            </Link>
            {/* end::Link */}
          </div>
        </div>
        <input
          type='password'
          placeholder='Password'
          autoComplete='off'
          onChange={(e) => handleChange(e)}
          name='password'
          className={clsx(
            'form-control form-control-lg form-control-solid',
            {
              'is-invalid': formData.password && errors.password
            },
            {
              'is-valid': formData.password && !errors.password
            }
          )}
        />
        {errors.password && (
          <div className='fv-plugins-message-container text-danger'>
            <div className='fv-help-block'>
              <span role='alert'>{errors.password}</span>
            </div>
          </div>
        )}
      </div>

      {/* end::Form group */}

      {/* begin::Action */}
      <div className='text-center'>
        <button
          type='button'
          id='kt_sign_in_submit'
          className='btn btn-lg btn-primary w-100 mb-5'
          onClick={(e) => handleSubmit(e)}
          disabled={loading}
        >
          {!loading && <span className='indicator-label'>Continue</span>}
          {loading && (
            <span className='indicator-progress' style={{ display: 'block' }}>
              Please wait...
              <span className='spinner-border spinner-border-sm align-middle ms-2' />
            </span>
          )}
        </button>
      </div>
      {/* end::Action */}
    </>
  )
}

const mapStateToProps = state => {
  const { loginStore } = state
  return {
    loginData: loginStore && loginStore.login ? loginStore.login : {},
    loading: loginStore && loginStore.loading ? loginStore.loading : false
  }
}

const mapDispatchToProps = dispatch => ({
  loginDispatch: (data) => dispatch(LoginActions.login(data)),
  clearLogin: () => dispatch(LoginActions.clearLogin())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)