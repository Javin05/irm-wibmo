
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { setLocalStorage } from '../../../utils/helper'
import { REGEX, STATUS_RESPONSE, DROPZONE_MESSAGES } from '../../../utils/constants'
import {
  KYCaccountAction,
  AccountUploadAction
} from '../../../store/actions'
import _, { values } from 'lodash'
import { warningAlert, successAlert } from "../../../utils/alerts"
import {
  RESTRICTED_FILE_FORMAT_TYPE
} from "../../../constants/index";

function AccountVerification(props) {
  const {
    onClickNext,
    getaccountDetail,
    clearAccountDetailDispatch,
    setFullKycDetails,
    AccountUploadDocDispatch,
    AccountUploadRes,
    AccountUploadResLoading,
    clearAccountUploadDocDispatch
  } = props

  const cancelledFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [Data, setData] = useState(true)
  const [cancelledChequeUpload, setcancelledChequeUpload] = useState('Upload')
  const [showchecque, setShowchecque] = useState(false)
  const [formData, setFormData] = useState({
    accountDetails: '',
    ifscCode: '',
    accountHolderName: '',
    cancelledCheque: ''
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const proccedData = () => {
    const errors = {}
    if (_.isEmpty(formData.accountDetails)) {
      errors.accountDetails = 'Account Number Is Required'
    }
    if (_.isEmpty(formData.ifscCode)) {
      errors.ifscCode = 'IFSC Is Required'
    }
    if (_.isEmpty(errors)) {
      setLocalStorage('AccountVerify', JSON.stringify(formData))
      setData(formData)
      setFullKycDetails((values) => ({ ...values, Account: formData }))
      onClickNext(5)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (getaccountDetail && getaccountDetail.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        `${getaccountDetail && getaccountDetail.message} ${getaccountDetail && getaccountDetail.name}`,
        'success',
      )
      onClickNext(5)
      clearAccountDetailDispatch()
    } else if (getaccountDetail && getaccountDetail.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        getaccountDetail && getaccountDetail.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearAccountDetailDispatch()
    }
  }, [getaccountDetail])

  const cancelledFilePanClick = (event) => {
    cancelledFilesInput.current.click(event);
  }

  const FileChangeHandler = (e) => {
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', 'cancelledCheque')
        data.append('file_to_upload', files)
        AccountUploadDocDispatch(data)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
        setShowchecque(false)
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID })
      setShowchecque(false)
    }
  }

  useEffect(() => {
    if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = AccountUploadRes && AccountUploadRes.data && AccountUploadRes.data.path
      setFormData((values) => ({ ...values, cancelledCheque: data }))
      setcancelledChequeUpload('Uploaded')
      setShowchecque(true)
      clearAccountUploadDocDispatch()
    } else if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, cancelledCheque: '' }))
      clearAccountUploadDocDispatch()
      setcancelledChequeUpload('Upload')
      setShowchecque(false)
    }
  }, [AccountUploadRes])

  useEffect(() => {
    return () => {
      setShowchecque(false)
      setFormData({
        accountDetails: '',
        ifscCode: '',
        accountHolderName: '',
        cancelledCheque: ''
      })
    }
  }, [])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Add bank account details linked to {``}</span>
                  </label>
                </div>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>Payment from your customers will be transfered to this account.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Individual Pan'
                    />
                  </label>
                </div>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='accountDetails'
                placeholder='Account Number'
                onChange={(e) => handleChange(e)}
                value={formData.accountDetails || ''}
                maxLength={42}
                onKeyPress={(e) => {
                  if (!REGEX.NUMERIC.test(e.key)) {
                    e.preventDefault()
                  }
                }}
              />
              {errors && errors.accountDetails && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.accountDetails}
                </div>
              )}
              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='ifscCode'
                placeholder='IFSC Code'
                onChange={(e) => handleChange(e)}
                value={formData.ifscCode || ''}
              />
              {errors && errors.ifscCode && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.ifscCode}
                </div>
              )}

              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='accountHolderName'
                placeholder='Account Holder Name'
                onChange={(e) => handleChange(e)}
                value={formData.accountHolderName || ''}
                maxLength={200}
              />
              {errors && errors.accountHolderName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.accountHolderName}
                </div>
              )}

              <input
                type="file"
                className="d-none mt-4"
                name="cancelledCheque"
                id="cancelledCheque"
                multiple={false}
                ref={cancelledFilesInput}
                onChange={FileChangeHandler} />
              <span className='me-4 fs-5 fw-bolder'>
                Cancelled Cheque :
              </span>
              <button type="button"
                className={`${!showchecque ? 'btn btn-light-primary btn-sm mt-4' : 'btn btn-sm btn-success mt-4'}`}
                onClick={cancelledFilePanClick}>
                {AccountUploadResLoading
                  ? (
                    'Uploading...'
                  )
                  : (
                    <>
                      {cancelledChequeUpload}
                    </>
                  )}
              </button>

              {errors && errors.cancelledCheque && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.cancelledCheque}
                </div>
              )}
            </div>
            <div className='fv-row mb-4'>
              <div className='d-flex flex-stack pt-10 justify-content-end'>
                <div>
                  <button type='submit' className='btn btn-sm btn-light-primary ms-2'
                    onClick={() => { proccedData() }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Proceed
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  getaccountDetail: state && state.accountDetailStore && state.accountDetailStore.accountDetail,
  accountDetailLoading: state && state.accountDetailStore && state.accountDetailStore.loading,
  AccountUploadResLoading: state && state.AccountUploadStore && state.AccountUploadStore.loading,
  AccountUploadRes: state && state.AccountUploadStore && state.AccountUploadStore.AccountUploadRes,
})

const mapDispatchToProps = (dispatch) => ({
  kycAccountDispatch: (params) => dispatch(KYCaccountAction.KYCaccountDetail(params)),
  clearAccountDetailDispatch: (params) => dispatch(KYCaccountAction.clearKYCaccountDetail(params)),
  AccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.AccountUploadDoc(params)),
  clearAccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.clearAccountUploadDoc(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountVerification)