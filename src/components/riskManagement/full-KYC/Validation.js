import { USER_ERROR, USER_MANAGEMENT_ERROR, CRM_ERROR, REGEX } from '../../../utils/constants'
import { getLocalStorage } from '../../../utils/helper'

export const contactInfoValidation = (values, setErrors) => {
  const errors = {}
  if (!values.contactName) {
    errors.contactName = 'Name Is Required'
  }
  if (!values.contactNumber) {
    errors.contactNumber = 'Number Is Required'
  }
  if (!values.contactEmail) {
    errors.contactEmail = 'Email Is Required'
  } else if (values.contactEmail && !REGEX.EMAIL.test(values.contactEmail)) {
    errors.contactEmail = USER_ERROR.EMAIL_INVALID
  }
  setErrors(errors)
  return errors
}

export const businessValidation = (values, setErrors) => {
  const errors = {}
  if (!values.businessType) {
    errors.businessType = 'Business Type Is Required'
  }
  if (!values.businessCategory) {
    errors.businessCategory = 'Business Category Is Required'
  }
  if (!values.subCategory) {
    errors.subCategory = 'Sub Category Is Required'
  }
  if (!values.businessDiscrition) {
    errors.businessDiscrition = 'Business Discrition Is Required'
  }
  setErrors(errors)
  return errors
}

export const businesDetailValidation = (values, setErrors) => {
  const validValue = getLocalStorage('VALIDNAME')
  const errors = {}
  if (!values.businessName) {
    errors.businessName = 'Business Name Is Required'
  }else if (validValue === values.businessName) {
    errors.businessName = 'Company name cannot be same as Contact Name'
  }
  if (!values.panNumber) {
    errors.panNumber = 'Pan Number Is Required'
  } else if(values.panNumber && !REGEX.PAN.test(values.panNumber)) {
    errors.panNumber = 'Pan Number Is InValid'
  }
  if (!values.businessPan) {
    errors.businessPan = 'BusinessPan Number Is Required'
  } else if(values.businessPan && !REGEX.PAN.test(values.businessPan)) {
    errors.businessPan = 'BusinessPan Number Is InValid'
  }
  if (!values.panOwnerName) {
    errors.panOwnerName = 'Pan Owner Name Is Required'
  }
  if (!values.address) {
    errors.address = 'Address Is Required'
  } 
  if (!values.pinCode) {
    errors.pinCode = 'PinCode Is Required'
  } 
  if (!values.billingLabel) {
    errors.billingLabel = 'Billing Label Is Required'
  }  
  if (!values.cinIn) {
    errors.cinIn = 'CIN Is Required'
  } else if(values.cinIn && !REGEX.CIN.test(values.cinIn)) {
    errors.cinIn = 'CIN Number Is InValid'
  }
  if (!values.address) {
    errors.address = USER_ERROR.ADDRESS
  }  
  if (!values.state) {
    errors.state = USER_ERROR.STATE_REQUIRED
  }  
  if (!values.city) {
    errors.city = USER_ERROR.CITY_REQUIRED
  }  
  setErrors(errors)
  return errors
}

export const AccountValidation = (values, setErrors) => {
  const errors = {}
  if (!values.beneficiaryName) {
    errors.beneficiaryName = 'Beneficiary Name Is Required'
  }
  if (!values.IFSCcode) {
    errors.IFSCcode = 'IFSC Code Is Required'
  }
  if (!values.accountNumber) {
    errors.accountNumber = 'Account Number Is Required'
  }
  if (!values.reEnterAccountNumber) {
    errors.reEnterAccountNumber = 'Re-Enter Account Number Is Required'
  }else if(values.accountNumber !== values.reEnterAccountNumber) {
    errors.reEnterAccountNumber = 'Account Number Should Be Same'
  }
  setErrors(errors)
  return errors
}

export const DocumentVerifyValidation = (values, setErrors) => {
  const errors = {}
  if (!values.aadharVerification) {
    errors.aadharVerification = 'aadharVerification Is Required'
  }
  if (!values.captcha) {
    errors.captcha = 'Captcha Is Required'
  }
  setErrors(errors)
  return errors
}

export const phoneOtp = (values, setError, setShowValidateOtp) => {
  const error = {}
  if (!values.otp) {
    error.otp = 'Phone Otp Is Required'
    setShowValidateOtp(true)
  }
  setError(error)
  return error
}
