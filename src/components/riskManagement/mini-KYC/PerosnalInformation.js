
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import _ from 'lodash'
import { KYCphoneVerify } from '../../../store/actions'
import "toastify-js/src/toastify.css"

function PersonalInformation(props) {
  const {
    onClickNext,
    setClientDetails
  } = props

  const [errors, setErrors] = useState({})
  const [showOtp, setShowOtp] = useState(false)
  const [showValidateOtp, setShowValidateOtp] = useState(false)
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    personalAddress: '',
  })

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.firstName)) {
      errors.firstName = "First Name is required"
    }
    if (_.isEmpty(formData.lastName)) {
      errors.lastName = "Last Name is required"
    }
    if (_.isEmpty(formData.personalAddress)) {
      errors.personalAddress = "Personal Address is required"
    }
    if (_.isEmpty(errors)) {
      onClickNext(3)
      setClientDetails((values) => ({...values, personalInformation : formData}))
    }
    setErrors(errors)
  }


  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>First Name</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='First Name'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='firstName'
                placeholder='First Name'
                onChange={(e) => handleChange(e)}
                value={formData.firstName || ''}
              />
              {errors && errors.firstName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.firstName}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Last Name</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Last Name'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='lastName'
                placeholder='Last Name'
                onChange={(e) => handleChange(e)}
                value={formData.lastName || ''}
              />
              {errors && errors.lastName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.lastName}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Personal Address</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Personal Address'
                />
              </label>
              <textarea
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='personalAddress'
                placeholder='Personal Address'
                onChange={(e) => handleChange(e)}
                value={formData.personalAddress || ''}
              />
              {errors && errors.personalAddress && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.personalAddress}
                </div>
              )}
            </div>
            <div className='fv-row mb-4'>
              <div className='d-flex align-items-center pt-10 justify-content-end'>
                <div>
                  <button type='submit' className='btn btn-sm btn-light-primary'
                    onClick={() => { handleSubmit() }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/arrows/arr064.svg'
                        className='svg-icon-3 ms-2 me-0'
                      />
                      Proceed
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists
})

const mapDispatchToProps = (dispatch) => ({
  KycPhoneVerifyDispatch: (data) => dispatch(KYCphoneVerify.KYCphoneVerifyList(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInformation)