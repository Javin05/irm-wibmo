import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  SWEET_ALERT_MSG,
} from "../../utils/constants"
import {
  wrmStatusChangeActions,
  WrmUpdateQueueActions
} from "../../store/actions"
import { useLocation } from "react-router-dom"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../utils/alerts"
import clsx from "clsx"
import Modal from 'react-bootstrap/Modal'

function Status(props) {
  const {
    loading,
    WrmStatusDispatch,
    wrmStatusChangeResponce,
    clearWrmStatusDispatch,
    formData,
    setFormData,
    WrmUpdateQueue,
    clearWrmUpdateQueue,
    WrmUpdateQueueResponse,
    getRiskManagementlistDispatch,
    limit
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const [approveShow, setApproveShow] = useState(false)
  const [rejectShow, setRejectShow] = useState(false)
  const [updateQueue, setUpdateQueue] = useState(false)
  const [queueData, setQueueData] = useState({})
  const [errors, setErrors] = useState({
    reason: "",
    updateQueue:""
  })
  const [rejectFormData, setRejectFormData] = useState({
    status: "REJECTED",
    reason: "",
    rejectType: "",
    rejectMoreValue: "",
  })
  const [approveFormData, setApproveFormData] = useState({
    status: "APPROVED",
    reason: "",
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setQueueData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleQueue = () => {
    const errors = {}
    if (_.isEmpty(queueData.updateQueue)) {
      errors.updateQueue = USER_ERROR.UPDATE_QUEUE
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        ids:formData,
        queueName: queueData && queueData.updateQueue,
      }
      WrmUpdateQueue(params)
    }
  }

  const approveSubmit = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        ids: formData,
        comments: approveFormData.reason,
        status: "APPROVED"
      }
      WrmStatusDispatch(params)
    }
  }

  const onConfirmReject = () => {
    const params = {
      ids: formData,
      comments: rejectFormData.reason,
      status: "REJECTED"
    }
    WrmStatusDispatch(params)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmReject()
        },
        () => { }
      )
    }
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  useEffect(() => {
    if (WrmUpdateQueueResponse && WrmUpdateQueueResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        WrmUpdateQueueResponse && WrmUpdateQueueResponse.message,
        'success'
      )
      setUpdateQueue(false)
      clearWrmUpdateQueue()
      setFormData([])
      const params = {
        limit: limit,
        page: 1,
      }
      getRiskManagementlistDispatch(params)
    } else if (WrmUpdateQueueResponse && WrmUpdateQueueResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        WrmUpdateQueueResponse && WrmUpdateQueueResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearWrmUpdateQueue()
    }
  }, [WrmUpdateQueueResponse])

  useEffect(() => {
    if (wrmStatusChangeResponce && wrmStatusChangeResponce.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        wrmStatusChangeResponce && wrmStatusChangeResponce.message,
        'success'
      )
      setApproveShow(false)
      setRejectShow(false)
      clearWrmStatusDispatch()
      setFormData([])
      const params = {
        limit: limit,
        page: 1,
      }
      getRiskManagementlistDispatch(params)
    } else if (wrmStatusChangeResponce && wrmStatusChangeResponce.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        wrmStatusChangeResponce && wrmStatusChangeResponce.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearWrmStatusDispatch()
    }
  }, [wrmStatusChangeResponce])

  const clearPopup = () => {
    setApproveShow(false)
    setRejectShow(false)
    setUpdateQueue(false)
    setRejectFormData({
      reason: "",
      rejectType: "",
      rejectMoreValue: ""
    })
    setApproveFormData({
      comment: '',
      reason: "",
    })
    setErrors({})
  }

  return (
    <>
      <Modal
        show={approveShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to Approve This Users Risk Analytics Report ?

          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Approved
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 ">
                      <textarea
                        name="reason"
                        type="text"
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              approveFormData.reason && errors.reason,
                          },
                          {
                            "is-valid":
                              approveFormData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => approveChange(e)}
                        autoComplete="off"
                        value={approveFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                        type="submit"
                        className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                        onClick={(event) => {
                          approveSubmit(event)
                        }}
                        disabled={loading}
                      >
                        {!loading && (
                          "Submit"
                        )}
                        {loading && (
                            <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        )}
                      </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </Modal.Body>
      </Modal>

      <Modal
        show={rejectShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Status
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Reject :
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                      <textarea
                        name="reason"
                        type="text"
                        // className='form-control'
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid": rejectFormData.reason && errors.reason,
                          },
                          {
                            "is-valid": rejectFormData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => rejectChange(e)}
                        autoComplete="off"
                        value={rejectFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                        type="submit"
                        className="btn btn-sm btn-light-primary m-2 fa-pull-right"
                        onClick={
                          rejectSubmit
                        }
                        disabled={loading}
                      >
                        {!loading && (
                          "Submit"
                        )}
                        {loading && (
                            <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        )}
                      </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </Modal.Body>
      </Modal>

      <Modal
        show={updateQueue}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to Update This Users Accounts Risk Management Report ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Update Queue
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 ">
                    <select
                      name='updateQueue'
                      className='form-select form-select-solid mb-4'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                    >
                      <option value=''>Select...</option>
                      <option value='SUSPECT_MERCHANT_ONBOARDING_Q'>SUSPECT_MERCHANT_ONBOARDING_Q</option>
                      <option value='MERCHANT_ONBOARDING_Q'>MERCHANT_ONBOARDING_Q</option>
                    </select>
                    {errors.updateQueue && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.updateQueue}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                        type="submit"
                        className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                        onClick={(event) => {
                          handleQueue(event)
                        }}
                        disabled={loading}
                      >
                        {!loading && (
                          "Submit"
                        )}
                        {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className="card-toolbar d-flex">
        <>
          <ul className="nav">
            <li className="nav-item">
              <a
                className="nav-link btn btn-sm btn-responsive font-7vw fw-bold px-4 me-1 btn-light-success ms-2"
                onClick={() => {
                  setUpdateQueue(true)
                }}
              >
                Update Queues
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link btn btn-sm btn-responsive font-7vw fw-bold px-4 me-1 btn-light-success ms-2"
                onClick={() => {
                  setApproveShow(true)
                }}
              >
                Approve
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link btn btn-sm btn-responsive font-7vw fw-bold px-4 me-1 btn-light-danger"
                onClick={() => {
                  setRejectShow(true)
                }}
              >
                Reject
              </a>
            </li>
          </ul>
        </>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    wrmStatusChangeStore,
    wrmUpdateQueueStore
  } = state
  return {
    wrmStatusChangeResponce: wrmStatusChangeStore && wrmStatusChangeStore.wrmStatusChangeResponce ? wrmStatusChangeStore.wrmStatusChangeResponce : {},
    loading: wrmStatusChangeStore && wrmStatusChangeStore.loading ? wrmStatusChangeStore.loading : false,
    WrmUpdateQueueResponse: wrmUpdateQueueStore && wrmUpdateQueueStore.wrmUpdateQueueResponce ? wrmUpdateQueueStore.wrmUpdateQueueResponce :{},
  }
}

const mapDispatchToProps = (dispatch) => ({
  WrmStatusDispatch: (params) => dispatch(wrmStatusChangeActions.wrmStatusChange(params)),
  clearWrmStatusDispatch: () => dispatch(wrmStatusChangeActions.clearwrmStatusChange()),
  WrmUpdateQueue: (params) => dispatch(WrmUpdateQueueActions.WrmUpdateQueue(params)),
  clearWrmUpdateQueue: () => dispatch(WrmUpdateQueueActions.clearWrmUpdateQueue()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Status)