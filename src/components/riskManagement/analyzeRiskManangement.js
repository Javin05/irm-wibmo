import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  riskManagementActions
} from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage, getLocalStorage, removeLocalStorage } from '../../utils/helper';
import { RISKSTATUS, REVIEWTYPE, SET_FILTER, CREATE_PERMISSION } from '../../utils/constants'
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import { STATUS_RESPONSE } from '../../utils/constants'
import FindRole from './Role'
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'
import KYCminiAdd from "./mini-KYC/Add"
import Status from './ARH'
import moment from "moment"

function RiskManagementList(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction
  } = props
  const location = useLocation()
  const didMount = React.useRef(false);
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1)
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [show, setShow] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [activeKyc, setActiveKyc] = useState({})
  const [formData, setFormData] = useState([])
  const pathName = useLocation().pathname
  const getUsersPermissions = getUserPermissions(pathName, true)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  useEffect(() => {
    if (location.state) {
      const data = location.state
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      const credBasedParams = {
        clientId: credBasedClientValue
      }
      const params = {
        limit: limit,
        page: 1,
        ...searchParams,
      }
      Object.assign(params, data)
      const pickByParams = _.pickBy(params);
      getRiskManagementlistDispatch(pickByParams)
    } else {
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      const credBasedParams = {
        clientId: credBasedClientValue
      }
      const params = {
        limit: limit,
        page: 1,
        ...searchParams,
      }
      const pickByParams = _.pickBy(params);
      getRiskManagementlistDispatch(pickByParams)
    }
  }, [location.state])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
      };
      const pickByParams = _.pickBy(params);
      getRiskManagementlistDispatch(pickByParams)
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: value,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params);
    getRiskManagementlistDispatch(pickByParams)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      ...searchParams,
      limit: limit,
      page: pageNumber,
      ...credBasedParams
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params);
    setActivePageNumber(pageNumber)
    getRiskManagementlistDispatch(pickByParams)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getRiskManagementlistDispatch(params)
    }
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  })

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count) / limit)
      : 1

  return (

    <>
      <KYCminiAdd setShow={setShow} show={show} editMode={editMode} activeKyc={activeKyc} setActiveKyc={setActiveKyc} />
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="csvReport"
          className="download-table-xls-button"
          table="table-to-xlss"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex px-2'>
            <div className='d-flex justify-content-between col-md-5'>
              <div className='col-md-4 mt-4 ms-2'>
                {riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-8 d-flex mt-4'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-7 align-items-center justify-content-end mt-4'>
              <div className=''>
                {
                  !_.isEmpty(formData) ? (
                    <Status formData={formData} setFormData={setFormData} getRiskManagementlistDispatch={getRiskManagementlistDispatch} limit={limit} />
                  ) : null
                }
              </div>
              <div className='my-auto'>
                <SearchList setSearchParams={setSearchParams} />
              </div>
              {/* <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              > */}
              {/* <div className="my-auto me-3">
                  <button
                    type="button"
                    className="btn btn-sm btn-light-primary font-5vw pull-right"
                    data-toggle="modal"
                    data-target="#addModal"
                    onClick={() => {
                      setShow(true)
                      setEditMode(false)
                    }}
                  >
                    <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                    Add Merchant
                  </button>
                </div> */}
              {/* </Can> */}
              <div className='my-auto me-3'>
                <Link
                  to='/merchant-login'
                  className='btn btn-sm btn-light-primary btn-responsive font-7vw'
                >
                  <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                  Add Account Queue
                </Link>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>First Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${sorting.firstName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Last Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("lastName")}
                        >
                          <i
                            className={`bi ${sorting.lastName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("phone")}
                        >
                          <i
                            className={`bi ${sorting.phone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Personal Email</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("personalEmail")}
                        >
                          <i
                            className={`bi ${sorting.personalEmail
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Enqueue Hours</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Priority</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Business Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("businessName")}
                        >
                          <i
                            className={`bi ${sorting.businessName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Business Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("businessPhone")}
                        >
                          <i
                            className={`bi ${sorting.businessPhone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("website")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Manual Status</span>
                      </div>
                    </th>
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Auto Status</span>
                      </div>
                    </th>
                  </FindRole>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      riskmgmtlists &&
                        riskmgmtlists.data
                        ? (
                          riskmgmtlists.data && riskmgmtlists.data.result.map((riskmgmtlist, i) => {
                            const indiaDate = moment.utc(riskmgmtlist && riskmgmtlist.createdAt).local()
                            const todayDate = moment()
                            const duration = moment.duration(todayDate.diff(indiaDate))
                            const hours = Math.floor(duration.asHours());
                            const minutes = duration.minutes();
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="min-width-150px text-center form-check form-check-custom form-check-success form-check-solid">
                                  <input
                                    className="form-check-input cursor-pointer "
                                    type="Checkbox"
                                    value={riskmgmtlist._id}
                                    onChange={(e) => handleChange(e)}
                                    checked={formData && formData.includes(riskmgmtlist._id)}
                                    name='checkName'
                                  />
                                </td>
                                <td className="ellipsis">
                                  <>
                                    <Link to={`/account-risk-summary/update/${riskmgmtlist._id}`} className="ellipsis">
                                      IRM{riskmgmtlist && riskmgmtlist.riskId}
                                    </Link>
                                  </>
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.riskStatus && riskmgmtlist.riskStatus]}`}>
                                      {riskmgmtlist.riskStatus ? riskmgmtlist.riskStatus : "--"}
                                    </span>
                                  </td>
                                </FindRole>
                                <td className="ellipsis">
                                  {riskmgmtlist.firstName ? riskmgmtlist.firstName : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.lastName ? riskmgmtlist.lastName : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.phone ? riskmgmtlist.phone : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.personalEmail ? riskmgmtlist.personalEmail : "--"}
                                </td>
                                <td className="ellipsis text-start">
                                  {riskmgmtlist && riskmgmtlist.enqueueHours ? !_.isEmpty(riskmgmtlist.enqueueHours) ? riskmgmtlist.enqueueHours : `${hours}:${minutes}` : `${hours}:${minutes}`}
                                </td> <td className="ellipsis text-start">
                                  {riskmgmtlist && riskmgmtlist.priority ? riskmgmtlist.priority : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.businessName ? riskmgmtlist.businessName : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.businessPhone ? riskmgmtlist.businessPhone : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.website ? riskmgmtlist.website : "--"}
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.manualStatus && riskmgmtlist.manualStatus]}`}>
                                      {riskmgmtlist.manualStatus ? riskmgmtlist.manualStatus : "--"}
                                    </span>
                                  </td>
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.autoStatus && riskmgmtlist.autoStatus]}`}>
                                      {riskmgmtlist.autoStatus ? riskmgmtlist.autoStatus : "--"}
                                    </span>
                                  </td>
                                </FindRole>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div >
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore } = state
  return {
    riskmgmtlists: state && state.riskManagementlistStore && state.riskManagementlistStore.riskmgmtlists,
    loading: riskManagementlistStore && riskManagementlistStore.loading ? riskManagementlistStore.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(RiskManagementList);