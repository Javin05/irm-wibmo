import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { TransactionActions } from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage } from '../../utils/helper';
import { RISKSTATUS, TXNSTATUS } from '../../utils/constants'

function RecentTransaction(props) {

  const {
    className,
    Transactionlists,
    loading,
    getTransactionDispatch,
  } = props
  const [limit, setLimit] = useState(3)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const currentRoute = url && url[1]
  const [routeShow, setRouteShow] = useState()
  const TransactonData = Transactionlists && Transactionlists.data ? Transactionlists.data : []

  useEffect(() => {
    const params = {
      merchantId: currentId,
      limit: limit,
      page: 1
    }
    getTransactionDispatch(params)
  }, [])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getTransactionDispatch(params)
  }

  const totalPages =
    Transactionlists && Transactionlists.count
      ? Math.ceil(parseInt(Transactionlists && Transactionlists.count) / limit)
      : 1

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6 mt-2'>
              <div className='col-md-3 mt-1'>
                {Transactionlists && Transactionlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {Transactionlists.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Case ID</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Transaction Status</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>First Name</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Last Name</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Description</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Phone</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Email</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Currency</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>TotalAmount</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Quantity</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Department</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>PaymentMethod</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>CreditCardNumber</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>BillingAddress</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>billingCity</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>BillingState</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>BillingCountry</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>billingZipCode</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>AVS</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>CVV</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShipToFirstName</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShipToLastName</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingAddress</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingCity</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingState</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingCountry</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingZipCode</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      !_.isEmpty(TransactonData)
                        ? (
                          TransactonData && TransactonData.map((transaction, i) => {
                            return (
                              <tr
                                key={i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <Link to={`/transaction-dashboard/${transaction._id}`} className="ellipsis">
                                    {transaction.transactionId}
                                  </Link>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[transaction.riskStatus && transaction.riskStatus]}`}>
                                    {transaction.riskStatus}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${TXNSTATUS[transaction.transactionStatus && transaction.transactionStatus]}`}>
                                    {transaction.transactionStatus}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  {transaction.firstName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.lastName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.description}
                                </td>
                                <td className="ellipsis">
                                  {transaction.phone}
                                </td>
                                <td className="ellipsis">
                                  {transaction.emailAddress}
                                </td>
                                <td className="ellipsis">
                                  {transaction.currency}
                                </td>
                                <td className="ellipsis">
                                  {transaction.totalAmount}
                                </td>
                                <td className="ellipsis">
                                  {transaction.quantity}
                                </td>
                                <td className="ellipsis">
                                  {transaction.department}
                                </td>
                                <td className="ellipsis">
                                  {transaction.paymentMethod}
                                </td>
                                <td className="ellipsis">
                                  {transaction.creditCardNumber}
                                </td>
                                <td>
                                  {transaction.billingAddress}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingCity}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingState}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingCountry}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingZipCode}
                                </td>
                                <td className="ellipsis">
                                  {transaction.AVS}
                                </td>
                                <td className="ellipsis">
                                  {transaction.CVV}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shipToFirstName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shipToLastName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingAddress}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingCity}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingState}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingCountry}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingZipCode}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-transactions-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { TransactionlistStore } = state;
  return {
    Transactionlists: TransactionlistStore && TransactionlistStore.Transactionlists ? TransactionlistStore.Transactionlists : [],
    loading: TransactionlistStore && TransactionlistStore.loading ? TransactionlistStore.loading : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getTransactionDispatch: (params) => dispatch(TransactionActions.getTransactionlist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(RecentTransaction)
