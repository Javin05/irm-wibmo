import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import { Link, useLocation } from 'react-router-dom'
import './index.scss'
import NetworkGraph from './Netwok'
import RecentTransaction from './RecentTransaction'
import Annatation from './Annotation'
import AccountList from './AccountDetailsList'
import { AMLPercentageActions, linkAnalyticsActions, AMLWMYActions } from '../../store/actions'
import { Scrollbars } from 'react-custom-scrollbars';
import Chart from "react-apexcharts";

function Investigation(props) {
  const {
    getRiskSummarys,
    PercentageAMLDispatch,
    TxngetbyIdDispatch,
    TransactionGetById,
    getlinkAnalyticslistDispatch,
    LinkAnlyticslists,
    PercentageData,
    AMLWMYDispatch,
    WMYResponcelists,
    PersonData
  } = props

  const [empty, setEmpty] = useState([
    ["Metrics", "Total Sales", "Chargeback", "Refund", "Disputes"],
    ["Sun", 0, 0, 0, 0]
  ]);

  const [key, setKey] = useState('Account Details');
  const [tabDefault, setTabdefault] = useState('chart');
  const [chartTitle, setChartTitle] = useState('Sales');
  const [chartData, setChartData] = useState(empty);
  const [chartCategory, setChartCategory] = useState(null);
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [filterType, setFilterType] = useState("seven_days")

  // Account details chart - top
  const WMYRdata = WMYResponcelists && WMYResponcelists.data ? WMYResponcelists.data : null

  let sevenDay = []
  let thirtyDay = []
  let monthDay = []
  let yearDay = []
  let sevenCategory = []
  let thirtyCategory = []
  let monthCategory = []
  let yearCategory = []
  let mData = []
  let mCategory = []

  if (WMYRdata) {
    let sevenData = WMYRdata.sevenDays;
    let thirtyData = WMYRdata.last31Days;
    let monthData = WMYRdata.monthly;
    let yearData = WMYRdata.yearly;

    if (filterType === "last31Days") {
      let totalAmount = []
      let chargebackAmount = []
      let refundAmount = []
      let disputeAmount = []
      Object.keys(thirtyData).forEach(function (thirty) {
        mCategory.push(thirtyData[thirty].monthName)
        totalAmount.push(thirtyData[thirty].totalAmount)
        chargebackAmount.push(thirtyData[thirty].chargebackAmount)
        refundAmount.push(thirtyData[thirty].refundAmount)
        disputeAmount.push(thirtyData[thirty].disputeAmount)
      });
      mData.push({ name: "Total Sales", data: totalAmount })
      mData.push({ name: "Chargeback", data: chargebackAmount })
      mData.push({ name: "Refund", data: refundAmount })
      mData.push({ name: "Dispute", data: disputeAmount })
    }
    else if (filterType === "seven_days") {
      let totalAmount = []
      let chargebackAmount = []
      let refundAmount = []
      let disputeAmount = []
      Object.keys(sevenData).forEach(function (day) {
        mCategory.push(sevenData[day].monthName)
        totalAmount.push(sevenData[day].totalAmount)
        chargebackAmount.push(sevenData[day].chargebackAmount)
        refundAmount.push(sevenData[day].refundAmount)
        disputeAmount.push(sevenData[day].disputeAmount)
      });
      mData.push({ name: "Total Sales", data: totalAmount })
      mData.push({ name: "Chargeback", data: chargebackAmount })
      mData.push({ name: "Refund", data: refundAmount })
      mData.push({ name: "Dispute", data: disputeAmount })
    }

    else if (filterType === "one_month") {
      let totalAmount = []
      let chargebackAmount = []
      let refundAmount = []
      let disputeAmount = []
      Object.keys(monthData).forEach(function (month) {
        mCategory.push(monthData[month].monthName)
        totalAmount.push(monthData[month].totalAmount)
        chargebackAmount.push(monthData[month].chargebackAmount)
        refundAmount.push(monthData[month].refundAmount)
        disputeAmount.push(monthData[month].disputeAmount)
      });
      mData.push({ name: "Total Sales", data: totalAmount })
      mData.push({ name: "Chargeback", data: chargebackAmount })
      mData.push({ name: "Refund", data: refundAmount })
      mData.push({ name: "Dispute", data: disputeAmount })
    }
    else if (filterType === "one_year") {
      let totalAmount = []
      let chargebackAmount = []
      let refundAmount = []
      let disputeAmount = []
      Object.keys(yearData).forEach(function (year) {
        mCategory.push(yearData[year].year)
        totalAmount.push(yearData[year].totalAmount)
        chargebackAmount.push(yearData[year].chargebackAmount)
        refundAmount.push(yearData[year].refundAmount)
        disputeAmount.push(yearData[year].disputeAmount)
      });
      mData.push({ name: "Total Sales", data: totalAmount })
      mData.push({ name: "Chargeback", data: chargebackAmount })
      mData.push({ name: "Refund", data: refundAmount })
      mData.push({ name: "Dispute", data: disputeAmount })
    }
  }

  var v = {
    chart: {
      height: 350,
      type: 'bar',
      stacked: true,
      toolbar: {
        show: true,
         tools: {
            download: true,
            selection: false,
            zoom: false,
            zoomin: false,
            zoomout: false,
            pan: false,
            reset : false,
            //customIcons: []
        },
      }
    },
    colors: ['#4157e2', '#993404', '#60c3f3', '#ec974f'],
    dataLabels: {
      style: {
        colors: ['#000']
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
      }
    },
    xaxis: {
      categories: mCategory
    },
    yaxis: {
      labels: {
        show: false,
        trim: false
      }
    }
  }


  const ToggleTab = (tab) => {
    setTabdefault(tab)
  }
  const ToggleChart = (type) => {
    setFilterType(type)
    if (type === "last31Days") {
      setChartTitle("Sales Data")
      setChartData(thirtyDay)
      setChartCategory(thirtyCategory)
    }
    if (type === "seven_days") {
      setChartTitle("7 Days")
      setChartData(sevenDay)
      setChartCategory(sevenCategory)
    }
    if (type === "one_month") {
      setChartTitle("Monthly")
      setChartData(monthDay)
      setChartCategory(monthCategory)
    }
    if (type === "one_year") {
      setChartTitle("Yearly")
      setChartData(yearDay)
      setChartCategory(yearCategory)
    }
  }

  // Transactions Chart -bottom
  const AMLlinkAnalytics = LinkAnlyticslists && LinkAnlyticslists.data ? LinkAnlyticslists.data : null
  let mainData = [];
  let LabelsArray = ["Metrics", "Total Sales", "Chargeback", "Refund", "Disputes"];
  let lifetimeArray = ["Sales"];
  let sevenArray = [];
  let monthArray = ["1 Month"];
  let yearArray = ["1 Year"];

  useEffect(() => {
    PercentageAMLDispatch(currentId)
    AMLWMYDispatch(currentId)
    const params = {
      caseId: currentId,
    }
    getlinkAnalyticslistDispatch(params)
  }, [])


  return (
    <>

      <div className="row mb-6 mt-6">
        <div className='col-md-12'>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab mb-3">
            <Tab eventKey="Account Details" title="Defect Trend" className='tab-color'>
              <>
                <ul className="account-tab">
                  <li onClick={() => ToggleTab("list")} className={tabDefault === "list" ? "active" : "none"}>List</li>
                  <li onClick={() => ToggleTab("chart")} className={tabDefault === "chart" ? "active" : "none"}>Chart</li>
                </ul>
                {tabDefault === "list" ? (
                  <Scrollbars style={{ width: "100%", height: 400 }}>
                    <AccountList filterType={filterType} />
                  </Scrollbars>
                ) : (
                  <>
                    <ul className="chart-tab">
                      <li onClick={() => ToggleChart("seven_days")} className={filterType === "seven_days" ? "active" : "none"}>7 Days</li>
                      <li onClick={() => ToggleChart("last31Days")} className={filterType === "last31Days" ? "active" : "none"}>last 31 Days</li>
                      <li onClick={() => ToggleChart("one_month")} className={filterType === "one_month" ? "active" : "none"}>Monthly</li>
                      <li onClick={() => ToggleChart("one_year")} className={filterType === "one_year" ? "active" : "none"}>Yearly</li>
                    </ul>
                    {WMYRdata !== null ? (
                      <Chart
                        series={mData}
                        options={v}
                        type="bar"
                        height={340}
                      />
                    ) : <div>Not Available</div>}
                  </>
                )}
              </>
            </Tab>
            <Tab eventKey="Linked account" title="Linked account">
              <Scrollbars style={{ width: "100%", height: 400 }}>
                <NetworkGraph AMLlinkAnalytics={AMLlinkAnalytics} PersonData={PersonData} />
              </Scrollbars>
            </Tab>
            <Tab eventKey="Recent Transactions" title="Recent Transactions">
              <>
                <RecentTransaction />
              </>
            </Tab>
            <Tab eventKey="Annotations" title="Annotations">
              <>
                <Annatation currentId={currentId} />
              </>
            </Tab>
          </Tabs>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { linkAnalyticslistStore, AMLWMYAStore } = state

  return {
    LinkAnlyticslists: linkAnalyticslistStore && linkAnalyticslistStore.linkAnalyticallists ? linkAnalyticslistStore.linkAnalyticallists : {},
    WMYResponcelists: AMLWMYAStore && AMLWMYAStore.WMYResponce ? AMLWMYAStore.WMYResponce : {}
  }
}

const mapDispatchToProps = (dispatch) => ({
  PercentageAMLDispatch: (id) => dispatch(AMLPercentageActions.PercentageAML(id)),
  getlinkAnalyticslistDispatch: (params) => dispatch(linkAnalyticsActions.getlinkAnalyticslist(params)),
  AMLWMYDispatch: (id) => dispatch(AMLWMYActions.WMYAML(id)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Investigation)