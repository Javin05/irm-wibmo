import React, { useState } from 'react'
import { connect } from 'react-redux'
import AMLdashboard from './AccountsDetail'
import routeConfig from '../../routing/routeConfig'
import { useHistory } from 'react-router-dom'
import { TransactionActions } from '../../store/actions'

function Scores(props) {
  const {
    PercentageAMLResponce,
    currentId,
    getTransactionDispatch
  } = props
  const history = useHistory()
  const [tabDefault, setTabdefault] = useState('Percentage');

  const PercentageData = PercentageAMLResponce && PercentageAMLResponce.data ? PercentageAMLResponce && PercentageAMLResponce.data : '--'
  const riskLevel = PercentageData && PercentageData.riskLevel ? PercentageData && PercentageData.riskLevel : '0'
  const lifeTimeData = PercentageData && PercentageData.lifeTime ? PercentageData && PercentageData.lifeTime : '0'

  const ToggleTab = (tab) => {
    setTabdefault(tab)
  }

  const OnSearchChargeback = () => {
    history.push(routeConfig.transactionSearch)
    const params = {
      merchantId: currentId,
      transactionStatus: 'Chargeback'
    }
    getTransactionDispatch(params)
  }

  const OnSearchRefund = () => {
    history.push(routeConfig.transactionSearch)
    const params = {
      merchantId: currentId,
      transactionStatus: 'Refund'
    }
    getTransactionDispatch(params)
  }

  const OnSearchDispute = () => {
    history.push(routeConfig.transactionSearch)
    const params = {
      merchantId: currentId,
      transactionStatus: 'Dispute'
    }
    getTransactionDispatch(params)
  }
  return (
    <>
      <div className='row mb-6'>
        <div className='mb-4'>
          <div className='row'>
            <div className='col-sm-3 col-md-3 col-lg-3' />
            <div className='col-sm-9 col-md-9 col-lg-9 d-flex justify-content-end '>
              <a onClick={() => ToggleTab("Percentage")} className={tabDefault === "Percentage" ? "active btn btn-primary me-2 btn-sm" : "btn btn-outline btn-outline-dashed btn-outline-primary me-2 btn-sm"}>
                <i
                  className={tabDefault === "Percentage" ? 'active bi bi-percent bi-primary fs-5' : 'bi bi-percent fs-5'}
                />
              </a>
              <a onClick={() => ToggleTab("TotalChargeBack")} className={tabDefault === "TotalChargeBack" ? "active btn btn-primary me-2 btn-sm" : "btn btn-outline btn-outline-dashed btn-outline-primary me-2 btn-sm"}>
                <i
                  className={tabDefault === "TotalChargeBack" ? 'active bi bi-hash bi-primary fs-5' : 'bi bi-hash fs-5'}
                />
              </a>
              <a onClick={() => ToggleTab("TotalChargebackAmount")} className={tabDefault === "TotalChargebackAmount" ? "active btn btn-primary btn-sm" : "btn btn-outline btn-outline-dashed btn-outline-primary btn-sm"}>
                <i
                  className={tabDefault === "TotalChargebackAmount" ? 'active bi bi-currency-dollar bi-primary' : 'bi bi-currency-dollar'}
                />
              </a>
            </div>
          </div>
        </div>
        <div className='col-sm-4 col-md-4 col-lg-4'>
          <div className="card card-flush h-md-100">
            <div className=" no-padding" style={{ background: '#f15242', borderRadius: 'calc(0.475rem - 1px) calc(0.475rem - 1px) 0 0' }}>
              <div className="menu-item ">
                <span className="menu-link">
                  <span className="menu-title fw-bolder fs-3">Chargeback Rate</span>
                  <span className="menu-icon"
                    onClick={() => { OnSearchChargeback() }}
                  >
                    <span className="svg-icon-2 me-3" style={{ color: '#1f95e8' }}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                      </svg>
                    </span>
                  </span>
                </span>
              </div>
            </div>
            <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
            <div className="card-body pt-1">
              <div className="row">
                <div className='col-sm-12 col-md-12 col-lg-12'>
                  <h1 className="d-flex justify-content-center fs-1">
                    {
                      tabDefault === 'TotalChargeBack' ?
                        (
                          lifeTimeData && lifeTimeData.chargeBack && lifeTimeData.chargeBack.totalChargeback ? lifeTimeData.chargeBack.totalChargeback : '0'
                        ) :
                        tabDefault === 'Percentage' ?
                          (
                            lifeTimeData && lifeTimeData.chargeBack && lifeTimeData.chargeBack.ChargebackData ? lifeTimeData.chargeBack.ChargebackData : '0'
                          ) : tabDefault === 'TotalChargebackAmount' ?
                            (
                              <span>
                                <i className="bi bi-currency-dollar fs-3" />
                                {
                                  lifeTimeData && lifeTimeData.chargeBack && lifeTimeData.chargeBack.totalChargebackAmount ? lifeTimeData.chargeBack.totalChargebackAmount : '0'
                                }
                              </span>
                            ) : null
                    }
                  </h1>
                  <div className='d-flex justify-content-center mb-4'>{riskLevel && riskLevel.chargeBackRate ? riskLevel.chargeBackRate : '0'}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-sm-4 col-md-4 col-lg-4'>
          <div className="card card-flush h-md-100">
            <div className=" no-padding" style={{ background: '#f9af15', borderRadius: 'calc(0.475rem - 1px) calc(0.475rem - 1px) 0px 0px' }}>
              <div className="menu-item ">
                <span className="menu-link">
                  <span className="menu-title fw-bolder fs-3">Refund Rate</span>
                  <span className="menu-icon"
                    onClick={() => { OnSearchRefund() }}
                  >
                    <span className="svg-icon-2 me-3" style={{ color: '#1f95e8' }}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                      </svg>
                    </span>
                  </span>
                </span>
              </div>
            </div>
            <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
            <div className="card-body pt-1">
              <div className="row">
                <div className='col-sm-12 col-md-12 col-lg-12'>
                  <h1 className="d-flex justify-content-center fs-1">
                    {
                      tabDefault === 'TotalChargeBack' ?
                        (
                          lifeTimeData && lifeTimeData.refund && lifeTimeData.refund.totalrefund ? lifeTimeData.refund.totalrefund : '0'
                        ) :
                        tabDefault === 'Percentage' ?
                          (
                            lifeTimeData && lifeTimeData.refund && lifeTimeData.refund.refundData ? lifeTimeData.refund.refundData : '0'
                          ) : tabDefault === 'TotalChargebackAmount' ?
                            (
                              <span>
                                <i className="bi bi-currency-dollar fs-3" />
                                {
                                  lifeTimeData && lifeTimeData.refund && lifeTimeData.refund.totalrefundAmount ? lifeTimeData.refund.totalrefundAmount : '0'
                                }
                              </span>
                            ) : null
                    }
                  </h1>
                  <div className='d-flex justify-content-center mb-4'>{riskLevel && riskLevel.refundRate ? riskLevel.refundRate : '0'}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-sm-4 col-md-4 col-lg-4'>
          <div className="card card-flush h-md-100">
            <div className=" no-padding" style={{ background: '#9cadf5', borderRadius: 'calc(0.475rem - 1px) calc(0.475rem - 1px) 0 0' }}>
              <div className="menu-item ">
                <span className="menu-link active">
                  <span className="menu-title fw-bolder fs-3">Dispute Rate</span>
                  <span className="menu-icon"
                    onClick={() => { OnSearchDispute() }}
                  >
                    <span className="svg-icon-2 svg-icon-primary me-3" style={{ color: '#1f95e8' }}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                      </svg>
                    </span>
                  </span>
                </span>
              </div>
            </div>
            <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
            <div className="card-body pt-1">
              <div className="row">
                <div className='col-sm-12 col-md-12 col-lg-12'>
                  <h1 className="d-flex justify-content-center fs-1">
                    {
                      tabDefault === 'TotalChargeBack' ?
                        (
                          lifeTimeData && lifeTimeData.dispute && lifeTimeData.dispute.totaldispute ? lifeTimeData.dispute.totaldispute : '0'
                        ) :
                        tabDefault === 'Percentage' ?
                          (
                            lifeTimeData && lifeTimeData.dispute && lifeTimeData.dispute.disputeData ? lifeTimeData.dispute.disputeData : '0'
                          ) : tabDefault === 'TotalChargebackAmount' ?
                            (
                              <span>
                                <i className="bi bi-currency-dollar fs-3" />
                                {
                                  lifeTimeData && lifeTimeData.dispute && lifeTimeData.dispute.totaldisputeAmount ? lifeTimeData.dispute.totaldisputeAmount : '0'
                                }
                              </span>
                            ) : null
                    }
                  </h1>
                  <div className='d-flex justify-content-center mb-4'>{riskLevel && riskLevel.disputeRate ? riskLevel.disputeRate : '0'}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <AMLdashboard />
    </>
  )
}

const mapStateToProps = (state) => {
  const { PercentageAMLStore } = state

  return {
    PercentageAMLResponce: PercentageAMLStore && PercentageAMLStore.PercentageAMLResponce ? PercentageAMLStore && PercentageAMLStore.PercentageAMLResponce : '--'
  }
}
const mapDispatchToProps = (dispatch) => ({
  getTransactionDispatch: (params) => dispatch(TransactionActions.getTransactionlist(params)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Scores)