import React, { useState } from "react";
import Chart from "react-apexcharts";

export function ChartWidget({ className, filterType, PercentageData }) {
    const strokeColor = "#D13647";
    var s= [
        {
            name: 'Life Time',
            data: [4, 2, 4, 6, 2]
        },
        {
            name: '90 days',
            data: [6, 3, 4, 3, 4]
        },
        {
            name: '60 days',
            data: [6, 3, 4, 3, 4]
        },
        {
            name: '30 days',
            data: [6, 3, 4, 3, 4]
        }
    ];
    var v = {
        chart: {
            height: 350,
            type: 'bar',
            stacked: true,
            toolbar: {
                show: false
            }
        },
        colors: ['#13d820', '#f7eb04', '#000000', '#eeeeee', '#808080'],
        dataLabels: {
            style: {
                colors: ['#000']
            }
        },
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            categories: ["1", "2", "3", "4", "5"]
        },
        yaxis: {
            max: 10,
            labels: {
                show: true,
                trim: false
            }
        }
    }
    return (
        <div className={`card card-custom bg-gray-100 ${className}`}>
            <div className="card-header border-0 py-5">
                {/* <h3 className="card-title font-weight-bolder capitalize">{ filterType==="year"? "Yearly" : filterType} Transaction Status</h3> */}
                <h3 className="card-title font-weight-bolder capitalize">Defect Summary</h3>

                <p className="text-muted">...</p>
            </div>
            <div className="card-body p-0 position-relative overflow-hidden">
                <Chart
                    series={s}
                    options={v}
                    type="bar"
                    height={340}
                />
            </div>
        </div>
    );
}
