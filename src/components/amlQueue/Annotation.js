import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { AMLqueueActions, AMLAnnatationActions } from '../../store/actions'
import { connect } from 'react-redux'
import moment from 'moment'
import { RISKSTATUS } from '../../utils/constants'

function RecentTransaction(props) {
  const {
    getAMLqueuelistDispatch,
    className,
    AnnatationData,
    loading,
    AnnatationAMLDispatch,
    currentId
  } = props

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getAMLqueuelistDispatch(params)
    if (currentId) {
      AnnatationAMLDispatch(currentId)
    }
  }, [])

  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)

  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getAMLqueuelistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getAMLqueuelistDispatch(params)
    }
  }

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Transaction Date</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Transaction ID</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Risk Status</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Type</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Reason</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      AnnatationData &&
                        AnnatationData.data
                        ? (
                          AnnatationData.data.map((item, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  {moment(item.updatedAt ? item.updatedAt : "--").format('DD/MM/YYYY')}
                                </td>
                                <td className="ellipsis">
                                  {item.transactionId ? item.transactionId : '--'}
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[item.riskStatus && item.riskStatus]}`}>
                                    {item.riskStatus ? item.riskStatus : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  {item.rejectType ? item.rejectType : '--'}
                                </td>
                                <td className="ellipsis">
                                {item.reason ? item.reason : '--'}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { AMLAnnatationAStore } = state;
  return {
    loading: AMLAnnatationAStore && AMLAnnatationAStore.loading ? AMLAnnatationAStore.loading : false,
    AnnatationData: AMLAnnatationAStore && AMLAnnatationAStore.AnnatationData ? AMLAnnatationAStore.AnnatationData : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getAMLqueuelistDispatch: (params) => dispatch(AMLqueueActions.getAMLqueuelist(params)),
  AnnatationAMLDispatch: (params) => dispatch(AMLAnnatationActions.AnnatationAML(params))

})

export default connect(mapStateToProps, mapDispatchToProps)(RecentTransaction)
