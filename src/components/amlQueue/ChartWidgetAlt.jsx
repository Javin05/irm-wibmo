import React, { useMemo, useEffect, useState } from "react";
import ApexCharts from "apexcharts";
import Chart from "react-apexcharts";

export function ChartWidgetAlt() {
    const strokeColor = "#D13647";
    const [filterType, setFilterType] = useState("year")
    const [className, setClassName] = useState("danger")
    const [month, setMonth] = useState({
        series: [
            {
                name: "Approved",
                data: generateDayWiseTimeSeries(
                    new Date("11 Feb 2017 GMT").getTime(),
                    20,
                    {
                        min: 10,
                        max: 60
                    }
                )
            },
            {
                name: "Rejected",
                data: generateDayWiseTimeSeries(
                    new Date("11 Feb 2017 GMT").getTime(),
                    20,
                    {
                        min: 10,
                        max: 40
                    }
                )
            },
            {
                name: "Hold",
                data: generateDayWiseTimeSeries(
                    new Date("11 Feb 2017 GMT").getTime(),
                    20,
                    {
                        min: 10,
                        max: 20
                    }
                )
            },
            {
                name: "Pending",
                data: generateDayWiseTimeSeries(
                    new Date("11 Feb 2017 GMT").getTime(),
                    20,
                    {
                        min: 10,
                        max: 17
                    }
                )
            }
        ],
        options: {
            chart: {
                type: "area",
                height: 220,
                toolbar: {
                    autoSelected: "pan",
                    show: false
                }
                // stacked: true,
            },
            colors: ['#1BC5BD', '#F64E60', '#FFA800', '#8950FC'],
            dropShadow: {
                enabled: true,
                enabledOnSeries: undefined,
                top: 5,
                left: 0,
                blur: 3,
                color: strokeColor,
                opacity: 0.5
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: "smooth",
                show: true,
                width: 2,
            },
            fill: {
                type: "gradient",
                gradient: {
                    opacityFrom: 0.2,
                    opacityTo: 0.8
                }
            },
            legend: {
                position: "bottom",
                horizontalAlign: "center"
            },
            xaxis: {
                title: { text: "Transaction Status" },
                type: "datetime",
                labels: {
                    format: 'MMM dd'
                },
            },
            yaxis: {
                title: { text: "Number of Transactions" }
            }
        }
    });
    const [day, setDay] = useState({
        series: [
            {
                name: "Approved",
                data: generateMonthWiseTimeSeries(
                    7,
                    {
                        min: 10,
                        max: 150
                    }
                )
            },
            {
                name: "Rejected",
                data: generateMonthWiseTimeSeries(
                    7,
                    {
                        min: 10,
                        max: 150
                    }
                )
            },
            {
                name: "Hold",
                data: generateMonthWiseTimeSeries(
                    7,
                    {
                        min: 10,
                        max: 150
                    }
                )
            },
            {
                name: "Pending",
                data: generateMonthWiseTimeSeries(
                    7,
                    {
                        min: 10,
                        max: 150
                    }
                )
            }
        ],
        options: {
            chart: {
                type: "area",
                height: 220,
                toolbar: {
                    autoSelected: "pan",
                    show: false
                }
                // stacked: true,
            },
            colors: ['#1BC5BD', '#F64E60', '#FFA800', '#8950FC'],
            dropShadow: {
                enabled: true,
                enabledOnSeries: undefined,
                top: 5,
                left: 0,
                blur: 3,
                color: strokeColor,
                opacity: 0.5
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: "smooth",
                show: true,
                width: 2,
            },
            fill: {
                type: "gradient",
                gradient: {
                    opacityFrom: 0.2,
                    opacityTo: 0.8
                }
            },
            legend: {
                position: "bottom",
                horizontalAlign: "center"
            },
            xaxis: {
                title: { text: "Transaction Status" },
                type: "datetime",
                labels: {
                    format: 'HH'
                },
                categories: [
                    "2022-04-21T00:00:00.000Z",
                    "2022-04-21T04:00:00.000Z",
                    "2022-04-21T08:00:00.000Z",
                    "2022-04-21T12:00:00.000Z",
                    "2022-04-21T16:00:00.000Z",
                    "2022-04-21T20:00:00.000Z",
                    "2022-04-21T23:00:00.000Z",
                ]
            },
            yaxis: {
                title: { text: "Number of Transactions" }
            }
        }
    });
    const [yeard, setYear] = useState({
        series: [
            {
                name: "Approved",
                data: generateMonthWiseTimeSeries(
                    12,
                    {
                        min: 10,
                        max: 150
                    }
                )
            },
            {
                name: "Rejected",
                data: generateMonthWiseTimeSeries(
                    12,
                    {
                        min: 10,
                        max: 150
                    }
                )
            },
            {
                name: "Hold",
                data: generateMonthWiseTimeSeries(
                    12,
                    {
                        min: 10,
                        max: 150
                    }
                )
            },
            {
                name: "Pending",
                data: generateMonthWiseTimeSeries(
                    12,
                    {
                        min: 10,
                        max: 150
                    }
                )
            }
        ],
        options: {
            chart: {
                type: "area",
                height: 220,
                toolbar: {
                    autoSelected: "pan",
                    show: false
                }
                // stacked: true,
            },
            colors: ['#1BC5BD', '#F64E60', '#FFA800', '#8950FC'],
            dropShadow: {
                enabled: true,
                enabledOnSeries: undefined,
                top: 5,
                left: 0,
                blur: 3,
                color: strokeColor,
                opacity: 0.5
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: "smooth",
                show: true,
                width: 2,
            },
            fill: {
                type: "gradient",
                gradient: {
                    opacityFrom: 0.2,
                    opacityTo: 0.8
                }
            },
            legend: {
                position: "bottom",
                horizontalAlign: "center"
            },
            xaxis: {
                title: { text: "Transaction Status" },
                type: "datetime",
                labels: {
                    format: 'MMM'
                },
                categories: [
                    "2022-01-01T00:00:00.000Z",
                    "2022-02-01T00:00:00.000Z",
                    "2022-03-01T00:00:00.000Z",
                    "2022-04-01T00:00:00.000Z",
                    "2022-05-01T00:00:00.000Z",
                    "2022-06-01T00:00:00.000Z",
                    "2022-07-01T00:00:00.000Z",
                    "2022-08-01T00:00:00.000Z",
                    "2022-09-01T00:00:00.000Z",
                    "2022-10-01T00:00:00.000Z",
                    "2022-11-01T00:00:00.000Z",
                    "2022-12-01T00:00:00.000Z",
                ]
                //categories: ["Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            },
            yaxis: {
                title: { text: "Number of Transactions" }
            }
        }
    });
    const ToggleChart = (type) =>{
        setFilterType(type)
    }

    let rdata=[
      ["Metrics", "Total Sales", "Chargeback", "Refund", "Disputes"],
      ["Sun", 1000, 2000, 3000, 4000, 6000],
      ["Mon", 1000, 2000, 3000, 4000, 6000],
      ["Tue", 1000, 2000, 3000, 4000, 6000],
      ["Wed", 1000, 2000, 3000, 4000, 6000],
      ["Thu", 1000, 2000, 3000, 4000, 6000],
      ["Fri", 1000, 2000, 3000, 4000, 6000],
      ["Sat", 1000, 2000, 3000, 4000, 6000]
    ];

    return (
        <div className={`card card-custom bg-gray-100 ${className}`}>
            <div className="card-header border-0 py-5">
                <h3 className="card-title font-weight-bolder capitalize">{ filterType==="year"? "Yearly" : filterType} Transaction Status</h3>
                <p className="text-muted">
                    <ul className="chart-tab">
                        <li onClick={()=>ToggleChart("today")} className={filterType==="today"? "active" : "none"}>Today</li>
                        <li onClick={()=>ToggleChart("monthly")} className={filterType==="monthly"? "active" : "none"}>Month</li>
                        <li onClick={()=>ToggleChart("year")} className={filterType==="year"? "active" : "none"}>Year</li>
                    </ul>
                </p>
            </div>
            <div className="card-body p-0 position-relative overflow-hidden">
                {
                    (() => {
                        if (filterType==="today") {
                            return <Chart
                                series={day.series}
                                options={day.options}
                                type="area"
                                height={260}
                              />
                        } else if (filterType==="monthly") {
                            return <Chart
                                series={month.series}
                                options={month.options}
                                type="area"
                                height={260}
                              />
                        } else if(filterType==="year"){
                            return <Chart
                                series={rdata}
                                options={yeard.options}
                                type="area"
                                height={260}
                              />
                        }
                    })()
                }
            </div>
        </div>
    );
}

const getColorArray = (length) => {
    let array = [];
    for (let n = 0; n < length; n++) {
        array.push(
            "#" + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
        );
    }

    return array;
}
const generateDayWiseTimeSeries = function (baseval, count, yrange) {
    let i = 0;
    let series = [];
    while (i < count) {
        let x = baseval;
        let y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push([x, y]);
        baseval += 86400000;
        i++;
    }

    return series;
}
const generateTimeWiseTimeSeries = function (baseval, count, yrange) {
    let i = 0;
    let series = [];
    while (i < count) {
        let x = Math.floor(Math.random() * (750 - 1 + 1)) + 1;
        let y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
        let z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;

        series.push([x, y, z]);
        baseval += 86400000;
        i++;
    }

    return series;
}
const generateMonthWiseTimeSeries = function (count, yrange) {
    let i = 0;
    let series = [];
    while (i < count) {
        //let x = baseval;
        let y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push(y);
        //baseval += 86400000;
        i++;
    }
    return series;
}
