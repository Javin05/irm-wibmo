/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useMemo } from "react";
import { Dropdown } from "react-bootstrap";
import objectPath from "object-path";
import ApexCharts from "apexcharts";
import { useHtmlClassService } from "../../theme/theme-settings/_core/MetronicLayout";
import { Chart } from "react-google-charts";
import Spinner from 'react-bootstrap/Spinner'

export function ChartWidget1(props) {
    const { loading, PercentageData } = props

    // const data = [
    //     ["Metrics", "Total Sales", "Chargeback", "Refund", "Disputes"],
    //     ["Lifetime", 8175000, 8008000, 900000, 980000],
    //     ["30 Days", 3792000, 3694000, 900000, 764323],
    //     ["60 Days", 2695000, 2896000, 900000, 45555],
    //     ["90 Days", 2099000, 1953000, 900000, 90008]
    // ];
    const options = {
        title: "",
        chartArea: { width: "72%" },
        isStacked: true,
        hAxis: {
            title: "Total Transactions",
            minValue: 0,
        },
        vAxis: {
            title: "Transactions",
        },
        colors: ['#65DAD6', '#F2A5AD', '#BFA3F4', '#F4D59A'],
    };

    let mainData = [];
    let LabelsArray = ["Metrics", "Total Sales", "Chargeback", "Refund", "Disputes"];
    let lifetimeArray =["Lifetime"];
    let thirtyArray =["7 Days"];
    let sixtyArray =["1 Month"];
    let ninetyArray =["1 Year"];
    if( PercentageData && PercentageData.length !== 0){
        mainData.push(LabelsArray);
        Object.keys(PercentageData).forEach(function(key) {
            if(key !== 'riskLevel'){
                switch(key) {
                    case 'lifeTime':
                        lifetimeArray.push(PercentageData[key]['sales'].totalSalesAmount);
                        lifetimeArray.push(PercentageData[key]['chargeBack'].totalChargebackAmount);
                        lifetimeArray.push(PercentageData[key]['refund'].totalrefundAmount);
                        lifetimeArray.push(PercentageData[key]['dispute'].totaldisputeAmount);
                        mainData.push(lifetimeArray);
                        return;

                    case 'sevenDays':
                        thirtyArray.push(PercentageData[key]['sales'].totalSalesAmount);
                        thirtyArray.push(PercentageData[key]['chargeBack'].totalChargebackAmount);
                        thirtyArray.push(PercentageData[key]['refund'].totalrefundAmount);
                        thirtyArray.push(PercentageData[key]['dispute'].totaldisputeAmount);
                        mainData.push(thirtyArray);

                        return;

                    case 'oneMonth':
                        sixtyArray.push(PercentageData[key]['sales'].totalSalesAmount);
                        sixtyArray.push(PercentageData[key]['chargeBack'].totalChargebackAmount);
                        sixtyArray.push(PercentageData[key]['refund'].totalrefundAmount);
                        sixtyArray.push(PercentageData[key]['dispute'].totaldisputeAmount);
                        mainData.push(sixtyArray);
                        return;

                    case 'oneYear':
                        ninetyArray.push(PercentageData[key]['sales'].totalSalesAmount);
                        ninetyArray.push(PercentageData[key]['chargeBack'].totalChargebackAmount);
                        ninetyArray.push(PercentageData[key]['refund'].totalrefundAmount);
                        ninetyArray.push(PercentageData[key]['dispute'].totaldisputeAmount);
                        mainData.push(ninetyArray);
                        return;
                }
            }
        });
    }

    return (
        <>
            <div className={`card card-custom card-stretch card-stretch-half gutter-b`} style={{marginTop:"30px"}}>
                <div className="card-header border-0 pt-5">
                    <h3 className="card-title font-weight-bolder">
                        Transactions
                    </h3>
                </div>    
                <div className="card-body d-flex flex-column p-0">
                    {
                        (() => {
                            if (loading !== false) {
                                if (mainData.length) {
                                    return <Chart
                                        chartType="ColumnChart"
                                        width="100%"
                                        height="400px"
                                        data={mainData}
                                        options={options}
                                    />
                                } else {
                                  return <p>Data Not Available</p>
                                }
                            } else {
                                return <div className="d-flex flex-column" style={{height:"400px"}}>
                                    <span className="text-dark-75 font-weight-bolder font-size-h3 text-center" style={{height:"400px"}}>
                                        <Spinner animation="border" size="sm" /> 
                                    </span>
                                </div>
                            }
                        })()
                    }
                </div>
            </div>
        </>
    )
}