import React, { useState } from "react";
import Chart from "react-apexcharts";

const DefectSummary=(props)=> {
	const { loading, PercentageData } = props
    const [currentView, setCurrentView] = useState("chart")
    const [chartFilter, setChartFilter] = useState("lifetime")
    const [title, setTitle] = useState("Thirty Days")

    let lifeTimeData =[];
    let ninetyDaysData =[];
    let sixtyDaysData =[];
    let thirtyDaysData =[];
    if( PercentageData && PercentageData.length !== 0){
        Object.keys(PercentageData).forEach(function(key) {
            switch(key) {

                case 'ninetyDays':
                    ninetyDaysData.push({fillColor: "#F44336", name:"Ninety Days", data: [PercentageData["ninetyDays"]['sales'].totalSalesAmount, PercentageData["ninetyDays"]['chargeBack'].totalChargebackAmount, PercentageData["ninetyDays"]['refund'].totalrefundAmount, PercentageData["ninetyDays"]['dispute'].totaldisputeAmount]})
                    return;

                case 'sixtyDays':
                    sixtyDaysData.push({fillColor: "#f9af15", name:"Sixty Days", data: [PercentageData["sixtyDays"]['sales'].totalSalesAmount, PercentageData["sixtyDays"]['chargeBack'].totalChargebackAmount, PercentageData["sixtyDays"]['refund'].totalrefundAmount, PercentageData["sixtyDays"]['dispute'].totaldisputeAmount]})
                    return;

                case 'thirtyDays':
                    thirtyDaysData.push({fillColor: "#F4D59A", name:"Thirty Days", data: [PercentageData["thirtyDays"]['sales'].totalSalesAmount, PercentageData["thirtyDays"]['chargeBack'].totalChargebackAmount, PercentageData["thirtyDays"]['refund'].totalrefundAmount, PercentageData["thirtyDays"]['dispute'].totaldisputeAmount]})
                    return;

					case 'lifeTime':
						lifeTimeData.push({fillColor: "#00FA9A", name:"Ninety Days", data: [PercentageData["lifeTime"]['sales'].totalSalesAmount, PercentageData["lifeTime"]['chargeBack'].totalChargebackAmount, PercentageData["lifeTime"]['refund'].totalrefundAmount, PercentageData["lifeTime"]['dispute'].totaldisputeAmount]})
						return;
            }                
        });
    }

    const sevenDays = PercentageData && PercentageData.thirtyDays ? PercentageData.thirtyDays : null
    const oneMonth = PercentageData && PercentageData.sixtyDays ? PercentageData.sixtyDays : null
    const oneYear = PercentageData && PercentageData.ninetyDays ? PercentageData.ninetyDays : null
    const lifeTime = PercentageData && PercentageData.lifeTime ? PercentageData.lifeTime : null

    const ToggleDeffect = (e, tab) =>{
    	e.preventDefault()
        setChartFilter(tab)
    }
    var s= [
        {
            name: 'Life Time',
            data: [4, 2, 4, 6, 2]
        },
        {
            name: '90 days',
            data: [6, 3, 4, 3, 4]
        },
        {
            name: '60 days',
            data: [6, 3, 4, 3, 4]
        },
        {
            name: '30 days',
            data: [6, 3, 4, 3, 4]
        }
    ];
    var v = {
        chart: {
            height: 350,
            type: 'bar',
            stacked: true,
            toolbar: {
                show: true,
                tools: {
                    download: true,
                    selection: false,
                    zoom: false,
                    zoomin: false,
                    zoomout: false,
                    pan: false,
                    reset : false,
                    //customIcons: []
                },
            }
        },
        colors: [ 
        	function ({ value, seriesIndex, dataPointIndex, w }) {
				if (dataPointIndex === 0) {
  					return "#4157e2"; // Lifetime
				} 
				else if(dataPointIndex === 1){
					return "#993404"; // Chargeback
				}
				else if(dataPointIndex === 2){
					return "#60c3f3"; // Refund
				}
				else {
  					return "#ec974f"; //Dispute
				}
			}
    	],
        dataLabels: {
            style: {
                colors: ['#000']
            }
        },
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            categories: ["Sales", "Chargeback", "Refund", "Dispute"]
        },
        yaxis: {
            labels: {
                show: false,
                trim: false
            }
        }
    }
    const List = () => {
	  	return <div className="table-responsive" style={{ paddingLeft : "20px", paddingRight : "20px"}}>
                	<table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                		<thead className='fw-bolder fs-8 text-gray-800'>
		                    <tr>
		                      <th>
		                        <div className="d-flex">
		                          <span>Metrics</span>
		                        </div>
		                      </th>
		                      <th>
		                        <div className="d-flex">
		                          <span>30 Days</span>
		                        </div>
		                      </th>
		                      <th>
		                        <div className="d-flex">
		                          <span>60 Days</span>
		                        </div>
		                      </th>
		                      <th>
		                        <div className="d-flex">
		                          <span>90 Days</span>
		                        </div>
		                      </th>
							  <th>
		                        <div className="d-flex">
		                          <span>Life Time</span>
		                        </div>
		                      </th>
		                    </tr>
		                </thead>
		                <tbody className='fs-8'>
		                    <>
		                        <tr>
		                            <td className="ellipsis">
		                                Total Sales
		                            </td>
		                            <td className="ellipsis">
		                                ${sevenDays && sevenDays.sales && sevenDays.sales.totalSalesAmount}
		                            </td>
		                            <td className="ellipsis">
		                                ${oneMonth && oneMonth.sales && oneMonth.sales.totalSalesAmount}
		                            </td>
		                            <td className="ellipsis">
		                                ${oneYear && oneYear.sales && oneYear.sales.totalSalesAmount}
		                            </td>
									<td className="ellipsis">
		                                ${lifeTime && lifeTime.sales && lifeTime.sales.totalSalesAmount}
		                            </td>
		                        </tr>
		                      <tr >
		                        <td className="ellipsis">
		                          Chargeback
		                        </td>
		                        <td className="ellipsis">
		                          ${sevenDays && sevenDays.chargeBack && sevenDays.chargeBack.totalChargebackAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${oneMonth && oneMonth.chargeBack && oneMonth.chargeBack.totalChargebackAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${oneYear && oneYear.chargeBack && oneYear.chargeBack.totalChargebackAmount}
		                        </td>
								<td className="ellipsis">
		                        ${lifeTime && lifeTime.chargeBack && lifeTime.chargeBack.totalChargebackAmount}
		                        </td>
		                      </tr>
		                      <tr>
		                        <td className="ellipsis">
		                          Refund
		                        </td>
		                        <td className="ellipsis">
		                          ${sevenDays && sevenDays.refund && sevenDays.refund.totalrefundAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${oneMonth && oneMonth.refund && oneMonth.refund.totalrefundAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${oneYear && oneYear.refund && oneYear.refund.totalrefundAmount}
		                        </td>
								<td className="ellipsis">
		                        ${lifeTime && lifeTime.refund && lifeTime.refund.totalrefundAmount}
		                        </td>
		                      </tr>
		                      <tr>
		                        <td className="ellipsis">
		                          Disputes
		                        </td>
		                        <td className="ellipsis">
		                          ${sevenDays && sevenDays.dispute && sevenDays.dispute.totaldisputeAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${oneMonth && oneMonth.dispute && oneMonth.dispute.totaldisputeAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${oneYear && oneYear.dispute && oneYear.dispute.totaldisputeAmount}
		                        </td>
		                        <td className="ellipsis">
		                        ${lifeTime && lifeTime.dispute && lifeTime.dispute.totaldisputeAmount}
		                        </td>
		                      </tr>
		                    </>
		                </tbody>
		            </table>
              	</div>
	}
	const ChartRenderer = () => {
	  	if (chartFilter==="lifetime") {
          	return <Chart
                series={lifeTimeData}
                options={v}
                type="bar"
                height={340}
            />
        }
        else if (chartFilter==="30_days") {
          	return <Chart
                series={thirtyDaysData}
                options={v}
                type="bar"
                height={340}
            />
        } 
        else if (chartFilter==="60_days") {
          	return <Chart
                series={sixtyDaysData}
                options={v}
                type="bar"
                height={340}
            />
        } 
        else if(chartFilter==="90_days") {
          	return <Chart
                series={ninetyDaysData}
                options={v}
                type="bar"
                height={340}
            />
        }
	}

    return (
        <div className={`card card-custom bg-gray-100`}>
            <div className="card-header border-0 py-5">
                <h3 className="card-title font-weight-bolder capitalize">Defect Summary</h3>
                <p className="text-muted">...</p>
            </div>
            <div className="card-header border-0 py-5">
                <ul className="account-tab">
                	<li onClick={()=>setCurrentView("list")} className={currentView==="list"? "active" : "none"}>List</li>
                	<li onClick={()=>setCurrentView("chart")} className={currentView==="chart"? "active" : "none"}>Chart</li>
                </ul>
                { currentView === "chart"? (
                	    <ul className="chart-tab">
		                	<li onClick={(e)=>ToggleDeffect(e, "30_days")} className={chartFilter==="30_days"? "active" : "none"}>30 Days</li>
		                	<li onClick={(e)=>ToggleDeffect(e, "60_days")} className={chartFilter==="60_days"? "active" : "none"}>60 Days</li>
		                	<li onClick={(e)=>ToggleDeffect(e, "90_days")} className={chartFilter==="90_days"? "active" : "none"}>90 Days</li>
							<li onClick={(e)=>ToggleDeffect(e, "lifetime")} className={chartFilter==="lifetime"? "active" : "none"}>Life Time</li>
		                </ul>
                	) : null }

            </div>
            <div className="card-body p-0 position-relative overflow-hidden">
            	{ currentView==="list"? (
            			<List />
            		) : PercentageData !== null ? (
            				<ChartRenderer />
                        ) : <div>Not Available</div>           			            		
            	}                
            </div>
        </div>
    );
}

export default DefectSummary;
