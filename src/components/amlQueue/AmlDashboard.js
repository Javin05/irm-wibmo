import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { RISKSTATUS } from '../../utils/constants'
import { useLocation } from 'react-router-dom'
import { AMLGetIdActions } from '../../store/actions'
import Scores from './scores'
import Investigation from './InVestigationTab'
import ChangeStatus from './StatusCahnge'
import DefectSummary from './DefectSummary'
import {ChartWidget1} from './ChartWidget1'
import moment from 'moment'

function AMLdashboard(props) {
  const {
    AMLIdDetailsReponse,
    getAMLIdDispatch,
    PercentageAMLResponce,
    loading
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]

  useEffect(() => {
    if (currentId) {
      getAMLIdDispatch(currentId)
    }
  }, [currentId])

  const AMLIdDetails = AMLIdDetailsReponse && AMLIdDetailsReponse.data ? AMLIdDetailsReponse.data : '--'
  const PercentageData = PercentageAMLResponce && PercentageAMLResponce.data ? PercentageAMLResponce && PercentageAMLResponce.data : null
  const salesValue = PercentageData && PercentageData.lifeTime && PercentageData.lifeTime.sales ? PercentageData.lifeTime.sales : '--'

  return (
    <>

      <div className='col-md-12 card card-xl-stretch mb-xl-8 mt-8'>
        <div className='card-header border-0'>
          <div className='card-title align-items-start flex-column '>
            <div className='d-flex align-items-center fw-boldest my-1 fs-2'>Case Details - Case ID #{AMLIdDetails && AMLIdDetails.riskId ? AMLIdDetails.riskId : '--'}
              <span
                className={`badge ml-2 ${RISKSTATUS[AMLIdDetails.riskStatus && AMLIdDetails.riskStatus]} `}
              >
                {
                  AMLIdDetails.riskStatus
                }
              </span>
            </div>
          </div>

          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 mt-4' >
              <ChangeStatus />
            </div>
          </div>
          {/* <div className="card-toolbar">
            <Link to='/merchant-login'className=" badge badge-light-success">
                Next Case
            </Link>
        </div> */}
        </div>
        <div className="separator separator-dashed my-3" />
        <div className='card-body pt-0'>
          <div className='row g-5 g-xl-8'>
            <div className='col-sm-3 col-md-3 col-lg-3'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-sm-6 col-md-6 col-lg-6'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Id
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Queue
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Assigned To
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Status
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Date Received
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Age
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Type
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Fraud Type
                      </div>
                    </div>
                    <div className='col-sm-6 col-md-6 col-lg-6'>
                      <div className='text-700 fw-bold  pl-3 '>
                        IRM{AMLIdDetails && AMLIdDetails.riskId ? AMLIdDetails.riskId : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        AML_Q
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        Unassigned
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        New
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        {moment(AMLIdDetails.updatedAt ? AMLIdDetails.updatedAt : "--").format('DD/MM/YYYY')}
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        1
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        System/Manual
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        NA
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-3 col-md-3 col-lg-3'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-sm-6 col-md-6 col-lg-6'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Business Name:
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Business URL:
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Business Email:
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Business Phone
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Business Address:
                      </div>
                    </div>
                    <div className='col-sm-6 col-md-6 col-lg-6'>
                      <div className='text-700 fw-bold  pl-3 ellipsis'>
                        {AMLIdDetails && AMLIdDetails.companyName ? AMLIdDetails.companyName : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        {AMLIdDetails && AMLIdDetails.businessUrl ? AMLIdDetails.businessUrl : '--'}
                      </div>
                      <div className='text-700 fw-bold Id-response-Link  pl-3 ellipsis'
                      >
                        {AMLIdDetails && AMLIdDetails.businessEmail ? AMLIdDetails.businessEmail : '--'}
                      </div>
                      <div className='text-700 fw-bold Id-response-Link  pl-3 ellipsis'
                      >
                        {AMLIdDetails && AMLIdDetails.businessPhone ? AMLIdDetails.businessPhone : '--'}
                      </div>
                      <div className='text-700 fw-bold Id-response-Link  pl-3 ellipsis'
                      >
                        {AMLIdDetails && AMLIdDetails.businessAddress ? AMLIdDetails.businessAddress : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-3 col-md-3 col-lg-3'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-sm-9 col-md-9 col-lg-9'>
                      <div className='text-gray-700 fw-bold pl-3 ml-2 '>
                        Average Monthly Volume:
                      </div>
                      <div className='text-gray-700 fw-bold pl-3 ml-2 '>
                        Average Transaction Amount:
                      </div>
                      <div className='text-gray-700 fw-bold ellipsis  pl-3 ml-2 '>
                        GSTIN
                      </div>
                      <div className='text-gray-700 fw-bold ellipsis  pl-3 ml-2 '>
                        CIN
                      </div>
                      <div className='text-gray-700 fw-bold ellipsis  pl-3 ml-2 '>
                        PAN
                      </div>
                      <div className='text-gray-700 fw-bold ellipsis  pl-3 ml-2 '>
                        PAN Owner's Name
                      </div>
                    </div>
                    <div className='col-sm-3 col-md-3 col-lg-3'>
                      <div className='text-700 fw-bold  pl-3'>
                        {AMLIdDetails && AMLIdDetails.averageMonthlyVolume ? AMLIdDetails.averageMonthlyVolume : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        {AMLIdDetails && AMLIdDetails.averageTransactionAmount ? AMLIdDetails.averageTransactionAmount : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        {AMLIdDetails && AMLIdDetails.gstIn ? AMLIdDetails.gstIn : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        {AMLIdDetails && AMLIdDetails.cin ? AMLIdDetails.cin : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 ellipsis'>
                        {AMLIdDetails && AMLIdDetails.pan ? AMLIdDetails.pan : '--'}
                      </div>
                      <div className='text-700 fw-bold  pl-3 ellipsis'>
                        {AMLIdDetails && AMLIdDetails.panOwnerName ? AMLIdDetails.panOwnerName : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-3 col-md-3 col-lg-3'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-sm-8 col-md-8 col-lg-8'>
                      <div className='text-gray-700 fw-bold   pl-3 ml-2 '>
                        Actual Avg Monthly Volume
                      </div>
                      <div className='text-gray-700 fw-bold   pl-3 ml-2 '>
                        Actual Transaction Amount
                      </div>
                      <div className='text-gray-700 fw-bold   pl-3 ml-2 '>
                        Total Transaction Amount
                      </div>
                    </div>
                    <div className='col-sm-3 col-md-3 col-lg-3'>
                      <div className='text-700 fw-bold ellipsis pl-3 '>
                        {salesValue.averageMonthlyTransactionVolume ? salesValue.averageMonthlyTransactionVolume : '--'}
                      </div>
                      <div className='text-700 fw-bold ellipsis pl-3 '>
                        {salesValue.averageMonthlyTransactionAmount ? salesValue.averageMonthlyTransactionAmount : '--'}
                      </div>
                      <div className='text-700 fw-bold ellipsis pl-3 '>
                        {salesValue.totalSalesAmount ? salesValue.totalSalesAmount : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6 col-md-6 col-lg-6'>
          <Scores currentId={currentId} />
        </div>
        <div className='col-sm-6 col-md-6 col-lg-6' style={{ backgroundColor: 'white', borderRadius: '0.45rem' }}>
          <Investigation PercentageData={PercentageData} PersonData={AMLIdDetails} />
        </div>
      </div>
{/*      <div className='row my-bar-chart'>
        <div className='col-sm-12 col-md-12 col-lg-12'>
          <ChartWidget1 
            loading={loading}
            PercentageData={PercentageData}
            />
        </div>
      </div>*/}

       <div className='row my-bar-chart mt-6'>
        <div className='col-sm-12 col-md-12 col-lg-12'>
            <DefectSummary 
                loading={loading}
                PercentageData={PercentageData}/>
        </div>
      </div> 


    </>
  )

}

const mapStateToProps = (state) => {
  const { editAMLStore, PercentageAMLStore } = state

  return {
    loading: state && state.editAMLStore && state.editAMLStore.loading,
    AMLIdDetailsReponse: editAMLStore && editAMLStore.AMLIdDetail ? editAMLStore.AMLIdDetail : {},
    PercentageAMLResponce: PercentageAMLStore && PercentageAMLStore.PercentageAMLResponce ? PercentageAMLStore && PercentageAMLStore.PercentageAMLResponce : '--'
  }
}

const mapDispatchToProps = (dispatch) => ({
  getAMLIdDispatch: (id) => dispatch(AMLGetIdActions.getAMLIdDetails(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(AMLdashboard)
