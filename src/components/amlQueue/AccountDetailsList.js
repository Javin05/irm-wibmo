import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { AMLqueueActions } from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage } from '../../utils/helper';
import { RISKSTATUS } from '../../utils/constants'
import moment from 'moment'

function AccountList(props) {
  const {
    getAMLqueuelistDispatch,
    className,
    AMLgetList,
    loading,
    PercentageAMLResponce,
    filterType,
    WMYResponcelists
  } = props

  const [limit, setLimit] = useState(25)

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getAMLqueuelistDispatch(params)
  }, [])

  const PercentageData = PercentageAMLResponce && PercentageAMLResponce.data ? PercentageAMLResponce && PercentageAMLResponce.data : '--'
  const sevenDays = WMYResponcelists && WMYResponcelists.data && WMYResponcelists.data.sevenDays ? WMYResponcelists.data.sevenDays : '--'
  const oneMonth = WMYResponcelists && WMYResponcelists.data && WMYResponcelists.data.last31Days ? WMYResponcelists.data.last31Days : '--'
  const MonthlyData = WMYResponcelists && WMYResponcelists.data && WMYResponcelists.data.monthly ? WMYResponcelists.data.monthly : '--'
  const oneYear = WMYResponcelists && WMYResponcelists.data && WMYResponcelists.data.yearly ? WMYResponcelists.data.yearly : '--'
  const lifeTime = PercentageData && PercentageData.lifeTime ? PercentageData.lifeTime : null

  const CARDTITLE = {
    "seven_days": 'Seven Days',
    "one_month": 'Monthly',
    "one_year": 'Year',
    "last31Days": 'Last 31 Days',
  }

  const DATE = {
    "seven_days": 'Date',
    "one_month": 'Month',
    "one_year": 'Year',
    "last31Days": 'Date',
  }

  return (
    <div className={`card ${className}`}>
      <h3 className='card-title mt-4'>
        {`${CARDTITLE[filterType]}`}
      </h3>
      <div className='card-body py-3'>
        <div className="table-responsive">
          <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
            <thead className='fw-bolder fs-8 text-gray-800'>
              <tr>
                <th>
                  <div className="d-flex">
                    <span>
                      {`${DATE[filterType]}`}
                    </span>
                  </div>
                </th>
                <th>
                  <div className="d-flex">
                    <span>Chargeback Amount</span>
                  </div>
                </th>
                <th>
                  <div className="d-flex">
                    <span>Dispute Amount</span>
                  </div>
                </th>
                <th>
                  <div className="d-flex">
                    <span>Refund Amount</span>
                  </div>
                </th>
                <th>
                  <div className="d-flex">
                    <span>Total Amount</span>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody className='fs-8'>
              {
                filterType === 'seven_days'
                  ? (
                    sevenDays &&
                    sevenDays.map((item, id) => {
                      return (
                        <>
                          <tr
                            key={id}
                            style={
                              id === 0
                                ? { borderColor: "black" }
                                : { borderColor: "white" }
                            }
                          >
                            <td className="ellipsis">
                              {moment(item.date ? item.date : "--").format('DD/MM/YYYY')}
                            </td>
                            <td className="ellipsis">
                              {item.chargebackAmount ? item.chargebackAmount : "0"}
                            </td>
                            <td className="ellipsis">
                              {item.disputeAmount ? item.disputeAmount : "0"}
                            </td>
                            <td className="ellipsis">
                              {item.refundAmount ? item.refundAmount : "0"}
                            </td>
                            <td className="ellipsis">
                              {item.totalAmount ? item.totalAmount : "0"}
                            </td>
                          </tr>
                        </>
                      )
                    })
                  ) :
                  (
                    filterType === 'last31Days'
                      ? (
                        oneMonth &&
                        oneMonth.map((item, id) => {
                          return (
                            <>
                              <tr
                                key={id}
                                style={
                                  id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  {moment(item.date ? item.date : "--").format('DD/MM/YYYY')}
                                </td>
                                <td className="ellipsis">
                                  {item.chargebackAmount ? item.chargebackAmount : "0"}
                                </td>
                                <td className="ellipsis">
                                  {item.disputeAmount ? item.disputeAmount : "0"}
                                </td>
                                <td className="ellipsis">
                                  {item.refundAmount ? item.refundAmount : "0"}
                                </td>
                                <td className="ellipsis">
                                  {item.totalAmount ? item.totalAmount : "0"}
                                </td>
                              </tr>
                            </>
                          )
                        })
                      ) : (
                        filterType === 'one_month'
                          ? (
                            MonthlyData &&
                            MonthlyData.map((item, id) => {
                              return (
                                <>
                                  <tr
                                    key={id}
                                    style={
                                      id === 0
                                        ? { borderColor: "black" }
                                        : { borderColor: "white" }
                                    }
                                  >
                                    <td className="ellipsis">
                                      {item.monthName ? item.monthName : "0"}
                                    </td>
                                    <td className="ellipsis">
                                      {item.chargebackAmount ? item.chargebackAmount : "0"}
                                    </td>
                                    <td className="ellipsis">
                                      {item.disputeAmount ? item.disputeAmount : "0"}
                                    </td>
                                    <td className="ellipsis">
                                      {item.refundAmount ? item.refundAmount : "0"}
                                    </td>
                                    <td className="ellipsis">
                                      {item.totalAmount ? item.totalAmount : "0"}
                                    </td>
                                  </tr>
                                </>
                              )
                            })
                          ) :
                          (
                            filterType === 'one_year'
                              ? (
                                oneYear &&
                                oneYear.map((item, id) => {
                                  return (
                                    <>
                                      <tr
                                        key={id}
                                        style={
                                          id === 0
                                            ? { borderColor: "black" }
                                            : { borderColor: "white" }
                                        }
                                      >
                                        <td className="ellipsis">
                                          { item.year ? item.year : "--"}
                                        </td>
                                        <td className="ellipsis">
                                          {item.chargebackAmount ? item.chargebackAmount : "0"}
                                        </td>
                                        <td className="ellipsis">
                                          {item.disputeAmount ? item.disputeAmount : "0"}
                                        </td>
                                        <td className="ellipsis">
                                          {item.refundAmount ? item.refundAmount : "0"}
                                        </td>
                                        <td className="ellipsis">
                                          {item.totalAmount ? item.totalAmount : "0"}
                                        </td>
                                      </tr>
                                    </>
                                  )
                                })
                              ) :
                              (
                                null
                              )
                          )
                      )
                  )
              }

            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { AMLgetStore, PercentageAMLStore, AMLWMYAStore } = state;
  return {
    loading: AMLgetStore && AMLgetStore.loading ? AMLgetStore.loading : false,
    AMLgetList: AMLgetStore && AMLgetStore.AMLgetList ? AMLgetStore.AMLgetList : '',
    WMYResponcelists: AMLWMYAStore && AMLWMYAStore.WMYResponce ? AMLWMYAStore.WMYResponce : {},
    PercentageAMLResponce: PercentageAMLStore && PercentageAMLStore.PercentageAMLResponce ? PercentageAMLStore && PercentageAMLStore.PercentageAMLResponce : '--'
  }
}

const mapDispatchToProps = (dispatch) => ({
  getAMLqueuelistDispatch: (params) => dispatch(AMLqueueActions.getAMLqueuelist(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountList)