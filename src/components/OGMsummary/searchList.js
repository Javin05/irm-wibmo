import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { OGMSummaryActions } from '../../store/actions'
import { DATE } from '../../utils/constants'
import { DateSelector } from '../../theme/layout/components/DateSelector'
import moment from "moment"
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'

function SearchList(props) {
  const { getOGMSummarylistDispatch, loading, setCredFilterParams , setSearchData} = props
  const [error, setError] = useState({});
  const [show, setShow] = useState(false)
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [formData, setFormData] = useState({
    ogmSummaryId: '',
    ogm_interval: '',
    ogm_start_date: '',
    createdAtFrom: '',
    createdAtTo: '',
  })

  const [condition, setcondition] = useState(true)
  const [clear, setClear] = useState(false)

  const handleSearch = (e) => {
    // const errors = {}
    // if (_.isEmpty(formData.tag)) {
    //   errors.tag = 'Tag is required.'
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
      setShow(false)
      const UpDate = moment(formData.ogm_start_date).format("YYYY-MM-DD")
      const DpDate = moment(formData.createdAtFrom).format("YYYY-MM-DD")
      const DeFrDate = moment(formData.createdAtTo).format("YYYY-MM-DD")
      const params = {
        ogm_start_date: UpDate === 'Invalid date' ? '' : UpDate ,
        createdAtFrom:  DpDate  === 'Invalid date' ? '' : DpDate ,
        createdAtTo:  DeFrDate  === 'Invalid date' ? '' : DeFrDate ,
        ogmSummaryId: formData.ogmSummaryId,
        ogm_interval: formData.ogm_interval
      }
      getOGMSummarylistDispatch(params)
      setSearchData(params)
    // }
  }

  const handleChanges = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...error, [e.target.name]: '' })
  }

  const handleReset = () => {
    setFormData({
      uploadedDateFrom: clear,
      uploadedDateTo: new Date(),
    }
    )
    const params = {
      limit: 25,
      page: 1
    }
    getOGMSummarylistDispatch(params)
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-md btn-light-primary btn-responsive me-3 pull-right w-100px'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => setShow(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => setShow(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Ogm Summary Id :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Ogm Summary Id'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': formData.ogmSummaryId && error.ogmSummaryId },
                          {
                            'is-valid': formData.ogmSummaryId && !error.ogmSummaryId
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='ogmSummaryId'
                        autoComplete='off'
                        value={formData.ogmSummaryId || ''}
                      />
                      {errors && errors.ogmSummaryId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.ogmSummaryId}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Ogm Interval :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='ogm_interval'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.ogm_interval || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='WEEKLY'>WEEKLY</option>
                        <option value='MONTHLY'>MONTHLY</option>
                        <option value='QUARTERLY'>QUARTERLY</option>
                        <option value='ONE TIME'>ONE TIME</option>
                      </select>
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                    Ogm Start Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogm_start_date'
                        placeholder='Ogm Start Date'
                        className='form-control'
                        selected={formData.ogm_start_date || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, ogm_start_date: '' })
                          setFormData((values) => ({
                            ...values,
                            ogm_start_date: date
                          }))
                          setcondition(false)
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                    Created At From:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='createdAtFrom'
                        placeholder='Created At From'
                        className='form-control'
                        selected={formData.createdAtFrom || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, createdAtFrom: '' })
                          setFormData((values) => ({
                            ...values,
                            createdAtFrom: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Created At To:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='createdAtTo'
                        placeholder='Created At To'
                        className='form-control'
                        selected={formData.createdAtTo || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, createdAtTo: '' })
                          setFormData((values) => ({
                            ...values,
                            createdAtTo: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='form-group row mb-4 mt-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => handleSearch()}
                          disabled={loading}
                        >
                          {loading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Search'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => handleReset()}
                        >
                          Reset
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OGMsummaryStore } = state
  return {
    loading: OGMsummaryStore && OGMsummaryStore.loading ? OGMsummaryStore.loading : false,
  }
}

const mapDispatchToProps = dispatch => ({
  getOGMSummarylistDispatch: (params) => dispatch(OGMSummaryActions.getOGMSummarylist(params))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)