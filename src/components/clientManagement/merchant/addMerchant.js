import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import Step1 from './step1'
// import MidForm from './MidForm'
import { KTSVG } from '../../../theme/helpers'
import { useHistory, useLocation } from 'react-router-dom'
// import AddCrmForm from '../onboarding/merchantForm/AddCrmForm'
// import AddProcessorForm from '../onboarding/merchantForm/AddProcessorForm'
// import AddPayment from '../onboarding/merchantForm/AddPayment'
import ReactPaginate from 'react-paginate'
import { setLocalStorage, getLocalStorage, removeLocalStorage } from '../../../utils/helper'
import { midCRMActions, deletemidCRMActions, midCRMGetDetailsActions } from '../../../store/actions'
import { confirmationAlert, warningAlert, confirmAlert } from '../../../utils/alerts'
import { SWEET_ALERT_MSG, STATUS_RESPONSE, SAVE_CURRENT } from '../../../utils/constants'

function AddMerchant (props) {
  const {
    loading,
    mids,
    getmidCRM,
    getMidCrmDispatch,
    deletemidCRMDispatch,
    deletemidCRMStatus,
    deletemidCRMMessage,
    clearDeletemidCRMDispatch,
    getmidCRMDetailsDispatch
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update-merchant/')
  const id = url && url[1]
  const history = useHistory()
  const [activeTab, setActiveTab] = useState('MID_FORM')
  const [merchantForm, setMerchantForm] = useState(true)
  const [table, setTable] = useState(true)
  const [limit, setLimit] = useState(25)
  const [currentId, setCurrentMidId] = useState()
  const [setActivePageNumber] = useState(1)
  const [editMid, setEditMid] = useState(false)
  const currentClientID = getLocalStorage(SAVE_CURRENT.CLIENT_ID)
  const handleTabs = (tab) => {
    setActiveTab(tab)
  }

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  useEffect(() => {
    const midParam = {
      merchantId: id,
      clientId: currentClientID ? currentClientID : ""
    }
    // getMidCrmDispatch(midParam)
    return () => {
      setActiveTab('MID_FORM')
    }
  }, [])

  const onConfirm = () => {
    const midParam = {
      merchantId: id,
      clientId: currentClientID ? currentClientID : ""
    }
    // getMidCrmDispatch(midParam)
  }

  function onConfirmDelete (id) {
    // deletemidCRMDispatch(id)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_MID,
      'warning',
      'Yes',
      'No',
      () => { onConfirmDelete(id) },
      () => { }
    )
  }

  useEffect(() => {
    if (deletemidCRMStatus === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        deletemidCRMMessage,
        'success',
        'ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      // clearDeletemidCRMDispatch()
    } else if (deletemidCRMStatus === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        deletemidCRMMessage,
        '',
        'Ok'
      )
    }
    // clearDeletemidCRMDispatch()
  }, [deletemidCRMStatus])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      merchantId: id,
      clientId: currentClientID ? currentClientID : ""
    }
    setActivePageNumber(pageNumber)
    // getMidCrmDispatch(params)
  }

  const totalPages =
    getmidCRM && getmidCRM.count
      ? Math.ceil(parseInt(getmidCRM && getmidCRM.count) / limit)
      : 1

  return (
    <>
      <div>
        <div>
          <div className='d-flex justify-content-end mb-3 mr-5'>
            <button
              className='btn btn-sm btn-darkRed fa-pull-right'
              onClick={() => {
                history.goBack()
              }}
            >
              Back to Merchant
            </button>
          </div>
          <div
            className='accordion md-accordion accordion-2'
            id='accordionEx7'
            role='tablist'
            aria-multiselectable='true'
          >
            <div className='card bg-skyBlue'>
              <div
                className='card-header rgba-stylish-strong z-depth-1 mb-1'
                role='tab'
                id='heading2'
              >
                <div className='col-lg-6 my-auto'>
                  <div className='row'>
                    <div className='col-lg-6'>
                      <a
                        className='collapsed'
                        data-toggle='collapse'
                        data-parent='#accordionEx7'
                        href='#collapse2'
                        aria-expanded='false'
                        aria-controls='collapse2'
                        onClick={() => { setMerchantForm(val => !val) }}
                      >
                        <h5 className='text-dark fw-bolder fs-4 my-3'>
                          {merchantForm ? 'Hide' : 'Show'} Merchant Form{' '}
                          <i className={`fas fa-angle-${merchantForm ? 'up' : 'down'} rotate-icon`} />
                        </h5>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div
                id='collapse2'
                className='collapse show'
                role='tabpanel'
                aria-labelledby='heading2'
                data-parent='#accordionEx7'
              >
                <Step1 />
              </div>
            </div>
          </div>
        </div>
        <div className='d-flex justify-content-end col-md-12  mt-5 mb-5'>
          <div>
            <button
              className={`btn btn-sm btn-${table ? 'light-primary' : 'darkRed'}`}
              onClick={() => {
                removeLocalStorage(SAVE_CURRENT.MID_ID)
                if (table) {
                  setTable(false)
                  setEditMid(false)
                } else {
                  setTable(true)
                  const midParam = {
                    merchantId: id,
                    clientId: currentClientID ? currentClientID : ""
                  }
                  // getMidCrmDispatch(midParam)
                  setActiveTab('MID_FORM')
                }
              }}
            >
              {table ? (<KTSVG path='/media/icons/duotune/arrows/arr087.svg' />) : null}
              {table ? 'Add MID' : 'Back to MID'}
            </button>
          </div>
        </div>
        {table ? (
          <>
            <div className='table-responsive bg-white px-5 mt-8'>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start mt-3 ms-3'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3 mt-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
              <table className='table  table-striped border table-hover table-rounded gs-2 mt-6'>
                <thead>
                  <tr className='fw-bold fs-6 text-gray-800'>
                    <th className='min-w-100px text-center'>Action</th>
                    <th className='min-w-200px text-start'>
                      <div className='d-flex'>
                        <span>MID Number</span>
                      </div>
                    </th>
                    <th className='min-w-200px text-start'>
                      <div className='d-flex'>
                        <span>Descriptor name</span>
                      </div>
                    </th>
                    <th className='min-w-200px text-start'>
                      <div className='d-flex'>
                        <span>Descriptor ID</span>
                      </div>
                    </th>
                    <th className='min-w-200px text-start'>
                      <div className='d-flex'>
                        <span>Doing business as</span>
                      </div>
                    </th>
                    <th className='min-w-200px text-start'>
                      <div className='d-flex'>
                        <span>BIN</span>
                      </div>
                    </th>
                    <th className='min-w-200px text-start'>
                      <div className='d-flex'>
                        <span>CAID</span>
                      </div>
                    </th>

                  </tr>
                </thead>
                <tbody>
                  {
                    !loading
                      ? (
                          mids &&
                          mids.data
                            ? (
                                mids.data.map((mid, _id) => {
                                  return (
                                    <tr
                                      key={_id}
                                      style={
                                    _id === 0
                                      ? { borderColor: 'black' }
                                      : { borderColor: 'white' }
                                  }
                                    >
                                      <td className='pb-0 pt-3 text-start'>
                                        <div className='my-auto d-flex'>
                                          <button
                                            className='btn btn-icon btn-bg-light btn-icon-warning btn-active-color-warning btn-sm'
                                            onClick={() => {
                                              setTable(false)
                                              setEditMid(true)
                                              setCurrentMidId(mid._id)
                                              setLocalStorage(SAVE_CURRENT.MID_ID, mid._id)
                                            }}
                                          >
                                            {/* eslint-disable */}
                                        <KTSVG
                                          path="/media/icons/duotune/art/art005.svg"
                                          className="svg-icon-3"
                                        />
                                        {/* eslint-enable */}
                                          </button>
                                          <button
                                            className='btn btn-icon btn-bg-light btn-icon-danger btn-active-color-danger btn-sm ms-0'
                                            onClick={() => {
                                              onDeleteItem(mid._id)
                                            }}
                                          >
                                            {/* eslint-disable */}
                                        <KTSVG
                                          path="/media/icons/duotune/general/gen027.svg"
                                          className="svg-icon-3"
                                        />
                                        {/* eslint-enable */}
                                          </button>
                                        </div>
                                      </td>
                                      <td className='pb-0 pt-5  text-start'>
                                        {mid.midNumber ? mid.midNumber : '--'}
                                      </td>
                                      <td className='pb-0 pt-5  text-start'>
                                        {mid.descriptorName ? mid.descriptorName : '--'}
                                      </td>
                                      <td className='pb-0 pt-5  text-start'>
                                        {mid.descriptorId ? mid.descriptorId : '--'}
                                      </td>
                                      <td className='pb-0 pt-5  text-start'>
                                        {mid.doingBusinessAs ? mid.doingBusinessAs : '--'}
                                      </td>
                                      <td className='pb-0 pt-5  text-start'>
                                        {mid.bin ? mid.bin : '--'}
                                      </td>
                                      <td className='pb-0 pt-5  text-start'>
                                        {mid.caId ? mid.caId : '--'}
                                      </td>

                                    </tr>
                                  )
                                })
                              )
                            : (
                              <tr className='text-center py-3'>
                                <td colSpan='100%'>No record(s) found</td>
                              </tr>
                              )
                        )
                      : (
                        <tr>
                          <td colSpan='100%' className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </td>
                        </tr>
                        )
                  }
                </tbody>
              </table>
            </div>
            <div className='form-group row mb-4 mt-6'>
              <div className='col-lg-12 mb-4 align-items-end d-flex'>
                <div className='col-lg-12'>
                  <ReactPaginate
                    nextLabel='Next >'
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={3}
                    marginPagesDisplayed={2}
                    pageCount={totalPages}
                    previousLabel='< Prev'
                    pageClassName='page-item'
                    pageLinkClassName='page-link'
                    previousClassName='page-item'
                    previousLinkClassName='page-link'
                    nextClassName='page-item'
                    nextLinkClassName='page-link'
                    breakLabel='...'
                    breakClassName='page-item'
                    breakLinkClassName='page-link'
                    containerClassName='pagination'
                    activeClassName='active'
                    renderOnZeroPageCount={null}
                  />
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <nav className='pt-2'>
              <div
                className='nav nav-tabs nav-fill'
                id='nav-tab'
                role='tablist'
              >
                <a
                  className='nav-item nav-link active'
                  id='nav-midForm-tab'
                  data-toggle='tab'
                  href='#nav-midForm'
                  role='tab'
                  aria-controls='nav-midForm'
                  aria-selected='true'
                  onClick={() => {
                    handleTabs('MID_FORM')
                  }}
                >
                  MID
                </a>
                <a
                  className='nav-item nav-link'
                  id='nav-crm-tab'
                  data-toggle='tab'
                  href='#nav-crm'
                  role='tab'
                  aria-controls='nav-crm'
                  aria-selected='false'
                  onClick={() => {
                    handleTabs('CRM')
                  }}
                >
                  CRM
                </a>
                <a
                  className='nav-item nav-link'
                  id='nav-processor-tab'
                  data-toggle='tab'
                  href='#nav-processor'
                  role='tab'
                  aria-controls='nav-processor'
                  aria-selected='false'
                  onClick={() => {
                    handleTabs('PROCESSOR')
                  }}
                >
                  Payment Processor
                </a>
                <a
                  className='nav-item nav-link'
                  id='nav-gateway-tab'
                  data-toggle='tab'
                  href='#nav-gateway'
                  role='tab'
                  aria-controls='nav-gateway'
                  aria-selected='false'
                  onClick={() => {
                    handleTabs('PAYMENT')
                  }}
                >
                  Payment Gateway
                </a>
              </div>
            </nav>
            <div
              className='tab-content py-3 px-3 px-sm-0'
              id='nav-tabContent'
            >
              {/* <div
                className='tab-pane fade show active'
                id='nav-midForm'
                role='tabpanel'
                aria-labelledby='nav-midForm-tab'
              >
                {
                  activeTab === 'MID_FORM'
                    ? <MidForm
                        currentMerchantId={id}
                        editMode={editMid}
                        currentId={currentId}
                        setTable={setTable}
                        setCurrentId={setCurrentMidId}
                      />
                    : null
                }
              </div> */}
              {/* <div
                className='tab-pane fade'
                id='nav-crm'
                role='tabpanel'
                aria-labelledby='nav-crm-tab'
              >
                {activeTab === 'CRM' ? 
                  <AddCrmForm
                    currentMerchantId={id}
                    currentId={currentId}
                  />
                 : null}
              </div>
              <div
                className='tab-pane fade'
                id='nav-processor'
                role='tabpanel'
                aria-labelledby='nav-processor-tab'
              >
                {activeTab === 'PROCESSOR' ? 
                <AddProcessorForm
                  currentMerchantId={id}
                  currentId={currentId}
                 />
                 : null}
              </div>
              <div
                className='tab-pane fade'
                id='nav-gateway'
                role='tabpanel'
                aria-labelledby='nav-gateway-tab'
              >
                {activeTab === 'PAYMENT' ? 
                <AddPayment
                  currentMerchantId={id}
                  currentId={currentId}
                 />
                 : null}
              </div> */}
            </div>
          </>
        )}
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  mids: state && state.midCRMStore && state.midCRMStore.getmidCRM,
  loading: state && state.midCRMStore && state.midCRMStore.loading,
  getDetailsMID: state && state.midCRMStore && state.midCRMStore.getDetailsMID,
  statusGMIDCRM: state && state.midCRMStore && state.midCRMStore.statusGMIDCRM,
  messagesGMIDCRM: state && state.midCRMStore && state.midCRMStore.messagesGMIDCRM,
  loadingGMIDCRM: state && state.midCRMStore && state.midCRMStore.loadingGMIDCRM,
  deletemidCRM: state && state.midCRMDeleteStore && state.midCRMDeleteStore.deletemidCRM,
  deletemidCRMStatus: state && state.midCRMDeleteStore && state.midCRMDeleteStore.deletemidCRMStatus,
  deletemidCRMMessage: state && state.midCRMDeleteStore && state.midCRMDeleteStore.deletemidCRMMessage,
  deletemidCRMLoading: state && state.midCRMDeleteStore && state.midCRMDeleteStore.loading  
})

const mapDispatchToProps = (dispatch) => ({
  // getMidCrmDispatch: (params) => dispatch(midCRMActions.getmidCRM(params)),
  // deletemidCRMDispatch: (data) => dispatch(deletemidCRMActions.deletemidCRM(data)),
  // clearDeletemidCRMDispatch: () => dispatch(deletemidCRMActions.clearDeletemidCRM()),
  // getmidCRMDetailsDispatch: (data) => dispatch(midCRMGetDetailsActions.getmidCRMDetails(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddMerchant)
