import React, { useEffect, useState } from 'react'
import { packageValidation } from './validation'
import { CRM_FORM } from '../../../../utils/constants'
import { setLocalStorage, getLocalStorage } from '../../../../utils/helper'
import _ from 'lodash'
import { useLocation } from 'react-router-dom'
import { 
  packageActions, 
  packPaymentActions 
} from '../../../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'

const Package = (props) => {
  const {
    loading,
    onClickNext,
    goBack,
    setClientDetails,
    getPackageDispatch,
    getPackages,
    getPaymentDispatch,
    getPayments
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update')
  const id = url && url[1]
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [selectAll, setSelectAll] = useState(false)
  const [total, setTotal] = useState(false)
  const [limit, setLimit] = useState(25)
  const [setActivePageNumber] = useState(1)
  const [termData, setTermData] = useState({
    terms: '12'
  })
  const [formData, setFormData] = useState({
    _id: '620252d754ab2e5944c69495',
    orderIntelligence: 0,
    preventionAlert: 0,
    chargebackManagement: 0
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getPackageDispatch(params)
  }, [])

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getPaymentDispatch(params)
  }, [limit])

  const [errors, setErrors] = useState({})

  const handleChoose = (name, value) => {
    const val = !formData[name] ? value : ''
    setFormData((values) => ({
      ...values,
      [name]: val
    }))
    setErrors({ ...errors, orderIntelligence: '' })
  }

  const handleTotal = () => {
    let price = 0
    if (formData.orderIntelligence) {
      price = price + 39
    }
    if (formData.preventionAlert) {
      price = price + 339
    }
    if (formData.chargebackManagement) {
      price = price + 999
    }
    setTotal(price)
  }

  useEffect(() => {
    if (
      formData.orderIntelligence &&
      formData.preventionAlert &&
      formData.chargebackManagement
    ) {
      setSelectAll(true)
    }
    const checkAnySelected = Object.values(formData).every(
      (item) => item === ''
    )
    if (checkAnySelected) {
      setSelectAll(false)
    }
    handleTotal()
  }, [formData])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  const handleSorting = (name) => { }

  useEffect(() => {
    if (id) {
      setEditMode(true)
      setShowForm(false)
    } else {
      setEditMode(false)
    }
  }, [id])

  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)

  const handleInputChange = (e) => {
    e.preventDefault()
    setTermData((values) => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = packageValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      onClickNext(3)
      setClientDetails((values) => ({ ...values, package: formData }))
      setLocalStorage(CRM_FORM.PACKAGE_DETAILS, JSON.stringify(formData))
    }
  }

  const handleSelectAll = () => {
    if (selectAll) {
      setFormData({
        orderIntelligence: 0,
        preventionAlert: 0,
        chargebackManagement: 0
      })
    } else {
      setErrors({ ...errors, orderIntelligence: '' })
      setFormData({
        orderIntelligence: 39,
        preventionAlert: 339,
        chargebackManagement: 999
      })
    }
    setSelectAll((val) => !val)
  }

  const localData = JSON.parse(getLocalStorage(CRM_FORM.PACKAGE_DETAILS))
  useEffect(() => {
    if (!_.isEmpty(localData)) {
      setFormData(localData)
    }
  }, [])

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false)
      }
    }
  }, [isFormUpdated])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getPaymentDispatch(params)
  }

  const totalPages =
    getPayments && getPayments.count
      ? Math.ceil(parseInt(getPayments && getPayments.count) / limit)
      : 1

  const handlePagesClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    // getPackageDispatch(params)
  }

  const totalPaages =
    getPackages && getPackages.count
      ? Math.ceil(parseInt(getPackages && getPackages.count) / limit)
      : 1

  return (
    <>
      <div className='card-header bg-skyBlue'>
        <div className='card-body'>
          <h2 className='mb-2'>Package</h2>
          {showForm
            ? (
              <div className='card-header bg-skyBlue'>
                <div className='card-body'>
                  <div className='d-flex justify-content-end mb-4'>
                    {
                      pathName.includes('client-onboarding/update') ?
                      (
                        <button
                          className='btn btn-sm btn-darkRed mx-2'
                          onClick={() => {
                            setShowForm(false)
                          }}
                        > 
                          Back
                        </button> 
                      ) : null
                    }
                    <button
                      className='btn btn-sm btn-primary'
                      onClick={() => {
                        handleSelectAll()
                      }}
                    >
                      {selectAll ? 'Deselect' : 'Select'} All
                    </button>
                  </div>
                  <div className='row g-10'>
                    <div className='col-xl-4 col-sm-4 col-md-4'>
                      <div className='d-flex h-100'>
                        <div className='w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-10 px-10'>
                          <div className='mb-4 text-center'>
                            {formData.orderIntelligence
                              ? (
                                <i className='bi bi-check2-circle text-success font-xxxl-bold' />
                                )
                              : null}
                            <h1 className='text-dark my-3 fw-bold'>
                              Order Inquiries
                            </h1>
                            <div className='text-center'>
                              <span className='mb-2 text-primary'>$</span>
                              <span
                                className='fs-3x fw-bolder text-primary'
                                data-kt-plan-price-month='39'
                                data-kt-plan-price-annual='399'
                              >
                                39
                              </span>
                              <span className='fs-7 fw-bold opacity-50'>
                                /<span data-kt-element='period'>Month</span>
                              </span>
                            </div>
                          </div>
                          <div className='w-100 mb-10'>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Inquiries
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Notification
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1'>
                                Dashboard
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                          </div>
                          <button
                            className={`btn btn-sm ${formData.orderIntelligence
                              ? 'bg-blueDark'
                              : 'bg-blueDark'
                              }`}
                            onClick={() => {
                              handleChoose('orderIntelligence', 39)
                            }}
                          >
                            {formData.orderIntelligence ? 'Deselect' : 'Select'}
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className='col-xl-4 col-sm-4 col-md-4'>
                      <div className='d-flex h-100'>
                        <div className='w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-10 px-10'>
                          <div className='mb-4 text-center'>
                            {formData.preventionAlert
                              ? (
                                <i className='bi bi-check2-circle text-success font-xxxl-bold' />
                                )
                              : null}
                            <h1 className='text-dark my-3 fw-bold'>
                              Prevention Alerts
                            </h1>
                            <div className='text-center'>
                              <span className='mb-2 text-primary'>$</span>
                              <span
                                className='fs-3x fw-bolder text-primary'
                                data-kt-plan-price-month='339'
                                data-kt-plan-price-annual='3399'
                              >
                                339
                              </span>
                              <span className='fs-7 fw-bold opacity-50'>
                                /<span data-kt-element='period'>Month</span>
                              </span>
                            </div>
                          </div>

                          <div className='w-100 mb-10'>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Client Managed Alerts
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Duplicate Alert Credit
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Send Alert Notification
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1'>
                                Dashboard
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                          </div>
                          <button
                            className={`btn btn-sm ${formData.preventionAlert
                              ? 'bg-blueDark'
                              : 'bg-blueDark'
                              }`}
                            onClick={() => {
                              handleChoose('preventionAlert', 339)
                            }}
                          >
                            {formData.preventionAlert ? 'Deselect' : 'Select'}
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className='col-xl-4 col-sm-4 col-md-4'>
                      <div className='d-flex h-100'>
                        <div className='w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75  py-10 px-10'>
                          <div className='mb-4 text-center'>
                            {formData.chargebackManagement
                              ? (
                                <i className='bi bi-check2-circle text-success font-xxxl-bold' />
                                )
                              : null}
                            <h1 className='text-dark my-3 fw-bold'>
                              Chargeback Management
                            </h1>
                            <div className='text-center'>
                              <span className='mb-2 text-primary'>$</span>
                              <span
                                className='fs-3x fw-bolder text-primary'
                                data-kt-plan-price-month='999'
                                data-kt-plan-price-annual='9999'
                              >
                                999
                              </span>
                              <span className='fs-7 fw-bold opacity-50'>
                                /<span data-kt-element='period'>Month</span>
                              </span>
                            </div>
                          </div>
                          <div className='w-100 mb-10'>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Chargeback List
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center mb-5'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Manage Chargeback
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='d-flex align-items-center'>
                              <span className='fw-bold fs-6 text-gray-800 flex-grow-1 pe-3'>
                                Dashboard
                              </span>
                              <span className='svg-icon svg-icon-1 svg-icon-success'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='24'
                                  height='24'
                                  viewBox='0 0 24 24'
                                  fill='none'
                                >
                                  <rect
                                    opacity='0.3'
                                    x='2'
                                    y='2'
                                    width='20'
                                    height='20'
                                    rx='10'
                                    fill='black'
                                  />
                                  <path
                                    d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z'
                                    fill='black'
                                  />
                                </svg>
                              </span>
                            </div>
                            <div className='mt-4'>
                              <p className='fs-6 text-gray-600 flex-grow-1'>
                                - Root cause analyzer
                              </p>
                              <p className='fs-6 text-gray-600 flex-grow-1'>
                                - Performance analyzer
                              </p>
                            </div>
                          </div>
                          <button
                            className={`btn btn-sm ${formData.chargebackManagement
                              ? 'bg-blueDark'
                              : 'bg-blueDark'
                              }`}
                            onClick={() => {
                              handleChoose('chargebackManagement', 999)
                            }}
                          >
                            {formData.chargebackManagement
                              ? 'Deselect'
                              : 'Select'}
                          </button>
                        </div>
                      </div>
                    </div>
                    {errors && errors.orderIntelligence && (
                      <div className='rr mt-5 text-center bg-light-warning py-3'>
                        <style>{'.rr{color:black;}'}</style>
                        {errors.orderIntelligence}
                      </div>
                    )}
                  </div>
                  {
                    formData.orderIntelligence || formData.preventionAlert || formData.chargebackManagement
                      ? (
                        <div className='mb-0 mt-5 d-flex'>
                          <div className='mt-5 w-50'>
                            <select
                              name='terms'
                              className='form-select form-select-solid bg-white w-100'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handleInputChange(e)}
                              value={termData.terms || ''}
                            >
                              <option value=''>Select subscription terms...</option>
                              <option value='3'>3 Months</option>
                              <option value='6'>6 Months</option>
                              <option value='12'>12 Months</option>
                            </select>
                          </div>
                          <div className='mx-5 mt-5'>
                            <div className='card-header bg-white' style={{ width: 350 }}>
                              <div className='card-body w-100'>
                                {formData.orderIntelligence
                                  ? (
                                    <div className='d-flex justify-content-between my-4'>
                                      <div className=' my-auto'>Order Inquiries</div>
                                      <div className='mx-3'>
                                        <div className='text-center'>
                                          <span className='mb-2 text-primary'>$</span>
                                          <span
                                            className='fs-2x fw-bolder text-primary'
                                            data-kt-plan-price-month='39'
                                            data-kt-plan-price-annual='399'
                                          >
                                            {termData.terms
                                              ? Number(termData.terms) * 39
                                              : null}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                    )
                                  : null}
                                {formData.preventionAlert
                                  ? (
                                    <div className='d-flex justify-content-between my-4'>
                                      <div className='my-auto'>Prevention Alerts</div>
                                      <div className='mx-3'>
                                        <div className='text-center'>
                                          <span className='mb-2 text-primary'>$</span>
                                          <span
                                            className='fs-2x fw-bolder text-primary'
                                            data-kt-plan-price-month='39'
                                            data-kt-plan-price-annual='399'
                                          >
                                            {termData.terms
                                              ? Number(termData.terms) * 339
                                              : null}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                    )
                                  : null}
                                {formData.chargebackManagement
                                  ? (
                                    <div className='d-flex justify-content-between my-4'>
                                      <div className='my-auto'>Chargeback Management</div>
                                      <div className='mx-3'>
                                        <div className='text-center'>
                                          <span className='mb-2 text-primary'>$</span>
                                          <span
                                            className='fs-2x fw-bolder text-primary'
                                            data-kt-plan-price-month='39'
                                            data-kt-plan-price-annual='399'
                                          >
                                            {termData.terms
                                              ? Number(termData.terms) * 999
                                              : null}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                    )
                                  : null}
                                <div className='border' />
                                <div className='d-flex justify-content-between mb-4'>
                                  <div className='my-auto'>Total</div>
                                  <div className='mx-3'>
                                    <div className='text-center'>
                                      <span className='fs-7 fw-bold opacity-50 mx-3'>
                                        Per
                                        <span data-kt-element='period'>
                                          {termData.terms === '12'
                                            ? 'Annually'
                                            : ` ${termData.terms} Months`}
                                        </span>
                                      </span>
                                      <span className='mb-2 text-primary'>$</span>
                                      <span
                                        className='fs-2x fw-bolder text-primary'
                                        data-kt-plan-price-month='39'
                                        data-kt-plan-price-annual='399'
                                      >
                                        {Number(termData.terms) * total}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        )
                      : null
                  }
                </div>
                {
            showForm && !pathName.includes('client-onboarding/update')
              ? (
                <div className='form-group row mt-4 mb-1'>
                  <div className='col-lg-6' />
                  <div className='col-lg-6'>
                    <div className='col-lg-11'>
                      <button
                        className='btn btn-orange m-2 fa-pull-right'
                        onClick={(event) => {
                          handleSubmit(event)
                        }}
                      >
                        {loading
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true'
                            />
                            )
                          : (
                              'Next'
                            )}
                      </button>
                      <button
                        className='btn btn-darkRed m-2 fa-pull-right'
                        onClick={() => { goBack(1) }}
                      >
                        Back
                      </button>
                    </div>
                  </div>
                </div>
                )
              : null
          }
          {
            editMode && pathName.includes('client-onboarding/update')
              ? (
                <div className='form-group row mt-4'>
                  <div className='col-lg-6' />
                  <div className='col-lg-6'>
                    <div className='col-lg-11'>
                      <button
                        className='btn btn-blue m-2 fa-pull-right'
                      >
                        {''
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true'
                            />
                            )
                          : (
                              'Save'
                            )}
                      </button>
                    </div>
                  </div>
                </div>
                )
              : null
          }
              </div>
              )
            : null}
          {editMode && !showForm
            ? (
              <>
                <div className='d-flex  px - 2'>
                  <div className='d-flex justify-content-start col-md-6'>
                    <div className='col-md-3 mt-1'>
                      {getPackages && getPackages.count && (
                        <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                          Total: &nbsp;{' '}
                          <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                            {getPackages.count}
                          </span>
                        </span>
                      )}
                    </div>
                    <div className='col-md-9 d-flex' />
                    <div className='d-flex col-md-12 justify-content-end my-auto'>
                      <div className='my-auto'>
                        <button
                          className='btn btn-sm btn-primary'
                          onClick={() => {
                            setShowForm(true)
                          }}
                        >
                          Modify Package
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-5 ms-5'>
                  <div className='p-3 bg-white rounded shadow d-flex justify-content-start'>
                    <ul id='myTab' role='tablist' className='nav nav-tabs nav-pills flex-column flex-sm-row text-center bg-light border-0 rounded-nav col-lg-3'>
                      <li className='nav-item flex-sm-fill'>
                        <a
                          id='home-tab'
                          data-toggle='tab'
                          href='#home'
                          role='tab'
                          aria-controls='home'
                          aria-selected='true'
                          className='nav-link border-0 text-uppercase font-weight-bold active'
                        >
                          Packages
                        </a>
                      </li>
                      <li className='nav-item flex-sm-fill'>
                        <a
                          id='profile-tab'
                          data-toggle='tab'
                          href='#profile'
                          role='tab'
                          aria-controls='profile'
                          aria-selected='false'
                          className='nav-link border-0 text-uppercase font-weight-bold'
                        >
                          Payments
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className='tab-content' id='myTabContent'>
                  <div id='home' role='tabpanel' aria-labelledby='home-tab' className='tab-pane fade px-4 py-5 show active'>
                    <div className='card h-auto'>
                      <div className='card-body py-3'>
                        <div className='d-flex justify-content-start col-md-6'>
                          <div className='col-md-3 mt-1'>
                            <h4 className='mt-5'>Packages</h4>
                          </div>
                          <div className='col-md-7 d-flex'>
                            <label className='col-form-label text-lg-start mt-3'>
                              Record(s) per Page : &nbsp;{' '}
                            </label>
                            <div className='col-md-3 mt-3'>
                              <select
                                className='form-select w-6rem'
                                data-control='select'
                                data-placeholder='Select an option'
                                data-allow-clear='true'
                                onChange={(e) => handleRecordPerPage(e)}
                              >
                                <option value='25'>25</option>
                                <option value='50'>50</option>
                                <option value='75'>75</option>
                                <option value='100'>100</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className='table-responsive'>
                          <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
                            <thead className='fw-bolder fs-8 text-gray-800'>
                              <tr>
                                <th>
                                  <div className='d-flex'>
                                    <span>Package ID</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('_id')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Subscription Date</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('subscriptionDate')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Package Details</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('packageDetails')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Gateway</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('gateway')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Transaction ID</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('transactionId')}
                                      />
                                    </div>
                                  </div>
                                </th>
                              </tr>
                            </thead>
                            <tbody className='fs-8'>
                              {
                                !loading
                                  ? (
                                      getPackages &&
                                      getPackages
                                        ? (
                                            getPackages.map((getPackage, _id) => {
                                              return (
                                                <tr
                                                  key={_id}
                                                  style={
                                                _id === 0
                                                  ? { borderColor: 'black' }
                                                  : { borderColor: 'white' }
                                              }
                                                >
                                                  <td>
                                                    {getPackage._id ? getPackage._id : '--'}
                                                  </td>
                                                  <td>
                                                    {getPackage.subscriptionDate ? getPackage.subscriptionDate : '--'}
                                                  </td>
                                                  <td>
                                                    {getPackage.packageDetails ? getPackage.packageDetails : '--'}
                                                  </td>
                                                  <td>
                                                    {getPackage.gateway ? getPackage.gateway : '--'}
                                                  </td>
                                                  <td>
                                                    {getPackage.transactionId ? getPackage.transactionId : '--'}
                                                  </td>
                                                </tr>
                                              )
                                            })
                                          )
                                        : (
                                          <tr className='text-center py-3'>
                                            <td colSpan='100%'>No record(s) found</td>
                                          </tr>
                                          )
                                    )
                                  : (
                                    <tr>
                                      <td colSpan='100%' className='text-center'>
                                        <div
                                          className='spinner-border text-primary m-5'
                                          role='status'
                                        />
                                      </td>
                                    </tr>
                                    )
                              }
                            </tbody>
                          </table>
                        </div>
                        <div className='form-group row mb-4 mt-6'>
                          <div className='col-lg-12 mb-4 align-items-end d-flex'>
                            <div className='col-lg-12'>
                              <ReactPaginate
                                nextLabel='Next >'
                                onPageChange={handlePagesClick}
                                pageRangeDisplayed={3}
                                marginPagesDisplayed={2}
                                pageCount={totalPaages}
                                previousLabel='< Prev'
                                pageClassName='page-item'
                                pageLinkClassName='page-link'
                                previousClassName='page-item'
                                previousLinkClassName='page-link'
                                nextClassName='page-item'
                                nextLinkClassName='page-link'
                                breakLabel='...'
                                breakClassName='page-item'
                                breakLinkClassName='page-link'
                                containerClassName='pagination'
                                activeClassName='active'
                                renderOnZeroPageCount={null}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id='profile' role='tabpanel' aria-labelledby='profile-tab' className='tab-pane fade px-4 py-5'>
                    <div className='card h-auto'>
                      <div className='card-body py-3'>
                        <div className='d-flex justify-content-start col-md-6'>
                          <div className='col-md-3 mt-1'>
                            <h4 className='mt-5'>Payments</h4>
                          </div>
                          <div className='col-md-7 d-flex'>
                            <label className='col-form-label text-lg-start mt-3'>
                              Record(s) per Page : &nbsp;{' '}
                            </label>
                            <div className='col-md-3 mt-3'>
                              <select
                                className='form-select w-6rem'
                                data-control='select'
                                data-placeholder='Select an option'
                                data-allow-clear='true'
                                onChange={(e) => handleRecordPerPage(e)}
                              >
                                <option value='25'>25</option>
                                <option value='50'>50</option>
                                <option value='75'>75</option>
                                <option value='100'>100</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className='table-responsive'>
                          <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
                            <thead className='fw-bolder fs-8 text-gray-800'>
                              <tr>
                                <th>
                                  <div className='d-flex'>
                                    <span>Payment ID</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('paymentId')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Payment Date</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('paymentDate')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Package ID</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('packageId')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Subscription Tenure</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('subscriptionTenure')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Amount</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('amount')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Transaction ID</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('transactionId')}
                                      />
                                    </div>
                                  </div>
                                </th>
                                <th>
                                  <div className='d-flex'>
                                    <span>Payment Gateway</span>
                                    <div className='min-w-25px text-end'>
                                      <div
                                        className='cursor-pointer'
                                        onClick={() => handleSorting('paymentGateway')}
                                      />
                                    </div>
                                  </div>
                                </th>
                              </tr>
                            </thead>
                            <tbody className='fs-8'>
                              {
                                !loading
                                  ? (
                                      getPayments &&
                                      getPayments
                                        ? (
                                            getPayments.map((getPayment, _id) => {
                                              return (
                                                <tr
                                                  key={_id}
                                                  style={
                                                _id === 0
                                                  ? { borderColor: 'black' }
                                                  : { borderColor: 'white' }
                                              }
                                                >
                                                  <td>
                                                    {getPayment._id ? getPayment._id : '--'}
                                                  </td>
                                                  <td>
                                                    {getPayment.paymentDate ? getPayment.paymentDate : '--'}
                                                  </td>
                                                  <td>
                                                    {getPayment.packageId ? getPayment.packageId : '--'}
                                                  </td>
                                                  <td>
                                                    {getPayment.subscriptionTenure ? getPayment.subscriptionTenure : '--'}
                                                  </td>
                                                  <td>
                                                    {getPayment.amount ? getPayment.amount : '--'}
                                                  </td>
                                                  <td>
                                                    {getPayment.transactionId
                                                      ? getPayment.transactionId
                                                      : '--'}
                                                  </td>
                                                  <td>
                                                    {getPayment.paymentGateway
                                                      ? getPayment.paymentGateway
                                                      : '--'}
                                                  </td>
                                                </tr>
                                              )
                                            })
                                          )
                                        : (
                                          <tr className='text-center py-3'>
                                            <td colSpan='100%'>No record(s) found</td>
                                          </tr>
                                          )
                                    )
                                  : (
                                    <tr>
                                      <td colSpan='100%' className='text-center'>
                                        <div
                                          className='spinner-border text-primary m-5'
                                          role='status'
                                        />
                                      </td>
                                    </tr>
                                    )
                              }
                            </tbody>
                          </table>
                        </div>
                        <div className='form-group row mb-4 mt-6'>
                          <div className='col-lg-12 mb-4 align-items-end d-flex'>
                            <div className='col-lg-12'>
                              <ReactPaginate
                                nextLabel='Next >'
                                onPageChange={handlePageClick}
                                pageRangeDisplayed={3}
                                marginPagesDisplayed={2}
                                pageCount={totalPages}
                                previousLabel='< Prev'
                                pageClassName='page-item'
                                pageLinkClassName='page-link'
                                previousClassName='page-item'
                                previousLinkClassName='page-link'
                                nextClassName='page-item'
                                nextLinkClassName='page-link'
                                breakLabel='...'
                                breakClassName='page-item'
                                breakLinkClassName='page-link'
                                containerClassName='pagination'
                                activeClassName='active'
                                renderOnZeroPageCount={null}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
              )
            : null}
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { packageStore } = state
  return {
    getPackages:
      state && state.packageStore && state.packageStore.getPackages
        ? state.packageStore.getPackages
        : [],
    loading: state && state.packageStore && state.packageStore.loading,
    getPayments:
      state && state.packPaymentStore && state.packPaymentStore.packPaymentData
        ? state.packPaymentStore.packPaymentData
        : [],
    loading: state && state.packPaymentStore && state.packPaymentStore.loading
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPackageDispatch: (params) => dispatch(packageActions.getPackage(params)),
  getPaymentDispatch: (params) => dispatch(packPaymentActions.getPackPayment(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Package)
