import React, { useEffect, useState } from 'react'
import { KTSVG } from '../../../../theme/helpers'
import { CRM_FORM, REGEX } from '../../../../utils/constants'
import ReactSelect from '../../../../theme/layout/components/ReactSelect'
import { Modal } from '../../../../theme/layout/components/modal'
import { setLocalStorage, getLocalStorage } from '../../../../utils/helper'
import _ from 'lodash'
import { connect } from 'react-redux'
import { crmActions, processorActions, paymentActions } from '../../../../store/actions'
import color from '../../../../utils/colors'
import {
  crmValidation,
  addCrmValidation,
  addPaymentGateValidation,
  processorValidation,
  addApilValidation,
  paymentValidation
} from './validation'
import {
  getCrmLables,
  getProcessorLabels,
  getPaymentLabels,
  handleTrimWhiteSpace
} from '../functions/functions'

const CrmForm = (props) => {
  const {
    onClickNext,
    goBack,
    className,
    setClientDetails,
    getCRM,
    getCRMDispatch,
    getPaymentDispatch,
    getProcessorDispatch,
    processorData,
    paymentData,
    setSummary
  } = props
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [show, setShow] = useState(false)
  const [showAddProcessor, setShowAddProcessor] = useState(false)
  const [showAddGateway, setShowAddGateway] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [errors, setErrors] = useState({})
  const [showTable, setShowTable] = useState(false)
  const [showSFTPTable, setShowSFTPTable] = useState(false)
  const [showGatewayTable, setShowGatewayTable] = useState(false)
  const [selectedCrmOption, setSelectedCrmOption] = useState('')
  const [crmOption, setCrmOption] = useState()
  const [selectedProcessorOption, setSelectedProcessorOption] = useState('')
  const [processorOption, setProcessorOption] = useState()
  const [selectedPaymentOption, setSelectedPaymentOption] = useState('')
  const [paymentOption, setPaymentOption] = useState()
  const [formData, setFormData] = useState({
    crmId: '',
    crmLabel: '',
    paymentProcessorId: '',
    processorLabel: '',
    paymentGateway: '',
    paymentGatewayLabel: ''
  })
  const [form, setForm] = useState({
    crmId: '',
    crmLabel: '',
    apiKey: '',
    apiSecretKey: '',
    apiEndPoint: ''
  })
  const [paymentProcesserDetails, setPaymentProcessorDetails] = useState({
    paymentProcessorId: '',
    processorLabel: '',
    disputeSubmissionMode: '',
    method: "",
    oAuthUrl: '',
    oAuthUserName: '',
    oAuthPassword: '',
    hostName: '',
    portNumber: '22',
    username: '',
    password: '',
    faxNumber: '',
    email: '',
    emailBcc: "",
    emailCc: "",
    apiKey: '',
    apiSecretKey: '',
    apiEndPoint: '',
    requestApiKey: '',
    requestApiSecret: '',
    requestApiEndPoint: ''
  })

  const [tableData, setTableData] = useState([])
  const [tableForm, setTableForm] = useState([])
  const [processorTableData, setProcessorTableData] = useState([])
  const [processorTableForm, setProcessorTableForm] = useState([])
  const [paymentTableData, setPaymentTableData] = useState([])
  const [paymentTableForm, setPaymentTableForm] = useState([])
  const [paymentGatewayDetails, setPaymentGatewayDetails] = useState({
    paymentGateway: '',
    paymentGatewayLabel: '',
    gatewayEndPoint: '',
    gatewayApiKey: '',
    gatewayApiSecretKey: ''
  })
  const localData = JSON.parse(getLocalStorage(CRM_FORM.CRM_DETAILS))
  const localTableData = JSON.parse(
    getLocalStorage(CRM_FORM.CRM_DETAILS_TABLE)
  )
  const localProcessorDetails = JSON.parse(
    getLocalStorage(CRM_FORM.PROCESSOR_DETAILS)
  )
  const localProcessorTable = JSON.parse(
    getLocalStorage(CRM_FORM.PROCESSOR_DETAILS_TABLE)
  )
  const localGatewayDetails = JSON.parse(
    getLocalStorage(CRM_FORM.GATEWAY_DETAILS)
  )
  const localGatewayTable = JSON.parse(
    getLocalStorage(CRM_FORM.GATEWAY_DETAILS_TABLE)
  )

  const handleInputChange = (e, type) => {
    e.preventDefault()
    if (type === 'setForm') {
      setForm((values) => ({ ...values, [e.target.name]: e.target.value }))
    }
    if (type === 'setFormData') {
      setFormData((values) => ({ ...values, [e.target.name]: e.target.value }))
    }
    if (type === 'setPaymentProcessorDetails') {
      setPaymentProcessorDetails((values) => ({
        ...values,
        [e.target.name]: e.target.value
      }))
    }
    if (type === 'setPaymentGatewayDetails') {
      setPaymentGatewayDetails((values) => ({
        ...values,
        [e.target.name]: e.target.value
      }))
    }
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleAddClick = (type) => {
    if (type === 'CRM') {
      const errorMsg = crmValidation(formData, setErrors)
      if (_.isEmpty(errorMsg)) {
        if (!tableForm.includes(formData.crmId)) {
          setShow(true)
        }
      }
    }
    if (type === 'PAYMENT_PROCESSOR') {
      const errorMsg = processorValidation(formData, setErrors)
      if (_.isEmpty(errorMsg)) {
        if (!processorTableForm.includes(formData.paymentProcessorId)) {
          setShowAddProcessor(true)
        }
      }
    }
    if (type === 'PAYMENT_GATEWAY') {
      const errorMsg = paymentValidation(formData, setErrors)
      if (_.isEmpty(errorMsg)) {
        if (!paymentTableData.includes(formData.paymentGateway)) {
          setShowAddGateway(true)
        }
      }
    }
  }

  const handleSaveGatewayTable = () => {
    if (!paymentTableForm.includes(formData.paymentGateway)) {
      const errorMsg = addPaymentGateValidation(paymentGatewayDetails, setErrors)
      if (_.isEmpty(errorMsg)) {
        if (editMode) {
          setPaymentTableData((prevState) =>
            prevState.map((obj) =>
              obj.paymentGateway === paymentGatewayDetails.paymentGateway
                ? Object.assign(obj, {
                  paymentGatewayLabel: paymentGatewayDetails.paymentGatewayLabel,
                  gatewayEndPoint: paymentGatewayDetails.gatewayEndPoint,
                  gatewayApiKey: paymentGatewayDetails.gatewayApiKey,
                  gatewayApiSecretKey:
                    paymentGatewayDetails.gatewayApiSecretKey
                })
                : obj
            )
          )
        } else {
          setPaymentTableForm((values) => [...values, formData.paymentGateway])
          setPaymentTableData((values) => [
            ...values,
            {
              paymentGatewayLabel: formData.paymentGatewayLabel,
              paymentGateway: formData.paymentGateway,
              gatewayEndPoint: paymentGatewayDetails.gatewayEndPoint,
              gatewayApiKey: paymentGatewayDetails.gatewayApiKey,
              gatewayApiSecretKey: paymentGatewayDetails.gatewayApiSecretKey
            }
          ])
        }
        setSelectedPaymentOption()
        setShowGatewayTable(true)
        setShowAddGateway(false)
        setEditMode(false)
        setPaymentGatewayDetails({
          gatewayEndPoint: '',
          gatewayApiKey: '',
          gatewayApiSecretKey: ''
        })
        setFormData({ paymentGateway: '', paymentGatewayLabel: '' })
      }
    }
  }

  const handleAddTable = () => {
    if (!tableForm.includes(formData.crmId)) {
      const errorMsg = addCrmValidation(form, setErrors)
      if (_.isEmpty(errorMsg)) {
        if (editMode) {
          setTableData((prevState) =>
            prevState.map((obj) =>
              obj.crmId === form.crmId
                ? Object.assign(obj, {
                  crmLabel: form.crmLabel,
                  apiKey: form.apiKey,
                  apiSecretKey: form.apiSecretKey,
                  apiEndPoint: form.apiEndPoint,
                })
                : obj
            )
          )
        } else {
          setTableForm((values) => [...values, formData.crmId])
          setTableData((values) => [
            ...values,
            {
              crmLabel: formData.crmLabel,
              crmId: formData.crmId,
              apiKey: form.apiKey,
              apiSecretKey: form.apiSecretKey,
              apiEndPoint: form.apiEndPoint
            }
          ])
        }
        setSelectedCrmOption()
        setShowTable(true)
        setShow(false)
        setEditMode(false)
        setForm({ apiKey: '', apiSecretKey: '', apiEndPoint: '', crmId: '', crmLabel: '' })
        setFormData({ crmId: '', crmLabel: '' })
      }
    }
  }

  const handleAddProcessor = () => {
    if (editMode) {
      setProcessorTableData((prevState) =>
        prevState.map((obj) =>
          obj.paymentProcessorId === paymentProcesserDetails.paymentProcessorId
            ? Object.assign(obj, {
              paymentProcessorId: paymentProcesserDetails.paymentProcessorId,
              processorLabel: paymentProcesserDetails.processorLabel,
              method: paymentProcesserDetails.method,
              hostName: paymentProcesserDetails.hostName,
              portNumber: paymentProcesserDetails.portNumber,
              username: paymentProcesserDetails.username,
              password: paymentProcesserDetails.password,
              faxNumber: paymentProcesserDetails.faxNumber,
              email: paymentProcesserDetails.email,
              emailBcc: paymentProcesserDetails.emailBcc,
              emailCc: paymentProcesserDetails.emailCc,
              disputeSubmissionMode: paymentProcesserDetails.disputeSubmissionMode,
              oAuthUrl: paymentProcesserDetails.oAuthUrl,
              oAuthUserName: paymentProcesserDetails.oAuthUserName,
              oAuthPassword: paymentProcesserDetails.oAuthPassword,
              apiKey: paymentProcesserDetails.apiKey,
              apiSecretKey: paymentProcesserDetails.apiSecretKey,
              apiEndPoint: paymentProcesserDetails.apiEndPoint,
              requestApiKey: paymentProcesserDetails.requestApiKey,
              requestApiSecret: paymentProcesserDetails.requestApiSecret,
              requestApiEndPoint: paymentProcesserDetails.requestApiEndPoint
            })
            : obj
        )
      )
    } else {
      setProcessorTableForm((values) => [...values, formData.paymentProcessorId])
      setProcessorTableData((values) => [
        ...values,
        {
          paymentProcessorId: formData.paymentProcessorId,
          processorLabel: formData.processorLabel,
          method: paymentProcesserDetails.method,
          hostName: paymentProcesserDetails.hostName,
          portNumber: paymentProcesserDetails.portNumber,
          username: paymentProcesserDetails.username,
          password: paymentProcesserDetails.password,
          faxNumber: paymentProcesserDetails.faxNumber,
          email: paymentProcesserDetails.email,
          emailBcc: paymentProcesserDetails.emailBcc,
          emailCc: paymentProcesserDetails.emailCc,
          disputeSubmissionMode: paymentProcesserDetails.disputeSubmissionMode,
          oAuthUrl: paymentProcesserDetails.oAuthUrl,
          oAuthUserName: paymentProcesserDetails.oAuthUserName,
          oAuthPassword: paymentProcesserDetails.oAuthPassword,
          apiKey: paymentProcesserDetails.apiKey,
          apiSecretKey: paymentProcesserDetails.apiSecretKey,
          apiEndPoint: paymentProcesserDetails.apiEndPoint,
          requestApiKey: paymentProcesserDetails.requestApiKey,
          requestApiSecret: paymentProcesserDetails.requestApiSecret,
          requestApiEndPoint: paymentProcesserDetails.requestApiEndPoint
        }
      ])
    }
    setShowSFTPTable(true)
  }

  const handleAddProcessorTable = () => {
    const errorMsg = addApilValidation(paymentProcesserDetails, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (processorTableForm && !processorTableForm.includes(formData.paymentProcessorId)) {
        handleAddProcessor()
        setPaymentProcessorDetails({
          hostName: '',
          disputeSubmissionMode: '',
          method: "",
          oAuthUrl: '',
          oAuthUserName: '',
          oAuthPassword: '',
          portNumber: '22',
          username: '',
          password: '',
          faxNumber: '',
          email: '',
          emailBcc: "",
          emailCc: "",
          apiKey: '',
          apiSecretKey: '',
          apiEndPoint: '',
          requestApiKey: '',
          requestApiSecret: '',
          requestApiEndPoint: ''
        })
        setFormData({ paymentProcessorId: '', processorLabel: '' })
      }
      setSelectedProcessorOption()
      setShowAddProcessor(false)
      setEditMode(false)
    }
  }

  const onCloseProcessor = () => {
    setShowAddProcessor(false)
    setFormData({ paymentProcessorId: '', processorLabel: '' })
    setSelectedPaymentOption()
    setPaymentProcessorDetails({
      disputeSubmissionMode: '',
      oAuthUrl: '',
      oAuthUserName: '',
      oAuthPassword: '',
      hostName: '',
      method: '',
      portNumber: '22',
      username: '',
      password: '',
      faxNumber: '',
      email: '',
      emailBcc: "",
      emailCc: "",
      apiKey: '',
      apiSecretKey: '',
      apiEndPoint: '',
      requestApiKey: '',
      requestApiSecret: '',
      requestApiEndPoint: ''
    })
    setErrors((values) => ({
      ...values,
      paymentProcessorId: '',
      processorLabel: '',
      disputeSubmissionMode: '',
      oAuthUrl: '',
      oAuthUserName: '',
      oAuthPassword: '',
      hostName: '',
      portNumber: '22',
      username: '',
      password: '',
      faxNumber: '',
      email: '',
      emailBcc: "",
      emailCc: "",
      apiKey: '',
      apiSecretKey: '',
      apiEndPoint: '',
      requestApiKey: '',
      requestApiSecret: '',
      requestApiEndPoint: ''
    }))
    setEditMode(false)
  }

  const onEditClick = (item) => {
    setShow(true)
    setEditMode(true)
    setForm({
      crmLabel: item.crmLabel,
      crmId: item.crmId,
      apiKey: item.apiKey,
      apiSecretKey: item.apiSecretKey,
      apiEndPoint: item.apiEndPoint
    })
  }

  const onEditProcessorClick = (item) => {
    setShowAddProcessor(true)
    setEditMode(true)
    setPaymentProcessorDetails({
      disputeSubmissionMode: item.disputeSubmissionMode,
      oAuthUrl: item.oAuthUrl,
      oAuthUserName: item.oAuthUserName,
      oAuthPassword: item.oAuthPassword,
      method: item.method,
      paymentProcessorId: item.paymentProcessorId,
      processorLabel: item.processorLabel,
      hostName: item.hostName,
      portNumber: item.portNumber,
      username: item.username,
      password: item.password,
      faxNumber: item.faxNumber,
      email: item.email,
      emailBcc: item.emailBcc,
      emailCc: item.emailCc,
      apiKey: item.apiKey,
      apiSecretKey: item.apiSecretKey,
      apiEndPoint: item.apiEndPoint,
      requestApiKey: item.requestApiKey,
      requestApiSecret: item.requestApiSecret,
      requestApiEndPoint: item.requestApiEndPoint
    })
  }

  const onEditGateClick = (item) => {
    setShowAddGateway(true)
    setEditMode(true)
    setPaymentGatewayDetails({
      paymentGateway: item.paymentGateway,
      paymentGatewayLabel: item.paymentGatewayLabel,
      gatewayEndPoint: item.gatewayEndPoint,
      gatewayApiKey: item.gatewayApiKey,
      gatewayApiSecretKey: item.gatewayApiSecretKey
    })
  }

  const onDeleteGateClick = (item) => {
    setPaymentTableData((prevState) =>
      prevState.filter((obj) =>
        obj.paymentGateway !== item.paymentGateway ? obj : null
      )
    )
    setPaymentTableForm((prevState) =>
      prevState.filter((obj) => (obj !== item.paymentGateway ? obj : null))
    )
  }

  const onDeleteClick = (item) => {
    setTableData((prevState) =>
      prevState.filter((obj) => (obj.crmId !== item.crmId ? obj : null))
    )
    setTableForm((prevState) =>
      prevState.filter((obj) => (obj !== item.crmId ? obj : null))
    )
  }

  const onDeleteProcessorClick = (item) => {
    setProcessorTableData((prevState) =>
      prevState.filter((obj) =>
        obj.paymentProcessorId !== item.paymentProcessorId ? obj : null
      )
    )
    setProcessorTableForm((prevState) =>
      prevState.filter((obj) => (obj !== item.paymentProcessorId ? obj : null))
    )
  }

  const validateIntegration = () => {
    let valid = true
    if (!processorTableForm.length > 0) {
      valid = false
      setErrors((values) => ({
        ...values,
        paymentProcessorId: 'Please add Payment Processor!'
      }))
    }
    return valid
  }

  const handleNextClick = () => {
    if (validateIntegration()) {
      onClickNext(3)
      const getTable = tableData.map(({
        crmId,
        apiKey,
        apiSecretKey,
        apiEndPoint
      }) => ({
        crmId,
        apiKey,
        apiSecretKey,
        apiEndPoint
      }))
      const getProcessorTable = processorTableData.map(({
        paymentProcessorId,
        hostName,
        method,
        oAuthUrl,
        oAuthUserName,
        oAuthPassword,
        portNumber,
        username,
        password,
        faxNumber,
        email,
        emailBcc,
        emailCc,
        disputeSubmissionMode,
        apiKey,
        apiSecretKey,
        apiEndPoint,
        requestApiKey,
        requestApiSecret,
        requestApiEndPoint
      }) => ({
        paymentProcessorId,
        hostName,
        portNumber,
        method,
        oAuthUrl,
        oAuthUserName,
        oAuthPassword,
        username,
        password,
        faxNumber,
        email,
        emailBcc,
        emailCc,
        disputeSubmissionMode,
        apiKey,
        apiSecretKey,
        apiEndPoint,
        requestApiKey,
        requestApiSecret,
        requestApiEndPoint
      }))
      const getPaymentTableData = paymentTableData.map(({
        paymentGateway,
        gatewayEndPoint,
        gatewayApiKey,
        gatewayApiSecretKey
      }) => ({
        paymentGateway,
        gatewayEndPoint,
        gatewayApiKey,
        gatewayApiSecretKey
      }))
      const crmLabels = getCrmLables(tableData)
      const processorLabels = getProcessorLabels(processorTableData)
      const paymentLabels = getPaymentLabels(paymentTableData)
      setSummary((values) => ({
        ...values,
        crm: crmLabels,
        processorData: processorLabels,
        paymentData: paymentLabels
      }))

      setClientDetails((values) => ({
        ...values,
        crm: getTable,
        processorData: getProcessorTable,
        paymentData: getPaymentTableData
      }))
      setLocalStorage(CRM_FORM.CRM_DETAILS, JSON.stringify(tableData))
      setLocalStorage(CRM_FORM.CRM_DETAILS_TABLE, JSON.stringify(tableForm))
      setLocalStorage(
        CRM_FORM.PROCESSOR_DETAILS,
        JSON.stringify(processorTableData)
      )
      setLocalStorage(
        CRM_FORM.PROCESSOR_DETAILS_TABLE,
        JSON.stringify(processorTableForm)
      )
      setLocalStorage(
        CRM_FORM.GATEWAY_DETAILS,
        JSON.stringify(paymentTableData)
      )
      setLocalStorage(
        CRM_FORM.GATEWAY_DETAILS_TABLE,
        JSON.stringify(paymentTableForm)
      )
    }
  }
  useEffect(() => {
    if (!_.isEmpty(localData)) {
      setTableData(localData)
      setTableForm(localTableData)
      setShowTable(true)
    }
    if (!_.isEmpty(localGatewayTable)) {
      setShowGatewayTable(true)
    }
    if (!_.isEmpty(localProcessorDetails)) {
      localProcessorDetails && localProcessorDetails.forEach(ele => {
        if (ele.disputeSubmissionMode) {
          setShowSFTPTable(true)
        }
      })
      setProcessorTableData(localProcessorDetails)
      setProcessorTableForm(localProcessorTable)
    }
    if (!_.isEmpty(localProcessorDetails)) {
      setPaymentTableData(localGatewayDetails)
      setPaymentTableForm(localGatewayTable)
    }
  }, [])

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false)
      }
    }
  }, [isFormUpdated])

  useEffect(() => {
    getCRMDispatch()
    getPaymentDispatch()
    getProcessorDispatch()
  }, [])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const handleChangeCRM = (selectedOption) => {
    if (selectedOption !== null && !tableForm.includes(selectedOption.label)) {
      setSelectedCrmOption(selectedOption)
      setFormData((values) => ({
        ...values,
        crmId: selectedOption.value,
        crmLabel: selectedOption.label
      }))
    } else {
      setSelectedCrmOption()
      setFormData((values) => ({ ...values, crmId: '', crmLabel: '' }))
    }
    setErrors((values) => ({ ...values, crmId: '' }))
  }

  useEffect(() => {
    const crmId = getDefaultOptions(getCRM)
    setCrmOption(crmId)
    if (!_.isEmpty(formData.crmId)) {
      const selOption = _.filter(crmId, function (x) {
        if (_.includes(formData.crmId._id, x.value)) {
          return x
        }
      })
      setSelectedCrmOption(selOption)
    }
  }, [getCRM])

  const getDefaultProcessorOptions = (getData) => {
    const defaultOptions = []
    if (!_.isEmpty(getData)) {
      getData && getData.map((item) => {
        if (item && item.processorName) {
          defaultOptions.push({
            label: `${item.processorName ? item.processorName : ''}`,
            value: item._id
          })
        }
      })
      return defaultOptions
    }
  }

  const handleChangeProcessor = (selectedOption) => {
    if (selectedOption !== null && !processorTableForm.includes(selectedOption.label)) {
      setSelectedProcessorOption(selectedOption)
      setFormData((values) => ({
        ...values,
        paymentProcessorId: selectedOption.value,
        processorLabel: selectedOption.label
      }))
      setErrors((values) => ({ ...values, processorLabel: '', paymentProcessorId: '' }))
    } else {
      setSelectedProcessorOption()
      setFormData((values) => ({ ...values, paymentProcessorId: '', processorLabel: '' }))
    }
  }

  useEffect(() => {
    const data = getDefaultProcessorOptions(processorData)
    setProcessorOption(data)
    if (!_.isEmpty(formData.paymentProcessorId)) {
      const selOption = _.filter(data, function (x) {
        if (_.includes(formData.paymentProcessorId._id, x.value)) {
          return x
        }
      })
      setSelectedProcessorOption(selOption)
    }
  }, [processorData])

  const getDefaultOptions = (getCRM) => {
    const defaultOptions = []
    if (!_.isEmpty(getCRM)) {
      const { data } = getCRM
      if (!_.isEmpty(data)) {
        data.map((item) =>
          defaultOptions.push({
            label: `${item.crmName ? item.crmName : ''}`,
            value: item._id
          })
        )
      }
      return defaultOptions
    }
  }

  const getDefaultPaymentOptions = (getData) => {
    const defaultOptions = []
    if (!_.isEmpty(getData)) {
      getData && getData.map((item) => {
        if (item && item.gatewayName) {
          defaultOptions.push({
            label: `${item.gatewayName ? item.gatewayName : ''}`,
            value: item._id
          })
        }
      })
      return defaultOptions
    }
  }

  const handleChangePayment = (selectedOption) => {
    if (selectedOption !== null && !paymentTableForm.includes(selectedOption.label)) {
      setSelectedPaymentOption(selectedOption)
      setFormData((values) => ({
        ...values,
        paymentGateway: selectedOption.value,
        paymentGatewayLabel: selectedOption.label
      }))
      setErrors((values) => ({ ...values, paymentGateway: '', paymentGatewayLabel: '' }))
    } else {
      setSelectedPaymentOption()
      setFormData((values) => ({ ...values, paymentGateway: '', paymentGatewayLabel: '' }))
    }
  }

  useEffect(() => {
    const data = getDefaultPaymentOptions(paymentData)
    setPaymentOption(data)
    if (!_.isEmpty(formData.paymentGateway)) {
      const selOption = _.filter(data, function (x) {
        if (_.includes(formData.paymentGateway._id, x.value)) {
          return x
        }
      })
      setSelectedPaymentOption(selOption)
    }
  }, [paymentData])

  return (
    <>
      <Modal showModal={showAddProcessor} modalWidth={550}>
        <div
          className=''
          id='processorModal'
        >
          <div className=''>
            <div className='p-5'>
              <div className='d-flex justify-content-between align-items-center'>
                <h2 className='me-8'>Payment Processor Details</h2>
                <button
                  type='button'
                  className='btn btn-lg btn-icon btn-active-light-primary close'
                  onClick={() => {
                    onCloseProcessor()
                  }}
                >
                  {/* eslint-disable */}
                  <KTSVG
                    path="/media/icons/duotune/arrows/arr061.svg"
                    className="svg-icon-1"
                  />
                  {/* eslint-disable */}
                </button>
              </div>
              <div className="bg-light">
                <form className="container-fixed">
                  <div className="card-body">
                    <div className="mb-0">
                      <div className="row">
                        <div className="mb-3">
                          <div className="row">
                            <div className="col-lg-4 mt-3">
                              <label className="font-size-xs font-weight-bold mb-3  form-label">
                                Method:
                              </label>
                            </div>
                            <div className="col-lg-8">
                              <select
                                name="method"
                                className="form-select form-select-solid bg-secondary "
                                onChange={(e) =>
                                  handleInputChange(e, "setPaymentProcessorDetails")
                                }
                                value={
                                  paymentProcesserDetails.method || ""
                                }
                              >
                                <option value=''>Select...</option>
                                <option value='API'>API</option>
                                <option value='OAUTH'>OAuth</option>
                              </select>
                              {errors && errors.method && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red}"}</style>
                                  {errors.method}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                        {paymentProcesserDetails && paymentProcesserDetails.method === 'API'
                          ? (
                            <>
                              <div className="mb-3">
                                <div className="row">
                                  <div className="col-lg-4 mt-3">
                                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                                      API End Point :
                                    </label>
                                  </div>
                                  <div className="col-lg-8">
                                    <input
                                      name="apiEndPoint"
                                      type="text"
                                      className="form-control bg-secondary"
                                      placeholder="API End Point"
                                      onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                      onChange={(e) =>
                                        handleInputChange(
                                          e,
                                          "setPaymentProcessorDetails"
                                        )
                                      }
                                      autoComplete="off"
                                      value={
                                        paymentProcesserDetails.apiEndPoint || ""
                                      }
                                      maxLength={42}
                                      onKeyPress={(e) => {
                                        if (
                                          !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                            e.key
                                          )
                                        ) {
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {errors && errors.apiEndPoint && (
                                      <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.apiEndPoint}
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                              <div className="mb-3">
                                <div className="row">
                                  <div className="col-lg-4 mt-3">
                                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                                      API Key :
                                    </label>
                                  </div>
                                  <div className="col-lg-8">
                                    <input
                                      name="apiKey"
                                      type="text"
                                      className="form-control bg-secondary"
                                      placeholder="API Key"
                                      onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                      onChange={(e) =>
                                        handleInputChange(
                                          e,
                                          "setPaymentProcessorDetails"
                                        )
                                      }
                                      autoComplete="off"
                                      value={paymentProcesserDetails.apiKey || ""}
                                      maxLength={42}
                                      onKeyPress={(e) => {
                                        if (
                                          !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                            e.key
                                          )
                                        ) {
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {errors && errors.apiKey && (
                                      <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.apiKey}
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                              <div className="mb-3">
                                <div className="row">
                                  <div className="col-lg-4 mt-3">
                                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                                      API Secret Key:
                                    </label>
                                  </div>
                                  <div className="col-lg-8">
                                    <input
                                      name="apiSecretKey"
                                      type="text"
                                      className="form-control bg-secondary"
                                      placeholder="API Secret Key"
                                      onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                      onChange={(e) =>
                                        handleInputChange(
                                          e,
                                          "setPaymentProcessorDetails"
                                        )
                                      }
                                      autoComplete="off"
                                      value={paymentProcesserDetails.apiSecretKey || ""}
                                      maxLength={42}
                                      onKeyPress={(e) => {
                                        if (
                                          !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                            e.key
                                          )
                                        ) {
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {errors && errors.apiSecretKey && (
                                      <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.apiSecretKey}
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </>
                          )
                          : null}
                        <div className="mb-3">
                          {paymentProcesserDetails && paymentProcesserDetails.method === 'OAUTH'
                            ? (
                              <>
                                <div className="row my-6">
                                  <div className="col-lg-4 mt-3">
                                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                                      OAuth Url:
                                    </label>
                                  </div>
                                  <div className="col-lg-8">
                                    <input
                                      name="oAuthUrl"
                                      type="text"
                                      className="form-control bg-secondary"
                                      placeholder="OAuth Url"
                                      onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                      onChange={(e) =>
                                        handleInputChange(
                                          e,
                                          "setPaymentProcessorDetails"
                                        )
                                      }
                                      autoComplete="off"
                                      value={paymentProcesserDetails.oAuthUrl || ""}
                                      maxLength={42}
                                      onKeyPress={(e) => {
                                        if (
                                          !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                                        ) {
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {errors && errors.oAuthUrl && (
                                      <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.oAuthUrl}
                                      </div>
                                    )}
                                  </div>
                                </div>
                                <div className="row my-3">
                                  <div className="col-lg-6">
                                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                                      User Name :
                                    </label>
                                    <input
                                      name="oAuthUserName"
                                      type="text"
                                      className="form-control bg-secondary"
                                      placeholder="User Name"
                                      onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                      onChange={(e) =>
                                        handleInputChange(
                                          e,
                                          "setPaymentProcessorDetails"
                                        )
                                      }
                                      autoComplete="off"
                                      value={paymentProcesserDetails.oAuthUserName || ""}
                                      maxLength={42}
                                      onKeyPress={(e) => {
                                        if (
                                          !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                                        ) {
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {errors && errors.oAuthUserName && (
                                      <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.oAuthUserName}
                                      </div>
                                    )}
                                  </div>
                                  <div className="col-lg-6">
                                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                                      Password :
                                    </label>
                                    <input
                                      name="oAuthPassword"
                                      type="password"
                                      className="form-control bg-secondary"
                                      placeholder="Password"
                                      onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                      onChange={(e) =>
                                        handleInputChange(
                                          e,
                                          "setPaymentProcessorDetails"
                                        )
                                      }
                                      autoComplete="off"
                                      value={paymentProcesserDetails.oAuthPassword || ""}
                                      maxLength={42}
                                      onKeyPress={(e) => {
                                        if (
                                          !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                            e.key
                                          )
                                        ) {
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {errors && errors.oAuthPassword && (
                                      <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.oAuthPassword}
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </>
                            ) : null}
                        </div>
                        <div className="col-lg-4 mt-0">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Mode of Dispute submission:
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <select
                            name="disputeSubmissionMode"
                            className="form-select form-select-solid bg-secondary "
                            onChange={(e) =>
                              handleInputChange(e, "setPaymentProcessorDetails")
                            }
                            value={
                              paymentProcesserDetails.disputeSubmissionMode || ""
                            }
                          >
                            <option value="">Select submission mode...</option>
                            <option value="SFTP">SFTP</option>
                            <option value="FAX">Fax</option>
                            <option value="EMAIL">Email</option>
                            <option value="API">API</option>
                          </select>
                          {errors && errors.disputeSubmissionMode && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.disputeSubmissionMode}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      {paymentProcesserDetails.disputeSubmissionMode ===
                        "SFTP" ? (
                        <>
                          <div className="row">
                            <div className="col-lg-8">
                              <label className="font-size-xs font-weight-bold mb-3  form-label">
                                Host Name :
                              </label>
                              <input
                                name="hostName"
                                type="text"
                                className="form-control bg-secondary"
                                placeholder="Host Name"
                                onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                onChange={(e) =>
                                  handleInputChange(
                                    e,
                                    "setPaymentProcessorDetails"
                                  )
                                }
                                autoComplete="off"
                                value={paymentProcesserDetails.hostName || ""}
                                maxLength={42}
                                onKeyPress={(e) => {
                                  if (
                                    !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                                  ) {
                                    e.preventDefault();
                                  }
                                }}
                              />
                              {errors && errors.hostName && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red}"}</style>
                                  {errors.hostName}
                                </div>
                              )}
                            </div>
                            <div className="col-lg-4">
                              <label className="font-size-xs font-weight-bold mb-3  form-label">
                                Port Number :
                              </label>
                              <input
                                name="portNumber"
                                type="text"
                                className="form-control bg-secondary"
                                placeholder="Port Number"
                                onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                onChange={(e) =>
                                  handleInputChange(
                                    e,
                                    "setPaymentProcessorDetails"
                                  )
                                }
                                autoComplete="off"
                                value={paymentProcesserDetails.portNumber || ""}
                                maxLength={10}
                                onKeyPress={(e) => {
                                  if (!REGEX.NUMERIC.test(e.key)) {
                                    e.preventDefault();
                                  }
                                }}
                              />
                              {errors && errors.portNumber && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red}"}</style>
                                  {errors.portNumber}
                                </div>
                              )}
                            </div>
                          </div>
                          <div className="row mt-5">
                            <div className="col-lg-6">
                              <label className="font-size-xs font-weight-bold mb-3  form-label">
                                User Name :
                              </label>
                              <input
                                name="username"
                                type="text"
                                className="form-control bg-secondary"
                                placeholder="User Name"
                                onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                onChange={(e) =>
                                  handleInputChange(
                                    e,
                                    "setPaymentProcessorDetails"
                                  )
                                }
                                autoComplete="off"
                                value={paymentProcesserDetails.username || ""}
                                maxLength={42}
                                onKeyPress={(e) => {
                                  if (
                                    !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                                  ) {
                                    e.preventDefault();
                                  }
                                }}
                              />
                              {errors && errors.username && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red}"}</style>
                                  {errors.username}
                                </div>
                              )}
                            </div>
                            <div className="col-lg-6">
                              <label className="font-size-xs font-weight-bold mb-3  form-label">
                                Password :
                              </label>
                              <input
                                name="password"
                                type="password"
                                className="form-control bg-secondary"
                                placeholder="Password"
                                onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                onChange={(e) =>
                                  handleInputChange(
                                    e,
                                    "setPaymentProcessorDetails"
                                  )
                                }
                                autoComplete="off"
                                value={paymentProcesserDetails.password || ""}
                                maxLength={42}
                                onKeyPress={(e) => {
                                  if (
                                    !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                      e.key
                                    )
                                  ) {
                                    e.preventDefault();
                                  }
                                }}
                              />
                              {errors && errors.password && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red}"}</style>
                                  {errors.password}
                                </div>
                              )}
                            </div>
                          </div>
                        </>
                      ) : null}
                      {paymentProcesserDetails.disputeSubmissionMode === "FAX" ? (
                        <div className="row">
                          <div className="col-lg-4 mt-3">
                            <label className="font-size-xs font-weight-bold mb-3  form-label">
                              Fax Number :
                            </label>
                          </div>
                          <div className="col-lg-8">
                            <input
                              name="faxNumber"
                              type="text"
                              className="form-control bg-secondary"
                              placeholder="Fax Number"
                              maxLength={10}
                              onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                              onChange={(e) =>
                                handleInputChange(e, "setPaymentProcessorDetails")
                              }
                              autoComplete="off"
                              value={paymentProcesserDetails.faxNumber || ""}
                            />
                            {errors && errors.faxNumber && (
                              <div className="rr mt-1">
                                <style>{".rr{color:red}"}</style>
                                {errors.faxNumber}
                              </div>
                            )}
                          </div>
                        </div>
                      ) : null}
                      {paymentProcesserDetails.disputeSubmissionMode === "API" ? (
                        <div className="card-body">
                          <div className="mb-3">
                            <div className="row">
                              <div className="col-lg-4 mt-3">
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                  API End Point :
                                </label>
                              </div>
                              <div className="col-lg-8">
                                <input
                                  name="requestApiEndPoint"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="API End Point"
                                  onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                  onChange={(e) =>
                                    handleInputChange(
                                      e,
                                      "setPaymentProcessorDetails"
                                    )
                                  }
                                  autoComplete="off"
                                  value={
                                    paymentProcesserDetails.requestApiEndPoint || ""
                                  }
                                  maxLength={42}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                        e.key
                                      )
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                                {errors && errors.requestApiEndPoint && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.requestApiEndPoint}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="mb-3">
                            <div className="row">
                              <div className="col-lg-4 mt-3">
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                  API Key :
                                </label>
                              </div>
                              <div className="col-lg-8">
                                <input
                                  name="requestApiKey"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="API Key"
                                  onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                  onChange={(e) =>
                                    handleInputChange(
                                      e,
                                      "setPaymentProcessorDetails"
                                    )
                                  }
                                  autoComplete="off"
                                  value={paymentProcesserDetails.requestApiKey || ""}
                                  maxLength={42}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                        e.key
                                      )
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                                {errors && errors.requestApiKey && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.requestApiKey}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="mb-3">
                            <div className="row">
                              <div className="col-lg-4 mt-3">
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                  API Secret Key:
                                </label>
                              </div>
                              <div className="col-lg-8">
                                <input
                                  name="requestApiSecret"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="API Secret Key"
                                  onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                  onChange={(e) =>
                                    handleInputChange(
                                      e,
                                      "setPaymentProcessorDetails"
                                    )
                                  }
                                  autoComplete="off"
                                  value={paymentProcesserDetails.requestApiSecret || ""}
                                  maxLength={42}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(
                                        e.key
                                      )
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                                {errors && errors.requestApiSecret && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.requestApiSecret}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : null}
                      {paymentProcesserDetails.disputeSubmissionMode ===
                        "EMAIL" ? (
                        <>
                          <div className="mb-3">
                            <div className="row">
                              <div className="col-lg-4 mt-3">
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                  Email :
                                </label>
                              </div>
                              <div className="col-lg-8">
                                <input
                                  name="email"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="Email"
                                  onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                  onChange={(e) =>
                                    handleInputChange(e, "setPaymentProcessorDetails")
                                  }
                                  autoComplete="off"
                                  value={paymentProcesserDetails.email || ""}
                                />
                                {errors && errors.email && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.email}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="mb-3">
                            <div className="row">
                              <div className="col-lg-4 mt-3">
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                  BCC :
                                </label>
                              </div>
                              <div className="col-lg-8">
                                <input
                                  name="emailBcc"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="BCC"
                                  onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                  onChange={(e) =>
                                    handleInputChange(e, "setPaymentProcessorDetails")
                                  }
                                  autoComplete="off"
                                  value={paymentProcesserDetails.emailBcc || ""}
                                />
                                {errors && errors.emailBcc && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.emailBcc}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="mb-3">
                            <div className="row">
                              <div className="col-lg-4 mt-3">
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                  CC :
                                </label>
                              </div>
                              <div className="col-lg-8">
                                <input
                                  name="emailCc"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="CC"
                                  onBlur={e => handleTrimWhiteSpace(e, setPaymentProcessorDetails)}
                                  onChange={(e) =>
                                    handleInputChange(e, "setPaymentProcessorDetails")
                                  }
                                  autoComplete="off"
                                  value={paymentProcesserDetails.emailCc || ""}
                                />
                                {errors && errors.emailCc && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.emailCc}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </>
                      ) : null}
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-green m-2 fa-pull-right"
                          onClick={() => {
                            handleAddProcessorTable();
                          }}
                        >
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <Modal showModal={show} modalWidth={550}>
        <div
          className=""
          id="crmModal"
        >
          <div className="">
            <div className="p-5">
              <div className="d-flex justify-content-between align-items-center">
                <h2 className="me-8">Add CRM</h2>
                <button
                  type="button"
                  className="btn btn-lg btn-icon btn-active-light-primary close"
                  onClick={() => {
                    setShow(false);
                    setSelectedCrmOption()
                    setForm({
                      apiKey: "",
                      apiSecretKey: "",
                      apiEndPoint: "",
                      crmId: "",
                      crmLabel: ""
                    });
                    setErrors((values) => ({
                      ...values,
                      apiKey: "",
                      apiSecretKey: "",
                      apiEndPoint: "",
                      crmId: "",
                      crmLabel: ""
                    }))
                    setEditMode(false);
                    setFormData({ crmId: "", crmLabel: "" });
                  }}
                >
                  {/* eslint-disable */}
                  <KTSVG
                    path="/media/icons/duotune/arrows/arr061.svg"
                    className="svg-icon-1"
                  />
                  {/* eslint-disable */}
                </button>
              </div>
              <div className="bg-light">
                <form className="container-fixed">
                  <div className="card-body">
                    {form.crmLabel ? (
                      <h4 className="me-8 mb-2">{form.crmLabel}</h4>
                    ) : null}
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-lg-4 mt-3">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            API End Point :
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <input
                            name="apiEndPoint"
                            type="text"
                            className="form-control bg-skyBlue"
                            placeholder="API End Point"
                            onChange={(e) => handleInputChange(e, "setForm")}
                            onBlur={e => handleTrimWhiteSpace(e, setForm)}
                            autoComplete="off"
                            value={form.apiEndPoint || ""}
                            maxLength={42}
                            onKeyPress={(e) => {
                              if (
                                !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                              ) {
                                e.preventDefault();
                              }
                            }}
                          />
                          {errors && errors.apiEndPoint && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.apiEndPoint}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-lg-4 mt-3">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            API Key :
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <input
                            name="apiKey"
                            type="text"
                            className="form-control bg-skyBlue"
                            placeholder="API Key"
                            onChange={(e) => handleInputChange(e, "setForm")}
                            onBlur={e => handleTrimWhiteSpace(e, setForm)}
                            autoComplete="off"
                            value={form.apiKey || ""}
                            maxLength={42}
                            onKeyPress={(e) => {
                              if (
                                !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                              ) {
                                e.preventDefault();
                              }
                            }}
                          />
                          {errors && errors.apiKey && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.apiKey}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-lg-4 mt-3">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            API Secret :
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <input
                            name="apiSecretKey"
                            type="text"
                            className="form-control bg-skyBlue"
                            placeholder="API Secret"
                            onBlur={e => handleTrimWhiteSpace(e, setForm)}
                            onChange={(e) => handleInputChange(e, "setForm")}
                            autoComplete="off"
                            value={form.apiSecretKey || ""}
                            maxLength={42}
                            onKeyPress={(e) => {
                              if (
                                !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                              ) {
                                e.preventDefault();
                              }
                            }}
                          />
                          {errors && errors.apiSecretKey && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.apiSecretKey}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-green m-2 fa-pull-right"
                          onClick={() => {
                            handleAddTable();
                          }}
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <Modal showModal={showAddGateway} modalWidth={550}>
        <div
          className=''
          id='gatewayModal'
        >
          <div className=''>
            <div className='p-5'>
              <div className='d-flex justify-content-between align-items-center'>
                <h2 className='me-8'>Payment Gateway Details</h2>
                <button
                  type='button'
                  className='btn btn-lg btn-icon btn-active-light-primary close'
                  onClick={() => {
                    setSelectedPaymentOption()
                    setShowAddGateway(false)
                    setPaymentGatewayDetails({
                      paymentGatewayLabel: '',
                      paymentGateway: '',
                      gatewayEndPoint: '',
                      gatewayApiKey: '',
                      gatewayApiSecretKey: '',
                      disputeSubmissionMode: ''
                    })
                    setErrors((values) => ({
                      ...values,
                      paymentGatewayLabel: '',
                      paymentGateway: '',
                      gatewayEndPoint: '',
                      gatewayApiKey: '',
                      gatewayApiSecretKey: '',
                      disputeSubmissionMode: ''
                    }))
                    setEditMode(false)
                    setFormData({ paymentGateway: '', paymentGatewayLabel: '' })
                  }}
                >
                  {/* eslint-disable */}
                  <KTSVG
                    path="/media/icons/duotune/arrows/arr061.svg"
                    className="svg-icon-1"
                  />
                  {/* eslint-disable */}
                </button>
              </div>
              <div className="bg-light">
                <form className="container-fixed">
                  <div className="card-body">
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-lg-4 mt-3">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            API End Point :
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <input
                            name="gatewayEndPoint"
                            type="text"
                            className="form-control bg-skyBlue"
                            placeholder="API End Point"
                            onBlur={e => handleTrimWhiteSpace(e, setPaymentGatewayDetails)}
                            onChange={(e) =>
                              handleInputChange(e, "setPaymentGatewayDetails")
                            }
                            autoComplete="off"
                            value={paymentGatewayDetails.gatewayEndPoint || ""}
                            maxLength={42}
                            onKeyPress={(e) => {
                              if (
                                !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                              ) {
                                e.preventDefault();
                              }
                            }}
                          />
                          {errors && errors.gatewayEndPoint && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.gatewayEndPoint}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-lg-4 mt-3">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            API Key :
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <input
                            name="gatewayApiKey"
                            type="text"
                            className="form-control bg-skyBlue"
                            placeholder="API Key"
                            onBlur={e => handleTrimWhiteSpace(e, setPaymentGatewayDetails)}
                            onChange={(e) =>
                              handleInputChange(e, "setPaymentGatewayDetails")
                            }
                            autoComplete="off"
                            value={paymentGatewayDetails.gatewayApiKey || ""}
                            maxLength={42}
                            onKeyPress={(e) => {
                              if (
                                !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                              ) {
                                e.preventDefault();
                              }
                            }}
                          />
                          {errors && errors.gatewayApiKey && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.gatewayApiKey}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-lg-4 mt-3">
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            API Secret Key:
                          </label>
                        </div>
                        <div className="col-lg-8">
                          <input
                            name="gatewayApiSecretKey"
                            type="text"
                            className="form-control bg-skyBlue"
                            placeholder="API Secret Key"
                            onBlur={e => handleTrimWhiteSpace(e, setPaymentGatewayDetails)}
                            onChange={(e) =>
                              handleInputChange(e, "setPaymentGatewayDetails")
                            }
                            autoComplete="off"
                            value={
                              paymentGatewayDetails.gatewayApiSecretKey || ""
                            }
                            maxLength={42}
                            onKeyPress={(e) => {
                              if (
                                !REGEX.ALPHA_NUMERIC_SPECIAL_CHARS.test(e.key)
                              ) {
                                e.preventDefault();
                              }
                            }}
                          />
                          {errors && errors.gatewayApiSecretKey && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.gatewayApiSecretKey}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row pb-5">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-green m-2 fa-pull-right"
                          onClick={() => {
                            handleSaveGatewayTable();
                          }}
                        >
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <div className="card-header bg-skyBlue py-10">
        <div className="card-body">
          <h2 className="mb-5">CRM</h2>
          <div className="form-group row mb-2">
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="d-flex justify-content-start my-auto">
                    <div className="col-lg-5 pr-3 me-3">
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name="crmId"
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeCRM}
                        options={crmOption}
                        value={selectedCrmOption}
                      />
                    </div>
                    <div>
                      <button
                        className="btn btn-green mb-3 py-2"
                        onClick={() => {
                          handleAddClick("CRM");
                        }}
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {showTable ? (
            <>
              <div className="w-100 d-flex justify-content-center mt-2">
                <div className={`card ${className}`}>
                  <div className="card-body py-3">
                    <div className="table-responsive">
                      <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                        <thead>
                          <tr className="fw-bold fs-6 text-gray-800">
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>S.No</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>CRM</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>API Key</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>API Secret</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>API End Point</span>
                              </div>
                            </th>
                            <th className="min-w-80px text-start">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {tableData.length > 0 ? (
                            tableData.map((item, i) => (
                              <tr key={i} className="px-2">
                                <td className="pb-0 pt-5  text-start">
                                  {i + 1}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item.crmLabel}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item.apiKey}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item.apiSecretKey}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item.apiEndPoint}
                                </td>
                                <td className="pb-0 pt-3 text-start">
                                  <div className="my-auto d-flex">
                                    <button
                                      className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm"
                                      onClick={() => {
                                        onEditClick(item);
                                      }}
                                    >
                                      {/* eslint-disable */}
                                      <KTSVG
                                        path="/media/icons/duotune/art/art005.svg"
                                        className="svg-icon-3"
                                      />
                                      {/* eslint-enable */}
                                    </button>
                                    <button
                                      className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                                      onClick={() => {
                                        onDeleteClick(item)
                                      }}
                                    >
                                      {/* eslint-disable */}
                                      <KTSVG
                                        path="/media/icons/duotune/general/gen027.svg"
                                        className="svg-icon-3"
                                      />
                                      {/* eslint-enable */}
                                    </button>
                                  </div>
                                </td>
                              </tr>
                            ))
                          ) : (
                            <tr className='text-center py-3'>
                              <td colSpan='100%'>No record(s) found</td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : null}
          {errors && errors.crmForm && (
            <div className='rr mt-0 text-danger'>
              <style>{'.rr{color:red}'}</style>
              {errors.crmForm}
            </div>
          )}
        </div>
        <div className='card-body mt-5'>
          <h2 className='mb-5'>Payment Processor</h2>
          <div className='form-group row mb-2'>
            <div className='row'>
              <div className='col-lg-12'>
                <div className='row'>
                  <div className='d-flex justify-content-start my-auto'>
                    <div className='col-lg-5 pr-3 me-3'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='crmId'
                        className='basic-single'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeProcessor}
                        options={processorOption}
                        value={selectedProcessorOption}
                      />
                      {errors && errors.paymentProcessorId && (
                        <div className='rr mt-1'>
                          <style>{'.rr{color:red}'}</style>
                          {errors.paymentProcessorId}
                        </div>
                      )}
                    </div>
                    <div>
                      <button
                        className='btn btn-green mb-3 py-2'
                        onClick={() => {
                          handleAddClick('PAYMENT_PROCESSOR')
                        }}
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {errors && errors.crmForm && (
            <div className='rr mt-0 text-danger'>
              <style>{'.rr{color:red}'}</style>
              {errors.crmForm}
            </div>
          )}
          {showSFTPTable ? (
            <>
              <div className='w-100 d-flex justify-content-start mt-2'>
                <div className={`card ${className}`}>
                  <div className='card-body py-3'>
                    <div className='table-responsive'>
                      <table className='table table-hover table-rounded table-striped border w-100 gs-2 mt-2'>
                        <thead>
                          <tr className='fw-bold fs-6 text-gray-800'>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>S.No</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>Payment Processor</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>Submission mode</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>API End Point</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>API Key</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>API Secret</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>OAuth URL</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>Username</span>
                              </div>
                            </th>
                            <th className='min-w-200px text-start'>
                              <div className='d-flex'>
                                <span>Password</span>
                              </div>
                            </th>
                            <th className='min-w-80px text-start'>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {processorTableData.length > 0 ? (
                            processorTableData.map((item, i) => {
                              return (
                                <tr key={i} className='px-2'>
                                  <td className='pb-0 pt-5  text-start'>
                                    {i + 1}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.processorLabel ? item.processorLabel : '--'}
                                  </td>
                                  <td className='pb-0 pt-5 text-start'>
                                    {item.disputeSubmissionMode ? item.disputeSubmissionMode : '--'}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.apiKey ? item.apiKey : '--'}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.apiSecretKey ? item.apiSecretKey : '--'}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.apiEndPoint ? item.apiEndPoint : '--'}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.oAuthUrl ? item.oAuthUrl : '--'}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.oAuthUserName ? item.oAuthUserName : '--'}
                                  </td>
                                  <td className='pb-0 pt-5  text-start'>
                                    {item.oAuthPassword ? item.oAuthPassword : '--'}
                                  </td>
                                  <td className='pb-0 pt-3 text-start'>
                                    <div className='my-auto d-flex'>
                                      <button
                                        className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                                        onClick={() => {
                                          onEditProcessorClick(item)
                                        }}
                                      >
                                        {/* eslint-disable */}
                                        <KTSVG
                                          path="/media/icons/duotune/art/art005.svg"
                                          className="svg-icon-3"
                                        />
                                        {/* eslint-enable */}
                                      </button>
                                      <button
                                        className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                                        onClick={() => {
                                          onDeleteProcessorClick(item)
                                        }}
                                      >
                                        {/* eslint-disable */}
                                        <KTSVG
                                          path="/media/icons/duotune/general/gen027.svg"
                                          className="svg-icon-3"
                                        />
                                        {/* eslint-enable */}
                                      </button>
                                    </div>
                                  </td>
                                </tr>
                              )
                            })
                          ) : (
                            <tr className='text-center py-3'>
                              <td colSpan='100%'>No record(s) found</td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : null}
        </div>
        <div className='card-body mt-5'>
          <h2 className='mb-5'>Payment Gateway</h2>
          <div className='form-group row mb-2'>
            <div className='row'>
              <div className='col-lg-12'>
                <div className='row'>
                  <div className='d-flex justify-content-start my-auto'>
                    <div className='col-lg-5 pr-3 me-3'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='crmId'
                        className='basic-single'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangePayment}
                        options={paymentOption}
                        value={selectedPaymentOption}
                      />
                    </div>
                    <div>
                      <button
                        className='btn btn-green mb-3 py-2'
                        onClick={() => {
                          handleAddClick('PAYMENT_GATEWAY')
                        }}
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {showGatewayTable ? (
            <div className='w-100 d-flex justify-content-start mt-2'>
              <div className={`card ${className}`}>
                <div className='card-body py-3'>
                  <div className='table-responsive'>
                    <table className='table table-hover table-rounded table-striped border w-100 gs-2 mt-6'>
                      <thead>
                        <tr className='fw-bold fs-6 text-gray-800'>
                          <th className='min-w-200px text-start'>
                            <div className='d-flex'>
                              <span>S.No</span>
                            </div>
                          </th>
                          <th className='min-w-200px text-start'>
                            <div className='d-flex'>
                              <span>Gateway Api Key</span>
                            </div>
                          </th>
                          <th className='min-w-200px text-start'>
                            <div className='d-flex'>
                              <span>Gateway End Point</span>
                            </div>
                          </th>
                          <th className='min-w-200px text-start'>
                            <div className='d-flex'>
                              <span>Gateway Api Secret Key</span>
                            </div>
                          </th>
                          <th className='min-w-80px text-start'>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {paymentTableData.length > 0 ? (
                          paymentTableData.map((item, i) => (
                            <tr key={i} className='px-2'>
                              <td className='pb-0 pt-5 text-start'>{i + 1}</td>
                              <td className='pb-0 pt-5 text-start'>
                                {item.gatewayApiKey}
                              </td>
                              <td className='pb-0 pt-5 text-start'>
                                {item.gatewayEndPoint}
                              </td>
                              <td className='pb-0 pt-5 text-start'>
                                {item.gatewayApiSecretKey}
                              </td>
                              <td className='pb-0 pt-3 text-start'>
                                <div className='my-auto d-flex'>
                                  <button
                                    className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                                    onClick={() => {
                                      onEditGateClick(item)
                                    }}
                                  >
                                    {/* eslint-disable */}
                                    <KTSVG
                                      path="/media/icons/duotune/art/art005.svg"
                                      className="svg-icon-3"
                                    />
                                    {/* eslint-enable */}
                                  </button>
                                  <button
                                    className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                                    onClick={() => {
                                      onDeleteGateClick(item)
                                    }}
                                  >
                                    {/* eslint-disable */}
                                    <KTSVG
                                      path="/media/icons/duotune/general/gen027.svg"
                                      className="svg-icon-3"
                                    />
                                    {/* eslint-enable */}
                                  </button>
                                </div>
                              </td>
                            </tr>
                          ))
                        ) : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
        <div className='form-group row mb-4 mt-3'>
          <div className='col-lg-6' />
          <div className='col-lg-6'>
            <div className='col-lg-11'>
              <button
                className='btn btn-orange m-2 fa-pull-right'
                onClick={() => {
                  handleNextClick()
                }}
              >
                Next
              </button>
              <button
                className='btn btn-darkRed m-2 fa-pull-right'
                onClick={() => {
                  goBack(1)
                }}
              >
                Back
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  getCRM: state && state.crmStore && state.crmStore.getCRM,
  loading: state && state.crmStore && state.crmStore.loading,
  processorLoading: state && state.processorStore && state.processorStore.processorLoading,
  processorData: state && state.processorStore && state.processorStore.processorData,
  paymentLoading: state && state.paymentStore && state.paymentStore.paymentLoading,
  paymentData: state && state.paymentStore && state.paymentStore.paymentData
})

const mapDispatchToProps = (dispatch) => ({
  getCRMDispatch: () => dispatch(crmActions.getCRM()),
  crmActions: (data) => dispatch(crmActions.getCRM(data)),
  getProcessorDispatch: () => dispatch(processorActions.getProcessor()),
  getPaymentDispatch: () => dispatch(paymentActions.getPayment())
})

export default connect(mapStateToProps, mapDispatchToProps)(CrmForm)
