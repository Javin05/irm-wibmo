import React, { Fragment, useEffect, useState } from "react"
import { connect } from "react-redux"
import {
  ClientPlayStoreActions,
  PostClientPlayStoreActions
} from "../../../../../store/actions"
import { STATUS_RESPONSE } from "../../../../../utils/constants"
import { successAlert, warningAlert } from "../../../../../utils/alerts"
import { useLocation } from 'react-router-dom'
import _ from 'lodash'

const PlaystoreRiskAnalaysisConfig = (props) => {
  const {
    getClientPlayStorelistDispatch,
    clientReports,
    clientLoading,
    PostClientPlayStoreDispatch,
    PostClientPlaStoreReports,
    postclearClientPlayStorelistDispatch,
    postclientPlayLoading
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update/')
  const currentRoute = url && url[1]
  const [formData, setFormData] = useState([])
  
  useEffect(() => {
    getClientPlayStorelistDispatch(currentRoute)
  }, [])

  useEffect(() => {
    if (clientReports && clientReports.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setFormData(clientReports && clientReports.data)
    }
  }, [clientReports])

  const handelReset = () =>{
    getClientPlayStorelistDispatch(currentRoute)
  }

  let handleSave = () => {
    const data = _.filter(formData, function (o) { return o.is_enable === "YES" })
    const params = {
        "reportparams": data
    }
    PostClientPlayStoreDispatch(currentRoute, params)
  }

  let handleChange = (e, index) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData(list => list.map((item, i) =>
        i === index
          ? {
            ...item,
            [`is_enable`]: "YES"
          }
          : item
      ))
    } else if (e.target.checked === false) {
      setFormData(list => list.map((item, i) =>
        i === index
          ? {
            ...item,
            [`is_enable`]: "NO"
          }
          : item
      ))
    }
  }

  let positionChange = (e, index) => {
    e.persist()
    setFormData(list => list.map((item, i) =>
      i === index
        ? {
          ...item,
          'position': e.target.value
        }
        : item
    ))
  }

  useEffect(() => {
    if (PostClientPlaStoreReports && PostClientPlaStoreReports.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        PostClientPlaStoreReports && PostClientPlaStoreReports.message,
        'success'
      )
      postclearClientPlayStorelistDispatch()
      getClientPlayStorelistDispatch(currentRoute)
    } else if (PostClientPlaStoreReports && PostClientPlaStoreReports.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        PostClientPlaStoreReports && PostClientPlaStoreReports.message,
        "",
        "Cancel",
        "Ok",
        () => { },
        () => { }
      )
      postclearClientPlayStorelistDispatch()
    }
  }, [PostClientPlaStoreReports])


  return (
    <Fragment>
      <>
        {!clientLoading ? (
          <div
            className="d-flex flex-row-reverse bd-highlight tab-pane"
            id="exportlayout"
          >

            <div className="card-body bg-white">
              <div className="modal-body py-lg-10 px-lg-10">
                <div className="pt-1">
                  <div className="row">
                    {
                      clientReports && clientReports.data.map((item, i) => {
                        return (
                          <>
                            <div className="col-lg-4 border-left-lg-1">
                              <div className="menu-inline menu-column">
                                <div className="mb-5">
                                  <label className="form-check form-check-sm form-check-custom form-check-solid me-3">
                                    <input
                                      className="form-check-input"
                                      type="checkbox"
                                      name={item.report_key}
                                      value={item.report_key}
                                      defaultChecked={item.is_enable === "YES"}
                                      onChange={(e) => handleChange(e, i, item)}
                                    />
                                    <span
                                      className="form-check-label"
                                      htmlFor="flexCheckChecked"
                                    >
                                      {item.report_value}
                                    </span>
                                    <input
                                      className="form-control w-100px ms-2"
                                      type="text"
                                      name='position'
                                      placeholder="Position"
                                      defaultValue={item.position}
                                      onChange={(e) => positionChange(e, i, item)}
                                    />
                                  </label>
                                </div>
                              </div>
                            </div>
                          </>
                        )
                      })
                    }
                  </div>
                  <div className="pt-1">
                    <div className="form-group row mb-4">
                      <div className="col-lg-6 d-flex justify-content-end">
                        <button
                          type="button"
                          className="btn btn-lg btn-icon btn-active-light-primary close"
                          data-dismiss="modal"
                        >
                        </button>
                      </div>
                      <div className="col-lg-6 d-flex justify-content-end">
                        <button
                          className="btn btn-sm btn-light-danger close me-2"
                          onClick={() => handelReset()}
                        >
                          Reset
                        </button>
                        <button
                          className="btn btn-sm btn-light-primary close"
                          onClick={() => handleSave()}
                          disabled={postclientPlayLoading}
                        >
                          {!postclientPlayLoading && <span className='indicator-label'>Submit</span>}
                          {postclientPlayLoading && (
                            <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                              Please wait...
                              <span className='spinner-border spinner-border-sm align-middle ms-2' />
                            </span>
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <span
            className="spinner-border spinner-border-sm mx-3"
            role="status"
            aria-hidden="true"
          />
        )}
      </>
    </Fragment>
  )
}

const mapStateToProps = (state) => ({
  clientReports: state.clientPlayStoreReportStore && state.clientPlayStoreReportStore.clientPlayStoreReports,
  clientLoading: state.clientPlayStoreReportStore && state.clientPlayStoreReportStore.loading,
  PostClientPlaStoreReports: state.PostClientPlaStoreReportStore && state.PostClientPlaStoreReportStore.PostClientPlaStoreReports,
  postclientPlayLoading: state.PostClientPlaStoreReportStore && state.PostClientPlaStoreReportStore.loading
})
const mapDispatchToProps = (dispatch) => ({
  getClientPlayStorelistDispatch: (params) => dispatch(ClientPlayStoreActions.getClientPlayStorelist(params)),
  PostClientPlayStoreDispatch: (id, params) => dispatch(PostClientPlayStoreActions.PostClientPlayStore(id, params)),
  postclearClientPlayStorelistDispatch: () => dispatch(PostClientPlayStoreActions.postclearClientPlayStorlist()),
})

export default connect(mapStateToProps, mapDispatchToProps)(PlaystoreRiskAnalaysisConfig)