import React, { Fragment, useEffect, useState } from "react"
import { connect } from "react-redux"
import {
  ClientWebActions,
  PostClientWebActions
} from "../../../../../store/actions"
import { STATUS_RESPONSE } from "../../../../../utils/constants"
import { successAlert, warningAlert } from "../../../../../utils/alerts"
import { useLocation } from 'react-router-dom'
import _ from 'lodash'

const WRMExportConfig = (props) => {
  const {
    getClientWeblistDispatch,
    clientReports,
    clientLoading,
    PostClientWebDispatch,
    postclientReports,
    postclearClientWeblistDispatch,
    postclientLoading
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update/')
  const currentRoute = url && url[1]
  const [formData, setFormData] = useState([])
  
  useEffect(() => {
    getClientWeblistDispatch(currentRoute)
  }, [])

  useEffect(() => {
    if (clientReports && clientReports.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setFormData(clientReports && clientReports.data)
    }
  }, [clientReports])

  const handelReset = () =>{
    getClientWeblistDispatch(currentRoute)
  }

  let handleSave = () => {
    const data = _.filter(formData, function (o) { return o.is_enable === "YES" })
    const params = {
        "reportparams": data
    }
    PostClientWebDispatch(currentRoute, params)
  }

  let handleChange = (e, index) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData(list => list.map((item, i) =>
        i === index
          ? {
            ...item,
            [`is_enable`]: "YES"
          }
          : item
      ))
    } else if (e.target.checked === false) {
      setFormData(list => list.map((item, i) =>
        i === index
          ? {
            ...item,
            [`is_enable`]: "NO"
          }
          : item
      ))
    }
  }

  let positionChange = (e, index) => {
    e.persist()
    setFormData(list => list.map((item, i) =>
      i === index
        ? {
          ...item,
          'position': e.target.value
        }
        : item
    ))
  }

  useEffect(() => {
    if (postclientReports && postclientReports.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        postclientReports && postclientReports.message,
        'success'
      )
      postclearClientWeblistDispatch()
      getClientWeblistDispatch(currentRoute)
    } else if (postclientReports && postclientReports.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        postclientReports && postclientReports.message,
        "",
        "Cancel",
        "Ok",
        () => { },
        () => { }
      )
      postclearClientWeblistDispatch()
    }
  }, [postclientReports])


  return (
    <Fragment>
      <>
        {!clientLoading ? (
          <div
            className="d-flex flex-row-reverse bd-highlight tab-pane"
            id="exportlayout"
          >

            <div className="card-body bg-white">
              <div className="modal-body py-lg-10 px-lg-10">
                <div className="pt-1">
                  <div className="row">
                    {
                      clientReports && clientReports.data.map((item, i) => {
                        return (
                          <>
                            <div className="col-lg-4 border-left-lg-1">
                              <div className="menu-inline menu-column">
                                <div className="mb-5">
                                  <label className="form-check form-check-sm form-check-custom form-check-solid me-3">
                                    <input
                                      className="form-check-input"
                                      type="checkbox"
                                      name={item.report_key}
                                      value={item.report_key}
                                      defaultChecked={item.is_enable === "YES"}
                                      onChange={(e) => handleChange(e, i, item)}
                                    />
                                    <span
                                      className="form-check-label"
                                      htmlFor="flexCheckChecked"
                                    >
                                      {item.report_value}
                                    </span>
                                    <input
                                      className="form-control w-100px ms-2"
                                      type="text"
                                      name='position'
                                      placeholder="Position"
                                      defaultValue={item.position}
                                      onChange={(e) => positionChange(e, i, item)}
                                    />
                                  </label>
                                </div>
                              </div>
                            </div>
                          </>
                        )
                      })
                    }
                  </div>
                  <div className="pt-1">
                    <div className="form-group row mb-4">
                      <div className="col-lg-6 d-flex justify-content-end">
                        <button
                          type="button"
                          className="btn btn-lg btn-icon btn-active-light-primary close"
                          data-dismiss="modal"
                        >
                        </button>
                      </div>
                      <div className="col-lg-6 d-flex justify-content-end">
                        <button
                          className="btn btn-sm btn-light-danger close me-2"
                          onClick={() => handelReset()}
                        >
                          Reset
                        </button>
                        <button
                          className="btn btn-sm btn-light-primary close"
                          onClick={() => handleSave()}
                          disabled={postclientLoading}
                        >
                          {!postclientLoading && <span className='indicator-label'>Submit</span>}
                          {postclientLoading && (
                            <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                              Please wait...
                              <span className='spinner-border spinner-border-sm align-middle ms-2' />
                            </span>
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <span
            className="spinner-border spinner-border-sm mx-3"
            role="status"
            aria-hidden="true"
          />
        )}
      </>
    </Fragment>
  )
}

const mapStateToProps = (state) => ({
  clientReports: state.clientReportStore && state.clientReportStore.clientReports,
  clientLoading: state.clientReportStore && state.clientReportStore.loading,
  postclientReports: state.postclientReportStore && state.postclientReportStore.postclientReports,
  postclientLoading: state.postclientReportStore && state.postclientReportStore.loading
})
const mapDispatchToProps = (dispatch) => ({
  getClientWeblistDispatch: (params) => dispatch(ClientWebActions.getClientWeblist(params)),
  PostClientWebDispatch: (id, params) => dispatch(PostClientWebActions.PostClientWeb(id, params)),
  postclearClientWeblistDispatch: () => dispatch(PostClientWebActions.postclearClientWeblist()),
})

export default connect(mapStateToProps, mapDispatchToProps)(WRMExportConfig)
