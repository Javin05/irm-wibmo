import  { useState } from "react";
import _ from "lodash";
import ShowFields from "../ShowFields";
import { useLocation } from "react-router-dom";
import WRMExportSetting from "./exportSetting/WRMExportSetting";
import PlaystoreExportSetting from "./exportSetting/PlaystoreExportSetting";

function ClientOnboarding() {
  const [tab, setTab] = useState("WRM");
  const pathName = useLocation().pathname;
  const urlName = pathName && pathName.split("update/");
  const clientId = urlName && urlName[1];
  const showTabDetails = (type) => {
    switch (type) {
      case "WRM":
        return <WRMExportSetting clientId={clientId} />;
      case "PLAYSTORE":
        return <PlaystoreExportSetting clientId={clientId} />;
      default:
        return <WRMExportSetting clientId={clientId} />;
    }
  };

  const handleTabs = (name) => {
    setTab(name);
  };
  return (
    <>
         <div className="row">
            <div className="col-md-12">
              <div className="card card-nav-tabs mt-10">
                <div className="card-header card text-white bg-dark">
                  <div className="nav-tabs-navigation">
                    <div className="nav-tabs-wrapper">
                      <ul className="nav nav-tabs" data-tabs="tabs">
                        <li className="client-nav-item">
                          <a
                            className="nav-link active"
                            href="#companydetails"
                            data-toggle="tab"
                            onClick={() => {
                              handleTabs("WRM");
                            }}
                          >
                            <i className="bi bi-building m-2 text-white" />
                            Web Risk Analysis
                          </a>
                        </li>
                        <li className="client-nav-item">
                          <a
                            className="nav-link"
                            href="#userinfo"
                            data-toggle="tab"
                            onClick={() => {
                              handleTabs("PLAYSTORE");
                            }}
                          >
                            <i className="bi bi-person-circle m-2 text-white" />
                            Playstore Risk Analysis
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body mt-4">
                <ShowFields>{showTabDetails(tab)}</ShowFields>
              </div>
            </div>
          </div>
    </>
  );
}

export default ClientOnboarding;
