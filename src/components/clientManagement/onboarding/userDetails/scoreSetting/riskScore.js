import React, { useEffect, useState } from 'react'
import { RiskScoreValidation } from './validation'
import { useLocation, Link } from 'react-router-dom'
import { KTSVG } from '../../../../../theme/helpers'
import { CRM_FORM, REGEX, STATUS_RESPONSE, RISKSTATUS, SWEET_ALERT_MSG } from '../../../../../utils/constants'
import {
  RiskScoreWeightAgeActions,
  RiskScoreWeightAgePostActions,
  RiskScoreWeightAgeEditActions,
  RiskScoreWeightAgeUpdateActions,
  RiskScoreWeightAgeDeleteActions,
  GroupIdDrpdnActions,
  ReportDrpdnActions
} from '../../../../../store/actions'
import { connect } from 'react-redux'
import _, { add } from 'lodash'
import { confirmationAlert, warningAlert, successAlert } from '../../../../../utils/alerts'
import ReactPaginate from 'react-paginate'
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setRiskScore } from './edit'
import ReactSelect from "../../../../../theme/layout/components/ReactSelect"
import color from "../../../../../utils/colors"

const RiskScoreWeightAge = (props) => {
  const {
    loading,
    RiskScoreWeightAge,
    getRiskScoreWeightAgeDispatch,
    clientId,
    postRiskScoreWeightAgeDispatch,
    RiskScoreWeightAgePost,
    RiskScoreWeightAgePostLoading,
    clearRiskScoreWeightAgePostDispatch,
    EditRiskScoreWeightAgeDispatch,
    RiskScoreWeightAgeEdit,
    clearRiskScoreWeightAgeEditDispatch,
    UpdateRiskScoreWeightAgeDispatch,
    RiskScoreWeightAgeUpdate,
    clearRiskScoreWeightAgeUpdateDispatch,
    DeleteRiskScoreWeightAgeDispatch,
    RiskScoreWeightAgeDelete,
    clearRiskScoreWeightAgeDeleteDispatch,
    getGroupIdDrpdnDispatch,
    getReportDrpdnDispatch,
    ReportDrpdn,
    GroupIdDrpdn
  } = props

  const [isFormUpdated, setFormUpdated] = useState(false)
  const [formData, setFormData] = useState({
    groupId: '',
    weightage: '',
    highRisk: '',
    highRiskScore: '',
    report: ''
  })
  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [show, setShow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [limit] = useState(25)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const id = url && url[3]
  const [grouIdOption, setGroupIdOption] = useState()
  const [selectedGroupIdOption, setSelectedGroupIdOption] = useState('')
  const [reportOption, setReportOption] = useState()
  const [selectedReportOption, setSelectedReportOption] = useState('')

  useEffect(() => {
    const params = {
      clientId: id,
      limit: limit,
      page: activePageNumber
    }
    getRiskScoreWeightAgeDispatch(params)
    const data = {
      clientId: id,
      skipPagination: 'true'
    }
    getGroupIdDrpdnDispatch(data)
    getReportDrpdnDispatch(data)
  }, [])

  const handleChange = (e) => {
    e.persist()
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = RiskScoreValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (!editMode) {
        const params = {
          clientId: id,
          groupId: formData.groupId,
          weightage: formData.weightage,
          highRisk: formData.highRisk,
          highRiskScore: formData.highRiskScore,
          riskName: formData.report
        }
        postRiskScoreWeightAgeDispatch(params)
      } else {
        const listId = RiskScoreWeightAgeEdit && RiskScoreWeightAgeEdit.data && RiskScoreWeightAgeEdit.data._id
        const params = {
          clientId: id,
          groupId: formData.groupId,
          weightage: formData.weightage,
          highRisk: formData.highRisk,
          highRiskScore: formData.highRiskScore,
          riskName: formData.report
        }
        UpdateRiskScoreWeightAgeDispatch(listId, params)
      }
    }
  }

  const handleSorting = (name) => { }
  const handleRecordPerPage = (e) => { }

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false)
        handleSubmit()
      }
    }
  }, [isFormUpdated])

  useEffect(() => {
    const groupName = getDefaultOptions(GroupIdDrpdn && GroupIdDrpdn.result)
    setGroupIdOption(groupName)
    if (!_.isEmpty(formData.groupName)) {
      const selOption = _.filter(groupName, function (x) { if (_.includes(formData.groupName._id, x.value)) { return x } })
      setSelectedGroupIdOption(selOption)
    }
  }, [GroupIdDrpdn])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].groupName, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const report = getDefaultOption(ReportDrpdn)
    setReportOption(report)
    if (!_.isEmpty(formData.report)) {
      const selOption = _.filter(report, function (x) { if (_.includes(formData.report._id, x.value)) { return x } })
      setSelectedReportOption(selOption)
    }
  }, [ReportDrpdn])

  const getDefaultOption = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].report_value, value: rawData[item]._id })
    }
    return defaultOptions
  }

  console.log('RiskScoreWeightAgeEdit', RiskScoreWeightAgeEdit)

  useEffect(() => {
    if (RiskScoreWeightAgeEdit && RiskScoreWeightAgeEdit.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setEditMode(true)
      setShow(true)
      const data = setRiskScore(RiskScoreWeightAgeEdit && RiskScoreWeightAgeEdit.data)
      const editData = RiskScoreWeightAgeEdit && RiskScoreWeightAgeEdit.data
      if (editData && editData._id) {
        const groupName = getDefaultOptions(GroupIdDrpdn && GroupIdDrpdn.result)
        if (!_.isEmpty(editData.groupId)) {
          const selOption = _.filter(groupName, function (x) {
            console.log('match', _.includes(editData.groupId, x.value))
            if (_.includes(editData.groupId, x.value)) {
              return x
            }
          })
          setSelectedGroupIdOption(selOption)
        }
      }
      const report = getDefaultOption(ReportDrpdn)
      if (!_.isEmpty(editData.riskName)) {
        const selOption = _.filter(report, function (x) {
          if (_.includes(editData.riskName, x.label)) {
            return x
          }
        })
        setSelectedReportOption(selOption)
      }
      setFormData(data)
    } else if (RiskScoreWeightAgeEdit && RiskScoreWeightAgeEdit.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskScoreWeightAgeEdit && RiskScoreWeightAgeEdit.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskScoreWeightAgeEditDispatch()
    }
  }, [RiskScoreWeightAgeEdit])

  const onConfirm = () => {
    setShow(false)
    const params = {
      clientId: id
    }
    getRiskScoreWeightAgeDispatch(params)
  }

  useEffect(() => {
    if (RiskScoreWeightAgePost && RiskScoreWeightAgePost.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskScoreWeightAgePost && RiskScoreWeightAgePost.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearRiskScoreWeightAgePostDispatch()
    } else if (RiskScoreWeightAgePost && RiskScoreWeightAgePost.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskScoreWeightAgePost && RiskScoreWeightAgePost.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskScoreWeightAgePostDispatch()
    }
  }, [RiskScoreWeightAgePost])

  useEffect(() => {
    if (RiskScoreWeightAgeUpdate && RiskScoreWeightAgeUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskScoreWeightAgeUpdate && RiskScoreWeightAgeUpdate.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearRiskScoreWeightAgeUpdateDispatch()
      clearRiskScoreWeightAgeEditDispatch()
    } else if (RiskScoreWeightAgeUpdate && RiskScoreWeightAgeUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskScoreWeightAgeUpdate && RiskScoreWeightAgeUpdate.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskScoreWeightAgeUpdateDispatch()
      clearRiskScoreWeightAgeEditDispatch()
    }
  }, [RiskScoreWeightAgeUpdate])

  useEffect(() => {
    if (RiskScoreWeightAgeDelete && RiskScoreWeightAgeDelete.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskScoreWeightAgeDelete && RiskScoreWeightAgeDelete.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      clearRiskScoreWeightAgeDeleteDispatch()
    } else if (RiskScoreWeightAgeDelete && RiskScoreWeightAgeDelete.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskScoreWeightAgeDelete && RiskScoreWeightAgeDelete.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskScoreWeightAgeDeleteDispatch()
    }
  }, [RiskScoreWeightAgeDelete])

  const handleEdit = (id) => {
    EditRiskScoreWeightAgeDispatch(id)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_LIST,
      'warning',
      'Yes',
      'No',
      () => { DeleteRiskScoreWeightAgeDispatch(id) },
      () => { { } }
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: id
    }
    setActivePageNumber(pageNumber)
    getRiskScoreWeightAgeDispatch(params)
  }

  const clearPopup = () => {
    setShow(false)
    setFormData(values => ({
      groupId: '',
      weightage: '',
      highRisk: '',
      highRiskScore: '',
      report: ''
    }))
    setSelectedReportOption('')
    setSelectedGroupIdOption('')
    clearRiskScoreWeightAgeEditDispatch()
  }

  const handleChangeGropuId = selectedOption => {
    if (selectedOption !== null) {
      setSelectedGroupIdOption(selectedOption)
      setFormData(values => ({ ...values, groupId: selectedOption.value }))
    } else {
      setFormData(values => ({ ...values, groupId: '' }))
    }
    setErrors({ ...errors, groupId: '' })
  }

  const handleChangeReport = selectedOption => {
    if (selectedOption !== null) {
      setSelectedReportOption(selectedOption)
      setFormData(values => ({ ...values, report: selectedOption.label }))
    } else {
      setFormData(values => ({ ...values, report: '' }))
    }
    setErrors({ ...errors, report: '' })
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }


  const totalPages =
    RiskScoreWeightAge && RiskScoreWeightAge.count
      ? Math.ceil(parseInt(RiskScoreWeightAge && RiskScoreWeightAge.count) / limit)
      : 1

  return (
    <>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Group Id :
                </label>
              </div>
              <div className='col-md-8'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='groupId'
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeGropuId}
                  options={grouIdOption}
                  value={selectedGroupIdOption}
                  isDisabled={!grouIdOption}
                />
                {errors && errors.groupId && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.groupId}
                  </div>
                )}
              </div>
            </div>

            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report :
                </label>
              </div>
              <div className='col-md-8'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='report'
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeReport}
                  options={reportOption}
                  value={selectedReportOption}
                  isDisabled={!grouIdOption}
                />
                {errors && errors.report && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  WeightAge :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='WeightAge'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.weightage && errors.weightage },
                    {
                      'is-valid': formData.weightage && !errors.weightage
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='weightage'
                  autoComplete='off'
                  value={formData.weightage || ''}
                  onKeyPress={(e) => {
                    if (
                      !REGEX.NUMERIC.test(e.key)
                    ) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.weightage && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.weightage}
                  </div>
                )}
              </div>
            </div>
            <div className='row mt-4'>
              <div className='col-lg-4 mb-3'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  High Risk :
                </label>
              </div>
              <div className='col-lg-8'>
                <div className="form-check form-check-custom form-check-solid mb-4">
                  <label className='d-flex flex-stack mb-5 cursor-pointer'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='Yes'
                        name='highRisk'
                        checked={formData.highRisk === 'Yes'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        Yes
                      </span>
                    </span>
                  </label>
                  <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='No'
                        name='highRisk'
                        checked={formData.highRisk === 'No'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        No
                      </span>
                    </span>
                  </label>
                </div>
                {errors && errors.highRisk && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.highRisk}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  High Risk Score :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='High Risk Score'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.highRiskScore && errors.highRiskScore },
                    {
                      'is-valid': formData.highRiskScore && !errors.highRiskScore
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='highRiskScore'
                  autoComplete='off'
                  value={formData.highRiskScore || ''}
                  onKeyPress={(e) => {
                    if (
                      !REGEX.NUMERIC.test(e.key)
                    ) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.highRiskScore && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.highRiskScore}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4' />
              <div className='col-md-8'>
                <button
                  type='button'
                  className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                  onClick={(e) => handleSubmit(e)}
                  disabled={RiskScoreWeightAgePostLoading}
                >
                  {!RiskScoreWeightAgePostLoading && <span className='indicator-label'>Submit</span>}
                  {RiskScoreWeightAgePostLoading && (
                    <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                      Please wait...
                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className='card h-auto'>
        <div className='card-body py-3'>
          <div className='row'>
            <div className='col-md-6'>
              <h3 className='fs-2 m-4 d-flex justify-content-start'>Risk Score Weightage</h3>
            </div>
            <div className='col-md-6'>
              <div className='d-flex justify-content-end m-4'>
                <button
                  className='btn btn-light-primary btn-sm'
                  onClick={() => setShow(true)}
                >
                  Add
                </button>

              </div>
            </div>
          </div>
          <div className='table-responsive'>
            <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className='d-flex'>
                      <span>Risk Name</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('riskName')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Group Name</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('groupName')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>HighRisk</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('highRisk')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>High Risk Score</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('highRiskScore')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>WeightAge</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('weightage')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>status</span>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Action</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      _.isArray(RiskScoreWeightAge && RiskScoreWeightAge.result)
                        ? (
                          RiskScoreWeightAge && RiskScoreWeightAge.result.map((riskItem, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: 'black' }
                                    : { borderColor: 'white' }
                                }
                              >
                                <td>
                                  {riskItem.riskName ? riskItem.riskName : '--'}
                                </td>
                                <td>
                                  {riskItem && riskItem.groupId && riskItem.groupId.groupName ? riskItem.groupId.groupName : '--'}
                                </td>
                                <td>
                                  {riskItem.highRisk ? riskItem.highRisk : '--'}
                                </td>
                                <td>
                                  {riskItem && riskItem.highRiskScore ? riskItem.highRiskScore : '--'}
                                </td>
                                <td>
                                  {riskItem && riskItem.weightage.toString() ? riskItem.weightage.toString() : '--'}
                                </td>
                                <td>
                                  <span className={`badge ${RISKSTATUS[riskItem.status && riskItem.status]}`}>
                                    {riskItem.status ? riskItem.status : "--"}
                                  </span>
                                </td>
                                <td>
                                  <td className="pb-0 pt-5 text-start">
                                    <button
                                      className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4'
                                      onClick={() => handleEdit(riskItem._id)}
                                      title="Edit customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                      onClick={() => onDeleteItem(riskItem._id)}
                                      title="Delete customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { RiskScoreWeightAgePostStore, RiskScoreWeightAgeEditStore, RiskScoreWeightAgeUpdateStore, RiskScoreWeightAgeDeleteStore, GroupIdDrpdngetStore, ReportDrpdngetStore } = state
  return {
    RiskScoreWeightAge:
      state && state.RiskScoreWeightAgegetStore && state.RiskScoreWeightAgegetStore.RiskScoreWeightAge
        ? state.RiskScoreWeightAgegetStore.RiskScoreWeightAge
        : [],
    loading: state && state.RiskScoreWeightAgegetStore && state.RiskScoreWeightAgegetStore.loading,

    RiskScoreWeightAgePost: RiskScoreWeightAgePostStore && RiskScoreWeightAgePostStore.RiskScoreWeightAgePost ? RiskScoreWeightAgePostStore.RiskScoreWeightAgePost : '',
    RiskScoreWeightAgePostLoading: RiskScoreWeightAgePostStore && RiskScoreWeightAgePostStore.loading ? RiskScoreWeightAgePostStore.loading : false,
    RiskScoreWeightAgeEdit: RiskScoreWeightAgeEditStore && RiskScoreWeightAgeEditStore.RiskScoreWeightAgeEdit ? RiskScoreWeightAgeEditStore.RiskScoreWeightAgeEdit : '',
    RiskScoreWeightAgeUpdate: RiskScoreWeightAgeUpdateStore && RiskScoreWeightAgeUpdateStore.RiskScoreWeightAgeUpdate ? RiskScoreWeightAgeUpdateStore.RiskScoreWeightAgeUpdate : '',
    RiskScoreWeightAgeDelete: RiskScoreWeightAgeDeleteStore && RiskScoreWeightAgeDeleteStore.RiskScoreWeightAgeDelete ? RiskScoreWeightAgeDeleteStore.RiskScoreWeightAgeDelete : '',
    GroupIdDrpdn: GroupIdDrpdngetStore && GroupIdDrpdngetStore.GroupIdDrpdn ? GroupIdDrpdngetStore.GroupIdDrpdn : '',
    ReportDrpdn: ReportDrpdngetStore && ReportDrpdngetStore.ReportDrpdn ? ReportDrpdngetStore.ReportDrpdn : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getRiskScoreWeightAgeDispatch: (params) => dispatch(RiskScoreWeightAgeActions.getRiskScoreWeightAge(params)),
  postRiskScoreWeightAgeDispatch: (params) => dispatch(RiskScoreWeightAgePostActions.postRiskScoreWeightAge(params)),
  clearRiskScoreWeightAgePostDispatch: () => dispatch(RiskScoreWeightAgePostActions.clearRiskScoreWeightAgePost()),
  EditRiskScoreWeightAgeDispatch: (params) => dispatch(RiskScoreWeightAgeEditActions.EditRiskScoreWeightAge(params)),
  clearRiskScoreWeightAgeEditDispatch: () => dispatch(RiskScoreWeightAgeEditActions.clearRiskScoreWeightAgeEdit()),
  UpdateRiskScoreWeightAgeDispatch: (id, params) => dispatch(RiskScoreWeightAgeUpdateActions.UpdateRiskScoreWeightAge(id, params)),
  clearRiskScoreWeightAgeUpdateDispatch: (id, params) => dispatch(RiskScoreWeightAgeUpdateActions.clearRiskScoreWeightAgeUpdate(id, params)),
  DeleteRiskScoreWeightAgeDispatch: (params) => dispatch(RiskScoreWeightAgeDeleteActions.DeleteRiskScoreWeightAge(params)),
  clearRiskScoreWeightAgeDeleteDispatch: (params) => dispatch(RiskScoreWeightAgeDeleteActions.clearRiskScoreWeightAgeDelete(params)),
  getGroupIdDrpdnDispatch: (params) => dispatch(GroupIdDrpdnActions.getGroupIdDrpdn(params)),
  getReportDrpdnDispatch: (params) => dispatch(ReportDrpdnActions.getReportDrpdn(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(RiskScoreWeightAge)
