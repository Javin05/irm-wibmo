import React, { useState } from 'react'
import _ from 'lodash'
import { useLocation, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import StatusWeightage from './statusWeightage'
import RiskyDomain from './riskyDomain'
import GroupScoreWeightAge from './groupScore'
import ShowFields from '../../ShowFields'

const ScoreSetting = (props) => {
  const {
    loading,
    getPackages,
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update')
  const id = url && url[1]
  const [tab, setTab] = useState('STATUSWEIGHTAGE')

  const showTabDetails = (type) => {
    switch (type) {
      case 'STATUSWEIGHTAGE':
        return <StatusWeightage />
      case 'RISKDOMAIN':
        return <RiskyDomain />
      case 'SCOREWEIGHTAGE':
        return <GroupScoreWeightAge />
    }
  }

  const handleTabs = (name) => {
    setTab(name)
  }

  return (    
    <>
            <div className='row'>
              <div className='col-md-12'>
                <div className='card card-nav-tabs mt-10'>
                  <div className='card-header card-header-secondary'>
                    <div className='nav-tabs-navigation'>
                      <div className='nav-tabs-wrapper'>
                        <ul className='nav nav-tabs' data-tabs='tabs'>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link active' href='#companydetails' data-toggle='tab'
                              onClick={() => { handleTabs('STATUSWEIGHTAGE') }}
                            >
                              Status Weightage
                            </a>
                          </li>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#userinfo' data-toggle='tab'
                              onClick={() => { handleTabs('RISKDOMAIN') }}
                            >
                              Risky Domain
                            </a>
                          </li>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#SCORE' data-toggle='tab'
                              onClick={() => { handleTabs('SCOREWEIGHTAGE') }}
                            >
                              Score Weightage
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='card-body mt-4'>
                  <ShowFields>
                    {showTabDetails(tab)}
                  </ShowFields>
                </div>
              </div>
            </div>
          </>
  )
}

const mapStateToProps = (state) => {
  const { } = state
  return {}
}

const mapDispatchToProps = (dispatch) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(ScoreSetting)
