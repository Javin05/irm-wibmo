import React, { useEffect, useState } from 'react'
import { StatusWeightageValidation } from './validation'
import { useLocation, Link } from 'react-router-dom'
import { KTSVG } from '../../../../../theme/helpers'
import { CRM_FORM, REGEX, STATUS_RESPONSE, RISKSTATUS, SWEET_ALERT_MSG } from '../../../../../utils/constants'
import {
  RiskweightageActions,
  RiskweightagePostActions,
  RiskweightageEditActions,
  RiskweightageUpdateActions,
  RiskweightageDeleteActions,
  CloneScoreActions
} from '../../../../../store/actions'
import { connect } from 'react-redux'
import _, { add } from 'lodash'
import { confirmationAlert, warningAlert, successAlert } from '../../../../../utils/alerts'
import ReactPaginate from 'react-paginate'
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setStatusWeightage } from './edit'

const StatusWeightage = (props) => {
  const {
    loading,
    getRiskweightage,
    getRiskweightageDispatch,
    RiskweightagePostDispatch,
    RiskweightagePost,
    RiskweightagePostloading,
    clearRiskweightagePostDispatch,
    RiskweightageEditDispatch,
    RiskweightageEdit,
    clearRiskweightageEditDispatch,
    getRiskweightageUpdateDispatch,
    RiskweightageUpdate,
    clearRiskweightageUpdateDispatch,
    RiskweightageDeleteDispatch,
    RiskweightageDelete,
    clearRiskweightageDeleteDispatch,
    CloneScoreDispatch,
    cloneScoreSetting,
    clearCloneScoreDispatch
  } = props
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [userId, setUserId] = useState()
  const [formData, setFormData] = useState({
    riskLevel: '',
    weightage: '',
    decision: ''
  })
  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [show, setShow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [limit] = useState(25)
  const [addForm, setAddForm] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const id = url && url[3]

  useEffect(() => {
    const params = {
      clientId: id,
      limit: limit,
      page: activePageNumber
    }
    getRiskweightageDispatch(params)
  }, [])

  const handleChange = (e) => {
    e.persist()
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = StatusWeightageValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (!editMode) {
        const params = {
          clientId: id,
          riskLevel: formData.riskLevel,
          weightage: formData.weightage,
          decision: formData.decision
        }
        RiskweightagePostDispatch(params)
      } else {
        const listId = RiskweightageEdit && RiskweightageEdit.data && RiskweightageEdit.data._id
        const params = {
          clientId: id,
          riskLevel: formData.riskLevel,
          weightage: formData.weightage,
          decision: formData.decision
        }
        getRiskweightageUpdateDispatch(listId, params)
      }
    }
  }

  const handleSorting = (name) => { }
  const handleRecordPerPage = (e) => { }

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false)
        handleSubmit()
      }
    }
  }, [isFormUpdated])

  useEffect(() => {
    if (RiskweightageEdit && RiskweightageEdit.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = setStatusWeightage(RiskweightageEdit && RiskweightageEdit.data)
      setFormData(data)
      setEditMode(true)
      setShow(true)
    } else if (RiskweightageEdit && RiskweightageEdit.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskweightageEdit && RiskweightageEdit.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskweightageEditDispatch()
    }
  }, [RiskweightageEdit])

  const onConfirm = () => {
    setShow(false)
    const params = {
      clientId: id
    }
    getRiskweightageDispatch(params)
  }

  useEffect(() => {
    if (RiskweightagePost && RiskweightagePost.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskweightagePost && RiskweightagePost.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearRiskweightagePostDispatch()
    } else if (RiskweightagePost && RiskweightagePost.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskweightagePost && RiskweightagePost.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskweightagePostDispatch()
    }
  }, [RiskweightagePost])


  useEffect(() => {
    if (RiskweightageUpdate && RiskweightageUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskweightageUpdate && RiskweightageUpdate.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearRiskweightageUpdateDispatch()
      clearRiskweightageEditDispatch()
    } else if (RiskweightageUpdate && RiskweightageUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskweightageUpdate && RiskweightageUpdate.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskweightageUpdateDispatch()
    }
  }, [RiskweightageUpdate])

  useEffect(() => {
    if (RiskweightageDelete && RiskweightageDelete.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskweightageDelete && RiskweightageDelete.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      clearRiskweightageDeleteDispatch()
    } else if (RiskweightageDelete && RiskweightageDelete.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskweightageDelete && RiskweightageDelete.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskweightageDeleteDispatch()
    }
  }, [RiskweightageDelete])

  const handleEdit = (id) => {
    RiskweightageEditDispatch(id)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_LIST,
      'warning',
      'Yes',
      'No',
      () => { RiskweightageDeleteDispatch(id) },
      () => { { } }
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: id
    }
    setActivePageNumber(pageNumber)
    getRiskweightageDispatch(params)
  }

  const clearPopup = () => {
    setShow(false)
    setFormData(values => ({
      riskLevel: '',
      weightage: ''
    }))
    clearRiskweightageEditDispatch()
  }

  const cloneScore = () => {
    const params = {
      clientId: id
    }
    CloneScoreDispatch(params)
  }
  useEffect(() => {
    if (cloneScoreSetting && cloneScoreSetting.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const params = {
        clientId: id,
        limit: limit,
        page: activePageNumber
      }
      getRiskweightageDispatch(params)
      clearCloneScoreDispatch()
    } else if (cloneScoreSetting && cloneScoreSetting.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        cloneScoreSetting && cloneScoreSetting.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearCloneScoreDispatch()
    }
  }, [cloneScoreSetting])

  const totalPages =
    getRiskweightage && getRiskweightage.count
      ? Math.ceil(parseInt(getRiskweightage && getRiskweightage.count) / limit)
      : 1


  return (
    <>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Risk Level :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='RiskLevel'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.riskLevel && errors.riskLevel },
                    {
                      'is-valid': formData.riskLevel && !errors.riskLevel
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='riskLevel'
                  autoComplete='off'
                  value={formData.riskLevel || ''}
                />
                {errors && errors.riskLevel && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.riskLevel}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  WeightAge :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='WeightAge'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.weightage && errors.weightage },
                    {
                      'is-valid': formData.weightage && !errors.weightage
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='weightage'
                  autoComplete='off'
                  value={formData.weightage || ''}
                  onKeyPress={(e) => {
                    if (
                      !REGEX.NUMERIC.test(e.key)
                    ) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.weightage && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.weightage}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Decision :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Decision'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.decision && errors.decision },
                    {
                      'is-valid': formData.decision && !errors.decision
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='decision'
                  autoComplete='off'
                  value={formData.decision || ''}
                />
                {errors && errors.decision && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.decision}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4' />
              <div className='col-md-8'>
                <button
                  type='button'
                  className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                  onClick={(e) => handleSubmit(e)}
                  disabled={RiskweightagePostloading}
                >
                  {!RiskweightagePostloading && <span className='indicator-label'>Submit</span>}
                  {RiskweightagePostloading && (
                    <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                      Please wait...
                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className='card h-auto'>
        <div className='card-body py-3'>
          <div className='row'>
            <div className='col-md-6'>
              <h3 className='fs-2 m-4 d-flex justify-content-start'>Status Weightage</h3>
            </div>
            <div className='col-md-6'>
              <div className='d-flex justify-content-end m-4'>
                {getRiskweightage && getRiskweightage.message === 'No Data Found' ?
                  <button
                    className='btn btn-light-primary btn-sm'
                    onClick={() => { cloneScore() }}
                  >
                    Clone Score Setting
                  </button> : null}
                <button
                  className='btn btn-light-primary btn-sm ms-2'
                  onClick={() => setShow(true)}
                >
                  Add
                </button>
              </div>
            </div>
          </div>
          <div className='table-responsive'>
            <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className='d-flex'>
                      <span>riskLevel</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('riskLevel')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>weightage</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('weightage')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>status</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('status')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Action</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      _.isArray(getRiskweightage && getRiskweightage.data && getRiskweightage.data.result)
                        ? (
                          getRiskweightage && getRiskweightage.data && getRiskweightage.data.result.map((riskItem, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: 'black' }
                                    : { borderColor: 'white' }
                                }
                              >
                                <td>
                                  {riskItem.riskLevel ? riskItem.riskLevel : '--'}
                                </td>
                                <td>
                                  {riskItem.weightage.toString() ? riskItem.weightage.toString() : '--'}
                                </td>
                                <td>
                                  <span className={`badge ${RISKSTATUS[riskItem.status && riskItem.status]}`}>
                                    {riskItem.status ? riskItem.status : "--"}
                                  </span>
                                </td>
                                <td>
                                  <td className="pb-0 pt-5 text-start">
                                    <button
                                      className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4'
                                      onClick={() => handleEdit(riskItem._id)}
                                      title="Edit customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                      onClick={() => onDeleteItem(riskItem._id)}
                                      title="Delete customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { RiskweightagePostStore, RiskweightageEditStore, RiskweightageUpdateStore, RiskweightageDeleteStore, CloneScoreTypesStore } = state
  return {
    getRiskweightage:
      state && state.RiskweightageStore && state.RiskweightageStore.getRiskweightage
        ? state.RiskweightageStore.getRiskweightage
        : [],
    loading: state && state.RiskweightageStore && state.RiskweightageStore.loading,
    RiskweightagePost: RiskweightagePostStore && RiskweightagePostStore.RiskweightagePost ? RiskweightagePostStore.RiskweightagePost : [],
    RiskweightagePostloading: RiskweightagePostStore && RiskweightagePostStore.loading ? RiskweightagePostStore.loading : false,
    RiskweightageEdit: RiskweightageEditStore && RiskweightageEditStore.RiskweightageEdit ? RiskweightageEditStore.RiskweightageEdit : '',
    RiskweightageUpdate: RiskweightageUpdateStore && RiskweightageUpdateStore.RiskweightageUpdate ? RiskweightageUpdateStore.RiskweightageUpdate : '',
    RiskweightageDelete: RiskweightageDeleteStore && RiskweightageDeleteStore.RiskweightageDelete ? RiskweightageDeleteStore.RiskweightageDelete : '',
    cloneScoreSetting: CloneScoreTypesStore && CloneScoreTypesStore.cloneScoreSetting ? CloneScoreTypesStore.cloneScoreSetting : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getRiskweightageDispatch: (params) => dispatch(RiskweightageActions.getRiskweightage(params)),
  RiskweightagePostDispatch: (params) => dispatch(RiskweightagePostActions.getRiskweightagePost(params)),
  clearRiskweightagePostDispatch: () => dispatch(RiskweightagePostActions.clearRiskweightagePost()),
  RiskweightageEditDispatch: (params) => dispatch(RiskweightageEditActions.getRiskweightageEdit(params)),
  clearRiskweightageEditDispatch: () => dispatch(RiskweightageEditActions.clearRiskweightageEdit()),
  getRiskweightageUpdateDispatch: (id, params) => dispatch(RiskweightageUpdateActions.getRiskweightageUpdate(id, params)),
  clearRiskweightageUpdateDispatch: (id, params) => dispatch(RiskweightageUpdateActions.clearRiskweightageUpdate(id, params)),
  RiskweightageDeleteDispatch: (params) => dispatch(RiskweightageDeleteActions.getRiskweightageDelete(params)),
  clearRiskweightageDeleteDispatch: (params) => dispatch(RiskweightageDeleteActions.clearRiskweightageDelete(params)),
  CloneScoreDispatch: (params) => dispatch(CloneScoreActions.getCloneScoreRequest(params)),
  clearCloneScoreDispatch: (params) => dispatch(CloneScoreActions.clearCloneScore(params))


})

export default connect(mapStateToProps, mapDispatchToProps)(StatusWeightage)
