import React, { useEffect, useState } from 'react'
import { RiskyDomainValidation } from './validation'
import { useLocation, Link } from 'react-router-dom'
import { KTSVG } from '../../../../../theme/helpers'
import { CRM_FORM, REGEX, STATUS_RESPONSE, RISKSTATUS, SWEET_ALERT_MSG } from '../../../../../utils/constants'
import {
  RiskyDomainActions,
  RiskyDomainPostActions,
  RiskyDomainEditActions,
  RiskyDomainUpdateActions,
  RiskyDomainDeleteActions
} from '../../../../../store/actions'
import { connect } from 'react-redux'
import _, { add } from 'lodash'
import { confirmationAlert, warningAlert, successAlert } from '../../../../../utils/alerts'
import ReactPaginate from 'react-paginate'
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setRiskyDomain } from './edit'

const RiskyDomain = (props) => {
  const {
    loading,
    RiskyDomain,
    getRiskyDomainDispatch,
    clientId,
    RiskyDomainPostDispatch,
    RiskyDomainPost,
    RiskyDomainPostloading,
    clearRiskyDomainPostDispatch,
    RiskyDomainEditDispatch,
    RiskyDomainEdit,
    clearRiskyEditDomainDispatch,
    UpdateRiskyDomainDispatch,
    RiskyDomainUpdate,
    clearRiskyUpdateDomainDispatch,
    DeleteRiskyDomainDispatch,
    RiskyDomainDelete,
    clearRiskyDeleteDomainDispatch
  } = props
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [formData, setFormData] = useState({
    domainRegisterCompany: '',
    risk: ''
  })
  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [show, setShow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [limit] = useState(25)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const id = url && url[3]

  useEffect(() => {
    const params = {
      clientId: id,
      limit: limit,
      page: activePageNumber
    }
    getRiskyDomainDispatch(params)
  }, [])

  const handleChange = (e) => {
    e.persist()
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = RiskyDomainValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (!editMode) {
        const params = {
          clientId: id,
          domainRegisterCompany: formData.domainRegisterCompany,
          risk: formData.risk
        }
        RiskyDomainPostDispatch(params)
      } else {
        const listId = RiskyDomainEdit && RiskyDomainEdit.data && RiskyDomainEdit.data._id
        const params = {
          clientId: id,
          domainRegisterCompany: formData.domainRegisterCompany,
          risk: formData.risk
        }
        UpdateRiskyDomainDispatch(listId, params)
      }
    }
  }

  const handleSorting = (name) => { }

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false)
        handleSubmit()
      }
    }
  }, [isFormUpdated])

  useEffect(() => {
    if (RiskyDomainEdit && RiskyDomainEdit.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = setRiskyDomain(RiskyDomainEdit && RiskyDomainEdit.data)
      setFormData(data)
      setEditMode(true)
      setShow(true)
    } else if (RiskyDomainEdit && RiskyDomainEdit.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskyDomainEdit && RiskyDomainEdit.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskyEditDomainDispatch()
    }
  }, [RiskyDomainEdit])

  const onConfirm = () => {
    setShow(false)
    const params = {
      clientId: id
    }
    getRiskyDomainDispatch(params)
  }

  useEffect(() => {
    if (RiskyDomainPost && RiskyDomainPost.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskyDomainPost && RiskyDomainPost.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearRiskyDomainPostDispatch()
    } else if (RiskyDomainPost && RiskyDomainPost.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskyDomainPost && RiskyDomainPost.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskyDomainPostDispatch()
    }
  }, [RiskyDomainPost])

  useEffect(() => {
    if (RiskyDomainUpdate && RiskyDomainUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskyDomainUpdate && RiskyDomainUpdate.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearRiskyUpdateDomainDispatch()
      clearRiskyEditDomainDispatch()
    } else if (RiskyDomainUpdate && RiskyDomainUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskyDomainUpdate && RiskyDomainUpdate.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskyUpdateDomainDispatch()
    }
  }, [RiskyDomainUpdate])

  useEffect(() => {
    if (RiskyDomainDelete && RiskyDomainDelete.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        RiskyDomainDelete && RiskyDomainDelete.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      clearRiskyDeleteDomainDispatch()
    } else if (RiskyDomainDelete && RiskyDomainDelete.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        RiskyDomainDelete && RiskyDomainDelete.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearRiskyDeleteDomainDispatch()
    }
  }, [RiskyDomainDelete])

  const handleEdit = (id) => {
    RiskyDomainEditDispatch(id)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_LIST,
      'warning',
      'Yes',
      'No',
      () => { DeleteRiskyDomainDispatch(id) },
      () => {{}}
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: id
    }
    setActivePageNumber(pageNumber)
    getRiskyDomainDispatch(params)
  }

  const clearPopup = () => {
    setShow(false)
    setFormData(values => ({
      domainRegisterCompany: '',
      risk: ''
    }))
    clearRiskyEditDomainDispatch()
  }

  const totalPages =
    RiskyDomain && RiskyDomain.count
      ? Math.ceil(parseInt(RiskyDomain && RiskyDomain.count) / limit)
      : 1

  return (
    <>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Domain Register Company :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Domain Register Company'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.domainRegisterCompany && errors.domainRegisterCompany },
                    {
                      'is-valid': formData.domainRegisterCompany && !errors.domainRegisterCompany
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='domainRegisterCompany'
                  autoComplete='off'
                  value={formData.domainRegisterCompany || ''}
                />
                {errors && errors.domainRegisterCompany && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.domainRegisterCompany}
                  </div>
                )}
              </div>
            </div>
            <div className='row mt-4'>
              <div className='col-lg-4 mb-3'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Risk :
                </label>
              </div>
              <div className='col-lg-8'>
                <div className="form-check form-check-custom form-check-solid mb-4">
                  <label className='d-flex flex-stack mb-5 cursor-pointer'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='Yes'
                        name='risk'
                        checked={formData.risk === 'Yes'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        Yes
                      </span>
                    </span>
                  </label>
                  <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='No'
                        name='risk'
                        checked={formData.risk === 'No'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        No
                      </span>
                    </span>
                  </label>
                </div>
                {errors && errors.risk && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.risk}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4' />
              <div className='col-md-8'>
                <button
                  type='button'
                  className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                  onClick={(e) => handleSubmit(e)}
                  disabled={RiskyDomainPostloading}
                >
                  {!RiskyDomainPostloading && <span className='indicator-label'>Submit</span>}
                  {RiskyDomainPostloading && (
                    <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                      Please wait...
                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className='card h-auto'>
        <div className='card-body py-3'>
          <div className='row'>
            <div  className='col-md-6'>
            <h3 className='fs-2 m-4 d-flex justify-content-start'>Risky Domain</h3>
            </div>
            <div className='col-md-6'>
              <div className='d-flex justify-content-end m-4'>
                <button
                  className='btn btn-light-primary btn-sm'
                  onClick={() => setShow(true)}
                >
                  Add
                </button>
              </div>
            </div>
          </div>
          <div className='table-responsive'>
            <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className='d-flex'>
                      <span>Domain Register Company</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('domainRegisterCompany')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Risk</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('risk')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>status</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('status')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Action</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      _.isArray(RiskyDomain && RiskyDomain.result)
                        ? (
                          RiskyDomain && RiskyDomain.result.map((riskItem, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: 'black' }
                                    : { borderColor: 'white' }
                                }
                              >
                                <td>
                                  {riskItem.domainRegisterCompany ? riskItem.domainRegisterCompany : '--'}
                                </td>
                                <td>
                                  {riskItem.risk ? riskItem.risk : '--'}
                                </td>
                                <td>
                                  <span className={`badge ${RISKSTATUS[riskItem.status && riskItem.status]}`}>
                                    {riskItem.status ? riskItem.status : "--"}
                                  </span>
                                </td>
                                <td>
                                  <td className="pb-0 pt-5 text-start">
                                    <button
                                      className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4'
                                      onClick={() => handleEdit(riskItem._id)}
                                      title="Edit customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                      onClick={() => onDeleteItem(riskItem._id)}
                                      title="Delete customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { RiskyDomainPostStore, RiskyDomainEditStore, RiskyDomainUpdateStore, RiskyDomainDeleteStore } = state
  return {
    RiskyDomain:
      state && state.RiskyDomaingetStore && state.RiskyDomaingetStore.RiskyDomain
        ? state.RiskyDomaingetStore.RiskyDomain
        : [],
    loading: state && state.RiskyDomaingetStore && state.RiskyDomaingetStore.loading,
    RiskyDomainPost: RiskyDomainPostStore && RiskyDomainPostStore.RiskyDomainPost ? RiskyDomainPostStore.RiskyDomainPost : '',
    RiskyDomainPostloading: RiskyDomainPostStore && RiskyDomainPostStore.loading ? RiskyDomainPostStore.loading : false,
    RiskyDomainEdit: RiskyDomainEditStore && RiskyDomainEditStore.RiskyDomainEdit ? RiskyDomainEditStore.RiskyDomainEdit : '',
    RiskyDomainUpdate: RiskyDomainUpdateStore && RiskyDomainUpdateStore.RiskyDomainUpdate ? RiskyDomainUpdateStore.RiskyDomainUpdate : '',
    RiskyDomainDelete: RiskyDomainDeleteStore && RiskyDomainDeleteStore.RiskyDomainDelete ? RiskyDomainDeleteStore.RiskyDomainDelete : '',
  }
}


const mapDispatchToProps = (dispatch) => ({
  getRiskyDomainDispatch: (params) => dispatch(RiskyDomainActions.getRiskyDomain(params)),
  RiskyDomainPostDispatch: (params) => dispatch(RiskyDomainPostActions.postRiskyDomain(params)),
  clearRiskyDomainPostDispatch: () => dispatch(RiskyDomainPostActions.clearRiskyPostDomain()),
  RiskyDomainEditDispatch: (params) => dispatch(RiskyDomainEditActions.EditRiskyDomain(params)),
  clearRiskyEditDomainDispatch: () => dispatch(RiskyDomainEditActions.clearRiskyEditDomain()),
  UpdateRiskyDomainDispatch: (id, params) => dispatch(RiskyDomainUpdateActions.UpdateRiskyDomain(id, params)),
  clearRiskyUpdateDomainDispatch: (id, params) => dispatch(RiskyDomainUpdateActions.clearRiskyUpdateDomain(id, params)),
  DeleteRiskyDomainDispatch: (params) => dispatch(RiskyDomainDeleteActions.DeleteRiskyDomain(params)),
  clearRiskyDeleteDomainDispatch: (params) => dispatch(RiskyDomainDeleteActions.clearRiskyDeleteDomain(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(RiskyDomain)
