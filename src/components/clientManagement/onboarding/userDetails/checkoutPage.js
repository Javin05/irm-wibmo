import React, { useEffect } from 'react'
import { CRM_FORM, STATUS_RESPONSE, SLICE_CHARACTERS } from '../../../../utils/constants'
import { useHistory } from 'react-router-dom'
import { removeLocalStorage, addEllipsis } from '../../../../utils/helper'
import { connect } from 'react-redux'
import { clientMgmtActions } from '../../../../store/actions'
import { warningAlert, confirmAlert } from '../../../../utils/alerts'
import { TooltipOnHover } from '../../../../theme/layout/components/tooltipOnHover/TooltipOnHover'

const Checkout = (props) => {
  const {
    addClientMgmtDispatch,
    clearaddClientMgmtDispatch,
    statusACL,
    messageACL,
    loadingACL,
    clientDetails,
    goBack,
    summary
  } = props
  const history = useHistory()

  const clientData = {
    ...clientDetails,
    // package: [
    //   {
    //     _id: '6202546454ab2e5944c694a1'
    //   },
    //   {
    //     _id: '6202542b54ab2e5944c6949f'
    //   }
    // ]
  }

  const removeFormStorage = () => {
    removeLocalStorage(CRM_FORM.COMPANY_DETAILS)
    removeLocalStorage(CRM_FORM.PACKAGE_DETAILS)
    removeLocalStorage(CRM_FORM.CRM_DETAILS)
    removeLocalStorage(CRM_FORM.CRM_DETAILS_TABLE)
    removeLocalStorage(CRM_FORM.PROCESSOR_DETAILS)
    removeLocalStorage(CRM_FORM.PROCESSOR_DETAILS_TABLE)
    removeLocalStorage(CRM_FORM.GATEWAY_DETAILS)
    removeLocalStorage(CRM_FORM.GATEWAY_DETAILS_TABLE)
    removeLocalStorage(CRM_FORM.MERCHANT_DETAILS)
    removeLocalStorage(CRM_FORM.USER_DETAILS)
    removeLocalStorage(CRM_FORM.ACTIVE_STEP)
  }

  const onConfirm = () => {
    history.push('/client-management')
    removeFormStorage()
  }

  useEffect(() => {
    if (statusACL === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageACL,
        'success',
        'ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearaddClientMgmtDispatch()
    } else if (statusACL === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageACL,
        '',
        'Ok'
      )
    }
    clearaddClientMgmtDispatch()
  }, [statusACL])

  const handleSave = () => {
    addClientMgmtDispatch(clientData)
  }

  return (
    <>
      <div className='card-header bg-skyBlue'>
        <div className='card-body'>
          <div className='ms-4 mt-4 mb-4'>
          <h2 className='mb-2'>Summary</h2>
          </div>
          <div className='scroll h-400px px-5'>
            <div className='card-header bg-skyBlue'>
              <div className='card-body'>
                <div className='row g-4'>
                  <div className='col-xl-6'>
                    <div className='d-flex h-100 align-items-center'>
                      <div className='w-100 h-100 rounded-3 bg-light bg-opacity-75 py-12 px-10'>
                        <div className='mb-7'>
                          <h2 className='text-gray-700 text-center mb-5'>
                            Client Details
                          </h2>
                        </div>
                        <div className='w-80 mb-10'>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Company Name : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              <TooltipOnHover
                                string={
                                  clientDetails &&
                                  clientDetails.client &&
                                  clientDetails.client.company
                                }
                                sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                              />
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Country : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {summary &&
                                summary.client &&
                                summary.client.country}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Industry : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                                {summary &&
                                summary.client &&
                                summary.client.industry}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Customer Id : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.customerId, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Address : &nbsp; &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientAddress, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              City : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {summary &&
                                summary.client &&
                                summary.client.city}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              State : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {summary &&
                                summary.client &&
                                summary.client.state}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Zip Code : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientZip, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                             Report Email : &nbsp;
                            </span>
                            <div className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.report_email, SLICE_CHARACTERS.UP_TO_40)}
                            </div>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Email : &nbsp;
                            </span>
                            <div className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientEmail, SLICE_CHARACTERS.UP_TO_40)}
                            </div>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Phone : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientPhoneNumber, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Extension : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.phoneNumberExtension, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              URL : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientURL, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Source : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.leadSource, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Client Complexity : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientComplexity, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Currency : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.currency, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Company Description : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.client &&
                                clientDetails.client.clientDescription, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='col-xl-6'>
                    <div className='h-100 align-items-center'>
                      <div className='w-100 h-100 rounded-3 bg-light bg-opacity-75 py-12 px-10'>
                        <div className='mb-7'>
                          <h2 className='text-gray-700 text-center mb-5'>
                            Login Info
                          </h2>
                        </div>
                        <div className='w-80 mb-10'>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              First Name : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.user &&
                                clientDetails.user.firstName, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                        </div>
                        <div className='w-80 mb-10'>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Last Name: &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.user &&
                                clientDetails.user.lastName, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                        </div>
                        <div className='w-80 mb-10'>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Email : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(clientDetails &&
                                clientDetails.user &&
                                clientDetails.user.email, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                        </div>
                        <div className='w-80 mb-10'>
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Role : &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {addEllipsis(summary &&
                                summary.user &&
                                summary.user.roleId, SLICE_CHARACTERS.UP_TO_40)}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className='card-body'>
                <div className='row g-4'>
                  <div className='col-xl-6'>
                    <div className='h-100 align-items-center'>
                      <div className='w-100 h-100 rounded-3 bg-light bg-opacity-75 py-12 px-10'>
                        <div className='mb-7'>
                          <h2 className='text-gray-700 text-center mb-5'>
                            Package
                          </h2>
                        </div>
                        <div className='w-80 mb-10'>
                          {clientDetails &&
                          clientDetails.package &&
                          clientDetails.package.orderIntelligence
                            ? (
                              <div className='d-flex align-items-center mb-5'>
                                <span className='fw-bold fs-6 text-gray-800'>
                                  Order Intelligence : &nbsp;
                                </span>
                                <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                                  {clientDetails &&
                                  clientDetails.package &&
                                  clientDetails.package
                                    .orderIntelligence}
                                </span>
                              </div>
                              )
                            : null}
                          {clientDetails &&
                          clientDetails.package &&
                          clientDetails.package.preventionAlert
                            ? (
                              <div className='d-flex align-items-center mb-5'>
                                <span className='fw-bold fs-6 text-gray-800'>
                                  Prevention Alerts : &nbsp;
                                </span>
                                <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                                  {clientDetails &&
                                  clientDetails.package &&
                                  clientDetails.package.preventionAlert}
                                </span>
                              </div>
                              )
                            : null}
                          {clientDetails &&
                          clientDetails.package &&
                          clientDetails.package.chargebackManagement
                            ? (
                              <div className='d-flex align-items-center mb-5'>
                                <span className='fw-bold fs-6 text-gray-800'>
                                  Chargeback Management : &nbsp;
                                </span>
                                <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                                  {clientDetails &&
                                  clientDetails.package &&
                                  clientDetails.package
                                    .chargebackManagement}
                                </span>
                              </div>
                              )
                            : null}
                          <div className='d-flex align-items-center mb-5'>
                            <span className='fw-bold fs-6 text-gray-800'>
                              Total: &nbsp;
                            </span>
                            <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                              {
                                Number(clientDetails && clientDetails.package && clientDetails.package.preventionAlert) +
                                  Number(clientDetails && clientDetails.package && clientDetails.package.chargebackManagement) +
                                    Number(clientDetails && clientDetails.package && clientDetails.package.orderIntelligence)
                              }
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
          <div className='form-group row mb-2 mt-3'>
            <div className='col-lg-6' />
            <div className='col-lg-6'>
              <div className='col-lg-11'>
                <button
                  className='btn btn-success m-2 fa-pull-right'
                  onClick={() => {
                    handleSave()
                  }}
                >
                  {loadingACL
                    ? (
                      <span
                        className='spinner-border spinner-border-sm mx-3'
                        role='status'
                        aria-hidden='true'
                      />
                      )
                    : (
                        'Create Client Account'
                      )}
                </button>
                <button
                  className='btn btn-darkRed m-2 fa-pull-right'
                  onClick={() => {
                    goBack(1)
                  }}
                >
                  Back
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => ({
  loadingACL:
    state && state.addClientMgmtStore && state.addClientMgmtStore.loadingACL,
  statusACL:
    state && state.addClientMgmtStore && state.addClientMgmtStore.statusACL,
  messageACL:
    state && state.addClientMgmtStore && state.addClientMgmtStore.messageACL,
  dataACL: state && state.addClientMgmtStore && state.addClientMgmtStore.dataACL
})

const mapDispatchToProps = dispatch => ({
  addClientMgmtDispatch: (data) =>
    dispatch(clientMgmtActions.addClientMgmt(data)),
  clearaddClientMgmtDispatch: () =>
    dispatch(clientMgmtActions.clearaddClientMgmt())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Checkout)