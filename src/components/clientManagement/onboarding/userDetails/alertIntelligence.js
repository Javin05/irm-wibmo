import React, { useState } from 'react'

const [formData] = useState({
  merchantName: '',
  country: '',
  industry: '',
  address: '',
  city: '',
  state: '',
  zip: '',
  email: '',
  phoneNumber: '',
  ext: '',
  url: '',
  companyDescription: ''
})

const handleChange = () => {}

const MerchantInfo = () => {
  return (
    <>
      <div className='card-header bg-skyBlue py-10'>
        <div className='card-body'>
          <div className='form-group row mb-4'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Merchant Name:</label>
              <div className='col-lg-12'>
                <input
                  name='merchantName'
                  type='text'
                  className='form-control'
                  placeholder='Merchant Name'
                  onChange={(e) => handleChange(e)}
                  value={formData.merchantName || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Country:</label>
              <div className='col-lg-12'>
                <input
                  name='country'
                  type='text'
                  className='form-control'
                  placeholder='Country'
                  onChange={(e) => handleChange(e)}
                  value={formData.country || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Industry:</label>
              <div className='col-lg-12'>
                <input
                  name='industry'
                  type='text'
                  className='form-control'
                  placeholder='Industry'
                  onChange={(e) => handleChange(e)}
                  value={formData.industry || ''}
                />
              </div>
            </div>
          </div>
          <div className='form-group row mb-4'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Address:</label>
              <div className='col-lg-12'>
                <input
                  name='address'
                  type='text'
                  className='form-control'
                  placeholder='Address'
                  onChange={(e) => handleChange(e)}
                  value={formData.address || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>City:</label>
              <div className='col-lg-12'>
                <input
                  name='city'
                  type='text'
                  className='form-control'
                  placeholder='City'
                  onChange={(e) => handleChange(e)}
                  value={formData.city || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>State:</label>
              <div className='col-lg-12'>
                <input
                  name='state'
                  type='text'
                  className='form-control'
                  placeholder='State'
                  onChange={(e) => handleChange(e)}
                  value={formData.state || ''}
                />
              </div>
            </div>
          </div>
          <div className='form-group row mb-4'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Zip Code:</label>
              <div className='col-lg-12'>
                <input
                  name='zip'
                  type='text'
                  className='form-control'
                  placeholder='Zip Code'
                  onChange={(e) => handleChange(e)}
                  value={formData.zip || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Email:</label>
              <div className='col-lg-12'>
                <input
                  name='email'
                  type='text'
                  className='form-control'
                  placeholder='Email'
                  onChange={(e) => handleChange(e)}
                  value={formData.email || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Phone Number:</label>
              <div className='col-lg-12'>
                <input
                  name='phoneNumber'
                  type='text'
                  className='form-control'
                  placeholder='Phone Number'
                  onChange={(e) => handleChange(e)}
                  value={formData.phoneNumber || ''}
                />
              </div>
            </div>
          </div>
          <div className='form-group row mb-4'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Phone Ext:</label>
              <div className='col-lg-12'>
                <input
                  name='ext'
                  type='text'
                  className='form-control'
                  placeholder='Phone Ext'
                  onChange={(e) => handleChange(e)}
                  value={formData.ext || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>External Id:</label>
              <div className='col-lg-12'>
                <input
                  name='url'
                  type='text'
                  className='form-control'
                  placeholder='URL'
                  onChange={(e) => handleChange(e)}
                  value={formData.url || ''}
                />
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>Company Description:</label>
              <div className='col-lg-12'>
                <input
                  name='companyDescription'
                  type='text'
                  className='form-control'
                  placeholder='Company Description'
                  onChange={(e) => handleChange(e)}
                  value={formData.companyDescription || ''}
                />
              </div>
            </div>
          </div>

          <div className='form-group row mb-4'>
            <div className='col-lg-6' />
            <div className='col-lg-6'>
              <div className='col-lg-11'>
                <button
                  className='btn btn-blue m-2 fa-pull-right'
                  onClick={(event) => {
                    handleSubmit(event)
                  }}
                >
                  {loading
                    ? (
                      <span
                        className='spinner-border spinner-border-sm mx-3'
                        role='status'
                        aria-hidden='true'
                      />
                      )
                    : (
                        'Save'
                      )}
                </button>
                <Link to='/chargeback-management' className='btn btn-darkRed m-2 fa-pull-right'>
                  Reset
                </Link>
              </div>
            </div>
          </div>

        </div>
      </div>

    </>
  )
}

export default MerchantInfo
