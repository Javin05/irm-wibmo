import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Link, useLocation } from 'react-router-dom'
import { clientValidation } from './validation'
import ReactSelect from '../../../../theme/layout/components/ReactSelect'
import { REGEX } from '../../../../utils/constants'
import color from '../../../../utils/colors'
import {
  countryActions,
  stateActions,
  cityActions
} from '../../../../store/actions'

const AddClientForm = (props) => {
  const {
    getCountryDispatch,
    getCountrys,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys
  } = props
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update-partners-client/");
  const id = url && url[1];
  const [countryOption, setCountryOption] = useState()
  const [selectedCountryOption, setSelectedCountryOption] = useState('')
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [errors, setErrors] = useState({})
  const [typingTimeout, setTypingTimeout] = useState(0)
  const [formData, setFormData] = useState({
    clientFirstName: '',
    clientLastName: '',
    industry: '',
    company: '',
    clientCountry: '',
    clientAddress: '',
    clientCity: '',
    clientState: '',
    clientZip: '',
    clientEmail: '',
    clientPhoneNumber: '',
    clientNotes: ''
  })
  useEffect(() => {
    getCountryDispatch()
    getStateDispatch()
    getCityDispatch()
  }, [])

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const customStyles = {
    option: (provided, clientState) => ({
      ...provided,
      color: clientState.isSelected ? color.gray : color.black,
      background: clientState.isSelected ? color.white : ''
    })
  }

  const handleChangeCountry = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          getStateDispatch({ countryId: selectedOption.value })
        }, 1500)
      )
      setSelectedCountryOption(selectedOption)
      setFormData((values) => ({
        ...values,
        clientCountry: selectedOption.value,
        clientState: '',
        clientCity: ''
      }))
      setSelectedStateOption()
      setSelectedCityOption()
    } else {
      setSelectedCountryOption()
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData((values) => ({
        ...values,
        clientCountry: '',
        clientState: '',
        clientCity: ''
      }))
    }
    setErrors({ ...errors, clientCountry: '' })
  }

  const handleChangeState = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          getCityDispatch({ stateId: selectedOption.value })
        }, 1500)
      )
      setSelectedStateOption(selectedOption)
      setFormData((values) => ({
        ...values,
        clientState: selectedOption.value,
        clientCity: ''
      }))
      setSelectedCityOption()
    } else {
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData((values) => ({ ...values, clientState: '', clientCity: '' }))
    }
    setErrors({ ...errors, clientState: '' })
  }

  const handleChangeCity = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData((values) => ({ ...values, clientCity: selectedOption.value }))
    } else {
      setSelectedCityOption()
      setFormData((values) => ({ ...values, clientCity: '' }))
    }
    setErrors({ ...errors, clientCity: '' })
  }

  useEffect(() => {
    const clientCountry = getDefaultOptions(getCountrys)
    setCountryOption(clientCountry)
    if (!_.isEmpty(formData.clientCountry)) {
      const selOption = _.filter(clientCountry, function (x) {
        if (_.includes(formData.clientCountry._id, x.value)) {
          return x
        }
      })
      setSelectedCountryOption(selOption)
    }
  }, [getCountrys])

  useEffect(() => {
    const clientState = getDefaultOptions(getStates)
    setStateOption(clientState)
    if (!_.isEmpty(formData.clientState)) {
      const selOption = _.filter(clientState, function (x) {
        if (_.includes(formData.clientState._id, x.value)) {
          return x
        }
      })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  useEffect(() => {
    const clientCity = getDefaultOptions(getCitys)
    setCityOptions(clientCity)
    if (!_.isEmpty(formData.clientCity)) {
      const selOption = _.filter(clientCity, function (x) {
        if (_.includes(formData.clientCity._id, x.value)) {
          return x
        }
      })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({
        label: rawData[item].name,
        value: rawData[item]._id
      })
    }
    return defaultOptions
  }

  const handleSubmit = () => {
    const errorMsg = clientValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {


    }
  }

  useEffect(() => {
    if (id) {

    }
  }, [id])

  return (
    <>
      <div className='card-header bg-skyBlue py-10'>
        <div className='card-body'>
          <div className="d-flex justify-content-end">
            <Link
              to='/partner-onboarding'
              className='btn btn-md btn-darkRed me-3 font-5vw'
            >
              Back
            </Link>
          </div>
          <div className='form-group row mb-6'>
            <h2 className='mb-5'>Client Details</h2>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Client First Name:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='clientFirstName'
                  type='text'
                  className='form-control'
                  placeholder='Client First Name'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientFirstName || ''}
                  maxLength={42}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientFirstName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientFirstName}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-2'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Client Last Name:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='clientLastName'
                  type='text'
                  className='form-control'
                  placeholder='Client Last Name'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientLastName || ''}
                  maxLength={42}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientLastName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientLastName}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Industry:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='industry'
                  type='text'
                  className='form-control'
                  placeholder='Industry'
                  onChange={(e) => handleChange(e)}
                  value={formData.industry || ''}
                  maxLength={42}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.industry && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.industry}
                </div>
              )}
            </div>
          </div>
          <div className='form-group row mb-6'>
            <div className='col-lg-4 mb-2'>
              <label className='font-size-xs  font-weight-bold mb-2 form-label'>
                Company Name:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='company'
                  type='text'
                  className='form-control'
                  placeholder='Company Name'
                  onChange={(e) => handleChange(e)}
                  value={formData.company || ''}
                  maxLength={42}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.company && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.company}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Email:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='clientEmail'
                  type='text'
                  className='form-control'
                  placeholder='Email'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientEmail || ''}
                  maxLength={42}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientEmail && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientEmail}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-2'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Phone Number:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='clientPhoneNumber'
                  type='text'
                  className='form-control'
                  placeholder='Phone Number'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientPhoneNumber || ''}
                  maxLength={12}
                  onKeyPress={(e) => {
                    if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientPhoneNumber && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientPhoneNumber}
                </div>
              )}
            </div>
          </div>
          <div className='form-group row mb-6'>
            <div className='col-lg-4 mb-2'>
              <label className='font-size-xs  font-weight-bold mb-2 form-label'>
                Client Description:
              </label>
              <div className='col-lg-11'>
                <textarea
                  autoComplete='off'
                  name='clientNotes'
                  type='text'
                  className='form-control'
                  placeholder='Client Description'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientNotes || ''}
                  maxLength={500}
                  onCopy={(e) => {
                    e.preventDefault()
                    return false
                  }}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientNotes && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientNotes}
                </div>
              )}
            </div>
          </div>
          <div className='form-group row mb-6'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Country:
              </label>
              <div className='col-lg-11'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='clientCountry'
                  className='basic-single'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeCountry}
                  options={countryOption}
                  value={selectedCountryOption}
                />
              </div>
              {errors && errors.clientCountry && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientCountry}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                State:
              </label>
              <div className='col-lg-11'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='clientState'
                  className='basic-single'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeState}
                  options={stateOption}
                  value={selectedStateOption}
                />
              </div>
              {errors && errors.clientState && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientState}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                City:
              </label>
              <div className='col-lg-11'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='clientCity'
                  className='basic-single'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeCity}
                  options={cityOptions}
                  value={selectedCityOption}
                />
              </div>
              {errors && errors.clientCity && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientCity}
                </div>
              )}
            </div>
          </div>
          <div className='form-group row mb-6'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Address:
              </label>
              <div className='col-lg-11'>
                <textarea
                  autoComplete='off'
                  name='clientAddress'
                  type='text'
                  className='form-control'
                  placeholder='Address'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientAddress || ''}
                  maxLength={500}
                  onCopy={(e) => {
                    e.preventDefault()
                    return false
                  }}
                  onKeyPress={(e) => {
                    if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientAddress && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientAddress}
                </div>
              )}
            </div>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 required form-label'>
                Zip Code:
              </label>
              <div className='col-lg-11'>
                <input
                  autoComplete='off'
                  name='clientZip'
                  type='text'
                  className='form-control'
                  placeholder='Zip'
                  onChange={(e) => handleChange(e)}
                  value={formData.clientZip || ''}
                  maxLength={6}
                  onKeyPress={(e) => {
                    if (!/[0-9]/.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
              </div>
              {errors && errors.clientZip && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.clientZip}
                </div>
              )}
            </div>
          </div>
        </div>
        <div className='form-group row mb-4'>
          <div className='col-lg-6' />
          <div className='col-lg-6'>
            <div className='col-lg-11'>
              <button
                className='btn btn-success m-2 fa-pull-right'
                onClick={() => {
                  handleSubmit()
                }}
              >
                Update
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (clientState) => ({
  getCountrys: clientState && clientState.CountrylistStore && clientState.CountrylistStore.getCountrys,
  loading: clientState && clientState.CountrylistStore && clientState.CountrylistStore.loading,
  getStates: clientState && clientState.StatelistStore && clientState.StatelistStore.getStates,
  loading: clientState && clientState.StatelistStore && clientState.StatelistStore.loading,
  getCitys: clientState && clientState.CitylistStore && clientState.CitylistStore.getCitys,
  loading: clientState && clientState.CitylistStore && clientState.CitylistStore.loading
})

const mapDispatchToProps = (dispatch) => ({
  getCountryDispatch: () => dispatch(countryActions.getCountrys()),
  countryActions: (data) => dispatch(countryActions.getCountrys(data)),
  getStateDispatch: (params) => dispatch(stateActions.getStates(params)),
  stateActions: (data) => dispatch(stateActions.getStates(data)),
  getCityDispatch: (params) => dispatch(cityActions.getCitys(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddClientForm)