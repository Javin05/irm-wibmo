import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { useLocation } from "react-router-dom";
import { companyValidation } from "./validation";
import ReactSelect from "../../../../theme/layout/components/ReactSelect";
import {
  SET_STORAGE,
  REGEX,
  STATUS_RESPONSE,
} from "../../../../utils/constants";
import { setLocalStorage, getLocalStorage } from "../../../../utils/helper";
import { warningAlert, confirmAlert } from "../../../../utils/alerts";
import {
  setPartnerCompanyDetails,
  getPartnerDetailsPayload,
} from "../functions/formData";
import color from "../../../../utils/colors";
import {
  countryActions,
  stateActions,
  cityActions,
  industryActions,
  getPartnersDetailsActions,
  updatePartnersActions,
} from "../../../../store/actions";

const CompanyDetails = (props) => {
  const {
    getCountryDispatch,
    getCountrys,
    loadingCountry,
    getStateDispatch,
    getStates,
    loadingState,
    getCityDispatch,
    getCitys,
    loadingCity,
    getIndustry,
    getIndustryDispatch,
    getPartnerDetailsDispatch,
    loadingGetDetailsPartners,
    getDetailsPartners,
    updatePartnersDispatch,
    messageUpdatePartners,
    loadingUpdatePartners,
    statusUpdatePartners,
    clearUpdatePartnersDispatch,
  } = props;

  const { onClickNext, setPartnerDetails } = props;
  const [isFormUpdated, setFormUpdated] = useState(false);
  const [countryOption, setCountryOption] = useState();
  const [selectedCountryOption, setSelectedCountryOption] = useState("");
  const [stateOption, setStateOption] = useState();
  const [selectedStateOption, setSelectedStateOption] = useState("");
  const [cityOptions, setCityOptions] = useState();
  const [selectedCityOption, setSelectedCityOption] = useState("");
  const [selectedIndustryOption, setSelectedIndustryOption] = useState("");
  const [industryOption, setIndustryOption] = useState();
  const [errors, setErrors] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [typingTimeout, setTypingTimeout] = useState(0)
  const [formData, setFormData] = useState({
    partnerFirstName: "",
    partnerLastName: "",
    industry: "",
    companyName: "",
    country: "",
    address: "",
    city: "",
    state: "",
    zip: "",
    email: "",
    phone: "",
    partnerNotes: "",
  });
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update/");
  const id = url && url[1];

  useEffect(() => {
    getCountryDispatch();
    getStateDispatch();
    getCityDispatch();
    getIndustryDispatch();
  }, []);

  const handleChange = (e) => {
    e.persist();
    const { value, name } = e.target;
    !isFormUpdated && setFormUpdated(true);
    setFormData((values) => ({ ...values, [name]: value }));
    setErrors({ ...errors, [name]: "" });
  };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  };

  const handleChangeCountry = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          getStateDispatch({ countryId: selectedOption.value })
        }, 1500)
      )
      setSelectedCountryOption(selectedOption);
      setFormData((values) => ({
        ...values,
        country: selectedOption.value,
        state: "",
        city: "",
      }));
      setSelectedStateOption();
      setSelectedCityOption();
    } else {
      setSelectedCountryOption();
      setSelectedStateOption();
      setSelectedCityOption();
      setFormData((values) => ({
        ...values,
        country: "",
        state: "",
        city: "",
      }));
    }
    setErrors({ ...errors, country: "" });
  };

  const handleChangeState = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          getCityDispatch({ stateId: selectedOption.value })
        }, 1500)
      )
      setSelectedStateOption(selectedOption);
      setFormData((values) => ({
        ...values,
        state: selectedOption.value,
        city: "",
      }));
      setSelectedCityOption();
    } else {
      setSelectedStateOption();
      setSelectedCityOption();
      setFormData((values) => ({ ...values, state: "", city: "" }));
    }
    setErrors({ ...errors, state: "" });
  };

  const handleChangeCity = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption);
      setFormData((values) => ({ ...values, city: selectedOption.value }));
    } else {
      setSelectedCityOption();
      setFormData((values) => ({ ...values, city: "" }));
    }
    setErrors({ ...errors, city: "" });
  };

  const handleChangeIndustry = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedIndustryOption(selectedOption);
      setFormData((values) => ({ ...values, industry: selectedOption.value }));
      setErrors({ ...errors, industry: "" });
    } else {
      setSelectedIndustryOption();
      setFormData((values) => ({ ...values, industry: "" }));
    }
  };

  useEffect(() => {
    const country = getDefaultOptions(getCountrys);
    setCountryOption(country);
    // if (!_.isEmpty(formData.country)) {
    //   const selOption = _.filter(country, function (x) {
    //     if (_.includes(formData.country._id, x.value)) {
    //       return x
    //     }
    //   })
    //   setSelectedCountryOption(selOption)
    // }
  }, [getCountrys]);

  useEffect(() => {
    const state = getDefaultOptions(getStates);
    setStateOption(state);
    // if (!_.isEmpty(formData.state)) {
    //   const selOption = _.filter(state, function (x) {
    //     if (_.includes(formData.state._id, x.value)) {
    //       return x
    //     }
    //   })
    //   setSelectedStateOption(selOption)
    // }
  }, [getStates]);

  useEffect(() => {
    const city = getDefaultOptions(getCitys);
    setCityOptions(city);
    // if (!_.isEmpty(formData.city)) {
    //   const selOption = _.filter(city, function (x) {
    //     if (_.includes(formData.city._id, x.value)) {
    //       return x
    //     }
    //   })
    //   setSelectedCityOption(selOption)
    // }
  }, [getCitys]);

  const getDefaultOptions = (rawData) => {
    const defaultOptions = [];
    for (const item in rawData) {
      defaultOptions.push({
        label: rawData[item].name,
        value: rawData[item]._id,
      });
    }
    return defaultOptions;
  };

  const handleSubmit = () => {
    const errorMsg = companyValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        const getPayload = getPartnerDetailsPayload(formData);
        updatePartnersDispatch(id, getPayload);
      } else {
        setPartnerDetails((values) => ({ ...values, partner: formData }));
        setLocalStorage(SET_STORAGE.PARTNER_DETAILS, JSON.stringify(formData));
        onClickNext(1);
      }
    }
  };

  const onConfirm = () => {
    // clientDetailsDispatch(id)
  };

  useEffect(() => {
    if (statusUpdatePartners === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageUpdatePartners,
        "success",
        "ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearUpdatePartnersDispatch();
    } else if (statusUpdatePartners === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert("Error", messageUpdatePartners, "", "Ok");
    }
    clearUpdatePartnersDispatch();
  }, [statusUpdatePartners]);

  const getDefaultIndustryOptions = (data, name) => {
    const defaultOptions = [];
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ""}`,
          value: item._id,
        })
      );
      return defaultOptions;
    }
  };

  useEffect(() => {
    if (!editMode) {
      const localData = JSON.parse(
        getLocalStorage(SET_STORAGE.PARTNER_DETAILS)
      );
      if (!_.isEmpty(localData)) {
        setFormData(localData);
        if (!_.isEmpty(localData.country)) {
          const country = getDefaultOptions(getCountrys);
          const selOption = _.filter(country, function (x) {
            if (_.includes(localData.country._id, x.value)) {
              return x;
            }
          });
          setSelectedCountryOption(selOption);
        }
        if (!_.isEmpty(localData.state)) {
          const state = getDefaultOptions(getStates);
          const selOption = _.filter(state, function (x) {
            if (_.includes(localData.state._id, x.value)) {
              return x;
            }
          });
          setSelectedStateOption(selOption);
        }
        if (!_.isEmpty(formData.city)) {
          const city = getDefaultOptions(getCitys);
          const selOption = _.filter(city, function (x) {
            if (_.includes(formData.city._id, x.value)) {
              return x;
            }
          });
          setSelectedCityOption(selOption);
        }
        if (localData.industry) {
          const data = getDefaultIndustryOptions(getIndustry, "industryType");
          const selOption = _.filter(data, function (x) {
            if (_.includes(localData.industry, x.value)) {
              return x;
            }
          });
          setSelectedIndustryOption(selOption);
        }
      }
    }
  }, []);

  useEffect(() => {
    const data = getDefaultIndustryOptions(getIndustry, "industryType");
    setIndustryOption(data);
  }, [getIndustry]);

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        if (!editMode) {
          setPartnerDetails((values) => ({ ...values, partner: formData }));
        }
        setFormUpdated(false);
      }
    };
  }, []);

  useEffect(() => {
    if (id) {
      setEditMode(true);
      getPartnerDetailsDispatch(id);
    } else {
      setEditMode(false);
    }
  }, [id]);

  useEffect(() => {
    if (getDetailsPartners && getDetailsPartners._id) {
      const data = setPartnerCompanyDetails(getDetailsPartners);
      setFormData(data);
      if (getDetailsPartners.partnerCountry) {
        const country = getDefaultOptions(getCountrys);
        const selOption = _.filter(country, function (x) {
          if (_.includes(getDetailsPartners.partnerCountry, x.value)) {
            return x;
          }
        });
        if (selOption.length > 0) {
          setSelectedCountryOption(selOption);
        }
      }
      if (getDetailsPartners.partnerState) {
        const state = getDefaultOptions(getStates);
        const selOption = _.filter(state, function (x) {
          if (_.includes(getDetailsPartners.partnerState, x.value)) {
            return x;
          }
        });
        if (selOption.length > 0) {
          setSelectedStateOption(selOption);
        }
      }
      if (getDetailsPartners.partnerCity) {
        const city = getDefaultOptions(getCitys);
        const selOption = _.filter(city, function (x) {
          if (_.includes(getDetailsPartners.partnerCity, x.value)) {
            return x;
          }
        });
        if (selOption.length > 0) {
          setSelectedCityOption(selOption);
        }
      }
      if (getDetailsPartners.industry) {
        const data = getDefaultIndustryOptions(getIndustry, "industryType");
        const selOption = _.filter(data, function (x) {
          if (_.includes(getDetailsPartners.industry, x.value)) {
            return x;
          }
        });
        setSelectedIndustryOption(selOption);
      }
    }
  }, [getDetailsPartners]);

  return (
    <>
      <div className="card-header bg-skyBlue py-10">
        <div className="card-body">
          {loadingGetDetailsPartners ? (
            <div className="d-flex justify-content-center py-5">
              <div className="spinner-border text-primary m-5" role="status" />
            </div>
          ) : (
            <>
              <div className="form-group row mb-6">
                <h2 className="mb-5">Partner Details</h2>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Partner First Name:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="partnerFirstName"
                      type="text"
                      className="form-control"
                      placeholder="Partner First Name"
                      onChange={(e) => handleChange(e)}
                      value={formData.partnerFirstName || ""}
                      maxLength={42}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.partnerFirstName && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.partnerFirstName}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Partner Last Name:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="partnerLastName"
                      type="text"
                      className="form-control"
                      placeholder="Partner Last Name"
                      onChange={(e) => handleChange(e)}
                      value={formData.partnerLastName || ""}
                      maxLength={42}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.partnerLastName && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.partnerLastName}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Industry:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="industry"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeIndustry}
                      options={industryOption}
                      value={selectedIndustryOption}
                    />
                  </div>
                  {errors && errors.industry && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.industry}
                    </div>
                  )}
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Company Name:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="companyName"
                      type="text"
                      className="form-control"
                      placeholder="Company Name"
                      onChange={(e) => handleChange(e)}
                      value={formData.companyName || ""}
                      maxLength={42}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.companyName && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.companyName}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Email:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="email"
                      type="text"
                      className="form-control"
                      placeholder="Email"
                      onChange={(e) => handleChange(e)}
                      value={formData.email || ""}
                      maxLength={42}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.email && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.email}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Phone Number:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="phone"
                      type="text"
                      className="form-control"
                      placeholder="Phone Number"
                      onChange={(e) => handleChange(e)}
                      value={formData.phone || ""}
                      maxLength={12}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.phone && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.phone}
                    </div>
                  )}
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Client Description:
                  </label>
                  <div className="col-lg-11">
                    <textarea
                      autoComplete="off"
                      name="partnerNotes"
                      type="text"
                      className="form-control"
                      placeholder="Client Description"
                      onChange={(e) => handleChange(e)}
                      value={formData.partnerNotes || ""}
                      maxLength={500}
                      onCopy={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.partnerNotes && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.partnerNotes}
                    </div>
                  )}
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Country:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="country"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeCountry}
                      options={countryOption}
                      value={selectedCountryOption}
                      isLoading={loadingCountry}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    State:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="state"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeState}
                      options={stateOption}
                      value={selectedStateOption}
                      isLoading={loadingState}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    City:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="city"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeCity}
                      options={cityOptions}
                      value={selectedCityOption}
                      isLoading={loadingCity}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Address:
                  </label>
                  <div className="col-lg-11">
                    <textarea
                      autoComplete="off"
                      name="address"
                      type="text"
                      className="form-control"
                      placeholder="Address"
                      onChange={(e) => handleChange(e)}
                      value={formData.address || ""}
                      maxLength={500}
                      onCopy={(e) => {
                        e.preventDefault();
                        return false;
                      }}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.address && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.address}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Zip Code:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="zip"
                      type="text"
                      className="form-control"
                      placeholder="Zip"
                      onChange={(e) => handleChange(e)}
                      value={formData.zip || ""}
                      maxLength={6}
                      onKeyPress={(e) => {
                        if (!/[0-9]/.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.zip && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.zip}
                    </div>
                  )}
                </div>
              </div>
            </>
          )}
        </div>
        {editMode ? (
          <div className="form-group row mb-4">
            <div className="col-lg-6" />
            <div className="col-lg-6">
              <div className="col-lg-11">
                <button
                  className="btn btn-blue m-2 fa-pull-right"
                  onClick={(event) => {
                    handleSubmit(event);
                  }}
                >
                  {loadingUpdatePartners ? (
                    <span
                      className="spinner-border spinner-border-sm mx-3"
                      role="status"
                      aria-hidden="true"
                    />
                  ) : (
                    "Save"
                  )}
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div className="form-group row mb-4">
            <div className="col-lg-6" />
            <div className="col-lg-6">
              <div className="col-lg-11">
                <button
                  className="btn btn-orange m-2 fa-pull-right"
                  onClick={(event) => {
                    handleSubmit(event);
                  }}
                >
                  Next
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.getCountrys,
  loadingCountry: state && state.CountrylistStore && state.CountrylistStore.loading,
  getStates: state && state.StatelistStore && state.StatelistStore.getStates,
  loadingState: state && state.StatelistStore && state.StatelistStore.loading,
  getCitys: state && state.CitylistStore && state.CitylistStore.getCitys,
  loadingCity: state && state.CitylistStore && state.CitylistStore.loading,
  getIndustry: state && state.industryStore && state.industryStore.getIndustry,
  getDetailsPartners:
    state &&
    state.getPartnersDetailsStore &&
    state.getPartnersDetailsStore.getDetailsPartners,
  loadingGetDetailsPartners:
    state &&
    state.getPartnersDetailsStore &&
    state.getPartnersDetailsStore.loadingGetDetailsPartners,
  loadingUpdatePartners:
    state &&
    state.updatePartnerstore &&
    state.updatePartnerstore.loadingUpdatePartners,
  statusUpdatePartners:
    state &&
    state.updatePartnerstore &&
    state.updatePartnerstore.statusUpdatePartners,
  messageUpdatePartners:
    state &&
    state.updatePartnerstore &&
    state.updatePartnerstore.messageUpdatePartners,
});

const mapDispatchToProps = (dispatch) => ({
  getCountryDispatch: () => dispatch(countryActions.getCountrys()),
  countryActions: (data) => dispatch(countryActions.getCountrys(data)),
  getStateDispatch: (params) => dispatch(stateActions.getStates(params)),
  stateActions: (data) => dispatch(stateActions.getStates(data)),
  getCityDispatch: (params) => dispatch(cityActions.getCitys(params)),
  getIndustryDispatch: (params) =>
    dispatch(industryActions.getIndustry(params)),
  cityActions: (data) => dispatch(cityActions.getCitys(data)),
  getPartnerDetailsDispatch: (data) =>
    dispatch(getPartnersDetailsActions.getDetails(data)),
  updatePartnersDispatch: (id, params) =>
    dispatch(updatePartnersActions.update(id, params)),
  clearUpdatePartnersDispatch: () => dispatch(updatePartnersActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CompanyDetails);
