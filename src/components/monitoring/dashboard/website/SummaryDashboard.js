import React, { useEffect, useState, useCallback } from 'react'
import moment from "moment"
import Status from './StatusChange'
import {
  MONITOR_STATUS, RISKSTATUS,
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  CONDITON_DETECT
} from '../../../../utils/constants'
import DashboardStatus from './dashboardStatus'
import { connect } from "react-redux"
import _ from "lodash"
import {
  MonitorDashboardActions,
  MonitorDashboardStatusActions
} from "../../../../store/actions"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../../../utils/alerts"
import { useLocation } from "react-router-dom"
import Update from './update'

function SummaryDashboard(props) {
  const {
    MonitorDashboardData,
    MonitorDashboardDateData,
    MonitorDashboardIData,
    setActiveStep,
    MonitorDashboardStatusData,
    monitorDashboardStatusDispatch,
    MonitorDashboardDispatch,
    clearMonitorDashboardStatusDispatch,
    MonitorDashboardloading
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split("update/")
  const id = url && url[1]
  const [statusData, setStatusData] = useState(undefined)
  const [idValue, setidValue] = useState({})
  const changeValue = async (name) => {
    setStatusData(name)
  }

  const Header = [
    "Website Working?",
    "Website Success Rate",
    "Line of Business & MCC",
    "Risk Classification",
    "Legal Name",
    "Merchant Address",
    "Website Redirection",
    "Page Loading Time",
    "Policy Compliance",
    "Domain Expiry Risk",
    "Parked Domain",
    "Online Reputation Drop",
    "Heavy Discounts",
    "Non INR Pricing",
    // "Blacklist Match Check",
    "Contact Details - Phone",
    "Valid Contact - Phone",
    "Valid Contact - Email",
    "Contact Details - Email",
  ]

  const CurrentScandata = MonitorDashboardData && MonitorDashboardData.data && !_.isEmpty(MonitorDashboardData.data.current_scan) ? MonitorDashboardData.data.current_scan : "--"
  const LastScanData = MonitorDashboardData && MonitorDashboardData.data && !_.isEmpty(MonitorDashboardData.data.base_Scan) ? MonitorDashboardData.data.base_Scan : "--"
  const currentId = CurrentScandata && CurrentScandata._id


  useEffect(() => {
    setidValue(currentId)
  }, [currentId])


  const onConfirmApprove = (Value) => {
    const params = {
      id: idValue,
      status: "ACCEPTED",
      reportKey: Value
    }
    monitorDashboardStatusDispatch(params)
  }


  const approveSubmit = (name) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.APPROVE,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmApprove(name)
      },
      () => { }
    )
  }

  const onConfirmReject = (name) => {
    const params = {
      id: idValue,
      status: "REJECTED",
      reportKey: name
    }
    monitorDashboardStatusDispatch(params)
  }

  const rejectSubmit = (name) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.REJECT,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmReject(name)
      },
      () => { }
    )
  }

  useEffect(() => {
    if (MonitorDashboardStatusData && MonitorDashboardStatusData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        MonitorDashboardStatusData && MonitorDashboardStatusData.message,
        'success'
      )
      MonitorDashboardDispatch(id)
      clearMonitorDashboardStatusDispatch()
    } else if (MonitorDashboardStatusData && MonitorDashboardStatusData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        MonitorDashboardStatusData && MonitorDashboardStatusData.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearMonitorDashboardStatusDispatch()
    }
  }, [MonitorDashboardStatusData])

  const summaryDateSelect = (date) => {
    const params = {
      ogm_last_run_date: moment(date).format("YYYY-MM-DD"),
    }
    MonitorDashboardDispatch(id, params)
  }


  const currentchangeInLine = CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.value.length
  const lastchangeInLine = LastScanData.changeInLineOfBusiness && LastScanData.changeInLineOfBusiness.value.length
  const currentaddress = CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.value.length
  const lastaddress = LastScanData.changeInMerchantAddress && LastScanData.changeInMerchantAddress.value.length

  return (
    <>
      <div className="card card-xl-stretch">
        <div className="card-body pt-0 mb-4">
          <div className="row">
            <div className="col-lg-12 ms-4">
              <div className="row mb-4 mt-4">
                <div className="col-lg-4">
                  <h2 className="ms-4 mt-4 mb-4">Summary</h2>
                </div>
                <div className="col-lg-3" />
                <div className="col-lg-5">
                  {/* <div className='d-flex justify-content-end me-8'>
                    <Status />
                  </div> */}
                </div>
              </div>
              <div className="card card-xl-stretch">
                <div className="table-responsive">
                  <table className="table">
                    <thead>
                      <tr className="fw-boldest fs-4 text-white text-center"
                        style={{
                          backgroundColor: '#5151b9'
                        }}
                      >
                        {
                          _.isArray(MonitorDashboardDateData && MonitorDashboardDateData.data) &&
                            !_.isEmpty(MonitorDashboardDateData && MonitorDashboardDateData.data) ? (
                            MonitorDashboardDateData && MonitorDashboardDateData.data.slice(0, 6).map((item, i) => {
                              return (
                                <th key={i}>
                                  <div className='cursor-pointer' onClick={() => { summaryDateSelect(item && item.ogm_run_date) }}>
                                    {
                                      !_.isEmpty(item && item.ogm_run_date) ? (
                                        moment(
                                          item && item.ogm_run_date ? item.ogm_run_date : "--"
                                        ).format("MMM Do YYYY")
                                      ) : 'No Data'
                                    }
                                  </div>
                                </th>
                              )
                            })
                          ) : ''
                        }
                      </tr>
                    </thead>
                    <tbody>
                      <tr className='text-center'>
                        {
                          _.isArray(MonitorDashboardDateData && MonitorDashboardDateData.data)
                            && !_.isEmpty(MonitorDashboardDateData && MonitorDashboardDateData.data)
                            ? (
                              MonitorDashboardDateData && MonitorDashboardDateData.data.slice(0, 6).map((item, id) => {
                                return (
                                  <td key={id}>
                                    <i className={`${MONITOR_STATUS[item && item.changeDetected]} fs-1`}
                                    />
                                    {
                                      item && item.changeDetected === 'YES' ? (
                                        <i className={`${CONDITON_DETECT[item && item.changeDetectedStatus]} ms-2 cursor-pointer`}
                                          title={item && item.changeDetectedStatus}
                                        />
                                      ) : (null)
                                    }
                                  </td>
                                )
                              })
                            ) : ''
                        }
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="separator separator-content border-dark my-0 mb-4 mt-4" />
                <div className="card-body pt-0 mb-4">
                  <div className="table-responsive">
                    <table className="table">
                      <thead>
                        <tr className="fw-boldest fs-4 text-white text-center"
                          style={{
                            backgroundColor: '#5151b9'
                          }}
                        >
                          <th>
                            Parameter
                          </th>
                          <th>
                            Base Line
                          </th>
                          <th>
                            Current Scan  {
                              CurrentScandata && CurrentScandata.ogm_run_date ?
                                moment(
                                  CurrentScandata && CurrentScandata.ogm_run_date ? CurrentScandata.ogm_run_date : "--"
                                ).format("MMM Do YYYY")
                                : ""
                            }
                            {MonitorDashboardData && MonitorDashboardData?.data?.edit ? <Update CurrentScandata={CurrentScandata} MonitorDashboardDispatch={MonitorDashboardDispatch} MonitorDashboardIData={MonitorDashboardIData} /> : null}
                          </th>
                          <th>
                            Changes Detected
                          </th>
                          <th>
                            Changes Status
                          </th>
                          <th>
                            Accept/Reject
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      {!MonitorDashboardloading ?
                        (<tr>
                          <td className='text-gray-700 fw-bold'>
                            {
                              _.isArray(Header) ? (
                                Header.map((item) => {
                                  return (
                                    <div className="p-10 font-13 h-100px">
                                      {
                                        item
                                      }<br />
                                    </div>
                                  )
                                })
                              ) : null
                            }
                          </td>
                          <td className='w-400px'>
                            {
                              !_.isEmpty(LastScanData) ? (
                                <div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.websiteWorking && LastScanData.websiteWorking.value ? LastScanData.websiteWorking.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.websiteSuccessRate && LastScanData.websiteSuccessRate.value ? LastScanData.websiteSuccessRate.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div
                                    className={`${lastchangeInLine
                                      > 44 ? 'p-10 ellipsisDashboardSummary' : lastchangeInLine === 0 ? 'p-10 ellipsisDashboardSummary' : 'p-10 ellipsisDashboardSummary'}`}
                                    title={CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.value}
                                  >
                                    <div>
                                      {
                                        LastScanData.changeInLineOfBusiness && LastScanData.changeInLineOfBusiness.value ? LastScanData.changeInLineOfBusiness.value : '--'
                                      }
                                    </div>
                                    <div>
                                      ({
                                        LastScanData.changeInMcc && LastScanData.changeInMcc.value ? LastScanData.changeInMcc && LastScanData.changeInMcc.value : '--'
                                      })
                                    </div>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.riskClassification && LastScanData.riskClassification.value ? LastScanData.riskClassification.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.changeInLegalName && LastScanData.changeInLegalName.value.trim() ? LastScanData.changeInLegalName.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.changeInMerchantAddress && LastScanData.changeInMerchantAddress.value ? LastScanData.changeInMerchantAddress.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.transactionLaunderingRedirection && LastScanData.transactionLaunderingRedirection.value ? LastScanData.transactionLaunderingRedirection.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.increaseInFullPageLoadingTime && LastScanData.increaseInFullPageLoadingTime.value ? LastScanData.increaseInFullPageLoadingTime.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div
                                    className={"p-10"}
                                  >
                                    <button className='btn btn-sm btn-light-primary'
                                      onClick={() => { setActiveStep(1) }}
                                    >
                                      View
                                    </button>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.domainRisk && LastScanData.domainRisk.value ? LastScanData.domainRisk.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.parkedDomain && LastScanData.parkedDomain.value ? LastScanData.parkedDomain.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.onlineReputationDrop && LastScanData.onlineReputationDrop.value ? LastScanData.onlineReputationDrop.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.heavyDiscounts && LastScanData.heavyDiscounts.value ? LastScanData.heavyDiscounts.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.nonInrPricing && LastScanData.nonInrPricing.value ? LastScanData.nonInrPricing.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.contactDetailsPhone && LastScanData.contactDetailsPhone.value ? LastScanData.contactDetailsPhone.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.containsValidPhone && LastScanData.containsValidPhone.value ? LastScanData.containsValidPhone.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.containsValidEmail && LastScanData.containsValidEmail.value ? LastScanData.containsValidEmail.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        LastScanData.contactDetailsEmail && LastScanData.contactDetailsEmail.value ? LastScanData.contactDetailsEmail.value : '--'
                                      }
                                    </p>
                                  </div>
                                </div>
                              ) : null
                            }
                          </td>
                          <td className='w-400px'>
                            {
                              !_.isEmpty(CurrentScandata) ? (
                                <div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.value ? CurrentScandata.websiteWorking.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.value ? CurrentScandata.websiteSuccessRate.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary"
                                    title={CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.value}
                                  >
                                    <p>
                                      {
                                        CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.value ? CurrentScandata.changeInLineOfBusiness.value : '--'
                                      }
                                    </p>
                                    <div className='pt-8'>
                                      {/* <p> */}
                                        ({
                                          CurrentScandata.changeInMcc && CurrentScandata.changeInMcc.value ? CurrentScandata.changeInMcc && CurrentScandata.changeInMcc.value : '--'
                                        })
                                      {/* </p> */}
                                    </div>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.riskClassification && CurrentScandata.riskClassification.value ? CurrentScandata.riskClassification.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.value.trim() ? CurrentScandata.changeInLegalName.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.value ? CurrentScandata.changeInMerchantAddress.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.value ? CurrentScandata.transactionLaunderingRedirection.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.value ? CurrentScandata.increaseInFullPageLoadingTime.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div
                                    className={
                                      CurrentScandata.changeInAnyPolicy && CurrentScandata.changeInAnyPolicy.value ? "p-10" : "p-10"}
                                  >
                                    <button className='btn btn-sm btn-light-primary'
                                      onClick={() => { setActiveStep(1) }}
                                    >
                                      View
                                    </button>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.domainRisk && CurrentScandata.domainRisk.value ? CurrentScandata.domainRisk.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.value ? CurrentScandata.parkedDomain.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.value ? CurrentScandata.onlineReputationDrop.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.value ? CurrentScandata.heavyDiscounts.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.value ? CurrentScandata.nonInrPricing.value : '--'
                                      }
                                    </p>
                                  </div>
                                  {/* <div className="mb-30 ellipsisDashboardSummary"
                                    title={CurrentScandata.blacklistMatchCheck && LastScanData.blacklistMatchCheck.value}
                                  >
                                    {
                                      CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.value ? CurrentScandata.blacklistMatchCheck.value : '--'
                                    }
                                  </div> */}
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.value ? CurrentScandata.contactDetailsPhone.value : '--'
                                      }
                                    </p>
                                  </div>

                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.value ? CurrentScandata.containsValidPhone.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.value ? CurrentScandata.containsValidEmail.value : '--'
                                      }
                                    </p>
                                  </div>
                                  <div className="p-10 ellipsisDashboardSummary">
                                    <p>
                                      {
                                        CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.value ? CurrentScandata.contactDetailsEmail.value : '--'
                                      }
                                    </p>
                                  </div>
                                </div>
                              ) : null
                            }
                          </td>
                          <td className="text-center">
                            {
                              !_.isEmpty(CurrentScandata) ? (
                                <>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.riskClassification && CurrentScandata.riskClassification.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.riskClassification && CurrentScandata.riskClassification.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.policyCompliance === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.policyCompliance]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.domainRisk && CurrentScandata.domainRisk.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.domainRisk && CurrentScandata.domainRisk.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  {/* <div className="mb-30">
                                    {
                                      CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div> */}
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                  <div className="p-10 h-100px">
                                    {
                                      CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.changeDetected === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.changeDetected]}`}
                                          />
                                        ) : '--'
                                    }
                                  </div>
                                </>
                              ) : null
                            }
                          </td>
                          {/* new changes */}
                          <td className="text-center">
                            <>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("websiteWorking")
                                }}
                              >
                                {
                                  CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.status === "PENDING" ? '--' :
                                    (
                                      <i className={`${CONDITON_DETECT[
                                        CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.status]}`} />
                                    )
                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("websiteSuccessRate")
                                }}
                              >
                                {
                                  CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.status === "PENDING" ? '--' :
                                    (
                                      <i className={`${CONDITON_DETECT[
                                        CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.status]}`} />
                                    )
                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("changeInLineOfBusiness")
                                }}
                              >
                                {
                                  CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.status]}`} />

                                }
                              </div>

                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("riskClassification")
                                }}
                              >
                                {
                                  CurrentScandata.riskClassification && CurrentScandata.riskClassification.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.riskClassification && CurrentScandata.riskClassification.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("changeInLegalName")
                                }}
                              >
                                {
                                  CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("changeInMerchantAddress")
                                }}
                              >
                                {
                                  CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("transactionLaunderingRedirection")
                                }}
                              >
                                {
                                  CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("increaseInFullPageLoadingTime")
                                }}
                              >
                                {
                                  CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.status]}`} />

                                }
                              </div>
                              <div className='w-250px h-100px p-10'
                              >
                                {/* {
                                      CurrentScandata.policyCompliance === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.policyCompliance]}`}
                                          />
                                        ) : '--'
                                    } */}
                                --
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("domainRisk")
                                }}
                              >
                                {
                                  CurrentScandata.domainRisk && CurrentScandata.domainRisk.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.domainRisk && CurrentScandata.domainRisk.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("parkedDomain")
                                }}
                              >
                                {
                                  CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("onlineReputationDrop")
                                }}
                              >
                                {
                                  CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("heavyDiscounts")
                                }}
                              >
                                {
                                  CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("nonInrPricing")
                                }}
                              >
                                {
                                  CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.status]}`} />

                                }
                              </div>
                              {/* <div className=
                              {
                                CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.changeDetected === 'YES' ? "mb-20" : "mb-30"}
                                onClick={() => {
                                  changeValue("blacklistMatchCheck")
                                }}
                              >
                                {
                                  CurrentScandata.blacklistMatchCheck ?
                                    CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.changeDetected === 'YES' ?
                                      (
                                        <DashboardStatus statusData={statusData} idValue={idValue} />
                                      ) : (
                                        CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.status === "PENDING" ? '--' :
                                          (
                                            <span className={`badge ${RISKSTATUS[
                                              CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.status]}`}
                                            >
                                              {
                                                CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.status
                                              }
                                            </span>
                                          )
                                      ) : '--'
                                }
                              </div> */}

                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("contactDetailsPhone")
                                }}
                              >
                                {
                                  CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.status === "PENDING" ? '--' :
                                    (
                                      <i className={`${CONDITON_DETECT[
                                        CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.status]}`} />
                                    )
                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={() => {
                                  changeValue("containsValidPhone")
                                }}
                              >
                                {
                                  CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={(e) => {
                                  changeValue("containsValidEmail")
                                }}
                              >
                                {
                                  CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.status]}`} />

                                }
                              </div>
                              <div
                                className='w-250px h-100px p-10'
                                onClick={(e) => {
                                  changeValue("contactDetailsEmail")
                                }}
                              >
                                {
                                  CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.status === "PENDING" ? '--' :

                                    <i className={`${CONDITON_DETECT[
                                      CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.status]}`} />

                                }
                              </div>
                            </>
                          </td>
                          {/* new changes */}
                          <td className="text-center">
                            <>
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("websiteWorking")
                                  }}
                                >
                                  {
                                    CurrentScandata.websiteWorking ?
                                      CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.changeDetected === 'YES' ?
                                        (
                                          // <DashboardStatus
                                          //   statusData={statusData}
                                          //   idValue={idValue}
                                          // />
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("websiteWorking")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("websiteWorking")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("websiteWorking")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("websiteWorking")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("websiteSuccessRate")
                                  }}
                                >
                                  {
                                    CurrentScandata.websiteSuccessRate ?
                                      CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("websiteSuccessRate")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("websiteSuccessRate")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("websiteSuccessRate")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("websiteSuccessRate")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("changeInLineOfBusiness")
                                  }}
                                >
                                  {
                                    CurrentScandata.changeInLineOfBusiness ?
                                      CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("changeInLineOfBusiness")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("changeInLineOfBusiness")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("changeInLineOfBusiness")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("changeInLineOfBusiness")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("riskClassification")
                                  }}
                                >
                                  {
                                    CurrentScandata.riskClassification ?
                                      CurrentScandata.riskClassification && CurrentScandata.riskClassification.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("riskClassification")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("riskClassification")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.riskClassification && CurrentScandata.riskClassification.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("riskClassification")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("riskClassification")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("changeInLegalName")
                                  }}
                                >
                                  {
                                    CurrentScandata.changeInLegalName ?
                                      CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("changeInLegalName")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("changeInLegalName")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("changeInLegalName")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("changeInLegalName")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("changeInMerchantAddress")
                                  }}
                                >
                                  {
                                    CurrentScandata.changeInMerchantAddress ?
                                      CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.changeDetected === 'YES' ?
                                        (
                                          // <DashboardStatus statusData={statusData} idValue={idValue} />

                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("changeInMerchantAddress")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("changeInMerchantAddress")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("changeInMerchantAddress")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("changeInMerchantAddress")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("transactionLaunderingRedirection")
                                  }}
                                >
                                  {
                                    CurrentScandata.transactionLaunderingRedirection ?
                                      CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("transactionLaunderingRedirection")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("transactionLaunderingRedirection")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("transactionLaunderingRedirection")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("transactionLaunderingRedirection")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("increaseInFullPageLoadingTime")
                                  }}
                                >
                                  {
                                    CurrentScandata.increaseInFullPageLoadingTime ?
                                      CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("increaseInFullPageLoadingTime")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("increaseInFullPageLoadingTime")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("increaseInFullPageLoadingTime")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("increaseInFullPageLoadingTime")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div className='w-250px h-100px p-10'>
                                  {/* {
                                      CurrentScandata.policyCompliance === 'YES' ?
                                        (
                                          <i className={`${MONITOR_STATUS[CurrentScandata.policyCompliance]}`}
                                          />
                                        ) : '--'
                                    } */}
                                  --
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("domainRisk")
                                  }}
                                >
                                  {
                                    CurrentScandata.domainRisk ?
                                      CurrentScandata.domainRisk && CurrentScandata.domainRisk.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("domainRisk")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("domainRisk")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.domainRisk && CurrentScandata.domainRisk.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("domainRisk")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("domainRisk")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("parkedDomain")
                                  }}
                                >
                                  {
                                    CurrentScandata.parkedDomain ?
                                      CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("parkedDomain")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("parkedDomain")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("parkedDomain")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("parkedDomain")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("onlineReputationDrop")
                                  }}
                                >
                                  {
                                    CurrentScandata.onlineReputationDrop ?
                                      CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("onlineReputationDrop")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("onlineReputationDrop")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("onlineReputationDrop")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("onlineReputationDrop")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("heavyDiscounts")
                                  }}
                                >
                                  {
                                    CurrentScandata.heavyDiscounts ?
                                      CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("heavyDiscounts")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("heavyDiscounts")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("heavyDiscounts")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("heavyDiscounts")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("nonInrPricing")
                                  }}
                                >
                                  {
                                    CurrentScandata.nonInrPricing ?
                                      CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("nonInrPricing")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("nonInrPricing")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("nonInrPricing")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("nonInrPricing")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {/* <div className=
                              {
                                CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.changeDetected === 'YES' ? "mb-20" : "mb-30"}
                                onClick={() => {
                                  changeValue("blacklistMatchCheck")
                                }}
                              >
                                {
                                  CurrentScandata.blacklistMatchCheck ?
                                    CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.changeDetected === 'YES' ?
                                      (
                                        <DashboardStatus statusData={statusData} idValue={idValue} />
                                      ) : (
                                        CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.status === "PENDING" ? '--' :
                                          (
                                            <span className={`badge ${RISKSTATUS[
                                              CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.status]}`}
                                            >
                                              {
                                                CurrentScandata.blacklistMatchCheck && CurrentScandata.blacklistMatchCheck.status
                                              }
                                            </span>
                                          )
                                      ) : '--'
                                }
                              </div> */}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("contactDetailsPhone")
                                  }}
                                >
                                  {
                                    CurrentScandata.contactDetailsPhone ?
                                      CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("contactDetailsPhone")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("contactDetailsPhone")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("contactDetailsPhone")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("contactDetailsPhone")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={() => {
                                    changeValue("containsValidPhone")
                                  }}
                                >
                                  {
                                    CurrentScandata.containsValidPhone ?
                                      CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("containsValidPhone")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("containsValidPhone")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>
                                        ) : (
                                          CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("containsValidPhone")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("containsValidPhone")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={(e) => {
                                    changeValue("containsValidEmail")
                                  }}
                                >
                                  {
                                    CurrentScandata.containsValidEmail ?
                                      CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("containsValidEmail")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("containsValidEmail")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>) : (
                                          CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("containsValidEmail")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("containsValidEmail")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                              {MonitorDashboardData && MonitorDashboardData?.data?.edit ?
                                <div
                                  className='w-250px h-100px p-10'
                                  onClick={(e) => {
                                    changeValue("contactDetailsEmail")
                                  }}
                                >
                                  {
                                    CurrentScandata.contactDetailsEmail ?
                                      CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.changeDetected === 'YES' ?
                                        (
                                          <ul className="nav">
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                onClick={() => {
                                                  approveSubmit("contactDetailsEmail")
                                                }}
                                              >
                                                Approve
                                              </a>
                                            </li>
                                            <li className="nav-item">
                                              <a
                                                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                onClick={() => {
                                                  rejectSubmit("contactDetailsEmail")
                                                }}
                                              >
                                                Reject
                                              </a>
                                            </li>
                                          </ul>) : (
                                          CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.status === "PENDING" ? '--' :
                                            (
                                              <ul className="nav">
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                                    onClick={() => {
                                                      approveSubmit("contactDetailsEmail")
                                                    }}
                                                  >
                                                    Approve
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                                    onClick={() => {
                                                      rejectSubmit("contactDetailsEmail")
                                                    }}
                                                  >
                                                    Reject
                                                  </a>
                                                </li>
                                              </ul>
                                            )
                                        ) : '--'
                                  }
                                </div> :
                                <div className='w-250px h-100px p-10'>--</div>}
                            </>
                          </td>
                        </tr>) : 
                          (
                            <tr>
                              <td colSpan='100%' className='text-center'>
                                <div
                                  className='spinner-border text-primary m-5'
                                  role='status'
                                />
                              </td>
                            </tr>
                          )}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  )
}

const mapStateToProps = (state) => {
  const {
    MonitorDashboardStatusStore,
    MonitorDashboardStore
  } = state
  return {
    MonitorDashboardStatusData: MonitorDashboardStatusStore && MonitorDashboardStatusStore.MonitorDashboardStatusData ? MonitorDashboardStatusStore.MonitorDashboardStatusData : {},
    MonitorDashboardloading: MonitorDashboardStore && MonitorDashboardStore.loading ? MonitorDashboardStore.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  MonitorDashboardDispatch: (id, params) => dispatch(MonitorDashboardActions.getMonitorDashboard(id, params)),
  monitorDashboardStatusDispatch: (params) => dispatch(MonitorDashboardStatusActions.getMonitorDashboardStatus(params)),
  clearMonitorDashboardStatusDispatch: (params) => dispatch(MonitorDashboardStatusActions.clearMonitorDashboardStatus(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(SummaryDashboard)