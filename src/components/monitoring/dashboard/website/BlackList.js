import React, { useState } from "react"
import moment from "moment"
import _ from "lodash"

export default function BlacklistMatch(props) {
  const {
    MonitorDashboardData
  } = props

  return (
    <>
      <div className="card card-xl-stretch">
        <h2 className="ms-4 mt-4 mb-4">Blacklist Match</h2>
        <i class="fa fa-times-circle-o" aria-hidden="true"></i>
      </div>
      <div className="card card-xl-stretch">
        {
          _.isArray(MonitorDashboardData && MonitorDashboardData.data) ? (
            MonitorDashboardData && MonitorDashboardData.data.map((result, i) => {
              return (
                <>
                  <h3 className="card-title align-items-start flex-column" key={i}>
                    <span className="card-label fw-bolder text-dark ms-4">
                      {moment(
                        result.ogm_run_date ? result.ogm_run_date : "--"
                      ).format("MMM Do YY")}
                    </span>
                  </h3>
                  <div className="separator separator-content border-dark my-0 mb-4" />
                  <div className="card-body pt-0 mb-4 ms-4">
                    <div className="mt-4 mb-4">
                      <h1 className="fs-3 fw-bolder">
                        Blacklisted Keywords Inserted Categories
                      </h1>
                      <span className="fs-5 fw-bold text-muted">
                        {result && result.blacklisted_keywords_inserted_categories ? result.blacklisted_keywords_inserted_categories : '--'}
                      </span>
                    </div>
                    <div className="mt-4 mb-4">
                      <h1 className="fs-3 fw-bolder">
                        Blacklisted Keywords Removed Categories
                      </h1>
                      <span className="fs-5 fw-bold text-muted">
                        {result && result.blacklisted_keywords_removed_categories ? result.blacklisted_keywords_removed_categories : '--'}
                      </span>
                    </div><div className="mt-4 mb-4">
                      <h1 className="fs-3 fw-bolder">
                        Blacklisted Keywords Change Detected
                      </h1>
                      <span className="fs-5 fw-bold text-muted">
                        {result && result.blacklisted_keywords_change_detected ? result.blacklisted_keywords_change_detected : '--'}
                      </span>
                    </div>
                  </div>
                </>
              )
            })
          ) : 'No Records'
        }
      </div>
    </>
  )
}
