import React, { useState, useEffect, Fragment } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  SWEET_ALERT_MSG,
} from "../../../../utils/constants"
import {
  MonitorDashboardActions,
  MonitorDashboardStatusActions
} from "../../../../store/actions"
import { useLocation } from "react-router-dom"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../../../utils/alerts"

function DashboardStatus(props) {
  const {
    MonitorDashboardStatusData,
    statusData,
    monitorDashboardStatusDispatch,
    idValue,
    MonitorDashboardDispatch,
    clearMonitorDashboardStatusDispatch
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split("update/")
  const id = url && url[1]
  const [Value, setValue] = useState()

  const onConfirmApprove = () => {
    const params = {
      id: idValue,
      status: "ACCEPTED",
      reportKey: Value
    }
    monitorDashboardStatusDispatch(params)
  }
  // console.log('statusData', statusData)
  // console.log('Value', Value)

  useEffect(() =>{
    setValue(statusData)
  },[statusData])


  const approveSubmit = () => {
    // console.log('statusData11', statusData)
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.APPROVE,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmApprove()
      },
      () => { }
    )
  }

  const onConfirmReject = () => {
    const params = {
      id: idValue,
      status: "REJECTED",
      reportKey: statusData
    }
    monitorDashboardStatusDispatch(params)
  }

  const rejectSubmit = () => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.REJECT,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmReject()
      },
      () => { }
    )
  }


  useEffect(() => {
    if (MonitorDashboardStatusData && MonitorDashboardStatusData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        MonitorDashboardStatusData && MonitorDashboardStatusData.message,
        'success'
      )
      MonitorDashboardDispatch(id)
      clearMonitorDashboardStatusDispatch()
    } else if (MonitorDashboardStatusData && MonitorDashboardStatusData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        MonitorDashboardStatusData && MonitorDashboardStatusData.message,
        '',
        'Try again',
        '',
        () => { }
      )
    }
  }, [MonitorDashboardStatusData])

  return (
    <>
      <ul className="nav">
        <li className="nav-item">
          <a
            className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
            onClick={() => {
              approveSubmit()
            }}
          >
            Approve
          </a>
        </li>
        <li className="nav-item">
          <a
            className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
            onClick={() => {
              rejectSubmit()
            }}
          >
            Reject
          </a>
        </li>
      </ul>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    MonitorDashboardStatusStore
  } = state
  return {
    MonitorDashboardStatusData: MonitorDashboardStatusStore && MonitorDashboardStatusStore.MonitorDashboardStatusData ? MonitorDashboardStatusStore.MonitorDashboardStatusData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  MonitorDashboardDispatch: (id) => dispatch(MonitorDashboardActions.getMonitorDashboard(id)),
  monitorDashboardStatusDispatch: (params) => dispatch(MonitorDashboardStatusActions.getMonitorDashboardStatus(params)),
  clearMonitorDashboardStatusDispatch: (params) => dispatch(MonitorDashboardStatusActions.clearMonitorDashboardStatus(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(DashboardStatus)