import React, { useState, useEffect } from "react"
import _ from "lodash"
import moment from "moment"
import DashboardStatus from './dashboardStatus'
import {
  MonitorDashboardActions,
  MonitorDashboardStatusActions
} from "../../../../store/actions"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../../../utils/alerts"
import { useLocation } from "react-router-dom"
import {
  MONITOR_STATUS, RISKSTATUS,
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
} from '../../../../utils/constants'
import { connect } from "react-redux"

function ContentViolation(props) {
  const {
    MonitorDashboardData,
    MonitorDashboardIData,

    MonitorDashboardStatusData,
    monitorDashboardStatusDispatch,
    MonitorDashboardDispatch,
    clearMonitorDashboardStatusDispatch
  } = props


  const pathName = useLocation().pathname
  const url = pathName && pathName.split("update/")
  const id = url && url[1]
  const [statusData, setStatusData] = useState()
  const [idValue, setidValue] = useState({})

  const Header = [
    "Return Policy",
    "Terms and Condition",
    "Privacy Policy",
    "Contact Us",
    "Shipping Policy"
  ]

  const CurrentScandata = MonitorDashboardData && MonitorDashboardData.data && MonitorDashboardData.data.current_scan ? MonitorDashboardData.data.current_scan : "--"
  const LastScanData = MonitorDashboardData && MonitorDashboardData.data && MonitorDashboardData.data.base_Scan ? MonitorDashboardData.data.base_Scan : "--"
  const currentId = CurrentScandata && CurrentScandata._id
  useEffect(() => {
    setidValue(currentId)
  }, [currentId])


  const onConfirmApprove = (Value) => {
    const params = {
      id: idValue,
      status: "ACCEPTED",
      reportKey: Value
    }
    monitorDashboardStatusDispatch(params)
  }


  const approveSubmit = (name) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.APPROVE,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmApprove(name)
      },
      () => { }
    )
  }

  const onConfirmReject = (name) => {
    const params = {
      id: idValue,
      status: "REJECTED",
      reportKey: name
    }
    monitorDashboardStatusDispatch(params)
  }

  const rejectSubmit = (name) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.REJECT,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmReject(name)
      },
      () => { }
    )
  }

  useEffect(() => {
    if (MonitorDashboardStatusData && MonitorDashboardStatusData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        MonitorDashboardStatusData && MonitorDashboardStatusData.message,
        'success'
      )
      MonitorDashboardDispatch(id)
      clearMonitorDashboardStatusDispatch()
    } else if (MonitorDashboardStatusData && MonitorDashboardStatusData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        MonitorDashboardStatusData && MonitorDashboardStatusData.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearMonitorDashboardStatusDispatch()
    }
  }, [MonitorDashboardStatusData])

  return (
    <>

      <div>
        <div className="card card-xl-stretch">
          <h2 className="ms-4 mt-4 mb-8">URL Violation </h2>
          <i className="fa fa-times-circle-o" aria-hidden="true"></i>
        </div>
        <div className="card card-xl-stretch">
          <div className="card-body pt-0 mb-4">

            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr className="fw-boldest fs-4 text-white text-center"
                    style={{
                      backgroundColor: '#5151b9'
                    }}
                  >
                    <th>
                      Parameter
                    </th>
                    <th>
                      Base Line
                    </th>
                    <th>
                      Current Scan  {
                        CurrentScandata && CurrentScandata.ogm_run_date ?
                          moment(
                            CurrentScandata && CurrentScandata.ogm_run_date ? CurrentScandata.ogm_run_date : "--"
                          ).format("MMM Do YYYY")
                          : ''
                      }
                    </th>
                    <th>
                      Change Detected
                    </th>
                    <th>
                      Accept/Reject
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="text-gray-700 fw-bold">
                      {
                        _.isArray(Header) ? (
                          Header.map((item) => {
                            return (
                              <div className="p-10 font14 h-100px">
                                {
                                  item
                                }<br />
                              </div>
                            )
                          })
                        ) : null
                      }
                    </td>
                    <td>
                      <>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.returnPolicyPageUrl && LastScanData.returnPolicyPageUrl.value ? LastScanData.returnPolicyPageUrl.value : "--", "_blank")}
                          >
                            {
                              LastScanData.returnPolicyPageUrl && LastScanData.returnPolicyPageUrl.value ? LastScanData.returnPolicyPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.termsAndConditionPageUrl && LastScanData.termsAndConditionPageUrl.value ? LastScanData.termsAndConditionPageUrl.value : "--", "_blank")}
                          >
                            {
                              LastScanData.termsAndConditionPageUrl && LastScanData.termsAndConditionPageUrl.value ? LastScanData.termsAndConditionPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.privacyPolicyPageUrl && LastScanData.privacyPolicyPageUrl.value ? LastScanData.privacyPolicyPageUrl.value : "--", "_blank")}
                          >
                            {
                              LastScanData.privacyPolicyPageUrl && LastScanData.privacyPolicyPageUrl.value ? LastScanData.privacyPolicyPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.contactUsPageUrl && LastScanData.contactUsPageUrl.value ? LastScanData.contactUsPageUrl.value : "--", "_blank")}
                          >
                            {
                              LastScanData.contactUsPageUrl && LastScanData.contactUsPageUrl.value ? LastScanData.contactUsPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.shippingPolicyPageUrl && LastScanData.shippingPolicyPageUrl.value ? LastScanData.shippingPolicyPageUrl.value : "--", "_blank")}
                          >
                            {
                              LastScanData.shippingPolicyPageUrl && LastScanData.shippingPolicyPageUrl.value ? LastScanData.shippingPolicyPageUrl.value : "--"
                            }
                          </a>
                        </div>
                      </>
                    </td>
                    <td>
                      <>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.value ? CurrentScandata.returnPolicyPageUrl.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.value ? CurrentScandata.returnPolicyPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.value ? CurrentScandata.termsAndConditionPageUrl.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.value ? CurrentScandata.termsAndConditionPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.value ? CurrentScandata.privacyPolicyPageUrl.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.value ? CurrentScandata.privacyPolicyPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.value ? CurrentScandata.contactUsPageUrl.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.value ? CurrentScandata.contactUsPageUrl.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.value ? CurrentScandata.shippingPolicyPageUrl.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.value ? CurrentScandata.shippingPolicyPageUrl.value : "--"
                            }
                          </a>
                        </div>
                      </>
                    </td>
                    <td className="text-center">
                      <>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                      </>
                    </td>
                    <td className="text-center">
                      <>

                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("returnPolicyPageUrl")
                          }}
                        >
                          {
                            CurrentScandata.returnPolicyPageUrl ?
                              CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("returnPolicyPageUrl")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("returnPolicyPageUrl")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.status]}`}
                                      >
                                        {
                                          CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("termsAndConditionPageUrl")
                          }}
                        >
                          {
                            CurrentScandata.termsAndConditionPageUrl ?
                              CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("termsAndConditionPageUrl")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("termsAndConditionPageUrl")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.status]}`}
                                      >
                                        {
                                          CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("privacyPolicyPageUrl")
                          }}
                        >
                          {
                            CurrentScandata.privacyPolicyPageUrl ?
                              CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("privacyPolicyPageUrl")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("privacyPolicyPageUrl")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.status]}`}
                                      >
                                        {
                                          CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("contactUsPageUrl")
                          }}
                        >
                          {
                            CurrentScandata.contactUsPageUrl ?
                              CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("contactUsPageUrl")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("contactUsPageUrl")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.status]}`}
                                      >
                                        {
                                          CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("shippingPolicyPageUrl")
                          }}
                        >
                          {
                            CurrentScandata.shippingPolicyPageUrl ?
                              CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("shippingPolicyPageUrl")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("shippingPolicyPageUrl")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.status]}`}
                                      >
                                        {
                                          CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                      </>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


      <div>
        <div className="card card-xl-stretch">
          <h2 className="ms-4 mt-4 mb-8">Content Violation </h2>
          <i className="fa fa-times-circle-o" aria-hidden="true"></i>
        </div>
        <div className="card card-xl-stretch">
          <div className="card-body pt-0 mb-4">

            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr className="fw-boldest fs-4 text-white text-center"
                    style={{
                      backgroundColor: '#5151b9'
                    }}
                  >
                    <th>
                      Parameter
                    </th>
                    <th>
                      Base Line
                    </th>
                    <th>
                      Current Scan  {
                        CurrentScandata && CurrentScandata.ogm_run_date ?
                          moment(
                            CurrentScandata && CurrentScandata.ogm_run_date ? CurrentScandata.ogm_run_date : "--"
                          ).format("MMM Do YYYY")
                          : ''
                      }
                    </th>
                    <th>
                      Change Detected
                    </th>
                    <th>
                      Accept/Reject
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="text-gray-700 fw-bold">
                      {
                        _.isArray(Header) ? (
                          Header.map((item) => {
                            return (
                              <div className="p-10 font14 h-100px">
                                {
                                  item
                                }<br />
                              </div>
                            )
                          })
                        ) : null
                      }
                    </td>
                    <td>
                      <>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.returnPolicyContentStatus && LastScanData.returnPolicyContentStatus.value ? LastScanData.returnPolicyContentStatus.value : "--", "_blank")}
                          >
                            {
                              LastScanData.returnPolicyContentStatus && LastScanData.returnPolicyContentStatus.value ? LastScanData.returnPolicyContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.termsAndConditionContentStatus && LastScanData.termsAndConditionContentStatus.value ? LastScanData.termsAndConditionContentStatus.value : "--", "_blank")}
                          >
                            {
                              LastScanData.termsAndConditionContentStatus && LastScanData.termsAndConditionContentStatus.value ? LastScanData.termsAndConditionContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.privacyPolicyContentStatus && LastScanData.privacyPolicyContentStatus.value ? LastScanData.privacyPolicyContentStatus.value : "--", "_blank")}
                          >
                            {
                              LastScanData.privacyPolicyContentStatus && LastScanData.privacyPolicyContentStatus.value ? LastScanData.privacyPolicyContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.contactUsContentStatus && LastScanData.contactUsContentStatus.value ? LastScanData.contactUsContentStatus.value : "--", "_blank")}
                          >
                            {
                              LastScanData.contactUsContentStatus && LastScanData.contactUsContentStatus.value ? LastScanData.contactUsContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(LastScanData.shippingPolicyContentStatus && LastScanData.shippingPolicyContentStatus.value ? LastScanData.shippingPolicyContentStatus.value : "--", "_blank")}
                          >
                            {
                              LastScanData.shippingPolicyContentStatus && LastScanData.shippingPolicyContentStatus.value ? LastScanData.shippingPolicyContentStatus.value : "--"
                            }
                          </a>
                        </div>
                      </>
                    </td>
                    <td>
                      <>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.value ? CurrentScandata.returnPolicyContentStatus.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.value ? CurrentScandata.returnPolicyContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.value ? CurrentScandata.termsAndConditionContentStatus.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.value ? CurrentScandata.termsAndConditionContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.value ? CurrentScandata.privacyPolicyContentStatus.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.value ? CurrentScandata.privacyPolicyContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.value ? CurrentScandata.contactUsContentStatus.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.value ? CurrentScandata.contactUsContentStatus.value : "--"
                            }
                          </a>
                        </div>
                        <div className="p-10 ellipsisDashboard">
                          <a
                            className='color-primary cursor-pointer'
                            onClick={() => window.open(CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.value ? CurrentScandata.shippingPolicyContentStatus.value : "--", "_blank")}
                          >
                            {
                              CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.value ? CurrentScandata.shippingPolicyContentStatus.value : "--"
                            }
                          </a>
                        </div>
                      </>
                    </td>
                    <td className="text-center">
                      <>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                        <div className="p-10 h-100px">
                          {
                            CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.changeDetected === 'YES' ?
                              (
                                <i className={`${MONITOR_STATUS[CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.changeDetected]}`}
                                />
                              ) : '--'
                          }
                        </div>
                      </>
                    </td>
                    <td className="text-center">
                      <>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("returnPolicyContentStatus")
                          }}
                        >
                          {
                            CurrentScandata.returnPolicyContentStatus ?
                              CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("returnPolicyContentStatus")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("returnPolicyContentStatus")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.status]}`}
                                      >
                                        {
                                          CurrentScandata.returnPolicyContentStatus && CurrentScandata.returnPolicyContentStatus.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("termsAndConditionContentStatus")
                          }}
                        >
                          {
                            CurrentScandata.termsAndConditionContentStatus ?
                              CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("termsAndConditionContentStatus")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("termsAndConditionContentStatus")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.status]}`}
                                      >
                                        {
                                          CurrentScandata.termsAndConditionContentStatus && CurrentScandata.termsAndConditionContentStatus.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("privacyPolicyContentStatus")
                          }}
                        >
                          {
                            CurrentScandata.privacyPolicyContentStatus ?
                              CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("privacyPolicyContentStatus")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("privacyPolicyContentStatus")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.status]}`}
                                      >
                                        {
                                          CurrentScandata.privacyPolicyContentStatus && CurrentScandata.privacyPolicyContentStatus.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("contactUsContentStatus")
                          }}
                        >
                          {
                            CurrentScandata.contactUsContentStatus ?
                              CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("contactUsContentStatus")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("contactUsContentStatus")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.status]}`}
                                      >
                                        {
                                          CurrentScandata.contactUsContentStatus && CurrentScandata.contactUsContentStatus.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                        <div
                          className='w-250px h-100px p-10'
                          onClick={() => {
                            setStatusData("shippingPolicyContentStatus")
                          }}
                        >
                          {
                            CurrentScandata.shippingPolicyContentStatus ?
                              CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.changeDetected === 'YES' ?
                                (
                                  <ul className="nav">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          approveSubmit("shippingPolicyContentStatus")
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          rejectSubmit("shippingPolicyContentStatus")
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </ul>
                                ) : (
                                  CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.status === "PENDING" ? '--' :
                                    (
                                      <span className={`badge ${RISKSTATUS[
                                        CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.status]}`}
                                      >
                                        {
                                          CurrentScandata.shippingPolicyContentStatus && CurrentScandata.shippingPolicyContentStatus.status
                                        }
                                      </span>
                                    )
                                ) : '--'
                          }
                        </div>
                      </>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = (state) => {
  const {
    MonitorDashboardStatusStore
  } = state
  return {
    MonitorDashboardStatusData: MonitorDashboardStatusStore && MonitorDashboardStatusStore.MonitorDashboardStatusData ? MonitorDashboardStatusStore.MonitorDashboardStatusData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  MonitorDashboardDispatch: (id) => dispatch(MonitorDashboardActions.getMonitorDashboard(id)),
  monitorDashboardStatusDispatch: (params) => dispatch(MonitorDashboardStatusActions.getMonitorDashboardStatus(params)),
  clearMonitorDashboardStatusDispatch: (params) => dispatch(MonitorDashboardStatusActions.clearMonitorDashboardStatus(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(ContentViolation)