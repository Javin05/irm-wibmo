import React, { Fragment, useEffect, useState } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import moment from 'moment'
// import PDF from '../pdf/Pdf'
import jsPDF from "jspdf"
import 'react-circular-progressbar/dist/styles.css'
import { renderToString } from "react-dom/server"
import ReactSpeedometer from "react-d3-speedometer"
import { connect } from 'react-redux'
import { TagSummaryAction, ExportListActions } from '../../../../store/actions'
import { getLocalStorage, removeLocalStorage } from '../../../../utils/helper'
import { warningAlert } from "../../../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import autoTable from 'jspdf-autotable'
import ReactPDF from '@react-pdf/renderer';
import { pdf } from "@react-pdf/renderer";
import { saveAs } from 'file-saver';
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';

function Score(props) {
  const {
    websiteLink,
    getExportDispatch,
    exportLoading,
    exportLists,
    clearExportListDispatch,

    NameData,
    website
  } = props





  const print = () => {
    const string = renderToString(
      // <PDF
      // />
      )
    const pdf = new jsPDF("p", "mm", "a1")
    let elementHandler = {
      '#ignorePDF': function (element, renderer) {
        return true
      }
    }
    let source = window.document.getElementById("pdf_converter")
    if (string) {
      // pdf.html(string).then(() => pdf.save(`${BsName}${id}.pdf`))
    }
    // if (source) {
    //   pdf.html(source).then(() => pdf.save('website.pdf'))
    // }
  }


  const clear = () => {
    clearExportListDispatch()
  }

  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === 'ok') {
      if (!_.isEmpty(exportLists && exportLists.data && exportLists.data.data)) {
        const closeXlsx = document.getElementById('bulkCsvReport')
        closeXlsx.click()
        clearExportListDispatch()
      } else if (_.isEmpty(exportLists && exportLists.data && exportLists.data.data)) {
        warningAlert(
          'error',
          exportLists && exportLists.data && exportLists.data.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearExportListDispatch()
      }
    }
  }, [exportLists])

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="table-to-xls">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Web Url</th>
              <th>Level 1 Status</th>
              <th>Acquirer</th>
              <th>Level 1 Reason</th>
              <th>Code</th>
              <th>Level 2 Status</th>
              <th>Level 2 Reason</th>
              <th>Website Working?</th>
              <th>Legal Name</th>
              <th>Malware Present</th>
              <th>Malware Risk</th>
              <th>Domain Registered</th>
              <th>Domain Registration Company</th>
              <th>Domain Registration Date</th>
              <th>Domain Registration Expiry Date</th>
              {/* <th>Website Traffic</th> */}
              <th>SSL Certificate Check</th>
              <th>Adult Content Monitoring</th>
              {/* <th>Product Category</th> */}
              <th>Merchant Policy Link Work</th>
              <th>Negative Keywords</th>
              <th>Readiness</th>
              <th>Transparency</th>
              <th>Contact Details Phone</th>
              <th>Contact Details Email</th>
              <th>Purchase Or Registration</th>
              <th>Merchant Intelligence</th>
              <th>Logo</th>
              <th>IP Address On Server</th>
              <th>Risk Score</th>
            </tr>
          </thead>
          <tbody>
            {
              !_.isEmpty(exportLists && exportLists.data && exportLists.data.data) ?
                exportLists && exportLists.data && exportLists.data.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      <td>
                        {item.webUrl}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.level1Status}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.acquirer}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.level1Reason}
                      </td>
                      <td>
                        {item && item.productCategoryCode}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.level2Status}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.level2Reason}
                      </td>
                      <td>
                        {item.websiteWorking}
                      </td>
                      <td>
                        {item.legalName}
                      </td>
                      <td>
                        {item.malwarePresent}
                      </td>
                      <td>
                        {item.malwareRisk}
                      </td>
                      <td>
                        {item.domainRegistered}
                      </td>
                      <td>
                        {item.domainRegistrationCompany}
                      </td>
                      <td>
                        {item.domainRegistrationDate}
                      </td>
                      <td>
                        {item.domainRegistrationExpiryDate}
                      </td>
                      {/* <td>
                          {item.websiteTraffic}
                        </td> */}
                      <td>
                        {item.sslCertificateCheck}
                      </td>
                      <td>
                        {item.adultContentMonitoring}
                      </td>
                      {/* <td>
                          {
                            !_.isEmpty(item && item.productCategory) ? (
                              item && item.productCategory.map((product, pr) => {
                                return (
                                  <div key={pr}>
                                    <span>
                                      {
                                        product
                                      }
                                    </span>
                                  </div>
                                )
                              })
                            ) : 'No Data'
                          }
                        </td> */}
                      <td>
                        {item.merchantPolicyLinkWork}
                      </td>
                      <td>
                        {item.negativeKeywords}
                      </td>
                      <td>
                        {item.readiness}
                      </td>
                      <td>
                        {item.transparency}
                      </td>
                      <td>
                        {/* {
                          !_.isEmpty(item && item.contactDetailsPhone) ? (
                            item && item.contactDetailsPhone.map((phone, p) => {
                              return (
                                <div key={p}>
                                  <span>
                                    {
                                      phone
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : 'No Data'
                        } */}
                      </td>
                      <td>
                        {
                          !_.isEmpty(item && item.contactDetailsEmail) ? (
                            item && item.contactDetailsEmail.map((email, e) => {
                              return (
                                <div key={e}>
                                  <span>
                                    {
                                      email
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : 'No Data'
                        }
                      </td>
                      <td>
                        {item.purchaseOrRegistration}
                      </td>
                      <td>
                        {item.merchantIntelligence}
                      </td>
                      <td>
                        <span>
                          {item.logo}
                        </span>
                      </td>
                      <td>
                        {
                          !_.isEmpty(item && item.ipAddressOfServer) ? (
                            item && item.ipAddressOfServer.map((ipAddress, Ip) => {
                              return (
                                <div key={Ip}>
                                  <span>
                                    {
                                      ipAddress
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : 'No Data'
                        }
                      </td>
                      <td>
                        {
                          item.riskScore === 'No Data' ? 'No Data' :
                            parseFloat(item.riskScore).toFixed(2)
                        }
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      {/* 
      <PDF
        websiteData={websiteData}
        successVerifyDomain={successVerifyDomain}
        DNSData={DNSData}
        matrixDetail={matrixDetail}
        adminContact={adminContact}
        adminContactData={adminContactData}
        BsName={BsName}
        websitetobusinessmatch={websitetobusinessmatch}
        websiteCatgories={websiteCatgories}
        ValidData={ValidData}
        webAnalysis={webAnalysis}
        LogoCheck={LogoCheck}
        websiteImageDetect={websiteImageDetect}
        userInteractionChecks={userInteractionChecks}
        domainInfoData={domainInfoData}
        domainRepetation={domainRepetation}
        registerData={registerData}
        successWhoDomain={successWhoDomain}
        websiteCategorizationV1={websiteCategorizationV1}
        reviewAnalysis={reviewAnalysis}
        websiteLink={websiteLink}
        merchantIddetails={merchantIddetails}
      /> */}


      <div className='row g-5 g-xl-8'>
        <div className='col-lg-12'>
          <div className="card card-xl-stretch">
            <div className="card-body pt-0">
              <div className='row mb-12'>
                <div className='col-lg-4'>
                  <span className='d-flex justify-content-center mt-4 fw-boldest my-1 fs-3'>
                    Website
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer'
                      // onClick={() => window.open(websiteLink)}
                    >- {website ? website : '--'}</a>
                  </span>
                </div>
                <div className='col-lg-4'>
                  <div className='d-flex justify-content-start mt-4 fw-boldest my-1 fs-3'>
                    Company Name
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer text-capital'
                    >{NameData ? NameData : '--'}</a>
                  </div>
                </div>
                <div className='col-lg-3'>
                  <div className='mt-2 d-flex'>
                    <button
                      onClick={print}
                      className='btn btn-outline btn-outline-dashed btn-outline-default w-100 me-2'
                      disabled={exportLoading}
                    >
                      {!exportLoading &&
                        <span className='indicator-label'>
                          <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                          PDF Report
                        </span>
                      }
                      {exportLoading && (
                        <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { exportlistStore } = state;
  return {
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Score)