import { useEffect, useState, useRef, Fragment } from 'react'
import Xarrow, { useXarrow } from 'react-xarrows';
import Icofont from 'react-icofont';
import { useLocation } from 'react-router-dom'
import { OGMlinkAnalyticsActions } from '../../../../store/actions'
import { connect } from 'react-redux'
import { Scrollbars } from 'react-custom-scrollbars';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'
import { toAbsoluteUrl } from '../../../../theme/helpers'

const MainColor = {
  "Contact Details Phone": "#009EF7",
  "IP Address": "#F1416C",
  "Contact Details Email": "#50CD89",
  "Legal Name": "#7239EA", 
  "Address Details": "#FFC700"
}
const SubColor = {
  "Contact Details Phone": "#D3EFFF",
  "IP Address": "#F9DBE4",
  "Contact Details Email": "#BFFDDC",
  "Legal Name": "#E1D7F9",
  "Address Details": "#FFF0B6"
}
const Icon = {
  "Contact Details Phone": "icofont-ui-touch-phone",
  "IP Address": "icofont-eclipse",
  "Contact Details Email": "icofont-email",
  "Legal Name": "icofont-web",
  "Address Details": "icofont-computer"
}

function NetworkGraph(props) {
  const {
    LinkAnlyticslists,
    getOGMlinkAnalyticslistDispatch,
    merchant
  } = props;

  const boxRef = useRef(null);
  const [path, pathType] = useState("straight"); 
  const [highlight, setHighlight] = useState(null);
  const [activePop, setActivePop] = useState(null);

  const url = useLocation().pathname
  const fields = url && url.split('/')
  const id = fields && fields[3]

  const HighlightHandler = (lighter) => {
    setHighlight(lighter);
  }
  const HoverToggle = (dummy, toggle) => {
    if (toggle) {
      setActivePop(dummy)
    }
    else {
      setActivePop(null)
    }
  }

  useEffect(() => {
    getOGMlinkAnalyticslistDispatch(id)
  }, []);

const merchantData = merchant && merchant.report

  return (
    <div className='row'>
      <div className='col-lg-12 col-md-12 col-sm-12'>
        <Scrollbars style={{ width: "100%", height: 600 }}>
          <div className='graph-row'>
            <div className='column'>
              <span className='primary'>OGM-{merchant && merchant.ogm_Id ? merchant.ogm_Id : '--'}</span>
              <div ref={boxRef} className="box main" id="main" style={{ backgroundColor: highlight !== null ? MainColor[highlight] : "#E1F0FF" }}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/profile.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>
            </div>
            <div className='column mid-column'>
              <span style={{ color: MainColor["Contact Details Phone"] }}>Contact Details Phone : {merchantData && merchantData.contactDetailsPhone_currentScan ? merchantData.contactDetailsPhone_currentScan : '--'}</span>
              <div className="box"style={{ backgroundColor: highlight === "Contact Details Phone" ? MainColor["Contact Details Phone"] : SubColor["Contact Details Phone"] }} id="Contact Details Phone" onClick={(e) => HighlightHandler("Contact Details Phone")}
              >
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/telephone.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Contact Details Email"] }}>Contact Details Email : {merchantData && merchantData.contactDetailsEmail_currentScan ? merchantData.contactDetailsEmail_currentScan : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Contact Details Email" ? MainColor["Contact Details Email"] : SubColor["Contact Details Email"] }} id="Contact Details Email" onClick={(e) => HighlightHandler("Contact Details Email")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/mail.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Legal Name"] }}>legalName : {merchantData && merchantData.changeInLegalName_currentScan ? merchantData.changeInLegalName_currentScan : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Legal Name" ? MainColor["Legal Name"] : SubColor["Legal Name"] }} id="Legal Name" onClick={(e) => HighlightHandler("Legal Name")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/user.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Address Details"] }}>Address Details : {merchantData && merchantData.changeInMerchantAddress_currentScan ? merchantData.changeInMerchantAddress_currentScan : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Address Details" ? MainColor["Address Details"] : SubColor["Address Details"] }} id="Address Details" onClick={(e) => HighlightHandler("Address Details")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/house.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <Xarrow start={'main'} end={'Contact Details Phone'} path={path} color={highlight === "Contact Details Phone" ? MainColor["Contact Details Phone"] : SubColor["Contact Details Phone"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Contact Details Phone") }} />
              <Xarrow start={'main'} end={'Contact Details Email'} path={path} color={highlight === "Contact Details Email" ? MainColor["Contact Details Email"] : SubColor["Contact Details Email"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Contact Details Email") }} />
              <Xarrow start={'main'} end={'Legal Name'} path={path} color={highlight === "Legal Name" ? MainColor["Legal Name"] : SubColor["Legal Name"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Legal Name") }} />
              <Xarrow start={'main'} end={'Address Details'} path={path} color={highlight === "Address Details" ? MainColor["Address Details"] : SubColor["Address Details"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Address Details") }} />
            </div>
            <div className='column mt-4'>

              {LinkAnlyticslists && LinkAnlyticslists?.data && LinkAnlyticslists?.data.linkData && LinkAnlyticslists.data.linkData.map((dummy, index) => {

                return (
                  <Fragment key={"Z_" + index}>
                    <a
                      className='color-primary cursor-pointer'
                      onClick={() => window.open(`/risk-summary/update/${dummy._id}`, "_blank")}
                      data-bs-toggle="tooltip"
                      data-bs-custom-class="tooltip-inverse"
                      data-bs-placement="top"
                      title="Tooltip on top">
                      IRM{dummy.ogm_Id}
                    </a>
                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                      className='tooltip'
                    >
                      Email - {dummy && dummy.contactDetailsEmail_currentScan} <br />
                      Source - {dummy && dummy.source.map((name) => { return (<>{name}</>) })}
                    </Tooltip>}
                      placement={"right"}
                    >
                      <div
                        key={"X_" + index}
                        className="box"
                        id={"XYZ_" + index}
                        style={{
                          backgroundColor: 'rgb(237 184 23)'
                        }}
                      >
                        <span>
                          <Icofont
                            icon={"student-alt"}
                            style={{
                              color: 'black'
                            }}
                          />
                        </span>
                      </div>
                    </OverlayTrigger>
                    {
                      dummy && dummy.source.map((item, i) => {
                        return (
                          <>
                            <Xarrow
                              key={"Y_" + index}
                              start={item}
                              end={"XYZ_" + index}
                              path={path}
                              color={highlight === item ? MainColor[item] : SubColor[item]}
                              headSize={4}
                              strokeWidth={2}
                              dashness={highlight === item ? true : false
                              } />
                          </>
                        )
                      })
                    }
                  </Fragment>
                )
              })}
            </div>
          </div>
        </Scrollbars>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  const { OGMlinkAnalyticsStore } = state;
  return {
    LinkAnlyticslists: OGMlinkAnalyticsStore && OGMlinkAnalyticsStore.OGMlinkAnalyticslists ? OGMlinkAnalyticsStore.OGMlinkAnalyticslists : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  getOGMlinkAnalyticslistDispatch: (params) => dispatch(OGMlinkAnalyticsActions.getOGMlinkAnalyticslist(params))

});

export default connect(mapStateToProps, mapDispatchToProps)(NetworkGraph);