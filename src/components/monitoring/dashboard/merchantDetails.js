import React, { useEffect, useState, createContext } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { useLocation } from 'react-router-dom'
import { merchantIdDetailsActions, GetAsigneeActions, AsiggnActions } from '../../../store/actions'
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import _ from 'lodash'
import { getLocalStorage } from '../../../utils/helper'
import { RISKSTATUS } from '../../../utils/constants'
import ARH from '../ARH'
export const UserContext = createContext();

function MonitorIdDetails(props) {
  const {
    MonitorDashboardIData
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]

  return (
    <>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='row'>
          <div className='card-header border-0'>
            <div className='col-lg-6'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='d-flex align-items-center fw-boldest my-1 fs-2'>
                  OGM{MonitorDashboardIData && MonitorDashboardIData.ogm_Id} -
                  <a
                    className='text-hover-primary fs-2 ms-4 cursor-pointer'
                    onClick={() => window.open(MonitorDashboardIData.website)}
                  >{MonitorDashboardIData && MonitorDashboardIData.website ? MonitorDashboardIData.website : '--'}</a>
                </span>
              </h3>
            </div>
            <div className='col-lg-6'>
              <div className='mt-4 d-flex justify-content-end'>
                <ARH currentId={currentId}/>
              </div>
              <div className='mt-4 d-flex justify-content-end'>
                <span className={`badge ${RISKSTATUS[MonitorDashboardIData && MonitorDashboardIData.ogm_status]} badge-lg`}>
                  {MonitorDashboardIData && MonitorDashboardIData.ogm_status}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="separator separator-dashed my-3" />
        <div className='card-body pt-0'>
          <div className='row g-5 g-xl-8'>
            <div className='col-sm-6 col-md-6 col-lg-6'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-black fw-boldest  pl-3 ml-2'>
                        Company Name :
                      </div>
                    </div>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <a
                        className='fw-boldest text-hover-primary ms-4 cursor-pointer text-capital'
                      >
                        {MonitorDashboardIData && MonitorDashboardIData.legalNameScrapped}
                      </a>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2'>
                        Acquirer :
                      </div>
                    </div>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-700 fw-bold  pl-3'>
                        {MonitorDashboardIData && MonitorDashboardIData.acquirer}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2'>
                        Merchant Category :
                      </div>
                    </div>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-700 fw-bold  pl-3'>
                        {MonitorDashboardIData && MonitorDashboardIData.reason}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2'>
                        Mcc Code :
                      </div>
                    </div>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-700 fw-bold  pl-3'>
                        {MonitorDashboardIData && MonitorDashboardIData.mccCodeScrapped}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2'>
                        Risk Level :
                      </div>
                    </div>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-700 fw-bold  pl-3'>
                        {MonitorDashboardIData && MonitorDashboardIData.riskLevel}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2'>
                        Risk Classification :
                      </div>
                    </div>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-700 fw-bold  pl-3'>
                        {MonitorDashboardIData && MonitorDashboardIData.riskClassification}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-6 col-md-6 col-lg-6'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-gray-700 fw-bold ml-2 d-flex justify-content-end'>
                        Onboarded Date :
                      </div>
                    </div>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-700 fw-bold d-flex justify-content-start'>
                        {
                          !_.isEmpty(MonitorDashboardIData && MonitorDashboardIData.createdAt) ?
                            moment(
                              MonitorDashboardIData && MonitorDashboardIData.createdAt ? MonitorDashboardIData && MonitorDashboardIData.createdAt : "--"
                            ).format("MMM Do YYYY")
                            : '--'
                        }
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-gray-700 fw-bold ml-2 d-flex justify-content-end'>
                        Start Scan Date :
                      </div>
                    </div>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-700 fw-bold d-flex justify-content-start'>
                        {
                          !_.isEmpty(MonitorDashboardIData && MonitorDashboardIData.ogm_start_date) ?
                            moment(
                              MonitorDashboardIData && MonitorDashboardIData.ogm_start_date ? MonitorDashboardIData && MonitorDashboardIData.ogm_start_date : "--"
                            ).format("MMM Do YYYY")
                            : '--'
                        }
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-gray-700 fw-bold ml-2 d-flex justify-content-end'>
                        Last Scan Date :
                      </div>
                    </div>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-700 fw-bold d-flex justify-content-start'>
                        {
                          !_.isEmpty(MonitorDashboardIData && MonitorDashboardIData.ogm_last_run_date) ?
                            moment(
                              MonitorDashboardIData && MonitorDashboardIData.ogm_last_run_date ? MonitorDashboardIData && MonitorDashboardIData.ogm_last_run_date : "--"
                            ).format("MMM Do YYYY")
                            : '--'
                        }
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-sm-8 col-md-8 col-lg-8 mb-4'>
                      <div className='text-gray-700 fw-bold ml-2 d-flex justify-content-end'>
                        Next Scan Date :
                      </div>
                    </div>
                    <div className='col-sm-4 col-md-4 col-lg-4 mb-4'>
                      <div className='text-700 fw-bold d-flex justify-content-start'>
                        {
                          !_.isEmpty(MonitorDashboardIData && MonitorDashboardIData.ogm_next_run_date) ?
                            moment(
                              MonitorDashboardIData && MonitorDashboardIData.ogm_next_run_date ? MonitorDashboardIData && MonitorDashboardIData.ogm_next_run_date : "--"
                            ).format("MMM Do YYYY")
                            : '--'
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div >

    </>
  )
}

const mapStateToProps = (state) => {
  const { } = state
  return {
  }
}

const mapDispatchToProps = (dispatch) => ({
  clearAsiggnDispatch: () => dispatch(AsiggnActions.clearAsiggn())
})

export default connect(mapStateToProps, mapDispatchToProps)(MonitorIdDetails)
