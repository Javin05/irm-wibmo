import { USER_MANAGEMENT_ERROR, REGEX, BACKEND_DASHBOARD_ERROR } from "../../utils/constants";
import _ from 'lodash'

export const userValidation = (values, setErrors) => {
  const errors = {};
  if (!values.file) {
    errors.file = 'CSV File is required.'
  }
  setErrors(errors);
  return errors;
};

export const WebValidation = (values, setErrors) => {
  const errors = {};
  if (!values.file) {
    errors.file = 'Xls File is required.'
  }
  setErrors(errors);
  return errors;
};

export const manualValidation = (values, setError) => {
  const error = {};
  if (!values.website) {
    error.website = 'Website is required.'
  } else if (values.website && !REGEX.WEBSITE_URL.test(values.website)) {
    error.website = 'Website is InValid please add http or https'
  }
  // if (!values.tag) {
  //   error.tag = 'Tag is required.'
  // }
  setError(error);
  return error;
};

export const searchValidation = (values, setError) => {
  const error = {};
  // if (!values.website) {
  //   error.website = 'Website is required.'
  // } else if (values.website && !REGEX.WEBSITE_URL.test(values.website)) {
  //   error.website = 'Website is InValid'
  // }
  // if (values.website && !REGEX.WEBSITE_URL.test(values.website.toLocaleLowerCase())) {
  //   error.website = 'Website is InValid'
  // }
  if (!values.tag) {
    error.tag = 'Tag is required.'
  }
  setError(error);
  return error;
};

export const backendDashboardEditValidation = (values, setErrors) => {
  const errors = {}
  if (!values.backendDelay) {
    errors.backendDelay = BACKEND_DASHBOARD_ERROR.BACKEND_DELAY_REQUIRED
  }
  if (!values.backendendNextExecutionDate) {
    errors.backendendNextExecutionDate = BACKEND_DASHBOARD_ERROR.NEXT_EXECUTION_DATE_REQUIRED
  }
  if (!values.lastExecutionDate) {
    errors.lastExecutionDate = BACKEND_DASHBOARD_ERROR.LAST_EXECUTION_DATE_REQUIRED
  }
  setErrors(errors)
  return errors
}
