import _ from 'lodash'

export const setBackendDashData = (data) => {
    if (!_.isEmpty(data)) {
        return {
            backendDelay: data.backendDelay ? data.backendDelay.toString() : "0",
            legalName: data.legalName,
            backendendNextExecutionDate: data && data.backendendNextExecutionDate ?  new Date(data.backendendNextExecutionDate): '',
            lastExecutionDate: data && data.backendLastUpdatedDate ? new Date(data.backendLastUpdatedDate): '',
            website: data.website,
            batchId: data.batchId
        }
    }
}