import React, { useState, useEffect } from "react"
import "bootstrap-icons/font/bootstrap-icons.css"
import { connect } from "react-redux"
import { KTSVG } from "../../theme/helpers"
import _ from "lodash"
import {
  getWebAnalysisActions,
  MonitorActions,
  ExportListActions,
  MonitorCountsActions,
  monitorExportReportActions
} from "../../store/actions"
import "./styles.css"
import Modal from "react-bootstrap/Modal"
import clsx from "clsx"
import { getLocalStorage } from "../../utils/helper"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { DateSelector } from '../../theme/layout/components/DateSelector'
import { DATE } from '../../utils/constants'
import moment from "moment"
import { SET_FILTER } from '../../utils/constants'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { warningAlert } from "../../utils/alerts"
import './styles.css'

function SearchList(props) {
  const {
    getMonitorlistDispatch,
    setSearchData,
    Value,
    activePageNumber,
    limit,
    clinetIdLists,
    setCredFilterParams,
    getMonitorCountsDispatch,
    getmonitorExportReportDispatch,
    monitorExportReportData,
    monitorExportloading,
    clearExportReportDispatch
  } = props
  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const clientShowId = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [show, setShow] = useState(false)
  const [exportShow, setExportShow] = useState(false)
  const [error, setError] = useState({})
  const [searchFormData, setSearchFormData] = useState({
    clientId: Role === 'Client User' ? ClinetId : '',
    website: "",
    status: "",
    ogm_start_date: "",
    ogm_last_run_date: "",
    ogm_next_run_date: "",
    ogm_stop_date: "",
    acquirer: '',
    changesDetected: '',
    batchId: '',
    monitoringInterval: '',
    from_date: '',
    to_date: '',
    callbackType:''
  })

  const handleChanges = (e) => {
    setSearchFormData((searchFormData) => ({
      ...searchFormData,
      [e.target.name]: e.target.value,
    }))
    setError({ ...error, [e.target.name]: "" })
  }

  const handleSearch = () => {
    const params = {
      ...searchFormData,
      ogm_start_date: _.isDate(searchFormData.ogm_start_date) ? moment(searchFormData.ogm_start_date).format("YYYY/MM/DD") : '',
      ogm_last_run_date: _.isDate(searchFormData.ogm_last_run_date) ? moment(searchFormData.ogm_last_run_date).format("YYYY/MM/DD") : '',
      ogm_next_run_date: _.isDate(searchFormData.ogm_next_run_date) ? moment(searchFormData.ogm_next_run_date).format("YYYY/MM/DD") : '',
      ogm_stop_date: _.isDate(searchFormData.ogm_stop_date) ? moment(searchFormData.ogm_stop_date).format("YYYY/MM/DD") : '',
      from_date: _.isDate(searchFormData.from_date) ? moment(searchFormData.from_date).format("YYYY/MM/DD") : '',
      to_date: _.isDate(searchFormData.to_date) ? moment(searchFormData.to_date).format("YYYY/MM/DD") : '',
      limit:limit,
      page:activePageNumber
    }
    setShow(false)
    getMonitorlistDispatch(params)
    getMonitorCountsDispatch(params)
    setSearchData(params)
    setExportShow(true)
  }

  const clearPopup = () => {
    setSearchFormData({
      website: "",
      status: "",
      ogm_start_date: "",
      ogm_last_run_date: "",
      ogm_next_run_date: "",
      ogm_stop_date: "",
      from_date: "",
      to_date: "",
      callbackType:""
    })
    setSelectedAsigneesOption('')
    setShow(false)
    setExportShow(false)
    const params = {
      limit:limit,
      page:activePageNumber
    }
    getMonitorlistDispatch(params)
    getMonitorCountsDispatch(params)
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setSearchFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const exportData = () => {
    const params = {
      ...searchFormData,
      ogm_start_date: _.isDate(searchFormData.ogm_start_date) ? moment(searchFormData.ogm_start_date).format("YYYY/MM/DD") : '',
      ogm_last_run_date: _.isDate(searchFormData.ogm_last_run_date) ? moment(searchFormData.ogm_last_run_date).format("YYYY/MM/DD") : '',
      ogm_next_run_date: _.isDate(searchFormData.ogm_next_run_date) ? moment(searchFormData.ogm_next_run_date).format("YYYY/MM/DD") : '',
      ogm_stop_date: _.isDate(searchFormData.ogm_stop_date) ? moment(searchFormData.ogm_stop_date).format("YYYY/MM/DD") : ''
    }
    getmonitorExportReportDispatch(params)
  }

  useEffect(() => {
    if (monitorExportReportData && monitorExportReportData.status === 'ok') {
      if (Array.isArray(monitorExportReportData && monitorExportReportData.data)) {
        const closeXlsx = document.getElementById('monitorExport')
        closeXlsx.click()
        clearExportReportDispatch()
      } else if (monitorExportReportData && monitorExportReportData.status === 'error') {
        warningAlert(
          'error',
          monitorExportReportData && monitorExportReportData.message,
          '',
          'Try again',
          '',
          () => { { } }
        )
        clearExportReportDispatch()
      }
    }
  }, [monitorExportReportData])

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-target='#monitorExport'
      >
        <ReactHTMLTableToExcel
          id="monitorExport"
          className="download-table-xls-button"
          table="categoryCsvModel"
          filename={`export-report`}
          sheet="tablexls"
        />
      </div>

      {/* csv Report */}
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="categoryCsvModel">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Website</th>
              <th>Acquirer</th>
              <th>Onboarding date</th>
              <th>Last scan date</th>
              <th>Next scan date</th>
              <th>Changes detected</th>
              <th>Domain Risk</th>
              <th>Website Working? - Base Scan</th>
              <th>Website Working? - Current Scan</th>
              <th>Line of Business - Base Scan</th>
              <th>Line of Business - Current Scan</th>
              <th>MCC - Base Scan</th>
              <th>MCC - Current Scan</th>
              <th>Risk Classification - Base Scan</th>
              <th>Risk Classification - Current Scan</th>
              <th>Website Success Rate - Base Scan</th>
              <th>Website Success Rate - Current Scan</th>
              <th>Legal Name - Base Scan</th>
              <th>Legal Name - Current Scan</th>
              <th>Website Redirection - Base Scan</th>
              <th>Website Redirection - Current Scan</th>
              <th>Merchant Address - Base Scan</th>
              <th>Merchant Address - Current Scan</th>
              <th>Page Loading Time - Base Scan</th>
              <th>Page Loading Time - Current Scan</th>
              <th>Return Policy URL Violation - Base Scan</th>
              <th>Return Policy URL Violation - Current Scan</th>
              <th>Terms and Condition URL Violation - Base Scan</th>
              <th>Terms and Condition URL Violation - Current Scan</th>
              <th>Privacy Policy URL Violation - Base Scan</th>
              <th>Privacy Policy URL Violation - Current Scan</th>
              <th>Contact Us URL Violation - Base Scan</th>
              <th>Contact Us URL Violation - Current Scan</th>
              <th>Shipping Policy URL Violation - Base Scan</th>
              <th>Shipping Policy URL Violation - Current Scan</th>
              <th>Parked Domain - Base Scan</th>
              <th>Parked Domain - Current Scan</th>
              <th>Domain Expiry Risk - Base Scan</th>
              <th>Domain Expiry Risk - Current Scan</th>
              <th>Non INR Pricing - Base Scan</th>
              <th>Non INR Pricing - Current Scan</th>
              <th>Online Reputation Drop - Base Scan</th>
              <th>Online Reputation Drop - Current Scan</th>
              <th>Heavy Discounts - Base Scan</th>
              <th>Heavy Discounts - Current Scan</th>
              <th>Contact Details - Phone - Base Scan</th>
              <th>Contact Details - Phone - Current Scan</th>
              <th>Contact Details - Valid Phone - Base Scan</th>
              <th>Contact Details - Valid Phone - Current Scan</th>
              <th>Contact Details - Email - Base Scan</th>
              <th>Contact Details - Email - Current Scan</th>
              <th>Contact Details - Valid Email - Base Scan</th>
              <th>Contact Details - Valid Email - Current Scan</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(monitorExportReportData && monitorExportReportData.data) ?
                monitorExportReportData && monitorExportReportData.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      <td>
                        {item.website ? item.website : 'No Data'}
                      </td>
                      <td>
                        {item && item.acquirer ? item.acquirer : 'No Data'}
                      </td>
                      <td>
                        {item && item.ogm_start_date ? item.ogm_start_date : 'No Data'}
                      </td>
                      <td>
                        {item && item.ogm_last_run_date ? item.ogm_last_run_date : 'No Data'}
                      </td>
                      <td>
                        {item && item.ogm_next_run_date ? item.ogm_next_run_date : 'No Data'}
                      </td>
                      <td>
                        {item && item.changesDetectedList ? item.changesDetectedList : 'No Data'}
                      </td>
                      <td>
                        {item && item.domainExpiryRisk ? item.domainExpiryRisk : 'No Data'}
                      </td>
                      <td>
                        {item && item.websiteWorking_baseScan ? item.websiteWorking_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.websiteWorking_currentScan ? item.websiteWorking_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInLineOfBusiness_baseScan ? item.changeInLineOfBusiness_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInLineOfBusiness_currentScan ? item.changeInLineOfBusiness_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInMcc_baseScan ? item.changeInMcc_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInMcc_currentScan ? item.changeInMcc_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.riskClassification_baseScan ? item.riskClassification_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.riskClassification_currentScan ? item.riskClassification_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.websiteSuccessRate_baseScan ? item.websiteSuccessRate_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.websiteSuccessRate_currentScan ? item.websiteSuccessRate_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInLegalName_baseScan ? item.changeInLegalName_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInLegalName_currentScan ? item.changeInLegalName_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.transactionLaunderingRedirection_baseScan ? item.transactionLaunderingRedirection_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.transactionLaunderingRedirection_currentScan ? item.transactionLaunderingRedirection_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInMerchantAddress_baseScan ? item.changeInMerchantAddress_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.changeInMerchantAddress_currentScan ? item.changeInMerchantAddress_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.increaseInFullPageLoadingTime_baseScan ? item.increaseInFullPageLoadingTime_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.increaseInFullPageLoadingTime_currentScan ? item.increaseInFullPageLoadingTime_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.returnPolicyPageUrl_baseScan ? item.returnPolicyPageUrl_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.returnPolicyPageUrl_currentScan ? item.returnPolicyPageUrl_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.termsAndConditionPageUrl_baseScan ? item.termsAndConditionPageUrl_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.termsAndConditionPageUrl_currentScan ? item.termsAndConditionPageUrl_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.privacyPolicyPageUrl_baseScan ? item.privacyPolicyPageUrl_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.privacyPolicyPageUrl_currentScan ? item.privacyPolicyPageUrl_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.contactUsPageUrl_baseScan ? item.contactUsPageUrl_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.contactUsPageUrl_currentScan ? item.contactUsPageUrl_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.shippingPolicyPageUrl_baseScan ? item.shippingPolicyPageUrl_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.shippingPolicyPageUrl_currentScan ? item.shippingPolicyPageUrl_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.parkedDomain_baseScan ? item.parkedDomain_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.parkedDomain_currentScan ? item.parkedDomain_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.domainRisk_baseScan ? item.domainRisk_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.domainRisk_currentScan ? item.domainRisk_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.nonInrPricing_baseScan ? item.nonInrPricing_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.nonInrPricing_currentScan ? item.nonInrPricing_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.onlineReputationDrop_baseScan ? item.onlineReputationDrop_baseScan : '0'}
                      </td>
                      <td>
                        {item && item.onlineReputationDrop_currentScan ? item.onlineReputationDrop_currentScan : '0'}
                      </td>
                      <td>
                        {item && item.heavyDiscounts_baseScan ? item.heavyDiscounts_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.heavyDiscounts_currentScan ? item.heavyDiscounts_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.contactDetailsPhone_baseScan ? item.contactDetailsPhone_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.contactDetailsPhone_currentScan ? item.contactDetailsPhone_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.containsValidPhone_baseScan ? item.containsValidPhone_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.containsValidPhone_currentScan ? item.containsValidPhone_currentScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.contactDetailsEmail_baseScan ? item.contactDetailsEmail_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.contactDetailsEmail_currentScan ? item.contactDetailsEmail_currentScan : 'No Data'}
                      </td>

                      <td>
                        {item && item.containsValidEmail_baseScan ? item.containsValidEmail_baseScan : 'No Data'}
                      </td>
                      <td>
                        {item && item.containsValidEmail_currentScan ? item.containsValidEmail_currentScan : 'No Data'}
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      <div>
        <button
          type="button"
          className="btn btn-primary ml-1"
          onClick={() => {
            setShow(true)
          }}
        >
          {/* eslint-disable */}
          <KTSVG path="/media/icons/duotune/general/gen021.svg" />
          {/* eslint-disable */}
          Search
        </button>
        {
          exportShow ? (
            <button
              type="button"
              className="btn btn-light-primary ml-1 ms-2"
              onClick={() => {
                exportData()
              }}
              disabled={monitorExportloading}
            >
              {!monitorExportloading &&
                <span className='indicator-label'>
                  <i className="bi bi-filetype-csv" />
                  Export
                </span>
              }
              {monitorExportloading && (
                <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                  Please wait...
                  <span className='spinner-border spinner-border-sm align-middle ms-2' />
                </span>
              )}
            </button>
          ) : null
        }
      </div>

      <Modal show={show} size="lg" centered onHide={ clearPopup}>
        <Modal.Header
          style={{ backgroundColor: "rgb(126 126 219)" }}
          closeButton={ clearPopup}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            Search Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            {
              Role === 'Admin' ? (
                !clientShowId ?
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Client :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                        isDisabled={!AsigneesOption}
                      />
                      {error && error.client && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {error.client}
                        </div>
                      )}
                    </div>
                  </div>
                  : null
              ) : (
                null
              )
            }

            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Acquirer :
                </label>
              </div>
              <div className="col-md-8">
                <input
                  placeholder="Acquirer"
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    { "is-invalid": searchFormData.acquirer && error.acquirer },
                    {
                      "is-valid": searchFormData.acquirer && !error.acquirer,
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="acquirer"
                  autoComplete="off"
                  value={searchFormData.acquirer || ""}
                />
                {error && error.acquirer && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.acquirer}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Batch ID:
                </label>
              </div>
              <div className="col-md-8">
                <input
                  placeholder="Batch ID"
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    { "is-invalid": searchFormData.batchId && error.batchId },
                    {
                      "is-valid": searchFormData.batchId && !error.batchId,
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="batchId"
                  autoComplete="off"
                  value={searchFormData.batchId || ""}
                />
                {error && error.batchId && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.batchId}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Ogm Interval :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name='monitoringInterval'
                  className='form-select form-select-solid'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={searchFormData.monitoringInterval || ''}
                >
                  <option value=''>Select...</option>
                  <option value='WEEKLY'>WEEKLY</option>
                  <option value='BIWEEKLY'>BIWEEKLY</option>
                  <option value='MONTHLY'>MONTHLY</option>
                  <option value='QUARTERLY'>QUARTERLY</option>
                  <option value='ONE TIME'>ONE TIME</option>
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Website :
                </label>
              </div>
              <div className="col-md-8">
                <input
                  placeholder="Website"
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    { "is-invalid": searchFormData.website && error.website },
                    {
                      "is-valid": searchFormData.website && !error.website,
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="website"
                  autoComplete="off"
                  value={searchFormData.website || ""}
                />
                {error && error.website && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.website}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  OGM Start Date:
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='ogm_start_date'
                  placeholder='OGM Start Date'
                  className='form-control'
                  selected={searchFormData.ogm_start_date || ''}
                  onChange={(date) => {
                    setError({ ...error, ogm_start_date: '' })
                    setSearchFormData((values) => ({
                      ...values,
                      ogm_start_date: date
                    }))
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  // maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  OGM Current Run Date:
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='ogm_last_run_date'
                  placeholder='OGM Current Run Date'
                  className='form-control'
                  selected={searchFormData.ogm_last_run_date || ''}
                  onChange={(date) => {
                    setError({ ...error, ogm_last_run_date: '' })
                    setSearchFormData((values) => ({
                      ...values,
                      ogm_last_run_date: date
                    }))
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  // maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  OGM Next Run Date:
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='ogm_next_run_date'
                  placeholder='OGM Next Run Date'
                  className='form-control'
                  selected={searchFormData.ogm_next_run_date || ''}
                  onChange={(date) => {
                    setError({ ...error, ogm_next_run_date: '' })
                    setSearchFormData((values) => ({
                      ...values,
                      ogm_next_run_date: date
                    }))
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  // maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  OGM Stop Date:
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='ogm_stop_date'
                  placeholder='OGM Stop Date'
                  className='form-control'
                  selected={searchFormData.ogm_stop_date || ''}
                  onChange={(date) => {
                    setError({ ...error, ogm_stop_date: '' })
                    setSearchFormData((values) => ({
                      ...values,
                      ogm_stop_date: date
                    }))
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  // maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Status :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="status"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={searchFormData.status || ""}
                >
                  <option value="">Select...</option>
                  <option value="Accepted">Accepted</option>
                  <option value="Rejected">Rejected</option>
                  <option value="Review Pending">Review Pending</option>
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Change Detected :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="changesDetected"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={searchFormData.changesDetected || ""}
                >
                  <option value="">Select...</option>
                  <option value="YES">YES</option>
                  <option value="NO">NO</option>
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Callback Type :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="callbackType"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={searchFormData.callbackType || ""}
                >
                  <option value="">Select...</option>
                  <option value="NEW">NEW</option>
                  <option value="OLD">OLD</option>
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report From Date:
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='from_date'
                  placeholder='Report From Date'
                  className='form-control'
                  selected={searchFormData.from_date || ''}
                  onChange={(date) => {
                    setError({ ...error, from_date: '' })
                    setSearchFormData((values) => ({
                      ...values,
                      from_date: date
                    }))
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  // maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report To Date:
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='to_date'
                  placeholder='Report To Date'
                  className='form-control'
                  selected={searchFormData.to_date || ''}
                  onChange={(date) => {
                    setError({ ...error, to_date: '' })
                    setSearchFormData((values) => ({
                      ...values,
                      to_date: date
                    }))
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  minDate={searchFormData.from_date}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-md-4"></div>
              <div className="col-md-8">
                <button
                  className="btn btn-light-primary m-1 mt-8 font-5vw "
                  onClick={handleSearch}
                >
                  Search
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { clinetListStore, monitorExportReportStore } = state
  return {
    loading: state && state.MonitorStore && state.MonitorStore.loading,
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
    monitorExportloading: state && state.monitorExportReportStore && state.monitorExportReportStore.loading,
    monitorExportReportData: monitorExportReportStore && monitorExportReportStore.monitorExportReportData ? monitorExportReportStore.monitorExportReportData : "",
  }
}

const mapDispatchToProps = (dispatch) => ({
  getWebAnalysisDispatch: (params) =>
    dispatch(getWebAnalysisActions.getgetWebAnalysislist(params)),
  getMonitorlistDispatch: (params) =>
    dispatch(MonitorActions.getMonitorlist(params)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  getMonitorCountsDispatch: (params) => dispatch(MonitorCountsActions.getMonitorCounts(params)),
  getmonitorExportReportDispatch: (params) => dispatch(monitorExportReportActions.getmonitorExportReport(params)),
  clearExportReportDispatch: (params) => dispatch(monitorExportReportActions.clearmonitorExportReport(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchList)
