import { useState, useEffect } from "react"
import "bootstrap-icons/font/bootstrap-icons.css"
import { KTSVG } from "../../theme/helpers"
import _ from 'lodash'
import "./styles.css"
import Modal from "react-bootstrap/Modal"
import {MonitorActionUpdateActions} from '../../store/actions'
import { connect } from 'react-redux'
import { STATUS_RESPONSE } from '../../utils/constants'
import { successAlert, warningAlert } from "../../utils/alerts"

function Actions(props) {
  const {
    PostMonitorActionUpdateDispatch,
    actionIds,
    MonitorActionUpdateData,
    clearMonitorActionUpdateDispatch,
    loading
  } = props
  const [show, setShow] = useState(false)
  const [FormData, setFormData] = useState({
    tag: '',
    monitoringInterval: '',
    summaryId: ''
  })
  const [Errors, setErrors] = useState({
    tag: '',
    monitoringInterval: '',
  })
  const clearPopup = () => {
    setShow(false)
  }

  const handleShow = () => {
    setShow(true)
    setFormData({
      monitoringInterval: ''
    })
  }

  const handleChanges = (e) => {
    setFormData((FormData) => ({ ...FormData, [e.target.name]: e.target.value }))
    setErrors({ ...Errors, [e.target.name]: '' })
  }
  const OnSubmit = () => {
    const Errors = {}
    if (_.isEmpty(FormData.monitoringInterval)) {
      Errors.monitoringInterval = 'Action is Required'
    }else if (actionIds.length === 0) {
      Errors.monitoringInterval = 'Please Select the OGM Data'
    }
    setErrors(Errors)
    if (_.isEmpty(Errors)) {
      const params = {
        data: actionIds,
        action: FormData.monitoringInterval
      }
      PostMonitorActionUpdateDispatch(params)
    }
  }

  useEffect(() => {
    if (MonitorActionUpdateData && MonitorActionUpdateData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        MonitorActionUpdateData && MonitorActionUpdateData.message,
        'success'
      )
      clearMonitorActionUpdateDispatch()
      handleShow(false)
      setFormData({
        monitoringInterval: '',
      })
    } else if (MonitorActionUpdateData && MonitorActionUpdateData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        MonitorActionUpdateData && MonitorActionUpdateData.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearMonitorActionUpdateDispatch()
    }
  }, [MonitorActionUpdateData])

  return (
    <>
      <div>
        <button
          type="button"
          className="btn btn-primary ml-1"
          onClick={() => {
            handleShow()
          }}
          // disabled={actionIds.length > 0 ? false : true}
        >
          {/* eslint-disable */}
          <KTSVG path="/media/icons/duotune/general/gen022.svg" />
          {/* eslint-disable */}
          Actions
        </button>
      </div>

      <Modal show={show} size="lg" centered onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: "rgb(126 126 219)" }}
          closeButton={() => clearPopup()}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            Action
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8"></div>
            <div className='row mt-4'>
              <div className='col-lg-3 mb-3'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Update Status :
                </label>
              </div>
              <div className='col-lg-8'>
                <div className="form-check form-check-custom form-check-solid mb-4">
                  <label className='d-flex flex-stack mb-5 cursor-pointer'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChanges(e)}
                        value='ACTIVE'
                        name='monitoringInterval'
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        Active
                      </span>
                    </span>
                  </label>
                  <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChanges(e)}
                        value='onHold'
                        name='monitoringInterval'
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        On Hold
                      </span>
                    </span>
                  </label>
                  <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChanges(e)}
                        value='stopMonitoring'
                        name='monitoringInterval'
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        Stop Monitoring
                      </span>
                    </span>
                  </label>
                </div>
                {Errors && Errors.monitoringInterval && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {Errors.monitoringInterval}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row">
            <div className='col-md-4'>
            </div>
            <div className='col-md-8'>
              <button
                type='button'
                className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                onClick={(e) => OnSubmit(e)}
              disabled={loading}
              >
                {!loading && <span className='indicator-label'>Submit</span>}
                      {loading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    MonitorActionUpdateData: state && state.MonitorActionUpdateStore && state.MonitorActionUpdateStore.MonitorActionUpdateData,
    loading: state && state.MonitorActionUpdateStore && state.MonitorActionUpdateStore.loading,
  }
}

const mapDispatchToProps = (dispatch) => ({
    PostMonitorActionUpdateDispatch: (params) => dispatch(MonitorActionUpdateActions.PostMonitorActionUpdate(params)),
    clearMonitorActionUpdateDispatch: (params) => dispatch(MonitorActionUpdateActions.clearMonitorActionUpdate(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Actions)