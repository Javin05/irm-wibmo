import React, { useState, useEffect, useContext } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  SWEET_ALERT_MSG,
  RISKSTATUS,
} from "../../utils/constants"
import {
  MonitarStatusChangeActions,
  MonitorUpdateQueueActions,
  OGMEmailTemplateactions,
  UpdateOGMEmailTemplateActions,
  DashbordOGMEmailTemplateactions,
  EmailHistoryIdActions
} from "../../store/actions"
import { useLocation } from "react-router-dom"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../utils/alerts"
import clsx from "clsx"
import Modal from 'react-bootstrap/Modal'
import TextSunEditor from "../../theme/layout/components/SunEditor"
import moment from "moment"
import Accordion from 'react-bootstrap/Accordion';

function Status(props) {
  const {
    loading,
    MonitarStatusDispatch,
    MonitarStatusChangeResponce,
    clearMonitarStatusDispatch,
    setActionIds,
    actionIds,
    MonitarUpdateQueue,
    clearMonitarUpdateQueue,
    MonitorUpdateQueueResponse,
    getMonitorCountsDispatch,
    getMonitorlistDispatch,
    limit,
    emailStatusDispatch,
    StatusResponse,
    StatusResponseLoading,
    clearEmailStatusDispatch,
    getUpdateOGMEmailTemplateDispatch,
    saveupdateEmailTemplate,
    updateEmailTemplateLoading,
    clearUpdateOGMEmailTemplateDispatch,
    dashboardOGMEmailTemplateDispatch,
    DashboardOGMEmailTemplate,
    DashboardOGMEmailTemplateLoading,
    cleardashboardOGMEmailTemplateDispatch,
    currentId,
    emailHistoryIdDispatch,
    EmailHistoryResponse,
    EmailHistoryLoading
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const [show, setShow] = useState(false)
  const [showError, setShowError] = useState(false)
  const [rejectShow, setRejectShow] = useState(false)
  const [updateQueue, setUpdateQueue] = useState(false)
  const [ogmStatus, setOgmStatus] = useState(false)
  const [emailHistory, setEmailHistory] = useState(false)
  const [queueData, setQueueData] = useState({})
  const [errors, setErrors] = useState({
    reason: "",
    updateQueue: ""
  })
  const [status, setStatus] = useState('')
  const [emailStatusValue, setEmailStatusvalue] = useState('')
  const [emailTemplate, setEmailTemplate] = useState('')

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setQueueData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleTab = (e) => {
    setStatus(e.target.id)
    setShowError(false)
    if (!_.isEmpty(currentId)) {
      const params = {
        status: e.target.id,
        id: currentId
      }
      dashboardOGMEmailTemplateDispatch(params)
    } else {
      const params = {
        status: e.target.id,
      }
      emailStatusDispatch(params)
    }
  }

  // const ChangeUpdateStatus = (e) => {
  //   setChangeStatus({ ...changeStatus, [e.target.name]: e.target.value })
  //   setErrors({ ...errors, [e.target.name]: "" })
  // }

  const handleQueue = () => {
    const errors = {}
    if (_.isEmpty(queueData.updateQueue)) {
      errors.updateQueue = USER_ERROR.UPDATE_QUEUE
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        ids: actionIds,
        queueName: queueData && queueData.updateQueue,
      }
      MonitarUpdateQueue(params)
    }
  }

  const approveSubmit = () => {
    const params = {
      ids: actionIds,
      status: "APPROVED",
    }
    MonitarStatusDispatch(params)
  }

  const onConfirmReject = () => {
    const params = {
      ids: actionIds,
      status: "REJECTED"
    }
    MonitarStatusDispatch(params)
  }

  const changeStatusSubmit = () => {
      if (!_.isEmpty(currentId)) {
      const params = {
        status: status,
        ids: [currentId],
        bodyContent: emailTemplate && emailTemplate.emailTemplate,
        emailSubject: emailStatusValue && emailStatusValue.emailSubject
      }
      getUpdateOGMEmailTemplateDispatch(params)
      } else {
        const params = {
          status: status,
          ids: actionIds,
          bodyContent: emailTemplate && emailTemplate.emailTemplate,
          emailSubject: emailStatusValue && emailStatusValue.emailSubject
        }
        getUpdateOGMEmailTemplateDispatch(params)
      }
  }

  useEffect(() => {
    if (MonitarStatusChangeResponce && MonitarStatusChangeResponce.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        MonitarStatusChangeResponce && MonitarStatusChangeResponce.message,
        'success'
      )
      setShow(false)
      setRejectShow(false)
      clearMonitarStatusDispatch()
      setActionIds('')
      const params = {
        limit: limit,
        page: 1,
      }
      getMonitorlistDispatch(params)
      getMonitorCountsDispatch(params)
    } else if (MonitarStatusChangeResponce && MonitarStatusChangeResponce.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        MonitarStatusChangeResponce && MonitarStatusChangeResponce.message,
        '',
        'Try again',
        '',
        () => { clearPopup() }
      )
      clearMonitarStatusDispatch()
    }
  }, [MonitarStatusChangeResponce])

  useEffect(() => {
    if (MonitorUpdateQueueResponse && MonitorUpdateQueueResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        MonitorUpdateQueueResponse && MonitorUpdateQueueResponse.message,
        'success'
      )
      setUpdateQueue(false)
      clearMonitarUpdateQueue()
      setActionIds('')
      const params = {
        limit: limit,
        page: 1,
      }
      getMonitorlistDispatch(params)
      getMonitorCountsDispatch(params)
    } else if (MonitorUpdateQueueResponse && MonitorUpdateQueueResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        MonitorUpdateQueueResponse && MonitorUpdateQueueResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearMonitarUpdateQueue()
    }
  }, [MonitorUpdateQueueResponse])

  useEffect(() => {
    if (show) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        "You want to Approve this Report ?",
        "warning",
        "Yes",
        "No",
        () => {
          approveSubmit()
        },
        () => { clearPopup() }
      )
    } else if (rejectShow) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        "You want to Reject this Report ?",
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmReject()
        },
        () => { clearPopup() }
      )
    }
  }, [show, rejectShow])

  useEffect(() => {
    if (StatusResponse && StatusResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setEmailStatusvalue(StatusResponse && StatusResponse.data)
    } else if (StatusResponse && StatusResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        StatusResponse && StatusResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
    }
  }, [StatusResponse])

  useEffect(() => {
    if (DashboardOGMEmailTemplate && DashboardOGMEmailTemplate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setEmailStatusvalue(DashboardOGMEmailTemplate && DashboardOGMEmailTemplate.data)
    } else if (DashboardOGMEmailTemplate && DashboardOGMEmailTemplate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DashboardOGMEmailTemplate && DashboardOGMEmailTemplate.message,
        '',
        'Try again',
        '',
        () => { }
      )
    }
  }, [DashboardOGMEmailTemplate])

  useEffect(() => {
    if (saveupdateEmailTemplate && saveupdateEmailTemplate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        saveupdateEmailTemplate && saveupdateEmailTemplate.message,
        'success'
      )
      setOgmStatus(false)
      setStatus('')
      setEmailTemplate('')
      setEmailStatusvalue('')
      clearUpdateOGMEmailTemplateDispatch()
      clearEmailStatusDispatch()
      cleardashboardOGMEmailTemplateDispatch()
    } else if (saveupdateEmailTemplate && saveupdateEmailTemplate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        saveupdateEmailTemplate && saveupdateEmailTemplate.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearEmailStatusDispatch()
    }
  }, [saveupdateEmailTemplate])

  const clearPopup = () => {
    setShow(false)
    setRejectShow(false)
    setUpdateQueue(false)
    setOgmStatus(false)
    setShowError(false)
    // setChangeStatus({
    //   reason: "",
    // })
    setEmailHistory(false)
    setErrors({})
    setStatus('')
    setEmailTemplate('')
    setEmailStatusvalue('')
    clearUpdateOGMEmailTemplateDispatch()
    clearEmailStatusDispatch()
    cleardashboardOGMEmailTemplateDispatch()
  }

  return (
    <>
      <Modal
        show={emailHistory}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Email History details
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {!EmailHistoryLoading
            ? (
              !_.isEmpty(EmailHistoryResponse && EmailHistoryResponse.data)
                ? (
                  EmailHistoryResponse && EmailHistoryResponse.data && EmailHistoryResponse.data.map((item, index) => {
                    return (
                      <>
                        <Accordion className="mb-2">
                          <Accordion.Item eventKey={index}>
                            <Accordion.Header>
                              <div className="row w-100" key={index}>
                                <div className="col-8 text-start">
                                  <p>{item && item.subjectContent}</p>
                                  <div>
                                    <span className={`badge ${RISKSTATUS[item && item.ogmStatus]}`}>
                                      {item.ogmStatus ? item.ogmStatus : "--"}
                                    </span>
                                  </div>
                                </div>
                                <div className="col-4 text-end">
                                  Date:
                                  {
                                    moment(item && item.createdAt ? item.createdAt : 'No Data').format('DD/MM/YYYY')
                                  }
                                </div>
                              </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              {item.bodyContent ? item.bodyContent : "--"}
                            </Accordion.Body>
                          </Accordion.Item>
                        </Accordion>
                      </>
                    )
                  }))
                : (
                  <div className='text-center py-3 fs-4 font-weight-bold'>
                    <div>{ EmailHistoryResponse && EmailHistoryResponse.message }</div>
                  </div>
                )
            )
            :
            (
              <div className='text-center'>
                <div
                  className='spinner-border text-primary m-5'
                  role='status'
                />
              </div>
            )}
        </Modal.Body>
      </Modal>
      <Modal
        show={ogmStatus}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to change this Users Ogm Record?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <div className="d-flex mb-1 align-items-center justify-content-center px-5">
                      <div className="w-25 d-flex justify-content-start">
                        <input className="form-check-input me-3" type="radio" name="flexRadioDefault" id="APPROVED" onChange={handleTab} />
                        <label className="form-check-label" htmlFor="APPROVED">
                          Approve
                        </label>
                      </div>
                      <div className="w-25 d-flex justify-content-start">
                        <input className="form-check-input me-3" type="radio" name="flexRadioDefault" id="REJECTED" onChange={handleTab} />
                        <label className="form-check-label" htmlFor="REJECTED">
                          Reject
                        </label>
                      </div>
                      <div className="w-25 d-flex justify-content-start">
                        <input className="form-check-input me-3" type="radio" name="flexRadioDefault" id="HOLD" onChange={handleTab} />
                        <label className="form-check-label" htmlFor="HOLD">
                          Hold
                        </label>
                      </div>
                      <div className="w-25 d-flex justify-content-start">
                        <input className="form-check-input me-3" type="radio" name="flexRadioDefault" id="FRAUD" onChange={handleTab} />
                        <label className="form-check-label" htmlFor="FRAUD">
                          Fraud
                        </label>
                      </div>
                    </div>
                    {showError ? <div className="py-2 text-danger px-5">Please Select the Status.</div> : ''}
                    {/* <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label mt-3">
                      Comments
                    </label>
                    <div className="col-lg-12 col-md-12 col-sm-12">
                      <textarea
                        name="reason"
                        type="text"
                        className="form-control form-control-lg form-control-solid"
                        placeholder="Message"
                        onChange={(e) => ChangeUpdateStatus(e)}
                        autoComplete="off"
                        value={changeStatus.reason || ""}
                      />
                    </div> */}
                  </div>
                  {StatusResponseLoading || DashboardOGMEmailTemplateLoading ? <div className='text-center w-100'>
                    <div
                      className='spinner-border text-primary m-5'
                      role='status'
                    />
                  </div> : !_.isEmpty(emailStatusValue && emailStatusValue.bodyContent) ?
                    <div className="my-4 px-5">
                      <TextSunEditor
                        setData={setEmailTemplate}
                        contentData={emailStatusValue && emailStatusValue.bodyContent}
                        name="emailTemplate"
                        minHeight={"400px"}
                      />
                    </div> : null}
                  <div className="col-lg-12">
                    <button
                      type="button"
                      className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                      onClick={() => changeStatusSubmit()}
                      disabled={loading}
                    >
                      {updateEmailTemplateLoading || DashboardOGMEmailTemplateLoading ? <div
                        className='spinner-border text-white'
                        role='status'
                      /> : "Submit"}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <Modal
        show={updateQueue}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to Update This Users Ongoing Monitoring Report ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Update Queue
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 ">
                    <select
                      name='updateQueue'
                      className='form-select form-select-solid mb-4'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                    >
                      <option value=''>Select...</option>
                      <option value='SUSPECT_ONGOING_MONITORING_Q'>SUSPECT_ONGOING_MONITORING_Q</option>
                      <option value='ONGOING_MONITORING_Q'>ONGOING_MONITORING_Q</option>
                    </select>
                    {errors.updateQueue && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.updateQueue}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                        type="submit"
                        className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                        onClick={(event) => {
                          handleQueue(event)
                        }}
                        disabled={loading}
                      >
                        {!loading && (
                          "Submit"
                        )}
                        {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      {/* <div className="row">
        <div className="col-lg-12"> */}
          <div className="card-toolbar d-flex">
            <>
              {/* <ul className="nav"> */}
                {
                  !_.isEmpty(currentId) ? (
                    <>
                      <div className="nav-item">
                        <a
                          className="nav-link btn btn-md fw-bolder px-4 me-1 btn-dark ms-2"
                          onClick={() => {
                            setEmailHistory(true)
                            emailHistoryIdDispatch(currentId)
                          }}
                        >
                          Email History
                        </a>
                      </div>
                    </>
                  ) : (
                    null
                  )
                }
                <div className="nav-item">
                  <a
                    className="nav-link btn btn-md fw-bolder px-4 me-1 btn-secondary ms-2"
                    onClick={() => {
                      setOgmStatus(true)
                    }}
                  >
                    Change Ogm Status
                  </a>
                </div>
                {
                  _.isEmpty(currentId) ? (
                    <>
                      <div className="nav-item">
                        <a
                          className="nav-link btn btn-md fw-bolder px-4 me-1 btn-light-success ms-2"
                          onClick={() => {
                            setUpdateQueue(true)
                          }}
                        >
                          Update Queues
                        </a>
                      </div>
                      <div className="nav-item">
                        <a
                          className="nav-link btn btn-md fw-bolder px-4 me-1 btn-light-success ms-2"
                          onClick={() => {
                            setShow(true)
                          }}
                        >
                          Approve
                        </a>
                      </div>
                      <div className="nav-item">
                        <a
                          className="nav-link btn btn-md fw-bolder px-4 me-1 btn-light-danger"
                          onClick={() => {
                            setRejectShow(true)
                          }}
                        >
                          Reject
                        </a>
                      </div>
                    </>

                  ) : (
                    null
                  )
                }
              {/* </ul> */}
            </>
          </div>
        {/* </div>
      </div> */}
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    monitarStatusARStore,
    monitorUpdateQueueStore,
    OGMEmailTemplateStore,
    updateEmailTemplateStore,
    DashbordOGMEmailTemplateStore,
    EmailHistoryIdStore
  } = state
  return {
    MonitarStatusChangeResponce: monitarStatusARStore && monitarStatusARStore.MonitarStatusChangeResponce ? monitarStatusARStore.MonitarStatusChangeResponce : {},
    loading: monitarStatusARStore && monitarStatusARStore.loading ? monitarStatusARStore.loading : false,
    MonitorUpdateQueueResponse: monitorUpdateQueueStore && monitorUpdateQueueStore.MonitorUpdateQueueResponce ? monitorUpdateQueueStore.MonitorUpdateQueueResponce : {},
    StatusResponse: OGMEmailTemplateStore && OGMEmailTemplateStore.OGMEmailTemplatepostData ? OGMEmailTemplateStore.OGMEmailTemplatepostData : {},
    StatusResponseLoading: OGMEmailTemplateStore && OGMEmailTemplateStore.loading ? OGMEmailTemplateStore.loading : false,
    saveupdateEmailTemplate: updateEmailTemplateStore && updateEmailTemplateStore.saveupdateEmailTemplateResponse ? updateEmailTemplateStore.saveupdateEmailTemplateResponse : {},
    updateEmailTemplateLoading: updateEmailTemplateStore && updateEmailTemplateStore.loading ? updateEmailTemplateStore.loading : false,
    DashboardOGMEmailTemplate: DashbordOGMEmailTemplateStore && DashbordOGMEmailTemplateStore.DashboardOGMEmailTemplate ? DashbordOGMEmailTemplateStore.DashboardOGMEmailTemplate : {},
    DashboardOGMEmailTemplateLoading: DashbordOGMEmailTemplateStore && DashbordOGMEmailTemplateStore.loading ? DashbordOGMEmailTemplateStore.loading : false,
    EmailHistoryResponse: EmailHistoryIdStore && EmailHistoryIdStore.EmailHistoryIdData ? EmailHistoryIdStore.EmailHistoryIdData : {},
    EmailHistoryLoading: EmailHistoryIdStore && EmailHistoryIdStore.loading ? EmailHistoryIdStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  MonitarStatusDispatch: (params) => dispatch(MonitarStatusChangeActions.MonitarStatusChange(params)),
  clearMonitarStatusDispatch: () => dispatch(MonitarStatusChangeActions.clearMonitarStatusChange()),
  MonitarUpdateQueue: (params) => dispatch(MonitorUpdateQueueActions.MonitorUpdateQueue(params)),
  clearMonitarUpdateQueue: () => dispatch(MonitorUpdateQueueActions.clearMonitorUpdateQueue()),
  emailStatusDispatch: (params) => dispatch(OGMEmailTemplateactions.OGMEmailTemplatepost(params)),
  clearEmailStatusDispatch: () => dispatch(OGMEmailTemplateactions.clearOGMEmailTemplatepost()),
  getUpdateOGMEmailTemplateDispatch: (params) => dispatch(UpdateOGMEmailTemplateActions.getUpdateOGMEmailTemplate(params)),
  clearUpdateOGMEmailTemplateDispatch: (params) => dispatch(UpdateOGMEmailTemplateActions.clearUpdateOGMEmailTemplate(params)),
  dashboardOGMEmailTemplateDispatch: (params) => dispatch(DashbordOGMEmailTemplateactions.DashbordOGMEmailTemplatepost(params)),
  cleardashboardOGMEmailTemplateDispatch: (params) => dispatch(DashbordOGMEmailTemplateactions.clearDashbordOGMEmailTemplatepost(params)),
  emailHistoryIdDispatch: (id) => dispatch(EmailHistoryIdActions.getEmailHistoryId(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Status)