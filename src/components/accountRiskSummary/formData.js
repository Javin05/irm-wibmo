import _ from 'lodash'

export const setAccountRiskCommentData = (data) => {
    if (!_.isEmpty(data)) {
        return {
            accountsStatus: data && data.accountsStatus ? data.accountsStatus : '',
            comments: data && data.comments ? data.comments : ''
        }
    }
}