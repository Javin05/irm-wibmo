import React, { Fragment } from 'react'
import { connect } from "react-redux";
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash";

import BusinessAddress from "./businessAddress";
import MapGoogle from "../../maps/MapGoogle";
import MapGoogleSingle from "../../maps/MapGoogleSingle";
import StreetMap from "../../maps/StreetMap";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'

function Address(props) {
  const { summary, dashboard, isLoaded, matrixDetail, dashboardDetails } = props;

  console.log('summary', summary)


  let addressTotal =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "addressTotalScore" in dashboard
      ? dashboard.addressTotalScore
      : 0; //Number
  let addressPositive =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "addresspositive" in dashboard
      ? dashboard.addresspositive
      : null; //Array
  let addressNegative =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "addressnegative" in dashboard
      ? dashboard.addressnegative
      : null; //Array

  const IDAddress = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.address && dashboardDetails.dashboardData.address.data
  const BusinessAddressData = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.businessAddress && dashboardDetails.dashboardData.businessAddress.data

  let allMarkers = [];
  if (matrixDetail && matrixDetail.individualAddressLocation) {
    allMarkers.push({
      lat: matrixDetail.individualAddressLocation.lat,
      lng: matrixDetail.individualAddressLocation.long,
      area: "INDIVIDUALADDRESS"
    })
  }
  
  const AddresData = summary && summary.addressCheck && summary.addressCheck.address_check


  return (
    <>
      <div className="container-fixed">
        <h1 className="d-flex justify-content-center mb-4 mt-8">
          Individual Address
        </h1>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={addressTotal}
            text={`${addressTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {addressTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8">
          <div className="col-lg-12">
            <div className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {!addressPositive ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(addressPositive) ? (
                    <span className=" text-danger " role="status">
                      N/A
                    </span>
                  ) : (
                    addressPositive &&
                    addressPositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {!addressNegative ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(addressNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    addressNegative &&
                    addressNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8 mb-12">
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Address
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                {
                  !IDAddress
                    ? (
                      <div>
                        <div className='text-center'>
                          <div className='spinner-border text-primary m-5' role='status' />
                        </div>
                      </div>
                    )
                    : IDAddress && IDAddress.map((item, i) => {
                      return (
                        <Fragment key={"FIX_3" + i}>
                          {
                            item && item.status === 'positive'
                              ? (
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                      {
                                        !_.isEmpty(item.info) ? (
                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                            className='tooltip'
                                          >
                                            {item.info}
                                          </Tooltip>}
                                            placement={"right"}
                                          >
                                            <span>
                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                            </span>
                                          </OverlayTrigger>
                                        ) : (
                                          null
                                        )
                                      }
                                    </div>
                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                  </div>
                                  <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                      <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                      <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                    </svg>
                                  </span>
                                </div>
                              )
                              : (
                                <>
                                  {
                                    item && item.status === 'warning'
                                      ? (
                                        <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                          <div className='flex-grow-1 me-2'>
                                            <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                              {
                                                !_.isEmpty(item.info) ? (
                                                  <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                    className='tooltip'
                                                  >
                                                    {item.info}
                                                  </Tooltip>}
                                                    placement={"right"}
                                                  >
                                                    <span>
                                                      <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                    </span>
                                                  </OverlayTrigger>
                                                ) : (
                                                  null
                                                )
                                              }
                                            </div>
                                            <span className='text-muted fw-bold d-block'>{item.value}</span>
                                          </div>
                                          <span className='fw-bolder text-warning py-1'>
                                            <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                          </span>
                                        </div>
                                      )
                                      : (
                                        <>
                                          {
                                            item && item.status === 'negative'
                                              ? (
                                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                  <div className='flex-grow-1 me-2'>
                                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                      {
                                                        !_.isEmpty(item.info) ? (
                                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                            className='tooltip'
                                                          >
                                                            {item.info}
                                                          </Tooltip>}
                                                            placement={"right"}
                                                          >
                                                            <span>
                                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                            </span>
                                                          </OverlayTrigger>
                                                        ) : (
                                                          null
                                                        )
                                                      }
                                                    </div>
                                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                  </div>
                                                  <span className='fw-bolder text-danger py-1' title={item.message}>
                                                    <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                  </span>
                                                </div>
                                              )
                                              : null
                                          }
                                        </>
                                      )
                                  }
                                </>
                              )
                          }
                        </Fragment>
                      )
                    })
                }

              </div>
            </div>
          </div>
          <div className="col-lg-5">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                  Individual Address
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Resident Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {AddresData && AddresData.name
                          ? AddresData.name
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Address
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {AddresData && AddresData.address
                          ? AddresData.address
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                {/* <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {AddresData && AddresData.status
                          ? AddresData.status
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8">
          <div className="col-lg-6">
            <div className="card w-744px h-450px">
              {isLoaded ? <MapGoogle mapData={matrixDetail} mapMarkers={allMarkers} /> : null}
            </div>
          </div>
          <div className="col-lg-6">
            <div
              style={{
                height: "450px",
                backgroundColor: "#eeeeee",
              }}
            >
              {isLoaded ? <StreetMap mapData={matrixDetail} /> : null}
            </div>
          </div>
        </div>
      </div>
      <div className="mt-10">
        <BusinessAddress
          summary={summary}
          dashboard={dashboard}
          splitData={matrixDetail}
          isLoaded={isLoaded}
          BusinessAddressData={BusinessAddressData}
        />
      </div>
    </>
  );
}

export default Address;
