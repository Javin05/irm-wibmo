import _, { isArray } from 'lodash'
import MapGoogle from "../../../maps/MapGoogle"
import StreetMap from "../../../maps/StreetMap"

function MerchantInfo(props) {
  const {
    matrixDetail,
    isLoaded,
    DashboardExportData
  } = props

  let allMarkers = []
  if (matrixDetail && matrixDetail.businessAddressLocation) {
    allMarkers.push({
      lat: matrixDetail.businessAddressLocation.lat,
      lng: matrixDetail.businessAddressLocation.long,
      area: "BUSINESS ADDRESS"
    })
  }
  return (
    <>
      <div className='row g-5 g-xl-8 mb-8'>
        <h1>Merchant Info</h1>
        <div className='col-lg-6'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='ms-4 border-0 pt-5'>
              <h3 class="card-title align-items-start flex-column">
                <div class="symbol symbol-50px me-2">
                  <span class="symbol-label">
                    <i class="bi bi-geo-alt-fill fs-2 text-danger h-40 align-self-center" alt="" />
                  </span>
                </div>
                <span class="card-label fw-bold fs-2 mb-1">Location</span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Business Address
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Ip Address
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {
                        Array.isArray(DashboardExportData && DashboardExportData.ipAddressOfServer) ? (
                          DashboardExportData && DashboardExportData.ipAddressOfServer.map((item) => {
                            return (
                              <>
                                {
                                  item.dnsType === "A" ? (
                                    <>
                                      {
                                        item.address
                                      }<br />
                                    </>
                                  ) : item
                                }
                              </>
                            )
                          })
                        ) : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Lat
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {
                        matrixDetail && matrixDetail.businessAddressLocation.lat ? matrixDetail.businessAddressLocation.lat : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Lng
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {
                        matrixDetail && matrixDetail.businessAddressLocation.long ? matrixDetail.businessAddressLocation.long : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-6'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='ms-4 border-0 pt-5'>
              <h3 class="card-title align-items-start flex-column">
                <div class="symbol symbol-50px me-2">
                  <span class="symbol-label">
                    <i class="bi bi-person-circle fs-2 text-danger h-40 align-self-center" alt="" />
                  </span>
                </div>
                <span class="card-label fw-bold fs-2 mb-1">Personal Details</span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className='align-items-center rounded p-2 mb-0 ms-4'>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Contact Details Email
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {
                        Array.isArray(DashboardExportData && DashboardExportData.contactDetailsEmail) ?
                        DashboardExportData && DashboardExportData.contactDetailsEmail.map((email) =>{
                          return(
                            <>{email}<br />
                            </>
                          )
                        })
                        :'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Legal Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-capital'>
                      {
                        DashboardExportData && DashboardExportData.legalName ? DashboardExportData.legalName : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Telephone
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                    {
                        Array.isArray(DashboardExportData && DashboardExportData.contactDetailsPhone) ?
                        DashboardExportData && DashboardExportData.contactDetailsPhone.map((Phone) =>{
                          return(
                            <>{Phone}<br />
                            </>
                          )
                        })
                        :'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-12'>
          <div className="row mt-4">
            <div className="col-lg-6">
              <div className="card w-744px h-450px">
                {isLoaded ? <MapGoogle mapData={matrixDetail} mapMarkers={allMarkers} /> : null}
              </div>
            </div>
            <div className="col-lg-6">
              <div
                style={{
                  height: "450px",
                  backgroundColor: "#eeeeee",
                }}
              >
                {isLoaded ? <StreetMap mapData={matrixDetail} /> : null}
              </div>
            </div>
          </div>
        </div>
        <div className="card card-xl-stretch mb-xl-8">
          <div className='border-0 pt-5'>
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label fw-bold fs-2 mb-1">Website</span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr class="fw-bold fs-6 text-gray-800">
                    <th>Risk Score</th>
                    <th>Domain</th>
                    <th>Ip Address On Server</th>
                    <th>Ip Country</th>
                    <th>Website Working</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{DashboardExportData.riskScore}</td>
                    <td>
                      <a
                        className='color-primary cursor-pointer'
                        onClick={() => window.open(DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'
                          ?
                          DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'
                          : 'No Data', "_blank")}
                      >
                        {
                          DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'
                        }
                      </a>
                    </td>
                    <td>
                      {
                        Array.isArray(DashboardExportData && DashboardExportData.ipAddressOfServer) ? (
                          DashboardExportData && DashboardExportData.ipAddressOfServer.map((item) => {
                            return (
                              <>
                                {
                                  item.dnsType === "A" ? (
                                    <>
                                      {
                                        item.address
                                      }<br />
                                    </>
                                  ) : item
                                }
                              </>
                            )
                          })
                        ) : 'No Data'
                      }
                    </td>
                    <td>{ DashboardExportData &&
                      DashboardExportData.geoCountry ?
                        DashboardExportData.geoCountry : 'No Data'
                    }
                    </td>
                    <td>
                      {DashboardExportData && DashboardExportData.websiteWorking ? DashboardExportData.websiteWorking : 'No Data'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </>
  )
}

export default MerchantInfo