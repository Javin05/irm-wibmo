import React, { useEffect, useState } from "react"
import moment from "moment"
import "react-circular-progressbar/dist/styles.css"
import { Link, useLocation } from "react-router-dom"
import { connect } from 'react-redux'
import WebsiteContent from './website-content'
import WebsiteSetup from './website-setup'
import GooggleReview from './Google-Review'
import Score from "./Score"
import Summarry from './Summary'
import ScreenShot from './ScreebShot'
import _ from 'lodash'
import {
  PlayStoreDashboardActions,
  ExportListActions,
  DashboardListPmaActions,
  dashboardDetailsActions
} from "../../../../store/actions"
import PlayStoreScreenShot from './playStore/ScreenShot'
import PlayStoreTransationLaundring from './playStore/TransactionLaundring'
import PlayStoreSummarry from './playStore/Summary'
import { getLocalStorage } from '../../../../utils/helper'

function Websites(props) {
  const {
    loading,
    getRiskSummarys,
    dashboardDetails,
    merchantIddetails,
    getriskScores,
    matrixDetail,
    isLoaded,
    PlayStoreDashboardDispatch,
    PlayStoreDashboardResponse,
    getExportDispatch,
    DashboardExportLists,
    clearExportListDispatch,
    DashboardListPmaDispatch,
    DashboardPmaLists,
    getWebsiteIdDataLists,
    getDashboardDispatch
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [activestep, setActiveStep] = useState(0)
  const [completed] = useState({})
  const steps = getSteps()
  const PlayStorestep = PlayStoreSteps()
  const [NameData, setNameData] = useState()
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))

  const viewData = getRiskSummarys && getRiskSummarys && getRiskSummarys.data ? getRiskSummarys.data : []
  const getData = _.isArray(viewData) ? viewData.filter(o => (o ? o : '')) : ''
  const website = getData && getData[0] && getData[0].websiteContacts ? getData[0].websiteContacts : '--'
  const socialLink = website && website.socialLinks && website.socialLinks ? website.socialLinks : '--'
  const domainAvailability = getData && getData[0] && getData[0].domainAvailability ? getData[0].domainAvailability : '--'
  const administrative = domainAvailability && domainAvailability.administrativeContact ? domainAvailability.administrativeContact : '--'
  const speedometetervalue = dashboardDetails && dashboardDetails.data && dashboardDetails.data ? dashboardDetails.data : '--'
  const ValidData = getData && getData[0] ? getData[0] : '--'

  const sslValue = ValidData && ValidData.sslVulnerabilityInfo ? ValidData.sslVulnerabilityInfo : '--'
  const domainHistoryData = ValidData && ValidData.domainHistory && ValidData.domainHistory.records ? ValidData.domainHistory.records : []
  const domainHistory = domainHistoryData.filter(o => (o ? o : null))
  const domainHistoryMap = domainHistory[0] && domainHistory[0].nameServers ? domainHistory[0] && domainHistory[0].nameServers : '--'
  const domainInfoData = ValidData && ValidData.domainInfo && ValidData.domainInfo.registryData
    ? ValidData.domainInfo.registryData : '--'
  const successVerifyDomain = ValidData && ValidData.verifyDomain && ValidData.verifyDomain.message && ValidData.verifyDomain.message.result ? ValidData.verifyDomain.message.result : null
  const successWhoDomain = ValidData && ValidData.whoDomain && ValidData.whoDomain.data ? ValidData.whoDomain.data : null
  const webAnalysis = ValidData && ValidData.webAnalysisStatus ? ValidData.webAnalysisStatus : null
  const webRiskAnalysis = ValidData && ValidData.webRiskAnalysis ? ValidData.webRiskAnalysis : null
  const BusinessName = ValidData && ValidData.webAnalysisShedule && ValidData.webAnalysisShedule.business_name ? ValidData.webAnalysisShedule.business_name : ''
  const policyComplianceCheck = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ? webAnalysis.data.policy_complaince_checks : null
  const shippingComplianceCheck = webAnalysis && webAnalysis.data && webAnalysis.data.product_pricing_checks ? webAnalysis.data.product_pricing_checks : null
  const userInteractionChecks = webAnalysis && webAnalysis.data && webAnalysis.data.user_interaction_checks ? webAnalysis.data.user_interaction_checks : null
  const webContentMonitoring = webAnalysis && webAnalysis.data && webAnalysis.data.web_content_monitoring ? webAnalysis.data.web_content_monitoring : null
  const websitetobusinessmatch = webAnalysis && webAnalysis.data && webAnalysis.data.website_to_business_name_match ? webAnalysis.data.website_to_business_name_match : null
  const domainRepetation = ValidData && ValidData.domainReputation ? ValidData.domainReputation : null
  const DNSData = ValidData && ValidData.dnsLookup && ValidData.dnsLookup.DNSData && ValidData.dnsLookup.DNSData ? ValidData.dnsLookup.DNSData : null
  const adminContact = domainInfoData && domainInfoData.administrativeContact ?
    domainInfoData.administrativeContact : '--'
  const registerData = domainInfoData ? domainInfoData : '--'
  const adminContactData = domainInfoData && domainInfoData.administrativeContact ? domainInfoData.administrativeContact : '--'
  const websiteCatgories = ValidData && ValidData.websiteCategorization && ValidData.websiteCategorization.message ? ValidData.websiteCategorization.message : '--'
  const reviewAnalysis = ValidData && ValidData.reviewAnalysis ? ValidData.reviewAnalysis : '--'
  const websiteImageDetect = ValidData && ValidData.websiteImageDetect ? ValidData.websiteImageDetect : '--'
  const websiteScore = getriskScores && getriskScores.data ? getriskScores.data : '--'
  const phonevalue = Math.round(websiteScore && websiteScore.riskScore)
  const websiteData = dashboardDetails && dashboardDetails.dashboardData &&  dashboardDetails.dashboardData.website &&  dashboardDetails.dashboardData.website.data

  const websiteCategorizationV1 = ValidData && ValidData.websiteCategorizationV1
  const LogoCheck = ValidData && ValidData.logoCheck
  const websiteLink = merchantIddetails && merchantIddetails.data && merchantIddetails.data.website ? merchantIddetails.data.website : successVerifyDomain && successVerifyDomain.websiteURL ? successVerifyDomain.websiteURL : '--'
  const DashboardExportData = DashboardExportLists && DashboardExportLists.data && DashboardExportLists.data.data ? DashboardExportLists.data.data[0] : 'No Data'

  useEffect(() => {
    if (websiteLink) {
      var clearURL = websiteLink.replace('https://', '').replace('http://', '').replace('www.', '').replace('/', '')
      var splitURL = clearURL.split('.')
      var domainLegalName = ''
      if (splitURL.length > 0) {
        domainLegalName = splitURL[0]
      }
      setNameData(domainLegalName)
    }
  }, [websiteLink])

  const accountId = getWebsiteIdDataLists && getWebsiteIdDataLists._id ? getWebsiteIdDataLists._id : null

  useEffect(() => {
    if (!_.isEmpty(accountId)) {
      getDashboardDispatch(accountId)
    }
  }, [accountId])

  useEffect(() => {
    const params = {
      id: !_.isEmpty(accountId) ? accountId : id
    }
    // PlayStoreDashboardDispatch(params)
    getExportDispatch(params)
  }, [])

  useEffect(() => {
    const params = {
      riskId: !_.isEmpty(accountId) ? accountId : id
    }
    DashboardListPmaDispatch(params)
  }, [])

  useEffect(() => {
    return () => {
      clearExportListDispatch()
    }
  }, [])

  function getSteps() {
    return [
      {
        label: "Summary",
        className: "btn web-label-six",
        stepCount: 0,
      },
      {
        label: "Website Content",
        className: "btn web-label-six",
        stepCount: 1,
      },
      {
        label: "Website Content Set-up",
        stepCount: 2,
        className: "btn web-label-six",
      },
      {
        label: "Reputation",
        stepCount: 3,
        className: "btn web-label-six",
      },
      {
        label: "Screenshots",
        stepCount: 4,
        className: "btn web-label-six",
      },
    ]
  }

  function PlayStoreSteps() {
    return [
      {
        label: "Summary",
        className: "btn web-label-six",
        stepCount: 0,
      },
      {
        label: "Transaction Laundering",
        stepCount: 1,
        className: "btn web-label-six",
      },
      {
        label: "Screenshots",
        stepCount: 2,
        className: "btn web-label-six",
      }
    ]
  }

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <Summarry
            websiteData={websiteData}
            matrixDetail={matrixDetail}
            isLoaded={isLoaded}
            successVerifyDomain={successVerifyDomain}
            adminContact={adminContact}
            adminContactData={adminContactData}
            domainInfoData={domainInfoData}
            merchantIddetails={merchantIddetails}
            websitevalue={phonevalue}
            websiteLink={websiteLink}
            ValidData={ValidData}
            DNSData={DNSData}
            BsName={NameData}
            DashboardExportData={DashboardExportData}
          />
        )
      case 1:
        return (
          <WebsiteContent
            ValidData={ValidData}
            successVerifyDomain={successVerifyDomain}
            websiteCatgories={websiteCatgories}
            websitetobusinessmatch={websitetobusinessmatch}
            websiteImageDetect={websiteImageDetect}
            userInteractionChecks={userInteractionChecks}
            webAnalysis={webAnalysis}
            policyComplianceCheck={policyComplianceCheck}
            websiteCategorizationV1={websiteCategorizationV1}
            LogoCheck={LogoCheck}
            websiteLink={websiteLink}
            DashboardExportData={DashboardExportData}
          />
        )
      case 2:
        return (
          <WebsiteSetup
            ValidData={ValidData}
            successVerifyDomain={successVerifyDomain}
            domainInfoData={domainInfoData}
            domainRepetation={domainRepetation}
            adminContact={adminContact}
            adminContactData={adminContactData}
            registerData={registerData}
            successWhoDomain={successWhoDomain}
            websiteCategorizationV1={websiteCategorizationV1}
            LogoCheck={LogoCheck}
            DashboardExportData={DashboardExportData}
          />
        )
      case 3:
        return (
          <GooggleReview
            reviewAnalysis={reviewAnalysis}
            DashboardExportData={DashboardExportData}
          />
        )
      case 4:
        return (
          <ScreenShot
            webAnalysis={webAnalysis}
            DashboardExportData={DashboardExportData}

          />
        )
      default:
        return "unknown step"
    }
  }

  const playStoreData = PlayStoreDashboardResponse && PlayStoreDashboardResponse.data && PlayStoreDashboardResponse.data.data

  function getPlayStoreStepContent(step) {
    switch (step) {
      case 0:
        return (
          <PlayStoreSummarry
            playStoreData={playStoreData}
          />
        )
      case 1:
        return (
          <PlayStoreTransationLaundring
          />
        )
      case 2:
        return (
          <PlayStoreScreenShot
            playStoreData={playStoreData}
          />
        )
      default:
        return "unknown step"
    }
  }

  const handleNext = (step) => {
    setActiveStep(activestep + 1)
  }

  const handleStep = (step) => () => {
    setActiveStep(step)
  }

  const handleBack = () => {
    setActiveStep(activestep - 1)
  }

  return (
    <>
      {
        merchantIddetails && merchantIddetails.data && merchantIddetails.data.playstoreUrl === "YES" ?
          (
            <div className="mt-0">
              <>
                <div className="d-flex">
                  {PlayStorestep.map((step, index) => (
                    <div
                      key={"A_" + index}
                      completed={completed[index]}
                      className={`my-10 mx-1 text mb-4 rounded-1 seven-label fs-8 fw-bolder text-gray-800 ${loading ? "event-disable" : ""
                        } ${step.stepCount === activestep
                          ? "btn web-label-sixActive fs-8 fw-bolder"
                          : `${step.className}`
                        }`}
                      onClick={handleStep(index)}
                    >
                      {step.label}
                    </div>
                  ))}
                </div>
                {activestep === PlayStorestep.length ? null : (
                  <>
                    <div>{getPlayStoreStepContent(activestep)}</div>
                  </>
                )}
              </>
            </div>
          ) : (
            <div className="mt-0">
              <Score
                websiteLink={websiteLink}
                websitevalue={phonevalue}
                BusinessName={BusinessName}
                ValidData={ValidData}
                BsName={NameData}
                websiteData={websiteData}
                successVerifyDomain={successVerifyDomain}
                DNSData={DNSData}
                matrixDetail={matrixDetail}
                adminContact={adminContact}
                adminContactData={adminContactData}
                websitetobusinessmatch={websitetobusinessmatch}
                webAnalysis={webAnalysis}
                LogoCheck={LogoCheck}
                websiteImageDetect={websiteImageDetect}
                userInteractionChecks={userInteractionChecks}
                domainInfoData={domainInfoData}
                domainRepetation={domainRepetation}
                registerData={registerData}
                successWhoDomain={successWhoDomain}
                websiteCategorizationV1={websiteCategorizationV1}
                reviewAnalysis={reviewAnalysis}
                merchantIddetails={merchantIddetails}
                id={id}
                DashboardExportData={DashboardExportData}
                DashboardPmaLists={DashboardPmaLists}
              />
              <>
                <div className="d-flex">
                  {steps.map((step, index) => (
                    <div
                      key={"A_" + index}
                      completed={completed[index]}
                      className={`my-10 mx-1 text mb-4 rounded-1 seven-label fs-8 fw-bolder text-gray-800 ${loading ? "event-disable" : ""
                        } ${step.stepCount === activestep
                          ? "btn web-label-sixActive fs-8 fw-bolder"
                          : `${step.className}`
                        }`}
                      onClick={handleStep(index)}
                    >
                      {step.label}
                    </div>
                  ))}
                </div>
                {activestep === steps.length ? null : (
                  <>
                    <div>{getStepContent(activestep)}</div>
                  </>
                )}
              </>
            </div>
          )
      }
    </>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore, WebsiteStore, riskScoreStore, PlayStoreDashboardStore, exportlistStore, DashboardListPmaStore } = state
  return {
    WebsiteResponce: WebsiteStore && WebsiteStore.WebsiteResponce ? WebsiteStore.WebsiteResponce : {},
    getRiskSummarys:
      state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    dashboardDetails:
      dashboardStore && dashboardStore.dashboardDetails?.data
        ? dashboardStore.dashboardDetails?.data
        : null,
    getriskScores: riskScoreStore && riskScoreStore.getriskScores ? riskScoreStore.getriskScores : {},
    PlayStoreDashboardResponse: PlayStoreDashboardStore && PlayStoreDashboardStore.PlayStoreDashboardResponse ? PlayStoreDashboardStore.PlayStoreDashboardResponse : {},
    DashboardExportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    DashboardPmaLists: DashboardListPmaStore && DashboardListPmaStore.DashboardListPmaRes ? DashboardListPmaStore.DashboardListPmaRes : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  PlayStoreDashboardDispatch: (params) => dispatch(PlayStoreDashboardActions.getPlayStoreDashboard(params)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),
  DashboardListPmaDispatch: (params) => dispatch(DashboardListPmaActions.getDashboardListPma(params)),
  getDashboardDispatch: (id) =>
    dispatch(dashboardDetailsActions.getdashboardDetails(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Websites)
