import React, { Fragment, useEffect, useState } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import moment from 'moment'
import PDF from '../pdf/Pdf'
import jsPDF from "jspdf"
import 'react-circular-progressbar/dist/styles.css'
import { renderToString } from "react-dom/server"
import ReactSpeedometer from "react-d3-speedometer"
import Status from './Status'
import { connect } from 'react-redux'
import { TagSummaryAction, ExportListActions } from '../../../../store/actions'
import { getLocalStorage, removeLocalStorage } from '../../../../utils/helper'
import { warningAlert } from "../../../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import autoTable from 'jspdf-autotable'
import ReactPDF from '@react-pdf/renderer';
import { pdf } from "@react-pdf/renderer";
import { saveAs } from 'file-saver';
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import { PMASTATUS } from '../../../../utils/constants'

function Score(props) {
  const {
    websiteLink,
    websitevalue,
    BusinessName,
    getExportDispatch,
    exportLoading,
    exportLists,
    clearExportListDispatch,
    BsName,
    websiteData,
    successVerifyDomain,
    DNSData,
    matrixDetail,
    adminContact,
    adminContactData,
    websitetobusinessmatch,
    websiteCatgories,
    ValidData,
    webAnalysis,
    LogoCheck,
    websiteImageDetect,
    userInteractionChecks,
    domainInfoData,
    domainRepetation,
    registerData,
    successWhoDomain,
    websiteCategorizationV1,
    reviewAnalysis,
    merchantIddetails,
    id,
    DashboardExportData,
    DashboardPmaLists,
  } = props

  const pmaData = DashboardPmaLists && DashboardPmaLists.data ? DashboardPmaLists.data : ''
  const pma = pmaData && pmaData.pma ? pmaData.pma : ''
  const pmaCheck = pmaData && pmaData.pmaCheck ? pmaData.pmaCheck : ''

  const print = () => {
    const string = renderToString(
      <PDF
        websiteData={websiteData}
        successVerifyDomain={successVerifyDomain}
        DNSData={DNSData}
        matrixDetail={matrixDetail}
        adminContact={adminContact}
        adminContactData={adminContactData}
        BsName={BsName}
        websitetobusinessmatch={websitetobusinessmatch}
        websiteCatgories={websiteCatgories}
        ValidData={ValidData}
        webAnalysis={webAnalysis}
        LogoCheck={LogoCheck}
        websiteImageDetect={websiteImageDetect}
        userInteractionChecks={userInteractionChecks}
        domainInfoData={domainInfoData}
        domainRepetation={domainRepetation}
        registerData={registerData}
        successWhoDomain={successWhoDomain}
        websiteCategorizationV1={websiteCategorizationV1}
        reviewAnalysis={reviewAnalysis}
        websiteLink={websiteLink}
        merchantIddetails={merchantIddetails}
      />)
    const pdf = new jsPDF("p", "mm", "a1")
    let elementHandler = {
      '#ignorePDF': function (element, renderer) {
        return true
      }
    }
    let source = window.document.getElementById("pdf_converter")
    if (string) {
      pdf.html(string).then(() => pdf.save(`${BsName}${id}.pdf`))
    }
    // if (source) {
    //   pdf.html(source).then(() => pdf.save('website.pdf'))
    // }
  }

  const exported = () => {
    const params = {
      website: websiteLink
    }
    getExportDispatch(params)
  }

  useEffect(() => {
    setTimeout(() => {
      clearExportListDispatch()
    },1000)
  },[])

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="simple"
          sheet="tablexls"
        />
      </div>

      <div className='row g-5 g-xl-8'>
        <div className='col-lg-12'>
          <div className="card card-xl-stretch">
            <div className="card-body pt-0">
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <span className='d-flex justify-content-center mt-4 fw-boldest my-1 fs-3'>
                    Website
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer'
                      onClick={() => window.open(websiteLink)}
                    >- {websiteLink}</a>
                  </span>
                </div>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-start mt-4 fw-boldest my-1 fs-3'>
                    Company Name
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer text-capital'
                    >{!_.isEmpty(BusinessName) ? BusinessName : BsName}</a>
                  </div>
                </div>
              </div>
              <div className="separator separator-dashed my-3" />
              <div className='row'>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-center'
                  >
                    <ReactSpeedometer
                      maxValue={100}
                      value={parseInt(DashboardExportData && DashboardExportData.riskScore)}
                      customSegmentStops={[0, 35, 75, 100]}
                      segmentColors={["limegreen", "gold", "tomato"]}
                      needleColor="red"
                      startColor="green"
                      segments={10}
                      endColor="blue"
                      className='pichart'
                      currentValueText="Over All Score: #{value}"
                      currentValuePlaceholderStyle={"#{value}"}
                    />
                  </div>
                </div>

                {/* <div className='col-lg-3'>
                  <Status />
                  <div className='mb-4'>
                    <div className='mt-4 '>
                      <a className="btn fw-bolder px-4 btn-light-success w-100"
                        data-toggle='modal'
                        data-target='#rejectModal'
                      onClick={() => { setActive(true) }}
                      >
                        Approve
                      </a>
                    </div>
                    <div className='mt-4 '>
                      <a className="btn fw-bolder px-4 btn-light-danger w-100"
                        data-toggle='modal'
                        data-target='#rejectModal'
                      onClick={() => { setActive(true) }}
                      >
                        Reject
                      </a>
                    </div>
                  </div>
                </div> */}

                <div className='col-lg-6'>
                  <div className='mt-2 d-flex'>
                    <button
                      onClick={print}
                      className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'
                      // onClick={(e) => exported(e)}
                      disabled={exportLoading}
                    >
                      {!exportLoading &&
                        <span className='indicator-label'>
                          <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                          PDF Report
                        </span>
                      }
                      {exportLoading && (
                        <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}

                    </button>
                  </div>
                </div>
              </div>
            </div>
            {
             merchantIddetails && merchantIddetails.data && merchantIddetails.data.clientId && merchantIddetails.data.clientId.pma === false ? (null) : (
                <div className='col-lg-12'>
                  <div className='fs-1 fw-boldest mt-4 d-flex justify-content-start ms-4'>
                    PMA
                  </div>
                  
                  {
                    pmaCheck && pmaCheck["PMA Flag"] === 'Yes' ? (
                      <>
                        <div className="separator separator-dashed my-3" />
                        <div class="table-responsive ms-4">
                          <table class="table"  style={{textAlign:"center"}}>
                            <thead>
                              <tr class="fw-bold fs-6 text-gray-800">
                                <th>Heavy Discounts</th>
                                <th>Login Credentials Are Required</th>
                                <th>Page Navigation Issue</th>
                                <th>Pricing is in Dollars</th>
                                <th>Pricing is not updated</th>
                                <th>Website Redirection</th>
                                <th>Website is not working</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Heavy Discounts']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Login Credentials are required']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Page Navigation Issue']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is in Dollars']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is not updated']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website Redirection']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website is not working']]} mt-4`} />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </>
                    ) : (
                      
                      <div className='ml-5'>
                        <p>No violation detected</p>
                        <br/>
                      </div>
                    )
                  }
                  <div className="separator separator-dashed my-3" />
                </div>
              )
            }
          </div>

        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { exportlistStore } = state;
  return {
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Score)