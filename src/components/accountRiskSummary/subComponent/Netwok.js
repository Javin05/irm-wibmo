import { useEffect, useState, useRef, Fragment } from 'react'
import Xarrow, { useXarrow } from 'react-xarrows';
import Icofont from 'react-icofont';
import './index.css'
import { useLocation } from 'react-router-dom'
import { LinkAnlyticsTransactionActions, linkAnalyticsActions } from '../../../store/actions'
import { connect } from 'react-redux'
import axiosInstance from '../../../services';
import { Scrollbars } from 'react-custom-scrollbars';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover'
import Tooltip from 'react-bootstrap/Tooltip'
import { toAbsoluteUrl } from '../../../theme/helpers'

const MainColor = {
  "Personal Phone": "#009EF7",
  // "IP Address": "#F1416C",
  "Personal Email": "#50CD89",
  "Website": "#7239EA",
  "Business Address": "#000000",
  "Business Phone": "#e45e9c",
  "Business Email": "rgb(210 123 8)",
  "Business Name" : "#F1416C"
}
const SubColor = {
  "Personal Phone": "#D3EFFF",
  // "IP Address": "#F9DBE4",
  "Personal Email": "#BFFDDC",
  "Website": "#E1D7F9",
  "Business Address": "#E8E0DF",
  "Business Phone": "#edd2de",
  "Business Email": "rgb(240 239 209)",
  "Business Name" : "#F9DBE4"
}
const Icon = {
  "Personal Phone": "icofont-ui-touch-phone",
  "IP Address": "icofont-eclipse",
  "Personal Email": "icofont-email",
  "Website": "icofont-web",
  "Business Address": "icofont-computer",
  "Business Phone": "icofont-ui-touch-phone",
  "Business Email": "icofont-email",
  "Business Name" : ''
}

function NetworkGraph(props) {
  const {
    className,
    linkAnalytics,
    LinkAnlyticslists,
    getlinkAnalyticslistDispatch,
    merchantIddetails
  } = props;

  const boxRef = useRef(null);
  const updateXarrow = useXarrow();
  const [path, pathType] = useState("straight"); //"smooth" | "grid" | "straight"
  const [childlineColor, setChildlineColor] = useState("#E1F0FF"); //"smooth" | "grid" | "straight"
  const [highlight, setHighlight] = useState(null);
  const [activePop, setActivePop] = useState(null);

  const url = useLocation().pathname
  const fields = url && url.split('/')
  const id = fields && fields[3]

  const HighlightHandler = (lighter) => {
    setHighlight(lighter);
  }
  const HoverToggle = (dummy, toggle) => {
    if (toggle) {
      setActivePop(dummy)
    }
    else {
      setActivePop(null)
    }
  }

  useEffect(() => {
    const params = {
      caseId: id,
    }
    getlinkAnalyticslistDispatch(params)
    // document.getElementById("Phone").addEventListener("click", updateXarrow);
  }, []);

  const merchant = merchantIddetails && merchantIddetails.data ? merchantIddetails.data : '--'

  return (
    <div className='row'>
      <div className='col-lg-12 col-md-12 col-sm-12'>
        <Scrollbars style={{ width: "100%", height: 600 }}>
          <div className='graph-row'>
            <div className='column'>
              <span className='primary'>IRM-{merchant && merchant.riskId ? merchant.riskId : '--'}</span>
              <div ref={boxRef} className="box main" id="main" style={{ backgroundColor: highlight !== null ? MainColor[highlight] : "#E1F0FF" }}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/profile.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>
            </div>
            <div className='column mid-column'>
              <span style={{ color: MainColor["Personal Phone"] }}>Personal Phone : {merchant && merchant.phone ? merchant.phone : '--'}</span>
              <div className="box"style={{ backgroundColor: highlight === "Personal Phone" ? MainColor["Personal Phone"] : SubColor["Personal Phone"] }} id="Personal Phone" onClick={(e) => HighlightHandler("Personal Phone")}
              >
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/telephone.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              {/* <span style={{ color: MainColor["IP Address"] }}>IP Address : {merchant && merchant.ipAddress ? merchant.ipAddress : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"] }} id="IP Address" onClick={(e) => HighlightHandler("IP Address")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/ip.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div> */}

              <span style={{ color: MainColor["Personal Email"] }}>Personal Email : {merchant && merchant.personalEmail ? merchant.personalEmail : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Personal Email" ? MainColor["Personal Email"] : SubColor["Personal Email"] }} id="Personal Email" onClick={(e) => HighlightHandler("Personal Email")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/mail.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Website"] }}>Website : {merchant && merchant.website ? merchant.website : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Website" ? MainColor["Website"] : SubColor["Website"] }} id="Website" onClick={(e) => HighlightHandler("Website")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/global-network.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Business Name"] }}>Business Name : {merchant && merchant.businessName ? merchant.businessName : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Business Name" ? MainColor["Business Name"] : SubColor["Business Name"] }} id="Business Name" onClick={(e) => HighlightHandler("Business Name")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/businessName.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Business Address"] }} className='z-999'>Business Address : {merchant && merchant.businessAddress ? merchant.businessAddress : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Business Address" ? MainColor["Business Address"] : SubColor["Business Address"] }} id="Business Address" onClick={(e) => HighlightHandler("Business Address")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/house.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Business Phone"] }}>Business Phone : {merchant && merchant.businessPhone ? merchant.businessPhone : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Business Phone" ? MainColor["Business Phone"] : SubColor["Business Phone"] }} id="Business Phone" onClick={(e) => HighlightHandler("Business Phone")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/businessPhone.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Business Email"] }}>Business Email : {merchant && merchant.businessEmail ? merchant.businessEmail : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Business Email" ? MainColor["Business Email"] : SubColor["Business Email"] }} id="Business Email" onClick={(e) => HighlightHandler("Business Email")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/businessMail.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <Xarrow start={'main'} end={'Personal Phone'} path={path} color={highlight === "Personal Phone" ? MainColor["Personal Phone"] : SubColor["Personal Phone"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Personal Phone") }} />
              {/* <Xarrow start={'main'} end={'IP Address'} path={path} color={highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("IP Address") }} /> */}
              <Xarrow start={'main'} end={'Personal Email'} path={path} color={highlight === "Personal Email" ? MainColor["Personal Email"] : SubColor["Personal Email"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Personal Email") }} />
              <Xarrow start={'main'} end={'Website'} path={path} color={highlight === "Website" ? MainColor["Website"] : SubColor["Website"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Website") }} />
              <Xarrow start={'main'} end={'Business Name'} path={path} color={highlight === "Business Name" ? MainColor["Business Name"] : SubColor["Business Name"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Name") }} />
              <Xarrow start={'main'} end={'Business Address'} path={path} color={highlight === "Business Address" ? MainColor["Business Address"] : SubColor["Business Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Address") }} />
              <Xarrow start={'main'} end={'Business Phone'} path={path} color={highlight === "Business Phone" ? MainColor["Business Phone"] : SubColor["Business Phone"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Phone") }} />
              <Xarrow start={'main'} end={'Business Email'} path={path} color={highlight === "Business Email" ? MainColor["Business Email"] : SubColor["Business Email"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Email") }} />
            </div>
            <div className='column mt-4'>

              {LinkAnlyticslists && LinkAnlyticslists?.data && LinkAnlyticslists?.data.linkData && LinkAnlyticslists.data.linkData.map((dummy, index) => {

                return (
                  <Fragment key={"Z_" + index}>
                    <a
                      className='color-primary cursor-pointer'
                      onClick={() => window.open(`/risk-summary/update/${dummy._id}`, "_blank")}
                      data-bs-toggle="tooltip"
                      data-bs-custom-class="tooltip-inverse"
                      data-bs-placement="top"
                      title="Tooltip on top">
                      IRM{dummy.riskId}
                    </a>
                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                      className='tooltip'
                    >
                      Email - {dummy && dummy.personalEmail} <br />
                      Source - {dummy && dummy.source.map((name) => { return (<>{name}</>) })}
                    </Tooltip>}
                      placement={"right"}
                    >
                      <div
                        key={"X_" + index}
                        className="box"
                        id={"XYZ_" + index}
                        style={{
                          backgroundColor: 'rgb(237 184 23)'
                        }}
                      >
                        <span>
                          <Icofont
                            icon={"student-alt"}
                            style={{
                              color: 'black'
                            }}
                          />
                        </span>
                      </div> 
                    </OverlayTrigger>
                    {
                      dummy && dummy.source.map((item, i) => {
                        return (
                          <>
                            <Xarrow
                              key={"Y_" + index}
                              start={item}
                              end={"XYZ_" + index}
                              path={path}
                              color={highlight === item ? MainColor[item] : SubColor[item]}
                              headSize={4}
                              strokeWidth={2}
                              dashness={highlight === item ? true : false
                              } />
                          </>
                        )
                      })
                    }
                  </Fragment>
                )
              })}
            </div>
          </div>
        </Scrollbars>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  const { linkAnalyticslistStore } = state;
  return {
    LinkAnlyticslists: linkAnalyticslistStore && linkAnalyticslistStore.linkAnalyticallists ? linkAnalyticslistStore.linkAnalyticallists : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  getlinkAnalyticslistDispatch: (params) => dispatch(linkAnalyticsActions.getlinkAnalyticslist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(NetworkGraph);