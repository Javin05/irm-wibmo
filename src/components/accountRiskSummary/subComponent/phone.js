import React, { useState, useEffect, Fragment, useRef } from 'react'
import { connect } from "react-redux";
import {
  CircularProgressbar,
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import moment from "moment";
import { DATE } from "../../../utils/constants";
import { Link, useLocation } from "react-router-dom";
import BusinessPhone from "./businessPhone";
import _ from "lodash";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'
import { FINDBLACKLIST } from '../../../utils/constants'

function Phone(props) {
  const {
    dashboardDetails,
    summary
  } = props

  const speedometetervalue =
    dashboardDetails && dashboardDetails.data && dashboardDetails.data
      ? dashboardDetails.data
      : "--";
  const phoneTotal =
    speedometetervalue && speedometetervalue.phoneTotalScore
      ? speedometetervalue.phoneTotalScore
      : "--";
  const phonePositive =
    dashboardDetails &&
      dashboardDetails.data &&
      dashboardDetails.data.phonepositive
      ? dashboardDetails.data.phonepositive
      : "--";
  const phoneNegative =
    dashboardDetails &&
      dashboardDetails?.data &&
      dashboardDetails.data.phonenegative
      ? dashboardDetails.data.phonenegative
      : "--";
  const forceRef = useRef(null);


  const dashboardIndidualPhone = summary && summary.verifyPhoneV1 && summary.verifyPhoneV1.verify_phone_v1
  const dashboardBusinesPhone = summary && summary.businessVerifyPhoneV1 && summary.businessVerifyPhoneV1.verify_phone_v1

  const IDPhone = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.phone && dashboardDetails.data.dashboardData.phone.data
  const BusinessPhoneData = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessPhone && dashboardDetails.data.dashboardData.businessPhone.data

  return (
    <div>
      <div>
        <div className="row mt-8">
          <div className="col-lg-4" />
          <div className="col-lg-4">
            <h1 className="d-flex justify-content-center mb-4">
              Individual Phone
            </h1>
          </div>
        </div>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={phoneTotal}
            text={`${phoneTotal}%`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              // pathColor: 'mediumseagreen',
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {phoneTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8 mt-8">
          <div className="col-lg-12">
            <h5 className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {!phonePositive ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(phonePositive) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    phonePositive &&
                    typeof phoneNegative === "object" &&
                    phonePositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {!phoneNegative ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(phoneNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    phoneNegative &&
                    typeof phoneNegative === "object" &&
                    phoneNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </h5>
          </div>
        </div>
        <div className="mb-4" style={{ backgroundColor: "orangered" }}>
          {/* <ReactFlow elements={elements} /> */}
          {/* <div>
            <div className="A" style={{ color:'orangered'}}>Element A</div>
            <div className="B"  style={{ color:'blue'}}>Element B</div>
            <LineTo from="A" to="B" orientation="v" />
        </div> */}
          {/* <AnyChart
           height={'650px'}
            type="sankey"
            data={sankeyData}
            title="Link Analyis - Individual"
            legend="true" 
            options={options}
          /> */}
        </div>
        <div className="row">
          <div className="mb-8">
            <a href="#" className="d-flex justify-content-center fs-2">
              CONFIDENCE MATRIX
            </a>
          </div>
        </div>
        <div className="row g-5 g-xl-8 mb-8">
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Phone
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                {
                  !IDPhone
                    ? (
                      <div>
                        <div className='text-center'>
                          <div className='spinner-border text-primary m-5' role='status' />
                        </div>
                      </div>
                    )
                    : IDPhone && IDPhone.map((item, i) => {
                      return (
                        <Fragment key={"FIX_3" + i}>
                          {
                            item && item.status === 'positive'
                              ? (
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                      {
                                        !_.isEmpty(item.info) ? (
                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                            className='tooltip'
                                          >
                                            {item.info}
                                          </Tooltip>}
                                            placement={"right"}
                                          >
                                            <span>
                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                            </span>
                                          </OverlayTrigger>
                                        ) : (
                                          null
                                        )
                                      }
                                    </div>
                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                  </div>
                                  <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                      <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                      <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                    </svg>
                                  </span>
                                </div>
                              )
                              : (
                                <>
                                  {
                                    item && item.status === 'warning'
                                      ? (
                                        <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                          <div className='flex-grow-1 me-2'>
                                            <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                              {
                                                !_.isEmpty(item.info) ? (
                                                  <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                    className='tooltip'
                                                  >
                                                    {item.info}
                                                  </Tooltip>}
                                                    placement={"right"}
                                                  >
                                                    <span>
                                                      <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                    </span>
                                                  </OverlayTrigger>
                                                ) : (
                                                  null
                                                )
                                              }
                                            </div>
                                            <span className='text-muted fw-bold d-block'>{item.value}</span>
                                          </div>
                                          <span className='fw-bolder text-warning py-1'>
                                            <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                          </span>
                                        </div>
                                      )
                                      : (
                                        <>
                                          {
                                            item && item.status === 'negative'
                                              ? (
                                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                  <div className='flex-grow-1 me-2'>
                                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                      {
                                                        !_.isEmpty(item.info) ? (
                                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                            className='tooltip'
                                                          >
                                                            {item.info}
                                                          </Tooltip>}
                                                            placement={"right"}
                                                          >
                                                            <span>
                                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                            </span>
                                                          </OverlayTrigger>
                                                        ) : (
                                                          null
                                                        )
                                                      }
                                                    </div>
                                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                  </div>
                                                  <span className='fw-bolder text-danger py-1' title={item.message}>
                                                    <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                  </span>
                                                </div>
                                              )
                                              : null
                                          }
                                        </>
                                      )
                                  }
                                </>
                              )
                          }
                        </Fragment>
                      )
                    })
                }

              </div>
            </div>
          </div>

          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Location
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        City
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {dashboardIndidualPhone && dashboardIndidualPhone.location && dashboardIndidualPhone.location.city ? dashboardIndidualPhone.location.city : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        country
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.location && dashboardIndidualPhone.location.country && dashboardIndidualPhone.location.country.name ? dashboardIndidualPhone.location.country.name : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Phone Type
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.phone_type && dashboardIndidualPhone.phone_type.description ? dashboardIndidualPhone.phone_type.description : "--"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">Sms</span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Zip Code
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.contact && dashboardIndidualPhone.contact.zip_postal_code ? dashboardIndidualPhone.contact.zip_postal_code : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Country Code
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6">
                      {dashboardIndidualPhone && dashboardIndidualPhone.numbering && dashboardIndidualPhone.numbering.original && dashboardIndidualPhone.numbering.original.country_code ? dashboardIndidualPhone.numbering.original.country_code : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Phone Number
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.numbering && dashboardIndidualPhone.numbering.original && dashboardIndidualPhone.numbering.original.phone_number ? dashboardIndidualPhone.numbering.original.phone_number : "--"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Current Phone Status
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Device Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.status && dashboardIndidualPhone.status.code ? dashboardIndidualPhone.status.code : "--"}
                      </span>
                    </div>
                  </div>
                </div>

                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Local Format
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {dashboardIndidualPhone && dashboardIndidualPhone.local_format ? dashboardIndidualPhone.local_format : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Mcc
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {dashboardIndidualPhone && dashboardIndidualPhone.mcc ? dashboardIndidualPhone.mcc : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Message
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.status && dashboardIndidualPhone.status.description ? dashboardIndidualPhone.status.description : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualPhone && dashboardIndidualPhone.carrier && dashboardIndidualPhone.carrier.name ? dashboardIndidualPhone.carrier.name : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Timezone
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {dashboardIndidualPhone && dashboardIndidualPhone.timezone ? dashboardIndidualPhone.timezone : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Blacklisted
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                     {/* {FINDBLACKLIST[summary && summary.blacklisted */}
                     --
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <BusinessPhone dashboardBusinesPhone={dashboardBusinesPhone} BusinessPhoneData={BusinessPhoneData} summary={summary} />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  const { accountdashboardGetDetailsStore } = state;
  return {
    dashboardDetails:
      accountdashboardGetDetailsStore && accountdashboardGetDetailsStore.aaccountdashboardGetDetails
        ? accountdashboardGetDetailsStore.aaccountdashboardGetDetails
        : {},
  };
};
const mapDispatchToProps = (dispatch) => ({
  // getRiskSummaryDispatch: (id) =>  dispatch(riskSummaryActions.getRiskSummary(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Phone);
