import './pdf.css';
import { jsPDF } from "jspdf";
import { renderToString } from "react-dom/server"
import { toAbsoluteUrl } from '../../../../theme/helpers'
import { forwardRef, Fragment } from "react";
import { connect } from 'react-redux'
import _ from 'lodash'
import { RISKSTATUS } from '../../../../utils/constants'
import { Link } from "react-router-dom"
import moment from 'moment'
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';

function PDF(props, ref) {
  const {
    websiteData,
    successVerifyDomain,
    DNSData,
    matrixDetail,
    adminContact,
    adminContactData,
    BsName,
    websitetobusinessmatch,
    websiteCatgories,
    ValidData,
    webAnalysis,
    LogoCheck,
    websiteImageDetect,
    userInteractionChecks,
    domainInfoData,
    domainRepetation,
    registerData,
    successWhoDomain,
    reviewAnalysis,
    websiteLink,
    merchantIddetails
  } = props

  const websitedataCategory = ValidData && ValidData.websiteCategorizationV1 &&
    ValidData.websiteCategorizationV1.data && ValidData.websiteCategorizationV1.data.classification ? ValidData.websiteCategorizationV1.data.classification : 'No Data'
  const webData = webAnalysis && webAnalysis.data ? webAnalysis.data : 'No Data'

  const policy = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ?
    webAnalysis.data.policy_complaince_checks : 'No Data'

  const screenShot = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ?
    webAnalysis.data.policy_complaince_checks : 'No Data'

  const Email = merchantIddetails && merchantIddetails.data && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo.email ? merchantIddetails.data.assignedTo.email : 'No Data'
  
  return (
    <div
      ref={ref}
      className="mt4"
      style={{
        backgroundColor: "white",
        width: "159mm",
        // maxHeight: "597mm",
        marginLeft: "auto",
        marginRight: "auto"
      }}
    >
      <div className="pdf-container">
        <table className="pdf-table">
          <tr>
            <td>
              <img
                src={LogoCheck && LogoCheck.irmLogo ? LogoCheck.irmLogo : LogoCheck && LogoCheck.original_url}
                // onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                alt="logo"
                className='mb-2'
              />
            </td>
            <td> <h3 className="pdf-h3">WRM Merchant Scorecard</h3></td>
            <td><p className="pdf-p"><b>Scan Date : </b>
              {
                moment(merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt ? merchantIddetails.data.createdAt : 'No Data').format('DD/MM/YYYY')
              }</p></td>
          </tr>
        </table>
      </div>
      <div className="pdf-containers" >
        <div className='row'>
          <div className='col-lg-3 col-md-3 col-sm-3'>
            <div>
              Sponsor :
            </div>
            <div>
              Site :
            </div>
            <div className='mb-6'>
              IP :
            </div>
            <div>
              Email Address :
            </div>
            <div>
              Phone :
            </div>
          </div>
          <div className='col-lg-7 col-md-7 col-sm-7'>
            <div>
              {
                adminContact.name ?
                  adminContact.name : adminContactData.name ? adminContactData.name : BsName
              }
            </div>
            <div>
              {websiteLink ? websiteLink : 'No Data'}
            </div>
            <div>
              {
                Array.isArray(DNSData && DNSData.dnsRecords) ? (
                  DNSData && DNSData.dnsRecords.map((item) => {
                    return (
                      <>
                        {
                          item.dnsType === "A" ? (
                            <>
                              {
                                item.address
                              }, <span />
                            </>
                          ) : ''
                        }
                      </>
                    )
                  })
                ) : 'No Data'
              }
            </div>
            <div className='mt-6'>
              {Email}
            </div>
            <div>
              {merchantIddetails && merchantIddetails.data && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo.mobile ? merchantIddetails.data.assignedTo.mobile : 'No Data'}
            </div>
          </div>


          <div className='row'>
            <div className='col-lg-9 col-md-7 col-sm-7' />
            <div className='col-lg-2 col-md-2 col-sm-2 mb-2'>
              <img
                src={LogoCheck && LogoCheck.irmLogo ? LogoCheck.irmLogo : LogoCheck && LogoCheck.original_url}
                // onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                alt="logo"
                className='mb-2'
              />
            </div>
            <div className='pdf-table' />
          </div>
        </div>
      </div>
      <div className="pdf-Container">
        <h4>ScoreCard</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th" scope="col">#</th>
              <th className="pdf-th">WCM Validation</th>
              <th className="pdf-th">Comments</th>
              <th className="pdf-th">Risk</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(websiteData) ?
                websiteData && websiteData.map((item, i) => {
                  return (
                    <tr>
                      <td className='pdf-td'> {i + 1}</td>
                      <td className='pdf-td'> {item.title}</td>
                      <td className='pdf-td'> {item.value}</td>
                      <td className={`${RISKSTATUS[item.status]}`}>
                        {item.status ? item.status : "--"}
                      </td>
                    </tr>
                  )
                })
                : (
                  <span className='text-dark fw-semibold fs-5 ms-4'>
                    No Records Found
                  </span>
                )
            }
          </tbody>
        </table>
      </div>
      <div className="pdf-Container">
        <h4>Location</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Business Address</th>
              <td className="pdf-td">{successVerifyDomain && successVerifyDomain.location ? successVerifyDomain.location : 'No Data'}</td>
            </tr>
            <tr>
              <th className="pdf-td">Ip Address</th>
              <td className="pdf-td">
                {
                  Array.isArray(DNSData && DNSData.dnsRecords) ? (
                    DNSData && DNSData.dnsRecords.map((item) => {
                      return (
                        <>
                          {
                            item.dnsType === "A" ? (
                              <>
                                {
                                  item.address
                                }, <br />
                              </>
                            ) : ''
                          }
                        </>
                      )
                    })
                  ) : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Lat</th>
              <td className="pdf-td">
                {
                  matrixDetail && matrixDetail.businessAddressLocation.lat ? matrixDetail.businessAddressLocation.lat : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Lng</th>
              <td className="pdf-td">
                {
                  matrixDetail && matrixDetail.businessAddressLocation.long ? matrixDetail.businessAddressLocation.long : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Personal Details</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Contact Details Email</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.email ?
                    adminContact.email : adminContactData && adminContactData.email ? adminContactData.email
                      : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Legal Name</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.name ?
                    adminContact.name : adminContactData && adminContactData.name ? adminContactData.name : BsName
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Telephone</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.telephone ?
                    adminContact.telephone :
                    adminContactData &&
                      adminContactData.telephone ? adminContactData.telephone : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Website Categorization</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Domain Name</th>
              <td className="pdf-td">
                <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                  <a
                    className='color-primary cursor-pointer'
                    onClick={() => window.open(successVerifyDomain && successVerifyDomain.websiteURL ? successVerifyDomain.websiteURL : 'No Data'
                      ?
                      successVerifyDomain && successVerifyDomain.websiteURL ? successVerifyDomain.websiteURL : 'No Data'
                      : 'No Data', "_blank")}
                  >
                    {
                      successVerifyDomain && successVerifyDomain.websiteURL ? successVerifyDomain.websiteURL : 'No Data'
                    }
                  </a>
                </span>
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Mcc</th>
              <td className="pdf-td">
                {
                  websitetobusinessmatch && websitetobusinessmatch.mcc_code_match
                    ? websitetobusinessmatch.mcc_code_match : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Industry</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.industry ? successVerifyDomain.category.industry : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Industry Group</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.industryGroup ? successVerifyDomain.category.industryGroup : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Sector</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.sector ? successVerifyDomain.category.sector : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">SubIndustry</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.subIndustry ? successVerifyDomain.category.subIndustry : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">SubIndustry</th>
              <td className="pdf-td">
                {
                  !_.isEmpty(
                    websiteCatgories &&
                    websiteCatgories.categories
                  ) ?
                    websiteCatgories &&
                    websiteCatgories.categories.map((item, i) => {
                      return (
                        <div key={"GG_" + i}>
                          <div>
                            {
                              item.tier1 && item.tier1.name
                            } - {
                              item.tier2 && item.tier2.name
                            },
                          </div>
                        </div>
                      )
                    })
                    : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Tag</th>
              <td className="pdf-td">
                {
                  !_.isEmpty(successVerifyDomain && successVerifyDomain.tags) ?
                    successVerifyDomain && successVerifyDomain.tags.map((item, i) => {
                      return (
                        <Fragment key={"HH_" + i}>
                          <div>
                            {item}
                          </div>
                        </Fragment>
                      )
                    })
                    : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Categories</th>
              <td className="pdf-td">
                {
                  Array.isArray(
                    websitedataCategory
                  ) ?
                    ValidData && ValidData.websiteCategorizationV1 &&
                    ValidData.websiteCategorizationV1.data && ValidData.websiteCategorizationV1.data.classification.map((item, i) => {
                      return (
                        <div className="px-5">
                          <span className='ffw-bold text-bold' key={"II_" + i}>
                            <div>
                              {
                                item && item.category
                              } -
                            </div>
                            <div>
                              {
                                item && item.value
                              };
                            </div>
                          </span>
                        </div>
                      )
                    })
                    : ValidData && ValidData.websiteCategorizationV1 &&
                      ValidData.websiteCategorizationV1.data && ValidData.websiteCategorizationV1.data.classification ? ValidData.websiteCategorizationV1.data.classification : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Website Details</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">LegalName</th>
              <td className="pdf-td">
                {
                  webAnalysis && webAnalysis.data && webAnalysis.data.Bussiness_details && webAnalysis.data.Bussiness_details.Company_Namevalue ? webAnalysis.data.Bussiness_details.Company_Namevalue : BsName ? BsName : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Logo</th>
              <td className="pdf-td">
                <img
                  src={LogoCheck && LogoCheck.irmLogo ? LogoCheck.irmLogo : 'No Data'}
                />
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Shopify</th>
              <td className="pdf-td">
                {ValidData && ValidData.websiteDetails && ValidData.websiteDetails.shopify ? ValidData.websiteDetails.shopify : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Method</th>
              <td className="pdf-td">
                {ValidData && ValidData.websiteDetails && ValidData.websiteDetails.method ? ValidData.websiteDetails.method : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Woo Commerce</th>
              <td className="pdf-td">
                {ValidData && ValidData.websiteDetails && ValidData.websiteDetails.woocommerce ? ValidData.websiteDetails.woocommerce : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Description</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.description ? successVerifyDomain.description : 'No Data'}
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Merchant Location</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Business Address</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.location ? successVerifyDomain.location : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Company name & address as image</th>
              <td className="pdf-td">
                --
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Business Address matches website address</th>
              <td className="pdf-td">
                --
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Price & Currency</th>
              <td className="pdf-td">
                --
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Card Scheme Compliance(BRAM/GBPP)</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Violation of Mastercard(BRAM) & VISA(GBPP)</th>
              <td className="pdf-td">
                --
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Adult Content Monitoring</th>
              <td className="pdf-td">
                {websiteImageDetect &&
                  websiteImageDetect.result &&
                  websiteImageDetect.result.query_response
                  ?
                  websiteImageDetect.result.query_response
                  : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Predict Medicine</th>
              <td className="pdf-td">
                {
                  ValidData && ValidData.predictMedicine && ValidData.predictMedicine.result ?
                    ValidData.predictMedicine.result : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Merchant's Policies</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Terms & Condition</th>
              <td className="pdf-td">
                {
                  policy && policy.terms_and_conditions && policy.terms_and_conditions.status ? policy.terms_and_conditions.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Shipping Policy</th>
              <td className="pdf-td">
                {
                  policy && policy.shipping && policy.shipping.status ? policy.shipping.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Return Policy</th>
              <td className="pdf-td">
                {
                  policy && policy.return && policy.return.status ? policy.return.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Contact Policy</th>
              <td className="pdf-td">
                {
                  policy && policy.contact && policy.contact.status ? policy.contact.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Privacy statements</th>
              <td className="pdf-td">
                {
                  policy && policy.privacy && policy.privacy.status ? policy.privacy.status : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Merchant's Policies</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Terms & Condition</th>
              <td className="pdf-td">
                {
                  policy && policy.terms_and_conditions && policy.terms_and_conditions.status ? policy.terms_and_conditions.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Shipping Policy</th>
              <td className="pdf-td">
                {
                  policy && policy.shipping && policy.shipping.status ? policy.shipping.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Return Policy</th>
              <td className="pdf-td">
                {
                  policy && policy.return && policy.return.status ? policy.return.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Contact Policy</th>
              <td className="pdf-td">
                {
                  policy && policy.contact && policy.contact.status ? policy.contact.status : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Privacy statements</th>
              <td className="pdf-td">
                {
                  policy && policy.privacy && policy.privacy.status ? policy.privacy.status : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Purchase Or Registration</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Bank Details Sumission</th>
              <td className="pdf-td">
                {
                  userInteractionChecks && userInteractionChecks.bank_details_sumission
                    ? userInteractionChecks.bank_details_sumission : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Id Submission prompt</th>
              <td className="pdf-td">
                {
                  userInteractionChecks && userInteractionChecks.id_submission_prompt
                    ? userInteractionChecks.id_submission_prompt : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Seller Redirection</th>
              <td className="pdf-td">
                {
                  userInteractionChecks && userInteractionChecks.seller_redirection
                    ? userInteractionChecks.seller_redirection : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Policy Complaince Checks</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Contact</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                  webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                  webAnalysis.data.policy_complaince_checks.contact.status
                  ? webAnalysis.data.policy_complaince_checks.contact.status : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Contains Valid Email</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                  webAnalysis.data.policy_complaince_checks.contact.details &&
                  webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_email
                  ? webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_email : webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                    webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                    webAnalysis.data.policy_complaince_checks.contact.details ? webAnalysis.data.policy_complaince_checks.contact.details : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Contains Valid Phone</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                  webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                  webAnalysis.data.policy_complaince_checks.contact.details &&
                  webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_phone
                  ? webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_phone : webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                    webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                    webAnalysis.data.policy_complaince_checks.contact.details ? webAnalysis.data.policy_complaince_checks.contact.details : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Contacts Form Status</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                  webAnalysis.data.policy_complaince_checks.contact.details &&
                  webAnalysis.data.policy_complaince_checks.contact.details.contacts_form &&
                  webAnalysis.data.policy_complaince_checks.contact.details.contacts_form.status
                  ? webAnalysis.data.policy_complaince_checks.contact.details.contacts_form.status : webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                    webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                    webAnalysis.data.policy_complaince_checks.contact.details ? webAnalysis.data.policy_complaince_checks.contact.details : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Phone Number</th>
              <td className="pdf-td">
                {
                  Array.isArray(
                    webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                    webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                    webAnalysis.data.policy_complaince_checks.contact.details &&
                    webAnalysis.data.policy_complaince_checks.contact.details.phone_number
                  ) ? (
                    webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                    webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                    webAnalysis.data.policy_complaince_checks.contact.details &&
                    webAnalysis.data.policy_complaince_checks.contact.details.phone_number.map((item, i) => {
                      return (
                        <Fragment key={"FF_" + i}>
                          <div className='ffw-bold text-bold ' key={i}>
                            {item}
                          </div>
                        </Fragment>
                      )
                    })
                  ) : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Social Media</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Facebook - Handle</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.facebook && successVerifyDomain.facebook.handle ? successVerifyDomain.facebook.handle : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Twitter - Bio</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.bio ? successVerifyDomain.twitter.bio : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Twitter - Followers</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.followers ? successVerifyDomain.twitter.followers : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Twitter - Handle</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.handle ? successVerifyDomain.twitter.handle : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Twitter - Id</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.id ? successVerifyDomain.twitter.id : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Twitter - Location</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.location ? successVerifyDomain.twitter.location : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Twitter - Site</th>
              <td className="pdf-td">
                {/* <Link> */}
                <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                  {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.site ? successVerifyDomain.twitter.site : 'No Data'}
                </span>
                {/* </Link> */}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Linkedin - Handle</th>
              <td className="pdf-td">
                {successVerifyDomain && successVerifyDomain.linkedin && successVerifyDomain.linkedin.handle ? successVerifyDomain.linkedin.handle : 'No Data'}
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Web Analysis Status</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Page Activity Check - Is Mining Happening?</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_activity_check &&
                  webAnalysis.data.page_analysis_results.page_activity_check.is_mining_happening
                  ? webAnalysis.data.page_analysis_results.page_activity_check.is_mining_happening : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Page Activity Check - Any Untrusted Downloads?</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_activity_check &&
                  webAnalysis.data.page_analysis_results.page_activity_check.untrusted_downloads
                  ? webAnalysis.data.page_analysis_results.page_activity_check.untrusted_downloads : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Page Availability Check - Url Status</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_availability_check &&
                  webAnalysis.data.page_analysis_results.page_availability_check.url_status
                  ? webAnalysis.data.page_analysis_results.page_availability_check.url_status : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Page Health Check - Content Accessibilty</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_health_check &&
                  webAnalysis.data.page_analysis_results.page_health_check.content_accessibilty
                  ? webAnalysis.data.page_analysis_results.page_health_check.content_accessibilty : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td"> Page Health Check - Page loading Time</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_health_check &&
                  webAnalysis.data.page_analysis_results.page_health_check.page_loading_time
                  ? webAnalysis.data.page_analysis_results.page_health_check.page_loading_time : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Page Links Connectivity Check - Status</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_links_connectivity_check &&
                  webAnalysis.data.page_analysis_results.page_links_connectivity_check.status
                  ? webAnalysis.data.page_analysis_results.page_links_connectivity_check.status : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Page Links Connectivity Check - Sucess Rate</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_links_connectivity_check &&
                  webAnalysis.data.page_analysis_results.page_links_connectivity_check.sucess_rate
                  ? webAnalysis.data.page_analysis_results.page_links_connectivity_check.sucess_rate : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Page Redirection Check - Domain Redirection</th>
              <td className="pdf-td">
                {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                  webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_redirection_check &&
                  webAnalysis.data.page_analysis_results.page_redirection_check.domain_redirection
                  ? webAnalysis.data.page_analysis_results.page_redirection_check.domain_redirection : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Website Working -  Page Analysis Results</th>
              <td className="pdf-td">
                {webData && webData.page_analysis_results && webData.page_analysis_results.page_accessibilty ? webData.page_analysis_results.page_accessibilty : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Default Template Lorel Ipsum -  Content Keyword Results</th>
              <td className="pdf-td">
                --
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Default Template Lorel Ipsum -  Content Repetition Status</th>
              <td className="pdf-td">
                {webData && webData.content_repetition_status && webData.content_repetition_status.content_repetition === true ? "true" : 'false'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Language Detection -  Language Detected</th>
              <td className="pdf-td">
                {Array.isArray(webData && webData.language_detected) ? webData.language_detected.map((item) => {
                  return (
                    <>{item.en}</>
                  )
                }) : 'No Data'}
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Domain Info</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Domain Registration Company</th>
              <td className="pdf-td">
                {
                  domainInfoData && domainInfoData.registrarName
                    ? domainInfoData.registrarName
                    : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Parent</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.parent &&
                    successVerifyDomain.parent.domain ? successVerifyDomain.parent.domain
                    : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Mode</th>
              <td className="pdf-td">
                {
                  domainRepetation && domainRepetation.mode ? domainRepetation.mode : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Reputation Score</th>
              <td className="pdf-td">
                {
                  domainRepetation && domainRepetation.reputationScore ? domainRepetation.reputationScore : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : 'No Data'

                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Registrant</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Domain Registered Date</th>
              <td className="pdf-td">
                {
                  !_.isEmpty(registerData && registerData.createdDateNormalized) ?
                    moment(registerData && registerData.createdDateNormalized ? registerData.createdDateNormalized : 'No Data').format('DD/MM/YYYY')
                    : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Domain Expiry Date</th>
              <td className="pdf-td">
                {
                  !_.isEmpty(registerData && registerData.expiresDate) ?
                    moment(registerData && registerData.expiresDateNormalized ? registerData.expiresDateNormalized : 'No Data').format('DD/MM/YYYY')
                    : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Name Servers</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Host Names</th>
              <td className="pdf-td">
                {
                  !_.isEmpty(registerData && registerData.nameServers && registerData.nameServers.hostNames) ? (
                    registerData && registerData.nameServers && registerData.nameServers.hostNames.map((item, i) => {
                      return (
                        <div key={"AA_" + i}>
                          <div className='ffw-bold text-bold '>
                            {item}
                          </div>
                        </div>
                      )
                    })
                  ) : (
                    null
                  )
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Logo</th>
              <td className="pdf-td">
                <img
                  src={LogoCheck && LogoCheck.irmLogo ? LogoCheck.irmLogo : 'No Data'}
                  // onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                />
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Dnssec</th>
              <td className="pdf-td">
                {successWhoDomain && successWhoDomain.dnssec ? successWhoDomain.dnssec : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Domain Aliases</th>
              <td className="pdf-td">
                {
                  !_.isEmpty(successVerifyDomain && successVerifyDomain.domainAliases) ?
                    successVerifyDomain && successVerifyDomain.domainAliases.slice(0, 6).map((item, i) => {
                      return (
                        <div key={"BB_" + i}>
                          <span className='text-muted fw-semibold text-gray-700'>
                            {item}
                          </span>
                        </div>
                      )
                    })
                    : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Administrative Contact</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">State</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.state ?
                    adminContact.state : adminContactData && adminContactData.state ? adminContactData.state :
                      successVerifyDomain && successVerifyDomain.geo && successVerifyDomain.geo.state
                        ? successVerifyDomain.geo.state : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">City</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.city ?
                    adminContact.city : adminContactData && adminContactData.city ? adminContactData.city : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Country</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.country ?
                    adminContact.country : adminContactData && adminContactData.country ? adminContactData.country : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Country Code</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.countryCode ?
                    adminContact.countryCode : adminContactData && adminContactData.countryCode ? adminContactData.countryCode : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Email</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.email ?
                    adminContact.email : adminContactData && adminContactData.email ? adminContactData.email : domainInfoData && domainInfoData.contactEmail ? domainInfoData.contactEmail
                      : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Name</th>
              <td className="pdf-td">
                {adminContact &&
                  adminContact.name ?
                  adminContact.name : adminContactData && adminContactData.name ? adminContactData.name : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Administrative Contact Telephone</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.telephone ?
                    adminContact.telephone : adminContactData && adminContactData.telephone ? adminContactData.telephone : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Administrative Contact Organization</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.organization ?
                    adminContact.organization : adminContactData && adminContactData.organization ? adminContactData.organization : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Administrative Contact Postal Code</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.postalCode ?
                    adminContact.postalCode : adminContactData && adminContactData.postalCode ? adminContactData.postalCode : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Administrative Contact Street1</th>
              <td className="pdf-td">
                {
                  adminContact &&
                    adminContact.street1 ?
                    adminContact.street1 :
                    adminContactData &&
                      adminContactData.street1 ? adminContactData.street1 : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Estimated Domain Age</th>
              <td className="pdf-td">
                {domainInfoData && domainInfoData.estimatedDomainAge ? domainInfoData.estimatedDomainAge : 'No Data'}
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Geo</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">City</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.city
                    ? successVerifyDomain.geo.city : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Country</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.country
                    ? successVerifyDomain.geo.country : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Country Code</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.countryCode
                    ? successVerifyDomain.geo.countryCode : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Latitude</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.lat
                    ? successVerifyDomain.geo.lat : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Longitude</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.lng
                    ? successVerifyDomain.geo.lng : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">postal Code</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.postalCode
                    ? successVerifyDomain.geo.postalCode : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">State</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.state
                    ? successVerifyDomain.geo.state : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">State Code</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.stateCode
                    ? successVerifyDomain.geo.stateCode : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Street Address</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.streetAddress
                    ? successVerifyDomain.geo.streetAddress : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Street Name</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.streetName
                    ? successVerifyDomain.geo.streetName : 'No Data'
                }
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Street Number</th>
              <td className="pdf-td">
                {
                  successVerifyDomain && successVerifyDomain.geo &&
                    successVerifyDomain.geo.streetNumber
                    ? successVerifyDomain.geo.streetNumber : 'No Data'
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Google Review Analysis</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Average Rating</th>
              <td className="pdf-td">
                {reviewAnalysis && reviewAnalysis.average_rating ? reviewAnalysis.average_rating : 'No Data'}
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Status</th>
              <td className="pdf-td">
                {reviewAnalysis && reviewAnalysis.status ? reviewAnalysis.status : 'No Data'}
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Transaction Laundering</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">URL redirection</th>
              <td className="pdf-td">
                No Redirection
              </td>
            </tr>
            <tr>
              <th className="pdf-td">AML Sanction Screening</th>
              <td className="pdf-td">
                Not part of Screened list
              </td>
            </tr>
            <tr>
              <th className="pdf-td">MCC Coding Mismatch(High Risk Prohibited/Restricted Category)</th>
              <td className="pdf-td">
                No Mismatch
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Illegal sale of prescription drugs</th>
              <td className="pdf-td">
                False
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Website Traffic</h4>
        <table className="pdf-table_s ">
          <thead className="pdf-table_secondary">
            <tr>
              <th className="pdf-th">Code</th>
              <th className="pdf-th">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th className="pdf-td">Traffic / Month</th>
              <td className="pdf-td">
                48102
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Organic Traffic</th>
              <td className="pdf-td">
                47928
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Paid Traffic</th>
              <td className="pdf-td">
                84
              </td>
            </tr>
            <tr>
              <th className="pdf-td">Bounce Rate</th>
              <td className="pdf-td">
                27.96%
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="pdf-Container">
        <h4>Terms and Conditions</h4>
        <div className='card card-xl-stretch mb-xl-8'>
          <div className="separator separator-dashed my-3" />
          <div className='card-body pt-0'>
            <div className='align-items-center  rounded p-2 mb-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  {
                    !_.isEmpty(screenShot && screenShot.terms_and_conditions && screenShot.terms_and_conditions.policy_screenshot) ? (
                      <img
                        src={screenShot && screenShot.terms_and_conditions && screenShot.terms_and_conditions.policy_screenshot ? screenShot.terms_and_conditions.policy_screenshot : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />
                    ) : (
                      <span className='text-muted fw-semibold text-gray-700'>
                        Terms and Conditions Page Not Found
                      </span>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="pdf-Container">
        <h4>Privacy Policy</h4>
        <div className='card card-xl-stretch mb-xl-8'>
          <div className="separator separator-dashed my-3" />
          <div className='card-body pt-0'>
            <div className='align-items-center  rounded p-2 mb-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  {
                    !_.isEmpty(screenShot && screenShot.privacy && screenShot.privacy.policy_screenshot) ? (
                      <img
                        src={screenShot && screenShot.privacy && screenShot.privacy.policy_screenshot ? screenShot.privacy.policy_screenshot : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />
                    ) : (
                      <span className='text-muted fw-semibold text-gray-700'>
                        Privacy Policy Page Not Found
                      </span>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="pdf-Container">
        <h4>Shipping Policy</h4>
        <div className='card card-xl-stretch mb-xl-8'>
          <div className="separator separator-dashed my-3" />
          <div className='card-body pt-0'>
            <div className='align-items-center  rounded p-2 mb-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  {
                    !_.isEmpty(screenShot && screenShot.shipping && screenShot.shipping.policy_screenshot) ? (
                      <img
                        src={screenShot && screenShot.shipping && screenShot.shipping.policy_screenshot ? screenShot.shipping.policy_screenshot : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />
                    ) : (
                      <span className='text-muted fw-semibold text-gray-700'>
                        Shipping Policy Page Not Found
                      </span>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="pdf-Container">
        <h4>Returns Policy</h4>
        <div className='card card-xl-stretch mb-xl-8'>
          <div className="separator separator-dashed my-3" />
          <div className='card-body pt-0'>
            <div className='align-items-center  rounded p-2 mb-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  {
                    !_.isEmpty(screenShot && screenShot.return && screenShot.return.policy_screenshot) ? (
                      <img
                        src={screenShot && screenShot.return && screenShot.return.policy_screenshot ? screenShot.return.policy_screenshot : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />
                    ) : (
                      <span className='text-muted fw-semibold text-gray-700'>
                        Returns Policy Page Not Found
                      </span>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="pdf-Container">
        <h4>Contact Policy</h4>
        <div className='card card-xl-stretch mb-xl-8'>
          <div className="separator separator-dashed my-3" />
          <div className='card-body pt-0'>
            <div className='align-items-center  rounded p-2 mb-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  {
                    !_.isEmpty(screenShot && screenShot.contact && screenShot.contact.policy_screenshot) ? (
                      <img
                        src={screenShot && screenShot.contact && screenShot.contact.policy_screenshot ? screenShot.contact.policy_screenshot : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />
                    ) : (
                      <span className='text-muted fw-semibold text-gray-700'>
                        Contact Policy Page Not Found
                      </span>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}

export default forwardRef(PDF)