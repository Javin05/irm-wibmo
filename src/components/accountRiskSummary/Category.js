import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import { USER_ERROR, REGEX, STATUS_RESPONSE, SESSION, SWEET_ALERT_MSG } from '../../utils/constants'
import { Link, useLocation } from 'react-router-dom'
import {
  AMLGetIdActions,
  ApproveAMLActions,
  AMLqueueActions,
  TransactionAMLActions,
  BlockListEmailActions,
  WhiteListActions,
  PostCategoryActions,
  GetCategoryActions
} from '../../store/actions'
import { KTSVG } from '../../theme/helpers'
import clsx from 'clsx'
import _, { values } from 'lodash'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { setCategorieData } from "./ValueData"

function Category(props) {
  const {
    getAMLIdDispatch,
    categoryList,
    getAssignee,
    getBlockListTypeDispatch,
    fetchWhitelistTypeDispatch,
    whitelistTypeData,
    BlockListType,
    PostCategory,
    PostCategroyDispatch,
    ClearPostCategroy,
    Categoryloading,
    getCategoryList,
    GetCategroyDispatch
  } = props
  const [active, setActive] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [value, setValue] = useState()
  const [BlockLIstOption, setBlockLIst] = useState()
  const [SelectedBlockLIstOption, setSelectedBlockListOption] = useState('')
  const [WhiteOption, setWhite] = useState()
  const [SelectedWhiteOption, setSelectedWhiteOption] = useState('')
  const [showOthers, setShowOthers] = useState(false)
  const [categoryValue, seCategoryValue] = useState(false)
  const [formData, setFormData] = useState({
    category: '',
    type: '',
    riskId: currentId,
    status: ''
  })
  const [errors, setErrors] = useState({
    reason: '',
  })

  useEffect(() => {
    if (currentId) {
      getAMLIdDispatch(currentId)
    }
    const params = {
      skipPagination: 'true',
      fieldType: 'website_category',
      queueId: "624fc6b3ae69dc1e03f47ec0"
    }
    const bloackListParams = {
      skipPagination: 'true',
      fieldType: 'Website_Category',
      queueId: "624fc67fae69dc1e03f47ebd"
    }
    getBlockListTypeDispatch(bloackListParams)
    fetchWhitelistTypeDispatch(params)
  }, [currentId])

  useEffect(() => {
    if (PostCategory && PostCategory.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const rejectmodalBtn = document.getElementById('category-model')
      rejectmodalBtn.click()
    }
  }, [PostCategory])

  const Submit = () => {
    const errors = {}
    if (_.isEmpty(formData.type)) {
      errors.reason = "Type Is Required"
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      PostCategroyDispatch(formData)
    }
  }

  const StatusArray = {
    "category": "Online Tutorial",
    "type": "whiteList",
    "status": "APPROVED"
  }

  useEffect(() => {
    if (getCategoryList && getCategoryList.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const categoryValue = getCategoryList && getCategoryList.data
      if (categoryValue && categoryValue.type === "blockList") {
        const category = blockListtOption(BlockListType && BlockListType.data)
        const addItem = (category, item) => category.find((x) => x.label === item.label) || category.push(item)
        addItem(category, { "label": "Others", "value": "others" })
        const categoriesMatch = categoryValue && categoryValue.type ? categoryValue.type : '--'
        setFormData({
          category: categoriesMatch
        })
        if (!_.isEmpty(categoryValue.category)) {

          const selOption = _.filter(category, function (x) {

            if (_.includes(categoryValue.category, x.value)) {
              return x
            }
          })
          setSelectedBlockListOption(selOption)
          seCategoryValue(
            !_.isEmpty(selOption) ? false : true
          )
        }

      } else if (categoryValue && categoryValue.type === "whiteList") {
        const category = whiteListOption(whitelistTypeData)
        const addItem = (category, item) => category.find((x) => x.label === item.label) || category.push(item)
        addItem(category, { "label": "Others", "value": "Others" })
        const categoriesMatch = categoryValue && categoryValue.type ? categoryValue.type : '--'
        setFormData({
          category: categoriesMatch
        })
        if (!_.isEmpty(categoryValue.category)) {
          console.log('categoryV', categoryValue.category)

          const selOption = _.filter(category, function (x) {
            // console.log('category', category)
            console.log('categoryValue.category, x.value', categoryValue.category, x.value)

            if (_.includes(categoryValue.category, x.value)) {
              console.log('categoryValue.category', _.includes(categoryValue.category, x.value))
              return x
            }
          })
          setSelectedWhiteOption(selOption)

          seCategoryValue(
            !_.isEmpty(selOption) ? false : true
          )
        }
      }
      setFormData(values => ({
        ...values,
        type: categoryValue && categoryValue.type,
        status: categoryValue && categoryValue.type === "blockList" ? 'REJECTED' : 'APPROVED',
        riskId: currentId,
        category: categoryValue && categoryValue.category
      }))
    }
  }, [getCategoryList])

  useEffect(() => {
    if (PostCategory && PostCategory.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        PostCategory && PostCategory.message,
        'success'
      )
      // ClearPostCategroy()
      GetCategroyDispatch(currentId)
      // setFormData({
      //   category: '',
      //   type: '',
      //   riskId: currentId,
      //   status: ''
      // })
      // setShowOthers(false)
      // setSelectedBlockListOption('')
      // setSelectedWhiteOption('')
    } else if (PostCategory && PostCategory.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        PostCategory && PostCategory.message,
        '',
        'Try again',
        '',
        () => { }
      )
      ClearPostCategroy()
    }
  }, [PostCategory])

  const rejectChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const apiData = categoryList && categoryList[0] ? categoryList[0] : '--'
  const websiteCatgories = apiData && apiData.websiteCategorization ? apiData.websiteCategorization : '--'

  const handleChangeBlockList = selectedOption => {
    if (selectedOption !== null) {
      setSelectedBlockListOption(selectedOption)
      setFormData(values => ({ ...values, category: selectedOption.value, status: 'REJECTED' }))
      setValue('REJECTED')
      setShowOthers(selectedOption.value === 'others' ? true : false)
    }
  }

  const handleChangeWhiteList = selectedOption => {
    if (selectedOption !== null) {
      setSelectedWhiteOption(selectedOption)
      setFormData(values => ({ ...values, category: selectedOption.value, status: 'APPROVED' }))
      setShowOthers(selectedOption.value === 'others' ? true : false)
    }
  }

  const bloaklistData = BlockListType && BlockListType.data
  useEffect(() => {
    const blockList = blockListtOption(bloaklistData)
    const addItem = (blockList, item) => blockList.find((x) => x.label === item.label) || blockList.push(item)
    addItem(blockList, { "label": "Others", "value": "others" })
    setBlockLIst(blockList)
  }, [bloaklistData])

  useEffect(() => {
    const whitList = whiteListOption(whitelistTypeData)
    const addItem = (whitList, item) => whitList.find((x) => x.label === item.label) || whitList.push(item)
    addItem(whitList, { "label": "Others", "value": "others" })
    setWhite(whitList)
  }, [whitelistTypeData])

  const blockListtOption = (bloaklistData) => {
    const defaultOptions = []
    for (const item in bloaklistData) {
      defaultOptions.push({ label: bloaklistData[item].fieldValue, value: bloaklistData[item].fieldValue })
    }
    return defaultOptions
  }

  const whiteListOption = (whitelistTypeData) => {
    const defaultOptions = []
    for (const item in whitelistTypeData) {
      defaultOptions.push({ label: whitelistTypeData[item].fieldValue, value: whitelistTypeData[item].fieldValue })
    }
    return defaultOptions
  }
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  return (
    <>
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="category-model"
        data-target='#categoryModal'
        onClick={() => { }}
      />
      <div
        className='modal fade'
        id='categoryModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-800px'>
          <div className='modal-content'>
            <div className='modal-header'
              style={{
                padding: "0.65rem 1.75rem"

              }}
            >
              <h2 className='me-8'>Override Category</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-danger close'
                data-dismiss='modal'
              // onClick={() => { setShow(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'
                style={{
                  backgroundColor: 'rgb(213 191 86 / 30%)',
                  borderRadius: '10px'
                }}
              >
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6 mt-4 mb-2'>
                        <h3>
                          Categories
                        </h3>
                      </div>
                      {/* <div className='col-lg-6 col-md-6 col-sm-6 mt-4'> */}
                      <div className="hover-scroll-overlay-y h-190px px-5 MB-2">
                        <span className='fw-bold text-bold'>
                          {
                            !_.isEmpty(
                              websiteCatgories &&
                              websiteCatgories.categories
                            ) ?
                              websiteCatgories &&
                              websiteCatgories.categories.map((item, i) => {
                                return (
                                  <div key={"GG_" + i}>
                                    {
                                      !_.isEmpty(item.tier1 && item.tier1.name) ? (
                                        <div className='text-black'>
                                          <span className="bullet bullet-dot me-5  bg-success" />
                                          {
                                            item.tier1 && item.tier1.name
                                          }
                                        </div>
                                      )
                                        :
                                        ('')
                                    }
                                    {
                                      !_.isEmpty(item.tier2 && item.tier2.name) ? (
                                        <div className='text-black'>
                                          <span className="bullet bullet-dot me-5  bg-success" />
                                          {
                                            item.tier2 && item.tier2.name
                                          }
                                        </div>
                                      ) :
                                        ('')
                                    }
                                  </div>
                                )
                              })
                              :
                              <span
                                className='text-danger'
                              >
                                No Category Found
                              </span>
                          }
                        </span>
                      </div>
                      {/* </div> */}
                    </div>
                    <div className='form-group row mb-4'>
                      <div className="form-check form-check-custom form-check-solid mt-8">
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => rejectChange(e)}
                              value='blockList'
                              name='type'
                              checked={formData.type === 'blockList'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-black'>
                              Blocklist Categories
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              name='type'
                              onChange={(e) => rejectChange(e)}
                              value='whiteList'
                              checked={formData.type === 'whiteList'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-black'>
                              Whitelist Categories
                            </span>
                          </span>
                        </label>
                      </div>
                      {errors.categories && (
                        <div className='fv-plugins-message-container text-danger'>
                          <span role='alert text-danger'>{errors.categories}</span>
                        </div>
                      )}
                      <div className='row'>
                        <div className='col-lg-12 col-md-12 col-sm-12 mb-3 mt-4'>

                          <div className='row mb-2 align-items-cente'>
                            <div className='col-lg-6 col-md-6 col-sm-6'>
                              {
                                formData.type === 'blockList' ? (
                                  <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                                    <div className='flex-column mt-4 w-300px'>
                                      <ReactSelect
                                        styles={customStyles}
                                        isMulti={false}
                                        name='AppUserId'
                                        className='select2'
                                        classNamePrefix='select'
                                        handleChangeReactSelect={handleChangeBlockList}
                                        options={BlockLIstOption}
                                        value={SelectedBlockLIstOption}
                                        isDisabled={!BlockLIstOption}
                                      />
                                    </div>
                                    {errors.reason && (
                                      <div className='fv-plugins-message-container text-danger'>
                                        <span role='alert text-danger'>{errors.reason}</span>
                                      </div>
                                    )}
                                  </div>
                                ) :
                                  (
                                    formData.type === 'whiteList' ? (
                                      <>
                                        <div className='col-lg-11 col-md-11 col-sm-11 mt-4'>
                                          <div className='flex-column mt-4 w-300px'>
                                            <ReactSelect
                                              styles={customStyles}
                                              isMulti={false}
                                              name='AppUserId'
                                              className='select2'
                                              classNamePrefix='select'
                                              handleChangeReactSelect={handleChangeWhiteList}
                                              options={WhiteOption}
                                              value={SelectedWhiteOption}
                                              isDisabled={!WhiteOption}
                                            />
                                          </div>
                                          {errors.reason && (
                                            <div className='fv-plugins-message-container text-danger'>
                                              <span role='alert text-danger'>{errors.reason}</span>
                                            </div>
                                          )}
                                        </div>
                                      </>
                                    ) : null
                                  )
                              }
                            </div>
                            <div className='col-lg-6 col-md-6 col-sm-6 mt-4'>
                              {
                                showOthers || categoryValue
                                  // || _.isEmpty(categoryValue) 
                                  ? (
                                    <div className='col-lg-11 col-md-11 col-sm-11 mb-2 w-300px'>
                                      <input
                                        name='category'
                                        type='text'
                                        className={'form-control form-control-lg form-control-solid'}
                                        placeholder='Category'
                                        onChange={(e) => rejectChange(e)}
                                        autoComplete='off'
                                        value={formData.category || ''}
                                      // value={() => { categoryValue ? formData.category : '' }}

                                      />
                                      {errors.category && (
                                        <div className='fv-plugins-message-container text-danger'>
                                          <span role='alert text-danger'>{errors.category}</span>
                                        </div>
                                      )}
                                    </div>
                                  ) : ('')
                              }
                            </div>
                          </div>
                        </div>
                      </div>
                      {
                        !_.isEmpty(formData.status) ? (
                          <div className='form-group row mb-4 mt-4'>
                            <div className='col-lg-6' />
                            <div className='col-lg-6'>
                              <div className='col-lg-11 d-flex justify-content-end me-4 mb-4 mt-4'>
                                <button type="button"
                                  className="btn btn-light-primary me-10 btn-sm"
                                  onClick={() => Submit()}
                                >
                                  {!Categoryloading && <span className='indicator-label'>
                                    <KTSVG path="/media/icons/duotune/finance/fin001.svg" />
                                    {
                                      formData.status === 'APPROVED' ? 'Approve'
                                        : formData.status === 'REJECTED' ? 'Reject' : ''
                                    }
                                  </span>}
                                  {Categoryloading && (
                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                      Please wait...
                                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                    </span>
                                  )}
                                </button>
                              </div>
                            </div>
                          </div>
                        ) :
                          ''
                      }
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <a className="btn btn-sm btn-light-danger fw-bolder px-4 me-1"
          data-toggle='modal'
          data-target='#categoryModal'
          onClick={() => setActive(true)}
        >
          <KTSVG path="/media/icons/duotune/files/fil021.svg" />
          Category
        </a>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    editAMLStore,
    ApproveAMLStore,
    postAsigneeStore,
    WhiteListStore,
    BlockListTypeStore,
    PostCategoryStore,
    GetCategoryStore
  } = state

  return {
    ApproveAMLResponceData: ApproveAMLStore && ApproveAMLStore.ApproveAMLResponce ? ApproveAMLStore.ApproveAMLResponce : {},
    loading: ApproveAMLStore && ApproveAMLStore.loading ? ApproveAMLStore.loading : false,
    AMLGetById: editAMLStore && editAMLStore.AMLIdDetail ? editAMLStore.AMLIdDetail : {},
    getAssignee: postAsigneeStore && postAsigneeStore.getAssignee ? postAsigneeStore.getAssignee : {},
    whitelistTypeData: WhiteListStore && WhiteListStore.whitelistTypeData ? WhiteListStore.whitelistTypeData : null,
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType?.data : null,
    PostCategory: PostCategoryStore && PostCategoryStore.PostCategory ? PostCategoryStore.PostCategory?.data : '',
    Categoryloading: PostCategoryStore && PostCategoryStore.loading ? PostCategoryStore.loading : '',
    getCategoryList: GetCategoryStore && GetCategoryStore.getCategory && GetCategoryStore.getCategory.data ? GetCategoryStore.getCategory.data : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getAMLIdDispatch: (id) => dispatch(AMLGetIdActions.getAMLIdDetails(id)),
  ApproveAMLDispatch: (id, params) => dispatch(ApproveAMLActions.ApproveAML(id, params)),
  clearApproveAML: () => dispatch(ApproveAMLActions.clearApproveAML()),
  getAMLqueuelistDispatch: (params) => dispatch(AMLqueueActions.getAMLqueuelist(params)),
  TransactionStatusAmlDispatch: (params) => dispatch(TransactionAMLActions.TransactionAML(params)),
  getBlockListTypeDispatch: (params) => dispatch(BlockListEmailActions.getBlockListType(params)),
  fetchWhitelistTypeDispatch: (params) => dispatch(WhiteListActions.fetchWhitelistType(params)),
  PostCategroyDispatch: (params) => dispatch(PostCategoryActions.PostCategroy(params)),
  ClearPostCategroy: () => dispatch(PostCategoryActions.ClearPostCategroy()),
  GetCategroyDispatch: (params) => dispatch(GetCategoryActions.GetCategroy(params)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Category)