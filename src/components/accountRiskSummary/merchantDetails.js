import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import { useLocation } from 'react-router-dom'
import { merchantIdDetailsActions, GetAsigneeActions, AsiggnActions } from '../../store/actions'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import _ from 'lodash'
import { getLocalStorage } from '../../utils/helper'
import PriceCheckPopup from './../shared-components/PriceCheckPopup'
import Category from './Category'
import FindRole from '../riskManagement/Role'

function MerchantDetails(props) {
  const {
    className,
    getRiskSummaryDispatch,
    loading,
    phone,
    dashboardDetails,
    riskmgmtlistdetails,
    merchantIddetails,
    setOpenPhone,
    setOpenEmail,
    setOpenAddress,
    setOpenIpAddress,
    setOpenBusinessAddress,
    setopenMap,
    setopenBusinessMap,
    merchantIdDetail,
    merchantSummary,
    getAssignee,
    getAsigneeslistDispatch,
    updateAssignDispatch,
    updateAssign,
    clearAsiggnDispatch,
    WebRiskScore,
    DashboardExportData
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [formData, setFormData] = useState({
    assignedTo: ''
  })
  const [value, setValue] = useState()
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))

  useEffect(() => {
    const params = {
      tag: "IRM"
    }
    getAsigneeslistDispatch(params)
  }, [])

  const StatusArray = {
    "PENDING": "badge-light-warning",
    "APPROVED": "badge-light-success",
    "REJECTED": "badge-light-danger",
    "HOLD": "badge-light-warning",
    "MANUAL REVIEW": "badge badge-orange text-black",
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, assignedTo: selectedOption.value, }))
      setValue(selectedOption.label)
    }
  }

  const AsigneesNames = getAssignee && getAssignee.data && getAssignee.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].firstName , value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const onConfirmupdate = (() => {
    updateAssignDispatch(currentId, formData)
  })

  const Assign = (() => {
    confirmationAlert(
      "Do You Want To",
      `Assign this Case #${merchantSummary && merchantSummary.riskId} to ${value}?`,
      'warning',
      'Yes',
      'No',
      () => { onConfirmupdate() },
      () => { }
    )
  })

  const onConfirm = () => {
    setFormData({
      assignedTo:''
    })
  }
  
  const clear = () => {
    setFormData({
      assignedTo:''
    })
  }

  useEffect(() => {
    if (updateAssign && updateAssign.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        "Assigned Successfully",
        'success',
        )      
        clearAsiggnDispatch()
        setFormData({
          assignedTo:''
        })
    } else if (updateAssign && updateAssign.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        updateAssign.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearAsiggnDispatch()
    }
  }, [updateAssign])

  console.log('merchantSummary', merchantSummary)

  return (
    <>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='card-header border-0'>
          <h3 className='card-title align-items-start flex-column '>
            <span className='d-flex align-items-center fw-boldest my-1 fs-2'>Case Details - Case ID #{merchantSummary && merchantSummary.riskId ? merchantSummary.riskId : '--'}
              <span className={`badge ml-2 ${ merchantSummary && merchantSummary.riskStatus ? StatusArray[ merchantSummary && merchantSummary.riskStatus] : ""}`} style={{ padding: "4px 12px", marginLeft: "10px", fontSize: "14px" }}>
                {merchantSummary && merchantSummary.riskStatus ? merchantSummary.riskStatus : ""}
              </span>
            </span>
          </h3>
          <div className='col-lg-4'/>
          {/* {
            Role === 'Admin' ? (
              <>
              <span className='mt-4 w-25'>
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name='AppUserId'
                className='select2'
                classNamePrefix='select'
                handleChangeReactSelect={handleChangeAsignees}
                options={AsigneesOption}
                value={SelectedAsigneesOption}
                isDisabled={!AsigneesOption}
              />
            </span>
            </>
            ):
            (
              null
            )
          } */}
          {
            !_.isEmpty(formData.assignedTo) ? (
              <span className='mt-4'>
              <button 
              class="btn btn-light-primary btn-sm"
              onClick={Assign}
              >
                Assign
                </button>
              </span>
            ):(
              null
            )
          }
        </div>
        <div className="separator separator-dashed my-3" />
        <div className="row row g-5 g-xl-8">
          <div className='col-sm-4 col-md-4 col-lg-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-body pt-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Case Id:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      IRM{merchantSummary && merchantSummary.riskId}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Queue:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.queueId && merchantSummary.queueId.queueName ? merchantSummary.queueId.queueName : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Date Received:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {moment(merchantSummary && merchantSummary.createdAt ? merchantSummary.createdAt : "--").format('DD/MM/YYYY')}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Name:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessName ? merchantSummary.businessName : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business URL:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.website ? merchantSummary.website : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Email:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessEmail ? merchantSummary.businessEmail : '--'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-4 col-md-4 col-lg-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-body pt-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Phone:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessPhone ? merchantSummary.businessPhone : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Address:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessAddress ? merchantSummary.businessAddress : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Customer Name:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.firstName ? merchantSummary.firstName : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Email Address:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.personalEmail ? merchantSummary.personalEmail : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Customer Address:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.address ? merchantSummary.address : '--'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-4 col-md-4 col-lg-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-body pt-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Phone Number:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.phone ? merchantSummary.phone : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Average Monthly Volume:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.averageMonthlyVolume ? merchantSummary.averageMonthlyVolume : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Average Transaction Amount:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.gmvId ? merchantSummary.gmvId : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      GSTIN:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.gstIn ? merchantSummary.gstIn : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      CIN:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.cin ? merchantSummary.cin : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      PAN:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.pan ? merchantSummary.pan : '--'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore, riskManagementlistStore, editMerchantStore, postAsigneeStore, updateAssignStore } = state

  return {
    dashboardDetails:
      dashboardStore &&
        dashboardStore.dashboardDetails
        ? dashboardStore.dashboardDetails
        : {},
    riskmgmtlistdetails:
      riskManagementlistStore &&
        riskManagementlistStore.riskmgmtlists ?
        riskManagementlistStore.riskmgmtlists : null,
    merchantIddetails: editMerchantStore && editMerchantStore.merchantIddetail ? editMerchantStore.merchantIddetail : {},
    getAssignee: postAsigneeStore && postAsigneeStore.getAssignee ? postAsigneeStore.getAssignee : {},
    updateAssign: updateAssignStore && updateAssignStore.updateAssign ? updateAssignStore.updateAssign : {}
  }
}

const mapDispatchToProps = (dispatch) => ({
  merchantIdDetail: (id) => dispatch(merchantIdDetailsActions.getmerchantDetails(id)),
  getAsigneeslistDispatch: (params) => dispatch(GetAsigneeActions.GetAsignee(params)),
  updateAssignDispatch: (id,params) => dispatch(AsiggnActions.Asiggn(id,params)),
  clearAsiggnDispatch: () => dispatch(AsiggnActions.clearAsiggn())
})

export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetails)
