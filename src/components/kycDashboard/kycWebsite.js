import React, { useEffect, useState, createRef, Fragment } from 'react'
import { connect } from 'react-redux'
import { buildStyles, CircularProgressbarWithChildren } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import { WebsiteActions } from '../../store/actions'
// import userPlaceholder from '../../../../public/media/avatars/blank.png'
import { toAbsoluteUrl } from '../../theme/helpers'
import PDF from '../riskSummary/subComponent/pdf/Pdf'
import { jsPDF } from "jspdf";
import { renderToString } from "react-dom/server";
import _ from 'lodash'
import { Link } from '@material-ui/core'
import { Scrollbars } from 'react-custom-scrollbars';

function KycWebsite(props) {
  const {
    getWebsiteDispatch,
    WebsiteResponce,
    className,
    getRiskSummarys,
    dashboardDetails,
    merchantIddetails,
    getriskScores,
    kyc_dashboard_details,
    UserDetails,
    AllDashboardRes
  } = props
  const value = 80

  useEffect(() => {
    getWebsiteDispatch()
  }, [])
  const websiteLink = merchantIddetails && merchantIddetails.data && merchantIddetails.data.website ? merchantIddetails.data.website : '--'
  const viewData = getRiskSummarys && getRiskSummarys && getRiskSummarys.data ? getRiskSummarys.data : []
  const getData = viewData.filter(o => (o ? o : null))
  const website = getData && getData[0] && getData[0].websiteContacts ? getData[0].websiteContacts : '--'
  const socialLink = website && website.socialLinks && website.socialLinks ? website.socialLinks : '--'
  const domainAvailability = getData && getData[0] && getData[0].domainAvailability ? getData[0].domainAvailability : '--'
  const administrative = domainAvailability && domainAvailability.administrativeContact ? domainAvailability.administrativeContact : '--'
  const speedometetervalue = dashboardDetails && dashboardDetails.data && dashboardDetails.data ? dashboardDetails.data : '--'
  // const ValidData = getData && getData[0] ? getData[0] : '--'
  const ValidData = AllDashboardRes && AllDashboardRes.data && AllDashboardRes.data.website

  const sslValue = ValidData && ValidData.sslVulnerabilityInfo ? ValidData.sslVulnerabilityInfo : '--'
  const domainHistoryData = ValidData && ValidData.domainHistory && ValidData.domainHistory.records ? ValidData.domainHistory.records : []
  const domainHistory = domainHistoryData.filter(o => (o ? o : null))
  const domainHistoryMap = domainHistory[0] && domainHistory[0].nameServers ? domainHistory[0] && domainHistory[0].nameServers : '--'
  const domainInfoData = ValidData && ValidData.domain_info ? ValidData.domain_info : '--'
  const successVerifyDomain = ValidData && ValidData.verify_domain ? ValidData.verify_domain : null
  const successWhoDomain = ValidData && ValidData.who_domain ? ValidData.who_domain : null
  const webAnalysis = ValidData && ValidData.web_risk_analysis_status ? ValidData.web_risk_analysis_status : null
  const webRiskAnalysis = ValidData && ValidData.webRiskAnalysis ? ValidData.webRiskAnalysis : null
  const policyComplianceCheck = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ? webAnalysis.data.policy_complaince_checks : null
  const shippingComplianceCheck = webAnalysis && webAnalysis.data && webAnalysis.data.product_pricing_checks ? webAnalysis.data.product_pricing_checks : null
  const userInteractionChecks = webAnalysis && webAnalysis.data && webAnalysis.data.user_interaction_checks ? webAnalysis.data.user_interaction_checks : null
  const webContentMonitoring = webAnalysis && webAnalysis.data && webAnalysis.data.web_content_monitoring ? webAnalysis.data.web_content_monitoring : null
  const websitetobusinessmatch = webAnalysis && webAnalysis.data && webAnalysis.data.website_to_business_name_match ? webAnalysis.data.website_to_business_name_match : null
  const domainRepetation = ValidData && ValidData.domainReputation ? ValidData.domainReputation : null
  const DNSData = ValidData && ValidData.dns_lookup ? ValidData.dns_lookup : null
  const adminContact = domainInfoData && domainInfoData.administrativeContact ?
    domainInfoData.administrativeContact : '--'
  const registerData = domainInfoData && domainInfoData.registrant ? domainInfoData.registrant : '--'
  const adminContactData = domainInfoData && domainInfoData.administrativeContact ? domainInfoData.administrativeContact : '--'
  const websiteCatgories = ValidData && ValidData.websiteCategorization && ValidData.websiteCategorization.message ? ValidData.websiteCategorization.message : '--'
  const reviewAnalysis = ValidData && ValidData.reviewAnalysis ? ValidData.reviewAnalysis : '--'
  const websiteImageDetect = ValidData && ValidData.websiteImageDetect ? ValidData.websiteImageDetect : '--'
  const websiteScore = getriskScores && getriskScores.data ? getriskScores.data : '--'
  const phonevalue = Math.round(websiteScore && websiteScore.riskScore)

  const print = () => {
    const string = renderToString(<PDF />);
    const pdf = new jsPDF("p", "mm", "a1");
    let elementHandler = {
      '#ignorePDF': function (element, renderer) {
        return true;
      }
    };
    let source = window.document.getElementById("pdf_converter");

    if (string) {
      pdf.html(string).then(() => pdf.save('website.pdf'));
    }
  }

  const phonePositive =
    kyc_dashboard_details &&
      kyc_dashboard_details.data &&
      kyc_dashboard_details.data.phonepositive
      ? kyc_dashboard_details.data.phonepositive
      : "--"
  const phoneNegative =
    kyc_dashboard_details &&
      kyc_dashboard_details?.data &&
      kyc_dashboard_details.data.phonenegative
      ? kyc_dashboard_details.data.phonenegative
      : "--"

  const websiteTotal = kyc_dashboard_details && kyc_dashboard_details.data && kyc_dashboard_details.data.websiteTotalScore ? kyc_dashboard_details.data.websiteTotalScore : 0

  const websiteName = UserDetails && UserDetails.website ? UserDetails.website : '--'

  return (

    <div>
      <div>
        <div className='row mt-8'>
          <div className='col-lg-4' />
          <div className='col-lg-4'>
            <h1 className='d-flex justify-content-center mb-4'>Website
              <span>
                <a
                  className='text-hover-primary fs-4 ms-4'
                  style={{
                    cursor: 'pointer'
                  }}
                  href={`${websiteName}`}
                > - {websiteName}</a>
              </span>
            </h1>
          </div>
        </div>
        <div className='d-flex justify-content-center mb-4'>
          <CircularProgressbarWithChildren
            value={websiteTotal}
            text={`${websiteTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: 'butt',
              textColor: 'mediumseagreen',
              // pathColor: 'mediumseagreen',
              pathColor: '#ed5555',
              trailColor: "mediumseagreen"
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {websiteTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className='row'>
          <div className='mb-8'>
            <a href='#' className='d-flex justify-content-center fs-2'>CONFIDENCE MATRIX</a>
          </div>
        </div>
        <div className='row g-5 g-xl-8 mb-8' >

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Domain Info
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row mb-2 align-items-cente'>
                    <div className='align-items-center  rounded p-2 mb-0'>
                      <div className='row mb-2 align-items-cente'>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                          <span className='text-gray-700 fw-bold  pl-3'>
                            Domain Registration Company
                          </span>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                          <span className='ffw-bold text-bold   '>
                            {
                              domainInfoData && domainInfoData.registrarName
                                ? domainInfoData.registrarName
                                : '--'
                            }
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className='align-items-center  rounded p-2 mb-0'>
                      <div className='row mb-2 align-items-cente'>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                          <span className='text-gray-700 fw-bold  pl-3'>
                            Parent
                          </span>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                          <span className='ffw-bold text-bold   '>
                            {
                              successVerifyDomain &&
                                successVerifyDomain.domain ? successVerifyDomain.domain
                                : '--'
                            }
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-2 align-items-cente'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Mode
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            domainRepetation && domainRepetation.mode ? domainRepetation.mode : '--'
                          }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2 align-items-cente'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Reputation Score
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            domainRepetation && domainRepetation.reputationScore ? domainRepetation.reputationScore : '--'
                          }
                        </span>
                      </div>
                    </div>
                    <div className='mt-4'>
                      <h4 className='d-flex justify-content-center '>
                        <span className='card-label fw-bolder text-dark'>
                          Administrative Contact
                        </span>
                      </h4>
                      <div className='align-items-center  rounded p-2 mb-0'>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              State
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.state ?
                                  adminContact.state : adminContactData.state ? adminContactData.state :
                                    successVerifyDomain && successVerifyDomain.geo && successVerifyDomain.geo.state
                                      ? successVerifyDomain.geo.state : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              City
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.city ?
                                  adminContact.city : adminContactData.city ? adminContactData.city : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Country
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.country ?
                                  adminContact.country : adminContactData.country ? adminContactData.country : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Country Code
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.countryCode ?
                                  adminContact.countryCode : adminContactData.countryCode ? adminContactData.countryCode : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Email
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.email ?
                                  adminContact.email : adminContactData.email ? adminContactData.email : domainInfoData.contactEmail ? domainInfoData.contactEmail
                                    : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Name
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.name ?
                                  adminContact.name : adminContactData.name ? adminContactData.name : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Telephone
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.telephone ?
                                  adminContact.telephone : adminContactData.telephone ? adminContactData.telephone : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Organization
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.organization ?
                                  adminContact.organization : adminContactData.organization ? adminContactData.organization : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              PostalCode
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.postalCode ?
                                  adminContact.postalCode : adminContactData.postalCode ? adminContactData.postalCode : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Street1
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.street1 ?
                                  adminContact.street1 : adminContactData.street1 ? adminContactData.street1 : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2 align-items-cente'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Telephone
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                adminContact.telephone ?
                                  adminContact.telephone : adminContactData.telephone ? adminContactData.telephone : '--'
                              }
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Estimated Domain Age
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {domainInfoData && domainInfoData.estimatedDomainAge ? domainInfoData.estimatedDomainAge : '--'}
                      </span>
                    </div>
                  </div>


                  <div className='row mb-2 align-items-cente'>
                    <div className='mt-4'>
                      <h4 className='d-flex justify-content-center '>
                        <span className='card-label fw-bolder text-dark'>
                          Registrant
                        </span>
                      </h4>
                      <div className='align-items-center  rounded p-2 mb-0'>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Domain Registered Date
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                !_.isEmpty(domainInfoData && domainInfoData.createdDateNormalized) ?
                                  moment(domainInfoData && domainInfoData.createdDateNormalized ? domainInfoData.createdDateNormalized : '__').format('DD/MM/YYYY')
                                  : '--'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Domain Expiry Date
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                !_.isEmpty(domainInfoData && domainInfoData.expiresDate) ?
                                  moment(domainInfoData && domainInfoData.expiresDateNormalized ? domainInfoData.expiresDateNormalized : '__').format('DD/MM/YYYY')
                                  : '--'
                              }
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row mb-2 align-items-cente'>
                    <div className='mt-4'>
                      <h4 className='d-flex justify-content-center '>
                        <span className='card-label fw-bolder text-dark'>
                          Geo
                        </span>
                      </h4>
                      <div className='align-items-center  rounded p-2 mb-0'>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              City
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.city
                                  ? successVerifyDomain.geo.city : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Country
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.country
                                  ? successVerifyDomain.geo.country : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Country Code
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.countryCode
                                  ? successVerifyDomain.geo.countryCode : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Latitude
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.lat
                                  ? successVerifyDomain.geo.lat : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Longitude
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.lng
                                  ? successVerifyDomain.geo.lng : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              postal Code
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.postalCode
                                  ? successVerifyDomain.geo.postalCode : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              State
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.state
                                  ? successVerifyDomain.geo.state : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              State Code
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.stateCode
                                  ? successVerifyDomain.geo.stateCode : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Street Name
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.streetName
                                  ? successVerifyDomain.geo.streetName : '__'
                              }
                            </span>
                          </div>
                        </div>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Street Number
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='ffw-bold text-bold   '>
                              {
                                successVerifyDomain && successVerifyDomain.geo &&
                                  successVerifyDomain.geo.streetNumber
                                  ? successVerifyDomain.geo.streetNumber : '__'
                              }
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row mb-2 align-items-cente'>
                    <div className='mt-4'>
                      <h4 className='d-flex justify-content-center '>
                        <span className='card-label fw-bolder text-dark'>
                          Name Servers
                        </span>
                      </h4>
                      <div className='align-items-center  rounded p-2 mb-0'>
                        <div className='row mb-2'>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            <span className='text-gray-700 fw-bold  pl-3'>
                              Host Names
                            </span>
                          </div>
                          <div className='col-lg-6 col-md-6 col-sm-6'>
                            {
                              !_.isEmpty(registerData && registerData.nameServers && registerData.nameServers.hostNames) ? (
                                registerData && registerData.nameServers && registerData.nameServers.hostNames.map((item, i) => {
                                  return (
                                    <div key={"AA_" + i}>
                                      <div className='ffw-bold text-bold '>
                                        {item}
                                      </div>
                                    </div>

                                  )
                                })
                              ) : (
                                'No Data'
                              )

                            }
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='row mb-4'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Logo
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        <img
                          src={
                            successVerifyDomain && successVerifyDomain.irmLogo ? successVerifyDomain.irmLogo
                              : (successVerifyDomain && successVerifyDomain.logo ? successVerifyDomain.logo : "")
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      </span>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Dnssec
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {successWhoDomain && successWhoDomain.dnssec ? successWhoDomain.dnssec : '--'}
                      </span>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Org
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {successWhoDomain && successWhoDomain.org ? successWhoDomain.org : '--'}
                      </span>
                    </div>
                  </div>
                  <div className='row mt-2'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Domain Aliases
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      {
                        !_.isEmpty(successVerifyDomain && successVerifyDomain.domainAliases) ?
                          successVerifyDomain && successVerifyDomain.domainAliases.slice(0, 6).map((item, i) => {
                            return (
                              <div key={"BB_" + i}>
                                <span className='ffw-bold text-bold   '>
                                  {item}
                                </span>

                              </div>
                            )
                          })

                          : '--'
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Social Media
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Facebook
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Handle
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.facebook && successVerifyDomain.facebook.handle ? successVerifyDomain.facebook.handle : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Twitter
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Avatar
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            <img
                              src={successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.avatar ? successVerifyDomain.twitter.avatar : '--'}
                            />
                          }
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Bio
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.bio ? successVerifyDomain.twitter.bio : '--'}
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Followers
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.followers ? successVerifyDomain.twitter.followers : '--'}
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Handle
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.handle ? successVerifyDomain.twitter.handle : '--'}
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Id
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.id ? successVerifyDomain.twitter.id : '--'}
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Location
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.location ? successVerifyDomain.twitter.location : '--'}
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Site
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <Link>
                          <span className='ffw-bold text-bold   '>
                            {successVerifyDomain && successVerifyDomain.twitter && successVerifyDomain.twitter.site ? successVerifyDomain.twitter.site : '--'}
                          </span>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Linkedin
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Handle
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.linkedin && successVerifyDomain.linkedin.handle ? successVerifyDomain.linkedin.handle : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Web Analysis Status
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>

                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Page Activity Check
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Is Mining Happening?
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_activity_check &&
                            webAnalysis.data.page_analysis_results.page_activity_check.is_mining_happening
                            ? webAnalysis.data.page_analysis_results.page_activity_check.is_mining_happening : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Any Untrusted Downloads?
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_activity_check &&
                            webAnalysis.data.page_analysis_results.page_activity_check.untrusted_downloads
                            ? webAnalysis.data.page_analysis_results.page_activity_check.untrusted_downloads : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Page Availability Check
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Url Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_availability_check &&
                            webAnalysis.data.page_analysis_results.page_availability_check.url_status
                            ? webAnalysis.data.page_analysis_results.page_availability_check.url_status : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Page Health Check
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Content Accessibilty
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_health_check &&
                            webAnalysis.data.page_analysis_results.page_health_check.content_accessibilty
                            ? webAnalysis.data.page_analysis_results.page_health_check.content_accessibilty : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Page loading Time
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_health_check &&
                            webAnalysis.data.page_analysis_results.page_health_check.page_loading_time
                            ? webAnalysis.data.page_analysis_results.page_health_check.page_loading_time : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Page Links Connectivity Check
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_links_connectivity_check &&
                            webAnalysis.data.page_analysis_results.page_links_connectivity_check.status
                            ? webAnalysis.data.page_analysis_results.page_links_connectivity_check.status : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Sucess Rate
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_links_connectivity_check &&
                            webAnalysis.data.page_analysis_results.page_links_connectivity_check.sucess_rate
                            ? webAnalysis.data.page_analysis_results.page_links_connectivity_check.sucess_rate : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Page Redirection Check
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Domain Redirection
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.page_analysis_results &&
                            webAnalysis.data.page_analysis_results && webAnalysis.data.page_analysis_results.page_redirection_check &&
                            webAnalysis.data.page_analysis_results.page_redirection_check.domain_redirection
                            ? webAnalysis.data.page_analysis_results.page_redirection_check.domain_redirection : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Malware Monitoring
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Message
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {ValidData && ValidData.safeBrowsing &&
                          ValidData.safeBrowsing.data && ValidData.safeBrowsing.data.message
                          ? ValidData.safeBrowsing.data.message : '--'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Domain Reputation
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row mb-2 align-items-cente'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Mode
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          domainRepetation && domainRepetation.mode ? domainRepetation.mode : '--'
                        }
                      </span>
                    </div>
                  </div>
                  <div className='row mb-2 align-items-cente'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Reputation Score
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          domainRepetation && domainRepetation.reputationScore ? domainRepetation.reputationScore : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    DNS Data
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row mb-2 align-items-cente'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Dns Types
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          DNSData && DNSData.dnsTypes ? DNSData.dnsTypes : '--'
                        }
                      </span>
                    </div>
                  </div>
                  <div className='row mb-2 align-items-cente'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Domain Name
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          DNSData && DNSData.domainName ? DNSData.domainName : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Policy Complaince Checks
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Contact
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.status
                            ? webAnalysis.data.policy_complaince_checks.contact.status : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Contains Valid Email
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_email
                            ? webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_email : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Contains Valid Phone
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_phone
                            ? webAnalysis.data.policy_complaince_checks.contact.details.contains_valid_phone : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Contacts Form Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.contacts_form &&
                            webAnalysis.data.policy_complaince_checks.contact.details.contacts_form.status
                            ? webAnalysis.data.policy_complaince_checks.contact.details.contacts_form.status : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Email
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        {webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                          webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                          webAnalysis.data.policy_complaince_checks.contact.details &&
                          webAnalysis.data.policy_complaince_checks.contact.details.emails
                          ? webAnalysis.data.policy_complaince_checks.contact.details.emails : '--'}

                        {
                          !_.isEmpty(
                            webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.email
                          ) ? (

                            webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.email.map((item, i) => {
                              return (
                                <span key={"EE_" + i} className='ffw-bold text-bold'>
                                  {
                                    item
                                  }
                                </span>
                              )
                            })
                          )
                            : null
                        }
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Phone Number
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        {
                          !_.isEmpty(
                            webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.phone_number
                          ) ? (
                            webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks &&
                            webAnalysis.data.policy_complaince_checks && webAnalysis.data.policy_complaince_checks.contact &&
                            webAnalysis.data.policy_complaince_checks.contact.details &&
                            webAnalysis.data.policy_complaince_checks.contact.details.phone_number.map((item, i) => {
                              return (
                                <Fragment key={"FF_" + i}>
                                  <div className='ffw-bold text-bold ' key={i}>
                                    {item}
                                  </div>
                                </Fragment>
                              )
                            })
                          ) : null
                        }
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Privacy
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Policy Screenshot
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          <img
                            src={policyComplianceCheck && policyComplianceCheck.privacy && policyComplianceCheck.privacy.policy_screenshot
                              ? policyComplianceCheck.privacy.policy_screenshot : '--'}
                          />
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {policyComplianceCheck && policyComplianceCheck.privacy &&
                            policyComplianceCheck.privacy.status ? policyComplianceCheck.privacy.status : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Return
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            policyComplianceCheck && policyComplianceCheck.return && policyComplianceCheck.return.status
                              ? policyComplianceCheck.return.status : '--'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Shipping
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            policyComplianceCheck && policyComplianceCheck.shipping && policyComplianceCheck.shipping.status
                              ? policyComplianceCheck.shipping.status : '--'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Terms and Conditions
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            policyComplianceCheck && policyComplianceCheck.terms_and_conditions && policyComplianceCheck.terms_and_conditions.status
                              ? policyComplianceCheck.terms_and_conditions.status : '--'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Product Pricing Checks
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  {/* <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Status
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          shippingComplianceCheck && shippingComplianceCheck.status
                            ? shippingComplianceCheck.status : '--'
                        }
                      </span>
                    </div>
                  </div> */}
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Message
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          shippingComplianceCheck && shippingComplianceCheck.message
                            ? shippingComplianceCheck.message : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    User Interaction Checks
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Bank Details Sumission
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          userInteractionChecks && userInteractionChecks.bank_details_sumission
                            ? userInteractionChecks.bank_details_sumission : '--'
                        }
                      </span>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Id Submission prompt
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          userInteractionChecks && userInteractionChecks.id_submission_prompt
                            ? userInteractionChecks.id_submission_prompt : '--'
                        }
                      </span>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Seller Redirection
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          userInteractionChecks && userInteractionChecks.seller_redirection
                            ? userInteractionChecks.seller_redirection : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Web Content Monitoring
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0 mb-2'>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Website To Business Name Match
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Mcc Code Match
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            websitetobusinessmatch && websitetobusinessmatch.mcc_code_match
                              ? websitetobusinessmatch.mcc_code_match : '--'
                          }
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Name Match Rating
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            websitetobusinessmatch && websitetobusinessmatch.name_match_rating
                              ? websitetobusinessmatch.name_match_rating : '--'
                          }
                        </span>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Content Keyword Risk
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {
                            webContentMonitoring && webContentMonitoring.content_keyword_risk
                              ? webContentMonitoring.content_keyword_risk : '--'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>


          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Organization Address - Transparency
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Location
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {successVerifyDomain && successVerifyDomain.location ? successVerifyDomain.location : '--'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    SSL CheckInfo
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Message
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {ValidData && ValidData.ssl_check && ValidData.ssl_check.message ? ValidData.ssl_check.message : '__'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    SSL Vulnerability Info
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        SSL Vulnerability Info
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {ValidData && ValidData.sslVulnerabilityInfo && ValidData.sslVulnerabilityInfo.breach_check ? ValidData.sslVulnerabilityInfo.breach_check : '__'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        SSL connection Message
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {sslValue && sslValue.ssl_connection && sslValue.ssl_connection.message ? sslValue.ssl_connection.message : '--'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Predict Medicine
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Result
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          ValidData && ValidData.predictMedicine && ValidData.predictMedicine.result ?
                            ValidData.predictMedicine.result : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Score
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          ValidData && ValidData.predictMedicine && ValidData.predictMedicine.score ?
                            ValidData.predictMedicine.score : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Website Details
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        LegalName
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          successVerifyDomain &&
                            successVerifyDomain.legalName ?
                            successVerifyDomain.legalName :
                            // successVerifyDomain.name ? 
                            // successVerifyDomain.name : 
                            '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Logo
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        <img
                          src={successVerifyDomain && successVerifyDomain.logo ? successVerifyDomain.logo : '--'}
                        />
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Shopify
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {ValidData && ValidData.websiteDetails && ValidData.websiteDetails.shopify ? ValidData.websiteDetails.shopify : '__'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Method
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {ValidData && ValidData.websiteDetails && ValidData.websiteDetails.method ? ValidData.websiteDetails.method : '__'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Woo Commerce
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {ValidData && ValidData.websiteDetails && ValidData.websiteDetails.woocommerce ? ValidData.websiteDetails.woocommerce : '__'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Description
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {successVerifyDomain && successVerifyDomain.description ? successVerifyDomain.description : '--'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Website Categorization
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Domain Name
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {
                          ValidData &&
                            ValidData.websiteCategorization &&
                            ValidData.websiteCategorization.domainName
                            ?
                            ValidData.websiteCategorization.domainName
                            : '--'

                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark'>
                      Category
                    </span>
                  </h4>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Industry
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.industry ? successVerifyDomain.category.industry : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Industry Group
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.industryGroup ? successVerifyDomain.category.industryGroup : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Sector
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.sector ? successVerifyDomain.category.sector : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          SubIndustry
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='ffw-bold text-bold   '>
                          {successVerifyDomain && successVerifyDomain.category && successVerifyDomain.category.subIndustry ? successVerifyDomain.category.subIndustry : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Name
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <div className="hover-scroll-overlay-y h-190px px-5">
                          <span className='ffw-bold text-bold'>
                            {
                              !_.isEmpty(
                                websiteCatgories &&
                                websiteCatgories.categories
                              ) ?
                                websiteCatgories &&
                                websiteCatgories.categories.map((item, i) => {
                                  return (
                                    <div key={"GG_" + i}>
                                      <div>
                                        {
                                          item.tier1 && item.tier1.name
                                        } - {
                                          item.tier2 && item.tier2.name
                                        },
                                      </div>
                                    </div>
                                  )
                                })
                                : '--'
                            }
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Tag
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <div className="hover-scroll-overlay-y h-190px px-5">
                          <span className='ffw-bold text-bold'>
                            {
                              !_.isEmpty(successVerifyDomain && successVerifyDomain.tags) ?
                                successVerifyDomain && successVerifyDomain.tags.map((item, i) => {
                                  return (
                                    <Fragment key={"HH_" + i}>
                                      <div>
                                        {item}
                                      </div>
                                    </Fragment>
                                  )
                                })
                                : '--'
                            }
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-gray-700 fw-bold  pl-3'>
                          Categories
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        {
                          !_.isEmpty(
                            ValidData && ValidData.websiteCategorization &&
                            ValidData.websiteCategorization.categories
                          ) ?
                            ValidData && ValidData.websiteCategorization &&
                            ValidData.websiteCategorization.categories.map((item, i) => {
                              return (
                                <div className="px-5">
                                  <span className='ffw-bold text-bold' key={"II_" + i}>
                                    <div>
                                      {
                                        item && item.tier1 && item.tier1.name
                                      }
                                    </div>
                                    <div>
                                      {
                                        item && item.tier2 && item.tier2.name
                                      }
                                    </div>
                                  </span>
                                </div>
                              )
                            })
                            : '--'
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Google Review Analysis
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Average Rating
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {reviewAnalysis && reviewAnalysis.average_rating ? reviewAnalysis.average_rating : '--'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Status
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {reviewAnalysis && reviewAnalysis.status ? reviewAnalysis.status : '--'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='col-xl-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Adult Content Monitoring
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='align-items-center  rounded p-2 mb-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-gray-700 fw-bold  pl-3'>
                        Query Response
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='ffw-bold text-bold   '>
                        {websiteImageDetect &&
                          websiteImageDetect.result &&
                          websiteImageDetect.result.query_response
                          ?
                          websiteImageDetect.result.query_response
                          : '--'
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore, WebsiteStore, riskScoreStore } = state
  return {
    WebsiteResponce: WebsiteStore && WebsiteStore.WebsiteResponce ? WebsiteStore.WebsiteResponce : {},
    getRiskSummarys:
      state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    dashboardDetails:
      dashboardStore &&
        dashboardStore.dashboardDetails
        ? dashboardStore.dashboardDetails
        : {},
    getriskScores: riskScoreStore && riskScoreStore.getriskScores ? riskScoreStore.getriskScores : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getWebsiteDispatch: (params) => dispatch(WebsiteActions.getWebsitelist(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(KycWebsite)