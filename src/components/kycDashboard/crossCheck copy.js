import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { riskSummaryActions, matrixActions } from "../../store/actions";
import _ from "lodash";
import MapGoogle from "../maps/MapGoogle";
// import StreetMap from "../../maps/StreetMap";
import StreetView from '../merchant/subComponent/StreetView'

function CrossCheck(props) {
  const {
    className,
    getRiskSummaryDispatch,
    loading,
    getRiskSummarys,
    phone,
    id,
    getMatrixDispatch,
    merchantIddetails,
    matrixDetail,
    distanceData,
    isLoaded,
    merchantSummary,
    DistanceRes
  } = props

  const viewData =
    distanceData && distanceData && distanceData ? distanceData : [];
  const getData = viewData.filter((o) => (o ? o : null));
  const splitData = getData && getData[0] ? getData[0] : "--";
  const individualAddress =
    splitData && splitData && splitData.address ? splitData.address : "--";
  const businessAddress =
    splitData && splitData && splitData.businessAddress
      ? splitData.businessAddress
      : "--";
  const INphoneKM =
    splitData && splitData && splitData.phoneDistance
      ? splitData.phoneDistance
      : "--";
  const INaddress =
    splitData && splitData && splitData.address ? splitData.address : "--";
  const INIPaddress =
    splitData && splitData && splitData.ipDistance
      ? splitData.ipDistance
      : "--";
  const BusinessPhone =
    splitData && splitData && splitData.businessPhoneDistance
      ? splitData.businessPhoneDistance
      : "--";
  const BusinessAddressDistance =
    splitData && splitData && splitData.businessAddressDistance
      ? splitData.businessAddressDistance
      : "--";
  const merchant =
    merchantIddetails && merchantIddetails.data ? merchantIddetails.data : "--";
  const viewAddress =
    getData && getData[0] && getData[0].businessAddressCheck
      ? getData[0].businessAddressCheck
      : "--";
  const riskId = merchant && merchant.riskId ? merchant.riskId : "--";
  const [markerID, setMarkerID] = useState(null);

  const [selectedElement, setSelectedElement] = useState(null);
  const [activeMarker, setActiveMarker] = useState(null);
  const [showInfoWindow, setInfoWindowFlag] = useState(true);


  const DistanceMapData = DistanceRes && DistanceRes.data

  let allMarkers = [];
  if (DistanceMapData && DistanceMapData.businessAddressLocation) {
    allMarkers.push({
      lat: DistanceMapData && DistanceMapData.businessAddressLocation.lat,
      lng: DistanceMapData && DistanceMapData.businessAddressLocation.long,
      area: "BUSINESSADDRESS"
    })
  }
  if (DistanceMapData && DistanceMapData.phoneLocation) {
    allMarkers.push({
      lat: DistanceMapData && DistanceMapData.phoneLocation.lat,
      lng: DistanceMapData && DistanceMapData.phoneLocation.long,
      area: "PHONE"
    })
  }

  return (
    <>
      <div className="container-fixed">
        <h1 className="d-flex justify-content-center mb-4">
          Triangulation Metrics
        </h1>
        <div className="row mt-8 mb-12">
          <div className="col-lg-6 col-md-6 col-sm-6">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Individual Address
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="d-flex align-items-center  rounded p-5 mb-0">
                  <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Resident Name
                  </span>
                  <span className="ffw-bold text-bold fs-6 ml-2">
                    {merchantSummary && merchantSummary?.contactName
                      ? merchantSummary?.contactName
                      : "--"}
                  </span>
                </div>
                <div className="d-flex align-items-center  rounded p-5 mb-0">
                  <span className="text-gray-700 fw-bold fs-5 pl-3">Phone</span>
                  <span className="ffw-bold text-bold fs-6 ml-2">
                    {merchantSummary && merchantSummary?.contactNumber
                      ? merchantSummary?.contactNumber
                      : "--"}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Business Address
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="d-flex align-items-center  rounded p-5 mb-0">
                  <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Associated Business
                  </span>
                  <span className="ffw-bold text-bold fs-6 ml-2">
                    {merchant && merchant.companyName
                      ? merchant.companyName
                      : "--"}
                  </span>
                </div>
                <div className="d-flex align-items-center  rounded p-5 mb-0">
                  <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Address
                  </span>
                  <span className="ffw-bold text-bold fs-6 ml-2">
                    {businessAddress && businessAddress
                      ? businessAddress
                      : "--"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8">
          <div className="col-lg-6 mb-4">
            <div className="card w-744px h-450px">
              {
                !_.isEmpty(allMarkers) ? (
                  <MapGoogle mapData={DistanceRes} mapMarkers={allMarkers} />
                ) :
                  (
                    <div className="text-muted">No data </div>
                  )
              }
              {/*<LocationSearchModal SplitData={splitData} AllMarkers={allMarkers} zoom={5}/>*/}
            </div>
          </div>
          <div className="col-lg-6">
            <div className="card w-744px h-450px">
              {
                !_.isEmpty(allMarkers) ? (
                  // <StreetMap mapData={DistanceMapData} />
                  <StreetView splitData={DistanceMapData}/>
                ) :
                  (
                    <div className="text-muted">No data </div>
                  )
              }
            </div>
          </div>
          <div className="col-lg-6">
            <div className="card card-xl-stretch mb-xl-8 mb-4">
              <div className="card-header border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Locations & Distances
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <h4 className="ml-2 text-muted">Individual</h4>
                    <div className="row mt-4">
                      <div className="col-lg-1 ml-2">
                        <span className="svg-icon svg-icon-success me-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-telephone-fill"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"
                            />
                          </svg>
                        </span>
                      </div>
                      <div className="col-lg-10 mb-4">
                        <span className="ml-2 card-label fw-bolder text-dark ">
                          Phone
                        </span>
                        <h5 className="ml-2 card-label fw-bolder text-dark fs-7 text-muted">
                          {DistanceMapData && DistanceMapData.phoneDistance ? DistanceMapData.phoneDistance : "--"}
                        </h5>
                      </div>
                      <div className="col-lg-1 ml-2">
                        <span className=" svg-icon-address svg me-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-house-fill"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"
                            />
                            <path
                              fillRule="evenodd"
                              d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"
                            />
                          </svg>
                        </span>
                      </div>
                      <div className="col-lg-10 mb-4">
                        <span className="ml-2 card-label fw-bolder text-dark ">
                          Measuring From
                        </span>
                        <h5 className="ml-2 card-label fw-bolder text-dark fs-7 text-muted">
                        {DistanceMapData && DistanceMapData.businessAddressDistance ? DistanceMapData.businessAddressDistance : "--"}
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <h4 className="ml-2 text-muted">Business</h4>
                    <div className="row mt-4">
                      <div className="col-lg-1 ml-2">
                        <span className="svg-icon svg-icon-success me-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-telephone-fill"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"
                            />
                          </svg>
                        </span>
                      </div>
                      <div className="col-lg-10 mb-4">
                        <span className="ml-2 card-label fw-bolder text-dark ">
                          Phone
                        </span>
                        <h5 className="ml-2 card-label fw-bolder text-dark fs-7 text-muted">
                        {DistanceMapData && DistanceMapData.phoneDistance ? DistanceMapData.phoneDistance : "--"}
                        </h5>
                      </div>
                      <div className="col-lg-1 ml-2">
                        <span className=" svg-icon-address svg me-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-house-fill"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"
                            />
                            <path
                              fillRule="evenodd"
                              d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"
                            />
                          </svg>
                        </span>
                      </div>
                      <div className="col-lg-10 mb-4">
                        <span className="ml-2 card-label fw-bolder text-dark ">
                          Address
                        </span>
                        <h5 className="ml-2 card-label fw-bolder text-dark fs-7 text-muted">
                          {BusinessAddressDistance && BusinessAddressDistance
                            ? BusinessAddressDistance
                            : "--"}
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    dashboardStore,
    riskManagementlistStore,
    editMerchantStore,
    MatrixStore,
  } = state;

  return {
    getRiskSummarys:
      state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    merchantIddetails:
      editMerchantStore && editMerchantStore.merchantIddetail
        ? editMerchantStore.merchantIddetail
        : {},
    matrixDetail:
      MatrixStore && MatrixStore.matrixDetail ? MatrixStore.matrixDetail : {},
  };
};
const mapDispatchToProps = (dispatch) => ({
  // getRiskSummaryDispatch: (id) => dispatch(riskSummaryActions.getRiskSummary(id)),
  // getMatrixDispatch: (id) => dispatch(matrixActions.getMatrixDetails(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(CrossCheck);
