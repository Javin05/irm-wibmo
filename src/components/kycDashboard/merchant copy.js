import React, { useState, useEffect, Fragment } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { useLocation, Link, useParams } from "react-router-dom";
import { DATE, STATUS_RESPONSE } from "../../utils/constants";
import {
  dashboardDetailsActions,
  riskSummaryActions,
  merchantDetailsActions,
  GetCategoryActions,
  KYCDashboardSummaryAction,
} from "../../store/actions";
import { KTSVG } from "../../theme/helpers";
import { riskManagementActions } from "../../store/actions";
import { ApproveActions } from "../../store/actions";
import CrossCheck from "./crossCheck copy";
import {
  USER_ERROR,
  REGEX,
  RESPONSE_STATUS,
  SESSION,
  SWEET_ALERT_MSG,
} from "../../utils/constants";
import clsx from "clsx";
import "react-circular-progressbar/dist/styles.css";
import ReactSpeedometer from "react-d3-speedometer";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";
import MapGoogle from "../maps/MapGoogle";
import StreetMap from "../maps/StreetMap";
import {
  KYCDashboardSummaryReducers,
  // KYCDashboardSummaryStoreKey,
} from "../../store/reducers/staticSummaryReducer";

function Merchant(props) {
  const {
    getPrevAlertDetailsDispatch,
    kyc_dashboard_details,
    id,
    setOpenPhone,
    setOpenEmail,
    setOpenAddress,
    setOpenIpAddress,
    riskmgmtlistdetails,
    getRiskManagementlistDispatch,
    ApprovePost,
    getRiskSummaryDispatch,
    approveResponceData,
    loading,
    openMap,
    openBusinessMap,
    setopenBusinessMap,
    setopenMap,
    getIdMerchantDispatch,
    clearApprove,
    setOpenWebsite,
    matrixDetail,
    merchantIddetails,
    merchantSummary,
    isLoaded,
    GetCategroyDispatch,
    getKYCDashboardSummaryDispatch,
    kyc_dashboard_summary_positive,
    isLoading_kyc_dashboard_summary,
    kyc_dashboard_summary_negavite,
    distanceData,
    kyc_dashboard_summary,
    UserDetails,
    DistanceRes
  } = props;

  const [active, setActive] = useState(false);
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("risk-summary/update/");
  const currentId = url && url[1]
  const [rejectValue, setRejectValue] = useState();



  // useEffect(() => {
  //   getIdMerchantDispatch(currentId);
  //   GetCategroyDispatch(currentId);
  // }, [currentId]);

  useEffect(() => {
    if (openMap) {
      const modalBtn = document.getElementById("modal-btn");
      modalBtn.click();
    }
  }, [openMap]);

  useEffect(() => {
    if (openBusinessMap) {
      const modalBtn = document.getElementById("businesmodal-btn");
      modalBtn.click();
    }
  }, [openBusinessMap]);

  // useEffect(() => {
  //   if (id) {
  //     getKYCDashboardSummaryDispatch(id);
  //   }
  // }, []);

  // useEffect(() => {
  //   if (id) {
  //     getPrevAlertDetailsDispatch(id);
  //     getRiskSummaryDispatch(id);
  //   }
  // }, [id]);

  // useEffect(() => {
  //   if (
  //     approveResponceData &&
  //     approveResponceData.status === STATUS_RESPONSE.SUCCESS_MSG
  //   ) {
  //     successAlert(
  //       approveResponceData && approveResponceData.message,
  //       "success"
  //     );
  //     getRiskManagementlistDispatch();
  //     getIdMerchantDispatch(id);
  //     clearApprove();
  //   }
  // }, [approveResponceData]);

  // useEffect(() => {
  //   if (
  //     approveResponceData &&
  //     approveResponceData.message === "Record Status Approved Successfully"
  //   ) {
  //     const approvemodalBtn = document.getElementById("approve-model");
  //     approvemodalBtn.click();
  //   }
  //   if (
  //     approveResponceData &&
  //     approveResponceData.message === "Record Status Rejected Successfully"
  //   ) {
  //     const rejectmodalBtn = document.getElementById("reject-model");
  //     rejectmodalBtn.click();
  //   }
  //   if (
  //     approveResponceData &&
  //     approveResponceData.message === "Record Status Changed as Hold"
  //   ) {
  //     const modalBtn = document.getElementById("hold-model");
  //     modalBtn.click();
  //   }
  // }, [approveResponceData]);

  const IDPhone =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.phone &&
    kyc_dashboard_details.data.dashboardData.phone.data;
  const IDAddress =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.address &&
    kyc_dashboard_details.data.dashboardData.address.data;
  const IDIndidualPan =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.individualPan
  const IDBusinessPan =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.businessPan
  const IDEmail =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.email &&
    kyc_dashboard_details.data.dashboardData.email.data;
  const IDIP_Address =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.ipAddress &&
    kyc_dashboard_details.data.dashboardData.ipAddress.data;
  const IDNegative =
    kyc_dashboard_details && kyc_dashboard_details.data && kyc_dashboard_details.data.negative;
  const IDPositive =
    kyc_dashboard_details && kyc_dashboard_details.data && kyc_dashboard_details.data.positive;
  const IDPhoneNumber =
    kyc_dashboard_details &&
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.phone &&
    kyc_dashboard_details.data.phone[0].value;
  const Risk =
    kyc_dashboard_details &&
    kyc_dashboard_details &&
    kyc_dashboard_details.risk_overview &&
    kyc_dashboard_details.risk_overview.risk_score;
  const Email =
    kyc_dashboard_details &&
    kyc_dashboard_details &&
    kyc_dashboard_details.risk_data &&
    kyc_dashboard_details.risk_data.email &&
    kyc_dashboard_details.risk_data.email[0].value;
  const Address =
    kyc_dashboard_details &&
    kyc_dashboard_details &&
    kyc_dashboard_details.risk_data &&
    kyc_dashboard_details.risk_data.address &&
    kyc_dashboard_details.risk_data.address[0].value;
  const IPaddress =
    kyc_dashboard_details &&
    kyc_dashboard_details &&
    kyc_dashboard_details.risk_data &&
    kyc_dashboard_details.risk_data.ip_address &&
    kyc_dashboard_details.risk_data.ip_address[0].value;
  const BusinessPhone =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.businessPhone &&
    kyc_dashboard_details.data.dashboardData.businessPhone.data;
  const BusinessEmail =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.businessEmail &&
    kyc_dashboard_details.data.dashboardData.businessEmail.data;
  const BusinessAddress =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.address &&
    kyc_dashboard_details.data.dashboardData.address.data;
  const Device =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.deviceId &&
    kyc_dashboard_details.data.dashboardData.deviceId.data;
  const speedometetervalue =
    kyc_dashboard_details &&
      kyc_dashboard_details.data &&
      kyc_dashboard_details.data.totalScore
      ? kyc_dashboard_details.data.totalScore
      : "0";
  const website =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.website &&
    kyc_dashboard_details.data.dashboardData.website.data;

  const IDPhoneValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.phone &&
    kyc_dashboard_details.data.dashboardData.phone.value;
  const IDEmailValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.email &&
    kyc_dashboard_details.data.dashboardData.email.value;
  const IDAddressValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.address &&
    kyc_dashboard_details.data.dashboardData.address.value;
  const IDIP_AddressValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.ipAddress &&
    kyc_dashboard_details.data.dashboardData.ipAddress.value;
  const BusinessPhoneValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.businessPhone &&
    kyc_dashboard_details.data.dashboardData.businessPhone.value;
  const BusinessEmailValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.businessEmail &&
    kyc_dashboard_details.data.dashboardData.businessEmail.value;
  const BusinessAddressValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.businessAddress &&
    kyc_dashboard_details.data.dashboardData.businessAddress.value;
  const DeviceValue =
    kyc_dashboard_details &&
    kyc_dashboard_details.data &&
    kyc_dashboard_details.data.dashboardData &&
    kyc_dashboard_details.data.dashboardData.deviceId &&
    kyc_dashboard_details.data.dashboardData.deviceId.value;
  const websiteValue =
    kyc_dashboard_details &&
      kyc_dashboard_details.data &&
      kyc_dashboard_details.data.dashboardData &&
      kyc_dashboard_details.data.dashboardData.website &&
      kyc_dashboard_details.data.dashboardData.website.value
      ? kyc_dashboard_details.data.dashboardData.website.value
      : "__";

  const viewData =
    matrixDetail && matrixDetail && matrixDetail.data ? matrixDetail.data : [];
  const getData = viewData.filter((o) => (o ? o : null));
  const splitData = getData && getData[0] ? getData[0] : "--";
  const value = 100;
  // const positive_data =
  //   typeof kyc_dashboard_summary_positive !== "undefined"
  //     ? kyc_dashboard_summary_positive.map((item) => {
  //         return item.message;
  //       })
  //     : "No data found";


  const List_positive_list = () => {
    return (
      <>
        {!isLoading_kyc_dashboard_summary ? (
          typeof kyc_dashboard_summary_positive !== "undefined" &&
            kyc_dashboard_summary_positive.length > 0 ? (
            kyc_dashboard_summary_positive.status !== "error" &&
            kyc_dashboard_summary_positive.map((item, i) => {
              // return <li key={i}>{item.message}</li>;
              return (
                <span key={"S_" + i} className="d-flex justify-content-start">
                  <i className="bi bi-check-circle-fill text-limegreen min-w-30px fsu " />
                  <h6 className="fw-bold fs-6 ">{item.message}</h6>
                </span>
              );
            })
          ) : (
            <h6 className="fw-bold fs-6 "> No data found</h6>
          )
        ) : (
          <div className="text-center">
            <div className="spinner-border text-primary m-5" />
          </div>
        )}
      </>
    )
  }

  const List_negavite_list = () => {
    return (
      <>
        {!isLoading_kyc_dashboard_summary ? (
          typeof kyc_dashboard_summary_negavite !== "undefined" &&
            kyc_dashboard_summary_negavite.length > 0 ? (
            kyc_dashboard_summary_positive.status !== "error" &&
            kyc_dashboard_summary_negavite.map((item, i) => {
              // return <li key={i}>{item.message}</li>;
              return (
                <span key={"S_" + i} className="d-flex justify-content-start">
                  <i className="bi bi-exclamation-triangle-fill text-darkorange min-w-30px fsu" />
                  <h6 className="fw-bold fs-6 ">{item.message}</h6>
                </span>
              );
            })
          ) : (
            <h6 className="fw-bold fs-6 "> No data found</h6>
          )
        ) : (
          <div className="text-center">
            <div className="spinner-border text-primary m-5" />
          </div>
        )}
      </>
    );
  };

  return (
    <>
      <button
        type="button"
        className="d-none"
        data-toggle="modal"
        id="hold-model"
        data-target="#holdModal"
        onClick={() => { }}
      />
      <button
        type="button"
        className="d-none"
        data-toggle="modal"
        id="reject-model"
        data-target="#rejectModal"
        onClick={() => { }}
      />
      <button
        type="button"
        className="d-none"
        data-toggle="modal"
        id="approve-model"
        data-target="#approveModal"
        onClick={() => { }}
      />
      <button
        type="button"
        className="d-none"
        data-toggle="modal"
        id="modal-btn"
        data-target="#InmapModal"
        onClick={() => { }}
      />
      <button
        type="button"
        className="d-none"
        data-toggle="modal"
        id="businesmodal-btn"
        data-target="#mapModal"
        onClick={() => { }}
      />
      <div
        className="modal fade"
        id="InmapModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className="modal-dialog modal-dialog-centered mw-1500px">
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="me-8">Individual Address</h2>
              <button
                type="button"
                className="btn btn-lg btn-icon btn-active-light-primary close"
                data-dismiss="modal"
                onClick={() => {
                  setopenMap(false);
                }}
              >
                {/* eslint-disable */}
                <KTSVG
                  path="/media/icons/duotune/arrows/arr061.svg"
                  className="svg-icon-1"
                />
                {/* eslint-disable */}
              </button>
            </div>
            <div className="modal-body">
              <div className="row mt-8">
                <div className="col-lg-6">
                  <div className="card w-504px h-450px">
                    {isLoaded ? (
                      <MapGoogle mapData={splitData} mapMarkers={null} />
                    ) : null}
                    {/*<AddressMapView splitData={splitData} />*/}
                  </div>
                </div>
                <div className="col-lg-6">
                  <div>
                    {isLoaded ? <StreetMap mapData={splitData} /> : null}
                    {/*<StreetView splitData={splitData} />*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="mapModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className="modal-dialog modal-dialog-centered mw-1500px">
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="me-8">Business Address</h2>
              <button
                type="button"
                className="btn btn-lg btn-icon btn-active-light-primary close"
                data-dismiss="modal"
                onClick={() => {
                  setopenBusinessMap(false);
                }}
              >
                {/* eslint-disable */}
                <KTSVG
                  path="/media/icons/duotune/arrows/arr061.svg"
                  className="svg-icon-1"
                />
                {/* eslint-disable */}
              </button>
            </div>
            <div className="modal-body">
              <div className="row mt-8">
                <div className="col-lg-6">
                  <div className="card w-504px h-450px">
                    {isLoaded ? (
                      <MapGoogle mapData={splitData} mapMarkers={null} />
                    ) : null}
                    {/*<BusinessAddressMapView splitData={splitData}/>*/}
                  </div>
                </div>
                <div className="col-lg-6">
                  <div
                    style={{
                      width: "714px",
                      height: "450px",
                      backgroundColor: "#eeeeee",
                    }}
                  >
                    {isLoaded ? <StreetMap mapData={splitData} /> : null}
                    {/*<StreetView splitData={splitData} />*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className=" col-md-12 card mb-5 mb-xl-10">
        <div className="card-body pt-9 pb-0">
          <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
            <div className="flex-grow-1">
              <div className="row mt-6">
                <div className="col-lg-3 col-md-3 col-sm-3 ps-8">
                  <h2 className="mb-2 d-flex justify-content-start text-limegreen mb-5">
                    Positive Factors
                  </h2>
                  {/* {IDPositive ? (
                    <div>
                      <div className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </div>
                    </div>
                  ) : (
                    IDPositive &&
                    IDPositive.map((item, i) => {
                      return (
                        <span
                          key={"S_" + i}
                          className="d-flex justify-content-start"
                        >
                          <i className="bi bi-check-circle-fill text-limegreen min-w-30px fsu " />
                          <h6 className="fw-bold fs-6 ">{item.message}</h6>
                        </span>
                      );
                    })
                  )} */}
                  <List_positive_list />
                </div>
                <div className="col-lg-5 col-md-5 col-sm-5">
                  <div className="d-flex justify-content-center mb-4">
                    <ReactSpeedometer
                      maxValue={100}
                      value={parseInt(speedometetervalue)}
                      customSegmentStops={[0, 35, 75, 100]}
                      segmentColors={["limegreen", "gold", "tomato"]}
                      needleColor="red"
                      startColor="green"
                      segments={10}
                      endColor="blue"
                      className="pichart"
                      currentValueText="Over All Score: #{value}"
                      currentValuePlaceholderStyle={"#{value}"}
                    />
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 col-sm-4">
                  <h2 className="mb-2 d-flex justify-content-start symbol-label text-darkorange mb-5">
                    Negative Factors
                  </h2>
                  {/* {IDNegative ? (
                    <div>
                      <div className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </div>
                    </div>
                  ) : (
                    IDNegative &&
                    IDNegative.map((item, i) => {
                      return (
                        <span
                          key={"E" + i}
                          className="d-flex justify-content-start"
                        >
                          <i className="bi bi-exclamation-triangle-fill text-darkorange min-w-30px fsu" />
                          <h5 className="text-darkorange fw-bold fs-6 ">
                            {item.message}
                          </h5>
                        </span>
                      );
                    })
                  )} */}
                  <List_negavite_list />
                </div>
                <div className="row mb-0">
                  <div className="col-lg-4 col-md-4 col-sm-4" />
                  <div className="col-lg-4 col-md-4 col-sm-4">
                    {/* <h1 className='d-flex justify-content-center  fw-boldest my-1 fs-2'>Risk Score </h1> */}
                    <h1 className="d-flex justify-content-center  fw-boldest my-1 fs-2">
                      {Risk}{" "}
                    </h1>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-4" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 col-md-6 col-sm-6">
          <div className="mb-4">
            <span className="text-muted mt-1 fw-bold fs-4 justify-content-start">
              INDIVIDUAL
            </span>
            <h1 className="text-dark fw-bolder justify-content-start">
              {merchantSummary ? merchantSummary.contactName : "--"}
            </h1>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="card card-xl-stretch mb-xl-8 ">
                <div className="card-header border-0 mb-2  bg-card">
                  <h3 className="card-title align-items-start flex-column">
                    <span className="card-label fw-bolder text-dark bg-card">
                      Phone
                    </span>
                    <span className="text-muted mt-1 fw-bold fs-7">
                      {merchantSummary && merchantSummary?.contactNumber
                        ? merchantSummary.contactNumber
                        : "--"}
                    </span>
                    <a
                      className="mt-1 fw-bold fs-7 cp card-link"
                      onClick={() => {
                        setOpenPhone(true);
                      }}
                    >
                      View Phone Details
                    </a>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  {!IDPhone ? (
                    <div>
                      <div className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </div>
                    </div>
                  ) : (
                    IDPhone &&
                    IDPhone.map((item, i) => {
                      return (
                        <Fragment key={"FIX_1" + i}>
                          {item && item.status === "positive" ? (
                            <div className="d-flex align-items-center  rounded p-5 mb-0">
                              <div className="flex-grow-1 me-2">
                                <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                  {item.title}
                                </div>
                                <span className="text-muted fw-bold d-block">
                                  {item.value}
                                </span>
                              </div>
                              <span
                                className="svg-icon svg-icon-1 svg-icon-success"
                                title={item.message}
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24"
                                  height="24"
                                  viewBox="0 0 24 24"
                                  fill="none"
                                >
                                  <rect
                                    opacity="0.3"
                                    x="2"
                                    y="2"
                                    width="20"
                                    height="20"
                                    rx="10"
                                    fill="black"
                                  />
                                  <path
                                    d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
                                    fill="black"
                                  />
                                </svg>
                              </span>
                            </div>
                          ) : (
                            <>
                              {item && item.status === "warning" ? (
                                <div className="d-flex align-items-center  rounded p-5 mb-0">
                                  <div className="flex-grow-1 me-2">
                                    <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                      {item.title}
                                    </div>
                                    <span className="text-muted fw-bold d-block">
                                      {item.value}
                                    </span>
                                  </div>
                                  <span
                                    className="fw-bolder text-warning py-1"
                                    title={item.message}
                                  >
                                    <i
                                      className="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu"
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              ) : (
                                <>
                                  {item && item.status === "negative" ? (
                                    <div className="d-flex align-items-center  rounded p-5 mb-0">
                                      <div className="flex-grow-1 me-2">
                                        <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                          {item.title}
                                        </div>
                                        <span className="text-muted fw-bold d-block">
                                          {item.value}
                                        </span>
                                      </div>
                                      <span className="fw-bolder text-danger py-1">
                                        <i
                                          className="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu"
                                          title={item.message}
                                        />
                                      </span>
                                    </div>
                                  ) : null}
                                </>
                              )}
                            </>
                          )}
                        </Fragment>
                      );
                    })
                  )}
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="card card-xl-stretch mb-xl-8 ">
                <div className="card-header border-0  bg-card">
                  <h3 className="card-title align-items-start flex-column">
                    <span className="card-label fw-bolder text-dark bg-card">
                      {/* <span className=' svg-icon-email svg me-2'>
                        <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-envelope-fill' viewBox='0 0 16 16'>
                          <path d='M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z' />
                        </svg>
                      </span> */}
                      Email
                    </span>
                    <span
                      className="text-muted fw-bold fs-7 ellipsis"
                      style={{ maxWidth: "100px" }}
                    >
                      {merchantSummary && merchantSummary?.contactEmail
                        ? merchantSummary.contactEmail
                        : "--"}
                    </span>
                    <a
                      className="mt-1 fw-bold fs-7 cp card-link"
                      onClick={() => {
                        setOpenEmail(true);
                      }}
                    >
                      View Email Details
                    </a>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  {
                    !IDEmail ? (
                      <div>
                        <div className="text-center">
                          <div
                            className="spinner-border text-primary m-5"
                            role="status"
                          />
                        </div>
                      </div>
                    ) : (
                      IDEmail &&
                      IDEmail.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            {item && item.status === "positive" ? (
                              <div className="d-flex align-items-center  rounded p-5 mb-0">
                                <div className="flex-grow-1 me-2">
                                  <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                    {item.title}
                                  </div>
                                  <span className="text-muted fw-bold d-block">
                                    {item.value}
                                  </span>
                                </div>
                                <span
                                  className="svg-icon svg-icon-1 svg-icon-success"
                                  title={item.message}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="black"
                                    />
                                    <path
                                      d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
                                      fill="black"
                                    />
                                  </svg>
                                </span>
                              </div>
                            ) : (
                              <>
                                {item && item.status === "warning" ? (
                                  <div className="d-flex align-items-center  rounded p-5 mb-0">
                                    <div className="flex-grow-1 me-2">
                                      <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                        {item.title}
                                      </div>
                                      <span className="text-muted fw-bold d-block">
                                        {item.value}
                                      </span>
                                    </div>
                                    <span className="fw-bolder text-warning py-1">
                                      <i className="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" />
                                    </span>
                                  </div>
                                ) : (
                                  <>
                                    {item && item.status === "negative" ? (
                                      <div className="d-flex align-items-center  rounded p-5 mb-0">
                                        <div className="flex-grow-1 me-2">
                                          <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                            {item.title}
                                          </div>
                                          <span className="text-muted fw-bold d-block">
                                            {item.value}
                                          </span>
                                        </div>
                                        <span
                                          className="fw-bolder text-danger py-1"
                                          title={item.message}
                                        >
                                          <i className="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu" />
                                        </span>
                                      </div>
                                    ) : null}
                                  </>
                                )}
                              </>
                            )}
                          </Fragment>
                        );
                      })
                    )}
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="card card-xl-stretch mb-xl-8 ">
                <div className="card-header border-0 bg-card">
                  <h3 className="card-title align-items-start flex-column ">
                    <span className="card-label fw-bolder text-dark  bg-card">
                      Website
                    </span>
                    <span
                      className="text-muted mt-1 fw-bold fs-7 ellipsis"
                      style={{ maxWidth: "100px" }}
                    >
                      {merchantSummary && merchantSummary?.website
                        ? merchantSummary?.website
                        : "--"}
                    </span>
                    <a
                      className="mt-1 fw-bold fs-7 cp card-link"
                      onClick={() => {
                        setOpenWebsite(true);
                      }}
                    >
                      View Website Details
                    </a>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  {
                    !website ? (
                      <div>
                        <div className="text-center">
                          <div
                            className="spinner-border text-primary m-5"
                            role="status"
                          />
                        </div>
                      </div>
                    ) : (
                      website &&
                      website.map((item, i) => {
                        return (
                          <Fragment key={"FIX_4" + i}>
                            {item && item.status === "positive" ? (
                              <div className="d-flex align-items-center  rounded p-5 mb-0">
                                <div className="flex-grow-1 me-2">
                                  <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                    {item.title}
                                  </div>
                                  <span className="text-muted fw-bold d-block">
                                    {item.value}
                                  </span>
                                </div>
                                <span
                                  className="svg-icon svg-icon-1 svg-icon-success"
                                  title={item.message}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="black"
                                    />
                                    <path
                                      d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
                                      fill="black"
                                    />
                                  </svg>
                                </span>
                              </div>
                            ) : (
                              <>
                                {item && item.status === "warning" ? (
                                  <div className="d-flex align-items-center  rounded p-5 mb-0">
                                    <div className="flex-grow-1 me-2">
                                      <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                        {item.title}
                                      </div>
                                      <span className="text-muted fw-bold d-block">
                                        {item.value}
                                      </span>
                                    </div>
                                    <span
                                      className="fw-bolder text-warning py-1"
                                      title={item.message}
                                    >
                                      <i className="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" />
                                    </span>
                                  </div>
                                ) : (
                                  <>
                                    {item && item.status === "negative" ? (
                                      <div className="d-flex align-items-center  rounded p-5 mb-0">
                                        <div className="flex-grow-1 me-2">
                                          <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                            {item.title}
                                          </div>
                                          <span className="text-muted fw-bold d-block">
                                            {item.value}
                                          </span>
                                        </div>
                                        <span
                                          className="fw-bolder text-danger py-1"
                                          title={item.message}
                                        >
                                          <i className="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu" />
                                        </span>
                                      </div>
                                    ) : null}
                                  </>
                                )}
                              </>
                            )}
                          </Fragment>
                        );
                      })
                    )}
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="card card-xl-stretch mb-xl-8 ">
                <div className="card-header border-0 mb-xl-5  bg-card">
                  <h3 className="card-title align-items-start flex-column">
                    <span className="card-label fw-bolder text-dark  bg-card">
                      Individual Pan
                    </span>
                    <a
                      className="mt-1 fw-bold fs-7 cp text link-muted"
                      data-toggle="modal"
                      data-target="#InmapModal"
                    // onClick={() => {setActive(true)}}
                    >
                      {IDIndidualPan && IDIndidualPan.value ? IDIndidualPan.value : '--'}
                    </a>
                    <a
                      className="mt-1 fw-bold fs-7 cp card-link"
                      onClick={() => {
                        setOpenAddress(true);
                      }}
                    >
                      View Pan Details
                    </a>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  {
                    !IDIndidualPan ? (
                      <div>
                        <div className="text-center">
                          <div
                            className="spinner-border text-primary m-5"
                            role="status"
                          />
                        </div>
                      </div>
                    ) : (
                      IDIndidualPan &&
                      IDIndidualPan.data.map((item, i) => {
                        return (
                          <Fragment key={"FIX_5" + i}>
                            {item && item.status === "positive" ? (
                              <div className="d-flex align-items-center  rounded p-5 mb-0">
                                <div className="flex-grow-1 me-2">
                                  <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                    {item.title}
                                  </div>
                                  <span className="text-muted fw-bold d-block ellipsis">
                                    {item.value}
                                  </span>
                                </div>
                                <span
                                  className="svg-icon svg-icon-1 svg-icon-success"
                                  title={item.message}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="black"
                                    />
                                    <path
                                      d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
                                      fill="black"
                                    />
                                  </svg>
                                </span>
                              </div>
                            ) : (
                              <>
                                {item && item.status === "warning" ? (
                                  <div className="d-flex align-items-center  rounded p-5 mb-0">
                                    <div className="flex-grow-1 me-2">
                                      <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                        {item.title}
                                      </div>
                                      <span className="text-muted fw-bold d-block ellipsis">
                                        {item.value}
                                      </span>
                                    </div>
                                    <span
                                      className="fw-bolder text-warning py-1"
                                      title={item.message}
                                    >
                                      <i className="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" />
                                    </span>
                                  </div>
                                ) : (
                                  <>
                                    {item && item.status === "negative" ? (
                                      <div className="d-flex align-items-center  rounded p-5 mb-0">
                                        <div className="flex-grow-1 me-2">
                                          <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                            {item.title}
                                          </div>
                                          <span className="text-muted fw-bold d-block ellipsis">
                                            {item.value}
                                          </span>
                                        </div>
                                        <span
                                          className="fw-bolder text-danger py-1"
                                          title={item.message}
                                        >
                                          <i className="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu" />
                                        </span>
                                      </div>
                                    ) : null}
                                  </>
                                )}
                              </>
                            )}
                          </Fragment>
                        );
                      })
                    )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-6 col-sm-6">
          <div className="mb-4">
            <span className="text-muted mt-1 fw-bolder fs-4  justify-content-start">
              BUSINESS
            </span>
            <h1 className="text-dark fw-bolder justify-content-start ">
              {UserDetails && UserDetails.businessType ? UserDetails.businessType : "--"}
            </h1>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-header border-0 bg-business-card">
                  <h3 className="card-title align-items-start flex-column">
                    <span className="card-label fw-bolder text-dark bg-business-card">
                      Business Pan
                    </span>
                    <span className="text-muted mt-1 fw-bold fs-7 ellipsis">
                      {IDBusinessPan ? IDBusinessPan.value : "--"}
                    </span>
                    <a
                      className="mt-1 fw-bold fs-7 cp card-link"
                      onClick={() => {
                        setOpenEmail(true);
                      }}
                    >
                      View Pan Details
                    </a>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  {
                    !IDBusinessPan ? (
                      <div>
                        <div className="text-center">
                          <div
                            className="spinner-border text-primary m-5"
                            role="status"
                          />
                        </div>
                      </div>
                    ) : (
                      IDBusinessPan &&
                      IDBusinessPan.data.map((item, i) => {
                        return (
                          <Fragment key={"FIX_8" + i}>
                            {item && item.status === "positive" ? (
                              <div className="d-flex align-items-center  rounded p-5 mb-0">
                                <div className="flex-grow-1 me-2">
                                  <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                    {item.title}
                                  </div>
                                  <span className="text-muted fw-bold d-block">
                                    {item.value}
                                  </span>
                                </div>
                                <span
                                  className="svg-icon svg-icon-1 svg-icon-success"
                                  title={item.message}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="black"
                                    />
                                    <path
                                      d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
                                      fill="black"
                                    />
                                  </svg>
                                </span>
                              </div>
                            ) : (
                              <>
                                {item && item.status === "warning" ? (
                                  <div className="d-flex align-items-center  rounded p-5 mb-0">
                                    <div className="flex-grow-1 me-2">
                                      <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                        {item.title}
                                      </div>
                                      <span className="text-muted fw-bold d-block">
                                        {item.value}
                                      </span>
                                    </div>
                                    <span
                                      className="fw-bolder text-warning py-1"
                                      title={item.message}
                                    >
                                      <i className="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" />
                                    </span>
                                  </div>
                                ) : (
                                  <>
                                    {item && item.status === "negative" ? (
                                      <div className="d-flex align-items-center  rounded p-5 mb-0">
                                        <div className="flex-grow-1 me-2">
                                          <div className="fw-bolder text-gray-800 text-hover-primary fs-6">
                                            {item.title}
                                          </div>
                                          <span className="text-muted fw-bold d-block">
                                            {item.value}
                                          </span>
                                        </div>
                                        <span
                                          className="fw-bolder text-danger py-1"
                                          title={item.message}
                                        >
                                          <i className="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu" />
                                        </span>
                                      </div>
                                    ) : null}
                                  </>
                                )}
                              </>
                            )}
                          </Fragment>
                        );
                      })
                    )}
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-header border-0 mb-xl-5 bg-business-card">
                  <h3 className="card-title align-items-start flex-column">
                    <span className="card-label fw-bolder text-dark bg-business-card">
                      Address
                    </span>
                    <a
                      className="mt-1 fw-bold fs-7 cp text link-muted"
                      data-toggle="modal"
                      data-target="#mapModal"
                    >
                      {merchantSummary ? merchantSummary.businessAddress : "--"}
                    </a>
                    <a
                      className="mt-1 fw-bold fs-7 cp card-link"
                      onClick={() => {
                        setOpenAddress(true);
                      }}
                    >
                      View Address Details
                    </a>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  {
                    isLoading_kyc_dashboard_summary ? (
                      <div>
                        <div className="text-center">
                          <div
                            className="spinner-border text-primary m-5"
                            role="status"
                          />
                        </div>
                      </div>
                    ) : (
                        !_.isEmpty(BusinessAddress) ? (
                        BusinessAddress.map((item, i) => {
                          return (
                            <Fragment key={"FIX_9" + i}>
                              {item && item.status === "positive" ? (
                                <div className="d-flex align-items-center  rounded p-5 mb-0">
                                  <div className="flex-grow-1 me-2">
                                    <div className="fw-bolder text-gray-800 text-hover-primary fs-6 ellipsis">
                                      {item.title}
                                    </div>
                                    <span className="text-muted fw-bold d-block ellipsis">
                                      {item.value}
                                    </span>
                                  </div>
                                  <span
                                    className="svg-icon svg-icon-1 svg-icon-success ellipsis"
                                    title={item.message}
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                    >
                                      <rect
                                        opacity="0.3"
                                        x="2"
                                        y="2"
                                        width="20"
                                        height="20"
                                        rx="10"
                                        fill="black"
                                      />
                                      <path
                                        d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
                                        fill="black"
                                      />
                                    </svg>
                                  </span>
                                </div>
                              ) : (
                                <>
                                  {item && item.status === "warning" ? (
                                    <div className="d-flex align-items-center  rounded p-5 mb-0">
                                      <div className="flex-grow-1 me-2">
                                        <div className="fw-bolder text-gray-800 text-hover-primary fs-6 ellipsis">
                                          {item.title}
                                        </div>
                                        <span className="text-muted fw-bold d-block ellipsis ellipsis">
                                          {item.value}
                                        </span>
                                      </div>
                                      <span
                                        className="fw-bolder text-warning py-1 ellipsis"
                                        title={item.message}
                                      >
                                        <i className="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" />
                                      </span>
                                    </div>
                                  ) : (
                                    <>
                                      {item && item.status === "negative" ? (
                                        <div className="d-flex align-items-center  rounded p-5 mb-0">
                                          <div className="flex-grow-1 me-2">
                                            <div className="fw-bolder text-gray-800 text-hover-primary fs-6 ellipsis">
                                              {item.title}
                                            </div>
                                            <span className="text-muted fw-bold d-block ellipsis ellipsis">
                                              {item.value}
                                            </span>
                                          </div>
                                          <span
                                            className="fw-bolder text-danger py-1 ellipsis"
                                            title={item.message}
                                          >
                                            <i className="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu" />
                                          </span>
                                        </div>
                                      ) : null}
                                    </>
                                  )}
                                </>
                              )}
                            </Fragment>
                          )
                        })
                      ) : (
                        <span className='mt-4 mb-4 ms-4 text-muted'> No Records Found</span>
                      )
                    )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <CrossCheck
        isLoaded={isLoaded}
        id={id}
        distanceData={distanceData}
        merchantSummary={merchantSummary}
        DistanceRes={DistanceRes}
      />
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    dashboardStore,
    riskManagementlistStore,
    ApproveStore,
    editMerchantStores,
    MatrixStore,
    GetCategoryStore,
    KYCDashboardSummaryStoreKey,
  } = state;
  return {};
};

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Merchant);
