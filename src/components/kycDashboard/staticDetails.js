import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import "react-circular-progressbar/dist/styles.css";
import moment from "moment";
import { useLocation } from "react-router-dom";
import {
  merchantIdDetailsActions,
  GetAsigneeActions,
  AsiggnActions,
} from "../../store/actions";
import ReactSelect from "../../theme/layout/components/ReactSelect";
import color from "../../utils/colors";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from "../../utils/constants";
import _ from "lodash";
import { getLocalStorage } from "../../utils/helper";
import PriceCheckPopup from "../shared-components/PriceCheckPopup";
import Category from "./Category copy";
import FindRole from "../riskManagement/Role";

function MerchantDetails(props) {
  const {
    className,
    getRiskSummaryDispatch,
    loading,
    getRiskSummarys,
    phone,
    dashboardDetails,
    riskmgmtlistdetails,
    merchantIddetails,
    setOpenPhone,
    setOpenEmail,
    setOpenAddress,
    setOpenIpAddress,
    setOpenBusinessAddress,
    setopenMap,
    setopenBusinessMap,
    merchantIdDetail,
    merchantSummary,
    getAssignee,
    getAsigneeslistDispatch,
    updateAssignDispatch,
    updateAssign,
    clearAsiggnDispatch,
  } = props;
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("/");
  const currentId = url && url[3];
  const [AsigneesOption, setAsignees] = useState();
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState("");
  const [formData, setFormData] = useState({
    assignedTo: "",
  });
  const [value, setValue] = useState();
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));

  useEffect(() => {
    // if (currentId) {
    //   merchantIdDetail(currentId)
    // }
  }, [currentId]);

  useEffect(() => {
    const params = {
      tag: "IRM",
    };
    getAsigneeslistDispatch(params);
  }, []);

  const IDIP_Address =
    dashboardDetails &&
    dashboardDetails.data &&
    dashboardDetails.data.dashboardData &&
    dashboardDetails.data.dashboardData.ipAddress &&
    dashboardDetails.data.dashboardData.ipAddress.data[0]
      ? dashboardDetails.data.dashboardData.ipAddress.data[0]
      : "--";
  const viewData =
    getRiskSummarys && getRiskSummarys && getRiskSummarys.data
      ? getRiskSummarys.data
      : [];
  const getData = viewData.filter((o) => (o ? o : null));
  const viewIpAddress =
    getData && getData[0] && getData[0].ipCheck ? getData[0].ipCheck : "--";
  const merchantIdDetails =
    merchantIddetails && merchantIddetails.data ? merchantIddetails.data : "--";
  const totalScoreValue =
    dashboardDetails &&
    dashboardDetails.data &&
    dashboardDetails.data.totalScore
      ? dashboardDetails.data.totalScore
      : "0";

  const StatusArray = {
    PENDING: "badge-light-warning",
    APPROVED: "badge-light-success",
    REJECTED: "badge-light-danger",
    HOLD: "badge-light-warning",
    "MANUAL REVIEW": "badge badge-orange text-black",
  };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  };

  const handleChangeAsignees = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption);
      setFormData((values) => ({
        ...values,
        assignedTo: selectedOption.value,
      }));
      setValue(selectedOption.label);
    }
  };

  const AsigneesNames =
    getAssignee && getAssignee.data && getAssignee.data.result;
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames);
    setAsignees(Asignees);
  }, [AsigneesNames]);

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = [];
    for (const item in AsigneesNames) {
      defaultOptions.push({
        label: AsigneesNames[item].firstName,
        value: AsigneesNames[item]._id,
      });
    }
    return defaultOptions;
  };

  const onConfirmupdate = () => {
    updateAssignDispatch(currentId, formData);
  };

  const Assign = () => {
    confirmationAlert(
      "Do You Want To",
      `Assign this Case #${
        merchantSummary && merchantSummary?.riskId
      } to ${value}?`,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmupdate();
      },
      () => {}
    );
  };

  const onConfirm = () => {
    setFormData({
      assignedTo: "",
    });
  };

  const clear = () => {
    setFormData({
      assignedTo: "",
    });
  };

  useEffect(() => {
    if (updateAssign && updateAssign.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert("Assigned Successfully", "success");
      clearAsiggnDispatch();
      setFormData({
        assignedTo: "",
      });
    } else if (
      updateAssign &&
      updateAssign.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        updateAssign.message,
        "",
        "Try again",
        "",
        () => {}
      );
      clearAsiggnDispatch();
    }
  }, [updateAssign]);

  return (
    <>
      <div className="col-md-12 card card-xl-stretch mb-xl-8">
        <div className="card-header border-0">
          <h3 className="card-title align-items-start flex-column ">
            <span className="d-flex align-items-center fw-boldest my-1 fs-2">
              Case Details - Case ID #
              {merchantSummary && merchantSummary.kycRiskId
                ? `KYC${merchantSummary.kycRiskId}`
                : "--"}
              <span
                className={
                  `badge ml-2 ` + merchantSummary
                    ? StatusArray[merchantSummary?.riskStatus]
                    : ""
                }
                style={{
                  padding: "4px 12px",
                  marginLeft: "10px",
                  fontSize: "14px",
                }}
              >
                {merchantSummary ? merchantSummary.riskStatus : ""}
              </span>
            </span>
          </h3>
          {Role === "Admin" ? (
            <div className="flex-column mt-4 w-25">
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name="AppUserId"
                className="select2"
                classNamePrefix="select"
                handleChangeReactSelect={handleChangeAsignees}
                options={AsigneesOption}
                value={SelectedAsigneesOption}
                isDisabled={!AsigneesOption}
              />
            </div>
          ) : null}
          {!_.isEmpty(formData.assignedTo) ? (
            <span className="flex-column mt-4">
              <button class="btn btn-light-primary btn-sm" onClick={Assign}>
                Assign
              </button>
            </span>
          ) : null}
        </div>
        <div className="separator separator-dashed my-3" />
        <div className="card-body pt-0">
          <div className="row g-5 g-xl-8">
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-6 col-md-6 col-lg-6">
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Case Id
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Queue
                      </div>
                      {/* <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Assigned To
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Status
                      </div> */}
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Date Received
                      </div>
                      {/* <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Age
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Case Type
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Fraud Type
                      </div>
                      <div className='text-gray-700 fw-bold  pl-3 ml-2 '>
                        Neg Table Info:
                      </div> */}
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ellipsis">
                        Application Risk Score
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6">
                      <div className="text-700 fw-bold  pl-3 ">
                        KYC
                        {merchantSummary && merchantSummary.kycRiskId
                          ? merchantSummary.kycRiskId
                          : "--"}
                      </div>
                      <div className="text-700 fw-bold  pl-3 ">
                        SUSPECT_ACCOUNTS_Q
                      </div>
                      {/* <div className='text-700 fw-bold  pl-3 '>
                        Unassigned
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        New
                      </div> */}
                      <div className="text-700 fw-bold  pl-3 ">
                        {moment(
                          merchantSummary?.updatedAt
                            ? merchantSummary?.updatedAt
                            : "--"
                        ).format("DD/MM/YYYY")}
                      </div>
                      {/* <div className='text-700 fw-bold  pl-3 '>
                        1
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        System/Manual
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        NA
                      </div>
                      <div className='text-700 fw-bold  pl-3 '>
                        No
                      </div> */}
                      <div className="text-700 fw-bold  pl-3 ">
                        {totalScoreValue ? totalScoreValue : "0"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-8 col-md-8 col-lg-8">
                      {/* <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Business Name:
                      </div> */}
                      {/* <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Business URL:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Business Email:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Business Phone
                      </div> */}
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Business Address:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 text-nowrap">
                        Business Type
                      </div>
                    </div>
                    <div className="col-sm-4 col-md-4 col-lg-4">
                      {/* <div className="text-700 fw-bold  pl-3 ellipsis">
                        {merchantSummary && merchantSummary.companyName
                          ? merchantSummary.companyName
                          : "--"}
                      </div> */}

                      {/* <div
                        className="text-700 fw-bold Id-response-Link  pl-3 ellipsis"
                        onClick={() => setOpenEmail(true)}
                      >
                        {merchantSummary && merchantSummary.businessEmail
                          ? merchantSummary.businessEmail
                          : "--"}
                      </div> */}
                      {/* <div
                        className="text-700 fw-bold Id-response-Link  pl-3 ellipsis"
                        onClick={() => setOpenPhone(true)}
                      >
                        {merchantSummary && merchantSummary.businessPhone
                          ? merchantSummary.businessPhone
                          : "--"}
                      </div> */}
                      <div
                        className="text-700 fw-bold Id-response-Link  pl-3 ellipsis"
                        onClick={() => setopenBusinessMap(true)}
                      >
                        {merchantSummary && merchantSummary.businessAddress
                          ? merchantSummary.businessAddress
                          : "--"}
                      </div>
                      <div className="text-700 fw-bold  pl-3 ellipsis ">
                        {merchantSummary && merchantSummary.businessType
                          ? merchantSummary.businessType
                          : "--"}
                      </div>
                      {/* <div className="text-700 fw-bold  pl-3 ">
                        {merchantSummary && merchantSummary.accountType
                          ? merchantSummary.accountType
                          : "--"}
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-6 col-md-6 col-lg-6">
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Name:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Email:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Phone:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 text-nowrap">
                        Indiduval Pan :
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2">
                        Individual Pan Name :
                      </div>
                      {/* <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        IP of user
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Device ID
                      </div> */}
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6">
                      <div className="text-700 fw-bold  pl-3 ellipsis">
                        {merchantSummary && merchantSummary.contactName
                          ? merchantSummary.contactName
                          : "--"}
                      </div>
                      <div
                        className="text-700 fw-bold  pl-3 Id-response-Link ellipsis"
                        onClick={() => setOpenEmail(true)}
                      >
                        {merchantSummary && merchantSummary.contactEmail
                          ? merchantSummary.contactEmail
                          : "--"}
                      </div>
                      <div
                        className="text-700 fw-bold  pl-3 Id-response-Link ellipsis"
                        onClick={() => setOpenPhone(true)}
                      >
                        {merchantSummary && merchantSummary.contactNumber
                          ? merchantSummary.contactNumber
                          : "--"}
                      </div>
                      {/* <div
                        className="text-700 fw-bold  pl-3 Id-response-Link ellipsis"
                        onClick={() => setopenMap(true)}
                      >
                        {merchantSummary && merchantSummary.address
                          ? merchantSummary.address
                          : "--"}
                      </div> */}
                      {/* <div
                        className="text-700 fw-bold  pl-3  Id-response-Link ellipsis"
                        onClick={() => setOpenIpAddress(true)}
                      >
                        {merchantSummary && merchantSummary.ipAddress
                          ? merchantSummary.ipAddress
                          : "--"}
                      </div> */}
                      <div className="text-700 fw-bold  pl-3 ellipsis ">
                        {/* <br />{" "} */}
                        {merchantSummary && merchantSummary.individualPan
                          ? merchantSummary.individualPan
                          : "--"}
                      </div>
                      <div className="text-700 fw-bold  pl-3 ellipsis ">
                        <br />{" "}
                        {merchantSummary && merchantSummary.individualPanName
                          ? merchantSummary.individualPanName
                          : "--"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-sm-8 col-md-8 col-lg-8">
                      {/* <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Average Monthly Volume:
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        Average Transaction Amount:
                      </div> */}
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        GSTIN
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        CIN
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 text-nowrap">
                        Business PAN Name
                      </div>
                      <div className="text-gray-700 fw-bold  pl-3 ml-2 ">
                        business PAN
                      </div>
                    </div>
                    <div className="col-sm-4 col-md-4 col-lg-4">
                      <div className="text-700 fw-bold  pl-3 ellipsis">
                        {merchantSummary && merchantSummary.gstNumber
                          ? merchantSummary.gstNumber
                          : "--"}
                      </div>
                      <div className="text-700 fw-bold  pl-3 ellipsis">
                        {merchantSummary && merchantSummary.cin
                          ? merchantSummary.cin
                          : "--"}
                      </div>

                      <div className="text-700 fw-bold  pl-3 ellipsis">
                        {merchantSummary && merchantSummary.individualPanName
                          ? merchantSummary.individualPanName
                          : "--"}
                      </div>
                      <div className="text-700 fw-bold  pl-3 ellipsis">
                        {merchantSummary && merchantSummary.businessPan
                          ? merchantSummary.businessPan
                          : "--"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <div className="row">
            <FindRole role={Role}>
              <div className="col-sm-12 col-md-12 col-lg-12">
                <PriceCheckPopup /> <Category categoryList={getData} />
              </div>
            </FindRole>
          </div> */}
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    dashboardStore,
    riskManagementlistStore,
    editMerchantStore,
    postAsigneeStore,
    updateAssignStore,
  } = state;

  return {
    dashboardDetails:
      dashboardStore && dashboardStore.dashboardDetails
        ? dashboardStore.dashboardDetails
        : {},
    riskmgmtlistdetails:
      riskManagementlistStore && riskManagementlistStore.riskmgmtlists
        ? riskManagementlistStore.riskmgmtlists
        : null,
    getRiskSummarys:
      state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    merchantIddetails:
      editMerchantStore && editMerchantStore.merchantIddetail
        ? editMerchantStore.merchantIddetail
        : {},
    getAssignee:
      postAsigneeStore && postAsigneeStore.getAssignee
        ? postAsigneeStore.getAssignee
        : {},
    updateAssign:
      updateAssignStore && updateAssignStore.updateAssign
        ? updateAssignStore.updateAssign
        : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  merchantIdDetail: (id) =>
    dispatch(merchantIdDetailsActions.getmerchantIdDetailsData(id)),
  getAsigneeslistDispatch: (params) =>
    dispatch(GetAsigneeActions.GetAsignee(params)),
  updateAssignDispatch: (id, params) =>
    dispatch(AsiggnActions.Asiggn(id, params)),
  clearAsiggnDispatch: () => dispatch(AsiggnActions.clearAsiggn()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetails);
