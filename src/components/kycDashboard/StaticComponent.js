
import React, { useEffect, useState, createContext } from "react"
import { connect } from "react-redux"
import moment from "moment"
import "react-circular-progressbar/dist/styles.css"
import { Link, useLocation } from "react-router-dom"
import { useLoadScript } from "@react-google-maps/api"
import { getLocalStorage } from "../../utils/helper"
import Merchant from "./merchant copy"
import Phone from "./phone"
import Email from "./email"
import Address from "./address"
import MerchantDetails from "./staticDetails"
import KycWebsite from "./kycWebsite"
import Tabs from "react-bootstrap/Tabs"
import Tab from "react-bootstrap/Tab"
import UserProfileKYC from "../KYC/UserProfileKYC"
import {
  KYCDashboardSummaryAction,
  KYCUserAction,
  DistanceAction,
  AllDasboardDataAction,
  DasboardAadharAction,
  DasboardPanAction,
  DasboardCinAction,
  KycScoreAction,
  linkAnalyticsAction,
  VideoKYCAction,
  UENdashboardAction,
  FullKycValueAction
} from "../../store/actions"
import NetworkGraph from "./Netwok"
import NetworkGraph2 from './NetwokEditing'
import { STATUS_RESPONSE } from "../../utils/constants"
import { warningAlert, successAlert } from "../../utils/alerts"
export const KycContext = createContext();

function StaticComponent(props) {
  const {
    className,
    UserDetails,
    getKYCUserDetailsDispatch,
    getKYCDashboardSummaryDispatch,
    distanceData,
    kyc_dashboard_summary_positive,
    isLoading_kyc_dashboard_summary,
    kyc_dashboard_summary_negavite,
    kyc_dashboard_summary,
    kyc_dashboard_details,
    CommonFileDispatch,
    DistanceRes,
    AllDashboardDispatch,
    AllDashboardRes,
    DashboardAadhaarDispatch,
    DasboardPanDispatch,
    DashboardAadharRes,
    DashboardPANres,
    DashboardCINDispatch,
    DashboardCINres,
    KycScoreDispatch,
    linkAnalyticsDispatch,
    LinkAnalyticsRes,
    VideoKYCDispatch,
    VideoKycRes,
    UENdispatch,
    UENdashboardRes,
    FullKycResData,
    clearFullKycValueDispatch,
    fullKycDispatch,
    FullKycResDataLoading
  } = props


  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws"
  })
  const totalPhoneScore =
    kyc_dashboard_summary && kyc_dashboard_summary?.phoneTotalScore;
  const totalEmailScore =
    kyc_dashboard_summary && kyc_dashboard_summary?.emailTotalScore;

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [openPhone, setOpenPhone] = useState(false)
  const [openEmail, setOpenEmail] = useState(false)
  const [openAddress, setOpenAddress] = useState(false)
  const [openIpAddress, setOpenIpAddress] = useState(false)
  const [openBusinessPhone, setOpenBusinessPhone] = useState(false)
  const [openBusinessEmail, setOpenBusinessEmail] = useState(false)
  const [openBusinessAddress, setOpenBusinessAddress] = useState(false)
  const [openMap, setopenMap] = useState(false)
  const [openBusinessMap, setopenBusinessMap] = useState(false)
  const [openWebsite, setOpenWebsite] = useState(false)
  const [key, setKey] = useState("kyc")
  const [summaryData, setSummaryData] = useState({})
  const [dashboardData, setDashboardData] = useState({})
  const [activestep, setActiveStep] = useState(0)
  const [completed] = useState({})
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const steps = getSteps()
  const style = {
    width: "100%",
    height: "100%",
  }
  const containerStyle = {
    width: "50%",
    height: "50%",
  }

  function getSteps() {
    return [
      {
        label: "Merchant Review",
        className: "btn label-one",
        stepCount: 0,
      },
      // {
      //   label: "Network",
      //   stepCount: 1,
      //   className: "btn label-seven",
      // },
      {
        label: "PHONE",
        stepCount: 1,
        className: "btn label-two",
      },
      {
        label: "EMAIL",
        stepCount: 2,
        className: "btn label-three",
      },
      {
        label: "ADDRESS",
        stepCount: 3,
        className: "btn label-four",
      },
      // {
      //   label: "IP ADDRESS",
      //   stepCount: 5,
      //   className: "btn label-five",
      // },
      {
        label: "Website",
        stepCount: 4,
        className: "btn label-six",
      },
    ];
  }
  const temp_data = {
    addresspositive: true,
    addressnegative: true,
    emailpositive: true,
    emailnegative: true,
    ipaddressnegative: true,
    ipaddresspositive: true,
    TotalScore: totalEmailScore,
  };
  const Email_value = {
    emailTotalScore: totalEmailScore,
    emailpositive:
      kyc_dashboard_summary && kyc_dashboard_summary?.emailpositive,
    // kyc_dashboard_summary &&
    // kyc_dashboard_summary?.emailpositive.map((item) => {
    //   return item?.title;
    // }),
    emailnegative:
      kyc_dashboard_summary && kyc_dashboard_summary?.emailnegative,
    verifyEmail:
      kyc_dashboard_summary &&
      kyc_dashboard_summary?.getAllApiData?.[0].emailApiResponse?.data,
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <Merchant
            id={id}
            setOpenPhone={setOpenPhone}
            setOpenEmail={setOpenEmail}
            setOpenAddress={setOpenAddress}
            setOpenIpAddress={setOpenIpAddress}
            setOpenBusinessPhone={setOpenBusinessPhone}
            setOpenBusinessEmail={setOpenBusinessEmail}
            setOpenBusinessAddress={setOpenBusinessAddress}
            openMap={openMap}
            setopenMap={setopenMap}
            openBusinessMap={openBusinessMap}
            setopenBusinessMap={setopenBusinessMap}
            setOpenWebsite={setOpenWebsite}
            merchantSummary={UserDetails}
            isLoaded={isLoaded}
            kyc_dashboard_summary_positive={kyc_dashboard_summary_positive}
            isLoading_kyc_dashboard_summary={isLoading_kyc_dashboard_summary}
            kyc_dashboard_summary_negavite={kyc_dashboard_summary_negavite}
            distanceData={distanceData}
            kyc_dashboard_details={kyc_dashboard_details}
            UserDetails={UserDetails}
            DistanceRes={DistanceRes}
          />
        );
      // case 1:
      //   return <NetworkGraph />;
      case 1:
        return <Phone
          id={id} totalPhoneScore={totalPhoneScore}
          kyc_dashboard_summary={kyc_dashboard_summary}
          kyc_dashboard_details={kyc_dashboard_details}
          isLoading_kyc_dashboard_summary={isLoading_kyc_dashboard_summary}
          AllDashboardRes={AllDashboardRes}
        />
      case 2:
        return (
          <Email
            dashboard={Email_value}
            isLoading={isLoading_kyc_dashboard_summary}
            kyc_dashboard_details={kyc_dashboard_details}
            isLoading_kyc_dashboard_summary={isLoading_kyc_dashboard_summary}
            AllDashboardRes={AllDashboardRes}
          />
        );
      case 3:
        return (
          <Address
            // summary={summaryData}
            dashboard={temp_data}
            isLoaded={false}
            kyc_dashboard_details={kyc_dashboard_details}
            isLoading_kyc_dashboard_summary={isLoading_kyc_dashboard_summary}
            AllDashboardRes={AllDashboardRes}
          />
        );
      // case 5:
      //   return (
      //     <IpAddress
      //       //  summary={summaryData}
      //       dashboard={temp_data}
      //     />
      //   );
      case 4:
        return (
          <KycWebsite
            //  merchantIddetails={merchantIddetails}
            UserDetails={UserDetails}
            kyc_dashboard_details={kyc_dashboard_details}
            isLoading_kyc_dashboard_summary={isLoading_kyc_dashboard_summary}
            AllDashboardRes={AllDashboardRes}
          />
        );
      default:
        return "unknown step";
    }
  }

  const handleNext = (step) => {
    setActiveStep(activestep + 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleBack = () => {
    setActiveStep(activestep - 1);
  };

  useEffect(() => {
    if (openPhone) {
      setActiveStep(1);
    }
    if (openEmail) {
      setActiveStep(2);
    }
    if (openAddress) {
      setActiveStep(3);
    }
    if (openIpAddress) {
      setActiveStep(5);
    }
    if (openWebsite) {
      setActiveStep(4);
    }
    if (activestep !== 1) {
      setOpenPhone(false);
      setOpenEmail(false);
      setOpenAddress(false);
      setOpenIpAddress(false);
      setOpenBusinessPhone(false);
      setOpenBusinessEmail(false);
      setOpenBusinessAddress(false);
      setOpenWebsite(false);
    }
  }, [
    openPhone,
    openEmail,
    activestep,
    openAddress,
    openIpAddress,
    openBusinessPhone,
    openBusinessEmail,
    openBusinessAddress,
    openWebsite,
  ])

  // useEffect(() => {
  //   getKYCUserDetailsDispatch(id)
  //   DashboardAadhaarDispatch(id)
  //   DasboardPanDispatch(id)
  //   DashboardCINDispatch(id)
  //   KycScoreDispatch(id)
  //   linkAnalyticsDispatch(id)
  //   VideoKYCDispatch(id)
  //   UENdispatch(id)
  // }, [id])

  useEffect(() => {
    if (id) {
      getKYCDashboardSummaryDispatch(id)
      CommonFileDispatch(id)
      AllDashboardDispatch(id)
      getKYCUserDetailsDispatch(id)
      linkAnalyticsDispatch(id)
      KycScoreDispatch(id)
    }
  }, [id])


  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(FullKycResData && FullKycResData.message, "success")
      getKYCDashboardSummaryDispatch(id)
      CommonFileDispatch(id)
      AllDashboardDispatch(id)
      getKYCUserDetailsDispatch(id)
      clearFullKycValueDispatch()
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  useEffect(() => {
    const handleContextmenu = e => {
      e.preventDefault()
    }
    document.addEventListener('contextmenu', handleContextmenu)
    return function cleanup() {
      document.removeEventListener('contextmenu', handleContextmenu)
    }
  }, [])

  useEffect(() => {
    document.addEventListener('keydown', (e) => {
      if (e.ctrlKey && e.key == 'p') {
        alert('This section is not allowed to print Screenshots');
        e.cancelBubble = true;
        e.preventDefault();
        e.stopImmediatePropagation();
      }
    })
    document.addEventListener('keyup', (e) => {
      if (e.key == 'PrintScreen') {
        navigator.clipboard.writeText('');
        alert('Screenshot disabled!');
      }
    })
  }, [])

  return (
    <>
      <div className="mt-0">
        <Tabs
          id="controlled-tab-example"
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className="ctab mb-3"
        >
          <Tab eventKey="kyc" title="eKYC">
            <KycContext.Provider value={ UserDetails && UserDetails.data ?UserDetails.data : '' }>
              <UserProfileKYC
                UserDetails={UserDetails}
                DistanceRes={DistanceRes}
                DashboardPANres={DashboardPANres}
                DashboardAadharRes={DashboardAadharRes}
                DashboardCINres={DashboardCINres}
                VideoKycRes={VideoKycRes}
                UENdashboardRes={UENdashboardRes}
                UpdateKycDispatch={fullKycDispatch}
                FullKycResData={FullKycResData}
              />
            </KycContext.Provider>
          </Tab>
          <Tab eventKey="network" title="Network">
            <NetworkGraph2
              UserDetails={UserDetails}
              DistanceRes={DistanceRes}
              DashboardPANres={DashboardPANres}
              DashboardAadharRes={DashboardAadharRes}
              DashboardCINres={DashboardCINres}
              networkData={LinkAnalyticsRes}
            />
          </Tab>
        </Tabs>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { KYCUserStoreKey, KYCDashboardSummaryStoreKey, DistanceStore, AllDashboardStore,
    DashBoardAadharStore, DashBoardPANStore, DashBoardCINStore, LinkAnalyticsStore, VideoKYCStore,
    UENdashboardStore, FullKycValueStore
  } = state
  return {
    UserDetails: KYCUserStoreKey && KYCUserStoreKey.KYCUser ? KYCUserStoreKey.KYCUser : '',
    kyc_dashboard_summary:
      KYCDashboardSummaryStoreKey && KYCDashboardSummaryStoreKey
        ? KYCDashboardSummaryStoreKey.KYCDashboardSummary?.data
        : {},
    kyc_dashboard_summary_positive:
      KYCDashboardSummaryStoreKey && KYCDashboardSummaryStoreKey
        ? KYCDashboardSummaryStoreKey.KYCDashboardSummary?.data?.positive
        : {},
    kyc_dashboard_summary_negavite:
      KYCDashboardSummaryStoreKey && KYCDashboardSummaryStoreKey
        ? KYCDashboardSummaryStoreKey.KYCDashboardSummary?.data?.negative
        : {},
    isLoading_kyc_dashboard_summary:
      KYCDashboardSummaryStoreKey && KYCDashboardSummaryStoreKey
        ? KYCDashboardSummaryStoreKey.loading
        : {},
    distanceData:
      KYCDashboardSummaryStoreKey && KYCDashboardSummaryStoreKey
        ? KYCDashboardSummaryStoreKey.KYCDashboardSummary?.data?.distanceData
        : {},
    kyc_dashboard_details: KYCDashboardSummaryStoreKey && KYCDashboardSummaryStoreKey.KYCDashboardSummary ? KYCDashboardSummaryStoreKey.KYCDashboardSummary : {},
    DistanceRes: DistanceStore && DistanceStore.DistanceRes ? DistanceStore.DistanceRes : {},
    AllDashboardRes: AllDashboardStore && AllDashboardStore.AllDashboardRes ? AllDashboardStore.AllDashboardRes : {},
    DashboardAadharRes: DashBoardAadharStore && DashBoardAadharStore.DashboardAadharRes ? DashBoardAadharStore.DashboardAadharRes : {},
    DashboardPANres: DashBoardPANStore && DashBoardPANStore.DashboardPANRes ? DashBoardPANStore.DashboardPANRes : {},
    DashboardCINres: DashBoardCINStore && DashBoardCINStore.DashboardCINres ? DashBoardCINStore.DashboardCINres : {},
    LinkAnalyticsRes: LinkAnalyticsStore && LinkAnalyticsStore.LinkAnalyticsRes && LinkAnalyticsStore.LinkAnalyticsRes.data ? LinkAnalyticsStore.LinkAnalyticsRes.data : {},
    VideoKycRes: VideoKYCStore && VideoKYCStore.VideoKycRes ? VideoKYCStore.VideoKycRes : {},
    UENdashboardRes: UENdashboardStore && UENdashboardStore.UENdashboardRes ? UENdashboardStore.UENdashboardRes : {},
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    FullKycResDataLoading: FullKycValueStore && FullKycValueStore.loading ? FullKycValueStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
  getKYCDashboardSummaryDispatch: (id) => dispatch(KYCDashboardSummaryAction.KYCDashboardSummary_INIT(id)),
  CommonFileDispatch: (id) => dispatch(DistanceAction.Distance(id)),
  AllDashboardDispatch: (id) => dispatch(AllDasboardDataAction.AllDashboard(id)),
  DashboardAadhaarDispatch: (id) => dispatch(DasboardAadharAction.DashboardAadhaar(id)),
  DasboardPanDispatch: (id) => dispatch(DasboardPanAction.DashboardPAN(id)),
  DashboardCINDispatch: (id) => dispatch(DasboardCinAction.DashboardCIN(id)),
  KycScoreDispatch: (id) => dispatch(KycScoreAction.KycScore(id)),
  linkAnalyticsDispatch: (id) => dispatch(linkAnalyticsAction.linkAnalytics(id)),
  VideoKYCDispatch: (id) => dispatch(VideoKYCAction.VideoKYC(id)),
  UENdispatch: (id) => dispatch(UENdashboardAction.UENdashboard(id)),

  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(StaticComponent);
