// import React, { useState, Fragment } from "react"
// import { GoogleMap, InfoWindow, Marker, Polyline } from "@react-google-maps/api"
// import { MAP } from '../../utils/constants'

// function MapGoogle(props) {
//   const { mapData, mapMarkers, viweLat, viweLng } = props
//   const [activeMarker, setActiveMarker] = useState(null)
//   const [center, setCenter] = useState({ lat: viweLat, lng: viweLng })

//   const polyOptions = {
//     strokeColor: '#FF0000',
//     strokeOpacity: 0.8,
//     strokeWeight: 2,
//     fillColor: '#FF0000',
//     fillOpacity: 0.35,
//     clickable: false,
//     draggable: false,
//     editable: false,
//     visible: true,
//     radius: 30000,
//     zIndex: 1
//   }

//   const handleActiveMarker = (marker) => {
//     if (marker === activeMarker) {
//       return
//     }
//     setActiveMarker(marker)
//   }

//   return (
//     <GoogleMap
//       zoom={10}
//       center={center}
//       onClick={() => setActiveMarker(null)}
//       mapContainerStyle={{ width: "100%", height: "100vh" }}>
//       {mapMarkers && mapMarkers.map((mark, index) => (
//         <Fragment key={"M_" + index}>
//           <Marker
//             position={{
//               lat: mark.lat,
//               lng: mark.lng
//             }}
//             onClick={() => handleActiveMarker(index)}
//             icon={`${MAP[mark.area]}`}>
//             {activeMarker === index ? (
//               <InfoWindow onCloseClick={() => setActiveMarker(null)}>
//                 <div>{mark.area}</div>
//               </InfoWindow>
//             ) : null}
//           </Marker>
//         </Fragment>
//       ))}
//     </GoogleMap>
//   )
// }

// export default MapGoogle

import React from "react"
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  InfoWindow,
  Marker,
  Polyline,
} from "react-google-maps"
import { MAP } from "../../utils/constants"

function MapGoogle (props) {
    const { mapData, mapMarkers, viweLat, viweLng } = props
    const google = window.google
    const onLoad = (polyline) => {}

    const options = {
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: false,
      draggable: false,
      editable: false,
      visible: true,
      radius: 30000,
      zIndex: 1,
    }

    const MapWithAMarker = withScriptjs(
      withGoogleMap((props) => (
        <GoogleMap
          defaultZoom={8}
          defaultCenter={{ lat: viweLat, lng: viweLng }}
          style={{ height: "100vh", width: "100%" }}
          center={{ lat: viweLat, lng: viweLng }}
        >
          {mapMarkers &&
            mapMarkers.map((mark, i) => (
              <Marker
                key={i}
                position={{
                  lat: mark.lat,
                  lng: mark.lng,
                }}
                defaultCenter={{
                  lat: mark.lat,
                  lng: mark.lng,
                }}
                draggable={false}
                icon={`${MAP[mark.area]}`}
              >
                <InfoWindow>
                  <div>{mark.area}</div>
                </InfoWindow>
                <Polyline onLoad={onLoad} path={mapMarkers} options={options} />
              </Marker>
            ))}
        </GoogleMap>
      ))
    )

    return (
      <div style={{ padding: "1rem", maxWidth: 10000 }}>
        <MapWithAMarker
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
    )
}

export default MapGoogle
