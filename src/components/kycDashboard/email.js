import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import BusinessEmail from "../riskSummary/subComponent/businessEmail";
import _ from "lodash";

function Email(props) {
  const { 
    summary, 
    dashboard, 
    isLoading, 
    kyc_dashboard_details, 
    isLoading_kyc_dashboard_summary,
    AllDashboardRes
  } = props

  let viewEmail =
    typeof dashboard !== "undefined" && dashboard?.verifyEmail
      ? typeof dashboard === "object" &&
      dashboard.verifyEmail?.[0]?.verify_email?.[0]?.syntax
      : null;

  const emailPositive =
    kyc_dashboard_details &&
      kyc_dashboard_details.data &&
      kyc_dashboard_details.data.emailpositive
      ? kyc_dashboard_details.data.emailpositive
      : "--"
  const emailNegative =
    kyc_dashboard_details &&
      kyc_dashboard_details?.data &&
      kyc_dashboard_details.data.emailnegative
      ? kyc_dashboard_details.data.emailnegative
      : "--"

  const emailTotal = kyc_dashboard_details && kyc_dashboard_details.data && kyc_dashboard_details.data.emailTotalScore ? kyc_dashboard_details.data.emailTotalScore : 0
  const emailData = AllDashboardRes && AllDashboardRes.data && AllDashboardRes.data.email ? AllDashboardRes.data.email : '--'
  let validateEmail = emailData && emailData.verify_email_v2 ? emailData.verify_email_v2 : '--'
  
  return (
    <div>
      <div className="row mt-8">
        <div className="col-lg-12">
          <h1 className="d-flex justify-content-center mb-4">
            Individual Email
          </h1>
        </div>
        <div className="d-flex justify-content-center mb-4">
          {!isLoading ? (
            <CircularProgressbarWithChildren
              value={emailTotal}
              text={`${emailTotal}`}
              strokeWidth={10}
              circleRatio={1}
              styles={buildStyles({
                rotation: 0.5,
                strokeLinecap: "butt",
                textColor: "mediumseagreen",
                pathColor: "#ed5555",
                trailColor: "mediumseagreen",
              })}
            >
              <div style={{ fontSize: 12, marginTop: 55 }}>
                <strong>Risk Score {emailTotal}</strong>
              </div>
            </CircularProgressbarWithChildren>
          ) : (
            <div className="text-center">
              <div className="spinner-border text-primary m-5" role="status" />
            </div>
          )}
        </div>
      </div>
      <div className="row mb-8 mt-8">
        <div className="col-lg-12">
          <div className="row mb-4">
            <div className="col-lg-6">
              <h1 className="mb-4">Positive Score Factors</h1>
              {isLoading_kyc_dashboard_summary ? (
                <div>
                  <div className="text-center">
                    <div
                      className="spinner-border text-primary m-5"
                      role="status"
                    />
                  </div>
                </div>
              ) : _.isEmpty(emailPositive) ? (
                <span className=" text-danger" role="status">
                  N/A
                </span>
              ) : (
                emailPositive &&
                emailPositive.map((item, i) => {
                  return (
                    <div className="mb-4" key={i}>
                      <div className="symbol symbol-45px me-4 mb-4">
                        <span className="symbol-label bg-success">
                          <span className="fs-5 text-white">
                            {item.riskscore}
                          </span>
                        </span>
                      </div>
                      <span className="fw-bold fs-5">{item.title}</span>
                    </div>
                  );
                })
              )}
            </div>
            <div className="col-lg-6">
              <h1 className="mb-4">Negative Score Factors</h1>
              {isLoading_kyc_dashboard_summary ? (
                <div>
                  <div className="text-center">
                    <div
                      className="spinner-border text-primary m-5"
                      role="status"
                    />
                  </div>
                </div>
              ) : _.isEmpty(emailNegative) ? (
                <span className=" text-danger" role="status">
                  N/A
                </span>
              ) : (
                emailNegative &&
                emailNegative.map((item, i) => {
                  return (
                    <div className="mb-4" key={i}>
                      <div className="symbol symbol-45px me-4 mb-4">
                        <span className="symbol-label bg-danger">
                          <span className="fs-5 text-white">
                            {item.riskscore}
                          </span>
                        </span>
                      </div>
                      <span className="fw-bold fs-5">{item.title}</span>
                    </div>
                  );
                })
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="mb-8">
          <a href="#" className="d-flex justify-content-center fs-2">
            CONFIDENCE MATRIX
          </a>
        </div>
      </div>
      <div className="row g-5 g-xl-8 mb-8">
        <div className="col-xl-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-header bg-col-bis border-0 ">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">Email</span>
              </h3>
            </div>
            <div className="card-body pt-0">
            <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3 ellipsis">
                    Blacklisted Email Check
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                      {emailData && emailData.blacklisted_email_check && emailData.blacklisted_email_check.blacklisted_email_check
                        ? emailData.blacklisted_email_check.blacklisted_email_check
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Risk Score
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                    {emailData && emailData.blacklisted_email_check && emailData.blacklisted_email_check.risk_score
                        ? emailData.blacklisted_email_check.risk_score
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-header bg-col-bis border-0 ">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Verify Details
                </span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      First Name
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {validateEmail && validateEmail.first_name
                        ? validateEmail.first_name
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Associated Names
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {validateEmail && validateEmail.associated_names && validateEmail.associated_names.status
                        ? validateEmail.associated_names.status
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3 ">
                    Associated Phone
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                    {validateEmail && validateEmail.associated_phone_numbers && validateEmail.associated_phone_numbers.status
                        ? validateEmail.associated_phone_numbers.status
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>

              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Deliverability
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {validateEmail && validateEmail.deliverability
                        ? validateEmail.deliverability
                        : "__"}
                    </span>
                  </div>
                </div>
              </div>

              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                   Domain Age Human
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                    {validateEmail && validateEmail.domain_age && validateEmail.domain_age.human
                        ? validateEmail.domain_age.human
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    ISO
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                    {validateEmail && validateEmail.domain_age && validateEmail.domain_age.iso
                        ? validateEmail.domain_age.iso
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Timestamp
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.domain_age && validateEmail.domain_age.timestamp
                        ? validateEmail.domain_age.timestamp
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Domain Velocity
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.domain_velocity
                        ? validateEmail.domain_velocity
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Overall_score
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.overall_score
                        ? validateEmail.overall_score
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Request Id
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.request_id
                        ? validateEmail.request_id
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Sanitized Email
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.sanitized_email
                        ? validateEmail.sanitized_email
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Smtp Score
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.smtp_score
                        ? validateEmail.smtp_score
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    Spam Trap Score
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.spam_trap_score
                        ? validateEmail.spam_trap_score
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                    User Activity
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6 ">
                    {validateEmail && validateEmail.user_activity
                        ? validateEmail.user_activity
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Email;
