import React, { useState, useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { FullKycValueAction } from '../../store/actions'
import _ from 'lodash'

function EditDashboard(props) {
  const {
    loading,
    queuesAddDispatch,
    UserDetails,
    fullKycDispatch,
    FullKycLoading
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]

  const [formData, setFormData] = useState({
    contactName: '',
    contactEmail: '',
    aadharNumber: '',
    individualPan: '',
    cin: '',
    businessPan: ''
  })
  const [errors, setErrors] = useState({
    contactName: '',
    contactEmail: '',
    aadharNumber: '',
    individualPan: '',
    cin: '',
    businessPan: ''
  })
  const [showBanner, setShowBanner] = useState(false)
  const [show, setShow] = useState(false)


  const handleSubmit = (e) => {
    const errors = {}
    if (_.isEmpty(formData.contactName)) {
      errors.contactName = 'Name Is Required'
    }
    if (_.isEmpty(formData.contactEmail)) {
      errors.contactEmail = 'Email Is Required'
    }
    if (_.isEmpty(formData.aadharNumber)) {
      errors.aadharNumber = 'Aadhar Number Is Required'
    }
    if (_.isEmpty(formData.individualPan)) {
      errors.individualPan = 'Individual Pan Is Required'
    }
    if (_.isEmpty(formData.cin)) {
      errors.cin = 'CIN Is Required'
    }
    if (_.isEmpty(formData.businessPan)) {
      errors.businessPan = 'Business Pan Is Required'
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      fullKycDispatch(currentId, formData)
    }
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    formData[name] = value
    setFormData(formData)
    setErrors({ ...errors, [name]: '' })
  }

  useEffect(() => {
    if (UserDetails) {
      const userIdDetails = {}
      userIdDetails.contactName = UserDetails.contactName
      userIdDetails.contactEmail = UserDetails.contactEmail
      userIdDetails.aadharNumber = UserDetails.aadharNumber
      userIdDetails.individualPan = UserDetails.individualPan
      userIdDetails.businessPan = UserDetails.businessPan
      userIdDetails.cin = UserDetails.cin
      setFormData(userIdDetails)
    }

  }, [UserDetails])

  return (
    <>
      {/* begin::Wrapper */}
      <div className='row'>
        <div className='col-lg-4'>

          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Name</label>
            <input
              placeholder='Name'
              className={clsx(
                'form-control form-control-lg form-control-solid',
                { 'is-invalid': formData.contactName && errors.contactName },
                {
                  'is-valid': formData.contactName && !errors.contactName
                }
              )}
              onChange={(e) => handleChange(e)}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
              type='text'
              name='contactName'
              autoComplete='off'
              value={formData.contactName || ''}
            />
            {errors.contactName && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.contactName}</span>
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Individual Pan</label>
            <input
              placeholder='individualPan'
              className={clsx(
                'form-control form-control-lg form-control-solid',
                { 'is-invalid': formData.individualPan && errors.individualPan },
                {
                  'is-valid': formData.individualPan && !errors.individualPan
                }
              )}
              onChange={(e) => handleChange(e)}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
              type='text'
              name='individualPan'
              autoComplete='off'
              value={formData.individualPan || ''}
            />
            {errors.individualPan && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.individualPan}</span>
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Aadhaar Front Image</label>
            <div>
            <a 
              className='btn btn-sm btn-primary'
              onClick={()=> window.open(`${UserDetails && UserDetails.aadhaar_frontImage}`, "_blank")}
            >
              Aadhar Front
            </a>
            </div>
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Business Pan Image</label>
            <div>
            <a 
              className='btn btn-sm btn-primary'
              onClick={()=> window.open(`${UserDetails && UserDetails.businessPanImage}`, "_blank")}
            >
              Business Pan
            </a>
            </div>
          </div>
        </div>
        <div className='col-lg-4'>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Email</label>
            <input
              placeholder='contactEmail'
              className={clsx(
                'form-control form-control-lg form-control-solid',
                { 'is-invalid': formData.contactEmail && errors.contactEmail },
                {
                  'is-valid': formData.contactEmail && !errors.contactEmail
                }
              )}
              onChange={(e) => handleChange(e)}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
              type='text'
              name='contactEmail'
              autoComplete='off'
              value={formData.contactEmail || ''}
            />
            {errors.contactEmail && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.contactEmail}</span>
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Business Pan</label>
            <input
              placeholder='Business Pan'
              className={clsx(
                'form-control form-control-lg form-control-solid',
                { 'is-invalid': formData.businessPan && errors.businessPan },
                {
                  'is-valid': formData.businessPan && !errors.businessPan
                }
              )}
              onChange={(e) => handleChange(e)}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
              type='text'
              name='businessPan'
              autoComplete='off'
              value={formData.businessPan || ''}
            />
            {errors.businessPan && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.businessPan}</span>
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Aadhaar Back Image</label>
            <div>
            <a 
              className='btn btn-sm btn-primary'
              onClick={()=> window.open(`${UserDetails && UserDetails.aadhaar_backImage}`, "_blank")}
            >
              Aadhar Back
            </a>
            </div>
          </div>
        </div>
        <div className='col-lg-4'>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>Aadhar Number</label>
            <input
              placeholder='AadharNumber'
              className={clsx(
                'form-control form-control-lg form-control-solid',
                { 'is-invalid': formData.aadharNumber && errors.aadharNumber },
                {
                  'is-valid': formData.aadharNumber && !errors.aadharNumber
                }
              )}
              onChange={(e) => handleChange(e)}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
              type='text'
              name='aadharNumber'
              autoComplete='off'
              value={formData.aadharNumber || ''}
            />
            {errors.aadharNumber && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.aadharNumber}</span>
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>CIN</label>
            <input
              placeholder='CIN'
              className={clsx(
                'form-control form-control-lg form-control-solid',
                { 'is-invalid': formData.cin && errors.cin },
                {
                  'is-valid': formData.cin && !errors.cin
                }
              )}
              onChange={(e) => handleChange(e)}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
              type='text'
              name='cin'
              autoComplete='off'
              value={formData.cin || ''}
            />
            {errors.cin && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.cin}</span>
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='form-label fs-6 fw-bolder text-dark'>individual Pan Image</label>
            <div>
            <a 
              className='btn btn-sm btn-primary'
              onClick={()=> window.open(`${UserDetails && UserDetails.individualPanImage}`, "_blank")}
            >
              Individual Pan
            </a>
            </div>
          </div>
        </div>
        {/* end::Action */}
        <div className='d-flex justify-content-end '>
          <Link
            to='/manage-queues'
            disabled={loading}
            className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
          >
            Back
          </Link>
          <button
            type='button'
            className='btn btn-sm btn-light-primary m-2 fa-pull-right'
            onClick={(e) => handleSubmit(e)}
            disabled={FullKycLoading}
          >
            {!FullKycLoading && <span className='indicator-label'>Submit</span>}
            {FullKycLoading && (
              <span className='indicator-progress' style={{ display: 'block' }}>
                Please wait...
                <span className='spinner-border spinner-border-sm align-middle ms-2' />
              </span>
            )}
          </button>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { queuesAddStore, FullKycValueStore } = state
  return {
    queueStatus: queuesAddStore && queuesAddStore.queueStatus ? queuesAddStore.queueStatus : '',
    FullKycLoading: FullKycValueStore && FullKycValueStore.loading ? FullKycValueStore.loading : false,
  }
}

const mapDispatchToProps = dispatch => ({
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditDashboard)
