import { useRef, useEffect, useState, Fragment } from 'react'
import Xarrow, { useXarrow } from 'react-xarrows'
import Icofont from 'react-icofont'
import '../riskSummary/subComponent/index.css'
import { useLocation, Link } from 'react-router-dom'
import { Scrollbars } from 'react-custom-scrollbars'

const MainColor = {
  "Phone": "#009EF7",
  "IP Address": "#F1416C",
  "Email": "#50CD89",
  "Website": "#7239EA",
  "CIN": "#FFC700",
  "GST Number": "#e45e9c",
  "Business Address": "rgb(18 68 230)",
  "Company Name": "#369a06",
  "Authorised Pan Number": "rgb(96 122 27)",
  "Business Pan Number": "rgb(210 192 8)",
  "Aadhar Number": "rgb(210 123 8)"
}
const SubColor = {
  "Phone": "#D3EFFF",
  "IP Address": "#F9DBE4",
  "Email": "#BFFDDC",
  "Website": "#E1D7F9",
  "CIN": "#FFF0B6",
  "GST Number": "#edd2de",
  "Business Address": "rgb(212 218 238)",
  "Company Name": "rgb(139 233 139)",
  "Authorised Pan Number": "rgb(207 234 161)",
  "Business Pan Number": "rgb(240 239 209)",
  "Aadhar Number": "rgb(240 239 209)"
}
const Icon = {
  "Phone": "icofont-ui-touch-phone",
  "IP Address": "icofont-eclipse",
  "Email": "icofont-email",
  "Website": "icofont-web",
  "CIN": "icofont-computer",
  "GST Number": "icofont-contrast",
  "Business Address": "icofont-business-man-alt-1",
  "Company Name": "icofont-card",
  "Aadhar Number": "icofont-ui-user-group",
  "Business Pan Number": "icofont-ui-user-group",
  "Authorised Pan Number": "icofont-ui-v-card",
}

function NetworkGraph(props) {
  const {
    className,
    networkData,
    UserDetails,
  } = props

  const url = useLocation().pathname
  const fields = url && url.split('/')
  const id = fields && fields[3]

  const [limit, setLimit] = useState(25)
  const updateXarrow = useXarrow()
  const [path, pathType] = useState("straight") //"smooth" | "grid" | "straight"
  const [childlineColor, setChildlineColor] = useState("#E1F0FF") // Default Color
  const [highlight, setHighlight] = useState(null)

  const HighlightHandler = (lighter) => {
    setHighlight(lighter)
  }

  useEffect(() => {
    window.dispatchEvent(new Event('resize')) //For Fixing
  })

  return (
    <div className='row'>
      <div className='col-lg-12 col-md-12 col-sm-12'>
        <div className='some-page-wrapper' id="triggerdiv">
          <Scrollbars style={{ width: "100%", height: 600 }}>
            <div className='graph-row'>
              <div className='column'>
                <span>KYC{UserDetails && UserDetails.kycRiskId ? UserDetails.kycRiskId : '--'}</span>
                <div className="box main" id="main" style={{ backgroundColor: highlight !== null ? MainColor[highlight] : "#E1F0FF" }}>
                  <Icofont icon="student-alt" style={{ color: highlight !== null ? SubColor[highlight] : "#009EF7" }} />
                </div>
              </div>
              <div className='column mid-column'>
                <span style={{ color: MainColor["Phone"] }}>
                  Phone : {UserDetails && UserDetails?.contactNumber}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Phone" ? MainColor["Phone"] : SubColor["Phone"] }} id="Phone" onClick={(e) => HighlightHandler("Phone")}>
                  <Icofont icon="icofont-ui-touch-phone" style={{ color: highlight === "Phone" ? SubColor["Phone"] : MainColor["Phone"] }} />
                </div>

                <span style={{ color: MainColor["IP Address"] }}>
                  IP Address : {UserDetails && UserDetails?.businessIpAddress}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"] }} id="IP Address" onClick={(e) => HighlightHandler("IP Address")}>
                  <Icofont icon="icofont-eclipse" style={{ color: highlight === "IP Address" ? SubColor["IP Address"] : MainColor["IP Address"] }} />
                </div>

                <span style={{ color: MainColor["Email"] }}>
                  Email : {UserDetails && UserDetails?.contactEmail}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Email" ? MainColor["Email"] : SubColor["Email"] }} id="Email" onClick={(e) => HighlightHandler("Email")}>
                  <Icofont icon="icofont-email" style={{ color: highlight === "Email" ? SubColor["Email"] : MainColor["Email"] }} />
                </div>

                <span style={{ color: MainColor["Website"] }}>
                  Website : {UserDetails && UserDetails?.website}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Website" ? MainColor["Website"] : SubColor["Website"] }} id="Website" onClick={(e) => HighlightHandler("Website")}>
                  <Icofont icon="icofont-web" style={{ color: highlight === "Website" ? SubColor["Website"] : MainColor["Website"] }} />
                </div>

                <span style={{ color: MainColor["CIN"] }}>
                  CIN : {UserDetails && UserDetails?.cin}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "CIN" ? MainColor["CIN"] : SubColor["CIN"] }} id="CIN" onClick={(e) => HighlightHandler("CIN")}>
                  <Icofont icon="icofont-computer" style={{ color: highlight === "CIN" ? SubColor["CIN"] : MainColor["CIN"] }} />
                </div>

                <span style={{ color: MainColor["GST Number"] }}>
                  GST Number : {UserDetails && UserDetails?.gstNumber}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "GST Number" ? MainColor["GST Number"] : SubColor["GST Number"] }} id="GST Number" onClick={(e) => HighlightHandler("GST Number")}>
                  <Icofont icon="icofont-contrast" style={{ color: highlight === "GST Number" ? SubColor["GST Number"] : MainColor["GST Number"] }} />
                </div>

                <span style={{ color: MainColor["Business Address"] }}>
                  Business Address : {UserDetails && UserDetails?.businessAddress}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Business Address" ? MainColor["Business Address"] : SubColor["Business Address"] }} id="Business Address" onClick={(e) => HighlightHandler("Business Address")}>
                  <Icofont icon="icofont-business-man-alt-1" style={{ color: highlight === "Business Address" ? SubColor["Business Address"] : MainColor["Business Address"] }} />
                </div>

                <span style={{ color: MainColor["Company Name"] }}>
                  Business PAN Name : {UserDetails && UserDetails?.businessPanName}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Company Name" ? MainColor["Company Name"] : SubColor["Company Name"] }} id="Company Name" onClick={(e) => HighlightHandler("Company Name")}>
                  <Icofont icon="icofont-card" style={{ color: highlight === "Company Name" ? SubColor["Company Name"] : MainColor["Company Name"] }} />
                </div>

                <span style={{ color: MainColor["Authorised Pan Number"] }}>
                  Individual Pan : {UserDetails && UserDetails?.individualPan}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Authorised Pan Number" ? MainColor["Authorised Pan Number"] : SubColor["Authorised Pan Number"] }} id="Authorised Pan Number" onClick={(e) => HighlightHandler("Authorised Pan Number")}>
                  <Icofont icon="icofont-ui-v-card" style={{ color: highlight === "Authorised Pan Number" ? SubColor["Authorised Pan Number"] : MainColor["Authorised Pan Number"] }} />
                </div>

                <span style={{ color: MainColor["Business Pan Number"] }}>
                  Business Pan : {UserDetails && UserDetails?.businessPan}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Business Pan Number" ? MainColor["Business Pan Number"] : SubColor["Business Pan Number"] }} id="Business Pan Number" onClick={(e) => HighlightHandler("Business Pan Number")}>
                  <Icofont icon="icofont-ui-user-group" style={{ color: highlight === "Business Pan Number" ? SubColor["Business Pan Number"] : MainColor["Business Pan Number"] }} />
                </div>

                <span style={{ color: MainColor["Aadhar Number"] }}>
                  Aadhar Number : {UserDetails && UserDetails?.aadharNumber}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Aadhar Number" ? MainColor["Aadhar Number"] : SubColor["Aadhar Number"] }} id="Aadhar Number" onClick={(e) => HighlightHandler("Aadhar Number")}>
                  <Icofont icon="icofont-penalty-card" style={{ color: highlight === "Aadhar Number" ? SubColor["Aadhar Number"] : MainColor["Aadhar Number"] }} />
                </div>

                <Xarrow start={'main'} end={'Phone'} path={path} color={highlight === "Phone" ? MainColor["Phone"] : SubColor["Phone"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Phone") }} />
                <Xarrow start={'main'} end={'IP Address'} path={path} color={highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("IP Address") }} />
                <Xarrow start={'main'} end={'Email'} path={path} color={highlight === "Email" ? MainColor["Email"] : SubColor["Email"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Email") }} />
                <Xarrow start={'main'} end={'Website'} path={path} color={highlight === "Website" ? MainColor["Website"] : SubColor["Website"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Website") }} />
                <Xarrow start={'main'} end={'CIN'} path={path} color={highlight === "CIN" ? MainColor["CIN"] : SubColor["CIN"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("CIN") }} />
                <Xarrow start={'main'} end={'GST Number'} path={path} color={highlight === "GST Number" ? MainColor["GST Number"] : SubColor["GST Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("GST Number") }} />
                <Xarrow start={'main'} end={'Business Address'} path={path} color={highlight === "Business Address" ? MainColor["Business Address"] : SubColor["Business Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Address") }} />
                <Xarrow start={'main'} end={'Company Name'} path={path} color={highlight === "Company Name" ? MainColor["Company Name"] : SubColor["Company Name"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Company Name") }} />
                <Xarrow start={'main'} end={'Authorised Pan Number'} path={path} color={highlight === "Authorised Pan Number" ? MainColor["Authorised Pan Number"] : SubColor["Authorised Pan Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Authorised Pan Number") }} />
                <Xarrow start={'main'} end={'Business Pan Number'} path={path} color={highlight === "Business Pan Number" ? MainColor["Business Pan Number"] : SubColor["Business Pan Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Pan Number") }} />
                <Xarrow start={'main'} end={'Aadhar Number'} path={path} color={highlight === "Aadhar Number" ? MainColor["Aadhar Number"] : SubColor["Aadhar Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Aadhar Number") }} />
              </div>
              <div className='column mt-4'>
                {networkData && networkData?.linkData && networkData.linkData.map((dummy, index) => (
                  <Fragment key={"Z_" + index}>
                    <span>
                      {
                        (() => {
                          if (dummy.source === "Phone") {
                            return dummy.contactNumber
                          }
                          else if (dummy.source === "IP Address") {
                            return dummy.businessIpAddress
                          }
                          else if (dummy.source === "Email") {
                            return dummy.contactEmail
                          }
                          else if (dummy.source === "Website") {
                            return dummy.website
                          }
                          else if (dummy.source === "CIN") {
                            return dummy?.cin
                          }
                          else if (dummy.source === "GST Number") {
                            return dummy?.gstNumber
                          }
                          else if (dummy.source === "Business Address") {
                            return dummy?.businessAddress
                          }
                          else if (dummy.source === "Company Name") {
                            return dummy?.businessPanName
                          }
                          else if (dummy.source === "Authorised Pan Number") {
                            return dummy?.individualPan
                          }
                          else if (dummy.source === "Business Pan Number") {
                            return dummy?.businessPan
                          }
                          else if (dummy.source === "Aadhar Number") {
                            return dummy?.aadharNumber
                          }
                          else {
                            return dummy.panOwnerName
                          }
                        })()
                      } - <Link>{`KYC${dummy.kycRiskId}`}</Link>
                    </span>
                    <div
                      key={"X_" + index}
                      className="box"
                      id={"XYZ_" + index}
                      style={{ backgroundColor: highlight === dummy.source ? MainColor[dummy.source] : SubColor[dummy.source], color: highlight === dummy.source ? SubColor[dummy.source] : MainColor[dummy.source] }}>
                      <span>
                        <Icofont icon={Icon[dummy.source]} style={{ color: highlight === dummy.source ? SubColor[dummy.source] : MainColor[dummy.source] }} />
                      </span>
                    </div>
                    <Xarrow
                      key={"Y_" + index}
                      start={dummy.source}
                      end={"XYZ_" + index}
                      path={path}
                      color={highlight === dummy.source ? MainColor[dummy.source] : SubColor[dummy.source]}
                      headSize={4}
                      strokeWidth={2}
                      dashness={highlight === dummy.source ? true : false
                      } />
                  </Fragment>
                ))}
              </div>
            </div>
          </Scrollbars>
        </div>
      </div>
    </div>
  )
}

export default NetworkGraph