import { useRef, useEffect, useState, Fragment } from 'react'
import Xarrow, { useXarrow } from 'react-xarrows'
import Icofont from 'react-icofont'
import '../riskSummary/subComponent/index.css'
import { useLocation, Link } from 'react-router-dom'
import { Scrollbars } from 'react-custom-scrollbars'
import _ from "lodash";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import './index.scss'

const MainColor = {
  "Phone": "#009EF7",
  // "IP Address": "#F1416C",
  "Email": "#50CD89",
  "Website": "#7239EA",
  // "CIN": "#FFC700",
  "GST Number": "#e45e9c",
  "Business Address": "rgb(18 68 230)",
  "Company Name": "#369a06",
  "Authorised Pan Number": "rgb(96 122 27)",
  "Business Pan Number": "rgb(210 192 8)",
  "Aadhar Number": "rgb(210 123 8)"
}
const SubColor = {
  "Phone": "#D3EFFF",
  // "IP Address": "#F9DBE4",
  "Email": "#BFFDDC",
  "Website": "#E1D7F9",
  // "CIN": "#FFF0B6",
  "GST Number": "#edd2de",
  "Business Address": "rgb(212 218 238)",
  "Company Name": "rgb(139 233 139)",
  "Authorised Pan Number": "rgb(207 234 161)",
  "Business Pan Number": "rgb(240 239 209)",
  "Aadhar Number": "rgb(240 239 209)"
}


function NetworkGraph2(props) {
  const {
    className,
    networkData,
    UserDetails,
  } = props

  const url = useLocation().pathname
  const fields = url && url.split('/')
  const id = fields && fields[3]

  const [limit, setLimit] = useState(25)
  const updateXarrow = useXarrow()
  const [path, pathType] = useState("straight") //"smooth" | "grid" | "straight"
  const [childlineColor, setChildlineColor] = useState("#E1F0FF") // Default Color
  const [highlight, setHighlight] = useState(null)

  const HighlightHandler = (lighter) => {
    setHighlight(lighter)
  }

  useEffect(() => {
    window.dispatchEvent(new Event('resize'))
  })

  const userData = UserDetails && UserDetails.data

  return (
    <div className='row'>
      <div className='col-lg-12 col-md-12 col-sm-12'>
        <div className='some-page-wrapper' id="triggerdiv">
          <Scrollbars style={{ width: "100%", height: 600 }}>
            <div className='graph-row'>
              <div className='column'>
                <span>KYC{userData && userData.kycId ? userData.kycId : '--'}</span>
                <div className="box main" id="main" style={{ backgroundColor: highlight !== null ? MainColor[highlight] : "#E1F0FF" }}>
                  <Icofont icon="student-alt" style={{ color: highlight !== null ? SubColor[highlight] : "#009EF7" }} />
                </div>
              </div>
              <div className='column mid-column'>
                <span style={{ color: MainColor["Phone"] }}>
                  Phone : {userData && userData?.phoneNumber?.number ? <>{`+${userData?.phoneNumber?.countryCode} 
                  ${userData?.phoneNumber?.number}`}</> : '--' }
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Phone" ? MainColor["Phone"] : SubColor["Phone"] }} id="Phone" onClick={(e) => HighlightHandler("Phone")}>
                  <Icofont icon="icofont-ui-touch-phone" style={{ color: highlight === "Phone" ? SubColor["Phone"] : MainColor["Phone"] }} />
                </div>

                {/* <span style={{ color: MainColor["IP Address"] }}>
                  IP Address : {userData && userData.businessIpAddress ? userData.businessIpAddress : '--'}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"] }} id="IP Address" onClick={(e) => HighlightHandler("IP Address")}>
                  <Icofont icon="icofont-eclipse" style={{ color: highlight === "IP Address" ? SubColor["IP Address"] : MainColor["IP Address"] }} />
                </div> */}

                <span style={{ color: MainColor["Email"] }}>
                  Email : {userData && userData?.emailId?.emailId ?  userData?.emailId?.emailId : '--'} 
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Email" ? MainColor["Email"] : SubColor["Email"] }} id="Email" onClick={(e) => HighlightHandler("Email")}>
                  <Icofont icon="icofont-email" style={{ color: highlight === "Email" ? SubColor["Email"] : MainColor["Email"] }} />
                </div>

                <span style={{ color: MainColor["Website"] }}>
                  Website : {userData && userData?.website ?  userData?.website : '--'} 
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Website" ? MainColor["Website"] : SubColor["Website"] }} id="Website" onClick={(e) => HighlightHandler("Website")}>
                  <Icofont icon="icofont-web" style={{ color: highlight === "Website" ? SubColor["Website"] : MainColor["Website"] }} />
                </div>

                {/* <span style={{ color: MainColor["CIN"] }}>
                  CIN : {userData && userData?.cin ?  userData?.cin : '--'} 
                </span>
                <div className="box" style={{ backgroundColor: highlight === "CIN" ? MainColor["CIN"] : SubColor["CIN"] }} id="CIN" onClick={(e) => HighlightHandler("CIN")}>
                  <Icofont icon="icofont-computer" style={{ color: highlight === "CIN" ? SubColor["CIN"] : MainColor["CIN"] }} />
                </div> */}

                <span style={{ color: MainColor["GST Number"] }}>
                  GST Number : {userData && userData?.gstNumber ?  userData?.gstNumber : '--'}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "GST Number" ? MainColor["GST Number"] : SubColor["GST Number"] }} id="GST Number" onClick={(e) => HighlightHandler("GST Number")}>
                  <Icofont icon="icofont-contrast" style={{ color: highlight === "GST Number" ? SubColor["GST Number"] : MainColor["GST Number"] }} />
                </div>

                <span style={{ color: MainColor["Business Address"] }}>
                  Business Address : 
                  <span>
                   {userData && userData?.businessAddress?.address ? userData?.businessAddress?.address : '--'}
                  </span><br />
                  <span>
                  {userData && userData?.businessAddress?.city && userData && userData?.businessAddress?.pincode && userData && userData?.businessAddress?.state ? <>{`${userData?.businessAddress?.city} - ${userData?.businessAddress?.pincode}, 
                  ${userData?.businessAddress?.state}`}</> : '--'}
                  </span>
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Business Address" ? MainColor["Business Address"] : SubColor["Business Address"] }} id="Business Address" onClick={(e) => HighlightHandler("Business Address")}>
                  <Icofont icon="icofont-business-man-alt-1" style={{ color: highlight === "Business Address" ? SubColor["Business Address"] : MainColor["Business Address"] }} />
                </div>

                <span style={{ color: MainColor["Company Name"] }}>
                  Business PAN Name : {userData && userData?.businessPanName ? userData?.businessPanName : '--'}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Company Name" ? MainColor["Company Name"] : SubColor["Company Name"] }} id="Company Name" onClick={(e) => HighlightHandler("Company Name")}>
                  <Icofont icon="icofont-card" style={{ color: highlight === "Company Name" ? SubColor["Company Name"] : MainColor["Company Name"] }} />
                </div>

                <span style={{ color: MainColor["Authorised Pan Number"] }}>
                  Individual Pan : {userData && userData?.individualPan ? userData?.individualPan : '--'}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Authorised Pan Number" ? MainColor["Authorised Pan Number"] : SubColor["Authorised Pan Number"] }} id="Authorised Pan Number" onClick={(e) => HighlightHandler("Authorised Pan Number")}>
                  <Icofont icon="icofont-ui-v-card" style={{ color: highlight === "Authorised Pan Number" ? SubColor["Authorised Pan Number"] : MainColor["Authorised Pan Number"] }} />
                </div>

                <span style={{ color: MainColor["Business Pan Number"] }}>
                  Business Pan : {userData && userData?.businessPan ? userData?.businessPan : '--'}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Business Pan Number" ? MainColor["Business Pan Number"] : SubColor["Business Pan Number"] }} id="Business Pan Number" onClick={(e) => HighlightHandler("Business Pan Number")}>
                  <Icofont icon="icofont-ui-user-group" style={{ color: highlight === "Business Pan Number" ? SubColor["Business Pan Number"] : MainColor["Business Pan Number"] }} />
                </div>

                <span style={{ color: MainColor["Aadhar Number"] }}>
                  Aadhar Number : {userData && userData?.aadharNumber ? userData?.aadharNumber : '--'}
                </span>
                <div className="box" style={{ backgroundColor: highlight === "Aadhar Number" ? MainColor["Aadhar Number"] : SubColor["Aadhar Number"] }} id="Aadhar Number" onClick={(e) => HighlightHandler("Aadhar Number")}>
                  <Icofont icon="icofont-penalty-card" style={{ color: highlight === "Aadhar Number" ? SubColor["Aadhar Number"] : MainColor["Aadhar Number"] }} />
                </div>

                <Xarrow start={'main'} end={'Phone'} path={path} color={highlight === "Phone" ? MainColor["Phone"] : SubColor["Phone"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Phone") }} />
                {/* <Xarrow start={'main'} end={'IP Address'} path={path} color={highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("IP Address") }} /> */}
                <Xarrow start={'main'} end={'Email'} path={path} color={highlight === "Email" ? MainColor["Email"] : SubColor["Email"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Email") }} />
                <Xarrow start={'main'} end={'Website'} path={path} color={highlight === "Website" ? MainColor["Website"] : SubColor["Website"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Website") }} />
                {/* <Xarrow start={'main'} end={'CIN'} path={path} color={highlight === "CIN" ? MainColor["CIN"] : SubColor["CIN"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("CIN") }} /> */}
                <Xarrow start={'main'} end={'GST Number'} path={path} color={highlight === "GST Number" ? MainColor["GST Number"] : SubColor["GST Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("GST Number") }} />
                <Xarrow start={'main'} end={'Business Address'} path={path} color={highlight === "Business Address" ? MainColor["Business Address"] : SubColor["Business Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Address") }} />
                <Xarrow start={'main'} end={'Company Name'} path={path} color={highlight === "Company Name" ? MainColor["Company Name"] : SubColor["Company Name"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Company Name") }} />
                <Xarrow start={'main'} end={'Authorised Pan Number'} path={path} color={highlight === "Authorised Pan Number" ? MainColor["Authorised Pan Number"] : SubColor["Authorised Pan Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Authorised Pan Number") }} />
                <Xarrow start={'main'} end={'Business Pan Number'} path={path} color={highlight === "Business Pan Number" ? MainColor["Business Pan Number"] : SubColor["Business Pan Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Business Pan Number") }} />
                <Xarrow start={'main'} end={'Aadhar Number'} path={path} color={highlight === "Aadhar Number" ? MainColor["Aadhar Number"] : SubColor["Aadhar Number"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Aadhar Number") }} />
              </div>
              <div className='column mt-4'>
                {networkData && networkData?.linkData && networkData.linkData.map((dummy, index) => {
                  return (
                    <Fragment key={"Z_" + index}>
                      <a
                        className='color-primary cursor-pointer'
                        onClick={() => window.open(`/static-summary/update/${dummy._id}`, "_blank")}
                        data-bs-toggle="tooltip"
                        data-bs-custom-class="tooltip-inverse"
                        data-bs-placement="top"
                        title="Tooltip on top">
                        KYC{dummy.kycId}
                      </a>
                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                      className='tooltip'
                      >
                      Business Name - {dummy && dummy.businessName}
                      {/* Source - { dummy && dummy.source.map((name) => {return( <>{name}</>)})} */}
                      </Tooltip>}
                      placement={"right"}
                      >
                      <div
                        key={"X_" + index}
                        className="box"
                        id={"XYZ_" + index}
                        style={{
                          backgroundColor: 'rgb(237 184 23)'
                        }}
                      >
                        <span>
                          <Icofont
                            icon={"student-alt"}
                            style={{
                              color: 'black'
                            }}
                          />
                        </span>
                      </div>
                      </OverlayTrigger>
                      {
                        dummy && dummy.source.map((item, i) => {
                          return (
                            <>
                              <Xarrow
                                key={"Y_" + index}
                                start={item}
                                end={"XYZ_" + index}
                                path={path}
                                color={highlight === item ? MainColor[item] : SubColor[item]}
                                headSize={4}
                                strokeWidth={2}
                                dashness={highlight === item ? true : false
                                } />
                            </>

                          )
                        })
                      }
                    </Fragment>
                  )
                })}
              </div>
            </div>
          </Scrollbars>
        </div>
      </div>
    </div>
  )
}

export default NetworkGraph2