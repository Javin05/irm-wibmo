import React, { useState, useEffect } from 'react'
import { KTSVG } from '../../../theme/helpers'
import {
  userRolesActions,
  userTypesActions
} from '../../../store/actions'
import { connect } from 'react-redux'
import color from '../../../utils/colors'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import _ from 'lodash'

function SearchList(props) {
  const {
    getUserroleDispatch,
    getUserTypeDispatch,
    getDataUserType,
    setSearchParams,
    activePageNumber,
    setActivePageNumber,
    limit
  } = props

  const [, setShow] = useState(false)
  const [selectedUserTypeOption, setSelectedUserTypeOption] = useState('')
  const [userTypeOption, setUserTypeOption] = useState()

  const [formData, setFormData] = useState({
    role: '',
    userTypeId: ''
  })

  useEffect(() => {
    const params = {
      tag: 'IRM'
    }
    getUserTypeDispatch(params)
  }, [])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? _.startCase(item[name]) : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getDataUserType, 'userType')
    setUserTypeOption(data)
  }, [getDataUserType])

  const handleChangeUserType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserTypeOption(selectedOption)
      setFormData((values) => ({
        ...values,
        userTypeId: selectedOption.value
      }))
    } else {
      setSelectedUserTypeOption()
      setFormData((values) => ({ ...values, userTypeId: '' }))
    }
  }

  const resetState = () => {
    setFormData({
      role: ''
    })
    setSelectedUserTypeOption()
  }

  const handleSearch = () => {
    const params = {
      limit: limit,
      page: 1
    }
    setActivePageNumber(1)
    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key] !== '') {
        params[key] = formData[key]
      }
    }
    const result = _.omit(params, ['limit', 'page']);
    setSearchParams(_.pickBy(result))
    setShow(false)
    getUserroleDispatch(params)
    resetState()
  }

  const handleReset = () => {
    resetState()
    setSearchParams()
    const params = {
      limit: limit,
      page: activePageNumber,
    }
    getUserroleDispatch(params)
  }

  const handleChange = (e) => {
    e.persist()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
    e.preventDefault()
  }

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  return (
    <>
      <div className='d-flex bd-highlight'>
        <button
          type='button'
          className='btn btn-sm btn-light-primary font-5vw pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>
      <div
        className='modal fade'
        id='searchModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-800px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Search</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => {
                  setShow(false)
                  resetState()
                }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header bg-lightBlue'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-6 mb-2'>
                        <label className='font-size-xs text-darkfont-weight-bold mb-3  form-label'>
                          User Type :
                        </label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name="userTypeId"
                            className="basic-single"
                            classNamePrefix="select"
                            handleChangeReactSelect={handleChangeUserType}
                            options={userTypeOption}
                            value={selectedUserTypeOption}
                          />
                        </div>
                      </div>
                      <div className='col-lg-6 mb-2'>
                        <label className='font-size-xs text-darkfont-weight-bold mb-3  form-label'>
                          User Role :
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='role'
                            type='text'
                            className='form-control'
                            placeholder='User Role'
                            onChange={(e) => handleChange(e)}
                            onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                            value={formData.role || ''}
                            autoComplete='off'
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mt-4'>
                      <div className='col-lg-6' />
                      <div className='col-lg-6'>
                        <div className='col-lg-12'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                            onClick={() => handleSearch()}
                            data-dismiss='modal'
                          >
                            Search
                          </button>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                            onClick={() => handleReset()}
                          >
                            Reset
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div >
    </>
  )
}

const mapStateToProps = (state) => {
  const { userroleStore, usertypeStore } = state;
  return {
    userRoleData:
      userroleStore && userroleStore.userRoleData ? userroleStore.userRoleData : {},
    getDataUserType:
    usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
  };
}

const mapDispatchToProps = dispatch => ({
  getUserroleDispatch: (params) =>
    dispatch(userRolesActions.getUserrole(params)),
  getUserTypeDispatch: (params) =>
    dispatch(userTypesActions.getUserType(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)
