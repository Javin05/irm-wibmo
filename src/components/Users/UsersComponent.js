import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { useLocation } from 'react-router-dom'
import { KTSVG } from '../../theme/helpers'
import { userRolesActions, deleteUserroleActions, userRoleDetailsActions } from '../../store/actions'
import { exportActions } from '../../store/actions/export'
import { connect } from 'react-redux'
import { getUserPermissions } from '../../utils/helper'
import { Can } from '../../theme/layout/components/can'
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  DELETE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from '../../utils/constants'
import { warningAlert, confirmationAlert, confirmAlert } from '../../utils/alerts'
import ReactPaginate from 'react-paginate'
import SearchList from './search/searchList'
import AddUsers from './userRole/add/AddUser'

const UsersComponent = (props) => {
  const {
    className,
    getUserroleDispatch,
    userRoleData,
    loadingGetUR,
    count,
    deleteUserroleMessage,
    deleteUserroleStatus,
    deleteUserroleDispatch,
    clearDeleteUserroleDispatch,
    getUserroleDetailsDispatch
  } = props

  const url = useLocation().pathname
  const getUsersPermissions = getUserPermissions(url)

  const [show, setShow] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [searchParams, setSearchParams] = useState()
  const [limit, setLimit] = useState(25)
  const [activePageNumber, setActivePageNumber] = useState(1)

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
      ...searchParams,
      tag:"IRM"
    }
    getUserroleDispatch(params)
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)

    const params = {
      limit: value,
      page: activePageNumber,
      ...searchParams,
      tag:"IRM"
    }
    getUserroleDispatch(params)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_USER_ROLE,
      'warning',
      'Yes',
      'No',
      () => {
        deleteUserroleDispatch(id)
      },
      () => { }
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      tag:"IRM"
    }
    setActivePageNumber(pageNumber)
    getUserroleDispatch(params)
  }

  const totalPages = count ? Math.ceil(parseInt(count) / limit) : 1

  const onConfirm = () => { }

  useEffect(() => {
    if (deleteUserroleStatus === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        deleteUserroleMessage,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearDeleteUserroleDispatch()
    } else if (deleteUserroleStatus === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        deleteUserroleMessage,
        'error',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearDeleteUserroleDispatch()
    }
  }, [deleteUserroleStatus])

  return (
    <>
      <AddUsers setShow={setShow} show={show} editMode={editMode} />
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {count && count && (
                  <span className='text-muted fw-bold d-flex fs-5 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-5'>
                      {count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto mt-4'>
              <div className='my-auto me-5'>
                <SearchList
                  setSearchParams={setSearchParams}
                  limit={limit}
                  activePageNumber={activePageNumber}
                  setActivePageNumber={setActivePageNumber}
                />
              </div>
                <Can
                    permissons={getUsersPermissions}
                    componentPermissions={CREATE_PERMISSION}>
                    <div className='my-auto me-3'>
                        <button
                            type='button'
                            className='btn btn-sm btn-light-primary font-5vw pull-right'
                            data-toggle='modal'
                            data-target='#addModal'
                            onClick={() => {
                                setShow(true)
                                setEditMode(false)
                            }}>
                            <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />Add
                        </button>
                    </div>
                </Can>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                    <Can
                        permissons={getUsersPermissions}
                        componentPermissions={UPDATE_DELETE_PERMISSION}>
                        <th>
                            <div className="d-flex">
                                <span>Action</span>
                            </div>
                        </th>
                    </Can>
                  <th>
                    <div className="d-flex min-w-125px">
                      <span>User Type</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>User Role</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loadingGetUR
                    ? (
                      userRoleData &&
                        userRoleData.length > 0
                        ? (
                          userRoleData.map((item, i) => {
                            return (
                              <tr
                                key={i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <Can
                                    permissons={getUsersPermissions}
                                    componentPermissions={UPDATE_DELETE_PERMISSION}>
                                    <td className='pb-0 pt-3 text-start my-auto'>
                                        <Can
                                            permissons={getUsersPermissions}
                                            componentPermissions={UPDATE_PERMISSION}>
                                            <button
                                                className='btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm'
                                                onClick={() => {
                                                    setShow(true)
                                                    setEditMode(true)
                                                    getUserroleDetailsDispatch(item._id)
                                                }}>
                                                <KTSVG
                                                    path="/media/icons/duotune/art/art005.svg"
                                                    className="svg-icon-3"/>
                                            </button>
                                        </Can>
                                        <Can
                                            permissons={getUsersPermissions}
                                            componentPermissions={DELETE_PERMISSION}>
                                            <button
                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm ms-2'
                                                onClick={() => {
                                                    onDeleteItem(item._id)
                                                }}>
                                                <KTSVG
                                                    path="/media/icons/duotune/general/gen027.svg"
                                                    className="svg-icon-3"
                                                    />
                                            </button>
                                        </Can>
                                    </td>
                                </Can>
                                <td className='ellipsis'>
                                  {item && item.userTypeId
                                    ? _.startCase(item.userTypeId.userType)
                                    : '--'}
                                </td>
                                <td className='ellipsis'>
                                  {item && item.role
                                    ? _.startCase(item.role)
                                    : '--'}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div className='spinner-border text-primary m-5' role='status' />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  forcePage={activePageNumber - 1}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { userroleStore, userroleDeleteStore } = state
  return {
    loadingGetUR:
      userroleStore && userroleStore.loadingGetUR ? userroleStore.loadingGetUR : false,
    userRoleData:
      userroleStore && userroleStore.userRoleData ? userroleStore.userRoleData : {},
    count:
      userroleStore && userroleStore.count ? userroleStore.count : 0,
    deleteUserroleStatus:
      userroleDeleteStore && userroleDeleteStore.deleteUserroleStatus ? userroleDeleteStore.deleteUserroleStatus : '',
    deleteUserroleMessage:
      userroleDeleteStore && userroleDeleteStore.deleteUserroleMessage ? userroleDeleteStore.deleteUserroleMessage : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  clearPostsDispatch: () => dispatch(exportActions.clearPosts()),
  getUserroleDispatch: (params) => dispatch(userRolesActions.getUserrole(params)),
  deleteUserroleDispatch: (params) => dispatch(deleteUserroleActions.deleteUserrole(params)),
  clearDeleteUserroleDispatch: (params) => dispatch(deleteUserroleActions.clearDeleteUserrole(params)),

  getUserroleDetailsDispatch: (params) => dispatch(userRoleDetailsActions.getUserroleDetails(params)),
  clearUserroleDetailsDispatch: () => dispatch(userRoleDetailsActions.clearUserroleDetails())
})

export default connect(mapStateToProps, mapDispatchToProps)(UsersComponent)
