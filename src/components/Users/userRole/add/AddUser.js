import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { useLocation } from 'react-router-dom'
import { KTSVG } from '../../../../theme/helpers'
import { STATUS_RESPONSE, REGEX } from '../../../../utils/constants'
import { Modal } from '../../../../theme/layout/components/modal'
import ReactSelect from '../../../../theme/layout/components/ReactSelect'
import {
  userTypesActions,
  addUserroleActions,
  userRoleDetailsActions,
  editUserrolesActions,
  userRolesActions
} from '../../../../store/actions'
import {
  warningAlert,
  confirmAlert
} from '../../../../utils/alerts'
import { setRoleData } from '../../formData'
import { userRoleValidation } from '../../validation'
import color from '../../../../utils/colors'
import _ from 'lodash'

const AddUsers = (props) => {
  const {
    getUserTypeDispatch,
    getDataUserType,
    addUserroleDispatch,
    clearaddUserroleDispatch,
    messageAUR,
    statusAUR,
    loadingAUR,
    clearUserroleDetailsDispatch,
    userroleDetails,
    statusURD,
    editUserrolesDispatch,
    cleareditUserrolesDispatch,
    statusEUR,
    messageEUR,
    loadingEUR,
    getUserroleDispatch,
    setShow,
    show,
    loadingURD,
    editMode
  } = props
  const searchName = useLocation().pathname
  const userParam = searchName && searchName.split('update-user-role/')
  const id = userParam && userParam[1]
  const [errors, setErrors] = useState({})
  const [selectedUserTypeOption, setSelectedUserTypeOption] = useState('')
  const [userTypeOption, setUserTypeOption] = useState()
  const [formData, setFormData] = useState({
    role: '',
    userTypeId: '',
    tag:"IRM"
  })

  const resetState = () => {
    setFormData({
      role: ''
    })
    setSelectedUserTypeOption()
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? _.startCase(item[name]) : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getDataUserType, 'userType')
    setUserTypeOption(data)
  }, [getDataUserType])

  const handleChangeUserType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserTypeOption(selectedOption)
      setFormData((values) => ({
        ...values,
        userTypeId: selectedOption.value
      }))
      setErrors((values) => ({ ...values, userTypeId: '' }))
    } else {
      setSelectedUserTypeOption()
      setFormData((values) => ({ ...values, userTypeId: '' }))
    }
  }

  const handleChange = (e) => {
    e.persist()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = userRoleValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        editUserrolesDispatch(id, formData)
      } else {
        addUserroleDispatch(formData)
      }
    }
  }

  useEffect(() => {
    const params = {
        tag:"IRM"
    }
    getUserTypeDispatch(params)
  }, [])


  useEffect(() => {
    if (statusURD === STATUS_RESPONSE.SUCCESS_MSG) {
      if (userroleDetails && userroleDetails._id) {
        const data = setRoleData(userroleDetails)
        setFormData(data)
      }
      if (userroleDetails.userTypeId) {
        const data = getDefaultOptions(getDataUserType, 'userType')
        const selOption = _.filter(data, function (x) {
          if (_.includes(userroleDetails.userTypeId, x.value)) {
            return x
          }
        })
        setSelectedUserTypeOption(selOption)
      }
      clearUserroleDetailsDispatch()
    }
  }, [statusURD])

  const onConfirm = () => {
    const params = {
      tag:"IRM"
    }
    getUserroleDispatch(params)
    setShow(false);
    resetState()
  }

  useEffect(() => {
    if (statusEUR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageEUR,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      cleareditUserrolesDispatch()
      clearUserroleDetailsDispatch()
    } else if (statusEUR === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageEUR,
        '',
        'Ok',
        // () => { onConfirm() },
        // () => { onConfirm() }
      )
      cleareditUserrolesDispatch()
    }
  }, [statusEUR])

  useEffect(() => {
    if (statusAUR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAUR,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearaddUserroleDispatch()
      const params = {
        tag:"IRM"
      }
      getUserroleDispatch(params)
      setSelectedUserTypeOption()
    } else if (statusAUR === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAUR,
        '',
        'Ok'
      )
      clearaddUserroleDispatch()
    }
  }, [statusAUR])

  useEffect(() => {
    return () => {
      setSelectedUserTypeOption()
      clearUserroleDetailsDispatch()
    }
  }, [])

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  return (
    <>
      <Modal
        showModal={show}
        modalWidth={650}
      >
        <div className="" id="userModal">
          <div>
            <div className='modal-dialog modal-dialog-centered mw-800px'>
              <div className='modal-content'>
                <div className='modal-header'>
                  <h2 className='me-8 ms-4'>{editMode ? 'Update' : 'Add'} a Role</h2>
                  <button
                    type='button'
                    className='btn btn-lg btn-icon btn-active-light-primary close m-4'
                    data-dismiss='modal'
                    onClick={() => {
                      setShow(false)
                      resetState()
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                    {/* eslint-disable */}
                  </button>
                </div>
                {loadingURD ? (
                  <div className="d-flex justify-content-center py-5">
                    <div className="spinner-border text-primary m-5" role="status" />
                  </div>
                ) : (
                  <>
                    <div className='container-fixed'>
                      <div className='card-header bg-lightBlue'>
                        <div className='card-body'>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <label className="fs-5 fw-bolder form-label mb-2 ms-4 mt-4">
                              <span className="required">User Type:</span>
                            </label>
                            <div className='row'>
                              <div className='col-lg-2' />
                              <div className='col-lg-7'>
                                <ReactSelect
                                  styles={customStyles}
                                  isMulti={false}
                                  name='userTypeId'
                                  className='basic-single'
                                  classNamePrefix='select'
                                  handleChangeReactSelect={handleChangeUserType}
                                  options={userTypeOption}
                                  value={selectedUserTypeOption}
                                />
                              </div>
                              {errors && errors.userTypeId && (
                                <div className='rr mt-1'>
                                  <style>{'.rr{color:red}'}</style>
                                  {errors.userTypeId}
                                </div>
                              )}
                            </div>
                          </div>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <label className="fs-5 fw-bolder form-label ms-4">
                              <span className="required">User Role :</span>
                            </label>
                            <div className='row'>
                              <div className='col-lg-2' />
                              <div className='col-lg-7'>
                                <input
                                  name='role'
                                  type='text'
                                  className='form-control'
                                  placeholder='User Role'
                                  onChange={(e) => handleChange(e)}
                                  value={formData.role || ''}
                                  autoComplete='off'
                                  onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                                  onKeyPress={(e) => {
                                    if (!REGEX.ALPHA_NUMERIC.test(e.key)) {
                                      e.preventDefault()
                                    }
                                  }} />
                              </div>
                              {errors && errors.role && (
                                <div className='rr mt-1'>
                                  <style>{'.rr{color:red}'}</style>
                                  {errors.role}
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='form-group row mt-4'>
                            <div className='col-lg-5' />
                            <div className='col-lg-7'>
                              <div className='col-lg-10'>
                                <button
                                  className='btn btn-blue mt-7 fa-pull-right mb-4'
                                  onClick={(event) => {
                                    handleSubmit()
                                  }}
                                >
                                  {loadingAUR || loadingEUR
                                    ? (
                                      <span
                                        className='spinner-border spinner-border-sm mx-3'
                                        role='status'
                                        aria-hidden='true'
                                      />
                                    )
                                    : (
                                      'Submit'
                                    )}
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div >
        </div>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    userroleStore,
    usertypeStore,
    addUserroleStore,
    editUserroleStore
  } = state
  return {
    getDataUserType:
    usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
    loadingGetRole:
    usertypeStore && usertypeStore.loadingGetRole
        ? usertypeStore.loadingGetRole
        : false,
    messageAUR:
      addUserroleStore && addUserroleStore.messageAUR
        ? addUserroleStore.messageAUR
        : 0,
    statusAUR:
      addUserroleStore && addUserroleStore.statusAUR
        ? addUserroleStore.statusAUR
        : {},
    loadingAUR:
      addUserroleStore && addUserroleStore.loadingAUR
        ? addUserroleStore.loadingAUR
        : false,
    statusEUR:
      editUserroleStore && editUserroleStore.statusEUR
        ? editUserroleStore.statusEUR
        : '',
    messageEUR:
      editUserroleStore && editUserroleStore.messageEUR
        ? editUserroleStore.messageEUR
        : '',
    loadingEUR:
      editUserroleStore && editUserroleStore.loadingEUR
        ? editUserroleStore.loadingEUR
        : false,
    userroleDetails:
      userroleStore && userroleStore.userroleDetails
        ? userroleStore.userroleDetails
        : {},
    statusURD:
      userroleStore && userroleStore.statusURD ? userroleStore.statusURD : '',
    messagesURD:
      userroleStore && userroleStore.messagesURD
        ? userroleStore.messagesURD
        : '',
    loadingURD:
      userroleStore && userroleStore.loadingURD
        ? userroleStore.loadingURD
        : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserTypeDispatch: (params) =>
    dispatch(userTypesActions.getUserType(params)),
  addUserroleDispatch: (data) => dispatch(addUserroleActions.addUserrole(data)),
  clearaddUserroleDispatch: () =>
    dispatch(addUserroleActions.clearaddUserrole()),
  editUserrolesDispatch: (id, data) =>
    dispatch(editUserrolesActions.editUserroles(id, data)),
  cleareditUserrolesDispatch: () =>
    dispatch(editUserrolesActions.cleareditUserroles()),
  getUserroleDetailsDispatch: (data) =>
    dispatch(userRoleDetailsActions.getUserroleDetails(data)),
  clearUserroleDetailsDispatch: () =>
    dispatch(userRoleDetailsActions.clearUserroleDetails()),
  getUserroleDispatch: (params) => dispatch(userRolesActions.getUserrole(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddUsers)
