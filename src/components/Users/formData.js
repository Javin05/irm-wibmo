import _ from 'lodash'

export const setRoleData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      role: _.startCase(data.role),
      userType: data.userTypeId.userType,
      tag:"IRM"
    }
  }
}
