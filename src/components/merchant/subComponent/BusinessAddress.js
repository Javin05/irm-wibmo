import React, { useState, useEffect } from 'react'
import { withGoogleMap, GoogleMap, withScriptjs, InfoWindow, Marker, Polyline } from "react-google-maps";

function BusinessAddressMapView(props) {

    const {
        splitData
    } = props

    const BusinessAddressMap = withScriptjs(withGoogleMap(props =>
        <GoogleMap
            defaultZoom={8}
            defaultCenter={{ lat: -34.397, lng: 150.644 }}
            style={{ height: '100vh', width: '100%' }}>

            <Marker
                position={{
                    lat: parseFloat(splitData && splitData.businessAddressLocation && splitData.businessAddressLocation.lat),
                    lng: parseFloat(splitData && splitData.businessAddressLocation && splitData.businessAddressLocation.long)
                }}
                defaultCenter={{
                    lat: parseFloat(splitData && splitData.businessAddressLocation && splitData.businessAddressLocation.lat),
                    lng: parseFloat(splitData && splitData.businessAddressLocation && splitData.businessAddressLocation.long)
                }}
                draggable={false}
                icon={`/media/icons/duotune/maps/map014.png`}>
                <InfoWindow>
                    <div>
                        {splitData && splitData.businessAddress}
                    </div>
                </InfoWindow>
            </Marker>
        </GoogleMap>
    ))

    return (
        <div>
            <BusinessAddressMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws&libraries=places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
          />
    </div>
    )
}
export default BusinessAddressMapView