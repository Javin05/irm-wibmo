import React, { useState, useEffect } from "react";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  InfoWindow,
  Marker,
  Polyline,
} from "react-google-maps";

function AddressMapView(props) {
  const { splitData } = props;

  const AddressMap = withScriptjs(
    withGoogleMap((props) => (
      <GoogleMap
        defaultZoom={8}
        defaultCenter={{ lat: -34.397, lng: 150.644 }}
        style={{ height: "100vh", width: "100%" }}
      >
        <Marker
          position={{
            lat: parseFloat(
              splitData &&
                splitData.phoneLocation &&
                splitData.phoneLocation.lat
            ),
            lng: parseFloat(
              splitData &&
                splitData.phoneLocation &&
                splitData.phoneLocation.long
            ),
          }}
          defaultCenter={{
            lat: parseFloat(
              splitData &&
                splitData.phoneLocation &&
                splitData.phoneLocation.lat
            ),
            lng: parseFloat(
              splitData &&
                splitData.phoneLocation &&
                splitData.phoneLocation.long
            ),
          }}
          draggable={false}
          icon={`/media/icons/duotune/maps/map011.png`}
        >
          <InfoWindow>
            <div>{splitData && splitData.address}</div>
          </InfoWindow>
        </Marker>
      </GoogleMap>
    ))
  );

  return (
    <div>
      <AddressMap
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws&libraries=places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
  );
}
export default AddressMapView;
