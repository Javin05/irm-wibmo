import React, { useState, useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { useLocation, Link, useParams } from 'react-router-dom'
import { DATE, STATUS_RESPONSE } from '../../utils/constants'
import {
  dashboardDetailsActions,
  riskSummaryActions,
  merchantIdDetailsActions,
  GetCategoryActions
} from '../../store/actions'
import { KTSVG } from '../../theme/helpers'
import { riskManagementActions } from '../../store/actions'
import { ApproveActions } from '../../store/actions'
import CrossCheck from './subComponent/crossCheck'
import { USER_ERROR, REGEX, RESPONSE_STATUS, SESSION, SWEET_ALERT_MSG, RISKMANAGEMENT_EMAIL_DASHBOARD, VALID_EMAIL, WEBSIT_VALIDATE, RISKMANAGEMENT_WEBSITE_DASHBOARD } from '../../utils/constants'
import clsx from 'clsx'
import "react-circular-progressbar/dist/styles.css";
import ReactSpeedometer from "react-d3-speedometer"
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import MapGoogle from "../maps/MapGoogle";
import StreetMap from "../maps/StreetMap";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'

function Merchant(props) {
  const {
    getPrevAlertDetailsDispatch,
    dashboardDetails,
    id,
    setOpenPhone,
    setOpenEmail,
    setOpenAddress,
    setOpenIpAddress,
    riskmgmtlistdetails,
    getRiskManagementlistDispatch,
    ApprovePost,
    getRiskSummaryDispatch,
    approveResponceData,
    loading,
    openMap,
    openBusinessMap,
    setopenBusinessMap,
    setopenMap,
    getIdMerchantDispatch,
    clearApprove,
    setOpenWebsite,
    matrixDetail,
    merchantIddetails,
    merchantSummary,
    isLoaded,
    GetCategroyDispatch,
  } = props

  const [active, setActive] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('risk-summary/update/')
  const currentId = url && url[1]
  const [rejectValue, setRejectValue] = useState()

  const [errors, setErrors] = useState({
    reason: '',
  })
  const [formData, setFormData] = useState({
    message: ''
  })
  const [holdFormData, setHoldFormData] = useState({
    riskStatus: 'HOLD',
    reason: ''
  })
  const [rejectFormData, setRejectFormData] = useState({
    riskStatus: 'REJECTED',
    reason: '',
    rejectType: '',
    rejectMoreValue: ''

  })

  const [approveFormData, setApproveFormData] = useState({
    riskStatus: 'APPROVED',
    reason: '',
  })

  useEffect(() => {
    // getIdMerchantDispatch(currentId)
    GetCategroyDispatch(currentId)
  }, [currentId])

  useEffect(() => {
    if (openMap) {
      const modalBtn = document.getElementById('modal-btn')
      modalBtn.click()
    }
  }, [openMap])

  useEffect(() => {
    if (openBusinessMap) {
      const modalBtn = document.getElementById('businesmodal-btn')
      modalBtn.click()
    }
  }, [openBusinessMap])

  const approveSubmit = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      ApprovePost(id, approveFormData)
    }
  }

  const onConfirmHold = () => {
    ApprovePost(id, holdFormData)
  }

  const holdSubmit = () => {
    const errors = {}
    if (_.isEmpty(holdFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.HOLD,
        'warning',
        'Yes',
        'No',
        () => { onConfirmHold() },
        () => { }
      )
    }
  }

  const onConfirmReject = () => {
    ApprovePost(id, rejectFormData)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        'warning',
        'Yes',
        'No',
        () => { onConfirmReject() },
        () => { }
      )
    }
  }

  const handleChange = (e) => {
    setHoldFormData({ ...holdFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
    setRejectValue(e.target.value)
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  useEffect(() => {
    getRiskManagementlistDispatch()
  }, [])

  useEffect(() => {
    if (id) {
      getPrevAlertDetailsDispatch(id)
      getRiskSummaryDispatch(id)
    }
  }, [id])

  useEffect(() => {
    if (approveResponceData && approveResponceData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        approveResponceData && approveResponceData.message,
        'success',
      )
      getRiskManagementlistDispatch()
      getIdMerchantDispatch(id)
      clearApprove()
    }
  }, [approveResponceData])

  useEffect(() => {
    if (approveResponceData && approveResponceData.message === 'Record Status Approved Successfully') {
      const approvemodalBtn = document.getElementById('approve-model')
      approvemodalBtn.click()
    }
    if (approveResponceData && approveResponceData.message === 'Record Status Rejected Successfully') {
      const rejectmodalBtn = document.getElementById('reject-model')
      rejectmodalBtn.click()
    }
    if (approveResponceData && approveResponceData.message === 'Record Status Changed as Hold') {
      const modalBtn = document.getElementById('hold-model')
      modalBtn.click()
    }
  }, [approveResponceData])

  const IDPhone = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.phone && dashboardDetails.data.dashboardData.phone.data
  const IDAddress = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.address && dashboardDetails.data.dashboardData.address.data
  const IDEmail = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.email && dashboardDetails.data.dashboardData.email.data
  const IDIP_Address = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.ipAddress && dashboardDetails.data.dashboardData.ipAddress.data
  const IDNegative = dashboardDetails && dashboardDetails.data && dashboardDetails.data.negative
  const IDPositive = dashboardDetails && dashboardDetails.data && dashboardDetails.data.positive
  const IDPhoneNumber = dashboardDetails && dashboardDetails && dashboardDetails.data && dashboardDetails.data.phone && dashboardDetails.data.phone[0].value
  const Risk = dashboardDetails && dashboardDetails && dashboardDetails.risk_overview && dashboardDetails.risk_overview.risk_score
  const Email = dashboardDetails && dashboardDetails && dashboardDetails.risk_data && dashboardDetails.risk_data.email && dashboardDetails.risk_data.email[0].value
  const Address = dashboardDetails && dashboardDetails && dashboardDetails.risk_data && dashboardDetails.risk_data.address && dashboardDetails.risk_data.address[0].value
  const IPaddress = dashboardDetails && dashboardDetails && dashboardDetails.risk_data && dashboardDetails.risk_data.ip_address && dashboardDetails.risk_data.ip_address[0].value
  const BusinessPhone = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessPhone && dashboardDetails.data.dashboardData.businessPhone.data
  const BusinessEmail = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessEmail && dashboardDetails.data.dashboardData.businessEmail.data
  const BusinessAddress = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessAddress && dashboardDetails.data.dashboardData.businessAddress.data
  const Device = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.deviceId && dashboardDetails.data.dashboardData.deviceId.data
  const speedometetervalue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.totalScore ? dashboardDetails.data.totalScore : '0'
  const website = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.website && dashboardDetails.data.dashboardData.website.data


  
  const IDPhoneValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.phone && dashboardDetails.data.dashboardData.phone.value
  const IDEmailValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.email && dashboardDetails.data.dashboardData.email.value
  const IDAddressValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.address && dashboardDetails.data.dashboardData.address.value
  const IDIP_AddressValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.ipAddress && dashboardDetails.data.dashboardData.ipAddress.value
  const BusinessPhoneValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessPhone && dashboardDetails.data.dashboardData.businessPhone.value
  const BusinessEmailValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessEmail && dashboardDetails.data.dashboardData.businessEmail.value
  const BusinessAddressValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessAddress && dashboardDetails.data.dashboardData.businessAddress.value
  const DeviceValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.deviceId && dashboardDetails.data.dashboardData.deviceId.value
  const websiteValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.website && dashboardDetails.data.dashboardData.website.value ? dashboardDetails.data.dashboardData.website.value : '__'

  const viewData = matrixDetail && matrixDetail && matrixDetail.data ? matrixDetail.data : []
  const getData = viewData.filter(o => (o ? o : null))
  const splitData = getData && getData[0] ? getData[0] : '--'
  const value = 100

  return (
    <>
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="hold-model"
        data-target='#holdModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#rejectModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="approve-model"
        data-target='#approveModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="modal-btn"
        data-target='#InmapModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="businesmodal-btn"
        data-target='#mapModal'
        onClick={() => { }}
      />
      <div
        className='modal fade'
        id='InmapModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Individual Address</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => { setopenMap(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <div className='row mt-8'>
                <div className='col-lg-6'>
                  <div className='card w-504px h-450px'>
                    {isLoaded ? <MapGoogle mapData={splitData} mapMarkers={null} /> : null}
                    {/*<AddressMapView splitData={splitData} />*/}
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div>
                    {isLoaded ? <StreetMap mapData={splitData} /> : null}
                    {/*<StreetView splitData={splitData} />*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='mapModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Business Address</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => { setopenBusinessMap(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <div className='row mt-8'>
                <div className='col-lg-6'>
                  <div className='card w-504px h-450px'>
                    {isLoaded ? <MapGoogle mapData={splitData} mapMarkers={null} /> : null}
                    {/*<BusinessAddressMapView splitData={splitData}/>*/}
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div
                    style={{
                      width: "714px",
                      height: "450px",
                      backgroundColor: "#eeeeee"
                    }}
                  >
                    {isLoaded ? <StreetMap mapData={splitData} /> : null}
                    {/*<StreetView splitData={splitData} />*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='holdModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Hold
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 '>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={holdFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => holdSubmit()}
                              disabled={loading}
                            >
                              <span className='indicator-label'>Submit</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='approveModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Are You Sure Want to Approve This Users Risk Analytics Report ?</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Approved
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 '>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': approveFormData.reason && errors.reason },
                              {
                                'is-valid': approveFormData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => approveChange(e)}
                            autoComplete='off'
                            value={approveFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => approveSubmit()}
                              disabled={loading}
                            >
                              <span className='indicator-label'>Submit</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='rejectModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              // onClick={() => { setShow(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Reject :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <textarea
                            name='reason'
                            type='text'
                            // className='form-control'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => rejectChange(e)}
                            autoComplete='off'
                            value={rejectFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Type :
                        </label>
                        <div className='col-lg-8 col-md-8 col-sm-8'>
                          <select
                            className='form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => rejectChange(e)}
                            value={rejectFormData.rejectType || ''}
                            name='rejectType'
                          >
                            <option value='Suspect Fraud'>Suspect Fraud</option>
                            <option value=' Fraud'>Fraud</option>
                            <option value='Spam'>Spam</option>
                            <option value='Fake'>Fake</option>
                            <option value='Funneling'>Funneling</option>
                            <option value='More'>More</option>
                          </select>
                          {
                            rejectFormData.rejectType === 'More' ? (
                              <>
                                <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label mt-2'>
                                  please enter your message :
                                </label>
                                <textarea
                                  name='rejectMoreValue'
                                  type='text'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.rejectMoreValue && errors.rejectMoreValue },
                                    {
                                      'is-valid': formData.rejectMoreValue && !errors.rejectMoreValue
                                    }
                                  )}
                                  placeholder='Message'
                                  onChange={(e) => rejectChange(e)}
                                  autoComplete='off'
                                  value={rejectFormData.rejectMoreValue || ''}
                                />
                              </>
                            ) : rejectValue === 'Spam' || rejectValue === 'Suspect Fraud' || rejectValue === 'Fraud' || rejectValue === 'Fake' || rejectValue === 'Funneling' ?
                              null
                              : null
                          }
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => rejectSubmit()}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className=' col-md-12 card mb-5 mb-xl-10'>
        <div className='card-body pt-9 pb-0'>
          <div className='d-flex flex-wrap flex-sm-nowrap mb-3'>
            <div className='flex-grow-1'>
              <div className='d-flex justify-content-between align-items-start flex-wrap mb-2'>
                <div className='d-flex flex-column'>
                  <div className='d-flex align-items-center mb-2'>
                    {/* <h1 className='d-flex align-items-center mb-5 fw-boldest my-1 fs-2 mt-8 ml-2'>Risk Dashboard - Case ID IRM{getRiskId} </h1> */}
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-12'>
                    <div className="card-toolbar d-flex mt-5">
                      {/* {
                        merchantSummary && merchantSummary.riskStatus === 'PENDING' ?
                          (
                            <ul className="nav">
                              <li className="nav-item">
                                <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                                  data-toggle='modal'
                                  data-target='#approveModal'
                                  onClick={() => setActive(true)}
                                >
                                  Approve
                                </a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                                  data-toggle='modal'
                                  data-target='#rejectModal'
                                  onClick={() => setActive(true)}
                                >
                                  Reject
                                </a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link btn btn-sm fw-bolder px-4 me-2 btn-warning"
                                  data-toggle='modal'
                                  data-target='#holdModal'
                                  onClick={() => setActive(true)}
                                >
                                  Hold
                                </a>
                              </li>
                            </ul>
                          )
                          : (
                            merchantSummary && merchantSummary.riskStatus === 'HOLD' ? (
                              <ul className="nav">
                                <li className="nav-item">
                                  <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                                    data-toggle='modal'
                                    data-target='#approveModal'
                                    onClick={() => { setActive(true) }}
                                  >
                                    Approve
                                  </a>
                                </li>
                                <li className="nav-item">
                                  <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                                    data-toggle='modal'
                                    data-target='#rejectModal'
                                    onClick={() => { setActive(true) }}
                                  >
                                    Reject
                                  </a>
                                </li>
                              </ul>
                            ) : ( */}
                      {/* merchantSummary && merchantSummary.riskStatus === "MANUAL REVIEW" ? ( */}
                      <>
                        <ul className="nav">
                          <li className="nav-item">
                            <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                              data-toggle='modal'
                              data-target='#approveModal'
                              onClick={() => { setActive(true) }}
                            >
                              Approve
                            </a>
                          </li>
                          <li className="nav-item">
                            <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                              data-toggle='modal'
                              data-target='#rejectModal'
                              onClick={() => { setActive(true) }}
                            >
                              Reject
                            </a>
                          </li>
                        </ul>
                      </>
                      {/* ):null
                            )                              
                          )
                      } */}
                    </div>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='col-lg-3 col-md-3 col-sm-3 ps-8'>
                  <h2 className='mb-2 d-flex justify-content-start text-limegreen mb-5'>Positive Factors</h2>
                  {
                    !IDPositive
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDPositive && IDPositive.map((item, i) => {
                        return (
                          <span key={"S_" + i} className='d-flex justify-content-start'>
                            <i className='bi bi-check-circle-fill text-limegreen min-w-30px fsu ' />
                            <h6 className='fw-bold fs-6 '>{item.message}</h6>
                          </span>
                        )
                      }
                      )
                  }
                </div>
                <div className='col-lg-5 col-md-5 col-sm-5'>
                  <div className='d-flex justify-content-center mb-4'
                  >
                    <ReactSpeedometer
                      maxValue={100}
                      value={parseInt(speedometetervalue)}
                      customSegmentStops={[0, 35, 75, 100]}
                      segmentColors={["limegreen", "gold", "tomato"]}
                      needleColor="red"
                      startColor="green"
                      segments={10}
                      endColor="blue"
                      className='pichart'
                      currentValueText="Over All Score: #{value}"
                      currentValuePlaceholderStyle={"#{value}"}
                    />
                  </div>
                </div>
                <div className='col-lg-4 col-md-4 col-sm-4'>
                  <h2 className='mb-2 d-flex justify-content-start symbol-label text-darkorange mb-5'>Negative Factors</h2>
                  {
                    !IDNegative
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDNegative && IDNegative.map((item, i) => {
                        return (
                          <span key={"E" + i} className='d-flex justify-content-start'>
                            <i className='bi bi-exclamation-triangle-fill text-darkorange min-w-30px fsu' />
                            <h5 className='text-darkorange fw-bold fs-6 '>{item.message}</h5>
                          </span>
                        )
                      }
                      )
                  }
                </div>
                <div className='row mb-0'>
                  <div className='col-lg-4 col-md-4 col-sm-4' />
                  <div className='col-lg-4 col-md-4 col-sm-4'>
                    {/* <h1 className='d-flex justify-content-center  fw-boldest my-1 fs-2'>Risk Score </h1> */}
                    <h1 className='d-flex justify-content-center  fw-boldest my-1 fs-2'>{Risk} </h1>
                  </div>
                  <div className='col-lg-4 col-md-4 col-sm-4' />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-lg-6 col-md-6 col-sm-6'>
          <div className='mb-4'>
            <span className='text-muted mt-1 fw-bold fs-4 justify-content-start'>INDIVIDUAL</span>
            <h1 className='text-dark fw-bolder justify-content-start'>{merchantSummary ? merchantSummary.firstName + ' ' + merchantSummary.lastName : "--"}</h1>
          </div>
          <div className='row'>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 mb-2  bg-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-card'>Phone</span>
                    <span className='text-muted mt-1 fw-bold fs-7'>{merchantSummary ? merchantSummary.phone : "--"}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenPhone(true) }}>
                      View Phone Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDPhone
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IDPhone) ?
                        IDPhone && IDPhone.map((item, i) => {
                          return (
                            <Fragment key={"FIX_1" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                      </div>
                                      <span className='svg-icon svg-icon-1 svg-icon-success'
                                        title={item.message}
                                      >
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1'
                                                title={item.message}
                                              ><i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu'
                                                title={item.message}
                                                /></span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1'>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' title={item.message} />
                                                      </span>
                                                    </div>

                                                  )
                                                  : null
                                              }
                                            </>

                                          )
                                      }
                                    </>
                                  )
                              }
                            </Fragment>
                          )
                        }
                        )
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
              {/* <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-card'>
                  <h3 className='card-title align-items-start flex-column '>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Device
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7 ellipsis'>{DeviceValue}</span>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !Device
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : 
                      !_.isEmpty(Device) ?
                      Device && Device.map((item, i) => {
                        return (
                          <Fragment key={"FIX_2" + i}>
                            {
                              item && item.status === 'positive'
                                ? (
                                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                    <div className='flex-grow-1 me-2'>
                                      <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                      <span className='text-muted fw-bold d-block'>{item.value}</span>
                                    </div>
                                    <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message} >
                                      <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                        <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                        <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                      </svg>
                                    </span>
                                  </div>
                                )
                                : (
                                  <>
                                    {
                                      item && item.status === 'warning'
                                        ? (
                                          <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                            <div className='flex-grow-1 me-2'>
                                              <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                              <span className='text-muted fw-bold d-block'>{item.value}</span>
                                            </div>
                                            <span className='fw-bolder text-warning py-1' title={item.message}>
                                              <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                            </span>
                                          </div>
                                        )
                                        : (
                                          <>
                                            {
                                              item && item.status === 'negative'
                                                ? (
                                                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                    <div className='flex-grow-1 me-2'>
                                                      <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                      <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                    </div>
                                                    <span className='fw-bolder text-danger py-1' title={item.message}>
                                                      <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                    </span>
                                                  </div>

                                                )
                                                : null
                                            }
                                          </>

                                        )
                                    }
                                  </>
                                )

                            }
                          </Fragment>
                        )
                      })
                      :<h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div> */}
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0  bg-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-card'>
                      {/* <span className=' svg-icon-email svg me-2'>
                        <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-envelope-fill' viewBox='0 0 16 16'>
                          <path d='M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z' />
                        </svg>
                      </span> */}
                      Email
                    </span>
                    <span className='text-muted fw-bold fs-7 ellipsis'>{merchantSummary ? merchantSummary.personalEmail : "--"}
                    </span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenEmail(true) }}
                    >
                      View Email Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDEmail
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IDEmail) ?
                        IDEmail && IDEmail.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                          {
                                            !_.isEmpty(item.info) ? (
                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                className='tooltip'
                                              >
                                                {item.info}
                                              </Tooltip>}
                                                placement={"right"}
                                              >
                                                <span>
                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                </span>
                                              </OverlayTrigger>
                                            ) : (
                                              null
                                            )
                                          }
                                        </div>
                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                      </div>

                                      <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                  {
                                                    !_.isEmpty(item.info) ? (
                                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                        className='tooltip'
                                                      >
                                                        {item.info}
                                                      </Tooltip>}
                                                        placement={"right"}
                                                      >
                                                        <span>
                                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                        </span>
                                                      </OverlayTrigger>
                                                    ) : (
                                                      null
                                                    )
                                                  }
                                                </div>
                                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1'>
                                                <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                              </span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                          {
                                                            !_.isEmpty(item.info) ? (
                                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                                className='tooltip'
                                                              >
                                                                {item.info}
                                                              </Tooltip>}
                                                                placement={"right"}
                                                              >
                                                                <span>
                                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                                </span>
                                                              </OverlayTrigger>
                                                            ) : (
                                                              null
                                                            )
                                                          }

                                                        </div>
                                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1' title={item.message}>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                      </span>
                                                    </div>
                                                  )
                                                  : null
                                              }
                                            </>
                                          )
                                      }
                                    </>
                                  )
                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>

              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-card'>
                  <h3 className='card-title align-items-start flex-column '>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Website
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7 ellipsis'>{websiteValue}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenWebsite(true) }}
                    >View Website Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !website
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(website) ?
                        website && website.map((item, i) => {
                          return (
                            <Fragment key={"FIX_4" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                          {
                                            _.includes(WEBSIT_VALIDATE, item.title) ? (
                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                className='tooltip'
                                              >
                                                {item.info}
                                              </Tooltip>}
                                                placement={"right"}
                                              >
                                                <span>
                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                </span>
                                              </OverlayTrigger>
                                            ) : (
                                              null
                                            )
                                          }
                                        </div>
                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                      </div>
                                      <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message} >
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                  {
                                                    _.includes(WEBSIT_VALIDATE, item.title) ? (
                                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                        className='tooltip'
                                                      >
                                                        {item.info}
                                                      </Tooltip>}
                                                        placement={"right"}
                                                      >
                                                        <span>
                                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                        </span>
                                                      </OverlayTrigger>
                                                    ) : (
                                                      null
                                                    )
                                                  }
                                                </div>
                                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1' title={item.message}>
                                                <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                              </span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                          {
                                                            _.includes(WEBSIT_VALIDATE, item.title) ? (
                                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                                className='tooltip'
                                                              >
                                                                {item.info}
                                                              </Tooltip>}
                                                                placement={"right"}
                                                              >
                                                                <span>
                                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                                </span>
                                                              </OverlayTrigger>
                                                            ) : (
                                                              null
                                                            )
                                                          }</div>
                                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1' title={item.message}>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                      </span>
                                                    </div>

                                                  )
                                                  : null
                                              }
                                            </>

                                          )
                                      }
                                    </>
                                  )

                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 mb-xl-5  bg-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Address
                    </span>
                    <a className='mt-1 fw-bold fs-7 cp text link-muted'
                      data-toggle='modal'
                      data-target='#InmapModal'
                    // onClick={() => {setActive(true)}}
                    >
                      {merchantSummary ? merchantSummary.address : "--"}
                    </a>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenAddress(true) }}
                    >View Address Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDAddress
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IDAddress) ?
                        IDAddress && IDAddress.map((item, i) => {
                          return (
                            <Fragment key={"FIX_5" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                        <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                      </div>
                                      <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1' title={item.message}>
                                                <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' /></span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                        <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1' title={item.message}>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                      </span>
                                                    </div>

                                                  )
                                                  : null
                                              }
                                            </>
                                          )
                                      }
                                    </>
                                  )
                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
              {/* <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-card'>
                  <h3 className='card-title align-items-start flex-column '>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      IP Address
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7 ellipsis'>{IDIP_AddressValue}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenIpAddress(true) }}
                    >View Ip Address Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDIP_Address
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : 
                      !_.isEmpty(IDIP_Address) ?
                      IDIP_Address && IDIP_Address.map((item, i) => {
                        return (
                          <Fragment key={"FIX_6" + i}>
                            {
                              item && item.status === 'positive'
                                ? (
                                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                    <div className='flex-grow-1 me-2'>
                                      <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                      <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                    </div>
                                    <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message} >
                                      <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                        <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                        <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                      </svg>
                                    </span>
                                  </div>
                                )
                                : (
                                  <>
                                    {
                                      item && item.status === 'warning'
                                        ? (
                                          <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                            <div className='flex-grow-1 me-2'>
                                              <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                              <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                            </div>
                                            <span className='fw-bolder text-warning py-1' title={item.message}>
                                              <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                            </span>
                                          </div>
                                        )
                                        : (
                                          <>
                                            {
                                              item && item.status === 'negative'
                                                ? (
                                                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                    <div className='flex-grow-1 me-2'>
                                                      <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                      <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                                    </div>
                                                    <span className='fw-bolder text-danger py-1' title={item.message}>
                                                      <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                    </span>
                                                  </div>

                                                )
                                                : null
                                            }
                                          </>

                                        )
                                    }
                                  </>
                                )

                            }
                          </Fragment>
                        )
                      })
                      :<h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div> */}
            </div>
          </div>
        </div>
        <div className='col-lg-6 col-md-6 col-sm-6'>
          <div className='mb-4'>
            <span className='text-muted mt-1 fw-bolder fs-4  justify-content-start'>BUSINESS</span>
            <h1 className='text-dark fw-bolder justify-content-start '>{merchantSummary ? merchantSummary.companyName : "--"}</h1>
          </div>
          <div className='row'>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Phone
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7'>{merchantSummary ? merchantSummary.businessPhone : "--"}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link '
                      onClick={() => { setOpenPhone(true) }}
                    >View Phone Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !BusinessPhone
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(BusinessPhone) ?
                        BusinessPhone && BusinessPhone.map((item, i) => {
                          return (
                            <Fragment key={"FIX_7" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                      </div>
                                      <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6'>{item.coffee}</div>
                                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1' title={item.message}>
                                                <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                              </span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1' title={item.message}>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                      </span>
                                                    </div>

                                                  )
                                                  : null
                                              }
                                            </>

                                          )
                                      }
                                    </>
                                  )

                              }
                            </Fragment>
                          )
                        }
                        )
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-header border-0 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Email
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7 ellipsis'>{merchantSummary ? merchantSummary.businessEmail : "--"}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenEmail(true) }}
                    >View Email Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !BusinessEmail
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(BusinessEmail) ?
                        BusinessEmail && BusinessEmail.map((item, i) => {
                          return (
                            <Fragment key={"FIX_8" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                          {
                                            !_.isEmpty(item.info) ? (
                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                className='tooltip'
                                              >
                                                {item.info}
                                              </Tooltip>}
                                                placement={"right"}
                                              >
                                                <span>
                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                </span>
                                              </OverlayTrigger>
                                            ) : (
                                              null
                                            )
                                          }
                                        </div>
                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                      </div>
                                      <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                  {
                                                    !_.isEmpty(item.info) ? (
                                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                        className='tooltip'
                                                      >
                                                        {item.info}
                                                      </Tooltip>}
                                                        placement={"right"}
                                                      >
                                                        <span>
                                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                        </span>
                                                      </OverlayTrigger>
                                                    ) : (
                                                      null
                                                    )
                                                  }
                                                </div>
                                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1' title={item.message}>
                                                <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                              </span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                          {
                                                            !_.isEmpty(item.info) ? (
                                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                                className='tooltip'
                                                              >
                                                                {item.info}
                                                              </Tooltip>}
                                                                placement={"right"}
                                                              >
                                                                <span>
                                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                                </span>
                                                              </OverlayTrigger>
                                                            ) : (
                                                              null
                                                            )
                                                          }
                                                        </div>
                                                        <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1' title={item.message}>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                      </span>
                                                    </div>

                                                  )
                                                  : null
                                              }
                                            </>

                                          )
                                      }
                                    </>
                                  )

                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }

                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-header border-0 mb-xl-5 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Address
                    </span>
                    <a className='mt-1 fw-bold fs-7 cp text link-muted'
                      data-toggle='modal'
                      data-target='#mapModal'
                    >
                      {merchantSummary ? merchantSummary.businessAddress : "--"}
                    </a>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenAddress(true) }}
                    >View Address Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !BusinessAddress
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(BusinessAddress) ?
                        BusinessAddress && BusinessAddress.map((item, i) => {
                          return (
                            <Fragment key={"FIX_9" + i}>
                              {
                                item && item.status === 'positive'
                                  ? (
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6 ellipsis'>{item.title}</div>
                                        <span className='text-muted fw-bold d-block ellipsis'>{item.value}</span>
                                      </div>
                                      <span className='svg-icon svg-icon-1 svg-icon-success ellipsis' title={item.message}>
                                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                          <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                          <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                        </svg>
                                      </span>
                                    </div>
                                  )
                                  : (
                                    <>
                                      {
                                        item && item.status === 'warning'
                                          ? (
                                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                              <div className='flex-grow-1 me-2'>
                                                <div className='fw-bolder text-gray-800  fs-6 ellipsis'>{item.title}</div>
                                                <span className='text-muted fw-bold d-block ellipsis ellipsis'>{item.value}</span>
                                              </div>
                                              <span className='fw-bolder text-warning py-1 ellipsis' title={item.message}>
                                                <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                              </span>
                                            </div>
                                          )
                                          : (
                                            <>
                                              {
                                                item && item.status === 'negative'
                                                  ? (
                                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                      <div className='flex-grow-1 me-2'>
                                                        <div className='fw-bolder text-gray-800  fs-6 ellipsis'>{item.title}</div>
                                                        <span className='text-muted fw-bold d-block ellipsis ellipsis'>{item.value}</span>
                                                      </div>
                                                      <span className='fw-bolder text-danger py-1 ellipsis' title={item.message}>
                                                        <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                      </span>
                                                    </div>

                                                  )
                                                  : null
                                              }
                                            </>

                                          )
                                      }
                                    </>
                                  )

                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <CrossCheck isLoaded={isLoaded} id={id} />

    </>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore, riskManagementlistStore, ApproveStore, editMerchantStores, MatrixStore, GetCategoryStore } = state
  return {
    dashboardDetails:
      dashboardStore &&
        dashboardStore.dashboardDetails
        ? dashboardStore.dashboardDetails
        : {},
    riskmgmtlistdetails:
      riskManagementlistStore &&
        riskManagementlistStore.riskmgmtlists ?
        riskManagementlistStore.riskmgmtlists : {},
    approveResponceData:
      ApproveStore &&
        ApproveStore.approveResponce ?
        ApproveStore.approveResponce : {},
    loading:
      ApproveStore &&
        ApproveStore.loading ?
        ApproveStore.loading : false,
    merchantIddetails:
      editMerchantStores &&
        editMerchantStores.merchantIddetails ?
        editMerchantStores.merchantIddetails : '',
    matrixDetail:
      MatrixStore &&
        MatrixStore.matrixDetail ?
        MatrixStore.matrixDetail : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPrevAlertDetailsDispatch: (id) => dispatch(dashboardDetailsActions.getdashboardDetails(id)),
  getRiskManagementlistDispatch: () => dispatch(riskManagementActions.getRiskManagementlist()),
  ApprovePost: (id, params) => dispatch(ApproveActions.approve(id, params)),
  getRiskSummaryDispatch: (id) => dispatch(riskSummaryActions.getRiskSummary(id)),
  getIdMerchantDispatch: (id) => dispatch(merchantIdDetailsActions.getmerchantIdDetailsData(id)),
  clearApprove: () => dispatch(ApproveActions.clearApprove()),
  GetCategroyDispatch: (params) => dispatch(GetCategoryActions.GetCategroy(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Merchant)
