import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { riskManagementActions, ExportListActions } from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage, getLocalStorage } from '../../utils/helper';
import { RISKSTATUS, REVIEWTYPE } from '../../utils/constants'
import WebRiskAnalysis from './webRiskAnalysis/WebRiskAnalysis'
import ReactHTMLTableToExcel from "react-html-table-to-excel";

function RiskManagementList(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading,
    exportLists,
    getExportDispatch,
    exportLoading
  } = props
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentRoute = url && url[1]
  const [routeShow, setRouteShow] = useState()
  const [exportShow, setexportShow] = useState(false)
  const csvReport = React.useRef()
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    if (currentRoute === 'risk-management-search') {
      setRouteShow(false)
    } else {
      getRiskManagementlistDispatch(params)
    }
    setexportShow(false)
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: activePageNumber
    }
    getRiskManagementlistDispatch(params)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = '/merchant-login';
  };

  const reportCount = riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.reportStatus
  const riskStatusCount = riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.riskStatus
  const exportHide = JSON.parse(getLocalStorage('ExportHide'))

  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === 'ok') {
      setexportShow(true)
      // setTimeout(() => {
      //   const closeXlsx = document.getElementById('csvReport')
      //   closeXlsx.click()
      // }, 3000)
    }
  }, [exportLists])

  useEffect(() => {
    return (
      setexportShow(false)
    )
  }, [])

  const Reset = (() => {
    getRiskManagementlistDispatch()
    setexportShow(false)
  })

  const exportData = ((data) => {
    const params = {
      id: data
    }
    getExportDispatch(params)
  })


  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="csvReport"
          className="download-table-xls-button"
          table="table-to-xlss"
          filename="simple"
          sheet="tablexls"
        />
      </div>

      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="table-to-xlss">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Web Url</th>
              <th>Status</th>
              <th>Reason</th>
              <th>Website Working?</th>
              <th>Legal Name</th>
              <th>Malware Present</th>
              <th>Malware Risk</th>
              <th>Domain Registered</th>
              <th>Domain Registration Company</th>
              <th>Domain Registration Date</th>
              <th>Domain Registration Expiry Date</th>
              {/* <th>Website Traffic</th> */}
              <th>SSL Certificate Check</th>
              <th>Adult Content Monitoring</th>
              <th>Product Category</th>
              <th>Merchant Policy Link Work</th>
              <th>Negative Keywords</th>
              <th>Readiness</th>
              <th>Transparency</th>
              <th>Contact Details Phone</th>
              <th>Contact Details Email</th>
              <th>Purchase Or Registration</th>
              <th>Merchant Intelligence</th>
              <th>Logo</th>
              <th>IP Address On Server</th>
            </tr>
          </thead>
          <tbody>
            {
              !_.isEmpty(exportLists && exportLists.data && exportLists.data.data) ?
                exportLists && exportLists.data && exportLists.data.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      <td>
                        {item.webUrl}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.riskStatus}
                      </td>
                      <td>
                        {item && item.riskmanagement && item.riskmanagement.reason}
                      </td>
                      <td>
                        {item.websiteWorking}
                      </td>
                      <td>
                        {item.legalName}
                      </td>
                      <td>
                        {item.malwarePresent}
                      </td>
                      <td>
                        {item.malwareRisk}
                      </td>
                      <td>
                        {item.domainRegistered}
                      </td>
                      <td>
                        {item.domainRegistrationCompany}
                      </td>
                      <td>
                        {item.domainRegistrationDate}
                      </td>
                      <td>
                        {item.domainRegistrationExpiryDate}
                      </td>
                      {/* <td>
                        {item.websiteTraffic}
                      </td> */}
                      <td>
                        {item.sslCertificateCheck}
                      </td>
                      <td>
                        {item.adultContentMonitoring}
                      </td>
                      <td>
                        {
                          !_.isEmpty(item && item.productCategory) ? (
                            item && item.productCategory.map((product, pr) => {
                              return (
                                <div key={pr}>
                                  <span>
                                    {
                                      product
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : '--'
                        }
                      </td>
                      <td>
                        {item.merchantPolicyLinkWork}
                      </td>
                      <td>
                        {item.negativeKeywords}
                      </td>
                      <td>
                        {item.readiness}
                      </td>
                      <td>
                        {item.transparency}
                      </td>
                      <td>
                        {
                          !_.isEmpty(item && item.contactDetailsPhone) ? (
                            item && item.contactDetailsPhone.map((phone, p) => {
                              return (
                                <div key={p}>
                                  <span>
                                    {
                                      phone
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : '--'
                        }
                      </td>
                      <td>
                        {
                          !_.isEmpty(item && item.contactDetailsEmail) ? (
                            item && item.contactDetailsEmail.map((email, e) => {
                              return (
                                <div key={e}>
                                  <span>
                                    {
                                      email
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : '--'
                        }
                      </td>
                      <td>
                        {item.purchaseOrRegistration}
                      </td>
                      <td>
                        {item.merchantIntelligence}
                      </td>
                      <td>
                        <span>
                          <img
                            src={item.logo}
                          />
                        </span>
                      </td>
                      <td>
                        {
                          !_.isEmpty(item && item.ipAddressOfServer) ? (
                            item && item.ipAddressOfServer.map((ipAddress, Ip) => {
                              return (
                                <div key={Ip}>
                                  <span>
                                    {
                                      ipAddress
                                    }
                                  </span>
                                </div>
                              )
                            })
                          ) : '--'
                        }
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-4'>
              <div className='col-md-6 mt-1 ms-2'>
                {riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists && riskmgmtlists.data && riskmgmtlists.data.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-6 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-8 justify-content-end my-auto mt-4'>
              <div className='my-auto'>
                <WebRiskAnalysis exportShow={exportShow} />
              </div>
              <div className='my-auto'>
                <SearchList />
              </div>
              <div className='my-auto me-3'>
                {
                  exportShow ?
                    <a
                      className='btn btn-sm btn-light-danger btn-responsive font-5vw'
                      onClick={Reset}
                    >
                      <KTSVG path="/media/icons/duotune/files/fil006.svg"
                      />
                      Reset Page
                    </a>
                    : ''
                }
                {/* <a
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                  onClick={logout}
                >
                  <KTSVG path="/media/icons/duotune/files/fil003.svg" 
                  />
                  Add Case
                </a> */}
                {/* <Link
                  to='/merchant-login'
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                   <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                  Add Case
                </Link> */}
              </div>
            </div>
          </div>
          {
            exportShow ? (

              <div className='d-flex col-md-12 align-items-start'
                style={{
                  marginLeft: '10px'
                }}
              >
                <div className='row'>
                  <div className='col-md-5'>
                    <div className='row'>
                      <div className='text-black-700 fw-bolder'>
                        CATEGORY STATUS
                      </div>
                      <div className='col-md-12 mt-4'>
                        <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                          APPROVED
                          <span className=' text-hover-primary'>
                            - {riskStatusCount && riskStatusCount.approvedCount ? riskStatusCount.approvedCount : '0'}
                          </span>
                        </span>
                        <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                          REJECTED
                          <span className='text-hover-primary'>
                            - {riskStatusCount && riskStatusCount.rejectCount ? riskStatusCount.rejectCount : '0'}
                          </span>
                        </span>
                        <span className='text-warning fw-bold mt-2 fs-8'>
                          MANUAL REVIEW
                          <span className='text-hover-primary'>
                            - {riskStatusCount && riskStatusCount.manualReviewCount ? riskStatusCount.manualReviewCount : '0'}
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='col-md-7'>
                    <div className='row'>
                      <div className='text-black-700 fw-bolder'>
                        REPORT STATUS
                      </div>
                      <div className='col-md-12 mt-4'>
                        <span className='text-warning fw-bold mt-2 fs-8 me-2'>
                          PENDING
                          <span className='text-hover-primary'>
                            - {reportCount && reportCount.pendingReportCount ? reportCount.pendingReportCount : '0'}
                          </span>
                        </span>
                        <span className='text-info fw-bold mt-2 fs-8 me-2'>
                          DATA PROCESSING
                          <span className='text-hover-primary'>
                            - {reportCount && reportCount.dataProcessingCount ? reportCount.dataProcessingCount : '0'}
                          </span>
                        </span>
                        <span className='text-dark fw-bold mt-2 fs-8 me-2'>
                          DATA CAPTURED
                          <span className='text-hover-primary'>
                            - {reportCount && reportCount.dataCapturedCount ? reportCount.dataCapturedCount : '0'}
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='col-md-5' />
                  <div className='col-md-7'>
                    <div
                      className='row'
                    >
                      <div className='col-md-12 mt-4'>
                        <span className='text-primary fw-bold mt-2 fs-8 me-4'>
                          PREPARING REPORT
                          <span className=' text-hover-primary'>
                            - {reportCount && reportCount.preparingReportCount ? reportCount.preparingReportCount : '0'}
                          </span>
                        </span>
                        <span className='text-success fw-bold mt-2 fs-8 me-4'>
                          COMPLETED
                          <span className=' text-hover-primary'>
                            - {reportCount && reportCount.completedCount ? reportCount.completedCount : '0'}
                          </span>
                        </span>
                        <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                          REJECTED
                          <span className=' text-hover-primary'>
                            - {reportCount && reportCount.rejectCount ? reportCount.rejectCount : '0'}
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) :
              null
          }

          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th className="">
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Review Type</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Suspected Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("suspectedStatus")}
                        >
                          <i
                            className={`bi ${sorting.suspectedStatus

                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Tag</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Report Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${sorting.ReportStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Download Report</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("downloadReport")}
                        >
                          <i
                            className={`bi ${sorting.downloadReport
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>First Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${sorting.firstName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Last Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("lastName")}
                        >
                          <i
                            className={`bi ${sorting.lastName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Company Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("companyName")}
                        >
                          <i
                            className={`bi ${sorting.companyName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Individual Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("phone")}
                        >
                          <i
                            className={`bi ${sorting.phone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Individual Email</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("email")}
                        >
                          <i
                            className={`bi ${sorting.email
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Company Email</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("companyEmail")}
                        >
                          <i
                            className={`bi ${sorting.companyEmail
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>IP Address</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ipAddress")}
                        >
                          <i
                            className={`bi ${sorting.ipAddress
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Address</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("address")}
                        >
                          <i
                            className={`bi ${sorting.address
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Device ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.deviceID
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Business Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.businessPhone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Business Address</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.businessAddress
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Block List Reason</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("blockListReason")}
                        >
                          <i
                            className={`bi ${sorting.blockListReason
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Reason</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("reason")}
                        >
                          <i
                            className={`bi ${sorting.reason
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Manual Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("manualStatus")}
                        >
                          <i
                            className={`bi ${sorting.manualStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Auto Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("autoStatus")}
                        >
                          <i
                            className={`bi ${sorting.autoStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      riskmgmtlists &&
                        riskmgmtlists.data
                        ? (
                          riskmgmtlists.data && riskmgmtlists.data.result.map((riskmgmtlist, i) => {
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <Link to={`/risk-summary/${riskmgmtlist._id}`} className="ellipsis">
                                    IRM{riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"}
                                  </Link>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskmgmtlist.riskStatus && riskmgmtlist.riskStatus]}`}>
                                    {riskmgmtlist.riskStatus ? riskmgmtlist.riskStatus : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${REVIEWTYPE[riskmgmtlist.reviewType && riskmgmtlist.reviewType ? riskmgmtlist.reviewType : '--']}`}>
                                    {riskmgmtlist.reviewType ? riskmgmtlist.reviewType : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${REVIEWTYPE[riskmgmtlist.amlStatus && riskmgmtlist.amlStatus ? riskmgmtlist.amlStatus : '--']}`}>
                                    {riskmgmtlist.amlStatus ? riskmgmtlist.amlStatus : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <a
                                    // href={riskmgmtlist.website}
                                    target={riskmgmtlist.website}
                                    rel="noopener noreferrer"
                                  >
                                    {riskmgmtlist.website ? riskmgmtlist.website : "--"}
                                  </a>
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.tag ? riskmgmtlist.tag : "--"}
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskmgmtlist.reportStatus && riskmgmtlist.reportStatus]}`}>
                                    {riskmgmtlist.reportStatus ? riskmgmtlist.reportStatus : "--"}
                                  </span>
                                </td>
                                {
                                  riskmgmtlist.reportStatus === 'COMPLETED' ? (
                                    <td className='text-center'>
                                      {/* <button type="button"
                                        class={`btn btn-light-success me-10 btn-sm ${exportLoading ? 'w-150px' : 'w-100px'}`}
                                        onClick={() => { exportData(riskmgmtlist._id) }}
                                      >
                                        {!exportLoading && 
                                        <span className='indicator-label'>
                                            <i class="bi bi-filetype-xlsx" />
                                          Export
                                        </span>
                                        }
                                        {exportLoading && riskmgmtlist.reportStatus[i]  (
                                          <span className='indicator-progress' style={{ display: 'block' }}>
                                            Please wait...
                                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                          </span>
                                        )}
                                      </button> */}
                                      --
                                    </td>
                                  ) :
                                    (
                                      <td className='text-center'>
                                        --
                                      </td>
                                    )
                                }
                                <td className="ellipsis">
                                  {riskmgmtlist.firstName ? riskmgmtlist.firstName : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.lastName ? riskmgmtlist.lastName : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.companyName ? riskmgmtlist.companyName : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.phone ? riskmgmtlist.phone : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.personalEmail ? riskmgmtlist.personalEmail : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.businessEmail ? riskmgmtlist.businessEmail : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.ipAddress ? riskmgmtlist.ipAddress : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.address ? riskmgmtlist.address : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.deviceID ? riskmgmtlist.deviceID : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.businessPhone ? riskmgmtlist.businessPhone : "--"}
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.businessAddress ? riskmgmtlist.businessAddress : "--"}
                                </td>
                                <td className="ellipsis">
                                  {
                                    !_.isEmpty(riskmgmtlist && riskmgmtlist.blackListHistory && riskmgmtlist.blackListHistory) ?
                                      riskmgmtlist && riskmgmtlist.blackListHistory && riskmgmtlist.blackListHistory.map((item, id) => {
                                        return (
                                          <div key={"rees_" + id}>
                                            <div>
                                              {item && item.fieldType ? item.fieldType : '--'} - {item && item.fieldValue ? item.fieldValue : '--'}
                                            </div>
                                          </div>
                                        )
                                      })
                                      :
                                      '--'
                                  }
                                </td>
                                <td>
                                  {riskmgmtlist.reason ? riskmgmtlist.reason : "--"}
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskmgmtlist.manualStatus && riskmgmtlist.manualStatus]}`}>
                                    {riskmgmtlist.manualStatus ? riskmgmtlist.manualStatus : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskmgmtlist.autoStatus && riskmgmtlist.autoStatus]}`}>
                                    {riskmgmtlist.autoStatus ? riskmgmtlist.autoStatus : "--"}
                                  </span>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore, exportlistStore } = state;
  return {
    riskmgmtlists: state && state.riskManagementlistStore && state.riskManagementlistStore.riskmgmtlists,
    loading: riskManagementlistStore && riskManagementlistStore.loading ? riskManagementlistStore.loading : false,
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : false
  };
};

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(RiskManagementList);
