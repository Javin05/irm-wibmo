import _ from 'lodash'

function FindRole(props) {
  const { role, children } = props
  const viewRole = (value) => {
    let valid = false
    if (value === 'Client User') {
      return valid = false
    } else {
      return valid = true
    }
  }

  return (
    !_.isEmpty(role) && viewRole(role) ? (
      <>{children}</>
    ) : (
      null
    )
  )
}
export default FindRole