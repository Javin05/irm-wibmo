import React from "react";
import ApexCharts from "react-apexcharts";

const MixedChart = (props) => {
    const { chartData } = props
    const chart = {
        series: chartData?.series,
        options: {
            colors : chartData?.colors,
            chart: {
                height: 350,
                type: 'line',
                stacked: true,
                toolbar: {
                    show: true,
                    tools: {
                        download: true,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset : false,
                        //customIcons: []
                    },
                },
            },
            stroke: {
                width: [0, 0]
            },
            dataLabels: {
                enabled: true,
                enabledOnSeries: [1]
            },
            labels: chartData?.labels,
            xaxis: {
                //type: 'datetime'
            },
            yaxis: [{
                title: {
                    text: '',
                },
            }, {
                opposite: true,
                title: {
                    text: ''
                }
            }]
        }
    };
    return (
        <ApexCharts
            options={chart.options}
            series={chart.series}
            type="line"
            height={240}
        />
    );
};

export default MixedChart;