/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useMemo, useEffect, useState } from "react";
import ReactApexChart from 'react-apexcharts'

const MultiHorizontalBarChart = (props) => {
    const { chartData } = props

    const chart = {
        series: chartData?.series,
        options: {
            colors : chartData?.colors,
            chart: {
                height: 350,
                type: 'bar',
                stacked: false,
                toolbar: {
                    show: true,
                    tools: {
                        download: true,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset : false,
                        //customIcons: []
                    },
                },
            },
            stroke: {
                width: [0, 0]
            },
            dataLabels: {
                enabled: true,
                enabledOnSeries: [1]
            },
            labels: chartData?.labels,
            xaxis: {
                //type: 'datetime'
            },
            yaxis: [{
                title: {
                    text: '',
                },
            }, {
                opposite: true,
                title: {
                    text: ''
                }
            }]
        }
    };
    
    return (
        <ReactApexChart
            options={chart.options}
            series={chart.series}
            type="bar"
            width="100%"
            height="300px"
        />
    );
}

export default MultiHorizontalBarChart;