import React from "react";
import ApexCharts from "react-apexcharts";

const BarLineChart = () => {
  const chart = {
    series: [
      {
        name: "Users",
        type: "column",
        data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3.8, 4.6]
      },
      {
        name: "Sales",
        type: "column",
        data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6.5, 8.5]
      },
      {
        name: "Clicks",
        type: "line",
        data: [20, 29, 37, 36, 44, 45, 50, 58]
      }
    ],
    options: {
      fill: {
        colors: ["#3498db", "#2ecc71", "#e67e22"]
      },
      stroke: {
        width: [1, 1, 4],
        colors: ["#3498db", "#2ecc71", "#e67e22"]
      },
      chart: {
        height: 350,
        type: "line",
        stacked: false,
        toolbar: {
            show: false
        },
      },
      dataLabels: {
        enabled: false
      },
      xaxis: {
        categories: [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016]
      },
      tooltip: {
        fixed: {
          enabled: true,
          position: "topLeft", // topRight, topLeft, bottomRight, bottomLeft
          offsetY: 30,
          offsetX: 60
        }
      },
      legend: {
        horizontalAlign: "center"
      }
    }
  };
  return (

    <ApexCharts
      options={chart.options}
      series={chart.series}
      type="line"
      height={300}
    />
  );
};

export default BarLineChart;
