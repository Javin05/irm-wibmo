/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useMemo, useEffect, useState } from "react";
import ReactApexChart from 'react-apexcharts'

const VerticleBarChart = (props) => {
    const { chartData, stacked } = props

    const chart = {
        options: {
            chart: {
                type: "bar",
                height: "auto",
                stacked: stacked,
                toolbar: {
                    show: true,
                    tools: {
                        download: true,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset : false,
                        //customIcons: []
                    },
                },
            },
            colors: chartData?.colors,
            plotOptions: {
                bar: {
                    horizontal: true,
                    endingShape: "rounded"
                }
            },
            yaxis: {
                show: true,                
            },
            xaxis: {
                labels: {
                    show: false,
                }
            },
            labels: chartData?.labels
        },
        series: chartData?.series
    }
    
    return (
        <ReactApexChart
            options={chart.options}
            series={chart.series}
            type="bar"
            width="100%"
            height="300px"
        />
    );
}

export default VerticleBarChart;