import React, { useState, useEffect, Fragment } from "react"
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import Select from 'react-select'

const CategoryDropdown=(props)=>{
	const {selected, blacklistTypes} = props
	const [show, setShow] = useState(false)
	const [blacklistDrop, setBlacklistDrop] = useState(null)
	const options = [
	  	{ value: 'chocolate', label: 'Chocolate' },
	  	{ value: 'strawberry', label: 'Strawberry' },
	  	{ value: 'vanilla', label: 'Vanilla' }
	]

	useEffect(() => {
        if(blacklistTypes){
        	let blacktemp = [];
        	Object.keys(blacklistTypes).forEach(function(key) {
                blacktemp.push({
                    "value":blacklistTypes[key].fieldType,
                    "label":blacklistTypes[key].fieldValue,
                });
            });

            setBlacklistDrop(blacktemp)
        }
    }, [blacklistTypes])
	return (
		<OverlayTrigger
      		trigger="click"
      		placement="bottom"
      		rootClose={true}      		
      		overlay={
        		<Popover className="custom-overlay-trigger" id={`popover-positioned-bottom`}>
          			<Popover.Body>
            			<Select 
            				onInputChange={()=>{
            					setShow()
            				}}
            				menuIsOpen={true}
            				onMenuClose={true}
            				autoBlur={true}
            				options={blacklistDrop} />
          			</Popover.Body>
        		</Popover>
      		}>
      		<span className="custom-react-select">{ selected==='' || selected===undefined ? "Select..." : selected}</span>
    	</OverlayTrigger>
	)
}

export default CategoryDropdown
