import { USER_ERROR } from '../../utils/constants'

export const crmValidation = (values, setErrors) => {
  const errors = {}
  if (!values.crmName) {
    errors.crmName = USER_ERROR.CRM_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const processorValidation = (values, setErrors) => {
  const errors = {}
  if (!values.processorName) {
    errors.processorName = USER_ERROR.PROCESSOR_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const acquirerValidation = (values, setErrors) => {
  const errors = {}
  if (!values.processorName) {
    errors.processorName = USER_ERROR.ACQUIRER_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const gatewayValidation = (values, setErrors) => {
  const errors = {}
  if (!values.gatewayName) {
    errors.gatewayName = USER_ERROR.GATEWAY_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const industryValidation = (values, setErrors) => {
  const errors = {}
  if (!values.industryType) {
    errors.industryType = USER_ERROR.INDUSTRY_REQUIRED
  }
  setErrors(errors)
  return errors
}
