import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, useLocation, useHistory } from "react-router-dom";
import ReactSelect from "../../../theme/layout/components/ReactSelect";
import TextSunEditor from "../../../theme/layout/components/SunEditor";
import color from "../../../utils/colors";
import { headersWithAuth } from '../../../utils/helper'
import { STATUS_RESPONSE } from "../../../utils/constants";
import {
  addGeneralTemplateActions,
  variableActions,
  generalTemplateDetailsActions,
  editGeneralTemplateActions,
  notifyActions,
  addNotifyActions
} from "../../../store/actions";
import { warningAlert, confirmAlert } from "../../../utils/alerts";
import { setTemplateData } from "./formData";
import { emailTemplateValidation } from "./validation";
import _ from "lodash";

const AddTemplate = (props) => {
  const {
    addMerchantTemplateDispatch,
    clearaddMerchantTemplateDispatch,
    getVariableDispatch,
    variableData,
    messageAGT,
    statusAGT,
    loadingAGT,
    getTemplateDetailsDispatch,
    generalTemplateDetails,
    statusGTD,
    loadingGTD,
    editTemplateDispatch,
    clearEditTemplateDispatch,
    statusEGT,
    messageEGT,
    loadingEGT,
    clearTemplateDetailsDispatch,
    addNotifyDispatch,
    getNotifyDispatch,
    getNotify,
    loadingANT,
    statusANT,
    messageANT,
    loading,
    clearaddNotifyDispatch
  } = props;

  const history = useHistory();
  const url = useLocation().pathname;
  const fields = url && url.split("update/");
  const id = fields && fields[1];
  const [errors, setErrors] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [copiedText, setCopyText] = useState("");
  const [selectedNotifyOptions, setSelectedNotifyOptions] = useState();
  const [notifyOptions, setNotifyOptions] = useState();
  const [typingTimeout, setTypingTimeout] = useState(0);
  const [othersNotificationType, setOthersNotificationType] = useState('');
  const [formData, setFormData] = useState({
    notificationType: "",
    subject: "",
    emailTemplate: "",
  });

  const resetState = (e) => {
    setSelectedNotifyOptions();
    setFormData({
      notificationType: "",
      subject: "",
      emailTemplate: "",
    });
  };

  const handleChange = (e) => {
    e.persist();
    e.preventDefault();
    if (e.target.name === "othersNotificationType") {
      setOthersNotificationType(e.target.value)
    } else {
      setFormData((values) => ({ ...values, [e.target.name]: e.target.value }));
      setErrors({ ...errors, [e.target.name]: "" });
    }
  };

  const handleSubmit = () => {
    const errorMsg = emailTemplateValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      const payload = {
        notificationType: othersNotificationType ? othersNotificationType : formData.notificationType,
        subject: formData.subject,
        emailTemplate: formData.emailTemplate,
      };
      if (editMode) {
        editTemplateDispatch(id, payload, headersWithAuth);
      } else {
        addMerchantTemplateDispatch(payload, headersWithAuth);
      }
    }
  };

  const handleNotify = () => {
    const data = {
      notificationType: othersNotificationType
    };
    addNotifyDispatch(data, headersWithAuth);
  };

  const customStyles = {
    notifyOptions: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      zIndex: 1,
    }),
    menu: (base) => ({
      ...base,
      zIndex: 25,
    }),
  };

  useEffect(() => {
    getNotifyDispatch(headersWithAuth);
  }, []);

  const getDefaultOptions = (data, name) => {
    const defaultOptions = [
      { label: "Others...", value: "others" }
    ]
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ''}`,
          value: item[name]
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getNotify, "notificationType");
    setNotifyOptions(data);
  }, [getNotify]);

  const handleSelectNotify = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          const notifyParams = {
            skipPagination: true,
            notificationType: selectedOption.value,
          };
          getVariableDispatch(notifyParams, headersWithAuth);
        }, 1500)
      );
      setSelectedNotifyOptions(selectedOption);
      setFormData((values) => ({
        ...values,
        notificationType: selectedOption.value,
      }));
      setErrors({ ...errors, notificationType: "", othersNotificationType: "" });
      setOthersNotificationType('')
    } else {
      setSelectedNotifyOptions();
      setOthersNotificationType('')
      setFormData((values) => ({ ...values, notificationType: "" }));
    }
  };

  const onANTConfirm = () => {
  };

  const onConfirm = () => {
    resetState();
    history.push("/email-templates");
  };

  useEffect(() => {
    if (statusEGT === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageEGT,
        "success",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearEditTemplateDispatch();
    } else if (statusEGT === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert("Error", messageEGT, "", "Ok");
      clearEditTemplateDispatch();
    }
  }, [statusEGT]);

  useEffect(() => {
    if (statusAGT === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageAGT,
        "success",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearaddMerchantTemplateDispatch();
    } else if (statusAGT === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert("Error", messageAGT, "", "Ok");
      clearaddMerchantTemplateDispatch();
    }
  }, [statusAGT]);

  useEffect(() => {
    if (statusANT === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageANT,
        "success",
        "Ok",
        () => {
          onANTConfirm();
        },
        () => {
          onANTConfirm();
        }
      );
      clearaddNotifyDispatch();
    } else if (statusANT === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageANT,
        '',
        'Ok'
      )
      clearaddNotifyDispatch();
    }
  }, [statusANT]);

  useEffect(() => {
    if (statusGTD === STATUS_RESPONSE.SUCCESS_MSG) {
      const setData = setTemplateData(generalTemplateDetails);
      setFormData(setData);
    }
    if (generalTemplateDetails.notificationType) {
      const getDefaultOptions = [
        { label: generalTemplateDetails.notificationType, value: generalTemplateDetails.notificationType }
      ]
      setSelectedNotifyOptions(getDefaultOptions)
      const notifyEditParams = {
        skipPagination: true,
        notificationType: generalTemplateDetails.notificationType,
      };
      getVariableDispatch(notifyEditParams, headersWithAuth);
    }
    clearTemplateDetailsDispatch();
  }, [statusGTD]);

  useEffect(() => {
    if (id) {
      setEditMode(true);
      getTemplateDetailsDispatch(id, headersWithAuth);
    } else {
      setEditMode(false);
    }
  }, [id]);

  const handleCopy = (id) => {
    const copyText = document.getElementById(id);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    navigator.clipboard.writeText(copyText.value);
    setCopyText(id);
    setTimeout(() => {
      setCopyText("");
    }, 1000);
  };

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target;
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, " ");
      setState((values) => ({ ...values, [name]: getData.trim() }));
    } else {
      setState((values) => ({ ...values, [name]: "" }));
    }
  };

  return (
    <>
      <div className="card-header bg-skyBlue py-0">
        <div className="card-body m-8 p-3">
          <div className="d-flex justify-content-end">
            <Link
              to="/email-templates"
              className="btn btn-darkRed fa-pull-right"
              onClick={() => {
                resetState();
              }}
            >
              Back
            </Link>
          </div>
          {loadingGTD ? (
            <div className="d-flex justify-content-center py-5">
              <div className="spinner-border text-primary m-5" role="status" />
            </div>
          ) : (
            <>
              <div className="card-body">
                <div className="row mt-2">
                  <div className="col-lg-6">
                    <label className="required fw-bold fs-6 mb-2">
                      Notification Type
                    </label>
                    <ReactSelect
                      isClearable
                      styles={customStyles}
                      isMulti={false}
                      name="notificationType"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleSelectNotify}
                      options={notifyOptions}
                      value={selectedNotifyOptions}
                      isLoading={loading}
                    />
                    {errors && errors.notificationType && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.notificationType}
                      </div>
                    )}
                  </div>
                  <div className="col-lg-6">
                    <label className="required fw-bold fs-6 mb-2">
                      Subject
                    </label>
                    <textarea
                      name="subject"
                      type="text"
                      className="form-control"
                      placeholder="Subject"
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onChange={(e) => handleChange(e)}
                      value={formData.subject || ""}
                      autoComplete="off"
                    />
                    {errors && errors.subject && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.subject}
                      </div>
                    )}
                  </div>
                </div>
                {formData.notificationType === "others" ? (
                  <div className="row mt-2 mb-5">
                    <div className="col-lg-6">
                      <label className="required fw-bold fs-6 mb-2">
                        Other Notification Type
                      </label>
                      <input
                        name='othersNotificationType'
                        type='text'
                        className='form-control'
                        placeholder='Other Notification Type'
                        autoComplete='off'
                        maxLength={42}
                        onChange={(e) => handleChange(e)}
                        value={othersNotificationType || ""}
                      />
                      {errors && errors.othersNotificationType ? (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.othersNotificationType}
                        </div>
                      ) : null}
                    </div>
                    <div className="col-lg-2">
                      <button
                        className='btn btn-sm btn-success mt-9'
                        onClick={(e) => {
                          handleNotify(e)
                        }}
                      >
                        {loadingANT
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true' />
                          )
                          : (
                            'Submit'
                          )}
                      </button>
                    </div>
                  </div>
                ) : null}
                <div className="row mt-2">
                  <div className="col-lg-8">
                    <label className="required fw-bold fs-6 mb-2">
                      Template
                    </label>
                    <TextSunEditor
                      setData={setFormData}
                      contentData={formData.emailTemplate}
                      name="emailTemplate"
                      minHeight={"400px"}
                    />
                    {errors && errors.emailTemplate && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.emailTemplate}
                      </div>
                    )}
                  </div>
                  <div className="col-lg-4 mt-8">
                    {variableData &&
                      variableData.data?.map((variable, i) => {
                        return (
                          <div className="card-big-shadow">
                            <div className="card min-w-300px">
                              <h5 className="text-center my-2">
                                Email Template Dynamic Variables
                              </h5>
                              <div
                                className="scroll h-500px ms-6 mt-3"
                                key={i}
                              >
                                {variable.dynamicVariable?.map(
                                  (item, index) => {
                                    return (
                                      <div
                                        className="input-group mb-5"
                                        key={index}
                                      >
                                        <input
                                          type="text"
                                          id={item}
                                          value={item}
                                          className="list-group-item list-group-item-primary rounded-0"
                                          style={{
                                            width: "225px",
                                            marginLeft: "10px",
                                          }}
                                          aria-describedby="basic-addon2"
                                        />
                                        <span
                                          className={`list-group-item rounded-0 list-group-item-${copiedText === `${item}`
                                            ? "success"
                                            : "primary"
                                            }`}
                                          id="basic-addon2"
                                        >
                                          <i
                                            className={`bi bi-clipboard${copiedText === `${item}`
                                              ? "-check"
                                              : ""
                                              } cursor-pointer`}
                                            onClick={() => {
                                              handleCopy(item);
                                            }}
                                          />
                                        </span>
                                      </div>
                                    );
                                  }
                                )}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                </div>
                <div className="form-group row mt-4">
                  <div className="col-lg-5" />
                  <div className="col-lg-7">
                    <div className="col-lg-12">
                      <button
                        className="btn btn-blue mt-7 fa-pull-right"
                        onClick={(event) => {
                          handleSubmit(event);
                        }}
                      >
                        {loadingAGT || loadingEGT ? (
                          <span
                            className="spinner-border spinner-border-sm mx-3"
                            role="status"
                            aria-hidden="true"
                          />
                        ) : (
                          "Submit"
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  const {
    addGeneralTemplateStore,
    variableStore,
    generalTemplateStore,
    editGeneralTemplateStore,
    addNotifyStore
  } = state;
  return {
    messageAGT:
      addGeneralTemplateStore && addGeneralTemplateStore.messageAGT
        ? addGeneralTemplateStore.messageAGT
        : 0,
    statusAGT:
      addGeneralTemplateStore && addGeneralTemplateStore.statusAGT
        ? addGeneralTemplateStore.statusAGT
        : {},
    loadingAGT:
      addGeneralTemplateStore && addGeneralTemplateStore.loadingAGT
        ? addGeneralTemplateStore.loadingAGT
        : false,
    variableData:
      variableStore && variableStore.variableData
        ? variableStore.variableData
        : [],
    variableLoading:
      state && state.variableStore && state.variableStore.variableLoading,

    statusEGT:
      editGeneralTemplateStore && editGeneralTemplateStore.statusEGT
        ? editGeneralTemplateStore.statusEGT
        : "",
    messageEGT:
      editGeneralTemplateStore && editGeneralTemplateStore.messageEGT
        ? editGeneralTemplateStore.messageEGT
        : "",
    loadingEGT:
      editGeneralTemplateStore && editGeneralTemplateStore.loadingEGT
        ? editGeneralTemplateStore.loadingEGT
        : false,
    generalTemplateDetails:
      generalTemplateStore && generalTemplateStore.generalTemplateDetails
        ? generalTemplateStore.generalTemplateDetails
        : {},
    statusGTD:
      generalTemplateStore && generalTemplateStore.statusGTD
        ? generalTemplateStore.statusGTD
        : "",
    messagesGTD:
      generalTemplateStore && generalTemplateStore.messagesGTD
        ? generalTemplateStore.messagesGTD
        : "",
    loadingGTD:
      generalTemplateStore && generalTemplateStore.loadingGTD
        ? generalTemplateStore.loadingGTD
        : false,
    getNotify: state && state.notifyStore && state.notifyStore.getNotify,
    loading: state && state.notifyStore && state.notifyStore.loading,
    loadingANT: state && state.addNotifyStore && state.addNotifyStore.loadingANT,
    statusANT:
      addNotifyStore && addNotifyStore.statusANT ? addNotifyStore.statusANT : "",
    messagesANT:
      addNotifyStore && addNotifyStore.messagesANT
        ? addNotifyStore.messagesANT
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  addMerchantTemplateDispatch: (data, headers) =>
    dispatch(addGeneralTemplateActions.add(data, headers)),
  clearaddMerchantTemplateDispatch: () =>
    dispatch(addGeneralTemplateActions.clear()),
  getVariableDispatch: (data, headers) => dispatch(variableActions.getVariable(data, headers)),
  editTemplateDispatch: (id, data, headers) =>
    dispatch(editGeneralTemplateActions.edit(id, data, headers)),
  clearEditTemplateDispatch: () => dispatch(editGeneralTemplateActions.clear()),
  getTemplateDetailsDispatch: (data, headers) =>
    dispatch(generalTemplateDetailsActions.get(data, headers)),
  clearTemplateDetailsDispatch: () =>
    dispatch(generalTemplateDetailsActions.clear()),
  getNotifyDispatch: (headers) => dispatch(notifyActions.get(headers)),
  notifyActions: (data, headers) => dispatch(notifyActions.get(data, headers)),
  addNotifyDispatch: (data, headers) =>
    dispatch(addNotifyActions.add(data, headers)),
  clearaddNotifyDispatch: () =>
    dispatch(addNotifyActions.clear())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTemplate);
