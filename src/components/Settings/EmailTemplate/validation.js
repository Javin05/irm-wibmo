import { USER_ERROR } from '../../../utils/constants'

export const emailTemplateValidation = (values, setErrors) => {
  const errors = {}
  if (!values.notificationType) {
    errors.notificationType = USER_ERROR.NOT_TYPE
  }
  if (!values.subject) {
    errors.subject = USER_ERROR.SUBJECT
  }
  if (!values.emailTemplate) {
    errors.emailTemplate = USER_ERROR.EMAIL_TEMPLATE
  }
  setErrors(errors)
  return errors
}
