import { SITE_CONFIG_ERROR } from '../../utils/constants'

export default function validate (values, setErrors) {
  const errors = {}
  if (!values.passwordMaxChar) {
    errors.passwordMaxChar = SITE_CONFIG_ERROR.PASS_MAX_REQUIRED
  }
  if (!values.passwordMinChar) {
    errors.passwordMinChar = SITE_CONFIG_ERROR.PASS_MIN_REQUIRED
  }
  if (!values.passwordLeastDigits) {
    errors.passwordLeastDigits = SITE_CONFIG_ERROR.PASS_LEAST_REQUIRED
  }
  if (!values.previousPasswordRepeatedTimes) {
    errors.previousPasswordRepeatedTimes = SITE_CONFIG_ERROR.PASS_REPEATED_REQUIRED
  }
  if (!values.passwordExpiredDays) {
    errors.passwordExpiredDays = SITE_CONFIG_ERROR.PASS_EXPIRED_REQUIRED
  }
  if (!values.passwordMaxFailureAttemt) {
    errors.passwordMaxFailureAttemt = SITE_CONFIG_ERROR.PASS_MAX_FAIL_REQUIRED
  }
  if (!values.loginDurationAfterFailureAttemt) {
    errors.loginDurationAfterFailureAttemt = SITE_CONFIG_ERROR.LOGIN_DURA_REQUIRED
  }
  if (!values.sessionTimeOut) {
    errors.sessionTimeOut = SITE_CONFIG_ERROR.SESSION_TIME
  }

  setErrors(errors)
  return errors
}
