import React, { useState, useEffect } from 'react'
import { REGEX, STATUS_RESPONSE } from '../../utils/constants'
import { connect } from "react-redux";
import {
  siteConfigActions,
  editSiteConfigsActions,
} from "../../store/actions";
import {
  warningAlert,
  confirmAlert
} from "../../utils/alerts";
import { SET_STORAGE } from "../../utils/constants";
import { setLocalStorage } from "../../utils/helper";
import { setConfigData } from './formData'
import validate from './validation'
import _ from 'lodash'

function SiteConfig(props) {

  const {
    getSiteconfigDispatch,
    siteConfigs,
    statusGSC,
    loadingGSC,
    editSiteconfigDispatch,
    clearEditSiteconfigDispatch,
    statusESC,
    messageESC,
    loadingESC
  } = props;

  const [formData, setFormData] = useState({
    passwordMaxChar: '',
    passwordMinChar: '',
    passwordLeastDigits: '',
    previousPasswordRepeatedTimes: '',
    passwordExpiredDays: '',
    passwordMaxFailureAttemt: '',
    loginDurationAfterFailureAttemt: '',
    sessionTimeOut: ''
  })
  const [errors, setErrors] = useState({})
  const id = siteConfigs._id

  const handleChange = (e) => {
    e.preventDefault()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = validate(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      editSiteconfigDispatch(id, formData)
    }
  }

  const onConfirm = () => {
    getSiteconfigDispatch()
  }

  useEffect(() => {
    if (statusGSC === STATUS_RESPONSE.SUCCESS_MSG) {
      const setData = setConfigData(siteConfigs);
      setFormData(setData);
      setLocalStorage(SET_STORAGE.SITE_CONFIG_DATA, JSON.stringify(setData))
    }
  }, [statusGSC]);

  useEffect(() => {
    getSiteconfigDispatch()
  }, [])

  useEffect(() => {
    if (statusESC === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageESC,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() },
      )
      clearEditSiteconfigDispatch();
    } else if (statusESC === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "Error",
        messageESC,
        "error",
        "Close",
        "Ok",
        () => {
          onConfirm();
        },
        () => { }
      );
      clearEditSiteconfigDispatch();
    }
  }, [statusESC]);

  const handleTrimSpaceOnly = (e, setState) => {
    const { name, value } = e.target
    const getData = value.replace(/ +/g, '')
    setState((values) => ({ ...values, [name]: getData.trim() }))
  }

  return (
    <>
      <div className='card-header bg-skyBlue py-10'>
        <div className="card-title d-flex row">
          <div className='col-lg-2 justify-content-start px-8'>
            <div className="card-header ribbon ribbon-start ribbon-clip">
              <div className="ribbon-label">
                <span className="bi bi-gear-fill" />
                <span className="ribbon-inner bg-dark"></span>
              </div>
              <div className="card-title d-flex justify-content-start mt-2 px-4">
                Site Configuration
              </div>
            </div>
          </div>
        </div>
        <div className='card-body'>
          {loadingGSC ? (
            <div className="d-flex justify-content-center py-5">
              <div className="spinner-border text-primary m-5" role="status" />
            </div>
          ) : (
            <>
              <div className='row mb-6 mt-6'>
                <div className='col-lg-1' />
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Password Min Character:</label>
                      <div className='col-lg-10'>
                        <input
                          name='passwordMinChar'
                          type='text'
                          className='form-control'
                          placeholder='Password Min Character'
                          onChange={(e) => handleChange(e)}
                          value={formData.passwordMinChar || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.passwordMinChar && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.passwordMinChar}</div>)}
                    </div>
                  </div>
                </div>
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Password Max Character:</label>
                      <div className='col-lg-10'>
                        <input
                          name='passwordMaxChar'
                          type='text'
                          className='form-control'
                          placeholder='Password Max Character'
                          onChange={(e) => handleChange(e)}
                          value={formData.passwordMaxChar || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.passwordMaxChar && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.passwordMaxChar}</div>)}
                    </div>
                  </div>
                </div>
              </div>
              <div className='row mb-6 mt-6'>
                <div className='col-lg-1' />
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Password Least Digits:</label>
                      <div className='col-lg-10'>
                        <input
                          name='passwordLeastDigits'
                          type='text'
                          className='form-control'
                          placeholder='Password Least Digits'
                          onChange={(e) => handleChange(e)}
                          value={formData.passwordLeastDigits || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.passwordLeastDigits && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.passwordLeastDigits}</div>)}
                    </div>
                  </div>
                </div>
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Previous Password Repeated Times:</label>
                      <div className='col-lg-10'>
                        <input
                          name='previousPasswordRepeatedTimes'
                          type='text'
                          className='form-control'
                          placeholder='Previous Password Repeated Times'
                          onChange={(e) => handleChange(e)}
                          value={formData.previousPasswordRepeatedTimes || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.previousPasswordRepeatedTimes && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.previousPasswordRepeatedTimes}</div>)}
                    </div>
                  </div>
                </div>
              </div>
              <div className='row mb-6 mt-6'>
                <div className='col-lg-1' />
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Password Expired Days:</label>
                      <div className='col-lg-10'>
                        <input
                          name='passwordExpiredDays'
                          type='text'
                          className='form-control'
                          placeholder='Password Expired Days'
                          onChange={(e) => handleChange(e)}
                          value={formData.passwordExpiredDays || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.passwordExpiredDays && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.passwordExpiredDays}</div>)}
                    </div>
                  </div>
                </div>
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Password Max Failure Attempts:</label>
                      <div className='col-lg-10'>
                        <input
                          name='passwordMaxFailureAttemt'
                          type='text'
                          className='form-control'
                          placeholder='Password Max Failure Attempts'
                          onChange={(e) => handleChange(e)}
                          value={formData.passwordMaxFailureAttemt || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.passwordMaxFailureAttemt && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.passwordMaxFailureAttemt}</div>)}
                    </div>
                  </div>
                </div>
              </div>
              <div className='row mb-6 mt-6'>
                <div className='col-lg-1' />
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Login Duration after Failure Attempt (in Mins):</label>
                      <div className='col-lg-10'>
                        <input
                          name='loginDurationAfterFailureAttemt'
                          type='text'
                          className='form-control'
                          placeholder='Login Duration after Failure Attempt'
                          onChange={(e) => handleChange(e)}
                          value={formData.loginDurationAfterFailureAttemt || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.loginDurationAfterFailureAttemt && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.loginDurationAfterFailureAttemt}</div>)}
                    </div>
                  </div>
                </div>
                <div className='col-lg-5'>
                  <div className="row">
                    <div className='col-lg-12 text-start'>
                      <label className='font-size-md fw-bolder col-form-label required'>Session Timeout:</label>
                      <div className='col-lg-10'>
                        <input
                          name='sessionTimeOut'
                          type='text'
                          className='form-control'
                          placeholder='Session Timeout'
                          onChange={(e) => handleChange(e)}
                          value={formData.sessionTimeOut || ''}
                          maxLength={3}
                          autoComplete='off'
                          onBlur={e => handleTrimSpaceOnly(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                      </div>
                      {errors && errors.sessionTimeOut && (<div className='rr mt-1'><style>{'.rr{color:red;}'}</style>{errors.sessionTimeOut}</div>)}
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-6' />
            <div className='col-lg-6'>
              <div className='col-lg-11'>
                <button
                  className='btn btn-blue m-2 fa-pull-right'
                  onClick={(event) => {
                    handleSubmit(event)
                  }}
                >
                  {loadingESC
                    ? (
                      <span
                        className='spinner-border spinner-border-sm mx-3'
                        role='status'
                        aria-hidden='true' />
                    )
                    : (
                      'Submit'
                    )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    siteConfigStore,
    editSiteConfigsStore,
  } = state;
  return {
    statusESC:
      editSiteConfigsStore &&
        editSiteConfigsStore.statusESC
        ? editSiteConfigsStore.statusESC
        : "",
    messageESC:
      editSiteConfigsStore &&
        editSiteConfigsStore.messageESC
        ? editSiteConfigsStore.messageESC
        : "",
    loadingESC:
      editSiteConfigsStore &&
        editSiteConfigsStore.loadingESC
        ? editSiteConfigsStore.loadingESC
        : false,
    siteConfigs:
      siteConfigStore &&
        siteConfigStore.siteConfigs
        ? siteConfigStore.siteConfigs
        : {},
    statusGSC:
      siteConfigStore &&
        siteConfigStore.statusGSC ?
        siteConfigStore.statusGSC : "",
    loadingGSC:
      siteConfigStore &&
        siteConfigStore.loadingGSC
        ? siteConfigStore.loadingGSC
        : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  editSiteconfigDispatch: (id, data) =>
    dispatch(editSiteConfigsActions.editSiteConfigs(id, data)),
  clearEditSiteconfigDispatch: () =>
    dispatch(editSiteConfigsActions.cleareditSiteConfigs()),
  getSiteconfigDispatch: () =>
    dispatch(siteConfigActions.getSiteConfig()),
  siteConfigActions: (data) => dispatch(siteConfigActions.getSiteConfig(data)),
  clearSiteconfigDispatch: () =>
    dispatch(siteConfigActions.clearSiteConfig()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SiteConfig);
