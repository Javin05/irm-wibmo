import _ from 'lodash'

export const setConfigData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      passwordMaxChar: data.passwordMaxChar,
      passwordMinChar: data.passwordMinChar,
      passwordLeastDigits: data.passwordLeastDigits,
      previousPasswordRepeatedTimes: data.previousPasswordRepeatedTimes,
      passwordExpiredDays: data.passwordExpiredDays,
      passwordMaxFailureAttemt: data.passwordMaxFailureAttemt,
      loginDurationAfterFailureAttemt: data.loginDurationAfterFailureAttemt,
      sessionTimeOut: data.sessionTimeOut
    }
  }
}
