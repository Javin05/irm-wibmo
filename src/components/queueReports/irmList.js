import React, { useEffect, useState, Fragment } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { riskManagementActions, queuesActions, HomeActions, QueueValuesActions } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { QUEUE_ROUTING } from '../../utils/constants'

function IRMList(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading,
    value,
    queuesLoading,
    HomeIdResponce,
    getQueueslistDispatch,
    getHomeDispatch,
    QueueValuesResponce,
    getQueueHomeDispatch
  } = props

  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getHomeDispatch()
    getQueueHomeDispatch()
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    // getRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      // getRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      // getRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.count) / limit)
      : 1

  return (
    <>
      <div className={`card md-10 ${className}`} >
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {riskmgmtlists && riskmgmtlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>QName</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("qName")}
                        >
                          <i
                            className={`bi ${sorting.qName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Approved</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Pending")}
                        >
                          <i
                            className={`bi ${sorting.Pending
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Hold</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Reject")}
                        >
                          <i
                            className={`bi ${sorting.Reject
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Rejected</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Hold")}
                        >
                          <i
                            className={`bi ${sorting.Hold
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Chargeback</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Pending")}
                        >
                          <i
                            className={`bi ${sorting.Pending
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Chargeback Amount</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Pending")}
                        >
                          <i
                            className={`bi ${sorting.Pending
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Refund</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Reject")}
                        >
                          <i
                            className={`bi ${sorting.Reject
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Refund Amount</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Reject")}
                        >
                          <i
                            className={`bi ${sorting.Reject
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Dispute</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Hold")}
                        >
                          <i
                            className={`bi ${sorting.Hold
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Dispute Amount</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Hold")}
                        >
                          <i
                            className={`bi ${sorting.Hold
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Reviewed</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Hold")}
                        >
                          <i
                            className={`bi ${sorting.Hold
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Reviewed Amount</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("Hold")}
                        >
                          <i
                            className={`bi ${sorting.Hold
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-7'>
                {
                  !queuesLoading
                    ? (
                      HomeIdResponce &&
                        HomeIdResponce.data
                        ? (
                          HomeIdResponce.data.map((item, i) => {
                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  <td className='ellipsis'>
                                    <Link
                                      to={`${QUEUE_ROUTING[item.queueName && item.queueName]}`}
                                    >
                                      {item.queueName ? item.queueName : "--"}
                                    </Link>
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalApprovedCount ? item.summary.totalApprovedCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalHoldCount ? item.summary.totalHoldCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalRejectedCount ? item.summary.totalRejectedCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalChargebackCount ? item.summary.totalChargebackCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalChargebackAmount ? item.summary.totalChargebackAmount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalRefundCount ? item.summary.totalRefundCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalRefundAmount ? item.summary.totalRefundAmount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalDisputeCount ? item.summary.totalDisputeCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalDisputeAmount ? item.summary.totalDisputeAmount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalReviewedCount ? item.summary.totalReviewedCount : '--'}
                                  </td>
                                  <td className='ellipsis'>
                                    {item && item.summary && item.summary.totalReviewedAmount ? item.summary.totalReviewedAmount : '--'}
                                  </td>
                                </tr>
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore, queueslistStore, HomeIdStore, QueueValuesActionsIdStore } = state;
  return {
    riskmgmtlists:
      state &&
      state.riskManagementlistStore &&
      state.riskManagementlistStore.riskmgmtlists,
    loading:
      riskManagementlistStore && riskManagementlistStore.loading ? riskManagementlistStore.loading : false,
    HomeIdResponce: HomeIdStore && HomeIdStore.HomeIdResponce ? HomeIdStore.HomeIdResponce : {},
    queuesLoading: queueslistStore && queueslistStore.loading ? queueslistStore.loading : false,
    QueueValuesResponce: QueueValuesActionsIdStore && QueueValuesActionsIdStore.QueueValuesResponce ? QueueValuesActionsIdStore.QueueValuesResponce : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) =>
    dispatch(riskManagementActions.getRiskManagementlist(params)),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  getHomeDispatch: (params) => dispatch(HomeActions.getHomelist(params)),
  getQueueHomeDispatch: (params) => dispatch(QueueValuesActions.getQueueValueslist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(IRMList)
