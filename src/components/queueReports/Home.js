import React, { useEffect, useState } from 'react'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import _ from 'lodash'
import { connect } from 'react-redux'
import IRMList from './irmList'
import clsx from 'clsx'
import { toAbsoluteUrl } from '../../theme/helpers'
import { queuesActions, riskManagementActions, TransactionActions, AMLqueueActions } from '../../store/actions'
import routeConfig from '../../routing/routeConfig'
import { useHistory } from 'react-router-dom'
import RiskManagementList from '../riskManagement/riskManagement'

function Home(props) {
  const {
    getQueues,
    getQueueslistDispatch,
    queuesLists,
    getRiskManagementlistDispatch,
    getTransactionDispatch,
    getAMLqueuelistDispatch,
  } = props
  const history = useHistory()

  useEffect(() => {
    getQueueslistDispatch()
  }, [])

  const [errors, setErrors] = useState()
  const [selectedQueuesOption, setSelectedQueuesOption] = useState('')
  const [selectedSearchOption, setSelectedSearchOption] = useState('')
  const [queuesOption, setQueuesOption] = useState()
  const [showTable, setShowTable] = useState(false)
  const [value, setValue] = useState('')
  const [searchValue, setSearchValue] = useState()
  const [searchNode, setSearchNode] = useState()
  const [QueueOption, setQueue] = useState()
  const [SelectedQueueOption, setSelectedQueueOption] = useState('')

  const [formData, setFormData] = useState({
    queues: '',
    mfilterit_IRM_Q: '',
    mfilterit_Chargeback_Q: ''
  })

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  useEffect(() => {
    if (showTable === 'SUSPECT_ACCOUNTS_Q') {
      setShowTable(true)
    } else if (showTable === 'mfilterit_Chargeback_Q') {
      setShowTable(false)
    }
  }, [showTable])

  const handleChange =(e) => {
    setSearchNode(e.target.value)
  }

  const handleChangeSearch = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedSearchOption(selectedOption)
      setSearchValue(selectedOption.value)
      setFormData((values) => ({ ...values, mfilterit_IRM_Q: selectedOption.value }))
    } else {
      setSelectedSearchOption()
      setFormData((values) => ({ ...values, queues: '' }))
    }
    setErrors({ ...errors, queues: '' })
  }

  const SearchOption = [
    { value: 'riskId', label: 'caseId' },
    { value: 'riskStatus', label: 'Status' },
    { value: 'deviceID', label: 'deviceID' },
    { value: 'personalEmail', label: 'personalEmail' },
    { value: 'ipAddress', label: 'ipAddress' },
    { value: 'phone', label: 'phone' }
  ]

  const SearchTXNOption = [
    { value: 'transactionId', label: 'TransactionId' },
    { value: 'emailAddress', label: 'EmailAddress' },
    { value: 'riskStatus', label: 'Status' },
    { value: 'deviceID', label: 'deviceID' },
    { value: 'ipAddress', label: 'ipAddress' },
    { value: 'phone', label: 'phone' }
  ]

  const handleChangeQueue = selectedOption => {
    if (selectedOption !== null) {
      setSelectedQueueOption(selectedOption)
      setShowTable(selectedOption.label)
      setValue(selectedOption.label)
    }
  }

  const queueNames = queuesLists && queuesLists.data
  useEffect(() => {
    const Queue = getDefaultOption(queueNames)
    setQueue(Queue)
  }, [queueNames])

  const getDefaultOption = (queueNames) => {
    const defaultOptions = []
    for (const item in queueNames) {
      defaultOptions.push({ label: queueNames[item].queueName, value: queueNames[item]._id })
    }
    return defaultOptions
  }

  const handleQueueClick = () => {
    if(value === "SUSPECT_ACCOUNTS_Q") {
      history.push(routeConfig.riskManagementSearch)
      const params ={
        [searchValue]: searchNode,
      }
      getRiskManagementlistDispatch(params)
    }
    if(value === "SUSPECT_TXN_Q") {
      history.push(routeConfig.transactionSearch)
      const params ={
        [searchValue]: searchNode,
      }
      getTransactionDispatch(params)
    }
    if(value === "AML_Q") {
      history.push(routeConfig.amlQueueSearch)
      const params ={
        [searchValue]: searchNode,
      }
      getAMLqueuelistDispatch(params)
    }
  }
  
  return (
    <div className='bg-pad pe-0'>
      <div className='card-header bg-black '>
        <div className='card-body'>
          <div className='container-fixed '>
            <div className='card-body'>
              <div className='row home-mb'>
                <div className='col-lg-4'>
                  <img
                    alt='Logo'
                    className='h-45px logo ms-4 mt-4'
                    // src={toAbsoluteUrl('/media/loginImage/MicrosoftTeams-image.png')}
                  src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
                  />
                </div>
                <div className='d-flex justify-content-center mt-15'>
                  <label className='font-size-xs font-weight-bold mb-3 mt-3 text-center text-white form-label mt-13'>
                    Select Queues : &nbsp;
                  </label>
                  <div className='col-lg-5 pr-3 me-2 mb-10 mt-10'>
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name='AppUserId'
                      className='select2'
                      classNamePrefix='select'
                      handleChangeReactSelect={handleChangeQueue}
                      options={QueueOption}
                      value={SelectedQueueOption}
                      isDisabled={!QueueOption}
                    />
                  </div>
                </div>
                {
                  (value === 'SUSPECT_ACCOUNTS_Q')  || (value === 'AML_Q') ? (
                    <div className='d-flex justify-content-center'>
                      <label className='font-size-xs font-weight-bold mb-3 mt-3 text-center text-white form-label'>
                        Search : &nbsp;
                      </label>
                      <div className='col-lg-2 pr-3 me-2'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name="queues"
                          className="basic-single"
                          classNamePrefix="select"
                          handleChangeReactSelect={handleChangeSearch}
                          options={SearchOption}
                          value={selectedSearchOption}
                        />
                      </div>
                      <div className='col-lg-2 me-2'>
                        <input
                          placeholder='Search'
                          className={clsx(
                            'form-control form-control-md form-control-solid'
                          )}
                          onChange={(e) => handleChange(e)}
                          type='personalEmail'
                          name='personalEmail'
                          autoComplete='off'
                        />
                      </div>
                      <button
                        type='button'
                        className='btn btn-sm btn-light-primary fa-pull-right mt-1'
                        data-dismiss='modal'
                      onClick={() => {
                        handleQueueClick()
                      }}
                      >
                        Search
                      </button>
                    </div>
                  ) : null
                }
                {
                   (value === 'SUSPECT_TXN_Q') ? (
                    <div className='d-flex justify-content-center'>
                      <label className='font-size-xs font-weight-bold mb-3 mt-3 text-center text-white form-label'>
                        Search : &nbsp;
                      </label>
                      <div className='col-lg-2 pr-3 me-2'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name="queues"
                          className="basic-single"
                          classNamePrefix="select"
                          handleChangeReactSelect={handleChangeSearch}
                          options={SearchTXNOption}
                          value={selectedSearchOption}
                        />
                      </div>
                      <div className='col-lg-2 me-2 mb-4'>
                        <input
                          placeholder='Search'
                          className={clsx(
                            'form-control form-control-md form-control-solid'
                          )}
                          onChange={(e) => handleChange(e)}
                          type='personalEmail'
                          name='personalEmail'
                          autoComplete='off'
                        />
                      </div>
                      <div className='mt-2'>
                      <button
                        type='button'
                        className='btn btn-sm btn-light-primary'
                      onClick={() => {
                        handleQueueClick()
                      }}
                      >
                        Search
                      </button>
                    </div>
                    </div>
                  ) : null
                }

              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='mb-0 home-body '>
        {/* {showTable ? ( */}
        <IRMList value={value} />
        {/* ) : null} */}
      </div>
    </div>
  )

}

const mapStateToProps = state => {
  const { queueslistStore } = (state)
  return {
    getQueues: state && state.bankStore && state.bankStore.getBank,
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params)),
  getTransactionDispatch: (params) => dispatch(TransactionActions.getTransactionlist(params)),
  getAMLqueuelistDispatch: (params) => dispatch(AMLqueueActions.getAMLqueuelist(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)