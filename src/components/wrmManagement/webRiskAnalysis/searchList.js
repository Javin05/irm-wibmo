import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../../theme/helpers'
import _, { values } from 'lodash'
import {
  getWebAnalysisActions,
  WrmRiskManagementActions,
  ExportListActions,
  WrmStatusActions,
  BWListActions,
  GetClientsActions,
  riskLevelActions
} from '../../../store/actions'
import './styles.css';
import Modal from 'react-bootstrap/Modal'
import { searchValidation } from './validation'
import clsx from 'clsx'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper';
import FindRole from '../Role'
import { DateSelector } from '../../../theme/layout/components/DateSelector'
import { DATE, SET_FILTER } from '../../../utils/constants'
import moment from "moment"
import { addDays, subDays } from 'date-fns';

function SearchList(props) {
  const {
    getRiskMgmtColumns,
    getWebAnalysisDispatch,
    getWrmRiskManagementlistDispatch,
    clinetIdLists,
    getExportDispatch,
    Value,
    setexportShow,
    getWrmStatusDispatch,
    getBlackWhiteLDispatch,
    getClientsWrmDispatch,
    getRiskLevelListDispatch,
    riskLevelList,
    setReportShow,
    setFilter,
    limit,
    setSearchParams
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [buil, setBUild] = useState(false);
  const [dataValue, setDataValue] = useState({});
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const headClientId = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))     
  const [manualFormData, setManualFormData] = useState({
    caseId:'',
    website: '',
    tag: '',
    acquirer: "",
    riskStatus: '',
    reportStatus: '',
    pma: '',
    createdAtFrom: '',
    createdAtTo: '',
    riskLevel: '',
    acquirer: ''
  })
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const PMAdropdown = [
    {
      "label": "Website is not working",
      "value": "Website is not working",

    },
    {
      "label": "Login Credentials are required",
      "value": "Login Credentials are required",

    },
    {
      "label": "Pricing is not updated",
      "value": "Pricing is not updated",

    },
    {
      "label": "Pricing is in Dollars",
      "value": "Pricing is in Dollars",

    },
    {
      "label": "Heavy Discounts",
      "value": "Heavy Discounts",

    },
    {
      "label": "Website Redirection",
      "value": "Website Redirection",
    },
    {
      "label": "Page Navigation Issue",
      "value": "Page Navigation Issue",
    },
    {
      "label": "No Data",
      "value": "No Data",
    }
  ]
  
  const handleSearch = () => {
    // const errorMsg = searchValidation(manualFormData, setError)
    // if (_.isEmpty(errorMsg)) {

    const UpDateFrom = moment(manualFormData.createdAtFrom).format("YYYY-MM-DD")
    const UpDateTo = moment(manualFormData.createdAtTo).format("YYYY-MM-DD")
    if (moment(UpDateFrom).isAfter(UpDateTo)) {
      setError({
        createdDate: "From Date Should Be Less Than To Date",
      });
      return setShow(true);
    }
    const params = {
      limit:limit ? limit :'',
      page: 1,
      riskId: manualFormData.caseId,
      website: manualFormData.website,
      tag: manualFormData.tag,
      acquirer: manualFormData.acquirer,
      riskStatus: manualFormData.riskStatus,
      reportStatus: manualFormData.reportStatus,
      pma: manualFormData.pma,
      createdAtFrom: UpDateFrom === 'Invalid date' ? '' : UpDateFrom,
      createdAtTo: UpDateFrom === 'Invalid date' ? '' : UpDateTo,
      riskLevel: manualFormData.riskLevel,
      acquirer: manualFormData.acquirer,
      clientId: ClinetId ? ClinetId : headClientId ? headClientId : '' ,
    }
    setSearchParams(params)
    if(!_.isEmpty(headClientId) || !_.isEmpty(manualFormData.tag) || !_.isEmpty(manualFormData.website) || !_.isEmpty(ClinetId)){
      setReportShow(true)
    }else{
      setReportShow(false)
    }
    setexportShow(true)
    setShow(false)
    // setExportBtn(true)
    // getWebAnalysisDispatch(params)
    setFilter(params)
    getWrmRiskManagementlistDispatch(params)
    getWrmStatusDispatch(params)
    setDataValue(params)
    setLocalStorage('ExportHide', JSON.stringify(
      !_.isEmpty(params && params) ? params : ''
    ))
    setLocalStorage('TAG', JSON.stringify(
      params
    ))
    setLocalStorage('WEBSITSEARCH', JSON.stringify(
      params
    ))
    const data = {
      tag: manualFormData.tag,
    }
    getBlackWhiteLDispatch(data)
    getClientsWrmDispatch(params)
    // }
  }


  const clearPopup = () => {
    setShow(false)
  }

  const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))
  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
      website: tagSearch.website,
      tag: tagSearch.tag,
      riskStatus: tagSearch.riskStatus,
      reportStatus: tagSearch.reportStatus,

    }))
    show && getRiskLevelListDispatch()
  }, [show])
  
  useEffect(() => {
    if (Value === true) {
      setManualFormData(values => ({
        ...values,
        website: '',
        acquirer: '',
        tag: '',
        riskStatus: '',
        reportStatus: '',
        createdAtFrom: '',
        createdAtTo: ''
      }))
    } else {
      setBUild(true)
    }
  }, [Value])
  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-7vw me-3 pull-right'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search & Export
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
          <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3 form-label">
                  CaseId :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='CaseId'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.caseId && error.caseId },
                    {
                      'is-valid': manualFormData.caseId && !error.caseId
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  onBlur={e => handleTrimWhiteSpace(e, setManualFormData)}
                  type='text'
                  name='caseId'
                  autoComplete='off'
                  value={manualFormData.caseId || ''}
                />
                {/* {error && error.caseId && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.caseId}
                  </div>
                )} */}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3 form-label">
                  Tag :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Tag'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.tag && error.tag },
                    {
                      'is-valid': manualFormData.tag && !error.tag
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  onBlur={e => handleTrimWhiteSpace(e, setManualFormData)}
                  type='text'
                  name='tag'
                  autoComplete='off'
                  value={manualFormData.tag || ''}
                />
                {/* {error && error.tag && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.tag}
                  </div>
                )} */}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Acquirer :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Acquirer'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.acquirerag && error.acquirer },
                    {
                      'is-valid': manualFormData.acquirer && !error.acquirer
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  onBlur={e => handleTrimWhiteSpace(e, setManualFormData)}
                  type='text'
                  name='acquirer'
                  autoComplete='off'
                  value={manualFormData.acquirer || ''}
                />
                {/* {error && error.tag && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.tag}
                  </div>
                )} */}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Website :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Website'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.website && error.website },
                    {
                      'is-valid': manualFormData.website && !error.website
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type='text'
                  name='website'
                  autoComplete='off'
                  value={manualFormData.website || ''}
                />
                {error && error.website && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.website}
                  </div>
                )}
              </div>
            </div>
            {/* <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Created Date From :
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='createdAtFrom'
                  placeholder='Uploaded Date From'
                  className='form-control'
                  selected={manualFormData.createdAtFrom || ''}
                  onChange={(date) => {
                    setManualFormData((values) => ({
                      ...values,
                      createdAtFrom: date
                    }))
                    setStartDate(date)
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  maxDate={new Date()}
                  isClearable={true}
                  selectsStart
                  startDate={startDate}
                  endDate={endDate}

                />
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Created Date To :
                </label>
              </div>
              <div className='col-md-8'>
                <DateSelector
                  name='createdAtTo'
                  placeholder='Uploaded Date To'
                  className='form-control'
                  selected={manualFormData.createdAtTo || ''}
                  onChange={(date) => {
                    setManualFormData((values) => ({
                      ...values,
                      createdAtTo: date
                    }))
                    setEndDate(date)
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  maxDate={new Date()}
                  isClearable={true}
                  selectsEnd
                  startDate={startDate}
                  endDate={endDate}
                  minDate={startDate}
                  excludeDates={[new Date(), subDays(startDate, 0)]}
                  // showDisabledMonthNavigation
                  disable={startDate}

                />
              </div>
            </div> */}
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Created date :
                </label>
              </div>
              <div className="col-md-4">
                <DateSelector
                  name="createdAt"
                  placeholder="Created Date From"
                  className="form-control"
                  selected={manualFormData.createdAtFrom || ""}
                  onChange={(date) => {
                    setManualFormData((values) => ({
                      ...values,
                      createdAtFrom: date,
                    }));
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
              <div className="col-md-4">
                <DateSelector
                  name="createdAt"
                  placeholder="Created Date To"
                  className="form-control"
                  selected={manualFormData.createdAtTo || ""}
                  onChange={(date) => {
                    setManualFormData((values) => ({
                      ...values,
                      createdAtTo: date,
                    }));
                  }}
                  dateFormat={DATE.DATE_FOR_PICKER}
                  maxDate={new Date()}
                  isClearable={true}
                  peek={true}
                  monthDropdown={true}
                  yearDropdown={true}
                  showYear={true}
                />
              </div>
              {error && error.createdDate && (
                <div className="rr mt-1 text-justify">
                  <style>{".rr{color:red}"}</style>
                  {error.createdDate}
                </div>
              )}
            </div>
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Risk level :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name="riskLevel"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.riskLevel || ""}
                >
                  <option value="">Select...</option>
                  {riskLevelList.map((riskitem) => (
                    <option
                      value={riskitem.fieldValue}
                      key={riskitem.fieldValue}
                    >
                      {riskitem.fieldName}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <FindRole
              role={Role}
            >
              <div className="row mb-8">
                <div className='col-md-4'>
                  <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Category Status :
                  </label>
                </div>
                <div className='col-md-8'>
                  <select
                    name='riskStatus'
                    className='form-select form-select-solid'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleChanges(e)}
                    value={manualFormData.riskStatus || ''}
                  >
                    <option value=''>Select...</option>
                    <option value='APPROVED'>APPROVED</option>
                    <option value='REJECTED'>REJECTED</option>
                    <option value='MANUAL REVIEW'>MANUAL REVIEW</option>
                    <option value='PENDING'>PENDING</option>
                  </select>
                </div>
              </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Status :
                </label>
              </div>
              <div className='col-md-8'>
                <select
                  name='reportStatus'
                  className='form-select form-select-solid'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.reportStatus || ''}
                >
                  <option value=''>Select...</option>
                  <option value='REJECTED'>REJECTED</option>
                  <option value='PENDING'>PENDING</option>
                  <option value='QUEUED'>QUEUED</option>
                  <option value='DATA CAPTURED'>DATA CAPTURED</option>
                  <option value='COMPLETED'>COMPLETED</option>
                  <option value='TAG PROCESSING'>TAG PROCESSING</option>
                  <option value='TAG CAPTURED'>TAG CAPTURED</option>
                  <option value='WAITING FOR REPORT'>WAITING FOR REPORT</option>
                </select>
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  PMA :
                </label>
              </div>
              <div className='col-md-8'>
                <select
                  name='pma'
                  className='form-select form-select-solid'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  options={PMAdropdown}
                  value={manualFormData.pma || ''}
                >
                  <option value=''>Select...</option>
                  {PMAdropdown.map((ele, i) => <option key={i} value={`${ele.value}`}>{`${ele.label}`}</option>)}

                </select>
              </div>
            </div>
            </FindRole>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  className='btn btn-light-primary m-1 mt-8 font-5vw '
                  onClick={handleSearch}>
                  Search
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    clinetListStore,
    RiskLevelStore
  } = state
  return {
    loading: state && state.riskManagementlistStore && state.riskManagementlistStore.loading,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    loadingRL: state && state.RiskLevelStore && state.RiskLevelStore.loadingRL,
    riskLevelList: RiskLevelStore && RiskLevelStore.riskLevelList ? RiskLevelStore.riskLevelList : [],
  }
}

const mapDispatchToProps = dispatch => ({
  getWebAnalysisDispatch: (params) => dispatch(getWebAnalysisActions.getgetWebAnalysislist(params)),
  getWrmRiskManagementlistDispatch: (params) => dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  getWrmStatusDispatch: (params) => dispatch(WrmStatusActions.getWrmStatus(params)),
  getBlackWhiteLDispatch: (params) => dispatch(BWListActions.getBWList(params)),
  getClientsWrmDispatch: (data) => dispatch(GetClientsActions.getClientsWrm(data)),
  getRiskLevelListDispatch: () => dispatch(riskLevelActions.getRiskLevelList()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)