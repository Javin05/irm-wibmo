import React, { useState, useEffect, Fragment } from "react"
import Modal from 'react-bootstrap/Modal'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  updateImageUploadActions,
} from '../../store/actions'
import { DROPZONE_MESSAGES, FILE_UPLOAD_TYPE, STATUS_RESPONSE } from '../../utils/constants'
import { successAlert, warningAlert } from "../../utils/alerts"

const UploadImage = (props) => {
  const {
    id,
    postImageUpdateDispatch,
    imageUploadResponse,
    clearImageUpdate,
    loading
  } = props
  const [show, setShow] = useState(false)
  const clearValues = () => {
    setLogoImage('')
    setContactUsImage('')
    setPrivacyPolicyImage('')
    setConditionPolicyImage('')
    setShippingPolicyImage('')
    setReturnPolicyImage('')
  }
  const [errors, setErrors] = useState({})
  const [logoName, setLogoName] = useState()
  const [contactUsName, setContactUsName] = useState()
  const [privacyPolicy, setPrivacyPolicyName] = useState()
  const [conditionPolicy, setConditionPolicyName] = useState()
  const [shippingPolicy, setShippingPolicyName] = useState()
  const [returnPolicy, setReturnPolicyName] = useState()
  const [logoImage, setLogoImage] = useState()
  const [contactUsImageName, setContactUsImage] = useState()
  const [privacyPolicyImage, setPrivacyPolicyImage] = useState()
  const [conditionPolicyImage, setConditionPolicyImage] = useState()
  const [shippingPolicyImage, setShippingPolicyImage] = useState()
  const [returnPolicyImage, setReturnPolicyImage] = useState()
  const [logoDisable, setLogoDisable] = useState(false)
  const [contactDisable, setContactDisable] = useState(false)
  const [privacyDisable, setPrivacyDisable] = useState(false)
  const [conditionDisable, setConditionDisable] = useState(false)
  const [shippingDisable, setShippingDisable] = useState(false)
  const [returnDisable, setReturnDisable] = useState(false)


  useEffect(() => {
    if(imageUploadResponse && imageUploadResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        imageUploadResponse && imageUploadResponse.message,
        'success'
      )
      setLogoDisable(false)
      setContactDisable(false)
      setPrivacyDisable(false)
      setConditionDisable(false)
      setShippingDisable(false)
      setReturnDisable(false)
      setLogoImage(logoName?logoName:'')
      setErrors({Logo:''})
      setContactUsImage(contactUsName?contactUsName:'')
      setErrors({contactUs:''})
      setPrivacyPolicyImage(privacyPolicy?privacyPolicy:'')
      setErrors({privacyPolicy:''})
      setConditionPolicyImage(conditionPolicy?conditionPolicy:'')
      setErrors({conditionPolicy:''})
      setShippingPolicyImage(shippingPolicy?shippingPolicy:'')
      setErrors({shippingPolicy:''})
      setReturnPolicyImage(returnPolicy?returnPolicy:'')
      setErrors({returnPolicy:''})
      clearImageUpdate()
    } else if (imageUploadResponse && imageUploadResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      setLogoDisable(false)
      setContactDisable(false)
      setPrivacyDisable(false)
      setConditionDisable(false)
      setShippingDisable(false)
      setReturnDisable(false)
      warningAlert(
        'error',
        imageUploadResponse && imageUploadResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearImageUpdate()
    }
  },[imageUploadResponse])

  const handleFileChange = (e) => {
      e.preventDefault()
      const { name } = e.target
      let isValidFileFormat = true
      const maxFileSize = 5
      const files = e.target.files[0]
      const fileType = files && files.type
      const uploadedFileSize = files && files.size
      isValidFileFormat = _.includes(FILE_UPLOAD_TYPE, fileType)
      const fileSize = Number(maxFileSize) * 1024 * 1024
      if (isValidFileFormat) {
        if (uploadedFileSize) {
          if (name == 'Logo') {
            setLogoDisable(true)
            const data = new FormData()
            data.append('type', 'Logo')
            data.append('file', files)
            postImageUpdateDispatch(id, data)
            setLogoName(files && files.name)
          } else if (name == 'contactUs') {
              setContactDisable(true)
              const data = new FormData()
              data.append('type', 'Contact')
              data.append('file', files)
              postImageUpdateDispatch(id, data)
              setContactUsName(files && files.name)
          } else if (name == 'privacyPolicy') {
              setPrivacyDisable(true)
              const data = new FormData()
              data.append('type', 'Privacy')
              data.append('file', files)
              postImageUpdateDispatch(id, data)
              setPrivacyPolicyName(files && files.name)
          } else if (name == 'conditionPolicy') {
              setConditionDisable(true)
              const data = new FormData()
              data.append('type', 'Terms')
              data.append('file', files)
              postImageUpdateDispatch(id, data)
              setConditionPolicyName(files && files.name)
          } else if (name == 'shippingPolicy') {
              setShippingDisable(true)
              const data = new FormData()
              data.append('type', 'Shipping')
              data.append('file', files)
              postImageUpdateDispatch(id, data)
              setShippingPolicyName(files && files.name)
          } else if (name == 'returnPolicy') {
              setReturnDisable(true)
              const data = new FormData()
              data.append('type', 'Return')
              data.append('file', files)
              postImageUpdateDispatch(id, data)
              setReturnPolicyName(files && files.name)
          }
          setErrors((values) => ({ ...values, file: "" }))
        } else {
          setErrors({
            ...errors,
            [name]: `File size must below ${fileSize / 1048576
              } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
          })
        }
      } else {
        setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID })
        setLogoDisable(false)
        setContactDisable(false)
        setPrivacyDisable(false)
        setConditionDisable(false)
        setShippingDisable(false)
        setReturnDisable(false)
      }
  }

  const handleClickLogo = (event) => {
    document.getElementById('Logo').click(event)
  }
  const handleClickContactUs = (event) => {
    document.getElementById('contactUs').click(event)
  }
  const handleClickPrivacy = (event) => {
    document.getElementById('privacyPolicy').click(event)
  }
  const handleClickCondition = (event) => {
    document.getElementById('conditionPolicy').click(event)
  }
  const handleClickShipping = (event) => {
    document.getElementById('shippingPolicy').click(event)
  }  
  const handleClickReturn = (event) => {
    document.getElementById('returnPolicy').click(event)
  }
  
  return (
    <Fragment>
      <a className="btn btn-sm btn-light-primary fw-bolder px-4 me-1"
        onClick={() => { setShow(true) }}>
        <i className="bi bi-file-image mr-1"></i>
        Upload Images
      </a>
      <Modal
        show={show}
        onHide={() => {
          setShow(false)
          clearValues()
          setErrors({})
        }}
        className="price-check-modal"
        size="md"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Upload Images</Modal.Title>
        </Modal.Header>
        <Modal.Body className="pt-2">
          <Fragment>
            <div className="row mb-8 align-items-center">
              <div className='col-md-8'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Logo :
                </label>
              </div>
              <div className='col-md-4'>
                <input
                  type="file"
                  className="d-none"
                  name="Logo"
                  id="Logo"
                  multiple={true}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClickLogo}
                  disabled={logoDisable}
                >
                  {logoDisable ? <span className="spinner-border spinner-border-sm mr-1"></span> :
                    <><i className="bi bi-filetype-csv" />
                      Browse
                    </>
                  }
                </button>
              </div>
              <div className='col-md-12 text-end text-success'>
                {logoImage && logoImage}
              </div>
              {errors && errors.Logo && (
                <div className="rr col-md-12 mt-1">
                  <style>{".rr{color:red}"}</style>
                  {errors.Logo}
                </div>
              )}
            </div>
            <div className="row mb-8 align-items-center">
              <div className='col-md-8'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Contact Us Screenshot :
                </label>
              </div>
              <div className='col-md-4'>
                <input
                  type="file"
                  className="d-none"
                  name="contactUs"
                  id="contactUs"
                  multiple={true}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClickContactUs}
                  disabled={contactDisable}
                >
                  {contactDisable ? <span className="spinner-border spinner-border-sm mr-1"></span> :
                    <><i className="bi bi-filetype-csv" />
                      Browse
                    </>
                  }
                </button>
              </div>
              <div className='col-md-12 text-end text-success'>
                {contactUsImageName && contactUsImageName}
              </div>
              {errors && errors.contactUs && (
                <div className="rr col-md-12 mt-1">
                  <style>{".rr{color:red}"}</style>
                  {errors.contactUs}
                </div>
              )}
            </div>
            <div className="row mb-8 align-items-center">
              <div className='col-md-8'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Privacy Policy Screenshot :
                </label>
              </div>
              <div className='col-md-4'>
                <input
                  type="file"
                  className="d-none"
                  name="privacyPolicy"
                  id="privacyPolicy"
                  multiple={true}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClickPrivacy}
                  disabled={privacyDisable}
                >
                  {privacyDisable ? <span className="spinner-border spinner-border-sm mr-1"></span> :
                    <><i className="bi bi-filetype-csv" />
                      Browse
                    </>
                  }
                </button>
              </div>
              <div className='col-md-12 text-end text-success'>
                {privacyPolicyImage && privacyPolicyImage}
              </div>
              {errors && errors.privacyPolicy && (
                <div className="rr col-md-12 mt-1">
                  <style>{".rr{color:red}"}</style>
                  {errors.privacyPolicy}
                </div>
              )}
            </div>
            <div className="row mb-8 align-items-center">
              <div className='col-md-8'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Terms & Condition Policy Screenshot :
                </label>
              </div>
              <div className='col-md-4'>
                <input
                  type="file"
                  className="d-none"
                  name="conditionPolicy"
                  id="conditionPolicy"
                  multiple={true}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClickCondition}
                  disabled={conditionDisable}
                >
                  {conditionDisable ? <span className="spinner-border spinner-border-sm mr-1"></span> :
                    <><i className="bi bi-filetype-csv" />
                      Browse
                    </>
                  }
                </button>
              </div>
              <div className='col-md-12 text-end text-success'>
                {conditionPolicyImage && conditionPolicyImage}
              </div>
              {errors && errors.conditionPolicy && (
                <div className="rr col-md-12 mt-1">
                  <style>{".rr{color:red}"}</style>
                  {errors.conditionPolicy}
                </div>
              )}
            </div>
            <div className="row mb-8 align-items-center">
              <div className='col-md-8'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Shipping Policy Screenshot :
                </label>
              </div>
              <div className='col-md-4'>
                <input
                  type="file"
                  className="d-none"
                  name="shippingPolicy"
                  id="shippingPolicy"
                  multiple={true}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClickShipping}
                  disabled={shippingDisable}
                >
                  {shippingDisable ? <span className="spinner-border spinner-border-sm mr-1"></span> :
                    <><i className="bi bi-filetype-csv" />
                      Browse
                    </>
                  }
                </button>
              </div>
              <div className='col-md-12 text-end text-success'>
                {shippingPolicyImage && shippingPolicyImage}
              </div>
              {errors && errors.shippingPolicy && (
                <div className="rr mt-1 col-md-12">
                  <style>{".rr{color:red}"}</style>
                  {errors.shippingPolicy}
                </div>
              )}
            </div>
            <div className="row mb-8 align-items-center">
              <div className='col-md-8'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Return Policy Screenshot :
                </label>
              </div>
              <div className='col-md-4'>
                <input
                  type="file"
                  className="d-none"
                  name="returnPolicy"
                  id="returnPolicy"
                  multiple={true}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClickReturn}
                  disabled={returnDisable}
                >
                  {returnDisable ? <span className="spinner-border spinner-border-sm mr-1"></span> :
                    <><i className="bi bi-filetype-csv" />
                      Browse
                    </>
                  }
                </button>
              </div>
              <div className='col-md-12 text-end text-success'>
                {returnPolicyImage && returnPolicyImage}
              </div>
              {errors && errors.returnPolicy && (
                <div className="rr mt-1 col-md-12">
                  <style>{".rr{color:red}"}</style>
                  {errors.returnPolicy}
                </div>
              )}
            </div>
          </Fragment>
        </Modal.Body>
      </Modal>
    </Fragment>
  )
}

const mapStateToProps = state => {
  const {
    updateImageUploadStore,
  } = state

  return {
    imageUploadResponse: updateImageUploadStore && updateImageUploadStore.updateImageUploadResponce ? updateImageUploadStore.updateImageUploadResponce : '',
  }
}
const mapDispatchToProps = dispatch => ({
  postImageUpdateDispatch: (id, data) => dispatch(updateImageUploadActions.updateImageUpload(id, data)),
  clearImageUpdate:() => dispatch(updateImageUploadActions.clearImageUpload()),
  loading: () => dispatch(updateImageUploadActions.saveImageUploadResponse())
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadImage);