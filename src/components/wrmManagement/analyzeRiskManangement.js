import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  WrmRiskManagementActions,
  ExportListActions,
  BlockListEmailActions,
  PostCategoryActions,
  BWListActions,
  clientCredFilterActions,
  WrmStatusActions,
  GetClientsActions
} from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage, getLocalStorage, removeLocalStorage } from '../../utils/helper'
import { RISKSTATUS, REVIEWTYPE, SET_FILTER, ADMINVALIDATE } from '../../utils/constants'
import WebRiskAnalysis from './webRiskAnalysis/WebRiskAnalysis'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import BlockListCategory from './blockListCategory'
import { STATUS_RESPONSE } from '../../utils/constants'
import PriceCheckPopup from '../shared-components/PriceCheckPopup'
import FindRole from './Role'
import BlockListCategory1 from './blockListCategory1'
import PMAcategory from './PMA'
import UploadImage from './uploadImage'
import StatusChange from './statusChange'
import moment from "moment"
import Status from './ARH'

function WrmRiskManagement(props) {
  const {
    getWrmRiskManagementlistDispatch,
    className,
    WrmRiskManagement,
    loading,
    exportLists,
    getExportDispatch,
    exportLoading,
    getBlockListTypeDispatch,
    PostCategory,
    BlockListTypes,
    getBlackWhiteLDispatch,
    BlackWhiteDataList,
    postPriceCheckSuccess,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    postPMAres,
    getWrmStatusDispatch,
    WrmStatusRes,
    WrmStatusloading,
    ClearWrmStatusDispatch,
    GetClientsRes,
    clearClientsWrmDispatch
  } = props

  const Client = [
    "KYC User",
    "Client User",
    "Phonepe User"
  ]
  const location = useLocation()
  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentRoute = url && url[1]
  const [routeShow, setRouteShow] = useState()
  const [riskIdValue, setRiskIdValue] = useState()
  const [exportShow, setexportShow] = useState(false)
  const [exportBtn, setExportBtn] = useState(false)
  const [blockListValue, setblockListValue] = useState()
  const [showBlockListClient, setShowBlockListClient] = useState(false)
  const [blacklistDropdown, setBlacklistDropdown] = useState(null)
  const [Value, setValue] = useState(false)
  const [showPma, setShowPma] = useState(false)
  const ClientName = JSON.parse(getLocalStorage("CLIENTNAME"))
  const ClinetId = JSON.parse(getLocalStorage("CLIENTID"))
  const [Webshow, setWebShow] = useState(false)
  const csvReport = React.useRef()
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [filter, setFilter] = useState('')
  const [sortingParams, setSortingParams] = useState()
  const [checked, setChecked] = useState(false)
  const [actionIds, setActionIds] = useState([])
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false,
    riskClassifcation: false,
    riskScore: false,
  })
  const [manualFormData, setManualFormData] = useState({
    website: '',
    tag: '',
    riskStatus: '',
    reportStatus: ''
  })

  useEffect(() => {
    if (location.state) {
      const data = location.state
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      const credBasedParams = {
        clientId: credBasedClientValue
      }
      const params = {
        ...sortingParams,
        limit: limit,
        page: activePageNumber,
        ...credBasedParams,
        ...searchParams,
      }
      Object.assign(params, data)
      const pickByParams = _.pickBy(params)
      getWrmRiskManagementlistDispatch(pickByParams)
      // getWrmStatusDispatch(pickByParams)
      // const bloackListParams = {
      //   skipPagination: 'true',
      //   fieldType: 'Website_Category',
      //   queueId: "624fc67fae69dc1e03f47ebd"
      // }
      // getBlockListTypeDispatch(bloackListParams)
      // setexportShow(false)
      // getBlackWhiteLDispatch()
      // removeLocalStorage('TAG')
      // setShowBlockListClient(false)
    } else {
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      const credBasedParams = {
        clientId: credBasedClientValue
      }
      const params = {
        ...sortingParams,
        limit: limit,
        page: activePageNumber,
        ...credBasedParams,
        ...searchParams,
      }
      const pickByParams = _.pickBy(params)
      getWrmRiskManagementlistDispatch(pickByParams)
      getWrmStatusDispatch(pickByParams)
      const bloackListParams = {
        skipPagination: 'true',
        fieldType: 'Website_Category',
        queueId: "624fc67fae69dc1e03f47ebd"
      }
      getBlockListTypeDispatch(bloackListParams)
      setexportShow(false)
      getBlackWhiteLDispatch()
      removeLocalStorage('TAG')
      setShowBlockListClient(false)
    }
  }, [location.state])

  const handleCheckboxChange = (id) => {
    setChecked(!checked);
    if (actionIds.includes(id)) {
      setActionIds(actionIds.filter((item) => item !== id))
    } else {
      setActionIds([...actionIds, id])
    }
  }

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      setShowBlockListClient(true)
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        ...sortingParams,
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      const pickByParams = _.pickBy(params)
      getWrmRiskManagementlistDispatch(pickByParams)
      getWrmStatusDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
      const data = {
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      getBlackWhiteLDispatch(!_.isEmpty(data && data.clientId) ? data : null)
    }
  }, [setFilterFunction, setCredFilterParams])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      ...sortingParams,
      ...searchParams,
      ...credBasedParams,
      limit: value,
      page: 1,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params)
    getWrmRiskManagementlistDispatch(pickByParams)
    getWrmStatusDispatch(pickByParams)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      ...searchParams,
      limit: limit,
      page: pageNumber,
      tag: paginationSearch.tag ? paginationSearch.tag : '',
      riskStatus: paginationSearch.riskStatus ? paginationSearch.riskStatus : '',
      reportStatus: paginationSearch.reportStatus ? paginationSearch.reportStatus : '',
      createdAtFrom: paginationSearch.createdAtFrom ? paginationSearch.createdAtFrom : '',
      createdAtTo: paginationSearch.createdAtTo ? paginationSearch.createdAtTo : '',
      riskLevel: paginationSearch.riskLevel ? paginationSearch.riskLevel : '',
      ...credBasedParams,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params)
    setActivePageNumber(pageNumber)
    getWrmRiskManagementlistDispatch(pickByParams)
    getWrmStatusDispatch(pickByParams)
  }

  const handleSorting = (name) => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        ...sortingParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC',
        clientId: credBasedClientValue ? credBasedClientValue : "",
      }
      setSortingParams(params)
      getWrmRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        ...sortingParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC',
        clientId: credBasedClientValue ? credBasedClientValue : "",
      }
      setSortingParams(params)
      getWrmRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count
      ? Math.ceil(parseInt(WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = '/merchant-login'
  }

  const reportCount = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.reportStatus
  const riskStatusCount = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.riskStatus
  const riskScoreStatus = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.riskScoreStatus
  const exportHide = JSON.parse(getLocalStorage('ExportHide'))
  const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))


  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === 'ok') {
      setexportShow(true)
      setExportBtn(true)
    }
  }, [exportLists])

  useEffect(() => {
    return (
      setexportShow(false),
      setValue(true),
      setExportBtn(false),
      setTimeout(() => {
        setValue(false)
      }, 1500)
    )
  }, [])

  const exportData = ((data) => {
    const params = {
      id: data
    }
    getExportDispatch(params)
  })

  const getIdValue = ((value, riskId) => {
    setblockListValue(value)
    setRiskIdValue(riskId)
  })


  useEffect(() => {
    if (
      PostCategory && PostCategory.status === STATUS_RESPONSE.SUCCESS_MSG ||
      postPriceCheckSuccess && postPriceCheckSuccess.status == STATUS_RESPONSE.SUCCESS_MSG
    ) {
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber
      }
      getWrmRiskManagementlistDispatch(tagSearch)
    }
  }, [PostCategory, postPriceCheckSuccess])

  useEffect(() => {
    if (BlackWhiteDataList) {
      const blackWhiteData = BlackWhiteDataList && _.isArray(BlackWhiteDataList.data) ? BlackWhiteDataList.data : []
      let blacktemp = []
      Object.keys(blackWhiteData).forEach(function (key) {
        blacktemp.push({
          "value": blackWhiteData[key].fieldValue,
          "label": blackWhiteData[key].fieldValue,
        })
      })

      blacktemp.push({
        "value": "Others",
        "label": "Others",
      })
      setBlacklistDropdown(blacktemp)
    }
  }, [BlackWhiteDataList])

  const Website = ((name) => {
    window.open(name)
  })

  const hadelRefresh = (() => {
    getWrmRiskManagementlistDispatch(tagSearch)
    getWrmStatusDispatch(tagSearch)
    setexportShow(true)
  })

  const hadelReset = (() => {
    const params = {
      limit: limit,
      page: 1,
      ...searchParams,
    }
    getWrmRiskManagementlistDispatch(params)
    ClearWrmStatusDispatch(params)
    removeLocalStorage("WEBSITSEARCH")
    setexportShow(false)
    setValue(true)
    setExportBtn(false)
    getBlackWhiteLDispatch()
    clearClientsWrmDispatch()
  })

  useEffect(() => {
    return (() => {
      const params = {
        limit: limit,
        page: 1,
        ...searchParams,
      }
      getWrmRiskManagementlistDispatch(params)
      ClearWrmStatusDispatch(params)
      removeLocalStorage("WEBSITSEARCH")
      setexportShow(false)
      setValue(true)
      setExportBtn(false)
      getBlackWhiteLDispatch()
      clearClientsWrmDispatch()
    })
  }, [])


  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const viewData = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.result ? WrmRiskManagement.data.result : []
  const getData = viewData.filter(o => (o ? o : null))
  const pmaData = getData[0]
  const pmaShowaColumn = pmaData && pmaData.clientId && pmaData.clientId.pma ? pmaData.clientId.pma : ''
  useEffect(() => {
    setShowPma(pmaShowaColumn)
  }, [pmaData])

  const ReportStatus = WrmStatusRes && WrmStatusRes.reportStatus
    ? WrmStatusRes.reportStatus : '--'
  const RiskScoreStatus = WrmStatusRes && WrmStatusRes.riskScoreStatus
    ? WrmStatusRes.riskScoreStatus : '--'
  const RiskStatus = WrmStatusRes && WrmStatusRes.riskStatus
    ? WrmStatusRes.riskStatus : '--'
  const ForceStatus = WrmStatusRes && WrmStatusRes.forceUploadStatus
    ? WrmStatusRes.forceUploadStatus : '--'

  const clearPopup = () => {
    setWebShow(false)
  }

  return (

    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="csvReport"
          className="download-table-xls-button"
          table="table-to-xlss"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='row'>
            <div className='d-flex justify-content-between align-items-center col-md-5 mt-4'>
              <div className='col-md-4 ms-2'>
                {WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-8 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-7 justify-content-end align-items-center my-auto mt-4'>
              <div className=''>
                {
                  !_.isEmpty(actionIds) ? (
                    <Status actionIds={actionIds} setActionIds={setActionIds} setChecked={setChecked} getWrmRiskManagementlistDispatch={getWrmRiskManagementlistDispatch} getWrmStatusDispatch={getWrmStatusDispatch} limit={limit}/>
                  ) : null
                }
              </div>
              <ul className="nav nav-tabs nav-line-tabs fs-6">
                <li className="nav-item">
                  <WebRiskAnalysis
                    exportBtn={exportBtn}
                    setExportBtn={setExportBtn}
                    exportShow={exportShow}
                    Value={Value}
                    setexportShow={setexportShow}
                    showPma={showPma}
                    setCredFilterParams={setCredFilterParams}
                    WrmRiskManagement={WrmRiskManagement}
                    setFilter={setFilter}
                    limit={limit}
                    setSearchParams={setSearchParams}
                  />
                </li>
              </ul>
              {/* <div className='my-auto'>
                <SearchList
                  ClinetId={ClinetId}
                  setCredFilterParams={setCredFilterParams}
                />
              </div> */}
            </div>
          </div>
          <div className='col-lg-12 ms-4 mb-4'>
            {
              exportShow ? (WrmRiskManagement &&
                WrmRiskManagement.data
                ?
                <>
                  <button
                    className='btn btn-sm btn-light-primary btn-responsive font-5vw me-2'
                    onClick={hadelRefresh}
                  >
                    {!WrmStatusloading && <span className='indicator-label'>
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-clockwise me-2" viewBox="0 0 16 16">
                        <path d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z" />
                        <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z" />
                      </svg>
                      Refresh
                    </span>
                    }
                    {WrmStatusloading && (
                      <span className='indicator-progress text-white' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                  <a
                    className='btn btn-sm btn-light-danger btn-responsive font-5vw'
                    onClick={hadelReset}
                  >
                    <KTSVG path="/media/icons/duotune/arrows\arr014.svg"
                    />
                    Reset
                  </a>
                </> : null)
                : ''
            }
          </div>
          {
            exportShow ? (
              <div className='ms-2 mb-4'>
                <span className='fw-bolder d-flex fs-6'>
                  Tag -
                  <span className='fw-bolder text-hover-primary text-primary'>
                    {paginationSearch && paginationSearch.tag ? paginationSearch.tag : ''}
                  </span>
                </span>
              </div>
            ) : ''
          }
          {
            exportShow ? (
              WrmRiskManagement &&
                WrmRiskManagement.data
                ?
                <div className='d-flex col-md-12 align-items-start'
                  style={{
                    marginLeft: '10px'
                  }}
                >
                  {
                    !WrmStatusloading ? (
                      !ClinetId ?
                        <div className='row'>
                          <FindRole
                            role={Role}
                          >
                            <div className='col-md-4'>
                              <div className='row'>
                                <div className='text-black-700 fw-bolder'>
                                  CATEGORY STATUS
                                </div>
                                <div className='col-md-12 mt-4'>
                                  <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                                    PENDING
                                    <span className=' text-hover-primary'>
                                      - {RiskStatus && RiskStatus.pendingCount ? RiskStatus.pendingCount : '0'}
                                    </span>
                                  </span>
                                  <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                                    APPROVED
                                    <span className=' text-hover-primary'>
                                      - {RiskStatus && RiskStatus.approvedCount ? RiskStatus.approvedCount : '0'}
                                    </span>
                                  </span>
                                  <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                                    REJECTED
                                    <span className='text-hover-primary'>
                                      - {RiskStatus && RiskStatus.rejectCount ? RiskStatus.rejectCount : '0'}
                                    </span>
                                  </span>
                                  <span className='text-warning fw-bold mt-2 fs-8'>
                                    MANUAL REVIEW
                                    <span className='text-hover-primary'>
                                      - {RiskStatus && RiskStatus.manualReviewCount ? RiskStatus.manualReviewCount : '0'}
                                    </span>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </FindRole>
                          <div className='col-md-5 col-lg-5'>
                            <div className='row'>
                              <div className='text-black-700 fw-bolder'>
                                <div className='row'>
                                  <div className='col-md-12 col-lg-12'>
                                    REPORT STATUS
                                  </div>
                                </div>
                              </div>
                              <div className='col-md-12 mt-4'>
                                <span className='text-warning fw-bold mt-2 fs-8 me-2'>
                                  PENDING
                                  <span className='text-hover-primary'>
                                    - {ReportStatus && ReportStatus.pendingReportCount ? ReportStatus.pendingReportCount : '0'}
                                  </span>
                                </span>
                                <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                                  QUEUED
                                  <span className=' text-hover-primary'>
                                    - {ReportStatus && ReportStatus.queuedCount ? ReportStatus.queuedCount : '0'}
                                  </span>
                                </span>
                                <span className='text-dark fw-bold mt-2 fs-8 me-2'>
                                  DATA CAPTURED
                                  <span className='text-hover-primary'>
                                    - {ReportStatus && ReportStatus.dataCapturedCount ? ReportStatus.dataCapturedCount : '0'}
                                  </span>
                                </span>
                                <span className='text-primary fw-bold mt-2 fs-8 me-4'>
                                  TAG PROCESSING
                                  <span className=' text-hover-primary'>
                                    - {ReportStatus && ReportStatus.tagProcessingReportCount ? ReportStatus.tagProcessingReportCount : '0'}
                                  </span>
                                </span>
                                <span className='text-pending fw-bold mt-2 fs-8 me-4'>
                                  TAG CAPTURED
                                  <span className=' text-hover-primary'>
                                    - {ReportStatus && ReportStatus.tagCapturedCount ? ReportStatus.tagCapturedCount : '0'}
                                  </span>
                                </span>
                                <span className='text-warning fw-bold mt-2 fs-8 me-4'>
                                  WAITING FOR REPORT
                                  <span className=' text-hover-primary'>
                                    - {ReportStatus && ReportStatus.waitingForReportCount ? ReportStatus.waitingForReportCount : '0'}
                                  </span>
                                </span>
                                <span className='text-success fw-bold mt-2 fs-8 me-4'>
                                  COMPLETED
                                  <span className=' text-hover-primary'>
                                    - {ReportStatus && ReportStatus.completedCount ? ReportStatus.completedCount : '0'}
                                  </span>
                                </span><span className='text-danger fw-bold mt-2 fs-8 me-4'>
                                  REJECTED
                                  <span className=' text-hover-primary'>
                                    - {ReportStatus && ReportStatus.rejectedReportCount ? ReportStatus.rejectedReportCount : '0'}
                                  </span>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='col-md-3'>
                            <div className='row'>
                              <div className='text-black-700 fw-bolder'>
                                RISK SCORE STATUS
                              </div>
                              <div className='col-md-12 mt-4'>
                                <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                                  COMPLETED
                                  <span className=' text-hover-primary'>
                                    - {RiskScoreStatus && RiskScoreStatus.completedRiskScore ? RiskScoreStatus.completedRiskScore : '0'}
                                  </span>
                                </span>
                                <span className='fw-bold mt-2 fs-8 me-4 text-info'>
                                  PENDING
                                  <span className=' text-hover-primary'>
                                    - {RiskScoreStatus && RiskScoreStatus.pendingRiskScoreCount ? RiskScoreStatus.pendingRiskScoreCount : '0'}
                                  </span>
                                </span>
                                <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                                  PROCESSING
                                  <span className=' text-hover-primary'>
                                    - {RiskScoreStatus && RiskScoreStatus.processingRiskScoreCount ? RiskScoreStatus.processingRiskScoreCount : '0'}
                                  </span>
                                </span>
                              </div>
                            </div>
                          </div>
                          <FindRole
                            role={Role}
                          >
                            <div className='col-md-12'>
                              <div className='row'>
                                <div className='text-black-700 fw-bolder'>
                                  FORCE UPLOAD STATUS
                                </div>
                                <div className='col-md-12 mt-4'>
                                  <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                                    COMPLETED
                                    <span className=' text-hover-primary'>
                                      - {ForceStatus && ForceStatus.forceUploadCompletedCount ? ForceStatus.forceUploadCompletedCount : '0'}
                                    </span>
                                  </span>
                                  <span className='fw-bold mt-2 fs-8 me-4 text-info'>
                                    PENDING
                                    <span className=' text-hover-primary'>
                                      - {ForceStatus && ForceStatus.forceUploadPendingCount ? ForceStatus.forceUploadPendingCount : '0'}
                                    </span>
                                  </span>
                                  <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                                    PROCESSING
                                    <span className=' text-hover-primary'>
                                      - {ForceStatus && ForceStatus.forceUploadProcessingCount ? ForceStatus.forceUploadProcessingCount : '0'}
                                    </span>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </FindRole>
                        </div>
                        : null
                    ) : (
                      <div className='col-md-12 d-flex justify-content-center'>
                        <div
                          className='spinner-border text-success'
                          role='status'
                        />
                      </div>
                    )
                  }
                </div> : null
            ) :
              null
          }
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th className="min-w-50px text-center">
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.deviceID
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Review Type</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("reviewType")}
                          >
                            <i
                              className={`bi ${sorting.reviewType
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                  </FindRole>
                  <th>
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("website")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Enqueue Hours</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("enqueueHours")}
                        >
                          <i
                            className={`bi ${sorting.enqueueHours
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Priority</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("priority")}
                        >
                          <i
                            className={`bi ${sorting.priority
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {
                    ClientName === 'Ippopay' ? (null) : (
                      <th>
                        <div className="d-flex">
                          <span>Acquirer</span>
                          <div className="min-w-25px text-end">
                            <div
                              className="cursor-pointer"
                              onClick={() => handleSorting("Acquirer")}
                            >
                              <i
                                className={`bi ${sorting.Acquirer
                                  ? "bi-arrow-up-circle-fill"
                                  : "bi-arrow-down-circle"
                                  } text-primary`}
                              />
                            </div>
                          </div>
                        </div>
                      </th>
                    )
                  }
                  <th>
                    <div className="d-flex">
                      <span>Tag</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {
                    showPma === 'true' ? (
                      <>
                        <th>
                          <div className="d-flex w-200px">
                            Level 1 Category
                          </div>
                        </th>
                        <th>
                          <div className="d-flex w-200px">
                            PMA
                          </div>
                        </th>
                      </>
                    ) : null
                  }
                  <FindRole
                    role={Role}
                  >
                    <th>
                      <div className="d-flex w-200px">
                        {GetClientsRes && GetClientsRes.levelCategory === true ? 'Level 1 Category' : 'Category'}
                      </div>
                    </th>
                    {
                      GetClientsRes && GetClientsRes.levelCategory === true ? null : (
                        <th>
                          <div className="d-flex w-200px">
                            PMA
                          </div>
                        </th>
                      )
                    }
                    {
                      GetClientsRes && GetClientsRes.levelCategory === true ?
                        <th>
                          <div className="d-flex w-150px">
                            Level 2 Category
                          </div>
                        </th>
                        : null
                    }
                    <th>
                      <div className="d-flex w-150px">
                        <span>Update Tags</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("updateTags")}
                          >
                            <i
                              className={`bi ${sorting.updateTags
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex w-150px">
                        <span>Upload Image</span>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex w-150px">
                        <span>Change Status</span>
                      </div>
                    </th>
                  </FindRole>
                  <th className="min-w-100px text-center">
                    {
                      _.includes(Client, Role) ? (
                        <div className="d-flex">
                          <span>Category</span>
                        </div>
                      ) : (
                        <div className="d-flex">
                          <span>
                            {GetClientsRes && GetClientsRes.levelCategory === true ? 'Level 1 Reason' : 'Reason'}
                          </span>
                        </div>
                      )
                    }
                  </th>
                  {
                    _.includes(Client, Role) ? (null) : (
                      <>
                        <th>
                          <div className="d-flex">
                            <span>Risk classification</span>
                            <div className="min-w-25px text-end">
                              <div
                                className="cursor-pointer"
                                onClick={() => handleSorting("riskClassification")}
                              >
                                <i
                                  className={`bi ${sorting.riskClassification
                                    ? "bi-arrow-up-circle-fill"
                                    : "bi-arrow-down-circle"
                                    } text-primary`}
                                />
                              </div>
                            </div>
                          </div>
                        </th>
                        <th>
                          <div className="d-flex">
                            <span>Risk score</span>
                            <div className="min-w-25px text-end">
                              <div
                                className="cursor-pointer"
                                onClick={() => handleSorting("riskScore")}
                              >
                                <i
                                  className={`bi ${sorting.riskScore
                                    ? "bi-arrow-up-circle-fill"
                                    : "bi-arrow-down-circle"
                                    } text-primary`}
                                />
                              </div>
                            </div>
                          </div>
                        </th>
                        <th>
                          <div className="d-flex">
                            <span>Risk Level</span>
                            <div className="min-w-25px text-end">
                              <div
                                className="cursor-pointer"
                                onClick={() => handleSorting("riskLevel")}
                              >
                                <i
                                  className={`bi ${sorting.riskLevel
                                    ? "bi-arrow-up-circle-fill"
                                    : "bi-arrow-down-circle"
                                    } text-primary`}
                                />
                              </div>
                            </div>
                          </div>
                        </th>
                      </>
                    )
                  }

                  {GetClientsRes && GetClientsRes.levelCategory === true ?
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Level 2 Reason</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("level2Reason")}
                          >
                            <i
                              className={`bi ${sorting.level2Reason
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    : null
                  }
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Report Status</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("reportStatus")}
                          >
                            <i
                              className={`bi ${sorting.reportStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex">
                        <span>Manual Status</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("manualStatus")}
                          >
                            <i
                              className={`bi ${sorting.manualStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Auto Status</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("autoStatus")}
                          >
                            <i
                              className={`bi ${sorting.autoStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                  </FindRole>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      WrmRiskManagement &&
                        WrmRiskManagement.data
                        ? (
                          WrmRiskManagement.data && WrmRiskManagement.data.result.map((riskmgmtlist, i) => {
                            const indiaDate = moment.utc(riskmgmtlist && riskmgmtlist.createdAt).local()
                            const todayDate = moment()
                            const duration = moment.duration(todayDate.diff(indiaDate))
                            const hours = Math.floor(duration.asHours());
                            const minutes = duration.minutes();
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                                  <input
                                    className="form-check-input cursor-pointer"
                                    type="checkbox"
                                    value={checked}
                                    onChange={() =>
                                      handleCheckboxChange(riskmgmtlist._id)
                                    }
                                    id="flexRadioLg"
                                  />
                                  {/* <button
                                    className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-2'
                                    onClick={() => onDeleteItem(riskmgmtlist._id)}
                                    title="Delete"
                                  >
                                    <KTSVG
                                      path='/media/icons/duotune/general/gen027.svg'
                                      className='svg-icon-3'
                                    />
                                  </button> */}
                                </td>
                                <td className="ellipsis">
                                  {
                                    Role !== 'Client User' ? (
                                      <>
                                        <a
                                          className='color-primary cursor-pointer'
                                          onClick={() => window.open(`/risk-summary/update/${riskmgmtlist._id}`, "_blank")}
                                          to={`/risk-summary/update/${riskmgmtlist._id}`}
                                        >
                                          WRM{
                                            riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"
                                          }
                                        </a>
                                      </>
                                    ) : (
                                      Role === 'Client User' && riskmgmtlist.riskStatus === "APPROVED" ? (
                                        <a
                                          className='color-primary cursor-pointer'
                                          onClick={() => window.open(`/risk-summary/update/${riskmgmtlist._id}`, "_blank")}
                                          to={`/risk-summary/update/${riskmgmtlist._id}`}
                                        >
                                          WRM{
                                            riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"
                                          }
                                        </a>
                                      ) : (
                                        <span>
                                          WRM{riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"}
                                        </span>
                                      )
                                    )
                                  }
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskmgmtlist.riskStatus && riskmgmtlist.riskStatus]}`}>
                                    {riskmgmtlist.riskStatus ? riskmgmtlist.riskStatus : "--"}
                                  </span>
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td className="ellipsis">
                                    <span className={`badge ${REVIEWTYPE[riskmgmtlist.reviewType && riskmgmtlist.reviewType ? riskmgmtlist.reviewType : '--']}`}>
                                      {riskmgmtlist.reviewType ? riskmgmtlist.reviewType : "--"}
                                    </span>
                                  </td>
                                </FindRole>
                                <td className="ellipsis">
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => window.open(riskmgmtlist.website ? riskmgmtlist.website : "--", "_blank")}
                                  >
                                    {
                                      riskmgmtlist.website ? riskmgmtlist.website : "--"
                                    }
                                  </a>
                                </td>

                                <td className="ellipsis">
                                  <div>
                                  {riskmgmtlist && riskmgmtlist.enqueueHours ? !_.isEmpty(riskmgmtlist.enqueueHours) ? riskmgmtlist.enqueueHours : `${hours}:${minutes}` : `${hours}:${minutes}`}
                                  </div>
                                </td>
                                <td className="ellipsis">
                                 <div>
                                 {riskmgmtlist && riskmgmtlist.priority ? riskmgmtlist.priority : "--"}
                                 </div>
                                </td>
                                {
                                  ClientName === 'Ippopay' ? (null) : (
                                    <td className="ellipsis">
                                      {riskmgmtlist.acquirer ? riskmgmtlist.acquirer : "--"}
                                    </td>
                                  )
                                }
                                <td className="ellipsis">
                                  {riskmgmtlist.tag ? riskmgmtlist.tag : "--"}
                                </td>
                                {
                                  showPma === 'true' ? (
                                    <>
                                      <td>
                                        {riskmgmtlist.level1Category ? riskmgmtlist.level1Category : "--"}
                                      </td>
                                      <td>
                                        {riskmgmtlist.pma ? riskmgmtlist.pma : "--"}
                                      </td>
                                    </>
                                  ) : null
                                }
                                <FindRole
                                  role={Role}
                                >
                                  <td>
                                    {
                                      riskmgmtlist && riskmgmtlist.playstoreUrl === 'YES' ? '--' :
                                        <BlockListCategory1
                                          blacklistDropdown={blacklistDropdown}
                                          manualCategorys={riskmgmtlist.level1Category}
                                          blockListValue={riskmgmtlist._id}
                                          riskIdValueId={riskmgmtlist.riskId}
                                          showBlockListClient={showBlockListClient}
                                        />
                                    }
                                  </td>
                                  {
                                    GetClientsRes && GetClientsRes.levelCategory === true ? null : (
                                      <td>
                                        {
                                          riskmgmtlist && riskmgmtlist.playstoreUrl === 'YES' ? '--' :
                                            <PMAcategory
                                              blacklistDropdown={blacklistDropdown}
                                              manualCategorys={riskmgmtlist.pma}
                                              blockListValue={riskmgmtlist._id}
                                              riskIdValueId={riskmgmtlist.riskId}
                                            />
                                        }
                                      </td>
                                    )
                                  }
                                  {
                                    GetClientsRes && GetClientsRes.levelCategory === true ? (
                                      <td>
                                        <BlockListCategory
                                          blacklistDropdown={blacklistDropdown}
                                          manualCategorys={riskmgmtlist.level2Category}
                                          blockListValue={riskmgmtlist._id}
                                          riskIdValueId={riskmgmtlist.riskId}
                                        />
                                      </td>
                                    ) : null
                                  }
                                  <td>
                                    {
                                      // riskmgmtlist.policyComplainceChecks === true && 
                                      // riskmgmtlist.riskStatus === 'APPROVED' &&
                                      riskmgmtlist.reportStatus === 'TAG PROCESSING'
                                        ?
                                        <span
                                          onClick={() => getIdValue(riskmgmtlist._id, riskmgmtlist.riskId)}
                                        >
                                          <PriceCheckPopup
                                            blockListValue={blockListValue}
                                            tagSearch={tagSearch}
                                          />
                                        </span>
                                        : '--'
                                    }
                                  </td>
                                  <td>
                                    <span
                                    // onClick={() => getIdValue(riskmgmtlist._id, riskmgmtlist.riskId)}
                                    >
                                      <UploadImage
                                        id={riskmgmtlist._id}
                                      />
                                    </span>
                                  </td>
                                  <td>
                                    <span
                                    // onClick={() => getIdValue(riskmgmtlist._id, riskmgmtlist.riskId)}
                                    >
                                      <StatusChange
                                        id={riskmgmtlist._id}
                                      />
                                    </span>
                                  </td>
                                </FindRole>
                                {
                                  _.includes(Client, Role) ? (
                                    <td>
                                      {riskmgmtlist.level1Category ? riskmgmtlist.level1Category : "--"}
                                    </td>
                                  ) : (
                                    <>
                                      <td>
                                        {riskmgmtlist.level1Reason ? riskmgmtlist.level1Reason : "--"}
                                      </td>
                                      <td>
                                        {riskmgmtlist.riskClasification
                                          ? riskmgmtlist.riskClasification
                                          : "--"}
                                      </td>
                                      <td>
                                        {riskmgmtlist.riskScore ? riskmgmtlist.riskScore : "--"}
                                      </td>
                                      <td>
                                        {riskmgmtlist.riskLevel ? riskmgmtlist.riskLevel : "--"}
                                      </td>
                                      {
                                        GetClientsRes && GetClientsRes.levelCategory === true ?
                                          <td>
                                            {riskmgmtlist.level2Reason ? riskmgmtlist.level2Reason : "--"}
                                          </td>
                                          : null
                                      }
                                    </>
                                  )
                                }
                                <FindRole
                                  role={Role}
                                >
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.reportStatus && riskmgmtlist.reportStatus]}`}>
                                      {riskmgmtlist.reportStatus ? riskmgmtlist.reportStatus : "--"}
                                    </span>
                                  </td>
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.manualStatus && riskmgmtlist.manualStatus]}`}>
                                      {riskmgmtlist.manualStatus ? riskmgmtlist.manualStatus : "--"}
                                    </span>
                                  </td>
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.autoStatus && riskmgmtlist.autoStatus]}`}>
                                      {riskmgmtlist.autoStatus ? riskmgmtlist.autoStatus : "--"}
                                    </span>
                                  </td>
                                </FindRole>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { WrmRiskManagementStore, exportlistStore, PostCategoryStore, BlockListTypeStore, BWlistStore, PriceStore,
    PostPMAStore, WrmStatusStore, GetClientsStore } = state
  return {
    WrmRiskManagement: state && state.WrmRiskManagementStore && state.WrmRiskManagementStore.WrmRiskManagement,
    loading: WrmRiskManagementStore && WrmRiskManagementStore.loading ? WrmRiskManagementStore.loading : false,
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : false,
    BlockListTypes: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType?.data?.data : null,
    PostCategory: PostCategoryStore && PostCategoryStore.PostCategory ? PostCategoryStore.PostCategory?.data : '',
    BlackWhiteDataList: BWlistStore && BWlistStore.BWLists && BWlistStore.BWLists.data ? BWlistStore.BWLists.data : '',

    postPriceCheckSuccess: PriceStore && PriceStore.priceSuccess && PriceStore.priceSuccess.data ? PriceStore.priceSuccess.data : '',
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    postPMAres: PostPMAStore && PostPMAStore.postPMAres ? PostPMAStore.postPMAres?.data : '',
    WrmStatusRes: WrmStatusStore && WrmStatusStore.WrmStatusRes ? WrmStatusStore.WrmStatusRes?.data : '',
    WrmStatusloading: WrmStatusStore && WrmStatusStore.loading ? WrmStatusStore.loading : false,
    GetClientsRes: GetClientsStore && GetClientsStore.GetClientsRes ? GetClientsStore.GetClientsRes?.data : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getWrmRiskManagementlistDispatch: (params) => dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  getBlockListTypeDispatch: (params) => dispatch(BlockListEmailActions.getBlockListType(params)),
  ClearPostCategroy: () => dispatch(PostCategoryActions.ClearPostCategroy()),
  getBlackWhiteLDispatch: (params) => dispatch(BWListActions.getBWList(params)),
  setFilterFunctionDispatch: (data) =>
    dispatch(clientCredFilterActions.setFilterFunction(data)),
  getWrmStatusDispatch: (params) => dispatch(WrmStatusActions.getWrmStatus(params)),
  ClearWrmStatusDispatch: () => dispatch(WrmStatusActions.clearWrmStatus()),
  clearClientsWrmDispatch: (data) => dispatch(GetClientsActions.clearClientsWrm(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(WrmRiskManagement)
