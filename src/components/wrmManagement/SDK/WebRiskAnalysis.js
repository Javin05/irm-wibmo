import React, { useEffect, useState, useRef } from 'react'
import {
  PostWebAnalysisActions,
  getWebAnalysisActions,
  SdkManualWebAnalysisActions,
  DeleteWebAnalysisActions,
  EditWebAnalysisActions,
  updateWebAnalysisActions,
  clientIdLIstActions,
  SdkExportListActions,
  wrmSdkActions
} from '../../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import { KTSVG } from '../../../theme/helpers'
import { STATUS_RESPONSE, SWEET_ALERT_MSG, DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT, STATUS_BADGE } from '../../../utils/constants'
import { successAlert, warningAlert, confirmationAlert } from "../../../utils/alerts"
import './styles.css';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Modal from 'react-bootstrap/Modal'
import { userValidation, manualValidation } from './validation'
import { setWebAnalysisData } from './formData'
import clsx from 'clsx'
import SearchList from './searchList'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { getLocalStorage, removeLocalStorage } from '../../../utils/helper'
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import { CSVLink } from "react-csv"
import FindRole from '../Role'

function WebRiskAnalysis(props) {
  const {
    className,
    loading,
    getWebAnalysisDispatch,
    BlockListType,
    WebAnalysisDispatch,
    getWebAnalysis,
    postCSVWebAnalysis,
    clearImportDispatch,
    cleargetWebAnalysislistDispatch,
    postManualWebAnalysisDispatch,
    postManualWebAnalysis,
    clearManualWebAnalysisDispatch,
    clientIdDispatch,
    clinetIdLists,
    getExportDispatch,
    exportLists,
    exportShow,
    postCSVLoading,
    getWrmRiskManagementlistDispatch,
    Value,
    setexportShow,
    setExportBtn,
    exportBtn,
    exportLoading,
    clearExportListDispatch,
    postManualWebLoading
  } = props

  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [errors, setErrors] = useState({});
  const [error, setError] = useState({})
  const [fileName, setFileName] = useState()
  const [show, setShow] = useState(false)
  const [edit, setEdit] = useState(false)
  const [key, setKey] = useState('BulkUpload');
  const [editMode, setEditMode] = useState(false)
  const [currentId, setcurrentId] = useState()
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [IndidualOption, setIndidualOption] = useState()
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState('')

  const [Check, setCheck] = useState(false);
  const [formData, setFormData] = useState({
    clientId: Role === 'Client User' ? ClinetId : '',
    tag: '',
    file: '',
    skip_category_validation: 'false'
  })
  const [manualFormData, setManualFormData] = useState({
    clientId: Role === 'Client User' ? ClinetId : '',
    website: '',
    tag: '',
  })
  const [sorting, setSorting] = useState({
    tag: false
  })

  useEffect(() => {
    getWebAnalysisDispatch()
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
    removeLocalStorage('ExportHide')
  }, [])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getWebAnalysisDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getWebAnalysisDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getWebAnalysisDispatch(params)
    }
  }
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const hiddenFileInput = useRef(null);
  const handleChange = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
    setFormData((formData) => ({ ...formData, skip_category_validation: e.target.checked }))
  }

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setFormData((values) => ({
          ...values,
          file: files,
        }));
        setErrors((values) => ({ ...values, file: "" }));
        setFileName(files && files.name);
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  };

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  };

  const OnSubmit = () => {
    const errorMsg = userValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("tag", formData.tag)
      data.append("clientId", formData.clientId)
      data.append("file", formData.file)
      data.append("skip_category_validation", formData.skip_category_validation)
      WebAnalysisDispatch(data)
    }
  }

  const handelSubmit = () => {
    const errorMsg = manualValidation(manualFormData, setError)
    if (_.isEmpty(errorMsg)) {
        postManualWebAnalysisDispatch(manualFormData)
    }
  }

  const clearPopup = () => {
    setSelectedIndidualOption('')
    setEditMode(false)
    setShow(false)
    setFormData(values => ({
      ...values,
      file: '',
      tag: '',
      skip_category_validation: 'false'
    }))
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  const onConfirm = () => {
    setShow(false)
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    clearImportDispatch()
    setFormData(values => ({
      ...values,
      file: '',
      tag: '',
      skip_category_validation: 'false'
    }))
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  const clear = () => {
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    clearImportDispatch()
    setManualFormData({
      website: '',
      tag: ''
    })
    setFormData(values => ({
      ...values,
      file: '',
      tag: ''
    }))
  }

  useEffect(() => {
    if (show === false) {
      setKey('BulkUpload')
    }
  }, [show])

  useEffect(() => {
    if (postCSVWebAnalysis && postCSVWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        'success',
        'Back to Web RisK Analysis',
        'Ok',
        () => { onConfirm() },
        () => { clear() },
        clearImportDispatch()
      )
      getWebAnalysisDispatch()
      cleargetWebAnalysislistDispatch()
      getWrmRiskManagementlistDispatch()
      setSelectedAsigneesOption('')
    } else if (postCSVWebAnalysis && postCSVWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      clearImportDispatch()
    }
  }, [postCSVWebAnalysis])

  useEffect(() => {
    if (postManualWebAnalysis && postManualWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        postManualWebAnalysis && postManualWebAnalysis.message,
        'success',
        'Back to Web RisK Analysis',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      getWebAnalysisDispatch()
      clearManualWebAnalysisDispatch()
      getWrmRiskManagementlistDispatch()
      setSelectedIndidualOption('')
    } else if (postManualWebAnalysis && postManualWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postManualWebAnalysis && postManualWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      clearManualWebAnalysisDispatch()
    }
  }, [postManualWebAnalysis])


  const totalPages =
    BlockListType && BlockListType.count
      ? Math.ceil(parseInt(BlockListType && BlockListType.count) / limit)
      : 1

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
    setIndidualOption(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const handleChangeIndidual = selectedOption => {
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      setManualFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const exportParams = JSON.parse(getLocalStorage('TAG'))
  const exported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website
    }
    getExportDispatch(params)
  }

  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === 'ok') {
      setexportShow(true)
      const closeXlsx = document.getElementById('bulkCsvReport')
      closeXlsx.click()
      clearExportListDispatch()
    }
  }, [exportLists])

  const data = [
    { website: "https://www.firangiyarn.com/", tag: 'Razorpay' },
    { website: "https://www.vanheusenindia.com/", tag: 'Citrus' },
    { website: "https://ragecoffee.com/", tag: 'CCAvenue' },
    { website: "https://www.buffalowildwings.in/", tag: 'PayUBiz' },
    { website: "https://supertails.com/", tag: 'Direcpay' },
    { website: "https://shop.lakmesalon.in/", tag: 'Zaakpay' },
    { website: "https://www.feorganics.com/", tag: 'Instamojo' },
    { website: "https://thameha.com/", tag: 'stripe' },
    { website: "https://ssrsilks.in/", tag: 'Adyen' },
    { website: "https://cielsports.com/", tag: 'Braintree' },
    { website: "https://www.curiohh.com/", tag: 'Worldpay' },
    { website: "https://spetrol.in/", tag: 'SagePay' },
    { website: "https://www.onlinemantra.in/", tag: 'Amazon Pay' },
    { website: "https://mymorningowl.com/", tag: 'Website Payments Pro' },
    { website: "https://mplsports.in/", tag: 'Helcim' },
    { website: "https://socialactivities.in/", tag: 'Square' },
    { website: "https://dishamultiservice.com/", tag: '2Checkout' },
    { website: "http://webr.in/", tag: 'Payza' },
    { website: "https://zozila.com/", tag: 'SecurionPay' },
    { website: "https://www.workersunity.com/", tag: 'Authorize.Net.' },
    { website: "https://www.probotronix.com/", tag: 'ECOMMPAY' },
    { website: "http://ishidiamonds.com/", tag: 'Skrill' },
    { website: "https://theinvincibleindia.in/", tag: 'WePay' },
    { website: "https://techwayonline.com/", tag: 'GOCARDLESS' },
    { website: "https://almajeedinstitute.in/", tag: 'forte' },
    { website: "https://ambitionhost.in/", tag: 'ICICI Bank' },
    { website: "https://50001.in/", tag: 'PayTabs' },
    { website: "https://www.jeevanrekhatheatregroup.com/", tag: 'mercadopago' }
  ]

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename={`${exportParams && exportParams.tag}-report`}
          sheet="tablexls"
        />
      </div>
      <div className={`card p-7 ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex px-2'>
            <div className='d-flex justify-content-end my-auto col-md-12 col-sm-12 col-lg-12'>
              <div className='my-auto me-3'>
                <SearchList setExportBtn={setExportBtn} Value={Value} setexportShow={setexportShow} />
              </div>
              {
                Role !== 'Analyst' ? (

                  <div className='my-auto me-3'>
                    <button
                      className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
                      onClick={() => {
                        setShow(true)
                        setEditMode(false)
                      }}
                    >
                      <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                      Add Web Case
                    </button>
                  </div>
                ) :
                  (
                    ''
                  )
              }
              {
                exportShow && Role !== 'Analyst' ? (
                  <button
                    type='button'
                    className='btn btn-sm btn-light-success btn-responsive font-5vw me-3 pull-right w-150px'
                    onClick={(e) => exported(e)}
                    disabled={exportLoading}
                  >
                    {!exportLoading &&
                      <span className='indicator-label'>
                        <i className="bi bi-filetype-csv" />
                        Export
                      </span>
                    }
                    {exportLoading && (
                      <span className='indicator-progress text-success' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                )
                  : null
              }
            </div>
          </div>
          <div className="table-responsive" style={{
            display: "none"
          }}>
            <table className="table" id="table-to-xls">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  <th>Web Url</th>
                  <th>Level 1 Status</th>
                  <th>Acquirer</th>
                  <th>Level 1 Reason</th>
                  <th>Code</th>
                  <th>Risk Classification</th>
                  <th>Risk Level</th>
                  <th>Level 2 Status</th>
                  <th>Level 2 Reason</th>
                  <th>Website Working?</th>
                  <th>Legal Name</th>
                  <th>Malware Present</th>
                  <th>Malware Risk</th>
                  <th>Domain Registered</th>
                  <th>Domain Registration Company</th>
                  <th>Domain Registration Date</th>
                  <th>Domain Registration Expiry Date</th>
                  {/* <th>Website Traffic</th> */}
                  <th>SSL Certificate Check</th>
                  <th>Adult Content Monitoring</th>
                  {/* <th>Product Category</th> */}
                  <th>Merchant Policy Link Work</th>
                  <th>Negative Keywords</th>
                  <th>Readiness</th>
                  <th>Transparency</th>
                  <th>Contact Details Phone</th>
                  <th>Contact Details Email</th>
                  <th>Purchase Or Registration</th>
                  <th>Merchant Intelligence</th>
                  <th>Logo</th>
                  <th>IP Address On Server</th>
                  <th>Risk Score</th>

                  <th>Language Detected</th>
                  <th>Business Category</th>
                  <th>Malware Monitoring</th>
                  <th>Parked Domain</th>
                  <th>Spamming</th>
                  <th>Http Status Code</th>
                  <th>Page Size</th>
                  <th>Server</th>
                  <th>Host Names</th>
                  <th>Dns Sec Org</th>
                  <th>Domain Aliases</th>
                  <th>Safety</th>
                  <th>Suspicious</th>
                  <th>Phishing</th>
                  <th>Domain Age</th>
                  <th>Domain Rank</th>
                  <th>Business Address</th>
                  <th>Content Repetition</th>
                  <th>Content Keyword Results</th>
                  <th>Administrative Contact State</th>
                  <th>Administrative Contact City</th>
                  <th>Administrative Contact Country</th>
                  <th>Administrative Contact Country Code</th>
                  <th>Administrative Contact Email</th>
                  <th>Administrative Contact Name</th>
                  <th>Administrative Contact Telephone</th>
                  <th>Administrative Contact Organization</th>
                  <th>Administrative Contact Postal Code</th>
                  <th>Administrative Contact Postal Street1</th>
                  <th>Geo City</th>
                  <th>Geo Country</th>
                  <th>Geo Country Code</th>
                  <th>Geo Latitude</th>
                  <th>Geo Longitude</th>
                  <th>Geo Postal Code</th>
                  <th>Geo State</th>
                  <th>Geo State Code</th>
                  <th>Geo Street Address</th>
                  <th>Geo Street Name</th>
                  <th>Geo Street Number</th>
                  <th>Page Activity Check Mining</th>
                  <th>Page Activity Check Untrusted Downloads</th>
                  <th>Page Availability Check URL Status</th>
                  <th>Page Health Check Content Accessibilty</th>
                  <th>Page Health Check PageLoading Time</th>
                  <th>Page Links Connectivity Check Success Rate</th>
                  <th>Page Redirection Check Domain Redirection</th>
                  <th>Policy Compliance Status Contact Us Page</th>
                  <th>Policy Contains Valid Phone</th>
                  <th>Policy Contains Valid Email</th>
                  <th>Contacts Form Status</th>
                  <th>Contacts Policy Page Screenshot</th>
                  <th>Privacy Policy Status</th>
                  <th>Privacy Policy Page Screenshot</th>
                  <th>Return Policy Status</th>
                  <th>Return Policy Page Screenshot</th>
                  <th>Shipping Policy Status</th>
                  <th>Shipping Policy Page Screenshot</th>
                  <th>Terms And Condition Status</th>
                  <th>Terms And Condition Policy Page Screenshot</th>
                  <th>Website To Business Name Mcc Code Match</th>
                  <th>Website To Business Name Match Rating</th>
                  <th>Content Keyword Risk</th>
                  <th>Ssl Vulnerability Info</th>
                  <th>Ssl Check Info</th>
                  <th>User Interaction Checks Bank Detail Submission</th>
                  <th>User Interaction Checks Id Proof Submission</th>
                  <th>User Interaction Checks Seller Redirection</th>
                  <th>Predict Medicine</th>
                  <th>Shopify</th>
                  <th>Woo Commerce</th>
                  <th>Website Tags</th>
                  <th>Facebook URL</th>
                  <th>Facebook Likes</th>
                  <th>Twitter URL</th>
                  <th>Twitter Followers</th>
                  <th>Linked In URL</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                {
                  !_.isEmpty(exportLists && exportLists.data && exportLists.data.data) ?
                    exportLists && exportLists.data && exportLists.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          <td>
                            {item.webUrl}
                          </td>
                          <td>
                            {item && item.riskmanagement && item.riskmanagement.level1Status}
                          </td>
                          <td>
                            {item && item.riskmanagement && item.riskmanagement.acquirer}
                          </td>
                          <td>
                            {item && item.riskmanagement && item.riskmanagement.level1Reason}
                          </td>
                          <td>
                            {item && item.productCategoryCode}
                          </td>
                          <td>
                            {item.riskClassification}
                          </td>
                          <td>
                            {item.riskLevel}
                          </td>
                          <td>
                            {item && item.riskmanagement && item.riskmanagement.level2Status}
                          </td>
                          <td>
                            {item && item.riskmanagement && item.riskmanagement.level2Reason}
                          </td>
                          <td>
                            {item.websiteWorking}
                          </td>
                          <td>
                            {item.legalName}
                          </td>
                          <td>
                            {item.malwarePresent}
                          </td>
                          <td>
                            {item.malwareRisk}
                          </td>
                          <td>
                            {item.domainRegistered}
                          </td>
                          <td>
                            {item.domainRegistrationCompany}
                          </td>
                          <td>
                            {item.domainRegistrationDate}
                          </td>
                          <td>
                            {item.domainRegistrationExpiryDate}
                          </td>
                          {/* <td>
                          {item.websiteTraffic}
                        </td> */}
                          <td>
                            {item.sslCertificateCheck}
                          </td>
                          <td>
                            {item.adultContentMonitoring}
                          </td>
                          {/* <td>
                          {
                            !_.isEmpty(item && item.productCategory) ? (
                              item && item.productCategory.map((product, pr) => {
                                return (
                                  <div key={pr}>
                                    <span>
                                      {
                                        product
                                      }
                                    </span>
                                  </div>
                                )
                              })
                            ) : 'No Data'
                          }
                        </td> */}
                          <td>
                            {item.merchantPolicyLinkWork}
                          </td>
                          <td>
                            {item.negativeKeywords}
                          </td>
                          <td>
                            {item.readiness}
                          </td>
                          <td>
                            {item.transparency}
                          </td>
                          <td>
                            {
                              !_.isEmpty(item && item.contactDetailsPhone) ? (
                                <div>
                                  <span>
                                    {
                                      item && item.contactDetailsPhone.join(" , ")
                                    }
                                  </span>
                                </div>
                              ) : 'No Data'
                            }
                          </td>
                          <td>
                            {
                              !_.isEmpty(item && item.contactDetailsEmail) ? (
                                <div>
                                  <span>
                                    {
                                      item && item.contactDetailsEmail.join(" , ")
                                    }
                                  </span>
                                </div>
                              ) : 'No Data'
                            }
                          </td>
                          <td>
                            {item.purchaseOrRegistration}
                          </td>
                          <td>
                            {item.merchantIntelligence}
                          </td>
                          <td>
                            <span>
                              {item.logo}
                            </span>
                          </td>
                          <td>
                            {
                              !_.isEmpty(item && item.ipAddressOfServer) ? (
                                <div>
                                  <span>
                                    {
                                      item && item.ipAddressOfServer.join(" , ")
                                    }
                                  </span>
                                </div>
                              ) : 'No Data'
                            }
                          </td>
                          <td>
                            {
                              item.riskScore === 'No Data' ? 'No Data' :
                                parseFloat(item.riskScore).toFixed(2)
                            }
                          </td>



                          <td>
                            {
                              !_.isEmpty(item && item.languageDetected) ? (
                                <div>
                                  <span>
                                    {
                                      item && item.languageDetected.join(" , ")
                                    }
                                  </span>
                                </div>
                              ) : 'No Data'
                            }
                          </td>
                          <td>
                            {item.businessCategory}
                          </td>
                          <td>
                            {item.malwareMonitoring}
                          </td>
                          <td>
                            {item.parkedDomain}
                          </td>
                          <td>
                            {item.spamming}
                          </td>
                          <td>
                            {item.httpStatusCode}
                          </td>
                          <td>
                            {item.pageSize}
                          </td>
                          <td>
                            {item.server}
                          </td>
                          <td>
                            {
                              !_.isEmpty(item && item.hostNames) ? (
                                <div>
                                  <span>
                                    {
                                      item && item.hostNames.join(" , ")
                                    },
                                  </span>
                                </div>
                              ) : 'No Data'
                            }
                          </td>
                          <td>
                            {item.dnsSecOrg}
                          </td>
                          <td>
                            {item.domainAliases}
                          </td>
                          <td>
                            {item.safety}
                          </td>
                          <td>
                            {item.suspicious}
                          </td>
                          <td>
                            {item.phishing}
                          </td>
                          <td>
                            {item.domainAge}
                          </td>
                          <td>
                            {item.domainRank}
                          </td>
                          <td>
                            {item.businessAddress}
                          </td>
                          <td>
                            {item.contentRepetition}
                          </td>
                          <td>
                            {item.contentKeywordResults}
                          </td>
                          <td>
                            {item.administrativeContactState}
                          </td>
                          <td>
                            {item.administrativeContactCity}
                          </td>
                          <td>
                            {item.administrativeContactCountry}
                          </td>
                          <td>
                            {item.administrativeCountryCode}
                          </td>
                          <td>
                            {item.administrativeContactEmail}
                          </td>
                          <td>
                            {item.administrativeContactName}
                          </td>
                          <td>
                            {item.administrativeContactTelephone}
                          </td>
                          <td>
                            {item.administrativeContactOrganization}
                          </td>
                          <td>
                            {item.administrativeContactPostalCode}
                          </td>
                          <td>
                            {item.administrativeContactStreet1}
                          </td>
                          <td>
                            {item.geoCity}
                          </td>
                          <td>
                            {item.geoCountry}
                          </td>
                          <td>
                            {item.geoCountryCode}
                          </td>
                          <td>
                            {item.geoLatitude}
                          </td>
                          <td>
                            {item.geoLongitude}
                          </td>
                          <td>
                            {item.geoPostalCode}
                          </td>
                          <td>
                            {item.geoState}
                          </td>
                          <td>
                            {item.geoStateCode}
                          </td>
                          <td>
                            {item.geoStreetAddress}
                          </td>
                          <td>
                            {item.geoStreetName}
                          </td>
                          <td>
                            {item.geoStreetNumber}
                          </td>
                          <td>
                            {item.pageActivityCheckMining}
                          </td>
                          <td>
                            {item.pageActivityCheckUntrustedDownloads}
                          </td>
                          <td>
                            {item.pageAvailabilityCheckURLStatus}
                          </td>
                          <td>
                            {item.pageHealthCheckContentAccessibilty}
                          </td>
                          <td>
                            {item.pageHealthCheckPageLoadingTime}
                          </td>
                          <td>
                            {item.pageLinksConnectivityCheckSuccessRate}
                          </td>
                          <td>
                            {item.pageRedirectionCheckDomainRedirection}
                          </td>
                          <td>
                            {item.policyComplianceStatusContactUsPage}
                          </td>
                          <td>
                            {item.policyContainsValidPhone}
                          </td>
                          <td>
                            {item.policyContainsValidEmail}
                          </td>
                          <td>
                            {item.contactsFormStatus}
                          </td>
                          <td>
                            {item.contactUsPageScreenshot}
                          </td>
                          <td>
                            {item.privacyPolicyStatus}
                          </td>
                          <td>
                            {item.privatePolicyPageScreenshot}
                          </td>
                          <td>
                            {item.returnPolicyStatus}
                          </td>
                          <td>
                            {item.returnPolicyPageScreenshot}
                          </td>
                          <td>
                            {item.shippingPolicyStatus}
                          </td>
                          <td>
                            {item.shippingPolicyPageScreenshot}
                          </td>
                          <td>
                            {item.termsAndConditionStatus}
                          </td>
                          <td>
                            {item.termsAndConditionPageScreenshot}
                          </td>
                          <td>
                            {item.websiteToBusinessNameMccCodeMatch}
                          </td>
                          <td>
                            {item.websiteToBusinessNameMatchRating}
                          </td>
                          <td>
                            {item.contentKeywordRisk}
                          </td>
                          <td>
                            {item.sslVulnerabilityInfo}
                          </td>
                          <td>
                            {item.sslCheckInfo}
                          </td>
                          <td>
                            {item.userInteractionChecksBankDetailSubmission}
                          </td>
                          <td>
                            {item.userInteractionChecksIdProofSubmission}
                          </td>
                          <td>
                            {item.userInteractionChecksSellerRedirection}
                          </td>
                          <td>
                            {item.predictMedicine}
                          </td>
                          <td>
                            {item.shopify}
                          </td>
                          <td>
                            {item.wooCommerce}
                          </td>
                          <td>
                            {
                              Array.isArray(item && item.websiteTags) ? (
                                <div>
                                  <span>
                                    {
                                      item && item.websiteTags.join(" , ")
                                    },
                                  </span>
                                </div>
                              ) : 'No Data'
                            }
                          </td>
                          <td>
                            {item.facebookURL}
                          </td>
                          <td>
                            {item.facebookLikes}
                          </td>
                          <td>
                            {item.twitterURL}
                          </td>
                          <td>
                            {item.twitterFollowers}
                          </td>
                          <td>
                            {item.linkedInURL}
                          </td>
                          <td>
                            {item.description}
                          </td>

                        </tr>
                      )
                    })
                    : null
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"} Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab ftab"
          >
            <Tab eventKey="BulkUpload" title="BulkUpload">
              <div className="card card-custom card-stretch gutter-b p-8">
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Client :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name='AppUserId'
                          className='select2'
                          classNamePrefix='select'
                          handleChangeReactSelect={handleChangeAsignees}
                          options={AsigneesOption}
                          value={SelectedAsigneesOption}
                          isDisabled={!AsigneesOption}
                        />
                        {error && error.client && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.client}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    null
                  )
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': formData.tag && errors.tag },
                        {
                          'is-valid': formData.tag && !errors.tag
                        }
                      )}
                      onChange={(e) => handleChange(e)}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={formData.tag || ''}
                    />
                    {errors && errors.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.tag}
                      </div>
                    )}
                  </div>
                </div>
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4' />
                      <div className='col-md-8'>
                        <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            id="flexRadioLg"
                            name='skip_category_validation'
                            onChange={(e) => handleChange(e)}
                          />
                          <label className="form-check-label text-muted" for="flexRadioLg">
                            Skip Category Validation
                          </label>
                        </div>
                      </div>
                    </div>
                  ) : null
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Upload Document :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      type="file"
                      className="d-none"
                      name="file"
                      id="file"
                      multiple={true}
                      ref={hiddenFileInput}
                      onChange={handleFileChange}
                    />
                    <button
                      type="button"
                      style={{
                        width: "100%",
                      }}
                      className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                      onClick={handleClick}
                    >
                      <i className="bi bi-filetype-csv" />
                      Upload Document
                    </button>
                    {errors && errors.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.file}
                      </div>
                    )}
                    {fileName && fileName}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <CSVLink
                      data={data}
                      filename={"download-sample.csv"}
                      className="btn btn-sm btn-light-success btn-responsive font-5vw"
                      target="_blank"
                    >
                      <i className="bi bi-filetype-csv" />
                      Download_Sample
                    </CSVLink>
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      type='button'
                      className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                      onClick={(e) => OnSubmit(e)}
                      disabled={postCSVLoading}
                    >
                      {!postCSVLoading && <span className='indicator-label'>Submit</span>}
                      {postCSVLoading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="Individual" title="Individual">
              <div className="card card-custom card-stretch gutter-b p-8">
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Client :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name='Indidual'
                          className='select2'
                          classNamePrefix='select'
                          handleChangeReactSelect={handleChangeIndidual}
                          options={IndidualOption}
                          value={SelectedIndidualOption}
                          isDisabled={!IndidualOption}
                        />
                        {error && error.client && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.client}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    null
                  )
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.tag && error.tag },
                        {
                          'is-valid': manualFormData.tag && !error.tag
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={manualFormData.tag || ''}
                    />
                    {error && error.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.tag}
                      </div>
                    )}
                  </div>
                </div>

                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Website :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Website'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.website && error.website },
                        {
                          'is-valid': manualFormData.website && !error.website
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='website'
                      autoComplete='off'
                      value={manualFormData.website || ''}
                    />
                    {error && error.website && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.website}
                      </div>
                    )}
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      className='btn btn-light-primary m-1 mt-8 font-5vw '
                      onClick={handelSubmit}
                      disabled={postManualWebLoading}
                    >
                      {!postManualWebLoading && <span className='indicator-label'>Submit</span>}
                      {postManualWebLoading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    rulesStore,
    BlockListlistStore,
    BlockListTypeStore,
    AddEmailToBlacklistKeysone,
    queueslistStore,
    BlockListUploadlistStore,
    BlockListEditlistStore,
    BlockListDeletelStore,
    BlockListUpdatelistStore,

    getWebAnalysisStore,
    PostSdkWebAnalysisStore,
    SdkManualWebAnalysisStore,
    DeleteWebAnalysisStore,
    EditWebAnalysisStore,
    UpdateWebAnalysisStore,
    clinetListStore,
    SdkexportlistStore
  } = state

  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : '',
    BlockListlists: BlockListlistStore && BlockListlistStore.BlockListlists ? BlockListlistStore.BlockListlists : {},
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType : {},
    BlockListUpdateSuccess: BlockListUpdatelistStore && BlockListUpdatelistStore.BlockListUpdatelists ? BlockListUpdatelistStore.BlockListUpdatelists : {},
    BlockEmailSuccess: AddEmailToBlacklistKeysone && AddEmailToBlacklistKeysone.emailSuccess ? AddEmailToBlacklistKeysone.emailSuccess : {},
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    BlocklistsUploads: BlockListUploadlistStore && BlockListUploadlistStore.BlocklistsUploads ? BlockListUploadlistStore.BlocklistsUploads : '',
    BlockListEditlists: BlockListEditlistStore && BlockListEditlistStore.BlockListEditlists ? BlockListEditlistStore.BlockListEditlists : '',
    DelteBlockList: BlockListDeletelStore && BlockListDeletelStore.DelteBlockList ? BlockListDeletelStore.DelteBlockList : '',

    postCSVWebAnalysis: PostSdkWebAnalysisStore && PostSdkWebAnalysisStore.postSdkCSVWebAnalysis ? PostSdkWebAnalysisStore.postSdkCSVWebAnalysis : '',
    postCSVLoading: PostSdkWebAnalysisStore && PostSdkWebAnalysisStore.loading ? PostSdkWebAnalysisStore.loading : '',
    postManualWebAnalysis: SdkManualWebAnalysisStore && SdkManualWebAnalysisStore.postManualSdkWebAnalysis ? SdkManualWebAnalysisStore.postManualSdkWebAnalysis : '',
    postManualWebLoading: SdkManualWebAnalysisStore && SdkManualWebAnalysisStore.loading ? SdkManualWebAnalysisStore.loading : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    exportLists: SdkexportlistStore && SdkexportlistStore.SdkExportLists ? SdkexportlistStore.SdkExportLists : '',
    exportLoading: SdkexportlistStore && SdkexportlistStore.loading ? SdkexportlistStore.loading : ''
  }
}

const mapDispatchToProps = dispatch => ({
  WebAnalysisDispatch: (data) => dispatch(PostWebAnalysisActions.postSdkWebAnalysis(data)),
  clearImportDispatch: (data) => dispatch(PostWebAnalysisActions.clearSdkWebAnalysis(data)),
  postManualWebAnalysisDispatch: (data) => dispatch(SdkManualWebAnalysisActions.postSdkManualWebAnalysis(data)),
  clearManualWebAnalysisDispatch: (data) => dispatch(SdkManualWebAnalysisActions.clearSdkManualWebAnalysis(data)),

  getWebAnalysisDispatch: (data) => dispatch(getWebAnalysisActions.getgetWebAnalysislist(data)),
  cleargetWebAnalysislistDispatch: (data) => dispatch(getWebAnalysisActions.cleargetWebAnalysislist(data)),
  
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  getExportDispatch: (data) => dispatch(SdkExportListActions.getSdkExportList(data)),
  getWrmRiskManagementlistDispatch: (params) => dispatch(wrmSdkActions.getWrmSdkManage(params)),
  clearExportListDispatch: (data) => dispatch(SdkExportListActions.clearSdkExportList(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WebRiskAnalysis)