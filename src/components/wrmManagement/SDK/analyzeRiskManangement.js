import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../../theme/helpers'
import {
  wrmSdkActions,
  SdkExportListActions,
  SDKBlockListEmailActions,
  PostCategoryActions,
  SdkBWListActions,
  clientCredFilterActions
} from '../../../store/actions'
import { connect } from 'react-redux'
import SearchList from './riskManagementsearchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage, getLocalStorage, removeLocalStorage } from '../../../utils/helper'
import { RISKSTATUS, REVIEWTYPE, SET_FILTER } from '../../../utils/constants'
import WebRiskAnalysis from './WebRiskAnalysis'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import BlockListCategory from './blockListCategory'
import { STATUS_RESPONSE } from '../../../utils/constants'
import PriceCheckPopup from '../../shared-components/PriceCheckPopup'
import FindRole from '../Role'
import BlockListCategory1 from './blockListCategory1'

function WrmRiskManagement(props) {
  const {
    getWrmRiskManagementlistDispatch,
    className,
    WrmRiskManagement,
    loading,
    exportLists,
    getExportDispatch,
    exportLoading,
    getBlockListTypeDispatch,
    PostCategory,
    BlockListTypes,
    getBlackWhiteLDispatch,
    BlackWhiteDataList,
    postPriceCheckSuccess,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentRoute = url && url[1]
  const [routeShow, setRouteShow] = useState()
  const [riskIdValue, setRiskIdValue] = useState()
  const [exportShow, setexportShow] = useState(false)
  const [exportBtn, setExportBtn] = useState(false)
  const [blockListValue, setblockListValue] = useState()
  const [blacklistDropdown, setBlacklistDropdown] = useState(null)
  const [Value, setValue] = useState(false)
  const csvReport = React.useRef()
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })
  const [manualFormData, setManualFormData] = useState({
    website: '',
    tag: '',
    riskStatus: '',
    reportStatus: ''
  })
const Api_key = getLocalStorage('SDKAPI')

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params)
    if (currentRoute === 'risk-management-search') {
      setRouteShow(false)
    } else {
      getWrmRiskManagementlistDispatch(pickByParams)
      const bloackListParams = {
        skipPagination: 'true',
        fieldType: 'Website_Category',
        queueId: "624fc67fae69dc1e03f47ebd"
      }
      getBlockListTypeDispatch(bloackListParams)
    }
    setexportShow(false)
    getBlackWhiteLDispatch()
    removeLocalStorage('TAG')
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      const pickByParams = _.pickBy(params)
      getWrmRiskManagementlistDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
    }
  }, [setFilterFunction, setCredFilterParams])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: activePageNumber,
      ...searchParams,
    }
    getWrmRiskManagementlistDispatch(params)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      tag: paginationSearch.tag ? paginationSearch.tag : '',
      riskStatus: paginationSearch.riskStatus ? paginationSearch.riskStatus : '',
      reportStatus: paginationSearch.reportStatus ? paginationSearch.reportStatus : ''
    }
    setActivePageNumber(pageNumber)
    getWrmRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getWrmRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getWrmRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count
      ? Math.ceil(parseInt(WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = '/merchant-login'
  }

  const reportCount = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.reportStatus
  const riskStatusCount = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.riskStatus
  const riskScoreStatus = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.riskScoreStatus
  const exportHide = JSON.parse(getLocalStorage('ExportHide'))
  const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))


  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === 'ok') {
      setexportShow(true)
      setExportBtn(true)
    }
  }, [exportLists])

  useEffect(() => {
    return (
      setexportShow(false),
      setValue(true),
      setExportBtn(false),
      setTimeout(() => {
        setValue(false)
      }, 1500)
    )
  }, [])

  const exportData = ((data) => {
    const params = {
      id: data
    }
    getExportDispatch(params)
  })

  const getIdValue = ((value, riskId) => {
    setblockListValue(value)
    setRiskIdValue(riskId)
  })


  useEffect(() => {
    if (
      PostCategory && PostCategory.status === STATUS_RESPONSE.SUCCESS_MSG ||
      postPriceCheckSuccess && postPriceCheckSuccess.status
    ) {
      const params = {
        ...searchParams,
        limit: limit,
        page: 1
      }
      getWrmRiskManagementlistDispatch(params)
    }
  }, [PostCategory, postPriceCheckSuccess])

  useEffect(() => {
    if (BlackWhiteDataList) {
      let blacktemp = []
      Object.keys(BlackWhiteDataList).forEach(function (key) {
        blacktemp.push({
          "value": BlackWhiteDataList[key].fieldValue,
          "label": BlackWhiteDataList[key].fieldValue,
        })
      })

      blacktemp.push({
        "value": "Others",
        "label": "Others",
      })
      setBlacklistDropdown(blacktemp)
    }
  }, [BlackWhiteDataList])

  const Website = ((name) => {
    window.open(name)
  })

  const hadelRefresh = (() => {
    getWrmRiskManagementlistDispatch(tagSearch)
    setexportShow(true)
  })

  const hadelReset = (() => {
    const params = {
      limit: limit,
      page: 1,
      ...searchParams,
    }
    getWrmRiskManagementlistDispatch(params)
    removeLocalStorage("WEBSITSEARCH")
    setexportShow(false)
    setValue(true)
    setExportBtn(false)
  })

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const SdkClientId = WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.result ? WrmRiskManagement.data.result[0] : ''
  const SdkClientName = SdkClientId && SdkClientId.clientId && SdkClientId.clientId.company ? SdkClientId.clientId.company : ''

  console.log('SdkClientId', SdkClientId)

  return (

    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="csvReport"
          className="download-table-xls-button"
          table="table-to-xlss"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className={`card ${className} ms-10 me-10`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-3'>
              <div className='col-md-6 mt-1 ms-2'>
                {WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: 
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {WrmRiskManagement && WrmRiskManagement.data && WrmRiskManagement.data.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-6 d-flex mt-4'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : 
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-9 col-lg-9 justify-content-end my-auto mt-4'>
              <div className='my-auto'>
                <WebRiskAnalysis exportBtn={exportBtn} setExportBtn={setExportBtn} exportShow={exportShow} Value={Value} setexportShow={setexportShow} />
              </div>
              <div className='my-auto'>
                <SearchList />
              </div>
              <div className='my-auto me-3'>
                {
                  exportShow ?
                    <>
                      <button
                        className='btn btn-sm btn-light-primary btn-responsive font-5vw me-2'
                        onClick={hadelRefresh}
                      >
                        {!loading && <span className='indicator-label'>
                          {/* <KTSVG path="/media/icons/duotune/general\gen056.svg"
                            /> */}
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-clockwise me-2" viewBox="0 0 16 16">
                            <path d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z" />
                            <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z" />
                          </svg>
                          Refresh
                        </span>
                        }
                        {loading && (
                          <span className='indicator-progress text-white' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                      <a
                        className='btn btn-sm btn-light-danger btn-responsive font-5vw'
                        onClick={hadelReset}
                      >
                        <KTSVG path="/media/icons/duotune/arrows\arr014.svg"
                        />
                        Reset
                      </a>
                    </>
                    : ''
                }
                {/* <Link
                  to='/merchant-login'
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                   <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                  Add Case
                </Link> */}
              </div>
            </div>
          </div>
          {
            exportShow ? (
              <div className='ms-2 mb-4'>
                <span className='fw-bolder d-flex fs-6'>
                  Tag - &nbsp{' '}
                  <span className='fw-bolder text-hover-primary text-primary'>
                    {paginationSearch && paginationSearch.tag ? paginationSearch.tag : ''}
                  </span>
                </span>
              </div>
            ) : ''
          }
          {
            exportShow ? (
              <div className='d-flex col-md-12 align-items-start'
                style={{
                  marginLeft: '10px'
                }}
              >
                {
                  !loading ? (
                    <div className='row'>
                      <FindRole
                        role={Role}
                      >
                        <div className='col-md-4'>
                          <div className='row'>
                            <div className='text-black-700 fw-bolder'>
                              CATEGORY STATUS
                            </div>
                            <div className='col-md-12 mt-4'>
                              <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                                PENDING
                                <span className=' text-hover-primary'>
                                  - {riskStatusCount && riskStatusCount.pendingCount ? riskStatusCount.pendingCount : '0'}
                                </span>
                              </span>
                              <span className='fw-bold mt-2 fs-8 me-4 text-info'>
                                PROCESSING
                                <span className=' text-hover-primary'>
                                  - {riskStatusCount && riskStatusCount.processingCount ? riskStatusCount.processingCount : '0'}
                                </span>
                              </span>
                              <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                                APPROVED
                                <span className=' text-hover-primary'>
                                  - {riskStatusCount && riskStatusCount.approvedCount ? riskStatusCount.approvedCount : '0'}
                                </span>
                              </span>
                              <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                                REJECTED
                                <span className='text-hover-primary'>
                                  - {riskStatusCount && riskStatusCount.rejectCount ? riskStatusCount.rejectCount : '0'}
                                </span>
                              </span>
                              <span className='text-warning fw-bold mt-2 fs-8'>
                                MANUAL REVIEW
                                <span className='text-hover-primary'>
                                  - {riskStatusCount && riskStatusCount.manualReviewCount ? riskStatusCount.manualReviewCount : '0'}
                                </span>
                              </span>
                            </div>
                          </div>
                        </div>
                      </FindRole>
                      <div className='col-md-5 col-lg-5'>
                        <div className='row'>
                          <div className='text-black-700 fw-bolder'>
                            <div className='row'>
                              <div className='col-md-12 col-lg-12'>
                                REPORT STATUS
                              </div>
                            </div>
                          </div>
                          <div className='col-md-12 mt-4'>
                            <span className='text-warning fw-bold mt-2 fs-8 me-2'>
                              PENDING
                              <span className='text-hover-primary'>
                                - {reportCount && reportCount.pendingReportCount ? reportCount.pendingReportCount : '0'}
                              </span>
                            </span>
                            <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                              REJECTED
                              <span className=' text-hover-primary'>
                                - {reportCount && reportCount.rejectedReportCount ? reportCount.rejectedReportCount : '0'}
                              </span>
                            </span>
                            <span className='text-info fw-bold mt-2 fs-8 me-2'>
                              DATA PROCESSING
                              <span className='text-hover-primary'>
                                - {reportCount && reportCount.dataProcessingCount ? reportCount.dataProcessingCount : '0'}
                              </span>
                            </span>
                            <span className='text-dark fw-bold mt-2 fs-8 me-2'>
                              DATA CAPTURED
                              <span className='text-hover-primary'>
                                - {reportCount && reportCount.dataCapturedCount ? reportCount.dataCapturedCount : '0'}
                              </span>
                            </span>
                          </div>
                          <div className='col-md-12 col-lg-12'>
                            <div
                              className='row'
                            >
                              <div className='col-md-12 mt-4'>
                                <span className='text-pending fw-bold mt-2 fs-8 me-4'>
                                  TAG PROCESSING
                                  <span className=' text-hover-primary'>
                                    - {reportCount && reportCount.tagProcessingReportCount ? reportCount.tagProcessingReportCount : '0'}
                                  </span>
                                </span>
                                <span className='text-warning fw-bold mt-2 fs-8 me-4'>
                                  WAITING FOR REPORT
                                  <span className=' text-hover-primary'>
                                    - {reportCount && reportCount.waitingForReportCount ? reportCount.waitingForReportCount : '0'}
                                  </span>
                                </span>
                                <span className='text-primary fw-bold mt-2 fs-8 me-4'>
                                  PREPARING REPORT
                                  <span className=' text-hover-primary'>
                                    - {reportCount && reportCount.preparingReportCount ? reportCount.preparingReportCount : '0'}
                                  </span>
                                </span>
                                <span className='text-success fw-bold mt-2 fs-8 me-4'>
                                  COMPLETED
                                  <span className=' text-hover-primary'>
                                    - {reportCount && reportCount.completedCount ? reportCount.completedCount : '0'}
                                  </span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-3'>
                        <div className='row'>
                          <div className='text-black-700 fw-bolder'>
                            RISK SCORE STATUS
                          </div>
                          <div className='col-md-12 mt-4'>
                            <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                              COMPLETED
                              <span className=' text-hover-primary'>
                                - {riskScoreStatus && riskScoreStatus.completedRiskScore ? riskScoreStatus.completedRiskScore : '0'}
                              </span>
                            </span>
                            <span className='fw-bold mt-2 fs-8 me-4 text-info'>
                              PENDING
                              <span className=' text-hover-primary'>
                                - {riskScoreStatus && riskScoreStatus.pendingRiskScoreCount ? riskScoreStatus.pendingRiskScoreCount : '0'}
                              </span>
                            </span>
                            <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                              PROCESSING
                              <span className=' text-hover-primary'>
                                - {riskScoreStatus && riskScoreStatus.processingRiskScoreCount ? riskScoreStatus.processingRiskScoreCount : '0'}
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className='col-md-12 d-flex justify-content-center'>
                      <div
                        className='spinner-border text-success'
                        role='status'
                      />
                    </div>
                  )
                }
              </div>
            ) :
              null
          }
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Status</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("status")}
                          >
                            <i
                              className={`bi ${sorting.status
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    {/* <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Review Type</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("status")}
                          >
                            <i
                              className={`bi ${sorting.status
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th> */}
                  </FindRole>
                  <th>
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {
                    SdkClientName === 'Ippopay' ? (null) : (
                      <th>
                        <div className="d-flex">
                          <span>Acquirer</span>
                          <div className="min-w-25px text-end">
                            <div
                              className="cursor-pointer"
                              onClick={() => handleSorting("Acquirer")}
                            >
                              <i
                                className={`bi ${sorting.Acquirer
                                  ? "bi-arrow-up-circle-fill"
                                  : "bi-arrow-down-circle"
                                  } text-primary`}
                              />
                            </div>
                          </div>
                        </div>
                      </th>
                    )
                  }
                  <th>
                    <div className="d-flex">
                      <span>Tag</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th>
                      <div className="d-flex w-150px">
                        Level 1 Category
                      </div>
                    </th>
                    <th>
                      <div className="d-flex w-150px">
                        Level 2 Category
                      </div>
                    </th>
                    <th>
                      <div className="d-flex w-150px">
                        <span>Update Tags</span>
                        <div className="min-w-25px text-end">
                          <div
                            className="cursor-pointer"
                            onClick={() => handleSorting("firstName")}
                          >
                            <i
                              className={`bi ${sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                                } text-primary`}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                  </FindRole>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Reason</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("reason")}
                        >
                          <i
                            className={`bi ${sorting.reason
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {/* <FindRole
                    role={Role}
                  >
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Manual Status</span>
                      </div>
                    </th>
                    <th className="min-w-100px text-center">
                      <div className="d-flex">
                        <span>Auto Status</span>
                      </div>
                    </th>
                  </FindRole> */}
                  <th>
                    <div className="d-flex">
                      <span>Report Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${sorting.ReportStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      WrmRiskManagement &&
                        WrmRiskManagement.data
                        ? (
                          WrmRiskManagement.data && WrmRiskManagement.data.result.map((riskmgmtlist, i) => {
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  {
                                    Role !== 'Client User' ? (
                                      <>
                                        <Link to={`/sdk-summary/update/${riskmgmtlist._id}?apiKey=${Api_key}`} className="ellipsis">
                                          IRM{riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"}
                                        </Link>
                                      </>
                                    ) : (
                                      Role === 'Client User' && riskmgmtlist.riskStatus === "APPROVED" ? (
                                        <Link to={`/sdk-summary/update/${riskmgmtlist._id}?apiKey=${Api_key}`} className="ellipsis">
                                          IRM{riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"}
                                        </Link>
                                      ) : (
                                        <span>
                                          IRM{riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"}
                                        </span>
                                      )
                                    )
                                  }
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.riskStatus && riskmgmtlist.riskStatus]}`}>
                                      {riskmgmtlist.riskStatus ? riskmgmtlist.riskStatus : "--"}
                                    </span>
                                  </td>
                                  {/* <td className="ellipsis">
                                    <span className={`badge ${REVIEWTYPE[riskmgmtlist.reviewType && riskmgmtlist.reviewType ? riskmgmtlist.reviewType : '--']}`}>
                                      {riskmgmtlist.reviewType ? riskmgmtlist.reviewType : "--"}
                                    </span>
                                  </td> */}
                                </FindRole>
                                <td className="ellipsis">
                                  <a
                                    href={riskmgmtlist.website}
                                  // Onclick={() => { Website(riskmgmtlist.website)}}
                                  >
                                    {riskmgmtlist.website ? riskmgmtlist.website : "--"}
                                  </a>
                                </td>
                                {
                                  SdkClientName === 'Ippopay' ? (null) : (
                                    <td className="ellipsis">
                                      {riskmgmtlist.acquirer ? riskmgmtlist.acquirer : "--"}
                                    </td>
                                  )
                                }
                                <td className="ellipsis">
                                  {riskmgmtlist.tag ? riskmgmtlist.tag : "--"}
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td>
                                    <BlockListCategory1
                                      blacklistDropdown={blacklistDropdown}
                                      manualCategorys={riskmgmtlist.level1Category}
                                      blockListValue={riskmgmtlist._id}
                                      riskIdValueId={riskmgmtlist.riskId}
                                    />
                                  </td>
                                  <td>
                                    <BlockListCategory
                                      blacklistDropdown={blacklistDropdown}
                                      manualCategorys={riskmgmtlist.manualCategory}
                                      blockListValue={riskmgmtlist._id}
                                      riskIdValueId={riskmgmtlist.riskId}
                                    />
                                  </td>
                                  <td>
                                    {
                                      riskmgmtlist.policyComplainceChecks === true && riskmgmtlist.riskStatus === 'APPROVED' ?
                                        <span
                                          onClick={() => getIdValue(riskmgmtlist._id, riskmgmtlist.riskId)}
                                        >
                                          <PriceCheckPopup
                                            blockListValue={blockListValue}
                                          />
                                        </span>
                                        : '--'
                                    }
                                  </td>
                                </FindRole>
                                <td>
                                  {riskmgmtlist.reason ? riskmgmtlist.reason : "--"}
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[riskmgmtlist.reportStatus && riskmgmtlist.reportStatus]}`}>
                                    {riskmgmtlist.reportStatus ? riskmgmtlist.reportStatus : "--"}
                                  </span>
                                </td>
                                {/* <FindRole
                                  role={Role}
                                >
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.manualStatus && riskmgmtlist.manualStatus]}`}>
                                      {riskmgmtlist.manualStatus ? riskmgmtlist.manualStatus : "--"}
                                    </span>
                                  </td>
                                  <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist.autoStatus && riskmgmtlist.autoStatus]}`}>
                                      {riskmgmtlist.autoStatus ? riskmgmtlist.autoStatus : "--"}
                                    </span>
                                  </td>
                                </FindRole> */}
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { WrmSdkManageStore, exportlistStore, PostCategoryStore, SdkBlockListTypeStore, SdkBWlistStore, PriceStore } = state
  return {
    WrmRiskManagement: state && state.WrmSdkManageStore && state.WrmSdkManageStore.wrmSdkManage,
    loading: WrmSdkManageStore && WrmSdkManageStore.loading ? WrmSdkManageStore.loading : false,
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : false,
    BlockListTypes: SdkBlockListTypeStore && SdkBlockListTypeStore.BlockListType ? SdkBlockListTypeStore.BlockListType.data : null,
    PostCategory: PostCategoryStore && PostCategoryStore.PostCategory ? PostCategoryStore.PostCategory?.data : '',
    BlackWhiteDataList: SdkBWlistStore && SdkBWlistStore.BWLists && SdkBWlistStore.BWLists.data ? SdkBWlistStore.BWLists.data?.data : '',
    postPriceCheckSuccess: PriceStore && PriceStore.priceSuccess ? PriceStore.priceSuccess : null,
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getWrmRiskManagementlistDispatch: (params) => dispatch(wrmSdkActions.getWrmSdkManage(params)),
  getExportDispatch: (data) => dispatch(SdkExportListActions.getSdkExportList(data)),
  getBlockListTypeDispatch: (params) => dispatch(SDKBlockListEmailActions.getSdkBlockListType(params)),
  ClearPostCategroy: () => dispatch(PostCategoryActions.ClearPostCategroy()),
  getBlackWhiteLDispatch: () => dispatch(SdkBWListActions.getSdkBWList()),
  setFilterFunctionDispatch: (data) =>
    dispatch(clientCredFilterActions.setFilterFunction(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(WrmRiskManagement)
