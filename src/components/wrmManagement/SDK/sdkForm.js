import React, { useState, useEffect } from 'react'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import color from '../../../utils/colors'
import _ from 'lodash'
import { connect } from 'react-redux'
import { clientIdLIstActions, apiKeyActions, wrmSdkActions } from '../../../store/actions'
import { setLocalStorage, removeLocalStorage } from '../../../utils/helper'
import { Link } from 'react-router-dom'
import { warningAlert } from "../../../utils/alerts"

function SDKCBForm(props) {
  const {
    getClientDispatch,
    getClient,
    loadingClient,
    getApiKeyDispatch,
    getWrmRiskManagementlistDispatch,
    getApiKey,
    clearApiKeyDispatch,
    loadingApi
  } = props
  const [errors, setErrors] = useState()
  const [selectedClientOption, setSelectedClientOption] = useState('')
  const [clientOption, setClientOption] = useState()
  const [show, SetShow] = useState(false)
  const [copyText, setCopyText] = useState('Copy')
  const [bankId, SetBankId] = useState()
  const [height] = useState(600)
  const [formData, setFormData] = useState({
    clientId: ''
  })

  useEffect(() => {
    getClientDispatch()
    removeLocalStorage("SDKROUTE")
  }, [])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const generate = () => {
    const errors = {}
    if (_.isEmpty(formData.clientId)) {
      errors.clientId = "Client Id Is Required"
    }
    if (_.isEmpty(errors)) {
      getApiKeyDispatch(formData.clientId)
    }
    setErrors(errors)
  }

  const copyClip = () => {
    const copyText = document.getElementById('iframeInput')
    copyText.select()
    copyText.setSelectionRange(0, 99999)
    navigator.clipboard.writeText(copyText.value)
    setCopyText('Copied')
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data && data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getClient && getClient.data && getClient.data.result, 'company')
    setClientOption(data)
  }, [getClient])

  const handleChangeClient = selectedOption => {
    if (selectedOption !== null) {
      setSelectedClientOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value }))
      setLocalStorage('SDKCLIENT', JSON.stringify(selectedOption))
    } else {
      setSelectedClientOption()
      setFormData(values => ({ ...values, clientId: '' }))
    }
    setErrors({ ...errors, clientId: '' })
  }

  useEffect(() => {
    if (copyText === 'Copied') {
      setTimeout(() => {
        setCopyText('Copy')
      }, 3000)
    }
  }, [copyText])

  useEffect(() => {
    if (!_.isEmpty(getApiKey && getApiKey.data && getApiKey.data.apiKey)) {
      setLocalStorage('SDKAPI',
        getApiKey && getApiKey.data && getApiKey.data.apiKey
      )
      SetBankId(getApiKey && getApiKey.data && getApiKey.data.apiKey)
      SetShow(true)
    } else if (getApiKey && getApiKey.status === 'error') {
      warningAlert(
        'error',
        "Please Generate Api Key",
        '',
        'Try again',
        '',
        () => { }
      )
      clearApiKeyDispatch()
    }
  }, [getApiKey])


  return (
    <div className='card-header bg-skyBlue py-10'>
      <div className='card-body'>
        <form className='container-fixed '>
          <div className='card-body'>
            <div className='row'>
              <div className='d-flex justify-content-center my-auto'>
                <label className='font-size-xs font-weight-bold mb-3 mt-2 text-center form-label'>
                  Client Id : &nbsp;
                </label>
                <div className='col-lg-5 pr-3 me-3'>
                  <ReactSelect
                    isClearable
                    styles={customStyles}
                    isMulti={false}
                    name='clientId'
                    className='basic-single'
                    classNamePrefix='select'
                    handleChangeReactSelect={handleChangeClient}
                    options={clientOption}
                    value={selectedClientOption}
                    isLoading={loadingClient}
                  />
                  {errors && errors.clientId && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.clientId}
                    </div>
                  )}
                </div>
                <div>
                  <Link
                    type='button'
                    className='btn btn-sm btn-light-danger fa-pull-right'
                    data-dismiss='modal'
                    to='/wrm-riskmmanagement'
                  >
                    Back
                  </Link>
                  <button
                    type='button'
                    className='btn btn-sm btn-light-primary fa-pull-right  me-4'
                    onClick={generate}
                    data-dismiss='modal'
                    disabled={loadingApi}
                  >
                    {!loadingApi && <span className='indicator-label'>Generate</span>}
                    {loadingApi && (
                      <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
          {show
            ? (
              <div className='card-header py-10'>
                <div className='card-body'>
                  <form className='container-fixed '>
                    <div className='card-body'>
                      <div className='row'>
                        <div className='col-lg-2 mb-3 ms-10'>
                          <label className='font-size-xs font-weight-bold mb-3  form-label'>
                            SDK Code
                          </label>
                        </div>
                        <input
                          type='text'
                          value={`https://irm-chargebackzero.web.app/wrm-riskmmanagement-sdk?apiKey=${bankId}`}
                          // value={`http://localhost:3000/wrm-riskmmanagement-sdk?apiKey=${bankId}`}
                          className='d-none'
                          id='iframeInput'
                          onChange={() => { }}
                        />
                        <div className='col-lg-10'>
                          <div className='card p-3 ms-10' style={{ height: '100%' }}>
                            <div className='d-flex my-auto justify-content-between'>
                              <pre className='my-auto'>
                                &lt;iframe
                                src="https://irm-chargebackzero.web.app/wrm-riskmmanagement-sdk?apiKey=
                                {bankId}" width='100%' height='100%' /&gt;
                              </pre>
                              <button
                                type='button'
                                className='btn btn-sm btn-light-primary'
                                data-bs-toggle='tooltip'
                                data-bs-placement='top'
                                title='Copy to Clipboard'
                                onClick={() => {
                                  copyClip()
                                }}
                              >
                                {copyText}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )
            : null}
          {show
            ? (
              <div className='card-header'>
                <div className='card-body'>
                  <form className='container-fixed '>
                    <div className='card-body'>
                      <div className='row'>
                        <div className='col-lg-2 mb-3'>
                          <label className='font-size-xs font-weight-bold mb-3  form-label ms-10'>
                            SDK View
                          </label>
                        </div>
                        <div className='col-lf-10'>
                          <div className='card p-3' style={{ height }}>
                            <iframe
                              src={`https://irm-chargebackzero.web.app/wrm-riskmmanagement-sdk?apiKey=${bankId}`}
                              // src={`http://localhost:3000/wrm-riskmmanagement-sdk?apiKey=${bankId}`}
                              width='100%'
                              height='100%'
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )
            : null}
        </form>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => ({
  getClient: state && state.clinetListStore && state.clinetListStore.clinetIdLists,
  loadingClient: state && state.clinetListStore && state.clinetListStore.loading,
  getApiKey: state && state.apiKeyStore && state.apiKeyStore.getApiKeyAllData && state.apiKeyStore.getApiKeyAllData.data,
  loadingApi: state && state.apiKeyStore && state.apiKeyStore.loading,
})

const mapDispatchToProps = (dispatch) => ({
  getClientDispatch: (params) => dispatch(clientIdLIstActions.getclientIdList(params)),
  getApiKeyDispatch: (id) => dispatch(apiKeyActions.getApiKey(id)),
  clearApiKeyDispatch: (id) => dispatch(apiKeyActions.clearApiKey(id)),
  getWrmRiskManagementlistDispatch: (params) => dispatch(wrmSdkActions.getWrmSdkManage(params)),

})

export default connect(mapStateToProps, mapDispatchToProps)(SDKCBForm)
