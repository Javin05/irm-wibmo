import React, { Fragment } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import moment from 'moment'

function TransationLaundring(props) {
  const {
    reviewAnalysis,
    DashboardExportData
  } = props

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Transaction Laundering
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0 ms-4 mb-4'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6 mb-4'>
                    <span className='text-dark fw-bold mb-1 fs-5 ms-4 ms-4'>
                      URL redirection
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                    {
                      DashboardExportData && DashboardExportData.websiteRedirection
                        ? DashboardExportData.websiteRedirection : 'No Data'
                    }
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6 mb-4'>
                    <span className='text-dark fw-bold mb-1 fs-5 ms-4'>
                      AML Sanction Screening
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      Not part of Screened list
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6 mb-4'>
                    <span className='text-dark fw-bold mb-1 fs-5 ms-4'>
                      MCC Coding Mismatch(High Risk
                    </span>
                    <br />
                    <span className='text-dark fw-bold mb-1 fs-5 ms-4'>
                      Prohibited/Restricted Category)</span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                    {
                      DashboardExportData && DashboardExportData.riskClassification
                        ? DashboardExportData.riskClassification : 'No Data'
                    }
                    </span>
                  </div>
                <div className='col-lg-6 col-md-6 col-sm-6 mt-4'>
                  <span className='text-dark fw-bold mb-1 fs-5 ms-4'>
                    Website Redirection URL
                  </span>
                </div>
                <div className='col-lg-6 col-md-6 col-sm-6 mt-4'>
                  <span className='text-muted fw-semibold text-capital'>
                    {
                      DashboardExportData && DashboardExportData.websiteRedirectionURL
                        ? DashboardExportData.websiteRedirectionURL : 'No Data'
                    }
                  </span>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default TransationLaundring