import React, { Fragment } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import moment from 'moment'

function GooggleReview(props) {
  const {
    reviewAnalysis,
    DashboardExportData
  } = props

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Google Review Analysis
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5  ms-6'>
                      Average Rating
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.merchantIntelligence
                          ? DashboardExportData.merchantIntelligence : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5  ms-6'>
                      Status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {reviewAnalysis && reviewAnalysis.status ? reviewAnalysis.status : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default GooggleReview