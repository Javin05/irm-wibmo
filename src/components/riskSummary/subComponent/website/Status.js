import React, { Fragment } from 'react'
import _ from 'lodash'

function Status(props) {
  const {
    websiteLink,
    websitevalue,
    BusinessName
  } = props

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8 ms-4' >
        <div className='col-lg-12'>
          {/* <div className='d-flex justify-content-between align-items-start flex-wrap mb-2'> */}
            <div className='row'>
              <div className='col-lg-12'>
                <div className="card-toolbar d-flex mt-5">
                  <>
                    <ul className="nav">
                      <li className="nav-item">
                        <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-warning"
                          data-toggle='modal'
                          data-target='#approveModal'
                        // onClick={() => { setActive(true) }}
                        >
                          Open
                        </a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success"
                          data-toggle='modal'
                          data-target='#rejectModal'
                        // onClick={() => { setActive(true) }}
                        >
                          Approve
                        </a>
                      </li>

                      <li className="nav-item">
                        <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                          data-toggle='modal'
                          data-target='#rejectModal'
                        // onClick={() => { setActive(true) }}
                        >
                          Decline
                        </a>
                      </li>
                    </ul>
                  </>
                </div>
              </div>
            </div>
          {/* </div> */}
        </div>
      </div>
    </>
  )
}

export default Status