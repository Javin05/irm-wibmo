import React, { Fragment, useEffect, useState } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import { toAbsoluteUrl } from '../../../../theme/helpers'

function WebsiteContent(props) {
  const {
    DashboardExportData,
    merchantSummary
  } = props
  
  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className="card-title align-items-start flex-column">
                <span className='card-label fw-bolder text-dark fs-3'>
                  Website Categorization
                </span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      <a
                        className='color-primary cursor-pointer'
                        onClick={() => window.open(DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'
                          ?
                          DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'
                          : 'No Data', "_blank")}
                      >
                        {
                          DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'
                        }
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Code
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.productCategoryCode
                          ? DashboardExportData.productCategoryCode : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Risk Classification
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.riskClassification
                          ? DashboardExportData.riskClassification : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Risk Level
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.riskLevel
                          ? DashboardExportData.riskLevel : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Categories
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.reason ? DashboardExportData.reason : 'No data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Paid Up Capital
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.paidUpCapital ? DashboardExportData.paidUpCapital : 'No data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Google Analytics Id Relation
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.googleAnalyticsIdRelation ? DashboardExportData.googleAnalyticsIdRelation : 'No data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Website Details
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      LegalName
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.legalName
                          ? DashboardExportData.legalName : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Logo
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      <img
                        src={DashboardExportData && DashboardExportData.logo ? DashboardExportData.logo : 'No Data'}
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Organization Address - Transparency
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Redirection
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Card Scheme Compliance(BRAM/GBPP)
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Adult Content Monitoring
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.adultContentMonitoring ? DashboardExportData.adultContentMonitoring : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Predict Medicine
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                    {DashboardExportData && DashboardExportData.predictMedicine ? DashboardExportData.predictMedicine : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Merchant's Policies Url
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Terms & Condition
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.termsAndConditionPageUrl ? DashboardExportData.termsAndConditionPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Shipping Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.shippingPolicyPageUrl ? DashboardExportData.shippingPolicyPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Return Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.returnPolicyPageUrl ? DashboardExportData.returnPolicyPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Contact Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.contactUsPageUrl ? DashboardExportData.contactUsPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Privacy Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.privacyPolicyPageUrl ? DashboardExportData.privacyPolicyPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Purchase Or Registration
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Id Submission prompt
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Web Analysis Status
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Activity Check
                  </span>
                </h4>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Is Mining Happening?
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageActivityCheckMining ? DashboardExportData.pageActivityCheckMining : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Any Untrusted Downloads?
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageActivityCheckUntrustedDownloads ? DashboardExportData.pageActivityCheckUntrustedDownloads : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Health Check
                  </span>
                </h4>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Content Accessibilty
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.pageHealthCheckContentAccessibilty ? DashboardExportData.pageHealthCheckContentAccessibilty : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Page loading Time
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.pageHealthCheckPageLoadingTime ? DashboardExportData.pageHealthCheckPageLoadingTime : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Links Connectivity Check
                  </span>
                </h4>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Status
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.pageAvailabilityCheckURLStatus ? DashboardExportData.pageAvailabilityCheckURLStatus : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center rounded p-2 mb-0 ms-8'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Sucess Rate
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageLinksConnectivityCheckSuccessRate ? DashboardExportData.pageLinksConnectivityCheckSuccessRate : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Redirection Check
                  </span>
                </h4>
              </div> */}
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  State banned Category
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Address Scrapped From Website
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.addressScrappedFromWebsite}
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Address From Google Map
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.addressFromGoogleMap}
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Matched Address From Google Map
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.matchedAddressFromGoogleMap}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Malware Risk
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Malware Risk
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.malwareRisk
                          ? DashboardExportData.malwareRisk : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Malware Present
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.malwarePresent
                          ? DashboardExportData.malwarePresent : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Content Monitoring
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Currency
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.currenciesFoundOnWebsite
                          ? DashboardExportData.currenciesFoundOnWebsite : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Price Listing
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.productsPriceListedInWebsite
                          ? DashboardExportData.productsPriceListedInWebsite : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Unreasonable Price
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.websiteContainsUnreasonablePrice
                          ? DashboardExportData.websiteContainsUnreasonablePrice : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Accessibility
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt
                          ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Average Price
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.averageProductPrice
                          ? DashboardExportData.averageProductPrice : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Min Price Listed 
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.minPriceListedInHomePage
                          ? DashboardExportData.minPriceListedInHomePage : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Max Price Listed
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.maxPriceListedInHomePage
                          ? DashboardExportData.maxPriceListedInHomePage : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </>
  )
}

export default WebsiteContent