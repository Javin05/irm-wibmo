import _ from 'lodash'

export const setWRMCommentData = (data) => {
    if (!_.isEmpty(data)) {
        return {
            riskStatus: data && data.riskStatus ? data.riskStatus : '',
            comments: data && data.comments ? data.comments : ''
        }
    }
}