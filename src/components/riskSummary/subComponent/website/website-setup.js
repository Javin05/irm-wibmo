import React, { Fragment } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import moment from 'moment'
import { toAbsoluteUrl } from '../../../../theme/helpers'

function  WebsiteSetup(props) {
  const {
    domainRepetation,
    DashboardExportData
  } = props

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Domain Info
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Registration Company
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Registered
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.domainRegistered ? DashboardExportData.domainRegistered : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Rank
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.domainRank ? DashboardExportData.domainRank : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Registrant
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5 me-2'>
                      Domain Registered Date
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        !_.isEmpty(DashboardExportData.domainRegistrationDate) ?
                            DashboardExportData.domainRegistrationDate
                          : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5 me-2'>
                      Domain Expiry Date
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                        !_.isEmpty(DashboardExportData.domainRegistrationExpiryDate) ?
                            DashboardExportData.domainRegistrationExpiryDate
                          : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Name Servers
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Logo
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      <img
                        src={
                          DashboardExportData && DashboardExportData.logo
                            ? DashboardExportData.logo : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        referrerpolicy="no-referrer"
                      />
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> */}
{/* 
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Administrative Contact
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      State
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                      DashboardExportData && DashboardExportData.administrativeContactState ? DashboardExportData.administrativeContactState : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      City
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactCity ? DashboardExportData.administrativeContactCity : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Country
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactCountry ? DashboardExportData.administrativeContactCountry : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Country Code
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeCountryCode ? DashboardExportData.administrativeCountryCode : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Email
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactEmail ? DashboardExportData.administrativeContactEmail : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactName ? DashboardExportData.administrativeContactName : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Administrative Contact Telephone
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactTelephone ? DashboardExportData.administrativeContactTelephone : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Administrative Contact Organization
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactOrganization ? DashboardExportData.administrativeContactOrganization : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Administrative Contact Postal Code
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactPostalCode ? DashboardExportData.administrativeContactPostalCode : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Administrative Contact Street1
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.administrativeContactStreet1 ? DashboardExportData.administrativeContactStreet1 : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Estimated Domain Age
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                      DashboardExportData && DashboardExportData.estimatedDomainAge ? DashboardExportData.estimatedDomainAge : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Geo
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='row mb-2 align-items-cente'>
                <div className='mt-4'>
                  <div className='align-items-center  rounded p-2 mb-0'>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          City
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoCity ? DashboardExportData.geoCity : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Country
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoCountry ? DashboardExportData.geoCountry : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Country Code
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoCountryCode ? DashboardExportData.geoCountryCode : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Latitude
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoLatitude ? DashboardExportData.geoLatitude : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Longitude
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoLongitude ? DashboardExportData.geoLongitude : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          postal Code
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoPostalCode ? DashboardExportData.geoPostalCode : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          State
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoState ? DashboardExportData.geoState : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          State Code
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoStateCode ? DashboardExportData.geoStateCode : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Street Address
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoStreetAddress ? DashboardExportData.geoStreetAddress : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Street Name
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoStreetName ? DashboardExportData.geoStreetName : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                    <div className='row mb-2'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5 pl-3 ms-6'>
                          Street Number
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold text-gray-700'>
                        {
                      DashboardExportData && DashboardExportData.geoStreetNumber ? DashboardExportData.geoStreetNumber : "No Data"
                      }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> */}

        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  SSL Check
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5 ellipsis me-2'>
                      Ssl Certificate Check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.sslCertificateCheck ? DashboardExportData.sslCertificateCheck : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default WebsiteSetup