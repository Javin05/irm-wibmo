import React, { Fragment } from 'react'
import _ from 'lodash'
import { Link } from "react-router-dom"
import moment from 'moment'

function WebsiteTraffic(props) {
  const {
    reviewAnalysis
  } = props

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <span className='mt-8'>
        No Data
        </span>
        {/* <div className='col-xl-3'>
          <div className='card card-xl-stretch mb-xl-8 bg-light-warning'>
            <div className='ms-4 border-0 pt-5'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-warning'>
                  Traffic / Month
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='d-flex justify-content-end me-4  rounded p-2 mb-0'>
                <span className='color1 fw-semibold fs-4'>
                  48102
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-3'>
          <div className='card card-xl-stretch mb-xl-8 backgroundcolor2'>
            <div className='ms-4 border-0 pt-5'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-primary'>
                  Organic Traffic
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='d-flex justify-content-end me-4 rounded p-2 mb-0'>
                <span className='color2 fw-semibold fs-4'>
                  47928
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-3'>
          <div className='card card-xl-stretch mb-xl-8 bg-light-danger'>
            <div className='ms-4 border-0 pt-5'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-danger'>
                  Paid Traffic
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='d-flex justify-content-end me-4 rounded p-2 mb-0'>
                <span className='color3 fw-semibold fs-4'>
                  84
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-3'>
          <div className='card card-xl-stretch mb-xl-8 bg-light-success'>
            <div className='ms-4 border-0 pt-5'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-success'>
                  Bounce Rate
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='d-flex justify-content-end me-4 rounded p-2 mb-0'>
                <span className='color4 fw-semibold fs-4'>
                  27.96%
                </span>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </>
  )
}

export default WebsiteTraffic