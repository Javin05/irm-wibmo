import React, { Fragment, useEffect, useState } from 'react'
import _ from 'lodash'
import { useLocation, Link } from "react-router-dom"
import moment from 'moment'
import PDF from '../pdf/Pdf'
import jsPDF from "jspdf"
import 'react-circular-progressbar/dist/styles.css'
import { renderToString } from "react-dom/server"
import ReactSpeedometer from "react-d3-speedometer"
import Status from './Status'
import { connect } from 'react-redux'
import { TagSummaryAction, ExportListActions } from '../../../../store/actions'
import { WRMCommentdeleteActions, WRMCommentActions } from '../../../../store/actions'
import { getLocalStorage, removeLocalStorage, getUserPermissions } from '../../../../utils/helper'
import { warningAlert, successAlert, confirmationAlert } from "../../../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import autoTable from 'jspdf-autotable'
import ReactPDF from '@react-pdf/renderer';
import { pdf } from "@react-pdf/renderer";
import { saveAs } from 'file-saver';
import { PMASTATUS, UPDATE_PERMISSION, STATUS_RESPONSE } from '../../../../utils/constants'
import { pdfjs } from 'react-pdf';
import { KTSVG } from '../../../../theme/helpers'
import { Document, Page } from 'react-pdf';
import WRMStatus from './wrmARH'
import CommentEdit from "./wrmCommentEdit";
import html2canvas from 'html2canvas';

function Score(props) {
  const {
    websiteLink,
    websitevalue,
    BusinessName,
    getExportDispatch,
    exportLoading,
    exportLists,
    clearExportListDispatch,
    BsName,
    websiteData,
    successVerifyDomain,
    DNSData,
    matrixDetail,
    adminContact,
    adminContactData,
    websitetobusinessmatch,
    websiteCatgories,
    ValidData,
    webAnalysis,
    LogoCheck,
    websiteImageDetect,
    userInteractionChecks,
    domainInfoData,
    domainRepetation,
    registerData,
    successWhoDomain,
    websiteCategorizationV1,
    reviewAnalysis,
    merchantIddetails,
    id,
    DashboardExportData,
    DashboardPmaLists,
    WRMlists,
    WRMlistsloading,
    DeleteWRMData,
    getWRMCommentDispatch,
    deleteWRMCommentDispatch,
    clearDeleteWRMCommentDispatch,
    merchantSummary
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('risk-summary/update/')
  const currentId = url && url[1]
  const getUsersPermissions = getUserPermissions(pathName, true)

  const [editMode, setEditMode] = useState(false)
  const [userId, setUserId] = useState('')
  const [loadingPdf, setLoadingPdf] = useState(false);

  const pmaData = DashboardPmaLists && DashboardPmaLists.data ? DashboardPmaLists.data : ''
  const pma = pmaData && pmaData.pma ? pmaData.pma : ''
  const pmaCheck = pmaData && pmaData.pmaCheck ? pmaData.pmaCheck : ''

  const [numPages, setNumPages] = useState();
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  const print = async () => {
    setLoadingPdf(true);
    const pdf = new jsPDF(`portrait`, `mm`, ``, true);
    const imgWidth = 190;
    const totalPages = 14;
    for (let pageNumber = 1; pageNumber <= totalPages; pageNumber++) {
      const canvas = await html2canvas(document.querySelector(`#page${pageNumber}`), { logging: true, letterRendering: 1, useCORS: true });
      const imageData = canvas.toDataURL('image/png');
      let imgHeight = (canvas.height * imgWidth) / canvas.width;
      pdf.addImage(imageData, 'PNG', 10, 0, imgWidth, imgHeight+30);
      if (pageNumber < totalPages) {
        pdf.addPage();
      }
    }
    pdf.save(`${BsName}${id}.pdf`);
    setLoadingPdf(false);
  };

  const exported = () => {
    const params = {
      website: websiteLink
    }
    getExportDispatch(params)
  }

  const clear = () => {
    clearExportListDispatch()
  }

  useEffect(() => {
    const params = {
      riskId: currentId
    }
    getWRMCommentDispatch(params)
  }, [currentId])

  const onDeleteItem = (id) => {
    confirmationAlert(
      "Are you sure want to delete this comment,",
      "",
      'warning',
      'Yes',
      'No',
      () => {
        deleteWRMCommentDispatch(id)
      },
      () => { { } }
    )
  }

  useEffect(() => {
    if (DeleteWRMData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const params = {
        riskId: currentId
      }
      successAlert(
        DeleteWRMData && DeleteWRMData.message,
        'success'
      )
      getWRMCommentDispatch(params)
      clearDeleteWRMCommentDispatch()
    } else if (DeleteWRMData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        DeleteWRMData.message,
        '',
        'Ok'
      )
    }
    clearDeleteWRMCommentDispatch()
  }, [DeleteWRMData])


  return (
    <>
      <div className='zn-1'>
        <PDF
          websiteData={websiteData}
          successVerifyDomain={successVerifyDomain}
          DNSData={DNSData}
          matrixDetail={matrixDetail}
          adminContact={adminContact}
          adminContactData={adminContactData}
          BsName={BsName}
          websitetobusinessmatch={websitetobusinessmatch}
          websiteCatgories={websiteCatgories}
          ValidData={ValidData}
          webAnalysis={webAnalysis}
          LogoCheck={LogoCheck}
          websiteImageDetect={websiteImageDetect}
          userInteractionChecks={userInteractionChecks}
          domainInfoData={domainInfoData}
          domainRepetation={domainRepetation}
          registerData={registerData}
          successWhoDomain={successWhoDomain}
          websiteCategorizationV1={websiteCategorizationV1}
          reviewAnalysis={reviewAnalysis}
          websiteLink={websiteLink}
          merchantIddetails={merchantIddetails}
          DashboardExportData={DashboardExportData}
          merchantSummary={merchantSummary}
          DashboardPmaLists={DashboardPmaLists}
        />
      </div>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="simple"
          sheet="tablexls"
        />
      </div>

      <div className='row g-5 g-xl-8' style={{ backgroundColor: '#f5f8fa' }}>
        <div className='col-lg-12'>
          <div className="card card-xl-stretch">
            <div className="card-body pt-0">
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <span className='d-flex justify-content-center mt-4 fw-boldest my-1 fs-3'>
                    Website
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer'
                      onClick={() => window.open(websiteLink)}
                    >- {websiteLink}</a>
                  </span>
                </div>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-start mt-4 fw-boldest my-1 fs-3'>
                    Company Name
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer text-capital'
                    >{!_.isEmpty(BusinessName) ? BusinessName : BsName}</a>
                  </div>
                </div>
              </div>
              <div className="separator separator-dashed my-3" />
              <div className='row'>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-center'
                  >
                    <ReactSpeedometer
                      maxValue={100}
                      value={parseInt(DashboardExportData && DashboardExportData.riskScore)}
                      customSegmentStops={[0, 35, 75, 100]}
                      segmentColors={["limegreen", "gold", "tomato"]}
                      needleColor="red"
                      startColor="green"
                      segments={10}
                      endColor="blue"
                      className='pichart'
                      currentValueText="Over All Score: #{value}"
                      currentValuePlaceholderStyle={"#{value}"}
                    />
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div className='d-flex align-items-center justify-content-between px-2'>
                    <button
                      onClick={() => {
                        setLoadingPdf(true);
                        print();
                      }}
                      className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'
                      // onClick={(e) => exported(e)}
                      disabled={loadingPdf}
                    >
                      {!loadingPdf &&
                        <span className='indicator-label'>
                          <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                          PDF Report
                        </span>
                      }
                      {loadingPdf && (
                        <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}

                    </button>
                    {/* <Can
                      permissons={getUsersPermissions}
                      componentPermissions={UPDATE_PERMISSION}
                    > */}
                    <WRMStatus getWRMCommentDispatch={getWRMCommentDispatch} />
                    {/* </Can> */}
                  </div>
                  <div>
                    {WRMlists && WRMlists.data && WRMlists.data.length > 0 ?
                      <h2 className='mb-2 d-flex justify-content-start symbol-label text-black mb-5'>Comments</h2>
                      : null
                    }
                    {
                      WRMlists && Array.isArray(WRMlists.data) > 0 ? WRMlists.data.map((item, i) => {
                        return (
                          <ul style={{ listStyleType: 'disc', paddingLeft: '1.5rem' }}>
                            <div className="d-flex justify-content-between align-items-center mb-2 w-75">
                              <div>
                                <div className="d-flex align-item-center">
                                  <div>
                                    <li>{item && item.riskStatus}&nbsp;: </li>
                                  </div>
                                  <div>
                                    &nbsp;{item && item.comments}
                                  </div>
                                </div>
                                <div>
                                  Created Date&nbsp;: {moment(item && item.createdAt).format('DD/MM/YYYY')}
                                </div>
                              </div>
                              <div className="d-flex align-item-center px-4">
                                <button
                                  className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px'
                                  title="Edit Data"
                                  onClick={() => {
                                    setEditMode(true)
                                    setUserId(item)
                                  }}
                                >
                                  <KTSVG
                                    path='/media/icons/duotune/art/art005.svg'
                                    className='svg-icon-3'
                                  />
                                </button>
                                <button
                                  className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-4'
                                  onClick={() => onDeleteItem(item && item._id)}
                                  title="Delete"
                                >
                                  <KTSVG
                                    path='/media/icons/duotune/general/gen027.svg'
                                    className='svg-icon-3'
                                  />
                                </button>
                              </div>
                            </div>
                          </ul>
                        )
                      }) : null}
                  </div>
                </div>
                {/* <div className='col-lg-3'>
                  <Status />
                  <div className='mb-4'>
                    <div className='mt-4 '>
                      <a className="btn fw-bolder px-4 btn-light-success w-100"
                        data-toggle='modal'
                        data-target='#rejectModal'
                      onClick={() => { setActive(true) }}
                      >
                        Approve
                      </a>
                    </div>
                    <div className='mt-4 '>
                      <a className="btn fw-bolder px-4 btn-light-danger w-100"
                        data-toggle='modal'
                        data-target='#rejectModal'
                      onClick={() => { setActive(true) }}
                      >
                        Reject
                      </a>
                    </div>
                  </div>
                </div> */}

                {/* <div className='col-lg-6'>
                  <div className='mt-2 d-flex'>
                    <button
                      onClick={print}
                      className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'
                      // onClick={(e) => exported(e)}
                      disabled={exportLoading}
                    >
                      {!exportLoading &&
                        <span className='indicator-label'>
                          <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                          PDF Report
                        </span>
                      }
                      {exportLoading && (
                        <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}

                    </button>
                  </div>
                </div> */}
              </div>
            </div>
            {
              merchantIddetails && merchantIddetails.data && merchantIddetails.data.clientId && merchantIddetails.data.clientId.pma === false ? (null) : (
                <div className='col-lg-12'>
                  <div className='fs-1 fw-boldest mt-4 d-flex justify-content-start ms-4'>
                    PMA
                  </div>

                  {
                    pmaCheck && pmaCheck["PMA Flag"] === 'Yes' ? (
                      <>
                        <div className="separator separator-dashed my-3" />
                        <div className="table-responsive ms-4">
                          <table className="table" style={{ textAlign: "center" }}>
                            <thead>
                              <tr className="fw-bold fs-6 text-gray-800">
                                <th>Heavy Discounts</th>
                                <th>Login Credentials Are Required</th>
                                <th>Page Navigation Issue</th>
                                <th>Pricing is in Dollars</th>
                                <th>Pricing is not updated</th>
                                <th>Website Redirection</th>
                                <th>Website is not working</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Heavy Discounts']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Login Credentials are required']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Page Navigation Issue']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is in Dollars']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is not updated']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website Redirection']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website is not working']]} mt-4 justify-content-center`} />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </>
                    ) : (

                      <div className='ml-5'>
                        <p>No violation detected</p>
                        <br />
                      </div>
                    )
                  }
                  <div className="separator separator-dashed my-3" />
                </div>
              )
            }
          </div>

        </div>
      </div>
      <CommentEdit editMode={editMode} setEditMode={setEditMode} userId={userId} setUserId={setUserId} currentId={currentId} />
    </>
  )
}

const mapStateToProps = (state) => {
  const { exportlistStore, WRMlistStore, WRMdeleteStore } = state;
  return {
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : '',

    WRMlists: WRMlistStore && WRMlistStore.WRMlists ? WRMlistStore.WRMlists : '',
    WRMlistsloading: WRMlistStore && WRMlistStore.loading ? WRMlistStore.loading : false,
    DeleteWRMData: WRMdeleteStore && WRMdeleteStore.DeleteWRMData ? WRMdeleteStore.DeleteWRMData : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),

  getWRMCommentDispatch: (params) => dispatch(WRMCommentActions.getWRMCommentlist(params)),
  deleteWRMCommentDispatch: (params) => dispatch(WRMCommentdeleteActions.delete(params)),
  clearDeleteWRMCommentDispatch: () => dispatch(WRMCommentdeleteActions.clear()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Score)