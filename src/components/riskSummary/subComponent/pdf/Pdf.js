import './pdf.css';
import { jsPDF } from "jspdf";
import { renderToString } from "react-dom/server"
import { toAbsoluteUrl } from '../../../../theme/helpers'
import { forwardRef, Fragment, useEffect } from "react";
import { connect } from 'react-redux'
import _ from 'lodash'
import { PMASTATUS, RISKSTATUS } from '../../../../utils/constants'
import { Link } from "react-router-dom"
import moment from 'moment'
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import Table from 'react-bootstrap/Table';

function PDF(props, ref) {
  const {
    websiteData,
    successVerifyDomain,
    DNSData,
    matrixDetail,
    adminContact,
    adminContactData,
    BsName,
    websitetobusinessmatch,
    websiteCatgories,
    ValidData,
    webAnalysis,
    LogoCheck,
    websiteImageDetect,
    userInteractionChecks,
    domainInfoData,
    domainRepetation,
    registerData,
    successWhoDomain,
    reviewAnalysis,
    websiteLink,
    merchantIddetails,
    DashboardExportData,
    merchantSummary,
    DashboardPmaLists
  } = props
  const filteredPeople = websiteData && websiteData.filter((item) => item.title !== "Risk Score");

  return (
    <>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page1'
        style={{
          backgroundColor: "white",
          width: "205mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <div className="d-flex align-items-center justify-content-between pdf-table mt-10">
          <img style={{ width: '50px', height: '50px', paddingBottom:'8px', objectFit:'cover' }}
            src={DashboardExportData && DashboardExportData.logo ? DashboardExportData.logo : 'No Data'}
            onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
          />
          <h3 className='ps-5'>WRM Reports</h3>
          <div className="pdf-p"><b>Scan Date : </b>
            {
              moment(merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt ? merchantIddetails.data.createdAt : 'No Data').format('DD/MM/YYYY')
            }</div>
        </div>
        <div className='row p-2 mt-20'>
          <div className='col-12 table-pdf'>
            <Table striped responsive bordered className="border border-secondary p-2 w-75">
              <tbody className='fw-bolder'>
                <tr>
                  <th>Website</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.website ? merchantIddetails.data.website : ''}</span></td>
                </tr>
                <tr>
                  <th>Queue</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.queueId && merchantIddetails.data.queueId.queueName ? merchantIddetails.data.queueId.queueName : ''}</span></td>
                </tr>
                <tr>
                  <th>Tag</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.tag ? merchantIddetails.data.tag : ''}</span></td>
                </tr>
                <tr>
                  <th>Date Recieved</th>
                  <td><span className='opacity-75'>{moment(merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt ? merchantIddetails.data.createdAt : 'No Data').format('DD/MM/YYYY')}</span></td>
                </tr>
              </tbody>
            </Table>
          </div>
          <div className="col-12 mt-10 table-pdf">
            <h6 className='ps-20'>WRM Inputs</h6>
            <Table striped bordered responsive className="border border-secondary p-2 w-75">
              <tbody className='fw-bolder'>
                <tr>
                  <th>Application Risk Score</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.riskScore ? merchantIddetails.data.riskScore : '--'}</span></td>
                </tr>
                <tr>
                  <th>Phone Number</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.phoneNumber ? merchantIddetails.data.phoneNumber : '--'}</span></td>
                </tr>
                <tr>
                  <th>Acquirer</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.acquirer ? merchantIddetails.data.acquirer : '--'}</span></td>
                </tr>
                <tr>
                  <th>Email Id</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.emailId ? merchantIddetails.data.emailId : '--'}</span></td>
                </tr>
                <tr>
                  <th>UPI</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.upi ? merchantIddetails.data.upi : '--'}</span></td>
                </tr>
                <tr>
                  <th>Legal Name</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.legalName ? merchantIddetails.data.legalName : '--'}</span></td>
                </tr>
                <tr>
                  <th>MCC Code</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.mccCodeMatch === "False" ? '' : merchantIddetails && merchantIddetails.data && merchantIddetails.data.mccCodeMatch ? merchantIddetails.data.mccCodeMatch : '--'}</span></td>
                </tr>
                <tr>
                  <th>Business Address</th>
                  <td><span className='opacity-75'>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.businessAddress ? merchantIddetails.data.businessAddress : '--'}</span></td>
                </tr>
              </tbody>
            </Table>
          </div>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page2'
        style={{
          backgroundColor: "white",
          width: "205mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container mt-20 table-pdf">
          <h4>Summary</h4>
          <h6>Website Summary</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>#</th>
                <th>WRM Validation</th>
                <th>Comments</th>
                <th>Risk</th>
              </tr>
            </thead>
            <tbody>
              {
                Array.isArray(filteredPeople) ?
                  filteredPeople && filteredPeople.map((item, i) => {
                    return (
                      <tr>
                        <td> {i + 1}</td>
                        <td> {item.title}</td>
                        <td> {item.value}</td>
                        <td> <span className={`badge ${RISKSTATUS[item.status]}`}>
                          {item.status ? item.status : "--"}</span>
                        </td>
                      </tr>
                    )
                  })
                  : (
                    <span className='text-dark fw-semibold fs-6 ms-4'>
                      No Records Found
                    </span>
                  )
              }
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page3'
        style={{
          backgroundColor: "white",
          width: "205mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Merchant Summary</h4>
          <h6>Website</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Risk Score</th>
                <td>{DashboardExportData.riskScore}</td>
              </tr>
              <tr>
                <th>Domain</th>
                <td>{DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'}</td>
              </tr>
              <tr>
                <th>Acquirer</th>
                <td>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.acquirer ? merchantIddetails.data.acquirer : ''}</td>
              </tr>
              <tr>
                <th>Reason</th>
                <td>{DashboardExportData && DashboardExportData.reason ? DashboardExportData.reason : 'No Data'}</td>
              </tr>
              <tr>
                <th>MCC Scrapped</th>
                <td>{DashboardExportData && DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : 'No Data'}</td>
              </tr>
              <tr>
                <th>MCC Mismatch</th>
                <td>{DashboardExportData && DashboardExportData.mccCodeMatch ? DashboardExportData.mccCodeMatch : 'No Data'}</td>
              </tr>
              <tr>
                <th>Risk Classification</th>
                <td>{DashboardExportData && DashboardExportData.riskClassification ? DashboardExportData.riskClassification : 'No Data'}</td>
              </tr>
              <tr>
                <th>Website Working</th>
                <td>{DashboardExportData && DashboardExportData.websiteWorking ? DashboardExportData.websiteWorking : 'No Data'}</td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>Overall Risk</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Risk Score</th>
                <td>{DashboardExportData.riskScore}</td>
              </tr>
              <tr>
                <th>Status</th>
                <td>{DashboardExportData && DashboardExportData.status ? DashboardExportData.status : 'No Data'}</td>
              </tr>
              <tr>
                <th>Business Risk</th>
                <td>{DashboardExportData && DashboardExportData.businessRisk ? DashboardExportData.businessRisk : 'No Data'}</td>
              </tr>
              <tr>
                <th>Web Content Risk</th>
                <td>{DashboardExportData && DashboardExportData.webContentRisk ? DashboardExportData.webContentRisk : 'No Data'}</td>
              </tr>
              <tr>
                <th>Transparency Risk</th>
                <td>{DashboardExportData && DashboardExportData.transparencyRisk ? DashboardExportData.transparencyRisk : 'No Data'}</td>
              </tr>
              <tr>
                <th>Website Health</th>
                <td>{DashboardExportData && DashboardExportData.websiteHealth ? DashboardExportData.websiteHealth : 'No Data'}</td>
              </tr>
              <tr>
                <th>Website Content Checkup Domain Health</th>
                <td>{DashboardExportData && DashboardExportData.websiteContentCheckupDomainHealth ? DashboardExportData.websiteContentCheckupDomainHealth : 'No Data'}</td>
              </tr>
              <tr>
                <th>Online Website Reputation</th>
                <td>{DashboardExportData && DashboardExportData.onlineWebsiteReputation ? DashboardExportData.onlineWebsiteReputation : 'No Data'}</td>
              </tr>
              <tr>
                <th>Web Security</th>
                <td>{DashboardExportData && DashboardExportData.webSecurity ? DashboardExportData.webSecurity : 'No Data'}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page4'
        style={{
          backgroundColor: "white",
          width: "205mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>Contact Details</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Email Id</th>
                <td>{
                  Array.isArray(DashboardExportData && DashboardExportData.contactDetailsEmail) ?
                    DashboardExportData && DashboardExportData.contactDetailsEmail.map((email) => {
                      return (
                        <>{email}<br />
                        </>
                      )
                    })
                    : DashboardExportData && DashboardExportData.contactDetailsEmail
                }</td>
              </tr>
              <tr>
                <th>Legal Name</th>
                <td>{DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : 'No Data'}</td>
              </tr>
              <tr>
                <th>Telephone</th>
                <td>{Array.isArray(DashboardExportData && DashboardExportData.contactDetailsPhone) ?
                  DashboardExportData && DashboardExportData.contactDetailsPhone.map((Phone) => {
                    return (
                      <>{Phone}<br />
                      </>
                    )
                  })
                  : DashboardExportData && DashboardExportData.contactDetailsPhone}</td>
              </tr>
              <tr>
                <th>Business Address</th>
                <td>{DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}</td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Merchant Location</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Business Address</th>
                <td>
                  {successVerifyDomain && successVerifyDomain.location ? successVerifyDomain.location : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Company name & address as image</th>
                <td>
                  --
                </td>
              </tr>
              <tr>
                <th>Business Address matches website address</th>
                <td>
                  --
                </td>
              </tr>
              <tr>
                <th>Price & Currency</th>
                <td>
                  --
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Website Content</h4>
          <h6>Website Categorization</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Domain Name</th>
                <td>
                  {DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Code</th>
                <td>
                  {DashboardExportData && DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Risk Classification</th>
                <td>
                  {DashboardExportData && DashboardExportData.riskClassification ? DashboardExportData.riskClassification : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Risk Level</th>
                <td>
                  {DashboardExportData && DashboardExportData.riskLevel ? DashboardExportData.riskLevel : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Categories</th>
                <td>
                  {DashboardExportData && DashboardExportData.reason ? DashboardExportData.reason : 'No data'}
                </td>
              </tr>
              <tr>
                <th>Paid Up Capital</th>
                <td>
                  {DashboardExportData && DashboardExportData.paidUpCapital ? DashboardExportData.paidUpCapital : 'No data'}
                </td>
              </tr>
              <tr>
                <th>Google Analytics Id Relation</th>
                <td>
                  {DashboardExportData && DashboardExportData.googleAnalyticsIdRelation ? DashboardExportData.googleAnalyticsIdRelation : 'No data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page5'
        style={{
          backgroundColor: "white",
          width: "205mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>Website Details</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Legal Name</th>
                <td>
                  {DashboardExportData && DashboardExportData.legalName ? DashboardExportData.legalName : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Logo</th>
                <td>
                  <img style={{ width: '50px', height: '50px', objectFit:'cover' }}
                    src={DashboardExportData && DashboardExportData.logo ? DashboardExportData.logo : 'No Data'}
                    onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                  />
                </td>
              </tr>
              <tr>
                <th>Organization Address - Transparency</th>
                <td>
                  {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Domain Redirection</th>
                <td>
                  {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>PMA</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Heavy Discounts</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Heavy Discounts']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Login Credentials Are Required</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Login Credentials are required']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Page Navigation Issue</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Page Navigation Issue']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Pricing is in Dollars</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Pricing is in Dollars']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Pricing is not updated</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Pricing is not updated']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Website Redirection</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Website Redirection']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Website is not working</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Website is not working']]} justify-content-start ms-4`} />
                </td>
              </tr>
              <tr>
                <th>Multiple Line of Businesses</th>
                <td>
                  <i className={`bi ${PMASTATUS[DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && DashboardPmaLists.data.pma['Multiple Line of Businesses']]} justify-content-start ms-4`} />
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Card Scheme Compliance(BRAM/GBPP)</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Adult Content Monitoring</th>
                <td>
                  {DashboardExportData && DashboardExportData.adultContentMonitoring ? DashboardExportData.adultContentMonitoring : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Predict Medicine</th>
                <td>
                  {DashboardExportData && DashboardExportData.predictMedicine ? DashboardExportData.predictMedicine : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page6'
        style={{
          backgroundColor: "white",
          width: "205mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Merchant's Policies Url</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Terms & Condition</th>
                <td className='text-break'>
                  {DashboardExportData && DashboardExportData.termsAndConditionPageUrl ? DashboardExportData.termsAndConditionPageUrl : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Shipping Policy</th>
                <td className='text-break'>
                  {DashboardExportData && DashboardExportData.shippingPolicyPageUrl ? DashboardExportData.shippingPolicyPageUrl : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Return Policy</th>
                <td className='text-break'>
                  {DashboardExportData && DashboardExportData.returnPolicyPageUrl ? DashboardExportData.returnPolicyPageUrl : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Contact Policy</th>
                <td className='text-break'>
                  {DashboardExportData && DashboardExportData.contactUsPageUrl ? DashboardExportData.contactUsPageUrl : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Privacy Policy</th>
                <td className='text-break'>
                  {DashboardExportData && DashboardExportData.privacyPolicyPageUrl ? DashboardExportData.privacyPolicyPageUrl : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Purchase Or Registration</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Id Submission prompt</th>
                <td>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Web Analysis Status</h4>
          <h6>Page Activity Check</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Is Mining Happening?</th>
                <td>
                  {DashboardExportData && DashboardExportData.pageActivityCheckMining ? DashboardExportData.pageActivityCheckMining : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Any Untrusted Downloads?</th>
                <td>
                  {DashboardExportData && DashboardExportData.pageActivityCheckUntrustedDownloads ? DashboardExportData.pageActivityCheckUntrustedDownloads : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
          <h6 className='mt-5'>Page Health Check</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Content Accessibilty</th>
                <td>
                  {DashboardExportData && DashboardExportData.pageHealthCheckContentAccessibilty ? DashboardExportData.pageHealthCheckContentAccessibilty : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Page loading Time</th>
                <td>
                  {DashboardExportData && DashboardExportData.pageHealthCheckPageLoadingTime ? DashboardExportData.pageHealthCheckPageLoadingTime : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page7'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h6 className='mt-5'>Page Links Connectivity Check</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Status</th>
                <td>
                  {DashboardExportData && DashboardExportData.pageAvailabilityCheckURLStatus ? DashboardExportData.pageAvailabilityCheckURLStatus : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Sucess Rate</th>
                <td>
                  {DashboardExportData && DashboardExportData.pageLinksConnectivityCheckSuccessRate ? DashboardExportData.pageLinksConnectivityCheckSuccessRate : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>State banned Category</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Address Scrapped From Website</th>
                <td>
                  {DashboardExportData && DashboardExportData.addressScrappedFromWebsite ? DashboardExportData.addressScrappedFromWebsite : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Address From Google Map</th>
                <td>
                  {DashboardExportData && DashboardExportData.addressFromGoogleMap ? DashboardExportData.addressFromGoogleMap : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Matched Address From Google Map</th>
                <td>
                  {DashboardExportData && DashboardExportData.matchedAddressFromGoogleMap ? DashboardExportData.matchedAddressFromGoogleMap : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Content Monitoring</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Currency</th>
                <td>
                  {DashboardExportData && DashboardExportData.currenciesFoundOnWebsite ? DashboardExportData.currenciesFoundOnWebsite : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Price Listing</th>
                <td>
                  {DashboardExportData && DashboardExportData.productsPriceListedInWebsite ? DashboardExportData.productsPriceListedInWebsite : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Unreasonable Price</th>
                <td>
                  {DashboardExportData && DashboardExportData.websiteContainsUnreasonablePrice ? DashboardExportData.websiteContainsUnreasonablePrice : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Accessibility</th>
                <td>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Average Price</th>
                <td>
                  {DashboardExportData && DashboardExportData.averageProductPrice ? DashboardExportData.averageProductPrice : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Min Price Listed</th>
                <td>
                  {DashboardExportData && DashboardExportData.minPriceListedInHomePage ? DashboardExportData.minPriceListedInHomePage : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Max Price Listed</th>
                <td>
                  {DashboardExportData && DashboardExportData.maxPriceListedInHomePage ? DashboardExportData.maxPriceListedInHomePage : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page8'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Malware Risk</h4>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Malware Risk</th>
                <td>
                  {DashboardExportData && DashboardExportData.malwareRisk ? DashboardExportData.malwareRisk : 'No Data'}
                </td>
              </tr>
              <tr>
                <th>Malware Present</th>
                <td>
                  {DashboardExportData && DashboardExportData.malwarePresent ? DashboardExportData.malwarePresent : 'No Data'}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Website Content Set-up</h4>
          <h6>Domain Info</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Domain Registration Company</th>
                <td>{DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : "No Data"}</td>
              </tr>
              <tr>
                <th>Domain Registered</th>
                <td>{DashboardExportData && DashboardExportData.domainRegistered ? DashboardExportData.domainRegistered : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"}</td>
              </tr>
              <tr>
                <th>Domain Rank</th>
                <td>{DashboardExportData && DashboardExportData.domainRank ? DashboardExportData.domainRank : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"}</td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>Registrant</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Domain Registered Date</th>
                <td>{!_.isEmpty(DashboardExportData.domainRegistrationDate) ? DashboardExportData.domainRegistrationDate : "No Data"}</td>
              </tr>
              <tr>
                <th>Domain Expiry Date</th>
                <td>{!_.isEmpty(DashboardExportData.domainRegistrationExpiryDate) ? DashboardExportData.domainRegistrationExpiryDate : "No Data"}</td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>SSL Check</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Ssl Certificate Check</th>
                <td>{DashboardExportData && DashboardExportData.sslCertificateCheck ? DashboardExportData.sslCertificateCheck : "No Data"}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page9'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2 table-pdf">
          <h4>Screenshots</h4>
          <h6>Url</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th className='url-table-parameter'>Parameter</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th className='url-table-parameter'>Terms & Condition</th>
                <td><div className='table-url-description'><span className='me-2'>{DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot}</span><span className={`badge ${RISKSTATUS[DashboardExportData && DashboardExportData.termsAndConditionStatus]}`}>{DashboardExportData.termsAndConditionStatus}</span></div></td>
              </tr>
              <tr>
                <th className='url-table-parameter'>Privacy Policy</th>
                <td><div className='table-url-description'><span className='me-2'>{DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot ? DashboardExportData.privacyPolicyPageScreenshot : 'No Data'}</span><span className={`badge ${RISKSTATUS[DashboardExportData && DashboardExportData.privacyPolicyStatus]}`}>{DashboardExportData.privacyPolicyStatus}</span></div></td>
              </tr>
              <tr>
                <th className='url-table-parameter'>Shipping Policy</th>
                <td><div className='table-url-description'><span className='me-2'>{DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot ? DashboardExportData.shippingPolicyPageScreenshot : 'No Data'}</span><span className={`badge ${RISKSTATUS[DashboardExportData && DashboardExportData.shippingPolicyStatus]}`}>{DashboardExportData.shippingPolicyStatus}</span></div></td>
              </tr>
              <tr>
                <th className='url-table-parameter'>Return Policy</th>
                <td><div className='table-url-description'><span className='me-2'>{DashboardExportData && DashboardExportData.returnPolicyPageScreenshot ? DashboardExportData.returnPolicyPageScreenshot : 'No Data'}</span><span className={`badge ${RISKSTATUS[DashboardExportData && DashboardExportData.returnPolicyStatus]}`}>{DashboardExportData.returnPolicyStatus}</span></div></td>
              </tr>
              <tr>
                <th className='url-table-parameter'>Contact Policy</th>
                <td><div className='table-url-description'><span className='me-2'>{DashboardExportData && DashboardExportData.contactUsPageScreenshot ? DashboardExportData.contactUsPageScreenshot : 'No Data'}</span><span className={`badge ${RISKSTATUS[DashboardExportData && DashboardExportData.contactsFormStatus]}`}>{DashboardExportData.contactsFormStatus}</span></div></td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="pdf-Container pt-2 table-pdf">
          <h6>Product Pricing Url</h6>
          <Table striped bordered responsive className="border border-secondary p-2">
            <thead>
              <tr>
                <th className='w-50'>Parameter</th>
                <th className='w-50'>Description</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Home</th>
                <td>{DashboardExportData && DashboardExportData.productPricingHomePageUrl ? DashboardExportData.productPricingHomePageUrl : 'No Data'}</td>
              </tr>
              <tr>
                <th>Product Page</th>
                <td>{DashboardExportData && DashboardExportData.productPricingProductPageUrl ? DashboardExportData.productPricingProductPageUrl : 'No Data'}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page10'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2">
          <h4>Terms and Conditions</h4>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12 text-center'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot) ? (
                        <img
                          src={DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot ? DashboardExportData.termsAndConditionPageScreenshot : 'No Data'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                          className='screenshot-image'
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700'>
                          Terms and Conditions Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page11'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container">
          <h4>Privacy Policy</h4>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12 text-center'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot) ? (
                        <img
                          src={DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot ? DashboardExportData.privacyPolicyPageScreenshot : 'No Data'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                          className='screenshot-image'
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700'>
                          Privacy Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page12'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2">
          <h4>Shipping Policy</h4>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12 text-center'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot) ? (
                        <img
                          src={DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot ? DashboardExportData.shippingPolicyPageScreenshot : 'No Data'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                          className='screenshot-image'
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700'>
                          Shipping Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page13'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2">
          <h4>Returns Policy</h4>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12 text-center'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.returnPolicyPageScreenshot) ? (
                        <img
                          src={DashboardExportData && DashboardExportData.returnPolicyPageScreenshot ? DashboardExportData.returnPolicyPageScreenshot : 'No Data'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                          className='screenshot-image'
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700'>
                          Returns Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="p-4 z-n2 fs-8 pdf-text"
        id='page14'
        style={{
          backgroundColor: "white",
          width: "205mm",
          // maxHeight: "597mm",
          marginLeft: "auto",
          marginRight: "auto",
        }}>
        <div className="pdf-Container pt-2">
          <h4>Contact Policy</h4>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className="separator separator-dashed my-3" />
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12 text-center'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.contactUsPageScreenshot) ? (
                        <img
                          src={DashboardExportData && DashboardExportData.contactUsPageScreenshot ? DashboardExportData.contactUsPageScreenshot : 'No Data'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                          className='screenshot-image'
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700'>
                          Contact Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default forwardRef(PDF)