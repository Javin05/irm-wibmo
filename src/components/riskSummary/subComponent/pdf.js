import React, { useEffect, useState } from 'react'

function PDF(props) {
  const {
    ref
  } = props
  return (

    <>
      <div className="container head-line">
        <div className="row">
          <div className="col-md-12 col-lg-4" >
            <h2 className="trust">Trust<span className="wave">Wave</span></h2>
          </div>
          <div className="col-md-12 col-lg-4 merchant">
            <h3>WCM Merchant Scorecard</h3>
          </div>
          <div className="col-md-12 col-lg-4">
            <b>Scan Date :</b>2022-01-12
          </div>
        </div>
      </div>
      <div className="container second-line">
        <div className="row">
            <div className="col-sm">
                <table className="table table-borderless">
                    <tbody>
                        <tr>
                            <th scope="row">Sponsor :</th>
                            <td>Razorpay<br/>
                                Google Payment India Private Limited
                                FdIDCkh8I4nK6j
                                </td>
                        </tr>
                        <tr>
                            <th scope="row">Site :</th>
                            <td>https://play.google.com/store</td>
                        </tr>
                        <tr>
                            <th scope="row">MCC :</th>
                            <td>142.250.190.110
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Address :</th>
                            <td>5815</td>
                        </tr>
                        <tr>
                            <th scope="row">Email Address:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">Phone :</th>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="col-sm wave-one">
                <h2 className="trust wave-two ">Trust<span className="wave">Wave</span></h2>
            </div>
        </div>
    </div>
    <div className="container">
        <h4>Score card</h4>
        <table className="table table-bordered table-striped ">
            <thead className="table-secondary">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">WCM Validation</th>
                    <th scope="col">Comments</th>
                    <th scope="col">Risk</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Web Content Analysis</td>
                    <td>Site has not been audited by Trustwave yet, but has
                        yielded potential findings to be reviewed by Trustwave</td>
                    <td className="bg-warning">Medium</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Terms of Service</td>
                    <td>No terms of service violations</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Secure Checkout (SSL)</td>
                    <td>Warning: SSL certificate found on merchant's website is
                        insecure. Please see the details section below for more
                        information on the SSL certificate</td>
                    <td className="bg-danger">High</td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td>Malware</td>
                    <td>No Malware found during scan</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td>3rd Party Analysis</td>
                    <td>Web host: Google LLC (Not Compliant)
                        Shopping Cart: None
                        PSP/Payment Gateway: None</td>
                    <td className="bg-danger">High</td>
                </tr>
                <tr>
                    <th scope="row">6</th>
                    <td>Geolocation </td>
                    <td>The website https://play.google.com/store with IP Address
                        142.250.190.110 is located in United States. This is a
                        NONE Risk country.</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">7</th>
                    <td>Website Classification</td>
                    <td>Website Classification unknown</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">8</th>
                    <td>Downstream Links </td>
                    <td>The website does not link to any sites that have been
                        reported for a Card Brand Violation or Card Brand Violation
                        Warning in WCM.</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">9</th>
                    <td>Known Risk</td>
                    <td>Merchant Website has been registered to another bank in
                        WCM.</td>
                    <td className="bg-warning">Medium</td>
                </tr>
                <tr>
                    <th scope="row">10</th>
                    <td>IP Analysis</td>
                    <td>The IP Address for this website hosts 25 other domains.
                        None of these sites have been reported in WCM for a Card
                        Brand Violation or Card Brand Violation Warning.</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">11</th>
                    <td>WHO IS Analysis</td>
                    <td>The website WHOIS information is publicly available</td>
                    <td className="bg-success">None</td>
                </tr>
                <tr>
                    <th scope="row">12</th>
                    <td>Website Information</td>
                    <td>There was a Privacy Policy page and Terms and Conditions
                        page found.
                        No Contact page was found on the website
                        No Shipping/Delivery page was found on the website</td>
                    <td className="bg-warning">Medium</td>
                </tr>
                <tr>
                    <th scope="row">13</th>
                    <td>Not Password Protected</td>
                    <td>Site is not Password protected</td>
                    <td className="bg-success">None</td>
                </tr>
            </tbody>
        </table>
    </div>

    </>

  )
}

export default PDF