import React, { useEffect, useState } from "react"
import { connect } from "react-redux"
import {
  riskSummaryActions,
  merchantIdDetailsActions,
  dashboardDetailsActions,
  riskScoreActions,
  matrixActions,
  KYCDashboardSummaryAction,
  KYCUserAction,
  DistanceAction,
  AllDasboardDataAction,
  DasboardAadharAction,
  DasboardPanAction,
  DasboardCinAction,
  KycScoreAction,
  linkAnalyticsActions
} from "../../store/actions"
import moment from "moment"
import "react-circular-progressbar/dist/styles.css"
import { Link, useLocation } from "react-router-dom"
import { useLoadScript } from "@react-google-maps/api"
import { getLocalStorage } from "../../utils/helper"
import Merchant from "../merchant/merchant"
import Phone from "./subComponent/phone"
import Email from "./subComponent/email"
import Address from "./subComponent/address"
import IpAddress from "./subComponent/ipAddress"
import MerchantDetails from "./merchantDetails"
import Website from "./subComponent/website"
import NetworkGraph from "./subComponent/Netwok"
import Tabs from "react-bootstrap/Tabs"
import Tab from "react-bootstrap/Tab"
import AMLdashboard from "../amlQueue/AmlDashboard"
import UserProfileKYC from "../KYC/UserProfileKYC"
import "./index.css"
import Websites from './subComponent/website/index'

function RiskSummary(props) {
  const {
    className,
    getRiskSummaryDispatch,
    loading,
    getRiskSummarys,
    getIdMerchantDispatch,
    phone,
    getDashboardDispatch,
    merchantIddetails,
    dashboardDetails,
    getRiskScoreDispatch,
    matrixDetails,
    getMatrixDispatch,
    UserDetails,
    DistanceRes,
    DashboardPANres,
    DashboardAadharRes,
    DashboardCINres,
    getKYCUserDetailsDispatch,
    DashboardAadhaarDispatch,
    DasboardPanDispatch,
    DashboardCINDispatch,
    KycScoreDispatch,
    getKYCDashboardSummaryDispatch,
    CommonFileDispatch,
    AllDashboardDispatch,
    UserDetailsloading,
    networkData,
    getlinkAnalyticslistDispatch,
    getriskScores,
    DashboardExportLists
  } = props

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws", // Add your API key
  })

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [openPhone, setOpenPhone] = useState(false)
  const [openEmail, setOpenEmail] = useState(false)
  const [openAddress, setOpenAddress] = useState(false)
  const [openIpAddress, setOpenIpAddress] = useState(false)
  const [openBusinessPhone, setOpenBusinessPhone] = useState(false)
  const [openBusinessEmail, setOpenBusinessEmail] = useState(false)
  const [openBusinessAddress, setOpenBusinessAddress] = useState(false)
  const [openMap, setopenMap] = useState(false)
  const [openBusinessMap, setopenBusinessMap] = useState(false)
  const [openWebsite, setOpenWebsite] = useState(false)
  const [key, setKey] = useState("MerchnatOnBOarding")
  const [summaryData, setSummaryData] = useState({})
  const [dashboardData, setDashboardData] = useState({})
  const [activestep, setActiveStep] = useState(0)
  const [completed] = useState({})
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const steps = getSteps()
  const style = {
    width: "100%",
    height: "100%",
  }
  const containerStyle = {
    width: "50%",
    height: "50%",
  }

  const merchantSummary = merchantIddetails && merchantIddetails.data
  const WebRiskScore = getriskScores && getriskScores.data
  const DashboardExportData = DashboardExportLists && DashboardExportLists.data && DashboardExportLists.data.data ?  DashboardExportLists.data.data[0] : 'No Data'

  function getSteps() {
    return [
      {
        label: "Merchant Review",
        className: "btn label-one",
        stepCount: 0,
      },
      {
        label: "Network",
        stepCount: 1,
        className: "btn label-seven",
      },
      {
        label: "PHONE",
        stepCount: 2,
        className: "btn label-two",
      },
      {
        label: "EMAIL",
        stepCount: 3,
        className: "btn label-three",
      },
      {
        label: "ADDRESS",
        stepCount: 4,
        className: "btn label-four",
      },
      {
        label: "IP ADDRESS",
        stepCount: 5,
        className: "btn label-five",
      },
      {
        label: "Website",
        stepCount: 6,
        className: "btn label-six",
      },
    ]
  }

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <Merchant
            id={id}
            setOpenPhone={setOpenPhone}
            setOpenEmail={setOpenEmail}
            setOpenAddress={setOpenAddress}
            setOpenIpAddress={setOpenIpAddress}
            setOpenBusinessPhone={setOpenBusinessPhone}
            setOpenBusinessEmail={setOpenBusinessEmail}
            setOpenBusinessAddress={setOpenBusinessAddress}
            openMap={openMap}
            setopenMap={setopenMap}
            openBusinessMap={openBusinessMap}
            setopenBusinessMap={setopenBusinessMap}
            setOpenWebsite={setOpenWebsite}
            merchantSummary={merchantSummary}
            isLoaded={isLoaded}
          />
        )
      case 1:
        return <NetworkGraph merchantIddetails={merchantIddetails} networkData={networkData} />
      case 2:
        return <Phone id={id}
          summary={summaryData}
          dashboardDetails={dashboardDetails}
        />
      case 3:
        return <Email summary={summaryData} dashboard={dashboardData} dashboardDetails={dashboardDetails} />
      case 4:
        return (
          <Address
            summary={summaryData}
            dashboard={dashboardData}
            isLoaded={isLoaded}
            matrixDetail={matrixDetails}
            dashboardDetails={dashboardDetails}
          />
        )
      case 5:
        return <IpAddress
          summary={summaryData}
          dashboard={dashboardData}
          dashboardDetails={dashboardDetails}
        />
      case 6:
        return <Websites
          merchantIddetails={merchantIddetails}
          dashboardDetails={dashboardDetails}
          matrixDetail={matrixDetails}
          isLoaded={isLoaded}
        />
      default:
        return "unknown step"
    }
  }

  const handleNext = (step) => {
    setActiveStep(activestep + 1)
  }

  const handleStep = (step) => () => {
    setActiveStep(step)
  }

  const handleBack = () => {
    setActiveStep(activestep - 1)
  }

  useEffect(() => {
    if (openPhone) {
      setActiveStep(2)
    }
    if (openEmail) {
      setActiveStep(3)
    }
    if (openAddress) {
      setActiveStep(4)
    }
    if (openIpAddress) {
      setActiveStep(5)
    }
    if (openWebsite) {
      setActiveStep(6)
    }
    if (activestep !== 1) {
      setOpenPhone(false)
      setOpenEmail(false)
      setOpenAddress(false)
      setOpenIpAddress(false)
      setOpenBusinessPhone(false)
      setOpenBusinessEmail(false)
      setOpenBusinessAddress(false)
      setOpenWebsite(false)
    }
  }, [
    openPhone,
    openEmail,
    activestep,
    openAddress,
    openIpAddress,
    openBusinessPhone,
    openBusinessEmail,
    openBusinessAddress,
    openWebsite,
  ])

  useEffect(() => {
    if (id) {
      // getRiskSummaryDispatch(id)
      getIdMerchantDispatch(id)
      getDashboardDispatch(id)
      getRiskScoreDispatch(id)
      getMatrixDispatch(id)
      const params = {
        caseId: id,
      }
      getlinkAnalyticslistDispatch(params)
    }
  }, [id])

  //kyc dashboard
  // useEffect(() => {
  //   getKYCUserDetailsDispatch(id)
  //   DashboardAadhaarDispatch(id)
  //   DasboardPanDispatch(id)
  //   DashboardCINDispatch(id)
  //   KycScoreDispatch(id)
  //   getKYCDashboardSummaryDispatch(id)
  //   CommonFileDispatch(id)
  //   AllDashboardDispatch(id)
  // }, [id])

  /*
  Summary Data
  */
  useEffect(() => {
    if (getRiskSummarys) {
      if (getRiskSummarys.length) {
        setSummaryData(getRiskSummarys[0])
      }
    }
  }, [getRiskSummarys])

  /*
      Dashboard Data
  */
  useEffect(() => {
    if (dashboardDetails) {
      setDashboardData(dashboardDetails)
    }
  }, [dashboardDetails])
  
  return (
    <>
      <div className="mt-0">
        <Tabs
          id="controlled-tab-example"
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className="ctab mb-3"
        >
          <Tab eventKey="MerchnatOnBOarding" title="ACCOUNT">
            <MerchantDetails
              id={id}
              setOpenPhone={setOpenPhone}
              setOpenEmail={setOpenEmail}
              setOpenAddress={setOpenAddress}
              setOpenIpAddress={setOpenIpAddress}
              setOpenBusinessPhone={setOpenBusinessPhone}
              setOpenBusinessEmail={setOpenBusinessEmail}
              setOpenBusinessAddress={setOpenBusinessAddress}
              setopenMap={setopenMap}
              setopenBusinessMap={setopenBusinessMap}
              merchantSummary={merchantSummary}
              WebRiskScore={WebRiskScore}
              DashboardExportData={DashboardExportData}
            />
            {
            merchantIddetails && merchantIddetails.data && merchantIddetails.data.merchantOnboarding ? (
              <>
              <div className="d-flex">
                {steps.map((step, index) => (
                  <div
                    key={"A_" + index}
                    completed={completed[index]}
                    className={`my-10 mx-1 text mb-4 rounded-6 seven-label ${loading ? "event-disable" : ""
                      } ${step.stepCount === activestep
                        ? "btn btn-outline btn-outline-dashed btn-outline-primary btn-active-light-primary"
                        : `${step.className}`
                      }`}
                    onClick={handleStep(index)}
                  >
                    {step.label}
                  </div>
                ))}
              </div>
              {activestep === steps.length ? null : (
                <>
                  <div>{getStepContent(activestep)}</div>
                </>
              )}
            </>
            ) : (
              <>
                <Websites
                  merchantIddetails={merchantIddetails}
                  dashboardDetails={dashboardDetails}
                  matrixDetail={matrixDetails}
                  isLoaded={isLoaded}
                  merchantSummary={merchantSummary}
                />
              </>
            )}
          </Tab>
          <Tab eventKey="network" title="NETWORK">
            <NetworkGraph 
            merchantIddetails={merchantIddetails} networkData={networkData}
            />
          </Tab>
          {merchantSummary &&
            merchantSummary.amlStatus === "SUSPECTED ACCOUNT" ? (
            <Tab eventKey="AML" title="SUSPECTED ACCOUNT">
              <div>
                <AMLdashboard />
              </div>
            </Tab>
          ) : null}
        </Tabs>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { editMerchantStore, riskSummaryStore, dashboardStore, MatrixStore,
    KYCUserStoreKey, DistanceStore, DashBoardAadharStore, DashBoardPANStore, DashBoardCINStore,
    linkAnalyticslistStore, riskScoreStore, exportlistStore
  } =
    state
  return {
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    getRiskSummarys:
      riskSummaryStore && riskSummaryStore.getRiskSummarys?.data
        ? riskSummaryStore.getRiskSummarys?.data
        : null,
    dashboardDetails:
      dashboardStore && dashboardStore.dashboardDetails?.data
        ? dashboardStore.dashboardDetails?.data
        : null,
    merchantIddetails:
      editMerchantStore && editMerchantStore.merchantIddetail
        ? editMerchantStore.merchantIddetail
        : "",
    matrixDetails:
      MatrixStore && MatrixStore.matrixDetail
        ? MatrixStore.matrixDetail?.data[0]
        : null,
    UserDetails: KYCUserStoreKey && KYCUserStoreKey.KYCUser && KYCUserStoreKey.KYCUser?.data ? KYCUserStoreKey.KYCUser?.data
      : "",
    DistanceRes: DistanceStore && DistanceStore.DistanceRes ? DistanceStore.DistanceRes : {},
    DashboardAadharRes: DashBoardAadharStore && DashBoardAadharStore.DashboardAadharRes ? DashBoardAadharStore.DashboardAadharRes : {},
    DashboardPANres: DashBoardPANStore && DashBoardPANStore.DashboardPANRes ? DashBoardPANStore.DashboardPANRes : {},
    DashboardCINres: DashBoardCINStore && DashBoardCINStore.DashboardCINres ? DashBoardCINStore.DashboardCINres : {},
    UserDetailsloading: KYCUserStoreKey.loading && KYCUserStoreKey.loading ? KYCUserStoreKey.loading : false,
    networkData: linkAnalyticslistStore && linkAnalyticslistStore.linkAnalyticallists ? linkAnalyticslistStore.linkAnalyticallists : {},
    getriskScores: riskScoreStore && riskScoreStore.getriskScores ? riskScoreStore.getriskScores : {},
    DashboardExportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getRiskSummaryDispatch: (id) =>
    dispatch(riskSummaryActions.getRiskSummary(id)),
  getIdMerchantDispatch: (id) =>
    dispatch(merchantIdDetailsActions.getmerchantIdDetailsData(id)),
  getDashboardDispatch: (id) =>
    dispatch(dashboardDetailsActions.getdashboardDetails(id)),
  getRiskScoreDispatch: (id) => dispatch(riskScoreActions.getRiskScore(id)),
  getMatrixDispatch: (id) => dispatch(matrixActions.getMatrixDetails(id)),
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
  getKYCDashboardSummaryDispatch: (id) => dispatch(KYCDashboardSummaryAction.KYCDashboardSummary_INIT(id)),
  CommonFileDispatch: (id) => dispatch(DistanceAction.Distance(id)),
  AllDashboardDispatch: (id) => dispatch(AllDasboardDataAction.AllDashboard(id)),
  DashboardAadhaarDispatch: (id) => dispatch(DasboardAadharAction.DashboardAadhaar(id)),
  DasboardPanDispatch: (id) => dispatch(DasboardPanAction.DashboardPAN(id)),
  DashboardCINDispatch: (id) => dispatch(DasboardCinAction.DashboardCIN(id)),
  KycScoreDispatch: (id) => dispatch(KycScoreAction.KycScore(id)),
  getlinkAnalyticslistDispatch: (params) => dispatch(linkAnalyticsActions.getlinkAnalyticslist(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(RiskSummary)
