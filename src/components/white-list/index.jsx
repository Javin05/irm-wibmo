import React, { useState, useEffect } from "react";
import { toAbsoluteUrl } from "../../theme/helpers";
import { connect } from "react-redux";
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  STATUS_BADGE,
  DROPZONE_MESSAGES,
  FILE_FORMAT_CB_DOCUMENT,
  SET_FILTER,
} from "../../utils/constants";
import {
  WhiteListTypes,
  WhiteListActions,
  queuesActions,
  clientCredFilterActions,
  clientIdLIstActions,
  WhiteListDeleteClientActions
} from "../../store/actions";
import ReactPaginate from "react-paginate";
import Icofont from "react-icofont";
import Select from "react-select";
import WhitelistModal from "./WhitelistModal";
import WhiteListTable from "./WhiteListTable";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";
import { getLocalStorage } from "../../utils/helper";
import _, { isEmpty } from 'lodash'
import { KTSVG } from '../../theme/helpers'
import Search from './Search'
import Modal from 'react-bootstrap/Modal'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import FindRole from '../wrmManagement/Role'

function Whitelist(props) {
  const {
    fetchQueueslistDispatch,
    fetchWhitelistDispatch,
    fetchWhitelistTypeDispatch,
    postWhitelistTypeDispatch,
    updateWhitelistTypeDispatch,
    deleteWhitelistTypeDispatch,
    loading,
    whitelistData,
    whitelistCount,
    QueuesList,
    whitelistTypeData,
    createWhitelistTypeResponse,
    updateWhitelistTypeResponse,
    deleteWhitelistTypeResponse,
    whitelistPageCount,
    WhiteListCSV,
    setFilterFunctionDispatch,
    setFilterFunction,
    clientIdDispatch,
    clinetIdLists,
    WhiteListDeleteClientDispatch,
    WhiteDeletListClient,
    WhiteListDeleteClientClearDispatch
  } = props;

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : "#000",
      background: state.isSelected ? color.white : color.white,
    }),
  }
  const recordOptions = [
    { label: 25, value: 25 },
    { label: 50, value: 50 },
    { label: 75, value: 75 },
    { label: 100, value: 100 },
  ];
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [currentPage, setCurrentPage] = useState(1);
  const [pageRecords, setPageRecords] = useState(25);
  const [records, setRecords] = useState(recordOptions);
  const [modalType, setModalType] = useState("create");
  const [editModal, setEditModal] = useState(false);
  const [queue, setQueue] = useState("All")
  const [whitelist, setWhitelist] = useState("All");
  const [whiteOptions, setWhiteOptions] = useState(null);
  const [queueOptions, setQueueOptions] = useState(null);
  const [defaultWhitelist, setDefaultWhitelist] = useState(null);
  const [defaultQueue, setDefaultQueue] = useState(null);
  const [editRow, setEditRow] = useState(null);
  const [client_id, setClientID] = useState();
  const didMount = React.useRef(false);
  const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const [searchData, setSearchData] = useState(false)
  const [searchDataShow, setSearchDataShow] = useState(false)
  const [ShowDelete, setShowDelete] = useState(false)
  const [IndidualOption, setIndidualOption] = useState()
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState('')
  const [formData, setFormData] = useState({
    clientId: ""
  })
  const editModaltoggle = (status) => {
    setEditRow(null);
    setModalType("create");
    setEditModal(status);
  };
  const editHandler = (e, row) => {
    setEditRow(row);
    setModalType("edit");
    setEditModal(true);
  };
  const getSearchParams = (label) => {
    let oneWhitelist =
      whiteOptions && whiteOptions.filter((wo) => wo.label === label);
    if (oneWhitelist !== null && oneWhitelist.length > 0) {
      return oneWhitelist[0].fieldType;
    }
  };
  const filterHandler = (e, fiterType) => {
    if (fiterType === "queue") {
      setDefaultQueue(e);
      localStorage.setItem("WhiteQueue", e.label);

      const params = {
        queueId: e.value,
        fieldType: getSearchParams(defaultWhitelist.label),
        limit: pageRecords,
        page: currentPage,
        clientId: credBasedClientValue ? credBasedClientValue : ""
      };

      fetchWhitelistTypeDispatch(params);
    } else if (fiterType === "whitelist") {
      setDefaultWhitelist(e);
      localStorage.setItem("WhiteList", e.label);
      let fieldType = getSearchParams(e.label);

      const params = {
        queueId: defaultQueue.value,
        fieldType: fieldType,
        limit: pageRecords,
        page: currentPage,
        clientId: credBasedClientValue ? credBasedClientValue : ""
      };

      fetchWhitelistTypeDispatch(params);
    }
  };
  const formSubmitHandler = (id, formData) => {
    if (id === null) {
      postWhitelistTypeDispatch(formData);
    } else {
      updateWhitelistTypeDispatch(id, formData);
    }
  };
  const deleteHandler = (e, id) => {
    e.preventDefault();
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_WHITELIST,
      "warning",
      "Yes",
      "No",
      () => {
        deleteWhitelistTypeDispatch(id);
      }
    );
  };
  const handleRecordPerPage = (e) => {
    e.preventDefault();
    setPageRecords(e.target.value);
  }

  // const handlePageClick = (event) => {
  //   const pageNumber = event.selected + 1;
  //   setCurrentPage(pageNumber);
  // }


  const handlePageClick = (event) => {
    if (searchDataShow) {
      const pageNumber = event.selected + 1
      setCurrentPage(pageNumber)
      const params = {
        limit: pageRecords,
        page: pageNumber,
        fieldName: searchData && searchData.fieldName,
        fieldValue: searchData && searchData.fieldValue,
        queueId: searchData && searchData.queueId,
        fieldType: searchData && searchData.fieldType,
        clientId: searchData && searchData.clientId
          ? searchData && searchData.clientId
          : ""
      }
      fetchWhitelistTypeDispatch(params)
    } else {
      const pageNumber = event.selected + 1
      setCurrentPage(pageNumber)
      const params = {
        limit: pageRecords,
        page: pageNumber,
        queueId: queue,
        fieldType: whitelist,
        clientId: credBasedClientValue
          ? credBasedClientValue
          : ""
      }
      setCurrentPage(pageNumber)
      fetchWhitelistTypeDispatch(params)
    }
    
  }
  const totalPages = whitelistCount && Math.ceil(parseInt(whitelistCount) / pageRecords)

  const getCurrentOption = (pageRecords) => {
    let rec = records && records.filter((r) => r.value === pageRecords);
    return rec[0];
  };
  const onReDispatch = () => {
    const params = {
      fieldType: getSearchParams(defaultWhitelist.label),
      queueId: defaultQueue.value,
      limit: pageRecords,
      page: currentPage,
    };
    fetchWhitelistTypeDispatch(params);
  };

  /*
    Dispatches
    */
  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
    fetchWhitelistDispatch();
    fetchQueueslistDispatch();
  }, []);

  useEffect(() => {
    if (setFilterFunction) {
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER);
      let fieldType =
        defaultWhitelist && getSearchParams(defaultWhitelist.label);
      let queueId = defaultQueue && defaultQueue.value;
      setCurrentPage(1);
      const params = {
        fieldType: fieldType,
        queueId: queueId,
        limit: pageRecords,
        page: currentPage,
        clientId: credBasedClientValue ? credBasedClientValue : "",
      };

      fetchWhitelistTypeDispatch(params);
      setFilterFunctionDispatch(false);
    }
  }, [setFilterFunction]);
  
  useEffect(() => {
    let fieldType = defaultWhitelist && getSearchParams(defaultWhitelist.label);
    let queueId = defaultQueue && defaultQueue.value;
    const params = {
      fieldType: fieldType,
      queueId: queueId,
      limit: pageRecords,
      page: currentPage,
      clientId: credBasedClientValue ? credBasedClientValue : "",
    };

    fetchWhitelistTypeDispatch(params);
  }, [defaultWhitelist, defaultQueue, pageRecords, currentPage]);

  /*
    Whitelist Data
    */
  useEffect(() => {
    let whiteTemp = [];
    if (whitelistData) {
      whiteTemp.push({
          value: "",
          label: "All",
          fieldType: "",
          Mcc: "",
      })
      Object.keys(whitelistData).forEach(function (key) {
        whiteTemp.push({
          value: whitelistData[key]._id,
          label: whitelistData[key].fieldName,
          fieldType: whitelistData[key].fieldType,
          Mcc: whitelistData[key].extraFields,
        });
      });

      setWhiteOptions(whiteTemp);

      // set selected option ... from localstorage
      if (whitelist) {
        let oneWhitelist = whiteTemp.filter((w) => w.label === whitelist);
        setDefaultWhitelist(oneWhitelist[0]);
      }
    }
  }, [whitelistData]);


  /*
    Queue Data
    */
  useEffect(() => {
    let queueTemp = [];
    if (QueuesList) {
       queueTemp.push({
          value: "",
          label: "All",
        });
      Object.keys(QueuesList).forEach(function (key) {
        queueTemp.push({
          value: QueuesList[key]._id,
          label: QueuesList[key].queueName,
        });
      });

      setQueueOptions(queueTemp);
      // set selected option ... from localstorage
      if (queue) {
        let oneQueue = queueTemp.filter((q) => q.label === queue);
        setDefaultQueue(oneQueue[0]);
      } else {
        setDefaultQueue(queueTemp[0]);
      }
    }
  }, [QueuesList])


  /*
    Create Success
    */
  useEffect(() => {
    if (createWhitelistTypeResponse && createWhitelistTypeResponse === "ok") {
      setEditModal(false);
      confirmationAlert(
        "success",
        "Created Successfully",
        "success",
        "Back to Whitelist",
        "Ok",
        () => {
          onReDispatch();
        },
        () => {
          onReDispatch();
        }
      );
    }
  }, [createWhitelistTypeResponse]);

  /*
    Update Success
    */
  useEffect(() => {
    if (updateWhitelistTypeResponse && updateWhitelistTypeResponse === "ok") {
      setEditModal(false);
      confirmationAlert(
        "success",
        "Updated Successfully",
        "success",
        "Back to Whitelist",
        "Ok",
        () => {
          onReDispatch();
        },
        () => {
          onReDispatch();
        }
      );
    }
  }, [updateWhitelistTypeResponse]);

  /*
    delete Success
    */
  useEffect(() => {
    if (deleteWhitelistTypeResponse && deleteWhitelistTypeResponse === "ok") {
      onReDispatch()
    }
  }, [deleteWhitelistTypeResponse])

  const [sorting, setSorting] = useState({
    ruleId: false,
    rulefield: false,
    ruleDescription: false,
    clientId: false
  })

  const onConfirmUpdate = () => {
    const params = {
      clientId: formData.clientId
    }
    WhiteListDeleteClientDispatch(params)
  }

  const DelteClientSubmit = () => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_CLIENT,
      'warning',
      'Yes',
      'No',
      () => { onConfirmUpdate() },
      () => { }
    )
  }

  const handleChangeIndidual = selectedOption => {
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setIndidualOption(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const DeleteData = WhiteDeletListClient && WhiteDeletListClient.data
    if (DeleteData && DeleteData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShowDelete(false)
      confirmationAlert(
        'success', DeleteData && DeleteData.message,
        'success',
        'Back to BlockList',
        'Ok',
        () => { { } }
      )
      WhiteListDeleteClientClearDispatch()
    }else if (DeleteData && DeleteData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DeleteData && DeleteData.message,
        '',
        'Try again',
        '',
        () => { }
      )
      WhiteListDeleteClientClearDispatch()
    }
  }, [WhiteDeletListClient])

  return (
    <>

      <Modal
        show={ShowDelete}
        size="lg"
        centered
        onHide={() => {
          setShowDelete(false)
          setSelectedIndidualOption(null)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(30 128 226), rgb(61 113 222))", "color": "#ffffff" }}
          closeButton={() => setShowDelete(false)}>
          <Modal.Title>Delete Client Category</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3 form-label">
                  Client :
                </label>
              </div>
              <div className='col-md-8'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name="clientId"
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeIndidual}
                  options={IndidualOption}
                  value={SelectedIndidualOption}
                  isDisabled={!IndidualOption}
                // isClearable
                />
              </div>
            </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  type="submit"
                  className='btn btn-light-primary m-1 mt-8'
                  onClick={DelteClientSubmit}
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className={`card card-custom card-stretch gutter-b`}>
        <div className="card-header border-0 pt-5">
          <div className="card-title align-items-start flex-column">
            <span className="text-muted font-size-sm">
              Record(s) per Page : &nbsp;{" "}
            </span>
            {/*                        <Select 
                            placeholder="" 
                            options={records} 
                            value={()=>getCurrentOption(pageRecords)}
                            onChange={(e)=>handleRecordPerPage(e)}
                            style={{width:"80px"}}
                            />*/}
            <div>
              <select
                className="form-select w-6rem"
                data-control="select"
                data-placeholder="Select an option"
                data-allow-clear="true"
                onChange={(e) => handleRecordPerPage(e)}
              >
                <option value="25" selected={pageRecords === 25 ? true : false}>
                  25
                </option>
                <option value="50" selected={pageRecords === 50 ? true : false}>
                  50
                </option>
                <option value="75" selected={pageRecords === 75 ? true : false}>
                  75
                </option>
                <option
                  value="100"
                  selected={pageRecords === 100 ? true : false}
                >
                  100
                </option>
              </select>
            </div>
          </div>
          <div className="card-toolbar">
            <div className="mr-2" >
              <span className="text-muted font-size-sm">Queue</span>
               <div style={{width:"150px"}}>
              <Select
                placeholder="Queue"
                options={queueOptions}
                value={defaultQueue}
                onChange={(e) => filterHandler(e, "queue")}
                style={{ width: "150px" }}
              />
              </div>
            </div>
            <div className="mr-2 ms-4 me-4">
              <span className="text-muted font-size-sm">Whitelist</span>
              <div style={{width:"150px"}}>
                <Select
                placeholder="Whitelist"
                options={whiteOptions}
                value={defaultWhitelist}
                onChange={(e) => filterHandler(e, "whitelist")}
                style={{ width: "150px"  }}
              />
              </div>
            </div>
            {
              <FindRole
                role={Role}
              >
                <div>
                  <div className="text-muted font-size-sm">&nbsp;</div>
                  <button
                    className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                    onClick={() => editModaltoggle(true)}
                  >
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    Add Whitelist
                  </button>
                  <button
                    className='btn btn-sm btn-light-danger btn-responsive font-5vw me-3 pull-right'
                    onClick={() => setShowDelete(true)}
                  >
                    <KTSVG
                      path='/media/icons/duotune/general/gen027.svg'
                    />
                    Delete All
                  </button>
                </div>
              </FindRole>
            }
            <Search
              clinetIdLists={clinetIdLists}
              QueueOptions={queueOptions}
              WhiteListOptions={whiteOptions}
              setSearchData={setSearchData}
              fetchWhitelistTypeDispatch={fetchWhitelistTypeDispatch}
              setSearchDataShow={setSearchDataShow}
            />
          </div>
        </div>
        <div className="card-body pt-2 pb-0 pl-5 pr-5">

          <div className="table-responsive white-wrapper">
            {/* <WhiteListTable
                      tableData={whitelistTypeData}
                      editHandler={editHandler}
                      deleteHandler={deleteHandler}
                      pageRecords={pageRecords}
                      currentPage={currentPage}
                    /> */}
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>S.No</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => SortingHandler("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>Client</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => SortingHandler("clientId")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Field Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => SortingHandler("fieldName")}
                        >
                          <i
                            className={`bi ${sorting.fieldName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Field Value</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                        // onClick={() => SortingHandler("fieldValue")}
                        >
                          <i
                            className={`bi ${sorting.fieldValue
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-200px text-start">
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                  </FindRole>
                </tr>
              </thead>
              <tbody>
                {!loading ? (
                  !_.isEmpty(whitelistTypeData) ? (
                    whitelistTypeData.map((item, id) => {
                      return (
                        <tr
                          key={id}
                          style={
                            id === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                          <td className="pb-0 pt-5  text-start">
                            {id + 1}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item?.clientId?.company
                              ? item.clientId?.company
                              : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item && item.fieldName ? item.fieldName : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item && item.fieldValue ? item.fieldValue : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            <span className={`badge ${STATUS_BADGE[item.status]}`}>
                              {item && item.status ? item.status : "--"}
                            </span>
                          </td>
                          <FindRole
                            role={Role}
                          >
                            <td className="pb-0 pt-5  text-start">
                              <button
                                className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
                                onClick={(e) => editHandler(e, item)}>
                                <KTSVG
                                  path='/media/icons/duotune/art/art005.svg'
                                  className='svg-icon-3'
                                  title="Edit BlockList"
                                />
                              </button>
                              <button
                                className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                onClick={(e) => deleteHandler(e, item._id)}
                                title="Delete BlockList"
                              >
                                <KTSVG
                                  path='/media/icons/duotune/general/gen027.svg'
                                  className='svg-icon-3'
                                />
                              </button>
                            </td>
                          </FindRole>
                        </tr>
                      )
                    })
                  )
                    : (
                      <tr className='text-center py-3'>
                        <td colSpan='100%'>No record(s) found</td>
                      </tr>
                    )
                )
                  :
                  (
                    <tr>
                      <td colSpan='100%' className='text-center'>
                        <div
                          className='spinner-border text-primary m-5'
                          role='status'
                        />
                      </td>
                    </tr>
                  )
                }
              </tbody>
            </table>
            <ReactPaginate
              nextLabel="Next >"
              onPageChange={handlePageClick}
              pageRangeDisplayed={3}
              marginPagesDisplayed={2}
              pageCount={totalPages}
              forcePage={currentPage - 1}
              previousLabel="< Prev"
              pageClassName="page-item"
              pageLinkClassName="page-link"
              previousClassName="page-item"
              previousLinkClassName="page-link"
              nextClassName="page-item"
              nextLinkClassName="page-link"
              breakLabel="..."
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName="pagination"
              activeClassName="active"
              activeLinkClassName="active_a"
              renderOnZeroPageCount={null}
            />
          </div>
        </div>
      </div>
      <WhitelistModal
        modalType={modalType}
        editModal={editModal}
        QueueOptions={queueOptions}
        WhiteOptions={whiteOptions}
        editModaltoggle={editModaltoggle}
        formSubmitHandler={formSubmitHandler}
        editRow={editRow}
      />
    </>
  );
}

const mapStateToProps = (state) => {
  const { WhiteListStore, queueslistStore, whiteListStore, clinetListStore, WhiteListDeleteClientStore } = state;

  return {
    loading: state && state.WhiteListStore && state.WhiteListStore.loading,
    whitelistData:
      WhiteListStore && WhiteListStore.whitelistData
        ? WhiteListStore.whitelistData
        : null,
    whitelistCount:
      WhiteListStore && WhiteListStore.whitelistCount
        ? WhiteListStore.whitelistCount
        : 0,
    whitelistTypeData:
      WhiteListStore && WhiteListStore.whitelistTypeData
        ? WhiteListStore.whitelistTypeData
        : null,
    createWhitelistTypeResponse:
      WhiteListStore && WhiteListStore.createWhitelistTypeResponse
        ? WhiteListStore.createWhitelistTypeResponse
        : null,
    updateWhitelistTypeResponse:
      WhiteListStore && WhiteListStore.updateWhitelistTypeResponse
        ? WhiteListStore.updateWhitelistTypeResponse
        : null,
    deleteWhitelistTypeResponse:
      WhiteListStore && WhiteListStore.deleteWhitelistTypeResponse
        ? WhiteListStore.deleteWhitelistTypeResponse
        : null,
    QueuesList:
      queueslistStore && queueslistStore.queueslists?.data
        ? queueslistStore.queueslists?.data
        : null,
    WhiteListCSV:
      whiteListStore && whiteListStore.queueslists?.data
        ? whiteListStore.queueslists?.data
        : null,
    setFilterFunction:
      state &&
        state.clientCrudFilterStore &&
        state.clientCrudFilterStore.setFilterFunction &&
        state.clientCrudFilterStore.setFilterFunction
        ? state.clientCrudFilterStore.setFilterFunction
        : false,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    WhiteDeletListClient: WhiteListDeleteClientStore && WhiteListDeleteClientStore.WhiteDeletListClient ? WhiteListDeleteClientStore.WhiteDeletListClient : '',

  };
};
const mapDispatchToProps = (dispatch) => ({
  fetchWhitelistDispatch: (params) =>
    dispatch(WhiteListActions.fetchWhitelist(params)),
  fetchWhitelistTypeDispatch: (params) =>
    dispatch(WhiteListActions.fetchWhitelistType(params)),
  postWhitelistTypeDispatch: (formData) =>
    dispatch(WhiteListActions.createWhitelistType(formData)),
  updateWhitelistTypeDispatch: (id, formData) =>
    dispatch(WhiteListActions.updateWhitelistType(id, formData)),
  deleteWhitelistTypeDispatch: (id) =>
    dispatch(WhiteListActions.deleteWhitelistType(id)),
  fetchQueueslistDispatch: () => dispatch(queuesActions.getqueueslist()),
  setFilterFunctionDispatch: (data) =>
    dispatch(clientCredFilterActions.setFilterFunction(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  WhiteListDeleteClientDispatch: (data) => dispatch(WhiteListDeleteClientActions.WhiteListDeleteClientInit(data)),
  WhiteListDeleteClientClearDispatch: (data) => dispatch(WhiteListDeleteClientActions.WhiteListDeleteClientClear(data)),


});
export default connect(mapStateToProps, mapDispatchToProps)(Whitelist);
