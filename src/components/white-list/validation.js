import { USER_MANAGEMENT_ERROR, REGEX } from "../../utils/constants";
import _ from 'lodash'

export const userValidation = (values, setErrors) => {
  const errors = {};
  if (!values.file) {
    errors.file = 'CSV File is required.'
  }
  if (!values.clientId) {
    errors.clientId = 'Client is required.'
  }
  setErrors(errors);
  return errors;
};

export const manualValidation = (values, setError) => {
  const error = {};
  if (!values.website) {
    error.website = 'Website is required.'
  } else if (values.website && !REGEX.WEBSITE_URL.test(values.website)) {
    error.website = 'Website is InValid'
  }
  if (!values.tag) {
    error.tag = 'Tag is required.'
  }
  setError(error);
  return error;
};

export const searchValidation = (values, setError) => {
  const error = {};
  // if (!values.website) {
  //   error.website = 'Website is required.'
  // } else if (values.website && !REGEX.WEBSITE_URL.test(values.website)) {
  //   error.website = 'Website is InValid'
  // }
  if (values.website && !REGEX.WEBSITE_URL.test(values.website.toLocaleLowerCase())) {
    error.website = 'Website is InValid'
  }
  if (!values.tag) {
    error.tag = 'Tag is required.'
  }
  setError(error);
  return error;
};