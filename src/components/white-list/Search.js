import React, { useEffect, useState, useRef } from 'react'
import Modal from 'react-bootstrap/Modal'
import { connect } from 'react-redux'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { KTSVG } from '../../theme/helpers'

function Search(props) {
  const {
    clinetIdLists,
    QueueOptions,
    WhiteListOptions,
    setSearchData,
    fetchWhitelistTypeDispatch,
    setSearchDataShow
  }
    = props

  const [showSearch, setshowSearch] = useState(false)
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    fieldName: '',
    queueId: '',
    fieldType: '',
    fieldData: '',
    clientId: ""
  })

  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [editQueue, setEditQueue] = useState(null);
  const [editBlacklist, setEditBlacklist] = useState(null);
  const [blacklistLable, setBlacklistLabel] = useState("--");

  const [IndidualOption, setIndidualOption] = useState()
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState('')

  const SubmitHandlerManual = (event) => {
    event.preventDefault();

    // Validate
    // if (formData.clientId === "") {
    //   setErrors({
    //     ...errors,
    //     ["clientId"]: `Client Is Required`
    //   })
    // }
    // else if (formData.queueId === "") {
    //   setErrors({
    //     ...errors,
    //     ["queueId"]: `Queue Is Required`
    //   })
    // }
    // else if (formData.fieldName === "") {
    //   setErrors({
    //     ...errors,
    //     ["fieldName"]: `${formData.fieldName} Field Name Is Required`,
    //   })
    // }
    // else if (formData.fieldData === "" || formData.fieldData === undefined) {
    //   setErrors({
    //     ...errors,
    //     ["fieldData"]: `${formData.fieldName} Is Required`,
    //   })
    // }

    // Form Submit Process
      let filteredArray = [];
      let f_array = formData.fieldData.split(',');

      f_array.forEach(element => {
        let f_value = element.trim();
        filteredArray.push({
          fieldValue: f_value,
        });
      });

      let data = {
        "clientId": formData.clientId,
        "queueId": formData.queueId,
        "fieldType": formData.fieldType,
        "fieldValue": formData.fieldData
        // "blackItems": formData.fieldType === "shipping_address" ?
        //   [
        //     {
        //       "fieldValue": formData.fieldData
        //     }
        //   ] : filteredArray
      }
      fetchWhitelistTypeDispatch(data)
      setSearchData(data)
      setshowSearch(false)
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: '' })
    }
  }


  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : "#000",
      background: state.isSelected ? color.white : color.white,
    }),
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
    setIndidualOption(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const handleChangeIndidual = selectedOption => {
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: '' })
    }
  }

  const QueueChangeHandler = (e) => {
    setEditQueue(e)
    setFormData(values => ({ ...values, queueId: e.value }))
    setErrors((values) => ({ ...values, queueId: "" }))
  }


  const BlacklistChangeHandler = (e) => {
    setBlacklistLabel(e.label)
    setEditBlacklist(e)
    setFormData((values) => ({
      ...values,
      fieldName: e.label,
      fieldType: e.fieldType
    }))
    setErrors((values) => ({ ...values, fieldName: "" }))
  }

  const FielddataChangeHandler = (event) => {
    setFormData(values => ({ ...values, fieldData: event.target.value }))
    setErrors({ ...errors, ["fieldData"]: '' })
  }

  return (
    <>
      <div>
        <div className="text-muted font-size-sm">&nbsp;</div>
        <button
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          onClick={() => {
            setSearchDataShow(true)
            setshowSearch(true)
            setSelectedIndidualOption(null)
            setEditQueue(null)
            setEditBlacklist(null)
          }}
          >
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />

          Search
        </button>
      </div>
      <Modal
        show={showSearch}
        size="lg"
        centered
        onHide={() => {
          setshowSearch(false)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(30 128 226), rgb(61 113 222))", "color": "#ffffff" }}
          closeButton={() => setshowSearch(false)}>
          <Modal.Title>{'Search'} BlockList</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={SubmitHandlerManual} className="dyna-block-form">
            <div className="card card-custom card-stretch gutter-b p-8">
              <div className="row mb-8">
                <div className='col-md-4'>
                  <label className="font-size-xs font-weight-bold mb-3 form-label">
                    Client :
                  </label>
                </div>
                <div className='col-md-8'>
                  <ReactSelect
                    styles={customStyles}
                    isMulti={false}
                    name="clientId"
                    placeholder="Select..."
                    className="basic-single"
                    classNamePrefix="select"
                    handleChangeReactSelect={handleChangeIndidual}
                    options={IndidualOption}
                    value={SelectedIndidualOption}
                    isDisabled={!IndidualOption}
                  // isClearable
                  />
                  {errors && errors.clientId && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red}"}</style>
                      {errors.clientId}
                    </div>
                  )}
                </div>
              </div>

              <div className="row mb-8">
                <div className='col-md-4'>
                  <label className="font-size-xs font-weight-bold mb-3 form-label">
                    Select Queues :
                  </label>
                </div>
                <div className='col-md-8'>
                  <ReactSelect
                    styles={customStyles}
                    isMulti={false}
                    name='AppUserId'
                    className='select2'
                    classNamePrefix='select'
                    handleChangeReactSelect={QueueChangeHandler}
                    options={QueueOptions}
                    value={editQueue}
                    // isClearable={true}
                     />
                  {errors && errors.queueId && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red}"}</style>
                      {errors.queueId}
                    </div>
                  )}
                </div>
              </div>
              <div className="row mb-8">
                <div className='col-md-4'>
                  <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Select WhiteList :
                  </label>
                </div>
                <div className='col-md-8'>
                  <ReactSelect
                    styles={customStyles}
                    isMulti={false}
                    name='fieldName'
                    className='select2'
                    classNamePrefix='select'
                    handleChangeReactSelect={BlacklistChangeHandler}
                    options={WhiteListOptions}
                    value={editBlacklist}
                    isDisabled={false}
                  // isClearable={true} 
                  />
                  {errors && errors.fieldName && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red}"}</style>
                      {errors.fieldName}
                    </div>
                  )}
                </div>
              </div>
              <div className="row mb-4">
                <div className='col-md-4'>
                  <label className="font-size-xs font-weight-bold mb-3  form-label">
                    {blacklistLable} :
                  </label>
                </div>
                <div className='col-md-8'>
                  <textarea
                    onChange={(e) => FielddataChangeHandler(e)}
                    name={'--'}
                    defaultValue={''}
                    className="form-control form-control-solid" />
                  {errors && errors["fieldData"] && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red}"}</style>
                      {errors["fieldData"]}
                    </div>
                  )}
                </div>
              </div>
              <div className="row">
                <div className='col-md-4'>
                </div>
                <div className='col-md-8'>
                  <button type="submit" className='btn btn-light-primary rounded-0 m-1 mt-8'>
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  )
}

export default Search