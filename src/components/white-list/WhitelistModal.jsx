import React, { useState, useEffect, useRef } from 'react';
import { Modal, Row, Col, Form } from 'react-bootstrap'
import { Formik, Field, Form as Formikform, ErrorMessage } from 'formik'
import Select from "react-select";
import * as Yup from 'yup';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { STATUS_RESPONSE, DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT } from '../../utils/constants'
import _ from 'lodash'
import { userValidation } from './validation'
import { connect } from 'react-redux'
import {
  whiteListUploadActions
} from '../../store/actions'
import {
  warningAlert,
  confirmationAlert
} from "../../utils/alerts"
import { CSVLink } from "react-csv";

const WhitelistModal = (props) => {
  const {
    modalType,
    editModal,
    editModaltoggle,
    QueueOptions,
    WhiteOptions,
    formSubmitHandler,
    editRow,
    WhiteListCSV,
    postCSVLoading,
    PostCSVdispatch,
    ClearCSVdispatch,
    clinetIdLists,
    updateWhitelistTypeResponse,
    createWhitelistTypeResponse
  } = props

  const [fieldLabel, setFieldLabel] = useState("--")
  const [queueOption, setQueueOption] = useState(null)
  const [whiteOption, setWhiteOption] = useState(null)
  const [tabDefault, setTabDefault] = useState('Manuval');
  const [createForm, setCreateForm] = useState(false)
  const [mccValue, setMccValue] = useState()
  const [showEdit, setshowEdit] = useState(false)
  const [IndidualOption, setIndidualOption] = useState()
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState('')
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')

  const [error, setError] = useState({})
  const [errors, setErrors] = useState({});
  const [fileName, setFileName] = useState()
  const hiddenFileInput = useRef(null);
  const [QueueAsigneesOption, setQueueAsignees] = useState()
  const [SelectedQueueAsigneesOption, setSelectedQueueAsigneesOption] = useState('')
  const [fieldTypeOption, setfieldTypeOtion] = useState()
  const [SelectedfieldTypeOption, setSelectedfieldTypeOption] = useState('')
  const [formData, setFormData] = useState({
    queueId: '',
    fieldName: '',
    fieldType: '',
    file: '',
    clientId: '',
    mcc_code: "",
    risk_classification: "",
    risk_catagory_status: "",
    manual_review: "",
    risk_level: ""
  })

  const initialValues = {
    queueId: editRow ? editRow.queueId?._id : '',
    fieldName: editRow ? editRow.fieldName : '',
    fieldType: editRow ? editRow.fieldType : '',
    whiteItems: editRow ? editRow.fieldValue : '',
    mcc: editRow ? editRow.mcc_code : '',
    clientId: editRow && editRow.clientId && editRow.clientId._id ? editRow.clientId._id : ''
  }

  console.log('editRow', editRow)
  console.log('formData', formData)

  const validationSchema = Yup.object().shape({

    queueId: Yup.string()
      .required('QueueID is required'),
    fieldName: Yup.string()
      .required('Field Name is required'),
    whiteItems: Yup.string()
      .required('White Items is required'),
    clientId: Yup.string()
      .required('Client is required')
    // mcc: Yup.string()
    //   .required('Mcc is required')
  })

  const setFieldName = (e, fName) => {
    setFieldLabel(e.label)
  }

  function onSubmit(fields, { setStatus, setSubmitting, resetForm }) {
    
    setStatus();
    /*
    Multiple values
    */
    let filtered_array = [];
    if (fields.fieldType !== "shipping_address") {
      let items = fields.whiteItems.split(',');
      items.forEach(element => {
        let value = element.trim();
        filtered_array.push({
          fieldValue: value,
        });
      });
    }

    /*
    Submit Forms
    */

    let formDatas = {
      "clientId": fields.clientId,
      "queueId": fields.queueId,
      "fieldName": fields.fieldName,
      "fieldType": fields.fieldType,
      "mcc_code": formData.mcc_code,
      "risk_classification": formData.risk_classification,
      "risk_catagory_status": formData.risk_catagory_status,
      "manual_review": formData.manual_review,
      "risk_level": formData.risk_level,
      "whiteItems": fields.fieldType === "shipping_address" ? [
        {
          "fieldValue": fields.whiteItems
        }
      ] : filtered_array
    }

    let data = {
      "fieldValue": fields.whiteItems
    }

    let rowId = editRow ? editRow._id : null;
    formSubmitHandler(rowId, editRow !== null ? formDatas : formDatas)
    resetForm({})
    setSubmitting(false)
  }

  useEffect(() => {
    if (editRow !== null) {
      let QueueID = editRow?.queueId._id
      let ClientID = editRow?.clientId._id
      let ClientOption = IndidualOption.filter(t => t.value === ClientID);
      let QueueOption = QueueOptions.filter(t => t.value === QueueID);
      let WhiteOption = WhiteOptions.filter(w => w.label === editRow.fieldName);
      setSelectedIndidualOption(ClientOption[0])
      setQueueOption(QueueOption[0])
      setWhiteOption(WhiteOption[0])
      setFieldLabel(editRow.fieldName)
      setshowEdit(true)
      setFormData(values => ({
        ...values,
        mcc_code: editRow && editRow.extraFields && editRow.extraFields.mcc_code,
        risk_classification:  editRow && editRow.extraFields && editRow.extraFields.risk_classification ,
        risk_catagory_status: editRow && editRow.extraFields && editRow.extraFields.risk_catagory_status ,
        manual_review: editRow && editRow.extraFields && editRow.extraFields.manual_review ,
        risk_level: editRow && editRow.extraFields && editRow.extraFields.risk_level
      }))
    }
    else {
      setQueueOption(null)
      setWhiteOption(null)
      setFieldLabel("--")
    }
  }, [editRow])

  const tabToggle = (tab) => {
    setTabDefault(tab)

    // setCreateForm(false)
    // setEditItem(null)
    // setBlacklistLabel(null)
    // setEditQueue(null)
    // setEditBlacklist(null)
    // setFormData(values => ({ ...values, 
    //     fieldName: '', 
    //     queueId: '', 
    //     fieldType: '', 
    //     file: '', 
    //     fieldData: '', 
    // }))
    // setErrors({})
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : "#000",
      background: state.isSelected ? color.white : color.white,
    }),
  }

  const handleChangeQueueAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedQueueAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, queueId: selectedOption.value, }))
    }
  }

  const handleChangeFieldType = selectedOption => {
    if (selectedOption !== null) {
      setSelectedfieldTypeOption(selectedOption)
      setFormData(values => ({
        ...values,
        fieldType: selectedOption.value,
        fieldName: selectedOption.label
      }))
    }
  }

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setFormData((values) => ({
          ...values,
          file: files,
        }));
        setErrors((values) => ({ ...values, file: "" }));
        setFileName(files && files.name);
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  };
  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  };

  const OnCSVSubmit = () => {
    const errorMsg = userValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("file", formData.file)
      data.append("fieldName", formData.fieldName)
      data.append("queueId", formData.queueId)
      data.append("fieldType", formData.fieldType)
      data.append("clientId", formData.clientId)
      PostCSVdispatch(data)
    }
  }

  useEffect(() => {
    const QueueAsignees = getDefaultOption(QueueOptions)
    setQueueAsignees(QueueAsignees)
  }, [QueueOptions])

  const getDefaultOption = (QueueOptions) => {
    const defaultOptions = []
    for (const item in QueueOptions) {
      defaultOptions.push({ label: QueueOptions[item].label, value: QueueOptions[item].value })
    }
    return defaultOptions
  }

  useEffect(() => {
    const fieldType = getfieldValue(WhiteOptions)
    setfieldTypeOtion(fieldType)
  }, [WhiteOptions])

  const getfieldValue = (WhiteOptions) => {
    const defaultOptions = []
    for (const item in WhiteOptions) {
      defaultOptions.push({ label: WhiteOptions[item].label, value: WhiteOptions[item].fieldType })
    }
    return defaultOptions
  }

  const onConfirm = () => {
    ClearCSVdispatch()
    setFormData({
      ...formData,
      queueId: '',
      fieldName: '',
      fieldType: '',
      file: '',
    })
    editModaltoggle(false)
    setSelectedQueueAsigneesOption('')
    setSelectedfieldTypeOption('')
    setFileName('')
  }

  const clear = () => {
    ClearCSVdispatch()
    setFormData({
      ...formData,
      queueId: '',
      fieldName: '',
      fieldType: '',
      file: '',
    })
    setSelectedQueueAsigneesOption('')
    setSelectedfieldTypeOption('')
    setFileName('')
  }

  useEffect(() => {
    if (WhiteListCSV && WhiteListCSV.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        WhiteListCSV && WhiteListCSV.message,
        'success',
        'Back to White List',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
    } else if (WhiteListCSV && WhiteListCSV.status === STATUS_RESPONSE.SUCCESS_MSG) {
      warningAlert(
        'error',
        WhiteListCSV && WhiteListCSV.message,
        '',
        'Try again',
        '',
        () => { }
      )
    }

  }, [WhiteListCSV])

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getIndDefaultOption(AsigneesNames)
    setAsignees(Asignees)
    setIndidualOption(Asignees)
  }, [AsigneesNames])

  const getIndDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: '' })
    }
  }

  const handelChange = (event) => {
    const { name, value } = event.target
    setFormData(values => ({ ...values, [name]: value }))
  }

  
  /*
    Create Success
    */
    useEffect(() => {
      if (createWhitelistTypeResponse && createWhitelistTypeResponse === "ok") {
        setSelectedIndidualOption('')
        setFormData({
          queueId: '',
          fieldName: '',
          fieldType: '',
          file: '',
          clientId: '',
          mcc_code: "",
          risk_classification: "",
          risk_catagory_status: "",
          manual_review: "",
          risk_level: ""
        })
      }
    }, [createWhitelistTypeResponse]);
  
    /*
      Update Success
      */
    useEffect(() => {
      if (updateWhitelistTypeResponse && updateWhitelistTypeResponse === "ok") {
        setSelectedIndidualOption('')
        setFormData({
          queueId: '',
          fieldName: '',
          fieldType: '',
          file: '',
          clientId: '',
          mcc_code: "",
          risk_classification: "",
          risk_catagory_status: "",
          manual_review: "",
          risk_level: ""
        })
      }
    }, [updateWhitelistTypeResponse]);

  const data = [
    {
      fieldValue: "/Adult",
      mcc_code: 'N/A',
      risk_classification: 'Prohibited',
      risk_level: "Prohibited",
      risk_catagory_status: 'Blacklist'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Rail Transport",
      mcc_code: '4112',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Bus & Rail",
      mcc_code: '4131',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Freight & Trucking",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Mail & Package Delivery",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Hobbies & Leisure/Water Activities/Boating",
      mcc_code: '7996',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Air Travel",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Air Travel/Airport Parking & Transportation",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Cruises & Charters",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Maritime Transport",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    }
  ]

  console.log('fieldLabel',fieldLabel)

  return (
    <Modal
      show={editModal}
      onHide={() => {
        editModaltoggle(false)
        setQueueOption(null)
        setWhiteOption(null)
        setFieldLabel("--")
        setshowEdit(false)
      }}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      className="custom-modal"
      centered>
      <Modal.Header closeButton
        style={{
          background: 'wheat'
        }}
      >
        <Modal.Title id="contained-modal-title-vcenter">
          {modalType === "create" ? "New Whitelist" : "Update Whitelist"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Tabs
          id="controlled-tab-example"
          activeKey={tabDefault}
          onSelect={(k) => {
            tabToggle(k)
            setCreateForm(true)
          }}
          className="ctab ftab"
        >
          <Tab eventKey="Manuval" title="Manual">
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={onSubmit}>
              {({ errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm }) => {
                return (
                  <Formikform id="signUpForm" className="signinupForm" noValidate>
                    <Field type="hidden" name="fieldType" />
                    <Form.Group as={Row} className="mb-3 mt-8" controlId="clientId">
                      <Form.Label column sm="3">
                        Client :
                      </Form.Label>
                      <Col sm="9">
                        <Select
                          name="clientId"
                          onBlur={() => setFieldTouched("clientId", true)}
                          onChange={(opt, e) => {
                            setFieldValue("clientId", opt.value)
                            setSelectedIndidualOption(opt)
                          }}
                          options={IndidualOption}
                          value={SelectedIndidualOption}
                          isDisabled={!IndidualOption}
                          error={errors.clientId}
                          touched={touched.clientId}
                          className={(errors.clientId && touched.clientId ? ' is-invalid' : '')} />
                        <ErrorMessage name="clientId" component="div" className="invalid-feedback" />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3 mt-8" controlId="queueId">
                      <Form.Label column sm="3">
                        Queue ID :
                      </Form.Label>
                      <Col sm="9">
                        <Select
                          name="queueId"
                          onBlur={() => setFieldTouched("queueId", true)}
                          onChange={(opt, e) => {
                            setFieldValue("queueId", opt.value);
                            setQueueOption(opt)
                          }}
                          value={queueOption}
                          options={QueueOptions}
                          error={errors.queueId}
                          touched={touched.queueId}
                          className={(errors.queueId && touched.queueId ? ' is-invalid' : '')}
                        />
                        <ErrorMessage name="queueId" component="div" className="invalid-feedback" />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3 mt-8" controlId="fieldName">
                      <Form.Label column sm="3">
                        White List :
                      </Form.Label>
                      <Col sm="9">
                        <Select
                          name="fieldName"
                          onBlur={() => setFieldTouched("fieldName", true)}
                          onChange={(opt, e) => {
                            setFieldValue("fieldName", opt.label);
                            setFieldValue("fieldType", opt.fieldType);
                            setFieldLabel(opt.label)
                            setMccValue(opt.Mcc)
                            setWhiteOption(opt)
                          }}
                          value={whiteOption}
                          options={WhiteOptions}
                          error={errors.fieldName}
                          touched={touched.fieldName}
                          className={(errors.fieldName && touched.fieldName ? ' is-invalid' : '')}
                        />
                        <ErrorMessage name="fieldName" component="div" className="invalid-feedback" />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3 mt-8" controlId="fieldName">
                      <Form.Label column sm="3">
                        {fieldLabel} :
                      </Form.Label>
                      <Col sm="9">
                        <Field
                          name="whiteItems"
                          as="textarea"
                          className={'form-control' + (errors.whiteItems && touched.whiteItems ? ' is-invalid' : '')} />
                        <ErrorMessage name="whiteItems" component="div" className="invalid-feedback" />
                      </Col>
                    </Form.Group>
                    {
                      !_.isEmpty(mccValue) ? (
                        <Form.Group as={Row} className="mb-3" controlId="fieldName">
                          <Form.Label column sm="3">
                            {mccValue && mccValue.mcc_code}
                          </Form.Label>
                          <Col sm="9">
                            <Field
                              name="mcc"
                              placeholder='Mcc'
                              className={'form-control' + (errors.mcc && touched.mcc ? ' is-invalid' : '')} />
                            <ErrorMessage name="mcc" component="div" className="invalid-feedback" />
                          </Col>
                        </Form.Group>
                      ) : (null)
                    }

                    {
                    fieldLabel === 'Website Category' ? (
                    <>
                      <div className="row mb-4">
                        <div className='col-md-3'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Mcc Code :
                          </label>
                        </div>
                        <div className='col-md-9'>
                          <input
                            onChange={(e) => handelChange(e)}
                            name='mcc_code'
                            value={formData.mcc_code}
                            placeholder='Mcc Code'
                            className="form-control form-control-solid" />
                        </div>
                      </div>
                      <div className="row mb-4">
                        <div className='col-md-3'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Risk Classification :
                          </label>
                        </div>
                        <div className='col-md-9'>
                          <input
                            onChange={(e) => handelChange(e)}
                            name='risk_classification'
                            placeholder='Risk Classification'
                            value={formData.risk_classification}
                            className="form-control form-control-solid" />
                        </div>
                      </div>
                      <div className="row mb-4">
                        <div className='col-md-3'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Risk Level :
                          </label>
                        </div>
                        <div className='col-md-9'>
                          <input
                            onChange={(e) => handelChange(e)}
                            name='risk_level'
                            placeholder='Risk Level'
                            value={formData.risk_level}
                            className="form-control form-control-solid" />
                        </div>
                      </div>
                      <div className="row mb-4">
                        <div className='col-md-3'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Risk Catagory Status :
                          </label>
                        </div>
                        <div className='col-md-9'>
                          <input
                            onChange={(e) => handelChange(e)}
                            name='risk_catagory_status'
                            placeholder='Risk Catagory Status'
                            value={formData.risk_catagory_status}
                            className="form-control form-control-solid" />
                        </div>
                      </div>
                      <div className="row mb-4">
                        <div className='col-md-3'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Manual Review :
                          </label>
                        </div>
                        <div className='col-md-9'>
                          <select
                            className='form-select'
                            data-control='select'
                            name='manual_review'
                            value={formData.manual_review}
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handelChange(e)}
                          >
                            <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                          </select>
                        </div>
                      </div>
                    </>
                     ):null
                  } 
                    <Form.Group as={Row} className="mb-3 mt-8" controlId="submitBtn">
                      <Form.Label column sm="3">
                        &nbsp;
                      </Form.Label>
                      <Col sm="9">
                        <button
                          type="submit"
                          disabled={isSubmitting}
                          className="btn btn-primary">
                          {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                          {modalType === "create" ? "Add Whitelist" : "Update Whitelist"}
                        </button>
                      </Col>
                    </Form.Group>
                  </Formikform>
                );
              }}
            </Formik>
          </Tab>
          {
            showEdit ? (null) : (
              <Tab eventKey="csvUpload" title="csvUpload" className="dyna-block-form">
                <div className="card card-custom card-stretch gutter-b p-8">
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3 form-label">
                        Client :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='clientId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                        isDisabled={!AsigneesOption}
                      // isClearable={true}
                      />
                      {errors && errors.clientId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.clientId}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row mb-8 mt-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Queue ID :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='QueueId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeQueueAsignees}
                        options={QueueAsigneesOption}
                        value={SelectedQueueAsigneesOption}
                      // isDisabled={!QueueAsigneesOption}
                      />
                      {error && error.client && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {error.client}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        FieldType :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='selectedType'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeFieldType}
                        options={fieldTypeOption}
                        value={SelectedfieldTypeOption}
                      // isDisabled={!fieldTypeOption}
                      />
                      {error && error.client && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {error.client}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Upload Document :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <input
                        type="file"
                        className="d-none"
                        name="file"
                        id="file"
                        multiple={true}
                        ref={hiddenFileInput}
                        onChange={handleFileChange}
                      />
                      <button
                        type="button"
                        style={{
                          width: "100%",
                        }}
                        className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                        onClick={handleClick}
                      >
                        <i className="bi bi-filetype-csv" />
                        Upload Document
                      </button>
                      {errors && errors.file && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.file}
                        </div>
                      )}
                      {fileName && fileName}
                    </div>
                  </div>
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <CSVLink
                        data={data}
                        filename={"sampleFileForImport.csv"}
                        className="btn btn-sm btn-light-success btn-responsive font-5vw"
                        target="_blank"
                      >
                        <i className="bi bi-filetype-csv" />
                        Download_Sample
                      </CSVLink>
                    </div>
                  </div>
                  <div className="row">
                    <div className='col-md-4'>
                    </div>
                    <div className='col-md-8'>
                      <button
                        type='button'
                        className='btn btn-sm btn-light-danger m-2 fa-pull-right'
                        onClick={(e) => OnCSVSubmit(e)}
                        disabled={postCSVLoading}
                      >
                        {!postCSVLoading && <span className='indicator-label'>Submit</span>}
                        {postCSVLoading && (
                          <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </Tab>
            )
          }
        </Tabs>
      </Modal.Body>
    </Modal>
  );
}

const mapStateToProps = state => {
  const {
    whiteListStore,
    clinetListStore,
    WhiteListStore
  } = state

  return {
    postCSVLoading: whiteListStore && whiteListStore.loading,
    WhiteListCSV: whiteListStore && whiteListStore.WhiteList && whiteListStore.WhiteList ? whiteListStore.WhiteList : null,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    createWhitelistTypeResponse:
      WhiteListStore && WhiteListStore.createWhitelistTypeResponse
        ? WhiteListStore.createWhitelistTypeResponse
        : null,
    updateWhitelistTypeResponse:
      WhiteListStore && WhiteListStore.updateWhitelistTypeResponse
        ? WhiteListStore.updateWhitelistTypeResponse
        : null,
  }
}
const mapDispatchToProps = dispatch => ({
  PostCSVdispatch: (params) => dispatch(whiteListUploadActions.postwhiteListUpload(params)),
  ClearCSVdispatch: () => dispatch(whiteListUploadActions.clearwhiteListUpload())
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WhitelistModal);