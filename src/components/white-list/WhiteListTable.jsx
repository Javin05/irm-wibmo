import React, { useState, useEffect } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { KTSVG } from "../../theme/helpers";
import { STATUS_BADGE } from "../../utils/constants";

function WhiteListTable(props) {
  let rowdy = 0;
  const { tableData, editHandler, deleteHandler, pageRecords, currentPage } =
    props;
  const options = {
    sizePerPageList: [5, 10, 15, 20, 50],
    sizePerPage: pageRecords,
    page: currentPage,
    noDataText: "Data not available ....",
  };
  function getCaret(direction) {
    if (direction === "asc") {
      return (
        <span className="ml-2">
          <i className="bi bi-arrow-up-circle-fill text-primary"></i>
        </span>
      );
    }
    if (direction === "desc") {
      return (
        <span className="ml-2">
          <i className="bi bi-arrow-down-circle-fill text-primary"></i>
        </span>
      );
    }
    return (
      <span className="ml-2">
        <i className="bi bi-arrow-down-up"></i>
      </span>
    );
  }
  function actionColumn(cell, row) {
    return (
      <>
        <button
          className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
          onClick={(e) => editHandler(e, row)}
        >
          <KTSVG
            path="/media/icons/duotune/art/art005.svg"
            className="svg-icon-3"
            title="Edit Whitelist"
          />
        </button>
        <button
          className="btn btn-icon btn-icon-danger btn-sm w-10px h-10px"
          onClick={(e) => deleteHandler(e, row._id)}
          title="Delete BlockList"
        >
          <KTSVG
            path="/media/icons/duotune/general/gen027.svg"
            className="svg-icon-3"
          />
        </button>
      </>
    );
  }
  function actionIndex(cell, row) {
    rowdy++;
    return <span>{rowdy}</span>;
  }
  function StatusColumn(cell, row) {
    return (
      <span className={`badge ${STATUS_BADGE[row && row.status]}`}>
        {row && row.status ? row.status : "--"}
      </span>
    );
  }
  function GetClientID(cell, row) {
    return (
      <span className={`badge ${STATUS_BADGE[row && row.clientId]}`}>
        {row && row.clientId ? row.clientId : "--"}
      </span>
    );
  }

  return (
    <>
      {tableData && (
        <BootstrapTable
          options={options}
          data={tableData}
          pagination={false}
          table
          border
          hover
        >
          <TableHeaderColumn
            width="0"
            dataField="_id"
            isKey
          ></TableHeaderColumn>
          <TableHeaderColumn width="50" dataFormat={actionIndex}>
            ID
          </TableHeaderColumn>
          <TableHeaderColumn
            width="200"
            dataField="fieldName"
            dataSort={true}
            caretRender={getCaret}
          >
            Field Name
          </TableHeaderColumn>
          <TableHeaderColumn
            width="200"
            dataField="fieldValue"
            dataSort={true}
            caretRender={getCaret}
          >
            Field Value
          </TableHeaderColumn>
          <TableHeaderColumn
            width="200"
            dataFormat={GetClientID}
            dataSort={true}
            caretRender={getCaret}
          >
            Client
          </TableHeaderColumn>
          <TableHeaderColumn
            width="200"
            dataFormat={StatusColumn}
            dataSort={true}
            caretRender={getCaret}
          >
            Status
          </TableHeaderColumn>
          <TableHeaderColumn width="70" dataFormat={actionColumn}>
            Action
          </TableHeaderColumn>
        </BootstrapTable>
      )}
    </>
  );
}
export default WhiteListTable;
