import _ from "lodash";

export const setRuleData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      ruleGroupName: data.ruleGroupName,
      status: data.status,
      condition: data.condition,
      description: data.description,
      action: data.action,
      displayRuleLogic: data.displayRuleLogic,
      customRules: data.customRules && data.customRules.length > 0 ? 
      data.customRules : [
        {
          ruleField: '',
          ruleOperator: '',
          ruleValue: '',
          notes: ''
        }
      ]
    };
  }
}