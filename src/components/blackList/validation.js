import { USER_MANAGEMENT_ERROR, REGEX } from "../../utils/constants";
import _ from 'lodash'

export const userValidation = (values, setErrors) => {
  const errors = {};
  if (!values.clientId) {
    errors.clientId = 'Client is required.'
  }
  if (!values.file) {
    errors.file = 'CSV File is required.'
  }
  if (!values.queueId) {
    errors.queueId = 'Queue is required.'
  } if (!values.fieldName) {
    errors.fieldName = 'BlockList Name is required.'
  }

  setErrors(errors);
  return errors;
};
