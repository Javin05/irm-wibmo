import React from 'react'
import {
  TablesWidget9
} from '../../theme/partials/widgets'

const Tables = () => (
  <>
    <TablesWidget9 className='mb-5 mb-xl-8' />
  </>
)

export default Tables
