import React, { useEffect, useState } from "react"
import { connect } from "react-redux"
import "react-circular-progressbar/dist/styles.css"
import { useLocation } from "react-router-dom"
import { getLocalStorage, setLocalStorage } from "../../utils/helper"
import "./index.css"
// import Websites from './tabs/index'
import { OgmOperationManagementDetailActions, WrmOperationManagementBackendAPIStatusActions, WrmOperationManagementDetailActions, WrmOperatorActions } from '../../store/actions';
import { useHistory } from 'react-router-dom';
import Websites from './Website'


function AccountRiskSummary(props) {
  const {
    wrmOperatorActionDispatch,
    getOgmOperationDetails,
    OgmOperationManagementDetails,
    WrmOperationActionStatus,
    WrmOperationManagement,
    loading,
    WrmReportEditStatus
   
  } = props
     const localSkip = parseInt(getLocalStorage("skip"))

  const [time,setTime] = useState(null)
  const [timeShow,setTimeShow] =useState(false)
  const [skip,setSkip] = useState(localSkip?localSkip:1)
  const [showAutoAcceptCheckbox, setShowAutoAcceptCheckbox] = useState(false)
  const [autChecked,setAutoChecked] = useState(false)
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  useEffect(()=>{
    let params ={}
    params.id = id
    params.skip = skip
    getOgmOperationDetails(params)
  },[id,WrmOperationActionStatus,WrmReportEditStatus])
//   useEffect(()=>{
//      let params ={}
//     params.id = OgmOperationManagementDetails?.ogmId?.id
//     params.skip = skip
// OgmOperationManagementDetails?.ogmId?.id && 
//   },[OgmOperationManagementDetails])
  
 const [ids,setIds] = useState([])
  const handleAcceptAnalyst = ()=>{
    let params={}
    params.name = "analyst accept work";
    params.case = id;
    wrmOperatorActionDispatch(params)
  }
 
  const handleSubmitAnalyst = ()=>{
    let params={}
   params.name = "analyst complete";
   params.case = id;
   wrmOperatorActionDispatch(params)
    setTime(null)
    setTimeShow(false)
  }
   const handleAcceptQA = ()=>{
    let params={}
    params.name = "qa accept work";
    params.case = id;
    wrmOperatorActionDispatch(params)
      setTime(Date.now() + 900000)
    setTimeShow(true)
    setShowAutoAcceptCheckbox(true)
  }
   const handleSubmitQA = ()=>{
   let params={}
   params.name = "qa complete";
   params.case = id;
   wrmOperatorActionDispatch(params)
    setTime(null)
    setTimeShow(false)     
  }
     const handleHoldTask = ()=>{
   let params={}
   params.name = "on hold";
   params.case = id;
   wrmOperatorActionDispatch(params)
   setTime(null)
    setTimeShow(false)   
  }
   const handleToOperationSupport = ()=>{
   let params={}
   params.name = "assign to operation support";
   params.case = id;
   wrmOperatorActionDispatch(params) 
   setTime(null)
    setTimeShow(false)  
  }
    const handleTaskResume= ()=>{
   let params={}
   params.name = "resume";
   params.case = id;
   wrmOperatorActionDispatch(params)
   setTime(Date.now() + 900000)
  setTimeShow(true) 
  }
  const handleAcceptOperatoinSupportTask =()=>{
    let params={}
   params.name = "accept operatoin analyst task";
   params.case = id;
   wrmOperatorActionDispatch(params)
    setTime(Date.now() + 900000)
    setTimeShow(true)
  }
  const handleSubmitOperationSupport =()=>{
   let params={}
   params.name = "submit operation task";
   params.case = id;
   wrmOperatorActionDispatch(params)
   setTime(null)
    setTimeShow(false)
  }
  const history = useHistory();

  const hanldeNextTask =()=>{
    if(!OgmOperationManagementDetails?.nextTask?._id){
        history.push('/ogm-management')
    }else {
       history.push(`/ogmmanagement/update/${OgmOperationManagementDetails?.nextTask?._id}`)
       setSkip(skip+1)
       setLocalStorage("skip",skip+1)
    } 
  }

  useEffect(()=>{
     setIds(WrmOperationManagement?WrmOperationManagement.result:[])
  },[WrmOperationManagement])
  console.log(OgmOperationManagementDetails?.ogmId?._id)

  return (
    <>        
            
      {!loading?<>  {OgmOperationManagementDetails&&  <div className="col-xs-12 d-flex" style={{ display:"flex",justifyContent:"center",alignItem:"center"}}>
                <div  className="text-center d-grid gap-2 d-md-flex">
                  {<button type="button" className="btn btn-primary me-3" onClick={()=>history.push('/ogm-management')}>Home</button>}

                    {(OgmOperationManagementDetails?.workStatus==="PENDING" && OgmOperationManagementDetails?.operationStatus==="ANALYST REVIEW"||OgmOperationManagementDetails?.workStatus==="ISSUE RESOLVED"  && OgmOperationManagementDetails?.operationStatus==="ANALYST REVIEW")&&
                    <div>
                    <button type="button" className="btn btn-primary me-3"onClick={()=>handleAcceptAnalyst()}>Anayst Accept</button>
                    </div>
                    }
                    {OgmOperationManagementDetails?.workStatus==="IN PROGRESS" && OgmOperationManagementDetails?.operationStatus==="ANALYST REVIEW" && 
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleSubmitAnalyst();hanldeNextTask()}}>Submit</button>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleHoldTask();hanldeNextTask()}}>Hold</button>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleToOperationSupport();hanldeNextTask()}}>Assign to Operation Support</button>
                    </div>
                    }
                     {OgmOperationManagementDetails?.workStatus==="IN PROGRESS" && OgmOperationManagementDetails?.operationStatus==="OPERATION SUPPORT REVIEW" && 
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleSubmitOperationSupport();hanldeNextTask()}}>Submit</button>
                    </div>
                    }
                   
                    {(OgmOperationManagementDetails?.workStatus==="PENDING" && OgmOperationManagementDetails?.operationStatus==="QA REVIEW"|| OgmOperationManagementDetails?.workStatus==="ISSUE RESOLVED"  && OgmOperationManagementDetails?.operationStatus==="QA REVIEW")&& 
                    <div>
                    <button type="button" className="btn btn-primary me-3"onClick={()=>handleAcceptQA()}>QA Accept</button>
                    </div>
                    }
                      {OgmOperationManagementDetails?.workStatus==="PENDING"  &&OgmOperationManagementDetails?.operationStatus==="OPERATION SUPPORT REVIEW"&& 
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>handleAcceptOperatoinSupportTask()}>Accept Operation Review</button>
                    </div>
                    }
                     {OgmOperationManagementDetails?.workStatus==="IN PROGRESS" && OgmOperationManagementDetails?.operationStatus==="QA REVIEW" && 
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleSubmitQA();hanldeNextTask()}}>Submit</button>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleHoldTask();hanldeNextTask()}}>Hold</button>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleToOperationSupport();hanldeNextTask()}}>Assign to Operation Support</button>
                    </div>
                    }
              
                     {OgmOperationManagementDetails?.workStatus==="ON HOLD" &&
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>handleTaskResume()}>Resume</button>
                    </div>
                    }
               
                    {OgmOperationManagementDetails?.workStatus!=="IN PROGRESS"&&<button type="button" className="btn btn-primary me-3" onClick={()=>hanldeNextTask()}>Skip</button>}
 
                </div>
            </div>}

            <br/>
            <br/>
          <div className="col-md-12 card card-xl-stretch mb-xl-8">   
              <div className="separator separator-dashed my-3" />
              <div className="card-body pt-0" >
                <div className="row g-5 g-xl-8">
                {OgmOperationManagementDetails?.ogmId?._id?<Websites
                  OgmOperationManagementDetails={OgmOperationManagementDetails}
                  ids = {OgmOperationManagementDetails?.ogmId?._id}
                />:null}
            </div>
        </div> 
    </div></>:     (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )}
    </>
  )
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    OgmOperationManagementDetails: state && state.OgmOperationManagementDetailStore && state.OgmOperationManagementDetailStore.OgmOperationManagementDetail,
    // loading: WrmOperationManagementStore && WrmOperationManagementStore.loading ? WrmOperationManagementStore.loading : false,
    WrmOperationActionStatus:state&&state.wrmOperationManagementActionStore && state.wrmOperationManagementActionStore,
    loading: state && state.OgmOperationManagementDetailStore && state.OgmOperationManagementDetailStore.loading,
    WrmOperationManagement: state && state.WrmOperationManagementStore && state.WrmOperationManagementStore.WrmOperationManagement&&state.WrmOperationManagementStore.WrmOperationManagement.data,
    WrmReportEditStatus: state && state.WrmReportEditStore &&state.WrmReportEditStore.WrmEditRerportMessage,

  }
}

const mapDispatchToProps = (dispatch) => ({
  getOgmOperationDetails: (id)=>dispatch(OgmOperationManagementDetailActions.getOgmOperationDetail(id)),
  wrmOperatorActionDispatch:(params)=>dispatch(WrmOperatorActions.wrmOperatorActionInit(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountRiskSummary)