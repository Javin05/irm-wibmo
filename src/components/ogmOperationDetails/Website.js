import React, { useEffect, useState } from "react"
import "react-circular-progressbar/dist/styles.css"
import { useLocation } from "react-router-dom"
import { connect } from 'react-redux'
import _ from 'lodash'
import './index.css'
import {
  MonitorDashboardActions,
  MonitorDashboardIdActions,
  MonitorDashboardDateActions,
} from '../../store/actions'
import MonitorIdDetails from './merchantDetails'
import ContentViolation from './ContentViolation'
import SummaryDashboard from './SummaryDashboard'
import NetworkGraph from "./Netwok"

function Websites(props) {
  const {
    loading,
    MonitorDashboardDispatch,
    MonitorDashboardData,
    getMonitorDashboardIdDispatch,
    MonitorDashboardIData,
    getMonitorDashboardDateDispatch,
    MonitorDashboardDateData,
    ids
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  // const id = fields && fields[3]
  const id = ids
  const [activestep, setActiveStep] = useState(0)
  const [completed] = useState({})
  const steps = getSteps()
  const [NameData, setNameData] = useState()

useEffect(() =>{
  MonitorDashboardDispatch(id)
  getMonitorDashboardIdDispatch(id)
  getMonitorDashboardDateDispatch(id)
},[id])

console.log('MonitorDashboardData', MonitorDashboardData)
// const viewData = MonitorDashboardData && MonitorDashboardData && MonitorDashboardData.data ? MonitorDashboardData.data : []
// const getData = viewData.filter(o => (o ? o : null))
// const website = getData && getData[0] && getData[0].website ? getData[0].website : '--'

// useEffect(() => {
//   if (website) {
//     var clearURL = website.replace('https://', '').replace('http://', '').replace('www.', '').replace('/', '')
//     var splitURL = clearURL.split('.')
//     var domainLegalName = ''
//     if (splitURL.length > 0) {
//       domainLegalName = splitURL[0]
//     }
//     setNameData(domainLegalName)
//   }
// }, [website])

  function getSteps() {
    return [
      {
        label: "Summary",
        className: "btn web-label-six",
        stepCount: 0,
      },
      {
        label: "Content Violation",
        className: "btn web-label-six",
        stepCount: 1,
      },
      {
        label: "Network Analysis",
        className: "btn web-label-six",
        stepCount: 2,
      }
    ]
  }

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <SummaryDashboard 
          MonitorDashboardData={MonitorDashboardData}
          loading={loading}
          MonitorDashboardDateData={MonitorDashboardDateData}
          MonitorDashboardIData={MonitorDashboardIData}
          setActiveStep={setActiveStep}
          />
        )
      case 1:
        return (
          <ContentViolation 
          MonitorDashboardData={MonitorDashboardData}
          loading={loading}
          MonitorDashboardIData={MonitorDashboardIData}
          />
        )
      case 2:
        return (
          <NetworkGraph 
          merchant={MonitorDashboardIData} 
          // networkData={networkData} 
          MonitorDashboardData={MonitorDashboardData}
          />
        )
      default:
        return "unknown step"
    }
  }

  const handleNext = (step) => {
    setActiveStep(activestep + 1)
  }

  const handleStep = (step) => () => {
    setActiveStep(step)
  }

  const handleBack = () => {
    setActiveStep(activestep - 1)
  }

  return (
    <>
      <div className="mt-0">
        <div className="mb-4">
          <MonitorIdDetails 
          NameData={NameData}
          MonitorDashboardIData={MonitorDashboardIData}
          />
        </div>
        <>
          <div className="d-flex">
            {steps.map((step, index) => (
              <div
                key={"A_" + index}
                completed={completed[index]}
                className={`my-10 mx-1 text mb-4 rounded-1 seven-label fs-8 fw-bolder text-gray-800 ${loading ? "event-disable" : ""
                  } ${step.stepCount === activestep
                    ? "btn web-label-sixActive fs-8 fw-bolder"
                    : `${step.className}`
                  }`}
                onClick={handleStep(index)}
              >
                {step.label}
              </div>
            ))}
          </div>
          {activestep === steps.length ? null : (
            <>
              <div>{getStepContent(activestep)}</div>
            </>
          )}
        </>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { MonitorDashboardStore, MonitorDashboardIdStore, MonitorDashboardDateStore } = state
  return {
    MonitorDashboardData: MonitorDashboardStore && MonitorDashboardStore.MonitorDashboardData ? MonitorDashboardStore.MonitorDashboardData : {},
    loading: MonitorDashboardStore && MonitorDashboardStore.loading ? MonitorDashboardStore.loading : false,
    MonitorDashboardIData: MonitorDashboardIdStore && MonitorDashboardIdStore.MonitorDashboardIData ? MonitorDashboardIdStore.MonitorDashboardIData && MonitorDashboardIdStore.MonitorDashboardIData.data : {},
    MonitorDashboardDateData: MonitorDashboardDateStore && MonitorDashboardDateStore.MonitorDashboardDateData ? MonitorDashboardDateStore.MonitorDashboardDateData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  MonitorDashboardDispatch: (id) => dispatch(MonitorDashboardActions.getMonitorDashboard(id)),
  getMonitorDashboardIdDispatch: (id) => dispatch(MonitorDashboardIdActions.getMonitorDashboardId(id)),
  getMonitorDashboardDateDispatch: (id) => dispatch(MonitorDashboardDateActions.getMonitorDashboardDate(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Websites)
