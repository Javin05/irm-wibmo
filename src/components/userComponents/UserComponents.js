import React, { useEffect, useState } from 'react'
import { Link, useLocation, useHistory } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  getMenuCompActions,
  deleteComponentsActions
} from '../../store/actions'
import { connect } from 'react-redux'
import { getUserPermissions } from '../../utils/helper'
import { Can } from '../../theme/layout/components/can'
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  DELETE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from '../../utils/constants'
import { warningAlert, confirmationAlert, confirmAlert } from '../../utils/alerts'

const UserComponents = (props) => {
  const {
    className,
    getMenuComponents,
    getMenuComp,
    loadingGMC,
    messageDeleteComp,
    statusDeleteComp,
    deleteComponentsDispatch,
    clearComponentDispatch
  } = props
  const history = useHistory()
  const url = useLocation().pathname
  const getUsersPermissions = getUserPermissions(url)

  const searchName = useLocation().search
  const compId = searchName && searchName.split('user-components/')
  const id = compId && compId[1]
  const [editMode, setEditMode] = useState(false)
  const [limit] = useState(25)

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1,
      tag:"IRM"
    }
    getMenuComponents(params)
  }, [])

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1,
      tag:"IRM"
    }
    getMenuComponents(params)
  }, [limit])

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_USER_COMPONENT,
      'warning',
      'Yes',
      'No',
      () => {
        deleteComponentsDispatch(id)
      },
      () => { }
    )
  }

  useEffect(() => {
    if (id) {
      setEditMode(true)
    } else {
      setEditMode(false)
    }
  }, [id])

  const onConfirm = () => {
  }

  useEffect(() => {
    if (statusDeleteComp === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageDeleteComp,
        "success",
        "ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearComponentDispatch()
    } else if (statusDeleteComp === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageDeleteComp,
        'error',
        'Ok',
        () => {
          onConfirm()
        },
        () => { }
      )
      clearComponentDispatch()
    }
  }, [statusDeleteComp])

  const table = (subComponents) => {
    return (
      <div className='table-responsive'>
        <table className='table table-hover table-rounded table-striped border gs-2'>
          <thead className='fw-bolder fs-6 text-gray-800'>
            <tr className='fw-bolder' style={{ backgroundColor: 'rgb(189 219 200)' }}>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={UPDATE_DELETE_PERMISSION}
              >
                <th className='text-center my-auto'>
                  <span>Action</span>
                </th>
              </Can>
              <th>
                <span>Sub Components</span>
              </th>
            </tr>
          </thead>
          <tbody className='fs-8'>
            {!loadingGMC ? (
              subComponents && subComponents.length > 0 ? (
                subComponents.map((item, i) => {
                  return (
                    <tr
                      key={i}
                      style={
                        i === 0
                          ? { borderColor: 'white' }
                          : { borderColor: 'white' }
                      }
                    >
                      <Can
                        permissons={getUsersPermissions}
                        componentPermissions={UPDATE_DELETE_PERMISSION}
                      >
                        <td className='pb-0 pt-3 text-center my-auto'>
                          <Can
                            permissons={getUsersPermissions}
                            componentPermissions={UPDATE_PERMISSION}
                          >
                            <button
                              className='btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm'
                              onClick={() => {
                                history.push(
                                  `/update-user-components/${item._id}`
                                )
                              }}
                            >
                              {/* eslint-disable */}
                              <KTSVG
                                path="/media/icons/duotune/art/art005.svg"
                                className="svg-icon-3"
                              />
                              {/* eslint-enable */}
                            </button>
                          </Can>
                          <Can
                            permissons={getUsersPermissions}
                            componentPermissions={DELETE_PERMISSION}
                          >
                            <button
                              className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm'
                              onClick={() => {
                                onDeleteItem(item._id)
                              }}
                            >
                              {/* eslint-disable */}
                              <KTSVG
                                path="/media/icons/duotune/general/gen027.svg"
                                className="svg-icon-3"
                              />
                              {/* eslint-enable */}
                            </button>
                          </Can>
                        </td>
                      </Can>
                      <td className='ellipsis'>
                        {item && item.component ? item.component : '--'}
                        {
                          item && item.submenu.length > 0 ? (
                            <button
                              className='btn btn-icon btn-icon-primary btn-sm collapsed px-5'
                              data-toggle='collapse'
                              data-parent={`#${_.camelCase(item.component)}`}
                              href={`#${_.camelCase(item.component)}`}
                              aria-expanded='false'
                              aria-controls={_.camelCase(item.component)}
                            >
                              {/* eslint-disable */}
                              <KTSVG
                                path="/media/icons/duotune/arrows/arr087.svg"
                                className="svg-icon-3"
                              />
                              {/* eslint-enable */}
                            </button>
                          ) :
                            null
                        }
                      </td>
                      <div
                        id={_.camelCase(item.component)}
                        className='collapse '
                        role='tabpanel'
                        aria-labelledby='heading2'
                        data-parent={`#${_.camelCase(item.component)}`}
                      >
                        {tableA(item.submenu)}
                      </div>
                    </tr>
                  )
                })
              ) : (
                <tr className='text-center py-3'>
                  <td colSpan='100%'>No record(s) found</td>
                </tr>
              )
            ) : (
              <tr>
                <td colSpan='100%' className='text-center'>
                  <div
                    className='spinner-border text-primary m-5'
                    role='status'
                  />
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    )
  }

  const tableA = (subMenuComponents) => {
    return (
      <div className='table-responsive'>
        <table className='table table-hover table-rounded table-striped border gs-2'>
          <thead className='fw-bolder fs-6 text-gray-800'>
            <tr className='fw-bolder' style={{ backgroundColor: 'rgb(189 219 200)' }}>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={UPDATE_DELETE_PERMISSION}
              >
                <th className='text-center my-auto'>
                  <span>Action</span>
                </th>
              </Can>
              <th>
                <span>Sub Components</span>
              </th>
            </tr>
          </thead>
          <tbody className='fs-8'>
            {!loadingGMC ? (
              subMenuComponents && subMenuComponents.length > 0 ? (
                subMenuComponents.map((item, i) => {
                  return (
                    <tr
                      key={i}
                      style={
                        i === 0
                          ? { borderColor: 'white' }
                          : { borderColor: 'white' }
                      }
                    >
                      <Can
                        permissons={getUsersPermissions}
                        componentPermissions={UPDATE_DELETE_PERMISSION}
                      >
                        <td className='pb-0 pt-3 text-center my-auto'>
                          <Can
                            permissons={getUsersPermissions}
                            componentPermissions={UPDATE_PERMISSION}
                          >
                            <button
                              className='btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm'
                              onClick={() => {
                                history.push(
                                  `/update-user-components/${item._id}`
                                )
                              }}
                            >
                              {/* eslint-disable */}
                              <KTSVG
                                path="/media/icons/duotune/art/art005.svg"
                                className="svg-icon-3"
                              />
                              {/* eslint-enable */}
                            </button>
                          </Can>
                          <Can
                            permissons={getUsersPermissions}
                            componentPermissions={DELETE_PERMISSION}
                          >
                            <button
                              className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm'
                              onClick={() => {
                                onDeleteItem(item._id)
                              }}
                            >
                              {/* eslint-disable */}
                              <KTSVG
                                path="/media/icons/duotune/general/gen027.svg"
                                className="svg-icon-3"
                              />
                              {/* eslint-enable */}
                            </button>
                          </Can>
                        </td>
                      </Can>
                      <td className='ellipsis'>
                        {item && item.component ? item.component : '--'}
                      </td>
                    </tr>
                  )
                })
              ) : (
                <tr className='text-center py-3'>
                  <td colSpan='100%'>No record(s) found</td>
                </tr>
              )
            ) : (
              <tr>
                <td colSpan='100%' className='text-center'>
                  <div
                    className='spinner-border text-primary m-5'
                    role='status'
                  />
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    )
  }

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6' />
            <div className='d-flex col-md-6 justify-content-end my-auto'>
              <div className='my-auto me-9 mt-4'>
                {!editMode ? (
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={CREATE_PERMISSION}
                  >
                    <div className='my-auto'>
                      <Link
                        to='/add-user-components'
                        className='btn btn-sm btn-light-primary fa-pull-right'
                      >
                        {/* eslint-disable */}
                        <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                        {/* eslint-disable */}
                        Add Component
                      </Link>
                    </div>
                  </Can>
                ) : null}
              </div>
            </div>
          </div>
          <div className="table-responsive px-10">
            <table className="table table-hover table-rounded table-striped gs-2 mt-6">
              <thead className="fw-bolder fs-6 text-gray-800" style={{ backgroundColor: 'rgb(189 219 200)' }}>
                <tr className='fw-bolder'>
                  <th className='px-5 min-w-250px'>
                    <span>Components</span>
                  </th>
                </tr>
              </thead>
              <tbody className="fs-8">
                {!loadingGMC ? (
                  getMenuComp && getMenuComp.length > 0 ? (
                    getMenuComp.map((item, i) => {
                      return (
                        <tr
                          key={i}
                          style={
                            i === 0
                              ? { borderColor: "white" }
                              : { borderColor: "white" }
                          }
                        >
                          <td className='ellipsis cursor-pointer min-w-350px'>
                            <Can
                              permissons={getUsersPermissions}
                              componentPermissions={UPDATE_DELETE_PERMISSION}
                            >
                              <Can
                                permissons={getUsersPermissions}
                                componentPermissions={UPDATE_PERMISSION}
                              >
                                <button
                                  className='btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm me-2'
                                  onClick={() => {
                                    history.push(
                                      `/update-user-components/${item._id}`
                                    )
                                  }}
                                >
                                  {/* eslint-disable */}
                                  <KTSVG
                                    path="/media/icons/duotune/art/art005.svg"
                                    className="svg-icon-3"
                                  />
                                  {/* eslint-enable */}
                                </button>
                              </Can>
                              <Can
                                permissons={getUsersPermissions}
                                componentPermissions={DELETE_PERMISSION}
                              >
                                <button
                                  className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm'
                                  onClick={() => {
                                    onDeleteItem(item._id)
                                  }}
                                >
                                  {/* eslint-disable */}
                                  <KTSVG
                                    path="/media/icons/duotune/general/gen027.svg"
                                    className="svg-icon-3"
                                  />
                                  {/* eslint-enable */}
                                </button>
                              </Can>
                            </Can>
                            {item && item.component ? item.component : '--'}
                            {
                              item && item.submenu.length > 0 ? (
                                <button
                                  className='btn btn-icon btn-icon-primary btn-sm collapsed'
                                  data-toggle='collapse'
                                  data-parent={`#${_.camelCase(item.component)}`}
                                  href={`#${_.camelCase(item.component)}`}
                                  aria-expanded='false'
                                  aria-controls={_.camelCase(item.component)}
                                >
                                  {/* eslint-disable */}
                                  <KTSVG
                                    path="/media/icons/duotune/arrows/arr087.svg"
                                    className="svg-icon-3"
                                  />
                                  {/* eslint-enable */}
                                </button>
                              ) : null
                            }
                            <div
                              id={_.camelCase(item.component)}
                              className='collapse '
                              role='tabpanel'
                              aria-labelledby='heading2'
                              data-parent={`#${_.camelCase(item.component)}`}
                            >
                              {table(item.submenu)}
                            </div>
                          </td>
                        </tr>
                      )
                    })
                  ) : (
                    <tr className='text-center py-3'>
                      <td colSpan='100%'>No record(s) found</td>
                    </tr>
                  )
                ) : (
                  <tr>
                    <td colSpan='100%' className='text-center'>
                      <div
                        className='spinner-border text-primary m-5'
                        role='status'
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { getMenuCompStore, deleteComponentsStore } = state
  return {
    loadingGMC:
      getMenuCompStore && getMenuCompStore.loadingGMC
        ? getMenuCompStore.loadingGMC
        : false,
    getMenuComp:
      getMenuCompStore && getMenuCompStore.getMenuComp
        ? getMenuCompStore.getMenuComp
        : {},
    count:
      getMenuCompStore && getMenuCompStore.count
        ? getMenuCompStore.count
        : 0,
    statusDeleteComp:
      deleteComponentsStore && deleteComponentsStore.statusDeleteComp
        ? deleteComponentsStore.statusDeleteComp
        : '',
    messageDeleteComp:
      deleteComponentsStore && deleteComponentsStore.messageDeleteComp
        ? deleteComponentsStore.messageDeleteComp
        : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getMenuComponents: (params) => dispatch(getMenuCompActions.get(params)),
  deleteComponentsDispatch: (params) =>
    dispatch(deleteComponentsActions.delete(params)),
  clearComponentDispatch: () => dispatch(deleteComponentsActions.clear())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserComponents)
