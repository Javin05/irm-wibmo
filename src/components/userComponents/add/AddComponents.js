import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, useLocation, useHistory } from "react-router-dom";
import { STATUS_RESPONSE, iconOptions } from "../../../utils/constants";
import ReactSelect from "../../../theme/layout/components/ReactSelect";
import { getMenuCompActions, addComponentsActions, getComponentsDetailsActions, updateComponentsActions } from "../../../store/actions";
import { warningAlert, confirmAlert } from "../../../utils/alerts";
import color from "../../../utils/colors";
import _ from "lodash";
import { setCompData } from './formData'
import { componentValidation } from '../validate/validation'

const AddComponents = (props) => {
  const {
    messageAddComp,
    statusAddComp,
    loadingAddComp,
    getMenuCompActionsDispatch,
    getMenuComp,
    loadingGMC,
    addCompDispatch,
    clearaddCompDispatch,
    getCompDetailsDispatch,
    clearCompDetailsDispatch,
    statusGetDetailsComp,
    getDetailsComponents,
    clearEditCompDispatch,
    statusUpdateComp,
    messageUpdateComp,
    editCompDispatch,
    loadingUpdateComp,
    loadingGetDetailsComp
  } = props;
  const history = useHistory()
  const searchName = useLocation().pathname;
  const userParam = searchName && searchName.split("update-user-components/");
  const id = userParam && userParam[1];
  const [errors, setErrors] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [,setComponentHierarchy] = useState();
  const [selectedCompIconOption, setSelectedCompIconOption] = useState()
  const [selectedComponentOption, setSelectedComponentOption] = useState("");
  const [componentOption, setComponentOption] = useState();
  const [selectedIconOption, setSelectedIconOption] = useState()
  const [formData, setFormData] = useState({
    component: "",
    parentId: "",
    parenticon: "",
    favicon: "",
    permissions: [],
    tag:"IRM"
  });

  const dot = (dotColor = 'transparent') => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
      backgroundColor: dotColor,
      borderRadius: 10,
      content: '" "',
      display: 'block',
      marginRight: 8,
      height: 10,
      width: 10,
    },
  });

  const colourStyles = {
    control: (styles) => ({ ...styles, backgroundColor: 'white' }),
    option: (styles, { data }) => {
      return {
        ...styles,
        marginLeft: data.marginLeft,
        color: data.color ? data.color : color.black,
      };
    },
    input: (styles) => ({ ...styles, ...dot() }),
    singleValue: (styles, { data }) => ({ ...styles, ...dot(data.dotColor) }),
  };

  const getDefaultOptions = (data, name) => {
    const defaultOptions = [];
    const componentIds = [];
    if (!_.isEmpty(data)) {
      data.map((item) => {
        if (item._id && !componentIds.includes(item._id)) {
          componentIds.push(item._id)
          defaultOptions.push({
            label: `${item[name] ? item[name] : ""}`,
            value: item._id,
            marginLeft: 2,
            color: color.black,
            dotColor: color.purple,
            hierarchy: 'parent'
          })
          if (item.submenu && item.submenu.length > 0) {
            item.submenu.map(subMenu => {
              if (!componentIds.includes(subMenu._id)) {
                componentIds.push(subMenu._id)
                defaultOptions.push({
                  label: `${subMenu[name] ? subMenu[name] : ""}`,
                  value: subMenu._id,
                  marginLeft: 15,
                  color: color.outerSpace,
                  dotColor: color.iris,
                  hierarchy: 'child'
                })
              }
            }
            )
          }
        }
      }
      );
      return defaultOptions;
    }
  };

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target;
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, " ");
      setState((values) => ({ ...values, [name]: getData.trim() }));
    } else {
      setState((values) => ({ ...values, [name]: "" }));
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    const { value, name } = e.target;
    setFormData((values) => ({ ...values, [name]: value }));
    setErrors({ ...errors, [name]: "" });
  };

  useEffect(() => {
    const data = getDefaultOptions(getMenuComp, "component");
    setComponentOption(data);
  }, [getMenuComp]);

  const handleChangeRole = (selectedOption) => {
    if (selectedOption !== null) {
      setComponentHierarchy(selectedOption.hierarchy)
      setSelectedComponentOption(selectedOption);
      setFormData((values) => ({
        ...values,
        parentId: selectedOption.value,
      }));
      setErrors((values) => ({ ...values, parentId: "" }));
    } else {
      setSelectedComponentOption();
      setFormData((values) => ({ ...values, parentId: "" }));
    }
  };

  const checkNameExist = (arr, name) => {
    let hasExist = false
    arr && arr.map((item) => {
      if (item === name) {
        hasExist = true
      }
    });
    return hasExist
  }

  const handleCheckbox = (e) => {
    const { name } = e.target;
    const nameHasExist = checkNameExist(formData.permissions, name)
    if (nameHasExist) {
      setFormData((values) => ({
        ...values,
        permissions: values.permissions.filter((o) => o !== name ? o : null)
      }));
    } else {
      setFormData((values) => ({
        ...values,
        permissions: values.permissions && values.permissions.length > 0 ?
          [...values.permissions, name] : [name]
      }));
    }
  };

  const handleSubmit = () => {
    const errorMsg = componentValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        editCompDispatch(id, formData);
      } else {
        addCompDispatch(formData);
      }
    }
  };

  useEffect(() => {
    const params ={
      tag:"IRM"
    }
    getMenuCompActionsDispatch(params);
  }, []);

  const onConfirm = () => {
    history.push("/user-components")
  };

  useEffect(() => {
    if (statusUpdateComp === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageUpdateComp,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearEditCompDispatch()
    } else if (statusUpdateComp === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageUpdateComp,
        'error',
        'Close',
        'Ok',
        () => {
          onConfirm()
        },
        () => { }
      )
      clearEditCompDispatch()
    }
  }, [statusUpdateComp])

  useEffect(() => {
    if (statusAddComp === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAddComp,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() },
      )
      clearaddCompDispatch()
    } else if (statusAddComp === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAddComp,
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearaddCompDispatch()
    }
  }, [statusAddComp])

  useEffect(() => {
    if (statusGetDetailsComp === STATUS_RESPONSE.SUCCESS_MSG) {
      const setData = setCompData(getDetailsComponents)
      setFormData(setData)
    }
    if (getDetailsComponents.parentId) {
      const data = getDefaultOptions(getMenuComp, 'component')
      const selOption = _.filter(data, function (x) {
        if (_.includes(getDetailsComponents.parentId, x.value)) {
          return x
        }
      })
      setSelectedComponentOption(selOption)
    }
    if (getDetailsComponents.parenticon) {
      const selOption = _.filter(iconOptions, function (x) {
        if (_.includes(getDetailsComponents.parenticon, x.value)) {
          return x
        }
      })
      setSelectedIconOption(selOption)
    }
    if (getDetailsComponents.favicon) {
      const selOption = _.filter(iconOptions, function (x) {
        if (_.includes(getDetailsComponents.favicon, x.value)) {
          return x
        }
      })
      setSelectedCompIconOption(selOption)
    }
  }, [statusGetDetailsComp])

  useEffect(() => {
    if (id) {
      setEditMode(true)
      getCompDetailsDispatch(id)
    } else {
      setEditMode(false)
    }
  }, [id])

  useEffect(() => {
    return () => {
      clearCompDetailsDispatch()
    };
  }, []);

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const handleChangeIcon = (selectedOption) => {
    if (selectedOption !== null) {
      setComponentHierarchy(selectedOption.hierarchy)
      setSelectedIconOption(selectedOption)
      setFormData((values) => ({ ...values, parenticon: selectedOption.value }))
    } else {
      setSelectedIconOption()
      setFormData((values) => ({ ...values, parenticon: '' }))
    }
    setErrors({ ...errors, parenticon: '' })
  }

  const handleComponentChangeIcon = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCompIconOption(selectedOption)
      setFormData((values) => ({ ...values, favicon: selectedOption.value }))
    } else {
      setSelectedCompIconOption()
      setFormData((values) => ({ ...values, favicon: '' }))
    }
    setErrors({ ...errors, favicon: '' })
  }

  return (
    <>
      <div className="card-header bg-skyBlue py-0">
        <div className="card-body ms-4">
          <div className="d-flex justify-content-end me-4">
            <Link
              to="/user-components"
              className="btn btn-darkRed fa-pull-right mt-4"
            >
              Back
            </Link>
          </div>
          {loadingGetDetailsComp ? (
            <div className="d-flex justify-content-center py-5">
              <div className="spinner-border text-primary m-5" role="status" />
            </div>
          ) : (
            <>
              {editMode ? (
                getDetailsComponents && getDetailsComponents.parentId &&
                (
                  <div className="form-group row mb-2">
                    <div className="col-lg-5 pr-3">
                      <h6 className="mb-3 required">Parent Menu</h6>
                      <ReactSelect
                        styles={colourStyles}
                        isMulti={false}
                        name="parentId"
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeRole}
                        options={componentOption}
                        value={selectedComponentOption}
                        isLoading={loadingGMC}
                      />
                    </div>
                    <div className="col-lg-2 pr-3">
                      <h6 className="mb-3 required">Parent Icon</h6>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='parenticon'
                        className='basic-single'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeIcon}
                        options={iconOptions}
                        value={selectedIconOption}
                      />
                    </div>
                  </div>
                )
              )
                :
                <div className="form-group row mb-2">
                  <div className="col-lg-5 pr-3">
                    <h6 className="mb-3">Parent Menu</h6>
                    <ReactSelect
                      styles={colourStyles}
                      isMulti={false}
                      name="parentId"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeRole}
                      options={componentOption}
                      value={selectedComponentOption}
                      isLoading={loadingGMC}
                    />
                  </div>
                </div>}
              <div className="form-group row my-4">
                <div className="col-lg-5 pr-3">
                  <h6 className="mb-3 required">Component</h6>
                  <input
                    name="component"
                    type="text"
                    className="form-control"
                    placeholder="Component"
                    onChange={(e) => handleChange(e)}
                    value={formData.component || ""}
                    maxLength={42}
                    autoComplete="off"
                    onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                  />
                  {errors && errors.component && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red}"}</style>
                      {errors.component}
                    </div>
                  )}
                </div>
                {/* {
                  componentHierarchy === "parent" ? ( */}
                <div className="col-lg-2 pr-3">
                  <h6 className="mb-3 required">Component Icon</h6>
                  <ReactSelect
                    styles={customStyles}
                    isMulti={false}
                    name='favicon'
                    className='basic-single'
                    classNamePrefix='select'
                    handleChangeReactSelect={handleComponentChangeIcon}
                    options={iconOptions}
                    value={selectedCompIconOption}
                  />
                </div>
                {/* ) : null
                } */}
              </div>
              <div className="form-group row my-4 mx-auto">
                <div className="col-lg-12 pr-3 mx-auto">
                  <h6 className="mb-3 required">Permissions</h6>
                  <div className="px-20 d-flex scroll py-3">
                    <label className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        name="view"
                        checked={formData.permissions && formData.permissions.includes("view") || false}
                        onChange={(e) => handleCheckbox(e)}
                      />
                      <span className="form-check-label text-gray-600">
                        View
                      </span>
                    </label>
                    <label className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        name="create"
                        checked={formData.permissions && formData.permissions.includes("create") || false}
                        onChange={(e) => handleCheckbox(e)}
                      />
                      <span className="form-check-label text-gray-600">
                        Create
                      </span>
                    </label>
                    <label className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        name="update"
                        checked={formData.permissions && formData.permissions.includes("update") || false}
                        onChange={(e) => handleCheckbox(e)}
                      />
                      <span className="form-check-label text-gray-600">
                        Update
                      </span>
                    </label>
                    <label className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-15">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        name="delete"
                        checked={formData.permissions && formData.permissions.includes("delete") || false}
                        onChange={(e) => handleCheckbox(e)}
                      />
                      <span className="form-check-label text-gray-600">
                        Delete
                      </span>
                    </label>
                    <label className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        name="import"
                        checked={formData.permissions && formData.permissions.includes("import") || false}
                        onChange={(e) => handleCheckbox(e)}
                      />
                      <span className="form-check-label text-gray-600">
                        Import
                      </span>
                    </label>
                    <label className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        name="export"
                        checked={formData.permissions && formData.permissions.includes("export") || false}
                        onChange={(e) => handleCheckbox(e)}
                      />
                      <span className="form-check-label text-gray-600">
                        Export
                      </span>
                    </label>
                  </div>
                  {errors && errors.permissions && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red}"}</style>
                      {errors.permissions}
                    </div>
                  )}
                </div>
              </div>
              <div className="d-flex justify-content-end">
                <button
                  className="btn btn-blue mt-7 fa-pull-right m-4"
                  onClick={(event) => {
                    handleSubmit();
                  }}
                >
                  {loadingAddComp || loadingUpdateComp ? (
                    <span
                      className="spinner-border spinner-border-sm mx-3"
                      role="status"
                      aria-hidden="true"
                    />
                  ) : (
                    "Submit"
                  )}
                </button>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  const { getMenuCompStore, addComponentsStore, getComponentsDetailsStore, updateComponentsStore } = state;
  return {
    getMenuComp:
      getMenuCompStore && getMenuCompStore.getMenuComp
        ? getMenuCompStore.getMenuComp
        : [],
    loadingGMC:
      getMenuCompStore && getMenuCompStore.loadingGMC
        ? getMenuCompStore.loadingGMC
        : false,
    messageAddComp:
      addComponentsStore && addComponentsStore.messageAddComp
        ? addComponentsStore.messageAddComp
        : 0,
    statusAddComp:
      addComponentsStore && addComponentsStore.statusAddComp
        ? addComponentsStore.statusAddComp
        : {},
    loadingAddComp:
      addComponentsStore && addComponentsStore.loadingAddComp
        ? addComponentsStore.loadingAddComp
        : false,
    getDetailsComponents:
      getComponentsDetailsStore && getComponentsDetailsStore.getDetailsComponents
        ? getComponentsDetailsStore.getDetailsComponents
        : {},
    statusGetDetailsComp:
      getComponentsDetailsStore && getComponentsDetailsStore.statusGetDetailsComp ? getComponentsDetailsStore.statusGetDetailsComp : '',
    messageGetDetailsComp:
      getComponentsDetailsStore && getComponentsDetailsStore.messageGetDetailsComp
        ? getComponentsDetailsStore.messageGetDetailsComp
        : '',
    loadingGetDetailsComp:
      getComponentsDetailsStore && getComponentsDetailsStore.loadingGetDetailsComp
        ? getComponentsDetailsStore.loadingGetDetailsComp
        : false,
    updateComponents: updateComponentsStore && updateComponentsStore.updateComponents ? updateComponentsStore.updateComponents : [],
    statusUpdateComp: updateComponentsStore && updateComponentsStore.statusUpdateComp ? updateComponentsStore.statusUpdateComp : '',
    messageUpdateComp: updateComponentsStore && updateComponentsStore.messageUpdateComp ? updateComponentsStore.messageUpdateComp : '',
    loadingUpdateComp: updateComponentsStore && updateComponentsStore.loadingUpdateComp ? updateComponentsStore.loadingUpdateComp : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getMenuCompActionsDispatch: (data) => dispatch(getMenuCompActions.get(data)),
  getCompDetailsDispatch: (data) =>
    dispatch(getComponentsDetailsActions.getDetails(data)),
  clearCompDetailsDispatch: () =>
    dispatch(getComponentsDetailsActions.clear()),
  addCompDispatch: (data) => dispatch(addComponentsActions.add(data)),
  clearaddCompDispatch: () => dispatch(addComponentsActions.clear()),
  editCompDispatch: (id, params) =>
    dispatch(updateComponentsActions.update(id, params)),
  clearEditCompDispatch: () =>
    dispatch(updateComponentsActions.clear())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddComponents);
