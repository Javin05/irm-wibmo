import _ from 'lodash'

export const setCompData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      component: data.component,
      parentId: data.parentId,
      permissions: data.permissions,
      favicon: data.favicon,
      parenticon: data.parenticon,
      tag:"IRM"
    }
  }
}
