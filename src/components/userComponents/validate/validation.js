import { USER_ERROR } from '../../../utils/constants'

export const componentValidation = (values, setErrors) => {
  const errors = {}
  if (!values.component) {
    errors.component = USER_ERROR.COMPONENT
  }
  if (!values.permissions) {
    errors.permissions = USER_ERROR.PERMISSION
  }
  setErrors(errors)
  return errors
}
