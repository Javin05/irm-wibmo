import React, {useState, useEffect} from 'react';
import Modal from 'react-bootstrap/Modal'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import { Formik, Field, Form, ErrorMessage } from 'formik'
import * as Yup from 'yup';

const WebAnalysisModal=(props)=> {
	const { modalType, modalStats, modalToggle, createWebAnalysis } = props

	const fileOnUpload = (e) =>{
		console.log(fileOnUpload)
	}
	return (
		<Modal
            show={modalStats}
            onHide={(e) => modalToggle(e, false)}
            size="lg"
            aria-labelledby="web-analysis-modal"
            className=""
            centered>
            <Modal.Header closeButton>
                <Modal.Title id="web-analysis-modal">
                   { modalType==="add"? "Add Web Analysis" : "Update Web Analysis"}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            	<Tabs defaultActiveKey="home" id="web-analysis-tab" className="mb-3">
				  	<Tab eventKey="home" title="Manual">
				    	<Formik 
		                    initialValues={
		                    	{ 
		                    		tag: '', 
		                    		website :'' 
		                    	}
		                    }
		                    validationSchema={
		                    	Yup.object({
				                	tag: Yup.string()
				                    	.max(15, 'Must be 15 characters or less')
				                    	.required('Tag is required'),
				                    website: Yup.mixed()
				                    	.required('Website is required'),
				            	})
				            }
		                    onSubmit={(values, 
		                    	{ setSubmitting }) => {
		                    		createWebAnalysis(values)
		                    		//console.log(values)
			            		}
			            	}>
		                    {({ errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm }) => {                    
		                        return (
		                            <Form
		                                id="webAnalysisForm" 
		                                className="webAnalysisForm" 
		                                noValidate>
		                                <div className="form-row">
		                                    <div className="form-group col-md-12 mb-5">
		                                    	<label>Tag</label>
		                                        <Field 
		                                        	name="tag" 
		                                        	type="text" 
		                                        	error={errors.tag}
		                                        	touched={touched.tag}
		                                        	className={'form-control' + (errors.tag && touched.tag ? ' is-invalid' : '')}/>   
		                                        <ErrorMessage name="tag" component="div" className="invalid-feedback" />                                     
		                                    </div>
		                                    <div className="form-group col-md-12 mb-5">
		                                    	<label>Website</label>
		                                        <Field 
		                                        	name="website" 
		                                        	type="text" 
		                                        	error={errors.website}
		                                        	touched={touched.website}
		                                        	className={'form-control' + (errors.website && touched.website ? ' is-invalid' : '')}/>   
		                                        <ErrorMessage name="website" component="div" className="invalid-feedback" />                                     
		                                    </div>
		                                </div>
		                                <div className="form-group">
		                                    <button 
		                                        type="submit" 
		                                        disabled={isSubmitting} 
		                                        className="btn btn-primary">
		                                        {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
		                                        {modalType==="add"? "Add Web Analysis" : "Update Web Analysis"}
		                                    </button> 
		                                </div>
		                            </Form>
		                        );
		                    }}
		                </Formik>
				  	</Tab>
				  	<Tab eventKey="profile" title="CSV Upload">
				    	<Formik 
		                    initialValues={
		                    	{ 
		                    		tag: '', 
		                    		upload :'' 
		                    	}
		                    }
		                    validationSchema={
		                    	Yup.object({
				                	tag: Yup.string()
				                    	.max(15, 'Must be 15 characters or less')
				                    	.required('Tag is required'),
				                    upload: Yup.mixed()
				                    	.required('Tag is required'),
				            	})
				            }
		                    onSubmit={(values, 
		                    	{ setSubmitting }) => {
		                    		console.log(values)
			            		}
			            	}>
		                    {({ errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm }) => {                    
		                        return (
		                            <Form
		                                id="webAnalysisForm" 
		                                className="webAnalysisForm" 
		                                noValidate>
		                                <div className="form-row">
		                                    <div className="form-group col-md-12 mb-5">
		                                    	<label>Tag</label>
		                                        <Field 
		                                        	name="tag" 
		                                        	type="text" 
		                                        	error={errors.tag}
		                                        	touched={touched.tag}
		                                        	className={'form-control' + (errors.tag && touched.tag ? ' is-invalid' : '')}/>   
		                                        <ErrorMessage name="tag" component="div" className="invalid-feedback" />                                     
		                                    </div>
		                                    <div className="form-group col-md-12 mb-5">
		                                    	<label>Tag</label>
		                                        <Field 
		                                        	name="tag" 
		                                        	type="text" 
		                                        	error={errors.tag}
		                                        	touched={touched.tag}
		                                        	className={'form-control' + (errors.tag && touched.tag ? ' is-invalid' : '')}/>   
		                                        <ErrorMessage name="tag" component="div" className="invalid-feedback" />                                     
		                                    </div>
		                                    <div className="form-group col-md-12 mb-5">
		                                    	<label>Upload File</label>
											    <input
											        className="attribute_input"
											        name="upload"
											        type="file"
											        onChange={(event) => {
													 	setFieldValue("upload", event.currentTarget.files[0]);
													}}
											        />									        
		                                    </div>
		                                </div>
		                                <div className="form-group">
		                                    <button 
		                                        type="submit" 
		                                        disabled={isSubmitting} 
		                                        className="btn btn-primary">
		                                        {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
		                                        {modalType==="add"? "Add Web Analysis" : "Update Web Analysis"}
		                                    </button> 
		                                </div>
		                            </Form>
		                        );
		                    }}
		                </Formik>
				  	</Tab>
				</Tabs>                
            </Modal.Body>
        </Modal>
	)
}
export default WebAnalysisModal;