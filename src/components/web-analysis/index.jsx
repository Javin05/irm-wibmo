import React, { useState, useEffect } from 'react'
import WebAnalysisModal from './WebAnalysisModal'
import { connect } from 'react-redux'
import {
    WebAnalysisTypes,
    WebAnalysisActions,
} from '../../store/actions'

function WebAnalysis (props) {
    const {
        createWebAnalysisDispatch,
        fetchWebAnalysisDispatch, 
        createWebAnalysisResponse,
    } = props

    const [modalType, setModalType] = useState("add")
    const [modalStats, setModalStats] = useState(false)
    const [params, setParams] = useState({
        limit:15,
        page:1
    })

    const modalToggle = (e, stats) =>{
        setModalStats(stats)
    }
    const fetchWebAnalysis = (params) =>{
        fetchWebAnalysisDispatch(params)
    }
    const createWebAnalysis = (formData) =>{
        createWebAnalysisDispatch(formData)
    }
    const updateWebAnalysis = (id, formData) =>{
        console.log(formData)
    }
    const deleteWebAnalysis = (id) =>{
        
    }


    useEffect(() => {
        fetchWebAnalysis()
    }, [])

    return (
        <>
            <div className={`card card-custom card-stretch gutter-b`}>
                <div className="card-header border-0 pt-5">
                    <h3 className="card-title align-items-start flex-column">
                        <span className="card-label font-weight-bolder text-dark">
                            Web Analysis
                        </span>
                    </h3>
                    <div className="card-toolbar">
                        <button
                                className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
                                onClick={(e)=>modalToggle(e, true)}>
                                <span className="svg-icon svg-icon-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                                        <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                Add Analysis
                            </button>
                    </div>
                </div>
                <div className="card-body pt-2 pb-0 pl-5 pr-5">
                </div>
            </div>
            <WebAnalysisModal
                modalType={modalType} 
                modalStats={modalStats}
                modalToggle={modalToggle}
                createWebAnalysis={createWebAnalysis}
                />
        </>
    )
}

const mapStateToProps = state => {
    const {
        WebAnalysisStore
    } = state

    return {
        loading: state && state.WebAnalysisStore && state.WebAnalysisStore.loading,
        WebAnalysisData: WebAnalysisStore && WebAnalysisStore.WebAnalysisData ? WebAnalysisStore.WebAnalysisData : null,
        createWebAnalysisResponse: WebAnalysisStore && WebAnalysisStore.WebAnalysisStore ? WebAnalysisStore.WebAnalysisStore : null,
    }
}
const mapDispatchToProps = dispatch => ({
    fetchWebAnalysisDispatch: (params) => dispatch(WebAnalysisActions.fetchWebAnalysisInit(params)),
    createWebAnalysisDispatch: (formData) => dispatch(WebAnalysisActions.createWebAnalysisInit(formData)),
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WebAnalysis);
