import React, { useEffect, useState } from "react"
import { Link, useLocation } from "react-router-dom"
import _, { values } from "lodash"
import { KTSVG } from "../../theme/helpers"
import { KYCActions, clientCredFilterActions, kycStatusAction } from "../../store/actions"
import { connect } from "react-redux"
import SearchList from "./searchList"
import ReactPaginate from "react-paginate"
import { unsetLocalStorage, getLocalStorage } from "../../utils/helper"
import {
  KYC_STATUS,
  KYC_TYPE_STATUS,
  SET_FILTER,
  KYC_SCORE_STATUS,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from "../../utils/constants"
import KYCminiAdd from "./mini-KYC/Add"
import EditKYC from "./editKyc/edit"
import moment from "moment"
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'

function KYCList(props) {
  const {
    getKYClistDispatch,
    getKYCUserDetailsDispatch,
    className,
    KYClists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    kycStatusDispatch,
    kycStatusRes,
    kycStatusloading
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("/")
  const currentRoute = url && url[1]
  const getUsersPermissions = getUserPermissions(pathName, true)
  const [routeShow, setRouteShow] = useState()
  const [show, setShow] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [activeKyc, setActiveKyc] = useState({})
  const [userId, setuserId] = useState('')
  const statusData = kycStatusRes && kycStatusRes.data
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false,
    clientId: false
  })

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params);
    if (currentRoute === 'risk-management-search') {
      setRouteShow(false)
    } else {
      getKYClistDispatch(pickByParams)
      kycStatusDispatch()
    }
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1);
      const params = {
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getKYClistDispatch(pickByParams)
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: 1,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    getKYClistDispatch(params)
    setActivePageNumber(1);
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      ...searchParams,
      limit: limit,
      page: pageNumber,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : ""
    }
    setActivePageNumber(pageNumber)
    getKYClistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "DESC",
      }
      getKYClistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "ASC",
      }
      getKYClistDispatch(params)
    }
  }

  const totalPages =
    KYClists && KYClists.count
      ? Math.ceil(parseInt(KYClists && KYClists.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = "/merchant-login"
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const ActiveFullKyc = (name, id, businessType) => {
    // setActiveKyc(name,id)
    const params = {
      kycName: name,
      kycId: id,
      organizationName: businessType
    }
    setActiveKyc((values) => ({ ...values, kycData: params }))
  }

  return (
    <>
      <EditKYC editMode={editMode} userId={userId} setEditMode={setEditMode} />
      <KYCminiAdd setShow={setShow} show={show} editMode={editMode} activeKyc={activeKyc} setActiveKyc={setActiveKyc} />
      <div className={`card ${className}`}>
        <div className="card-body py-3  mt-8 mb-8">
          <div className="d-flex  px - 2">
            <div className="d-flex justify-content-start col-md-6">
              <div className="col-md-3 mt-1 ms-2">
                {KYClists &&
                  KYClists?.count && (
                    <span className="text-muted fw-bold d-flex fs-3 mt-2">
                      Total: {" "}
                      <span className="text-gray-700 fw-bolder text-hover-primary fs-3 ms-1">
                        {KYClists && KYClists.count}
                      </span>
                    </span>
                  )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="d-flex col-md-6 justify-content-end my-auto mt-4">
              <div className="my-auto">
                <SearchList setSearchParams={setSearchParams} />
              </div>
              {/* <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              > */}
              <div className="my-auto me-3">
                <button
                  type="button"
                  className="btn btn-sm btn-light-primary font-5vw pull-right"
                  data-toggle="modal"
                  data-target="#addModal"
                  onClick={() => {
                    setShow(true)
                    setEditMode(false)
                  }}
                >
                  <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                  Add Uae
                </button>
              </div>
              {/* </Can> */}
            </div>
          </div>

          {/* {
            !kycStatusloading ? (
              <div className='row'>
                <div className='col-md-4 col-lg-4 col-sm-4 ms-4'>
                  <div className='row'>
                    <div className='text-black-700 fw-bolder'>
                      STATUS
                    </div>
                    <div className='col-md-12 mt-4'>
                      <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                        PENDING
                        <span className=' text-hover-primary'>
                          - {statusData && statusData.pendingCount ? statusData.pendingCount : '0'}
                        </span>
                      </span>
                      <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                        APPROVED
                        <span className=' text-hover-primary'>
                          - {statusData && statusData.approvedCount ? statusData.approvedCount : '0'}
                        </span>
                      </span>
                      <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                        REJECTED
                        <span className='text-hover-primary'>
                          - {statusData && statusData.rejectCount ? statusData.rejectCount : '0'}
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className='col-md-12 d-flex justify-content-center'>
                <div
                  className='spinner-border text-success'
                  role='status'
                />
              </div>
            )
          } */}
        </div>
      </div>
      {!loading ? (
        !_.isEmpty(KYClists.data) ? (
          KYClists &&
          KYClists.data.map((item, i) => {
            return (
              <>
                <div className="card mt-6" key={i}>
                  <div className="card-body">
                    <div className="row mt-4">
                      <div className="col-lg-8 col-md-8 col-sm-8">
                        <div className='row'>
                          <div className="col-lg-3 col-md-3 col-sm-3 ms-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Business Name
                            </div>
                            <div className="fs-8 fw-bolder">
                              {item.businessName
                                ? item.businessName
                                : "--"}
                            </div>
                          </div>
                          <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Phone
                            </div>
                            <div className="fs-8 fw-bolder">
                              {item && item.phoneNumber && item.phoneNumber.number
                                ? item.phoneNumber.number
                                : "--"}
                            </div>
                          </div>
                          <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Email
                            </div>
                            <div className="fs-8 fw-bolder">
                              {item && item.emailId && item.emailId.emailId
                                ? item.emailId.emailId
                                : "--"}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4 col-sm-4">
                        <div className="row">
                          <div className="col-lg-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Case#
                            </div>
                            <div className="fs-8 fw-bolder ellipsis">
                              <Link to={`/static-summary/update/${item._id}`}>
                                KYC{item.kycId ? item.kycId : "--"}
                              </Link>
                            </div>
                          </div>
                          {/* <div className="col-lg-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Client
                            </div>
                            <div className="fs-8 fw-bolder">
                              {item && item.clientId && item.clientId.company ? item.clientId.company : '--'}
                            </div>
                          </div> */}
                          <div className="col-lg-3">
                            <span
                              className={`ms-2 badge ${KYC_TYPE_STATUS[item && item.businessType]}`}
                            >
                              {item.businessType}
                            </span>
                          </div>
                          <div className="col-lg-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Created
                            </div>
                            <div className="fs-8 fw-bolder">
                              {moment(
                                item.createdAt ? item.createdAt : "--"
                              ).format("DD/MM/YYYY")}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="separator separator-dashed border-secondary" />
                    <div className="row">
                      <div className="col-sm-8 col-md-8 col-lg-8 mt-4 mb-4">
                        <span className="ms-4 fs-5 fw-bolder">
                          eKYC
                        </span>
                        <span className="ms-4 fs-7">
                          <i className={`ms-2 me-1 ${KYC_SCORE_STATUS[item && item.phoneNumber && item.phoneNumber.verified]}`}
                            style={{
                              backgroundColor: "transparent",
                              fontSize: "0.95rem"
                            }}
                          />
                          Phone
                        </span>
                        <span className="ms-4 fs-7">
                          <i className={`ms-2 me-1 ${KYC_SCORE_STATUS[item && item.emailId && item.emailId.verified]}`}
                            style={{
                              backgroundColor: "transparent",
                              fontSize: "0.95rem"
                            }}
                          />
                          Email
                        </span>
                        <span className="ms-4 fs-7">
                          {
                            item && item.documentVerification === true ? (
                              <i className="bi bi-check-circle-fill ms-2 me-1"
                                style={{
                                  color: "rgb(122 231 174)",
                                  backgroundColor: "transparent",
                                  fontSize: "0.95rem",
                                }}
                              />
                            ) : (
                              <i className="bi bi-x-circle-fill ms-2 me-1"
                                style={{
                                  color: "red",
                                  backgroundColor: "transparent",
                                  fontSize: "0.95rem",
                                }}
                              />
                            )
                          }
                          Document
                        </span>
                        <span className="ms-4 fs-7">
                          {
                            item && item.businessAddress && item.businessAddress.addressVerification === true ? (
                              <i className="bi bi-check-circle-fill ms-2 me-1"
                                style={{
                                  color: "rgb(122 231 174)",
                                  backgroundColor: "transparent",
                                  fontSize: "0.95rem",
                                }}
                              />
                            ) : (
                              <i className="bi bi-x-circle-fill ms-2 me-1"
                                style={{
                                  color: "red",
                                  backgroundColor: "transparent",
                                  fontSize: "0.95rem",
                                }}
                              />
                            )
                          }
                          Business Address
                        </span>
                        <span className="ms-4 fs-7">
                          {
                            item && item.bankVerificationStatus === "APPROVED" ? (
                              <i className="bi bi-check-circle-fill ms-2 me-1"
                                style={{
                                  color: "rgb(122 231 174)",
                                  backgroundColor: "transparent",
                                  fontSize: "0.95rem",
                                }}
                              />
                            ) : (
                              <i className="bi bi-x-circle-fill ms-2 me-1"
                                style={{
                                  color: "red",
                                  backgroundColor: "transparent",
                                  fontSize: "0.95rem",
                                }}
                              />
                            )
                          }
                          Bank Details
                        </span>
                      </div>
                      <div className="col-lg-2 col-md-2 col-sm-2">
                        <Can
                          permissons={getUsersPermissions}
                          componentPermissions={UPDATE_PERMISSION}
                        >
                          <button
                            className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px mt-4'
                            title="Edit Data"
                            onClick={() => {
                              setEditMode(true)
                              setuserId(item && item._id)
                            }}
                          >
                            <KTSVG
                              path='/media/icons/duotune/art/art005.svg'
                              className='svg-icon-3'
                            />
                          </button>
                        </Can>
                      </div>
                      <div className="col-lg-2">
                        <div
                          className={`card card-custom card-stretch gutter-b p-4 w-100 ${KYC_STATUS[item && item.kycStatus]
                            }`}
                          style={{ borderRadius: "0px" }}
                        >
                          <div className="card-body d-flex align-items-end pt-0">
                            <div className="d-flex align-items-center flex-column w-100">
                              <span className="fs-6 fw-bolder">
                                {item && item.kycStatus}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )
          })
        ) : (
          <div className="text-center py-3">
            <div colSpan="100%">No record(s) found</div>
          </div>
        )
      ) : (
        <div>
          <div colSpan="100%" className="text-center mt-10">
            <div className=" spinner-border text-primary m-5" role="status" />
          </div>
        </div>
      )}
      <div className="form-group row mb-4 mt-6">
        <div className="col-lg-12 mb-4 align-items-end d-flex">
          <div className="col-lg-12">
            <ReactPaginate
              nextLabel="Next >"
              onPageChange={handlePageClick}
              pageRangeDisplayed={3}
              marginPagesDisplayed={2}
              pageCount={totalPages}
              forcePage={activePageNumber - 1}
              previousLabel="< Prev"
              pageClassName="page-item"
              pageLinkClassName="page-link"
              previousClassName="page-item"
              previousLinkClassName="page-link"
              nextClassName="page-item"
              nextLinkClassName="page-link"
              breakLabel="..."
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName="pagination"
              activeClassName="active"
              renderOnZeroPageCount={null}
            />
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { KYClistStore, kycStatusStore } = state
  return {
    KYClists:
      KYClistStore && KYClistStore.KYClists ? KYClistStore.KYClists : "",
    KYCUserDetails:
      KYClistStore && KYClistStore.KYCuser ? KYClistStore.KYCuser : "",
    loading:
      KYClistStore && KYClistStore.loading ? KYClistStore.loading : false,
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    kycStatusRes:
      kycStatusStore && kycStatusStore.kycStatusRes ? kycStatusStore.kycStatusRes : "",
    kycStatusloading:
      kycStatusStore && kycStatusStore.loading ? kycStatusStore.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  getKYCUserDetailsDispatch: (params) => dispatch(KYCActions.getKYCUserDetails(params)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
  kycStatusDispatch: (data) => dispatch(kycStatusAction.kycStatus(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(KYCList)
