
import React, { useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businessValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import _ from 'lodash'
import {
  EntityAction,
  AccountUploadAction
} from "../../../store/actions"
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import { KYC_FORM, REGEX, STATUS_RESPONSE, DROPZONE_MESSAGES } from '../../../utils/constants'
import {
  RESTRICTED_FILE_FORMAT_TYPE
} from "../../../constants/index"

const getEmiratess = [
  {
    name: 'demo',
    _id: '1'
  }, {
    name: 'demo2',
    _id: '2'
  }
]

function BusinessForm(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    loading,
    goBack,
    setCompletedSteps,
    setActiveStep,
    getEntityDispatch,
    getEntityRes,
    AccountUploadRes,
    AccountUploadDocDispatch,
    AccountUploadResLoading,
    clearAccountUploadDocDispatch
  } = props

  const cancelledFilesInput = useRef(null)
  const merchantFIleInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [websiteCheked, setWebsiteCheked] = useState()
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [Data, setData] = useState(true)
  const [tabDefault, setTabdefault] = useState('')
  const [EmiratesOption, setEmiratesOption] = useState()
  const [selectedEmiratesOption, setSelectedEmiratesOption] = useState('')
  const [businessUpload, setbusinessUpload] = useState('Upload')
  const [ShowBusiness, setShowBusiness] = useState(false)
  const [merchantUpload, setmerchantUpload] = useState('Upload')
  const [Showmerchant, setShowmerchant] = useState(false)
  const [name, setName] = useState(false)
  const [formData, setFormData] = useState({
    businessCategory: '',
    website: '',
    entity_id: '',
    businessIpAddress: '',
    businessName: '',
    primaryContactName: '',
    entiryName: '',
    businessDescription: '',
    addressOfEstablishment: '',
    emirates: '',
    business: '',
    legalName: '',
    contactNumber: '',
    merchant:''
  })

  useEffect(() => {
    getEntityDispatch()
  }, [])

  const handleChange = (e) => {
    e.persist()
    const { value, name, checked } = e.target
    setWebsiteCheked(checked)
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.businessName)) {
      errors.businessName = 'Please Enter Business Name.'
    } if (_.isEmpty(formData.primaryContactName)) {
      errors.primaryContactName = 'Please Enter Primary Contact Name.'
    }
    if (_.isEmpty(formData.entity_id)) {
      errors.entity_id = 'Please Select entity Or Not Registered.'
    }
    if (_.isEmpty(formData.businessCategory)) {
      errors.businessCategory = 'Please Select One Payment.'
    }
    if (_.isEmpty(formData.website)) {
      errors.website = 'Website is required.'
    } else if (formData.website && !REGEX.WEBSITE_URLS.test(formData.website)) {
      errors.website = 'Website is InValid'
    }
    if (_.isEmpty(errors)) {
      onClickNext(3)
      setData(formData)
      setLocalStorage(KYC_FORM.BUSINESS, JSON.stringify(formData))
      setClientDetails((values) => ({ ...values, business: formData }))
    }
    setErrors(errors)
  }

  const ToggleTab = (tab, name) => {
    setFormData((values) => ({ ...values, entiryName: name }))
    setFormData((values) => ({ ...values, entity_id: tab }))
    setTabdefault(tab)
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const emirates = getDefaultOptions(getEmiratess)
    setEmiratesOption(emirates)
    if (!_.isEmpty(formData.emirates)) {
      const selOption = _.filter(emirates, function (x) { if (_.includes(formData.emirates._id, x.value)) { return x } })
      setSelectedEmiratesOption(selOption)
    }
  }, [getEmiratess])

  const handleChangeEmirates = selectedOption => {
    if (selectedOption !== null) {
      setSelectedEmiratesOption(selectedOption)
      setFormData(values => ({ ...values, emirates: selectedOption.label}))
    } else {
      setFormData(values => ({ ...values, emirates: ''}))
    }
    setErrors({ ...errors, emirates: '' })
  }


  const FileChangeHandler = (e) => {
    const { name } = e.target;
    setName(name)
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', 'uae')
        data.append('file_to_upload', files)
        AccountUploadDocDispatch(data)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
        setShowBusiness(false)
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID })
      setShowBusiness(false)
    }
  }

  useEffect(() => {
    if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = AccountUploadRes && AccountUploadRes.data && AccountUploadRes.data.path
      if(name === 'business'){
        setFormData((values) => ({ ...values, business: data }))
        setbusinessUpload('Uploaded')
        setShowBusiness(true)
      }else if(name === 'merchant'){
        setFormData((values) => ({ ...values, merchant: data }))
        setmerchantUpload('Uploaded')
        setShowmerchant(true)
      }
      clearAccountUploadDocDispatch()
    } else if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, business: '' }))
      clearAccountUploadDocDispatch()
      setbusinessUpload('Upload')
      setShowBusiness(false)
    }
  }, [AccountUploadRes])

  const cancelledFilePanClick = (event) => {
    cancelledFilesInput.current.click(event);
  }

  const merchantFIleClick = (event) => {
    merchantFIleInput.current.click(event);
  }

  return (
    <>
      <div>
        <h1 className='fs-2 mb-8'>Business Information</h1>
        <div className='col-lg-6 mb-8'>
          <label className='d-flex align-items-center fw-bold mb-2'>
            <span className='required fw-bold fs-4'>Emirates</span>
          </label>
          <ReactSelect
            isClearable
            styles={customStyles}
            isMulti={false}
            name='Emirates'
            placeholder="Select..."
            className="basic-single"
            classNamePrefix="select"
            handleChangeReactSelect={handleChangeEmirates}
            options={EmiratesOption}
            value={selectedEmiratesOption}
            isDisabled={!EmiratesOption}
          />
          {errors && errors.emirates && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red}'}</style>
              {errors.emirates}
            </div>
          )}
        </div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Address of Establishment</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='addressOfEstablishment'
            placeholder='Address of Establishment'
            onChange={(e) => handleChange(e)}
            value={formData.addressOfEstablishment || ''}
            maxLength={50}
          />
          {errors && errors.addressOfEstablishment && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.addressOfEstablishment}
            </div>
          )}
        </div>
        <div className='mb-8'>
          <div className='row'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center'>
                <span className='text-dark fs-4 fw-bolder'> Upload Document </span>
              </label>
            </div>
          </div>
          <input
            type="file"
            className="d-none"
            name="business"
            id="business"
            multiple={false}
            ref={cancelledFilesInput}
            onChange={FileChangeHandler} />
          <button type="button"
            className={`${!ShowBusiness ? 'btn btn-light-primary btn-sm mt-4' : 'btn btn-sm btn-success mt-4'}`}
            onClick={cancelledFilePanClick}>
            {AccountUploadResLoading
              ? (
                'Uploading...'
              )
              : (
                <>
                  {businessUpload}
                </>
              )}
          </button>
          {errors && errors.business && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.business}
            </div>
          )}
        </div>

        <h1 className='fs-2 mb-8'>Merchant Information</h1>
        <div className='mb-6'>
          <div className='row mb-2'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Legal Name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='legalName'
            placeholder='Legal Name'
            onChange={(e) => handleChange(e)}
            value={formData.legalName || ''}
            maxLength={50}
          />
          {errors && errors.legalName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.legalName}
            </div>
          )}
        </div>
        <div className='mb-6'>
          <div className='row mb-2'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Contact Number</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='contactNumber'
            placeholder='Contact Number'
            onChange={(e) => handleChange(e)}
            value={formData.contactNumber || ''}
            onKeyPress={(e) => {
              if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                e.preventDefault()
              }
            }}
          />
          {errors && errors.contactNumber && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.contactNumber}
            </div>
          )}
        </div>
        <div className='mb-6'>
          <div className='row mb-2'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Email Id</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='emailId'
            placeholder='Email Id'
            onChange={(e) => handleChange(e)}
            value={formData.emailId || ''}
          />
          {errors && errors.emailId && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.emailId}
            </div>
          )}
        </div>
        <div className='mb-8'>
          <div className='row'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center'>
                <span className='text-dark fs-4 fw-bolder'> Upload Document </span>
              </label>
            </div>
          </div>
          <input
            type="file"
            className="d-none"
            name="merchant"
            id="merchant"
            multiple={false}
            ref={merchantFIleInput}
            onChange={FileChangeHandler} />
          <button type="button"
            className={`${!Showmerchant ? 'btn btn-light-primary btn-sm mt-4' : 'btn btn-sm btn-success mt-4'}`}
            onClick={merchantFIleClick}>
            {merchantUpload}
          </button>
          {errors && errors.business && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.business}
            </div>
          )}
        </div>










        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Business name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='businessName'
            placeholder='Business name'
            onChange={(e) => handleChange(e)}
            value={formData.businessName || ''}
            maxLength={50}
          />
          {errors && errors.businessName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.businessName}
            </div>
          )}
        </div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Primary Contact Name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='primaryContactName'
            placeholder='Primary Contact Name'
            onChange={(e) => handleChange(e)}
            value={formData.primaryContactName || ''}
            maxLength={50}
          />
          {errors && errors.primaryContactName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.primaryContactName}
            </div>
          )}
        </div>

        <div className='row mt-6'>
          <h3 className='mb-8 d-flex justify-content-center'>Pick only one that suits to your business </h3>
          <div className='col-sm-12 col-md-12 col-lg-12 mb-4' >
            {
              getEntityRes && getEntityRes.data && getEntityRes.data.map((item, i) => {
                return (
                  <a
                    onClick={() =>
                      ToggleTab(item && item._id, item && item.kyc_entity)
                    }
                    className={tabDefault === `${item && item._id}` ? "active btn btn-primary me-2 btn-sm mt-2" : "btn btn-outline btn-outline-primary me-2 btn-sm mt-2"}>
                    {
                      item && item.kyc_entity
                    }
                  </a>
                )
              })
            }
          </div>
          {errors && errors.entity_id && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.entity_id}
            </div>
          )}
        </div>
        <div className='current mt-4' data-kt-stepper-element='content'>
          <div className='w-100'>
            <h3 className='mb-8 d-flex justify-content-center'>Business Category</h3>
            <div className="form-check form-check-custom form-check-solid form-check-sm mb-2">
              <label>
                <input
                  className="form-check-input"
                  type="radio"
                  value="Ecommerce/Online Store/MarketPlace"
                  id="flexRadioLg"
                  name='businessCategory'
                  onChange={(e) => handleChange(e)}
                />
                <span className='form-check-label text-muted'>
                  Ecommerce/Online Store/MarketPlace
                </span>
              </label>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm mb-2">
              <label>
                <input
                  className="form-check-input"
                  type="radio"
                  id="flexRadioLg"
                  name='businessCategory'
                  value='Restaurant/QSR'
                  onChange={(e) => handleChange(e)}
                />
                <span className='form-check-label text-muted'>
                  Restaurant/QSR
                </span>
              </label>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm mb-2">
              <label>
                <input
                  className="form-check-input"
                  type="radio"
                  value="Financial service provider"
                  id="flexRadioLg"
                  name='businessCategory'
                  onChange={(e) => handleChange(e)}
                />
                <span className='form-check-label text-muted'>
                  Financial service provider
                </span>
              </label>
            </div>

            {errors && errors.businessCategory && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.businessCategory}
              </div>
            )}
            <div className='fv-row mb-10 mt-8'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Add your website link</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Website'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='website'
                placeholder='Website'
                onChange={(e) => handleChange(e)}
                value={formData.website || ''}
              />
              {errors && errors.website && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.website}
                </div>
              )}
            </div>
            <div className='fv-row mb-10 mt-8'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'> Business Description</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Business Description'
                />
              </label>
              <textarea
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='businessDescription'
                placeholder='Business Description'
                onChange={(e) => handleChange(e)}
                value={formData.businessDescription || ''}
                maxLength={200}
              />
              {errors && errors.businessDescription && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.businessDescription}
                </div>
              )}
            </div>
          </div>

        </div>
        <div className='fv-row mt-4 mb-6'>
          <div className='d-flex justify-content-end'>
            <button type='submit' className='btn btn-sm btn-primary me-3'
              onClick={() => {
                handleSubmit()
              }}
            >
              Proceed
            </button>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists,
  getEntityRes: state && state.EntityStore && state.EntityStore.EntityRes,
  AccountUploadResLoading: state && state.AccountUploadStore && state.AccountUploadStore.loading,
  AccountUploadRes: state && state.AccountUploadStore && state.AccountUploadStore.AccountUploadRes,
})

const mapDispatchToProps = (dispatch) => ({
  getEntityDispatch: () => dispatch(EntityAction.Entity()),
  AccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.AccountUploadDoc(params)),
  clearAccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.clearAccountUploadDoc(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(BusinessForm)