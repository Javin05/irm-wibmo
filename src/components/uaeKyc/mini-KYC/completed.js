import React, { useEffect, useState } from "react"
import { toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { STATUS_RESPONSE, USER_ERROR } from '../../../utils/constants'
import { KYCAddAction, KYCActions } from '../../../store/actions'
import { warningAlert } from "../../../utils/alerts"
import _, { isEmpty, values } from 'lodash'

function ComptedPage(props) {
  const {
    setKycShow,
    setShow,
    MiniKycAllDataResLoading,
    MiniKycAllDataRes,
    kycAllDataSaved,
    KYCAddDispatch,
    ClearKYCDispatch,
    setClientDetails,
    getKYClistDispatch,
    setCompletedSteps,
    setActiveStep,
    onClickNext
  } = props

  const [show, SetShow] = useState(false)

  useEffect(() => {
    const data = {
      phoneNumber: kycAllDataSaved && kycAllDataSaved.phoneNumber,
      alternatePhoneNumber: kycAllDataSaved && kycAllDataSaved.alterNativeNumbar,
      countryCode: kycAllDataSaved && kycAllDataSaved.countryCode,
      emailId: kycAllDataSaved && kycAllDataSaved.Email && kycAllDataSaved.Email.contactEmail,
      alternateEmailId: kycAllDataSaved && kycAllDataSaved.Email && kycAllDataSaved.Email.alternateEmail,
      clientId: kycAllDataSaved && kycAllDataSaved.clientId,
      businessCategory: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessCategory,
      businessName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessName,
      entityName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.entiryName,
      entityId: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.entityId,
      primaryContactName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.primaryContactName,
      website: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.website,
      businessDescription: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessDescription
    }
    if (!_.isEmpty(data)) {
      KYCAddDispatch(data)
    }
  }, [])

  useEffect(() => {
    if (MiniKycAllDataRes && MiniKycAllDataRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setClientDetails((values) => ({
        ...values,
        PhoneNUmberId:
          MiniKycAllDataRes && MiniKycAllDataRes.data && MiniKycAllDataRes.data._id,
        allData: MiniKycAllDataRes && MiniKycAllDataRes.data
      }))
      getKYClistDispatch()
      onClickNext(4)
    } else if (MiniKycAllDataRes && MiniKycAllDataRes.status === STATUS_RESPONSE.ERROR_MSG) {
      SetShow(true)
      warningAlert(
        'error',
        MiniKycAllDataRes && MiniKycAllDataRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      ClearKYCDispatch()
    }
  }, [MiniKycAllDataRes])

  return (
    <>
      <div>
        {
          show ? (
            <>
              <div>
                <div class="card card-flush bg-danger"
                >
                  <div class="card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"
                  >
                    <div className="d-flex justify-content-center">
                    </div>
                    <div class="fs-1hx fw-bold text-white text-center mb-10 mt-5">
                      Your Data Not Updated
                    </div>
                  </div>
                </div>
                <div class="text-center mt-4">
                  <a class=" ms-4 btn btn-light-danger"
                    onClick={() => {
                      setShow(false)
                      setCompletedSteps([0])
                      setActiveStep(0)
                    }}
                  >Back to List</a>
                </div>
              </div>
            </>
          ) : (
            <>
              {
                !MiniKycAllDataResLoading ? (
                  null
                ) : (
                  <>
                    <div
                      className='d-flex justify-content-center mt-20'
                    >
                      <h4 className='text-muted me-4'>Processing</h4>
                      <span
                        className='spinner-grow text-primary me-4'
                        role='status'
                      />
                      <span
                        className='spinner-grow text-danger me-4'
                        role='status'
                      />
                      <span
                        className='spinner-grow text-warning me-4'
                        role='status'
                      />
                      <span
                        className='spinner-grow text-info me-4'
                        role='status'
                      />
                    </div>
                  </>
                )
              }
            </>
          )
        }
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  MiniKycAllDataRes: state && state.KYCAddStore && state.KYCAddStore.KYCAddResponse,
  MiniKycAllDataResLoading: state && state.KYCAddStore && state.KYCAddStore.loading,
})

const mapDispatchToProps = (dispatch) => ({
  KYCAddDispatch: (data) => dispatch(KYCAddAction.KYCAdd(data)),
  ClearKYCDispatch: (data) => dispatch(KYCAddAction.clearKYC(data)),
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ComptedPage)