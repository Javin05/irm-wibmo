
import React, { FC, useEffect, useRef, useState, useCallback } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  CommomFileAction,
  EntityValueAction
} from '../../../store/actions'
import { STATUS_RESPONSE, DROPZONE_MESSAGES, KYC_URL } from '../../../utils/constants'
import { useDropzone } from 'react-dropzone'
import {
  FILE_FORMAT_TYPE
} from "../../../constants/index";
import { warningAlert } from "../../../utils/alerts"
import { getLocalStorage } from '../../../utils/helper'
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import { identifyValidation } from './Validation'
import Select from 'react-select'

const customStyles = {
  control: (provided) => ({
    ...provided,
    alignItems: "baseline",
    background: "#fff",
    minHeight: "35px",
    border: "solid 0px",
    borderBottom: "solid 1px",
    boxShadow: "0 0 0 1px #fff",
    marginBottom: "0px",
    "&:hover": {
      border: "#fff",
      borderBottom: "solid 1px",
    },
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    color: "#cfc3c3",
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    width: "0px",
  }),
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? color.gray : color.black,
    background: state.isSelected ? color.white : "",
    borderColor: "#fff",
  }),
  placeholder: (provided) => ({
    ...provided,
    fontSize: "1em",
    color: "black",
    fontWeight: 200,
  })
}

function IdentityVerification(props) {
  const {
    onClickNext,
    setFullKycDetails,
    CommonFileDocument,
    ClearCommonFileDispatch,
    EntityValueDispatch,
    EntityValue,
    CommonFileDispatch,
    CommonFileLoading,
    clientDetails
  } = props

  const hiddenFileInput = useRef(null)
  const addressFilesInput = useRef(null)
  const businessFilesInput = useRef(null)

  const [IndidualOption, setIndidualOption] = useState("India")
  const [selectedIndidualOption, setSelectedIndidualOption] = useState([])
  const [indualValue, setIndualValue] = useState([])

  const [errors, setErrors] = useState({})
  const [individuaFileName, setindividuaFileName] = useState('Upload')
  const [showIndidual, setshowIndidual] = useState(false)
  const [currentInputIndex, setCurrentInputIndex] = useState()
  const [fileIndex, setfileIndex] = useState()
  const [indualFormData, setIndualFormData] = useState([])
  const [indidualError, setIndidualError] = useState([])

  const [addressFileName, setaddressFileName] = useState('Upload')
  const [showAddress, setshowAddress] = useState(false);
  const [addressFormData, setAddressFormData] = useState([])
  const [addressIndex, setAddressIndex] = useState()
  const [addressfileIndex, setAddressfileIndex] = useState()


  const [businessFileName, setBusinessFileName] = useState('Upload')
  const [showbusiness, setshowBusiness] = useState(false);
  const [businessFormData, setBusinessFormData] = useState([])
  const [businessIndex, setBusinessIndex] = useState()
  const [businessfileIndex, setBusinessfileIndex] = useState()

  useEffect(() => {
    const id = clientDetails && clientDetails.allData && clientDetails.allData.entityId
    // const id = '6447ba0e0ead250f08528265'
    const params = {
      entity_id: id
    }
    EntityValueDispatch(params)
  }, [])

  console.log('businessFormData', businessFormData)

  const IndidualHandleChange = (e, currentIndex) => {
    setCurrentInputIndex(currentIndex)
    const { name, value } = e.target
    const data = [...indualFormData]
    data[currentIndex][name] = value
    setIndualFormData(data)
  }

  const AddressHandleChange = (e, currentIndex) => {
    setAddressIndex(currentIndex)
    const { name, value } = e.target
    const data = [...addressFormData]
    data[currentIndex][name] = value
    setAddressFormData(data)
  }

  const BusinessHandleChange = (e, currentIndex) => {
    setBusinessIndex(currentIndex)
    const { name, value } = e.target
    const data = [...businessFormData]
    data[currentIndex][name] = value
    setBusinessFormData(data)
  }

  useEffect(() => {
    if (EntityValue && !_.isEmpty(EntityValue.identityProof)) {
      const data = EntityValue && EntityValue.identityProof
      setIndualFormData(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
      setIndidualError(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
    if (EntityValue && !_.isEmpty(EntityValue.otherDocuments)) {
      const data = EntityValue && EntityValue.otherDocuments
      setAddressFormData(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
    if (EntityValue && !_.isEmpty(EntityValue.businessProof)) {
      const data = EntityValue && EntityValue.businessProof
      setBusinessFormData(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
  }, [EntityValue])

  const handleSubmit = () => {
    // const errorMsg = identifyValidation(indualFormData, setIndidualError)
    // if (errorMsg) {
    //   onClickNext(5)
    // }
    setFullKycDetails((values) => ({
      ...values,
      indidual: indualFormData,
      adress: addressFormData,
      business: businessFormData
    }))
    onClickNext(6)
  }

  useEffect(() => {
    const typeData = CommonFileDocument && CommonFileDocument.data
    if (typeData && typeData.type === "individual" && CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      setErrors(false)
      setshowIndidual(true)
      setindividuaFileName('Uploaded')
      if (data) {
        setIndualFormData(list => list.map((item, i) =>
          i === fileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "Address" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAddressFormData(list => list.map((item, i) =>
          i === addressfileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      setshowAddress(true)
      setaddressFileName('Uploaded')
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "Business" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setBusinessFormData(list => list.map((item, i) =>
          i === businessfileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      setshowBusiness(true)
      setBusinessFileName('Uploaded')
      ClearCommonFileDispatch()
    }
  }, [CommonFileDocument])

  const FileChangeHandler = (e) => {
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(FILE_FORMAT_TYPE, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        if (name === 'individual') {
          const data = new FormData()
          data.append('type', 'individual')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'Address') {
          const data = new FormData()
          data.append('type', 'Address')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'Business') {
          const data = new FormData()
          data.append('type', 'Business')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        }
      } else {
        setErrors({
          ...errors,
          [name]:
            warningAlert(
              'error',
              `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
              '',
              'Try again',
              '',
              () => { { } }
            )
        })
      }
    } else {
      setErrors({
        ...errors, [name]:
          warningAlert(
            'error',
            DROPZONE_MESSAGES.IMAGE_INVALID,
            '',
            'Try again',
            '',
            () => { { } }
          )
      })
    }
  }

  const handleClick = (event, i) => {
    hiddenFileInput.current.click(event)
    setfileIndex(i)
  }
  const AdresshandleClick = (event, i) => {
    addressFilesInput.current.click(event)
    setAddressfileIndex(i)
  }

  const BusinesshandleClick = (event, i) => {
    businessFilesInput.current.click(event)
    setBusinessfileIndex(i)
  }
  const BusinessClick = (event, i) => {
    businessFilesInput.current.click(event)
    setBusinessfileIndex(i)
  }

  const IndidualChange = (selectedOption, currentIndex) => {
    setCurrentInputIndex(currentIndex)
    setIndualValue(selectedOption)
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      const data = [...indualFormData]
      data[currentIndex]['documentName'] = selectedOption.value
      setIndualFormData(data)
    } else {
      setSelectedIndidualOption()
    }
    setErrors({ ...errors, country: '' })
  }

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Upload your business documents</span>
                  </label>
                </div>
              </div>
              {
                _.isArray(EntityValue.identityProof) && EntityValue.identityProof.length ? (
                  <div className='row'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Identity & Address Proof
                      </label>
                    </div>
                    <div className='col-lg-4'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Document Number
                      </label>
                    </div>
                    <div className='col-lg-2'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                        Upload Doc
                      </label>
                    </div>
                  </div>
                ) : null
              }
              {
                EntityValue &&
                  _.isArray(EntityValue.identityProof) && EntityValue.identityProof.length ? (
                  EntityValue.identityProof.map((item, i) => {
                    const defaultValue = []
                    const businessProof = _.forEach(item.entity_document, function (o) {
                      defaultValue.push({
                        label: o,
                        value: o,
                        name: 'documentName'
                      })
                    })
                    return (
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-lg-6'>
                            {/* <Select
                              isClearable={currentInputIndex}
                              styles={customStyles}
                              name='country'
                              placeholder="Select..."
                              className="basic-single"
                              classNamePrefix="select"
                              onChange={(e) => {
                                IndidualChange(e, i)
                              }}
                              options={defaultValue}
                              defaultValue={() => {
                                if (currentInputIndex === i) {
                                  return selectedIndidualOption
                                } else if (_.isLength(indualFormData) > 0 ){
                                  return {
                                    value: indualFormData[i].documentName,
                                    label: indualFormData[i].documentName
                                  }
                                }
                              }}
                            /> */}
                            <select
                              name='documentName'
                              className='form-select form-select-solid mb-4'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => IndidualHandleChange(e, i)}
                            >
                              <option value=''>Select...</option>
                              {item &&
                                item.entity_document.length > 0 &&
                                item.entity_document.map((itemData, i) => (
                                  <option key={i}
                                    name='documentName'
                                    defaultValue={currentInputIndex === i ? indualFormData[i].documentName : itemData}
                                  >
                                    {itemData}
                                  </option>
                                ))}
                            </select>
                            {
                              _.isLength(indidualError) > 0 ?
                                indidualError && indidualError.documentName[i] && (
                                  <div className='rr mt-1'>
                                    <style>{'.rr{color:red;}'}</style>
                                    {indidualError.documentName[i]}
                                  </div>
                                )
                                : null}
                          </div>
                          <div className='col-lg-4'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid'
                              name='documentNumber'
                              placeholder='Document Number'
                              onChange={(e) => IndidualHandleChange(e, i)}
                            />

                            {/* {
                              indidualError.length > 0 ?
                                indidualError && indidualError.documentNumber[i] && (
                                  <div className='rr mt-1'>
                                    <style>{'.rr{color:red;}'}</style>
                                    {indidualError.documentNumber[i]}
                                  </div>
                                )
                                : null} */}
                          </div>
                          <div className='col-md-2'>
                            <input
                              type="file"
                              className="d-none"
                              name="individual"
                              id="individual"
                              multiple={false}
                              ref={hiddenFileInput}
                              accept="image/*"
                              onChange={(e) => {
                                FileChangeHandler(e)
                                e.target.value = null
                              }}
                            />
                            <button type="button"
                              className={`${showIndidual && !_.isEmpty(indualFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                              onClick={(e) => { handleClick(e, i) }}
                            >
                              {
                                showIndidual && !_.isEmpty(indualFormData[i].uploadDocument) ?
                                  <>
                                    {
                                      individuaFileName
                                    }
                                  </>
                                  : "Upload"
                              }
                            </button>
                            {/* {indidualError && indidualError.uploadDocument[i] && (
                              <div className="rr mt-1">
                                <style>
                                  {
                                    ".rr{color:red}"
                                  }
                                </style>
                                {indidualError.uploadDocument[i]}
                              </div>
                            )} */}
                          </div>
                        </div>
                      </div>
                    )
                  })
                ) : null
              }
              {
                _.isArray(EntityValue.otherDocuments) && EntityValue.otherDocuments.length ? (
                  <div className='row'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Other Documents Proof
                      </label>
                    </div>
                    <div className='col-lg-4'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Document Number
                      </label>
                    </div>
                    <div className='col-lg-2'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                        Upload Doc
                      </label>
                    </div>
                  </div>
                ) : null
              }
              {
                EntityValue &&
                  _.isArray(EntityValue.otherDocuments) && EntityValue.otherDocuments.length ? (
                  EntityValue.otherDocuments.map((item, i) => {
                    return (
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-lg-6'>
                            <select
                              name='documentName'
                              className='form-select form-select-solid mb-4'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => AddressHandleChange(e, i)}
                            >
                              <option value=''>Select...</option>
                              {item &&
                                item.entity_document.length > 0 &&
                                item.entity_document.map((itemData, i) => (
                                  <option key={i}
                                    name='documentName'
                                    defaultValue={addressIndex === i ? addressFormData[i].documentName : itemData}
                                  >
                                    {itemData}
                                  </option>
                                ))}
                            </select>
                          </div>
                          <div className='col-lg-4'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid'
                              name='documentNumber'
                              placeholder='Document Number'
                              onChange={(e) => AddressHandleChange(e, i)}
                            />
                          </div>
                          <div className='col-md-2'>
                            <input
                              type="file"
                              className="d-none"
                              name="Address"
                              id="Address"
                              multiple={false}
                              ref={addressFilesInput}
                              accept="image/*"
                              onChange={(e) => {
                                FileChangeHandler(e)
                                e.target.value = null
                              }}
                            />
                            <button type="button"
                              className={`${showAddress && !_.isEmpty(addressFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                              onClick={(e) => { AdresshandleClick(e, i) }}
                            >
                              {
                                showAddress && !_.isEmpty(addressFormData[i].uploadDocument) ?
                                  <>
                                    {
                                      addressFileName
                                    }
                                  </>
                                  : "Upload"
                              }
                            </button>
                          </div>
                        </div>
                      </div>
                    )
                  })
                ) : null
              }

              {
                _.isArray(EntityValue.businessProof) && EntityValue.businessProof.length ? (
                  <div className='row'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Business Proof
                      </label>
                    </div>
                    <div className='col-lg-4'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Document Number
                      </label>
                    </div>
                    <div className='col-lg-2'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                        Upload Doc
                      </label>
                    </div>
                  </div>
                ) : null
              }
              {
                EntityValue &&
                  _.isArray(EntityValue.businessProof) && EntityValue.businessProof.length ? (
                  EntityValue.businessProof.map((item, i) => {
                    return (
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-lg-6'>
                            <select
                              name='documentName'
                              className='form-select form-select-solid mb-4'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => BusinessHandleChange(e, i)}
                            >
                              <option value=''>Select...</option>
                              {item &&
                                item.entity_document.length > 0 &&
                                item.entity_document.map((itemData, i) => (
                                  <option key={i}
                                    name='documentName'
                                    defaultValue={businessIndex === i ? businessFormData[i].documentName : itemData}
                                  >
                                    {itemData}
                                  </option>
                                ))}
                            </select>
                          </div>
                          <div className='col-lg-4'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid'
                              name='documentNumber'
                              placeholder='Document Number'
                              onChange={(e) => BusinessHandleChange(e, i)}
                            />
                          </div>
                          <div className='col-md-2'>
                            <input
                              type="file"
                              className="d-none"
                              name="Business"
                              id="Business"
                              multiple={false}
                              ref={businessFilesInput}
                              accept="image/*"
                              onChange={(e) => {
                                FileChangeHandler(e)
                                e.target.value = null
                              }}
                            />
                            <button type="button"
                              className={`${showbusiness && !_.isEmpty(businessFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                              onClick={(e) => { BusinessClick(e, i) }}
                            >
                              {
                                showbusiness && !_.isEmpty(businessFormData[i].uploadDocument) ?
                                  <>
                                    {
                                      businessFileName
                                    }
                                  </>
                                  : "Upload"
                              }
                            </button>
                          </div>
                        </div>
                      </div>
                    )
                  })
                ) : null
              }


            </div>
            <div className='row'>
              <div className='d-flex justify-content-end pt-10'>
                <div className='me-2'>
                  <button
                    onClick={() => { handleSubmit() }}
                    type='button'
                    className='btn btn-sm btn-light-primary me-3'
                  >
                    Proceed
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { CommonFileStore, EntityValueTypeStore } = state
  return {
    CommonFileDocument: CommonFileStore && CommonFileStore.commonFileRes ? CommonFileStore.commonFileRes : {},
    CommonFileLoading: CommonFileStore && CommonFileStore.loading ? CommonFileStore.loading : {},
    EntityValue: EntityValueTypeStore && EntityValueTypeStore.EntityValue ? EntityValueTypeStore.EntityValue.data : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  CommonFileDispatch: (params) => dispatch(CommomFileAction.CommonFile(params)),
  ClearCommonFileDispatch: (params) => dispatch(CommomFileAction.clearCommonFile(params)),
  EntityValueDispatch: (params) => dispatch(EntityValueAction.EntityValue(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(IdentityVerification)