import React, { FC, useEffect, useRef, useState, useCallback } from 'react'
import Camera from './VideoKYC'
import { KTSVG } from "../../../theme/helpers"

function ScaningDocument(props) {
  const {
    onClickNext,
    goBack,
    setClientDetails,
    setFullKycDetails
  } = props

  const [frontShow, SetFrontSow] = useState(false)
  const [secondShow, SetSecondShow] = useState(false)
  const [Show, SetShow] = useState(false)
  const [ProfileShow, SetProfileShow] = useState(false)

  useEffect(() => {
    SetShow(true)
  }, [])

  return (
    <>
      {
        Show ? (
          <div className="card">
            <div className="card-body p-0">
              <h4 className='d-flex justify-content-center text-dark'>Verify your Identity</h4>
              <div className='row'>
                <div className='col-lg-2' />
                <div className='col-lg-8 mt-8'>
                  <ul className="">
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <i className="bi bi-1-circle fs-1 text-dark" />
                      <i className="bi bi-credit-card-2-back text-dark fs-5x" />
                      <span className="fs-5 fw-bold">Scan ID Card Front Side</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <i className="bi bi-1-circle fs-1 text-dark" />
                      <i className="bi bi-credit-card-2-back text-dark fs-5x" />
                      <span className="fs-5 fw-bold">Scan ID Card Back Side</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <i className="bi bi-1-circle fs-1 text-dark" />
                      <i className="bi bi-person-circle text-dark fs-5x" />
                      <span className="fs-5 fw-bold ms-4">Take a selfie picture</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className='d-flex justify-content-center'>
                <button
                  className='btn btn-sm btn-light-primary'
                  onClick={() => {
                    SetFrontSow(true)
                    SetShow(false)
                  }}
                >
                  Start
                </button>
              </div>
            </div>
          </div>
        ) : (null)
      }
      {
        frontShow ?
          <div className="card">
            <div className="card-body p-0">
              <h4 className='d-flex justify-content-center text-dark'>Scan your ID Card</h4>
              <div className='row'>
                <div className='col-lg-2' />
                <div className='col-lg-8 mt-8'>
                  <div className='row'>
                    <div className='col-lg-4'>
                      <i className="bi bi-credit-card-2-back text-dark fs-5x ms-8" />
                    </div>
                    <div className='col-lg-8 mt-6'>
                      <span className="fs-5 fw-bold">FRONT SIDE</span>
                    </div>
                  </div>
                </div>
                <div className='col-lg-12'>
                  <Camera
                    SetFrontSow={SetFrontSow}
                    SetSecondShow={SetSecondShow}
                    frontShow={frontShow}
                    setFullKycDetails={setFullKycDetails}
                  />
                </div>
              </div>
            </div>
          </div>
          : (null)
      }
      {
        secondShow ?
          <div className="card">
            <div className="card-body p-0">
              <h4 className='d-flex justify-content-center text-dark'>Scan your ID Card</h4>
              <div className='row'>
                <div className='col-lg-2' />
                <div className='col-lg-8 mt-8'>
                  <div className='row'>
                    <div className='col-lg-4'>
                      <i className="bi bi-credit-card-2-back text-dark fs-5x ms-8" />
                    </div>
                    <div className='col-lg-8 mt-6'>
                      <span className="fs-5 fw-bold">BACK SIDE</span>
                    </div>
                  </div>
                </div>
                <div className='col-lg-12'>
                  <Camera
                    SetFrontSow={SetFrontSow}
                    SetSecondShow={SetSecondShow}
                    secondShow={secondShow}
                    SetProfileShow={SetProfileShow}
                    ProfileShow={ProfileShow}
                    setFullKycDetails={setFullKycDetails}
                  />
                </div>
              </div>
            </div>
          </div>
          : (null)
      }
      {
        ProfileShow ?
          <div className="card">
            <div className="card-body p-0">
              <h4 className='d-flex justify-content-center text-dark'>Take a Selfie</h4>
              <div className='row'>
                <div className='col-lg-2' />
                <div className='col-lg-8 mt-8'>
                  <div className='row'>
                    <div className='col-lg-4'>
                      <i className="bi bi-person-circle text-dark fs-5x ms-8" />
                    </div>
                    <div className='col-lg-8 mt-6'>
                      <span className="fs-5 fw-bold">Fit your face in the middle</span>
                    </div>
                  </div>
                </div>
                <div className='col-lg-12'>
                  <Camera
                    SetFrontSow={SetFrontSow}
                    SetSecondShow={SetSecondShow}
                    secondShow={secondShow}
                    SetProfileShow={SetProfileShow}
                    ProfileShow={ProfileShow}
                    onClickNext={onClickNext}
                    setFullKycDetails={setFullKycDetails}
                  />
                </div>
              </div>
            </div>
          </div>
          : (null)
      }
      <div className='d-flex justify-content-end'>
        <button type='submit' className='btn btn-sm btn-light-danger ms-2'
          onClick={() => { onClickNext(5) }}
        >
          <span className='indicator-label'>
            <KTSVG
              path='/media/icons/duotune/files/fil007.svg'
              className='svg-icon-3 me-2'
            />
            Skip
          </span>
        </button>
      </div>
    </>
  )
}

export default ScaningDocument