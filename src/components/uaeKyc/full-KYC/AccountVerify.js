
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { contactInfoValidation } from './Validation'
import { setLocalStorage } from '../../../utils/helper'
import { REGEX, DROPZONE_MESSAGES } from '../../../utils/constants'
import _, { values } from 'lodash'
import {
  KYCaccountAction,
  AccountUploadAction
} from '../../../store/actions'
import {
  RESTRICTED_FILE_FORMAT_TYPE
} from "../../../constants/index";
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"

const getBanks = [
  {
    name: 'demo',
    _id: '1'
  }, {
    name: 'demo2',
    _id: '2'
  }
]

function AccountVerification(props) {
  const {
    onClickNext,
    setFullKycDetails,
    AccountUploadDocDispatch,
    AccountUploadRes,
    AccountUploadResLoading,
    clearAccountUploadDocDispatch
  } = props

  const cancelledFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [showForm, setShowForm] = useState(true)
  const [Data, setData] = useState(true)
  const [cancelledChequeUpload, setcancelledChequeUpload] = useState('Upload')
  const [showchecque, setShowchecque] = useState(false)
  const [BankOption, setBankOption] = useState()
  const [selectedBankOption, setSelectedBankOption] = useState('')
  const [formData, setFormData] = useState({
    bank: '',
    passWord: '',
    userName: '',
    accountType: ''
  })

  const handleChange = (e) => {
    console.log('sdsd', e.target.checked)
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
    setFormData((values) => ({ ...values, accountType: e.target.checked }))
  }

  const proccedData = () => {
    const errors = {}
    if (_.isEmpty(formData.userName)) {
      errors.userName = 'Account Number Is Required'
    }
    if (_.isEmpty(formData.ifscCode)) {
      errors.ifscCode = 'IFSC Is Required'
    }
    if (_.isEmpty(errors)) {
      setLocalStorage('AccountVerify', JSON.stringify(formData))
      setData(formData)
      setFullKycDetails((values) => ({ ...values, Account: formData }))
      onClickNext(5)
    }
    setErrors(errors)
  }

  useEffect(() => {
    return () => {
      setShowchecque(false)
      setFormData({
        userName: '',
        ifscCode: '',
        accountHolderName: '',
        cancelledCheque: ''
      })
    }
  }, [])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const Bank = getDefaultOptions(getBanks)
    setBankOption(Bank)
    if (!_.isEmpty(formData.bank)) {
      const selOption = _.filter(Bank, function (x) { if (_.includes(formData.bank._id, x.value)) { return x } })
      setSelectedBankOption(selOption)
    }
  }, [getBanks])

  const handleChangeBank = selectedOption => {
    if (selectedOption !== null) {
      setSelectedBankOption(selectedOption)
      setFormData(values => ({ ...values, Bank: selectedOption.label }))
    } else {
      setFormData(values => ({ ...values, Bank: '' }))
    }
    setErrors({ ...errors, Bank: '' })
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  console.log('formData', formData)

  return (
    <>
      <div>
        <div className='current'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='col-lg-6 mb-8'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='required fw-bold fs-4'>Select Your Bank</span>
                </label>
                <ReactSelect
                  isClearable
                  styles={customStyles}
                  isMulti={false}
                  name='bank'
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeBank}
                  options={BankOption}
                  value={selectedBankOption}
                  isDisabled={!BankOption}
                />
                {errors && errors.bank && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red}'}</style>
                    {errors.bank}
                  </div>
                )}
              </div>

              <div className='mb-4'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='required fw-bold fs-4'>Username</span>
                </label>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  name='userName'
                  placeholder='User Name'
                  onChange={(e) => handleChange(e)}
                  value={formData.userName || ''}
                  maxLength={42}
                />
                {errors && errors.userName && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.userName}
                  </div>
                )}
              </div>
              <div className='mb-4'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='required fw-bold fs-4'>PassWord</span>
                </label>
                <input
                  type='password'
                  className='form-control form-control-lg form-control-solid mt-4'
                  name='passWord'
                  placeholder='PassWord'
                  onChange={(e) => handleChange(e)}
                  value={formData.passWord || ''}
                />
                {errors && errors.passWord && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.passWord}
                  </div>
                )}
              </div>

              <div className='mb-4'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='fw-bold fs-4'>Account Type</span>
                </label>
                <div className='fs-5 mb-4'><i className="bi bi-circle-fill me-2 text-black" />Saving</div>
                <div className='fs-5 mb-4'><i className="bi bi-circle-fill me-2 text-black" />Checking</div>
                <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                  <span className='form-check form-check-custom form-check-solid me-2'>
                    <input
                      className='form-check-input'
                      type='checkbox'
                      name="accountType"
                      value='check'
                      onChange={(e) => handleChange(e)}
                    />
                  </span>
                  <span className='d-flex flex-column'>
                    <span className='fs-5 text-muted'>
                      I herby provide my consent to validate my bank account, and allos BNY
                      mellen,Signzy and Finicity to use my bank credentials to fetch the account
                      information from my bank
                    </span>
                  </span>
                </label>
              </div>

            </div>
            <div className='fv-row mb-4'>
              <div className='d-flex flex-stack pt-10 justify-content-end'>
                <div>
                  <button type='submit' className='btn btn-sm btn-light-primary ms-2'
                    onClick={() => { proccedData() }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Proceed
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  AccountUploadResLoading: state && state.AccountUploadStore && state.AccountUploadStore.loading,
  AccountUploadRes: state && state.AccountUploadStore && state.AccountUploadStore.AccountUploadRes,
})

const mapDispatchToProps = (dispatch) => ({
  kycAccountDispatch: (params) => dispatch(KYCaccountAction.KYCaccountDetail(params)),
  clearAccountDetailDispatch: (params) => dispatch(KYCaccountAction.clearKYCaccountDetail(params)),
  AccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.AccountUploadDoc(params)),
  clearAccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.clearAccountUploadDoc(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountVerification)