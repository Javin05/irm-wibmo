import _ from "lodash";

export const setPhoneData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      alterNativeNumbar: `${data && data.phoneNumber && data.phoneNumber.countryCode}${data && data.alternatePhoneNumber}`,
      contactNumber: `${data && data.phoneNumber && data.phoneNumber.countryCode}${data && data.phoneNumber && data.phoneNumber.number}`,
      clientId: data && data.clientId && data.clientId._id
    }
  }
}

export const setEmailData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      contactEmail: data && data.emailId && data.emailId.emailId,
      alternateEmail: data && data.alternateEmailId
    }
  }
}

export const setBusinessData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      businessCategory: data && data.businessCategory,
      website: data && data.website,
      businessName: data && data.businessName,
      primaryContactName: data && data.primaryContactName,
      entiryName: data && data.entityName,
      businessDescription: data && data.businessDescription,
      entity_id: data && data.entityId,
    }
  }
}

export const setAccountData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      accountDetails: data && data.bankAccountNumber,
      ifscCode: data && data.ifscCode,
      accountHolderName: data && data.accountHolderName,
      cancelledCheque: data && data.cancelledCheque
    }
  }
}

export const setAddressData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      Address: data && data.businessAddress && data.businessAddress.address,
      pinCode: data && data.businessAddress && data.businessAddress.pincode,
      state: data && data.businessAddress && data.businessAddress.state,
      city: data && data.businessAddress && data.businessAddress.city
    }
  }
}