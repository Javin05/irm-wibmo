import React, { useState, useEffect } from "react"
import { Modal } from "../../../theme/layout/components/modal/Modal"
import { connect } from "react-redux"
import { KTSVG } from "../../../theme/helpers"
import { VerticalStepper } from "../../../theme/layout/components/stepper/verticalStepper"
import {
  setLocalStorage,
  getLocalStorage,
  removeLocalStorage,
} from "../../../utils/helper"
import { KYC_FORM } from "../../../utils/constants"
import ShowFields from "../../KYC/ShowFields"
import { useLocation } from "react-router-dom"
import {
  StateActions,
  KYCAddAction,
  KYCUserAction,
  CityActions
} from "../../../store/actions"
import ContactInfo from "./ContactInfo"
import BusinessForm from "./Business"
import EmailInfo from "./Email"
import AccountVerification from "./AccountVerify"
import Address from "./Address"
import AddressDataValue from "./AddressData"
import FullKyc from "./completeKyc"
import FullKycCompletedPage from "./completed"
import IdentityVerification from './IdentifyVerificaion'
import _ from "lodash"

function EditKYC(props) {
  const {
    setEditMode,
    show,
    loading,
    getStateDispatch,
    clearPhoneDispatch,
    activeKyc,
    editMode,
    getKYCUserDetailsDispatch,
    userId,
    UserDetails,
    getCityDispatch,
    clearKYCUserDispatch
  } =
    props
  const [activeStep, setActiveStep] = useState(0)
  const [completedSteps, setCompletedSteps] = useState([-1])
  const [array, setArray] = useState([])
  const [clientDetails, setClientDetails] = useState({})
  const [fullKycDetails, setFullKycDetails] = useState({})
  const [summary, setSummary] = useState({})
  const active = getLocalStorage(KYC_FORM.ACTIVE_STEP)
  const [kycShow, setKycShow] = useState(false)
  const [EntityValue, setEntityValue] = useState(false)
  const [EntityName, setEntityName] = useState(false)

  const organName = EntityName ? EntityName : ''
  const UseData = UserDetails && UserDetails.data

  useEffect(() => {
    const params = {
      skipPagination: "true",
    }
    getStateDispatch(params)
    getCityDispatch(params)
  }, [])


  useEffect(() => {
    if (!_.isEmpty(`${userId}`)) {
      getKYCUserDetailsDispatch(userId)
    }
  }, [userId])

  const stepperArray = [
    {
      stepperLabel: "Mobile Verification",
      Optional: "",
    },
    {
      stepperLabel: "Email",
      Optional: "",
    },
    {
      stepperLabel: "Business Information ",
      Optional: "",
    },
    {
      stepperLabel: "Bank Account Details",
      Optional: "",
    },
    {
      stepperLabel: "Document Section",
      Optional: "",
    },
    {
      stepperLabel: "Business Address ",
      Optional: "",
    },
    {
      stepperLabel: "Complete",
      Optional: "",
    }
  ]

  const kycStepperArray = [
    {
      stepperLabel: "Mobile Verification",
      Optional: "",
    },
    {
      stepperLabel: "Email",
      Optional: "",
    },
    {
      stepperLabel: "Business Information ",
      Optional: "",
    },
    {
      stepperLabel: "Bank Account Details",
      Optional: "",
    },
    {
      stepperLabel: "Business Address ",
      Optional: "",
    },
    {
      stepperLabel: "Complete",
      Optional: "",
    }
  ]

  const onClickNext = (currentId) => {
    const id = currentId - 1
    setActiveStep(currentId)
    if (completedSteps.length === currentId) {
      if (completedSteps && !completedSteps.includes(id)) {
        setCompletedSteps((values) => [...values, id])
        setLocalStorage(KYC_FORM.ACTIVE_STEP, currentId)
      }
    }
  }

  const goBack = (id) => {
    setActiveStep(id)
  }

  const pathName = useLocation().pathname
  const url = pathName && pathName.split("update")
  const id = url && url[1]
  const urlName = pathName && pathName.split("update/")
  const clientId = urlName && urlName[1]

  const getDescendants = (arr, step) => {
    const val = []
    arr.forEach((item, i) => {
      if (step > i) {
        val.push(i)
      }
    })
    return val
  }
  const d = getDescendants(array, active)
  const editstepper = getDescendants(array, array.length)

  useEffect(() => {
    if (UseData && UseData.entityId === EntityValue) {
      setArray(kycStepperArray)
    } else {
      setArray(stepperArray)
    }
  }, [EntityValue])

  useEffect(() => {
    return () => {
      setKycShow(false)
      removeLocalStorage("ADDRESSVERIFY")
      removeLocalStorage("CONTACT_DETAILS")
      removeLocalStorage("BUSINESS")
      removeLocalStorage("BUSINESS_DETAILS")
      removeLocalStorage("BANK_ACCOUNT")
      removeLocalStorage("VALIDNAME")
      removeLocalStorage("BUSINESSTYPE")
      removeLocalStorage("ADDRESSVERIFY")
      removeLocalStorage("AccountVerify")
      removeLocalStorage("CINandGSTIN")
    }
  }, [])

  useEffect(() => {
    if (active) {
      setActiveStep(Number(active))
      setCompletedSteps([-1, ...d])
    } else {
      setActiveStep(0)
      setCompletedSteps([-1])
    }
  }, [active])

  useEffect(() => {
    onClickNext(0)
  }, [])
useEffect(() =>{
  setEntityValue(UseData && UseData.entityId)
  setEntityName(UseData && UseData.entiryName)
},[UseData])

useEffect(() =>{
  return() =>{
    clearKYCUserDispatch()
  }
},[])

// console.log('UseData && UseData.cancelledCheque', UseData)

  return (
    <>
      <Modal showModal={editMode} modalWidth={1000}>
        <div className="" id="userModal">
          <div>
            <div
              className="modal-dialog modal-dialog-centered mw-1000px"
              style={{ backgroundColor: "transparent" }}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h2 className="me-8 mt-4 mb-4 ms-4">Edit</h2>
                  <button
                    type="button"
                    className="btn btn-lg btn-icon btn-active-light-danger close me-4"
                    data-dismiss="modal"
                    onClick={() => {
                      setEditMode(false)
                      onClickNext(0)
                      setClientDetails(null)
                      clearPhoneDispatch()
                      removeLocalStorage("CONTACT_DETAILS")
                      removeLocalStorage("BUSINESS")
                      removeLocalStorage("BUSINESS_DETAILS")
                      removeLocalStorage("BANK_ACCOUNT")
                      removeLocalStorage("VALIDNAME")
                      removeLocalStorage("BUSINESSTYPE")
                      removeLocalStorage("ADDRESSVERIFY")
                      removeLocalStorage("AccountVerify")
                      removeLocalStorage("CINandGSTIN")
                      setKycShow(false)
                      setCompletedSteps([-1])
                      setActiveStep(0)
                    }}
                  >
                    <KTSVG
                      path="/media/icons/duotune/arrows/arr061.svg"
                      className="svg-icon-1"
                    />
                  </button>
                </div>
                <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                <div className="container-fixed">
                  <div className="card-header">
                    <div className="card-body">
                      <div
                        className="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid"
                        id="kt_modal_create_app_stepper"
                      >
                        <div className="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                          <VerticalStepper
                            activeStep={activeStep}
                            setActiveStep={setActiveStep}
                            completedSteps={completedSteps}
                            stepperArr={array}
                            setCompletedSteps={setCompletedSteps}
                            onStepperClick={onClickNext}
                          />
                        </div>
                        <div
                          className="flex-row-fluid py-lg-5 px-lg-15 -bs-gray-100 mt-10"
                          style={{
                            backgroundoutline: "#f1faff",
                          }}
                        >
                          <ShowFields>
                            <div className="scroll h-400px px-5">
                              <>
                                {activeStep === 0 ? (
                                  <ContactInfo
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    UserDetails={UserDetails}
                                  />
                                ) : null}
                                {activeStep === 1 ? (
                                  <EmailInfo
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    kycAllDataSaved={clientDetails}
                                    UserDetails={UserDetails}
                                  />
                                ) : null}
                                {activeStep === 2 ? (
                                  <BusinessForm
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    clientDetails={clientDetails}
                                    setCompletedSteps={setCompletedSteps}
                                    setActiveStep={setActiveStep}
                                    UserDetails={UserDetails}
                                    setEntityValue={setEntityValue}
                                    setEntityName={setEntityName}
                                  />
                                ) : null}
                                {activeStep === 3 ? (
                                  <AccountVerification
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    setFullKycDetails={setFullKycDetails}
                                    clientDetails={clientDetails}
                                    UserDetails={UserDetails}
                                  />
                                ) : null}
                                {
                                  UseData && UseData.entityId === EntityValue ? (
                                    <>
                                      {activeStep === 4 ? (
                                        <AddressDataValue
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setFullKycDetails={setFullKycDetails}
                                          UserDetails={UserDetails}
                                        />
                                      ) : null}
                                    </>
                                  ) : (
                                    <>
                                      {activeStep === 4 ? (
                                        <IdentityVerification
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setFullKycDetails={setFullKycDetails}
                                          organName={organName}
                                          UserDetails={UserDetails}
                                        />
                                      ) : null}
                                    </>
                                  )
                                }
                                {
                                  UseData && UseData.entityId === EntityValue ? (null) : (
                                    <>
                                      {activeStep === 5 ? (
                                        <Address
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setFullKycDetails={setFullKycDetails}
                                          UserDetails={UserDetails}
                                        />
                                      ) : null}
                                    </>
                                  )
                                }
                                {activeStep === 6 ? (
                                  <FullKyc
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    clientDetails={clientDetails}
                                    setKycShow={setKycShow}
                                    kycAllDataSaved={clientDetails}
                                    fullKycDetails={fullKycDetails}
                                    activeKyc={activeKyc}
                                    UseData={UseData}
                                  />
                                ) : null}
                                {activeStep === 7 ? (
                                  <FullKycCompletedPage
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    clientDetails={clientDetails}
                                    setKycShow={setKycShow}
                                    kycAllDataSaved={clientDetails}
                                    fullKycDetails={fullKycDetails}
                                    setEditMode={setEditMode}
                                    UseData={UseData}
                                    setCompletedSteps={setCompletedSteps}
                                    setActiveStep={setActiveStep}
                                  />
                                ) : null}
                              </>
                            </div>
                          </ShowFields>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}
const mapStateToProps = (state) => {
  const {
    usertypeStore,
    KYCUserStoreKey,
  } = state
  return {
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData ? usertypeStore.userTypeData : {},
    UserDetails: KYCUserStoreKey && KYCUserStoreKey.KYCUser ? KYCUserStoreKey.KYCUser : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  clearPhoneDispatch: () => dispatch(KYCAddAction.clearKYC()),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
  clearKYCUserDispatch: (id) => dispatch(KYCUserAction.KYCUser_CLEAR(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(EditKYC)
