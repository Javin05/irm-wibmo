import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { useLocation, Link, useParams } from 'react-router-dom'
import { DATE, STATUS_RESPONSE } from '../../utils/constants'
import {
    dashboardDetailsActions,
    riskSummaryActions,
    merchantIdDetailsActions,
    TransactiondashboardActions,
    TransactionDistanceActions
} from '../../store/actions'
import { riskManagementActions } from '../../store/actions'
import { ApproveActions } from '../../store/actions'
import { USER_ERROR, REGEX, RESPONSE_STATUS, SESSION, SWEET_ALERT_MSG } from '../../utils/constants'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import { useLoadScript } from "@react-google-maps/api";
import HorizontalGauge from 'react-horizontal-gauge';
// import LocationSearchModal from '../merchant/subComponent/map'
import MapGoogle from '../maps/MapGoogle'
import "react-circular-progressbar/dist/styles.css";

function TransactionDashboard(props) {
    const {
        id,
        riskmgmtlistdetails,
        getRiskManagementlistDispatch,
        ApprovePost,
        approveResponceData,
        openMap,
        openBusinessMap,
        getIdMerchantDispatch,
        clearApprove,
        TxndashboardDispatch,
        TransactionDashboardIdDetail,
        TransactionDistance,
        TxnDistanceDispatch,
        TransactionGetById
    } = props

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: "AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws" // Add your API key
    });

    const [active, setActive] = useState(false)
    const pathName = useLocation().pathname
    const url = pathName && pathName.split('/')
    const currentId = url && url[3]

    const [errors, setErrors] = useState({
        reason: '',
    })
    const [formData, setFormData] = useState({
        message: ''
    })
    const [holdFormData, setHoldFormData] = useState({
        riskStatus: 'HOLD',
        reason: ''
    })
    const [rejectFormData, setRejectFormData] = useState({
        riskStatus: 'REJECTED',
        reason: ''
    })
    const [approveFormData, setApproveFormData] = useState({
        riskStatus: 'APPROVED',
    })

    const approveSubmit = () => {
        ApprovePost(id, approveFormData)
    }
    const onConfirmHold = () => {
        ApprovePost(id, holdFormData)
    }
    const holdSubmit = () => {
        const errors = {}
        if (_.isEmpty(holdFormData.reason)) {
            errors.reason = USER_ERROR.REASON
        }
        setErrors(errors)
        if (_.isEmpty(errors)) {
            confirmationAlert(
                SWEET_ALERT_MSG.CONFIRMATION_TEXT,
                SWEET_ALERT_MSG.HOLD,
                'warning',
                'Yes',
                'No',
                () => { onConfirmHold() },
                () => {}
            )
        }
    }
    const onConfirmReject = () => {
        ApprovePost(id, rejectFormData)
    }
    const rejectSubmit = () => {
        const errors = {}
        if (_.isEmpty(rejectFormData.reason)) {
            errors.reason = USER_ERROR.REASON
        }
        setErrors(errors)
        if (_.isEmpty(errors)) {
            confirmationAlert(
                SWEET_ALERT_MSG.CONFIRMATION_TEXT,
                SWEET_ALERT_MSG.REJECT,
                'warning',
                'Yes',
                'No',
                () => { onConfirmReject() },
                () => {}
            )
        }
    }
    const handleChange = (e) => {
        setHoldFormData({ ...holdFormData, [e.target.name]: e.target.value })
        setErrors({ ...errors, [e.target.name]: '' })
    }
    const rejectChange = (e) => {
        setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
        setErrors({ ...errors, [e.target.name]: '' })
    }

    useEffect(() => {
        if (currentId) {
            TxndashboardDispatch(currentId)
            TxnDistanceDispatch(currentId)
        }
    }, [currentId])

    useEffect(() => {
        if (openMap) {
            const modalBtn = document.getElementById('modal-btn')
            modalBtn.click()
        }
    }, [openMap])

    useEffect(() => {
        if (openBusinessMap) {
            const modalBtn = document.getElementById('businesmodal-btn')
            modalBtn.click()
        }
    }, [openBusinessMap])

    useEffect(() => {
        if (approveResponceData && approveResponceData.status === STATUS_RESPONSE.SUCCESS_MSG) {
            successAlert(
                approveResponceData && approveResponceData.message,
                'success',
            )
            getRiskManagementlistDispatch()
            getIdMerchantDispatch(id)
            clearApprove()
        }
    }, [approveResponceData])

    useEffect(() => {
        if (approveResponceData && approveResponceData.message === 'Record Status Approved Successfully') {
            const approvemodalBtn = document.getElementById('approveModal')
            approvemodalBtn.click()
        }
        if (approveResponceData && approveResponceData.message === 'Record Status Rejected Successfully') {
            const rejectmodalBtn = document.getElementById('reject-model')
            rejectmodalBtn.click()
        }
        if (approveResponceData && approveResponceData.message === 'Record Status Changed as Hold') {
            const modalBtn = document.getElementById('hold-model')
            modalBtn.click()
        }
    }, [approveResponceData])

    const viewData = TransactionDashboardIdDetail && TransactionDashboardIdDetail.data && TransactionDashboardIdDetail.data ? TransactionDashboardIdDetail.data : '--'
    const dashboardData = viewData && viewData.dashboardData && viewData.dashboardData ? viewData.dashboardData && viewData.dashboardData : '--'
    const valueGauge = viewData && viewData.totalScore ? viewData.totalScore : '6'
    const distanceData = TransactionDistance && TransactionDistance.data ? TransactionDistance.data : []
    const getData = distanceData.filter(o => (o ? o : null))
    const DistanceViewData = getData && getData[0] ? getData[0] : '--'
    const TransactionIdDetail = TransactionGetById && TransactionGetById.data ? TransactionGetById.data : '--'

    const addressData = DistanceViewData && DistanceViewData.billing && DistanceViewData.billing.location && DistanceViewData.billing.location.billingAddressLocation
    const phoneData = DistanceViewData && DistanceViewData.phone && DistanceViewData.phone.location && DistanceViewData.phone.location.phoneLocation
    const ipData = DistanceViewData && DistanceViewData.ipAddress && DistanceViewData.ipAddress.location && DistanceViewData.ipAddress.location.ipLocation
    const billlingData = DistanceViewData && DistanceViewData.billing && DistanceViewData.billing.location && DistanceViewData.billing.location.billingAddressLocation
    const ShippingData = DistanceViewData && DistanceViewData.shipping && DistanceViewData.shipping.location && DistanceViewData.shipping.location.shippingPhoneLocation

    let allMarkers = [];

    if (ipData) {
        allMarkers.push({
            lat: ipData.lat,
            lng: ipData.long,
            area: "IP"
        })
    }
    if (billlingData) {
        allMarkers.push({
            lat: billlingData.lat,
            lng: billlingData.long,
            area: "BillingAdderess"
        })
    }
    if (ShippingData) {
        allMarkers.push({
            lat: ShippingData.lat,
            lng: ShippingData.long,
            area: "ShippingAdderess"
        })
    }

    return (
        <div className="card card-custom card-stretch gutter-b p-8 mb-8">
            <div className='card mb-5 mb-xl-10'>
              <div className='card-body pt-9 pb-0'>
                <div className='d-flex flex-wrap flex-sm-nowrap mb-3'>
                  <div className='flex-grow-1'>
                    <div className='row'>
                      <div className='col-lg-3 col-md-3 col-sm-3 ps-8'>
                        <h2 className='mb-2 d-flex justify-content-start text-limegreen mb-5'>Positive Factors</h2>
                        {
                          !viewData
                            ? (
                              <tr>
                                <td colSpan='6' className='text-center'>
                                  <div className='spinner-border text-primary m-5' role='status' />
                                </td>
                              </tr>
                            )
                            :
                            viewData &&
                            viewData.positive &&
                            viewData.positive.map((item, i) => {
                              return (
                                <span className='d-flex justify-content-start' key={"ee_" + i}>
                                  <i className='bi bi-check-circle-fill text-limegreen min-w-30px fsu ' />
                                  <h6 className='fw-bold fs-6 '>{item.message}</h6>
                                </span>
                              )
                            }
                            )
                        }
                      </div>
                      <div className='col-lg-5 col-md-5 col-sm-5'>
                        <div className='d-flex justify-content-center mb-4'
                        >
                          <HorizontalGauge
                            // ticks={gaugeTicks} 
                            height={70}
                            width={500}
                            min={0}
                            max={10}
                            value={valueGauge}
                          />

                        </div>
                      </div>
                      <div className='col-lg-4 col-md-4 col-sm-4'>
                        <h2 className='mb-2 d-flex justify-content-start symbol-label text-darkorange mb-5'>Negative Factors</h2>
                        {
                          !viewData
                            ? (
                              <tr>
                                <td colSpan='6' className='text-center'>
                                  <div className='spinner-border text-primary m-5' role='status' />
                                </td>
                              </tr>
                            )
                            :
                            viewData &&
                            viewData.negative &&
                            viewData.negative.map((item, i) => {
                              return (
                                <span className='d-flex justify-content-start' key={"ff_" + i}>
                                  <i className='bi bi-exclamation-triangle-fill text-darkorange min-w-30px fsu' />
                                  <h5 className='text-darkorange fw-bold fs-6 '>{item.message}</h5>
                                </span>
                              )
                            }
                            )
                        }
                      </div>
                      <div className='row mb-0'>
                        <div className='col-lg-4 col-md-4 col-sm-4' />
                        <div className='col-lg-4 col-md-4 col-sm-4'>
                        </div>
                        <div className='col-lg-4 col-md-4 col-sm-4' />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='container-fixed'>
              <h1 className='d-flex justify-content-center mb-4'>Triangulation Metrics</h1>
              <div className='row mt-8'>
                <div className='col-lg-6'>
                  <div className='card w-744px h-450px'>
                    {/*<LocationSearchModal AllMarkers={allMarkers} />*/}                    
                    { isLoaded ? <MapGoogle mapData={null} mapMarkers={allMarkers}/> : null}
                  </div>
                </div>
                <div className='col-lg-6'>

                  <div className='card card-xl-stretch mb-xl-8 mb-4' >
                    <div className='card-header border-0 '>
                      <h3 className='card-title align-items-start flex-column '>
                        <span className='card-label fw-bolder text-dark'>
                          Locations & Distances
                        </span>
                      </h3>
                    </div>
                    <div className='card-body pt-0'>
                      <div className='row'>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                          <h4 className='ml-2 text-muted'>Billing</h4>
                          <div className='row mt-4'>
                            <div className='col-lg-1 ml-2'>
                              <span className=' svg-icon-address svg me-2'>
                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-house-fill' viewBox='0 0 16 16'>
                                  <path fillRule='evenodd' d='m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z' />
                                  <path fillRule='evenodd' d='M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z' />
                                </svg>
                              </span>
                            </div>
                            <div className='col-lg-10 mb-4'>
                              <span className='ml-2 card-label fw-bolder text-dark '>
                                Measuring From
                              </span>
                              <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                                {
                                  DistanceViewData && DistanceViewData.distance && DistanceViewData.distance.billingToShippingDistance ? DistanceViewData.distance.billingToShippingDistance : '__'
                                }
                              </h5>
                            </div>
                            <div className='col-lg-1 ml-2'>
                              <span className='svg-icon svg-icon-success me-2'>
                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-telephone-fill' viewBox='0 0 16 16'>
                                  <path fillRule='evenodd' d='M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z' />
                                </svg>
                              </span>
                            </div>
                            <div className='col-lg-10 mb-4'>
                              <span className='ml-2 card-label fw-bolder text-dark '>
                                Phone
                              </span>
                              <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                                {
                                  DistanceViewData && DistanceViewData.distance && DistanceViewData.distance.phoneToShippingDistance ? DistanceViewData.distance.phoneToShippingDistance : '__'
                                }
                              </h5>
                            </div>
                            <div className='col-lg-1 ml-2'>
                              <span className='me-2 svg-icon svg-icon-2 svg-icon-info '>
                                <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                  <path opacity='0.3' d='M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z' fill='black' />
                                  <path d='M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z' fill='black' />
                                </svg>
                              </span>
                            </div>
                            <div className='col-lg-10 mb-4'>
                              <span className='ml-2 card-label fw-bolder text-dark '>
                                Ip Address
                              </span>
                              <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                                {
                                  DistanceViewData && DistanceViewData.distance && DistanceViewData.distance.phoneToIpDistance ? DistanceViewData.distance.phoneToIpDistance : '__'
                                }
                              </h5>
                            </div>
                          </div>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                          <h4 className='ml-2 text-muted'>Shipping</h4>
                          <div className='row mt-4'>
                            <div className='col-lg-1 ml-2'>
                              <span className='svg-icon-address svg me-2'>
                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-house-fill' viewBox='0 0 16 16'>
                                  <path fillRule='evenodd' d='m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z' />
                                  <path fillRule='evenodd' d='M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z' />
                                </svg>
                              </span>
                            </div>
                            <div className='col-lg-10 mb-4'>
                              <span className='ml-2 card-label fw-bolder text-dark '>
                                Address
                              </span>
                              <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                                {
                                  TransactionIdDetail && TransactionIdDetail.shippingAddress ? TransactionIdDetail.shippingAddress : '--'
                                }
                              </h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    const { dashboardStore, riskManagementlistStore, ApproveStore, TransactionDashboardStore, TransactionGetByIdStore, TransactionDistanceStore } = state
    return {
        dashboardDetails: dashboardStore && dashboardStore.dashboardDetails ? dashboardStore.dashboardDetails : {},
        riskmgmtlistdetails: riskManagementlistStore && riskManagementlistStore.riskmgmtlists ? riskManagementlistStore.riskmgmtlists : {},
        approveResponceData: ApproveStore && ApproveStore.approveResponce ? ApproveStore.approveResponce : {},
        loading: ApproveStore && ApproveStore.loading ? ApproveStore.loading : false,
        TransactionDashboardIdDetail: TransactionDashboardStore && TransactionDashboardStore.TransactionDashboardIdDetail ? TransactionDashboardStore.TransactionDashboardIdDetail : {},
        TransactionGetById: TransactionGetByIdStore && TransactionGetByIdStore.TransactionGetById ? TransactionGetByIdStore.TransactionGetById : {},
        TransactionDistance: TransactionDistanceStore && TransactionDistanceStore.TransactionDistance ? TransactionDistanceStore.TransactionDistance : {},
    }
}

const mapDispatchToProps = (dispatch) => ({
    getPrevAlertDetailsDispatch: (id) => dispatch(dashboardDetailsActions.getdashboardDetails(id)),
    getRiskManagementlistDispatch: () => dispatch(riskManagementActions.getRiskManagementlist()),
    ApprovePost: (id, params) => dispatch(ApproveActions.approve(id, params)),
    getRiskSummaryDispatch: (id) => dispatch(riskSummaryActions.getRiskSummary(id)),
    getIdMerchantDispatch: (id) => dispatch(merchantIdDetailsActions.getmerchantIdDetailsData(id)),
    clearApprove: () => dispatch(ApproveActions.clearApprove()),
    TxndashboardDispatch: (id) => dispatch(TransactiondashboardActions.getdashboardDetails(id)),
    TxnDistanceDispatch: (id) => dispatch(TransactionDistanceActions.getDistanceDetails(id)),
  // getMerchantIdDispatch: () => dispatch(merchantIdActions.getMerchantIdDetails()),

})

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDashboard)