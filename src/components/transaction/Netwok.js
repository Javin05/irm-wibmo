import { useRef, useEffect, useState, Fragment } from 'react'
import Xarrow, {useXarrow} from 'react-xarrows';
import Icofont from 'react-icofont';
import '../riskSummary/subComponent/index.css'
import { useLocation } from 'react-router-dom'
import { Scrollbars } from 'react-custom-scrollbars';

const MainColor = {
    "Phone": "#009EF7",
    "IP Address": "#F1416C",
    "Email": "#50CD89",
    "Website": "#7239EA",
    "Device": "#FFC700"
}    
const SubColor = {
    "Phone": "#D3EFFF",
    "IP Address": "#F9DBE4",
    "Email": "#BFFDDC",
    "Website": "#E1D7F9",
    "Device": "#FFF0B6"
}
const Icon = {
    "Phone": "icofont-ui-touch-phone",
    "IP Address": "icofont-eclipse",
    "Email": "icofont-email",
    "Website": "icofont-web",
    "Device": "icofont-computer"
}

function NetworkGraph(props) {
    const {
        className,
        networkData,
        TransactionIdDetail
    } = props

    const url = useLocation().pathname
    const fields = url && url.split('/')
    const id = fields && fields[3]

    const [limit, setLimit] = useState(25)
    const updateXarrow = useXarrow();
    const [path, pathType] = useState("straight"); //"smooth" | "grid" | "straight"
    const [childlineColor, setChildlineColor] = useState("#E1F0FF"); // Default Color
    const [highlight, setHighlight] = useState(null);

    const HighlightHandler = (lighter) =>{
        setHighlight(lighter);
    }

    useEffect(() => {
        window.dispatchEvent(new Event('resize')); //For Fixing
    });

    return (
        <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
                <div className='some-page-wrapper' id="triggerdiv">
                    <Scrollbars style={{ width: "100%", height: 600 }}>   
                        <div className='graph-row'>
                            <div className='column'>
                                <span>{ TransactionIdDetail && TransactionIdDetail.emailAddress ? TransactionIdDetail.emailAddress : '--' }</span>
                                <div className="box main" id="main" style={{ backgroundColor: highlight!==null? MainColor[highlight] : "#E1F0FF"}}>
                                    <Icofont icon="student-alt" style={{ color: highlight!==null? SubColor[highlight] : "#009EF7"}}  />
                                </div>
                            </div>
                            <div className='column mid-column'>
                                <span style={{ color: MainColor["Phone"] }}>
                                    Phone : {TransactionIdDetail && TransactionIdDetail?.phone}
                                </span>
                                <div className="box" style={{ backgroundColor: highlight==="Phone"? MainColor["Phone"] : SubColor["Phone"]}} id="Phone" onClick={(e)=>HighlightHandler("Phone")}>
                                    <Icofont icon="icofont-ui-touch-phone" style={{ color: highlight==="Phone"? SubColor["Phone"] : MainColor["Phone"]}} />
                                </div>

                                <span style={{ color: MainColor["IP Address"] }}>
                                    IP Address : {TransactionIdDetail && TransactionIdDetail?.ipAddress}
                                </span>
                                <div className="box" style={{ backgroundColor: highlight==="IP Address"? MainColor["IP Address"] : SubColor["IP Address"]}} id="IP Address" onClick={(e)=>HighlightHandler("IP Address")}>
                                    <Icofont icon="icofont-eclipse" style={{ color: highlight==="IP Address"? SubColor["IP Address"] : MainColor["IP Address"]}} />
                                </div>

                                <span style={{ color: MainColor["Email"] }}>
                                    Email : {TransactionIdDetail && TransactionIdDetail?.emailAddress}
                                </span>
                                <div className="box" style={{ backgroundColor: highlight==="Email"? MainColor["Email"] : SubColor["Email"]}} id="Email" onClick={(e)=>HighlightHandler("Email")}>
                                    <Icofont icon="icofont-email" style={{ color: highlight==="Email"? SubColor["Email"] : MainColor["Email"]}} />
                                </div>

                                <span style={{ color: MainColor["Website"] }}>
                                    Website : {TransactionIdDetail && TransactionIdDetail?.website}
                                </span>
                                <div className="box" style={{ backgroundColor: highlight==="Website"? MainColor["Website"] : SubColor["Website"]}} id="Website" onClick={(e)=>HighlightHandler("Website")}>
                                    <Icofont icon="icofont-web" style={{ color: highlight==="Website"? SubColor["Website"] : MainColor["Website"]}} />
                                </div>

                                <span style={{ color: MainColor["Device"] }}>
                                    Device : 
                                </span>
                                <div className="box" style={{ backgroundColor: highlight==="Device"? MainColor["Device"] : SubColor["Device"]}} id="Device" onClick={(e)=>HighlightHandler("Device")}>
                                    <Icofont icon="icofont-computer" style={{ color: highlight==="Device"? SubColor["Device"] : MainColor["Device"]}} />
                                </div>

                                <Xarrow start={'main'} end={'Phone'} path={path} color={highlight==="Phone"? MainColor["Phone"] : SubColor["Phone"]} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler("Phone")}}/>
                                <Xarrow start={'main'} end={'IP Address'} path={path} color={highlight==="IP Address"? MainColor["IP Address"] : SubColor["IP Address"]} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler("IP Address")}}/>
                                <Xarrow start={'main'} end={'Email'} path={path} color={highlight==="Email"? MainColor["Email"] : SubColor["Email"]} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler("Email")}}/>
                                <Xarrow start={'main'} end={'Website'} path={path} color={highlight==="Website"? MainColor["Website"] : SubColor["Website"]} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler("Website")}}/>
                                <Xarrow start={'main'} end={'Device'} path={path} color={highlight==="Device"? MainColor["Device"] : SubColor["Device"]} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler("Device")}}/>                        
                            </div>
                            <div className='column mt-4'>
                                { networkData && networkData?.linkData && networkData.linkData.map((dummy, index) => (
                                    <Fragment key={"Z_" + index}>
                                        <span>
                                            {
                                                (() => {
                                                    if (dummy.source==="Phone") {
                                                        return dummy.phone
                                                    } 
                                                    else if (dummy.source==="IP Address") {
                                                        return dummy.ipAddress
                                                    } 
                                                    else if (dummy.source==="Email") {
                                                        return dummy.emailAddress
                                                    } 
                                                    else if (dummy.source==="Website") {
                                                        return dummy.website
                                                    } 
                                                    else if (dummy.source==="Device") {
                                                        return dummy?.phone
                                                    }
                                                    else {
                                                        return dummy.panOwnerName
                                                    }
                                                })()
                                            }
                                        </span>
                                        <span>{dummy.panOwnerName}</span>
                                        <div 
                                            key={"X_" + index} 
                                            className="box" 
                                            id={"XYZ_" + index} 
                                            style={{ backgroundColor: highlight===dummy.source? MainColor[dummy.source] : SubColor[dummy.source], color: highlight===dummy.source? SubColor[dummy.source] : MainColor[dummy.source]}}>
                                            <span>
                                                <Icofont icon={Icon[dummy.source]} style={{color: highlight===dummy.source? SubColor[dummy.source] : MainColor[dummy.source]}}/>
                                            </span>
                                        </div>
                                        <Xarrow 
                                            key={"Y_" + index} 
                                            start={dummy.source} 
                                            end={"XYZ_" + index} 
                                            path={path} 
                                            color={ highlight === dummy.source ? MainColor[dummy.source] : SubColor[dummy.source]} 
                                            headSize={4} 
                                            strokeWidth={2} 
                                            dashness={ highlight === dummy.source ? true : false 
                                        }/>
                                    </Fragment>
                                ))}
                            </div> 
                        </div>
                    </Scrollbars> 
                </div>
            </div>
        </div>
    );
}

export default NetworkGraph;