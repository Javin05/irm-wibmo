import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { TransactionActions, clientCredFilterActions } from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { getLocalStorage, getUserPermissions } from '../../utils/helper';
import { RISKSTATUS, TXNSTATUS, SET_FILTER, CREATE_PERMISSION } from '../../utils/constants'
import { Can } from '../../theme/layout/components/can/index'

export const Add = (props) => {
  const { permissons, children, componentPermissions } = props;

  const checkArrayEquals = (item) => {
    let valid = false

    _.forEach(item, (x) => {
      if (_.includes(componentPermissions, x)) {
        valid = true
      }
    })
    return valid
  }

  return (
    <>
      {permissons && permissons.length > 0 && checkArrayEquals(permissons) ? (
        <>{children}</>
      ) : null}

    </>
  )
}

function TransactionList(props) {
  const {
    className,
    Transactionlists,
    loading,
    getTransactionDispatch,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction
  } = props
  const location = useLocation()
  const didMount = React.useRef(false);
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [searchParams, setSearchParams] = useState({});
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentRoute = url && url[1]
  const [routeShow, setRouteShow] = useState()
  const getUsersPermissions = getUserPermissions(url);

  const TransactonData = Transactionlists && Transactionlists.data ? Transactionlists.data : []
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false,
    clientId: false
  })

  useEffect(() => {
    if(location.state) {
      const data = location.state
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      const credBasedParams = {
        clientId: credBasedClientValue
      }
      const params = {
        limit: limit,
        page: 1,
        ...credBasedParams,
        ...searchParams,
      }
      Object.assign(params, data)
      const pickByParams = _.pickBy(params);
      getTransactionDispatch(pickByParams)
    } else {
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      const credBasedParams = {
        clientId: credBasedClientValue
      }
      const params = {
        limit: limit,
        page: 1,
        ...credBasedParams,
        ...searchParams,
      }
      const pickByParams = _.pickBy(params);
      getTransactionDispatch(pickByParams)
    }
  }, [location.state])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1);
      const params = {
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getTransactionDispatch(pickByParams)
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: value,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params);
    getTransactionDispatch(pickByParams);
    setActivePageNumber(1);
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: pageNumber,
      ...credBasedParams,
      ...searchParams,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params);
    setActivePageNumber(pageNumber)
    getTransactionDispatch(pickByParams)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getTransactionDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getTransactionDispatch(params)
    }
  }

  const totalPages =
    Transactionlists && Transactionlists.count
      ? Math.ceil(parseInt(Transactionlists && Transactionlists.count) / limit)
      : 1

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  });

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {Transactionlists && Transactionlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {Transactionlists.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto mt-4'>
              <div className='my-auto'>
                <SearchList />
              </div>
              {/* <Add

                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <div className='my-auto me-3'>
                  <Link
                    to='/add-transaction'
                    className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                  >
                    <span className="svg-icon svg-icon-3">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                        <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                      </svg>
                    </span>
                    Add Transaction
                  </Link>
                </div>
              </Add> */}



              <div className='my-auto me-3'>
                <Link
                  to='/add-transaction'
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                  <span className="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                      <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                    </svg>
                  </span>
                  Add Transaction
                </Link>
              </div>


            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}

                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Transaction Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("transactionStatus")}
                        >
                          <i
                            className={`bi ${sorting.transactionStatus
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>First Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${sorting.firstName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Last Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("lastName")}
                        >
                          <i
                            className={`bi ${sorting.lastName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Description</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("description")}
                        >
                          <i
                            className={`bi ${sorting.phone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Client</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("clientId")}
                        >
                          <i
                            className={`bi ${sorting.clientId
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("phone")}
                        >
                          <i
                            className={`bi ${sorting.phone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Email</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("email")}
                        >
                          <i
                            className={`bi ${sorting.email
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Currency</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("currency")}
                        >
                          <i
                            className={`bi ${sorting.currency
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>TotalAmount</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("totalAmount")}
                        >
                          <i
                            className={`bi ${sorting.totalAmount
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Quantity</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("quantity")}
                        >
                          <i
                            className={`bi ${sorting.quantity
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Department</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("department")}
                        >
                          <i
                            className={`bi ${sorting.department
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>PaymentMethod</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("paymentMethod")}
                        >
                          <i
                            className={`bi ${sorting.paymentMethod
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>CreditCardNumber</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("creditCardNumber")}
                        >
                          <i
                            className={`bi ${sorting.creditCardNumber
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>BillingAddress</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billingAddress")}
                        >
                          <i
                            className={`bi ${sorting.billingAddress
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>billingCity</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billingCity")}
                        >
                          <i
                            className={`bi ${sorting.billingCity
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>BillingState</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billingState")}
                        >
                          <i
                            className={`bi ${sorting.billingState
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>BillingCountry</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billingCountry")}
                        >
                          <i
                            className={`bi ${sorting.billingCountry
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>billingZipCode</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billingZipCode")}
                        >
                          <i
                            className={`bi ${sorting.billingZipCode
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>AVS</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("AVS")}
                        >
                          <i
                            className={`bi ${sorting.AVS
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>CVV</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("CVV")}
                        >
                          <i
                            className={`bi ${sorting.CVV
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShipToFirstName</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shipToFirstName")}
                        >
                          <i
                            className={`bi ${sorting.shipToFirstName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShipToLastName</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shipToLastName")}
                        >
                          <i
                            className={`bi ${sorting.shipToLastName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingAddress</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shippingAddress")}
                        >
                          <i
                            className={`bi ${sorting.shippingAddress
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingCity</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shippingCity")}
                        >
                          <i
                            className={`bi ${sorting.shippingCity
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingState</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shippingState")}
                        >
                          <i
                            className={`bi ${sorting.shippingState
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingCountry</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shippingCountry")}
                        >
                          <i
                            className={`bi ${sorting.shippingCountry
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>ShippingZipCode</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("shippingZipCode")}
                        >
                          <i
                            className={`bi ${sorting.shippingZipCode
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      !_.isEmpty(TransactonData)
                        ? (
                          TransactonData && TransactonData.map((transaction, i) => {
                            return (
                              <tr
                                key={"NN_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <Link to={`/transaction-dashboard/update/${transaction._id}`} className="ellipsis">
                                    TXN{transaction.transactionId}
                                  </Link>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${RISKSTATUS[transaction.riskStatus && transaction.riskStatus]}`}>
                                    {transaction.riskStatus}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <span className={`badge ${TXNSTATUS[transaction.transactionStatus && transaction.transactionStatus]}`}>
                                    {transaction.transactionStatus}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  {transaction.firstName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.lastName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.description}
                                </td>
                                <td className="ellipsis">
                                  {transaction?.clientId?.company
                                    ? transaction.clientId?.company
                                    : "--"}
                                </td>
                                <td className="ellipsis">
                                  {transaction.phone}
                                </td>
                                <td className="ellipsis">
                                  {transaction.emailAddress}
                                </td>
                                <td className="ellipsis">
                                  {transaction.currency}
                                </td>
                                <td className="ellipsis">
                                  {transaction.totalAmount}
                                </td>
                                <td className="ellipsis">
                                  {transaction.quantity}
                                </td>
                                <td className="ellipsis">
                                  {transaction.department}
                                </td>
                                <td className="ellipsis">
                                  {transaction.paymentMethod}
                                </td>
                                <td className="ellipsis">
                                  {transaction.creditCardNumber}
                                </td>
                                <td>
                                  {transaction.billingAddress}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingCity}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingState}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingCountry}
                                </td>
                                <td className="ellipsis">
                                  {transaction.billingZipCode}
                                </td>
                                <td className="ellipsis">
                                  {transaction.AVS}
                                </td>
                                <td className="ellipsis">
                                  {transaction.CVV}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shipToFirstName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shipToLastName}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingAddress}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingCity}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingState}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingCountry}
                                </td>
                                <td className="ellipsis">
                                  {transaction.shippingZipCode}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-transactions-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  forcePage={activePageNumber - 1}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { TransactionlistStore } = state;
  return {
    Transactionlists: TransactionlistStore && TransactionlistStore.Transactionlists ? TransactionlistStore.Transactionlists : [],
    loading: TransactionlistStore && TransactionlistStore.loading ? TransactionlistStore.loading : false,
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  getTransactionDispatch: (params) => dispatch(TransactionActions.getTransactionlist(params)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TransactionList);

