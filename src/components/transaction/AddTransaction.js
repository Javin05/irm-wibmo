import React, { useState, useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import { toAbsoluteUrl } from '../../theme/helpers'
import { colors } from '../../utils/constants'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  LoginActions, ownerActions, transactionActions, MonthlyActions, MccActions,
  CountryActions, StateActions, CityActions, AreaActions, TransactionAddAction, DropdownTransactionActions, AVSTransactionActions,
  CVVTransactionActions, PaymentTransactionActions, merchantIdActions
} from '../../store/actions'
import { MerchantLoginAction } from '../../store/actions'
import { USER_ERROR, REGEX, RESPONSE_STATUS, SESSION, STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import _ from 'lodash'
import { setLocalStorage } from '../../utils/helper'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import color from '../../utils/colors'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import { transactionValidation } from './validation'
import { warningAlert, confirmationAlert } from "../../utils/alerts"

function AddTransactions(props) {
  const {
    loginDispatch,
    loading,
    loginData,
    clearLogin,
    getCountryDispatch,
    getStateDispatch,
    getCityDispatch,
    getCountrys,
    getStates,
    getCitys,
    addTransactionDispatch,
    getTransacionData,
    clearAddTransaction,
    dropDownTransaction,
    CurrencyTransacionData,
    AVSTransaction,
    CVVTransaction,
    AVSTransactionlists,
    CVVTransactionlists,
    PyamentTransaction,
    PaymentTransactionlists,
    getMerchantIdDispatch,
    MerchantIDList
  } = props

  const [countryOption, setCountryOption] = useState()
  const [selectedCountryOption, setSelectedCountryOption] = useState('')
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [shippingCountryOption, setShippingCountryOption] = useState()
  const [selectedShippingCountryOption, setSelectedShippingCountryOption] = useState('')
  const [shippingStateOption, setShippingStateOption] = useState()
  const [selectedShippingStateOption, setSelectedShippingStateOption] = useState('')
  const [shippingCityOptions, setShippingCityOptions] = useState()
  const [selectedShippingCityOptions, setSelectedShippingCityOptions] = useState('')
  const [selectedtransactionDropDown, setSelectedtransactionDropDown] = useState('')
  const [transactionDropDown, setTransactionDropDown] = useState()
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState('')
  const [PaymentMethod, setPaymentMethod] = useState()
  const [selectedAVS, setSelectedAVS] = useState('')
  const [AVSMethod, setAVSMethod] = useState()
  const [selectedCVS, setSelectedCVS] = useState('')
  const [CVSMethod, setCVSMethod] = useState()
  const [selectedMerchantId, setSelectedMerchantId] = useState('')
  const [MerchantIdMethod, setMerchantIdMethod] = useState()

  const [stateQueryValue, setStateQueryValue] = useState()
  const query = useLocation().search
  const pathName = useLocation().pathname
  const [formData, setFormData] = useState({
    ipAddress: '',
    firstName: '',
    lastName: '',
    description: '',
    emailAddress: '',
    phone: '',
    currency: '',
    totalAmount: '',
    quantity: '',
    department: '',
    paymentMethod: '',
    creditCardNumber: '',
    billingAddress: '',
    billingCity: '',
    billingState: '',
    billingCountry: '',
    billingZipCode: '',
    AVS: '',
    CVV: '',
    shipToFirstName: '',
    shipToLastName: '',
    shippingAddress: '',
    shippingCity: '',
    shippingState: '',
    shippingCountry: '',
    shippingZipCode: '',
    shippingCharge: '',
    items: [
      {
        name: '',
        itemDescription: '',
        itemQuantity: '',
        price: '',
        discount: ''
      }
    ],
    promoCode: '',
    orderDiscount: '',
    binCountry: '',
    ccIssuer: '',
    ccType: '',
    ccLastFourDigits: '',
    binNumber: '',
    paymentGateway: '',
    OrderAmount: '',
    shippingCompany: '',
    shippingMethod: '',
    shippingPrice: ''
  })

  const [errors, setErrors] = useState({
    ipAddress: '',
    firstName: '',
    lastName: '',
    description: '',
    // emailAddress: '',
    emailAddress:'',
    phone: '',
    currency: '',
    totalAmount: '',
    quantity: '',
    department: '',
    paymentMethod: '',
    creditCardNumber: '',
    billingAddress: '',
    billingCity: '',
    billingState: '',
    billingCountry: '',
    billingZipCode: '',
    AVS: '',
    CVV: '',
    shipToFirstName: '',
    shipToLastName: '',
    shippingAddress: '',
    shippingCity: '',
    shippingState: '',
    shippingCountry: '',
    shippingZipCode: ''
  })
  const [showBanner, setShowBanner] = useState(false)
  const [show, setShow] = useState(false)
  const countryCodes = require('country-codes-list')
  const myCountryCodesObject = countryCodes.customList('countryCode', '[{countryCode}] {countryNameEn}: +{countryCallingCode}')

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    getCountryDispatch(params)
    dropDownTransaction()
    AVSTransaction()
    CVVTransaction()
    PyamentTransaction()
    getMerchantIdDispatch()
  }, [])


  const handleSubmit = (e) => {
    const errors = transactionValidation(formData, setErrors)
    if (_.isEmpty(errors)) {
      addTransactionDispatch(formData)
    }
  }

  const handleChange = (e, currentIndex) => {
    setErrors({ ...errors, [e.target.name]: '' })
    if (
      e.target.name === 'name' || e.target.name === "itemDescription" ||
      e.target.name === 'itemQuantity' || e.target.name === "price" || e.target.name === "discount"
    ) {
      setFormData((values) => ({
        ...values, [e.target.name]: e.target.value,
        items: formData.items.map((obj, i) =>
          i === currentIndex ?
            Object.assign(obj, {
              ...obj,
              [e.target.name]: e.target.value,
            })
            : obj
        )
      }))
    } else {
      setFormData({ ...formData, [e.target.name]: e.target.value })
    }
  }


  useEffect(() => {
    if (loginData.status === RESPONSE_STATUS.SUCCESS) {
      clearLogin()
      setLocalStorage(SESSION.TOKEN, loginData.data.token)
      setShow(true)
    } else if (loginData.status === RESPONSE_STATUS.ERROR) {
      setShowBanner(true)
      setTimeout(() => {
        setShowBanner(false)
        clearLogin()
      }, 3000)
    }
  }, [loginData])

  const showSubmit = () => {
    setShow(false)
    clearLogin()
    setFormData({
      email: '',
      phone: '',
      address: '',
      ipAddress: '',
      deviceID: ''
    })
  }
  const handleOnChange = value => {
    const phonevalue = `+${value} `
    setFormData((values) => ({ ...values, phone: phonevalue }));
  };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const handleChangeCountry = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCountryOption(selectedOption)
      setFormData(values => ({ ...values, billingCountry: selectedOption.value, billingState: '', billingCity: '' }))
      if (selectedOption.value) {
        const params = {
          countryId: selectedOption.value,
          skipPagination: 'true'
        }
        getStateDispatch(params)
      }
    } else {
      setSelectedCountryOption()
      setFormData(values => ({ ...values, billingCountry: '', billingState: '', billingCity: '' }))
      setSelectedStateOption()
      setSelectedCityOption()
    }
    setErrors({ ...errors, billingCountry: '' })
  }

  const handleChangeState = selectedOption => {
    if (selectedOption !== null) {
      setSelectedStateOption(selectedOption)
      setFormData(values => ({ ...values, billingState: selectedOption.value, billingCity: '' }))
      if (selectedOption.value) {
        const params = {
          stateId: selectedOption.value,
          skipPagination: 'true'
        }
        getCityDispatch(params)
      }
      setSelectedCityOption()
    } else {
      setSelectedStateOption()
      setFormData(values => ({ ...values, billingState: '', billingCity: '' }))
      setSelectedCityOption()
    }
    setErrors({ ...errors, billingState: '' })
  }

  const handleChangeCity = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData(values => ({ ...values, billingCity: selectedOption.value }))
    } else {
      setSelectedCityOption()
      setFormData(values => ({ ...values, billingCity: '', area: '', address: '' }))
    }
    setErrors({ ...errors, billingCity: '' })
  }

  useEffect(() => {
    const country = getDefaultOptions(getCountrys)
    setCountryOption(country)
    if (!_.isEmpty(formData.billingCountry)) {
      const selOption = _.filter(country, function (x) { if (_.includes(formData.billingCountry._id, x.value)) { return x } })
      setSelectedCountryOption(selOption)
    }
  }, [getCountrys])

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.billingState)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.billingState._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])


  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.billingCity)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.billingCity._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  const handleAddItem = () => {
    setFormData((values) => ({
      ...values,
      items: [
        ...formData.items,
        {
          name: '',
          itemDescription: '',
          itemQuantity: '',
          price: '',
          discount: ''
        }
      ]
    }))
  }

  const handleRemoveClick = currentIndex => {
    const removeObj = _.filter(formData.items, (x, i) => {
      if (currentIndex === i) { return null } else {
        return x
      }
    })
    setFormData((values) => ({
      ...values,
      items: removeObj
    }))
  }

  const handleChangeShippingCountry = selectedOption => {
    if (selectedOption !== null) {
      setSelectedShippingCountryOption(selectedOption)
      setFormData(values => ({ ...values, shippingCountry: selectedOption.value, shippingState: '', shippingCity: '' }))
      if (selectedOption.value) {
        const params = {
          country: selectedOption.value,
          skipPagination: 'true'
        }
        getStateDispatch(params)
      }
    } else {
      setSelectedShippingCountryOption()
      setFormData(values => ({ ...values, shippingCountry: '', shippingState: '', shippingCity: '' }))
      setSelectedShippingStateOption()
      setSelectedShippingCityOptions()
    }
    setErrors({ ...errors, shippingCountry: '' })
  }

  const handleShippingChangeState = selectedOption => {
    if (selectedOption !== null) {
      setSelectedShippingStateOption(selectedOption)
      setFormData(values => ({ ...values, shippingState: selectedOption.value, shippingCity: '' }))
      if (selectedOption.value) {
        const params = {
          state: selectedOption.value,
          skipPagination: 'true'
        }
        getCityDispatch(params)
      }
      setSelectedShippingCityOptions()
    } else {
      setSelectedShippingStateOption()
      setFormData(values => ({ ...values, shippingState: '', shippingCity: '' }))
      setSelectedShippingCityOptions()
    }
    setErrors({ ...errors, shippingState: '' })
  }

  const handleShippingChangeCity = selectedOption => {
    if (selectedOption !== null) {
      setSelectedShippingCityOptions(selectedOption)
      setFormData(values => ({ ...values, shippingCity: selectedOption.value }))
    } else {
      setSelectedShippingCityOptions()
      setFormData(values => ({ ...values, shippingCity: '' }))
    }
    setErrors({ ...errors, shippingCity: '' })
  }


  useEffect(() => {
    const country = getDefaultOptions(getCountrys)
    setShippingCountryOption(country)
    if (!_.isEmpty(formData.shippingCountry)) {
      const selOption = _.filter(country, function (x) { if (_.includes(formData.shippingCountry._id, x.value)) { return x } })
      setSelectedShippingCountryOption(selOption)
    }
  }, [getCountrys])

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setShippingStateOption(state)
    if (!_.isEmpty(formData.shippingState)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.shippingState._id, x.value)) { return x } })
      setSelectedShippingStateOption(selOption)
    }
  }, [getStates])


  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setShippingCityOptions(city)
    if (!_.isEmpty(formData.shippingCity)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.shippingCity._id, x.value)) { return x } })
      setSelectedShippingCityOptions(selOption)
    }
  }, [getCitys])

  useEffect(() => {
    if (getTransacionData && getTransacionData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        getTransacionData && getTransacionData.message,
        'success',
        'Back to Transaction',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      clearAddTransaction()
    } else if (getTransacionData && getTransacionData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        getTransacionData && getTransacionData.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearAddTransaction()
    }
  }, [getTransacionData])

  const onConfirm = () => {
    props.history.push('/transaction')
    clearAddTransaction()
    setFormData({
      ipAddress: '',
      firstName: '',
      lastName: '',
      description: '',
      emailAddress: '',
      phone: '',
      currency: '',
      totalAmount: '',
      quantity: '',
      department: '',
      paymentMethod: '',
      creditCardNumber: '',
      billingAddress: '',
      billingCity: '',
      billingState: '',
      billingCountry: '',
      billingZipCode: '',
      AVS: '',
      CVV: '',
      shipToFirstName: '',
      shipToLastName: '',
      shippingAddress: '',
      shippingCity: '',
      shippingState: '',
      shippingCountry: '',
      shippingZipCode: '',
      shippingCharge: '',
      items: [
        {
          name: '',
          itemDescription: '',
          itemQuantity: '',
          price: '',
          discount: ''
        }
      ]
    })
  }

  const clear = () => {
    // return () => {
    setFormData({
      ipAddress: '',
      firstName: '',
      lastName: '',
      description: '',
      emailAddress: '',
      phone: '',
      currency: '',
      totalAmount: '',
      quantity: '',
      department: '',
      paymentMethod: '',
      creditCardNumber: '',
      billingAddress: '',
      billingCity: '',
      billingState: '',
      billingCountry: '',
      billingZipCode: '',
      AVS: '',
      CVV: '',
      shipToFirstName: '',
      shipToLastName: '',
      shippingAddress: '',
      shippingCity: '',
      shippingState: '',
      shippingCountry: '',
      shippingZipCode: '',
      shippingCharge: '',
      items: [
        {
          name: '',
          itemDescription: '',
          itemQuantity: '',
          price: '',
          discount: ''
        }
      ]
    })
    // }
    clearAddTransaction()
  }

  const TransactionDropDownData = CurrencyTransacionData && CurrencyTransacionData.data ? CurrencyTransacionData.data : ''

  useEffect(() => {
    const currencyData = getDefaultDropdownOptions(CurrencyTransacionData)
    setTransactionDropDown(currencyData)
    if (!_.isEmpty(formData.currency)) {
      const selOption = _.filter(currencyData, function (x) { if (_.includes(formData.currency._id, x.value)) { return x } })
      setSelectedtransactionDropDown(selOption)
    }
  }, [CurrencyTransacionData])


  const handleChangeCurrency = selectedOption => {
    if (selectedOption !== null) {
      setSelectedtransactionDropDown(selectedOption)
      setFormData(values => ({ ...values, currency: selectedOption.value }))
    }
  }

  const getDefaultDropdownOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].currency, value: rawData[item]._id })
    }
    return defaultOptions
  }

  const getDefaultAVSOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].avs, value: rawData[item]._id })
    }
    return defaultOptions
  }

  const getDefaultCVVOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].cvv, value: rawData[item]._id })
    }
    return defaultOptions
  }

  const getDefaultPaymentOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].payment, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const payment = getDefaultPaymentOptions(PaymentTransactionlists)
    setPaymentMethod(payment)
    if (!_.isEmpty(formData.paymentMethod)) {
      const selOption = _.filter(payment, function (x) { if (_.includes(formData.paymentMethod._id, x.value)) { return x } })
      setSelectedPaymentMethod(selOption)
    }
  }, [PaymentTransactionlists])

  const handleChangePayment = selectedOption => {
    if (selectedOption !== null) {
      setSelectedPaymentMethod(selectedOption)
      setFormData(values => ({ ...values, paymentMethod: selectedOption.value }))
    }
  }

  useEffect(() => {
    const AVS = getDefaultAVSOptions(AVSTransactionlists)
    setAVSMethod(AVS)
    if (!_.isEmpty(formData.AVS)) {
      const selOption = _.filter(AVS, function (x) { if (_.includes(formData.AVS._id, x.value)) { return x } })
      setSelectedAVS(selOption)
    }
  }, [AVSTransactionlists])

  const handleChangeAVS = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAVS(selectedOption)
      setFormData(values => ({ ...values, AVS: selectedOption.value }))
    }
  }

  useEffect(() => {
    const CVV = getDefaultCVVOptions(CVVTransactionlists)
    setCVSMethod(CVV)
    if (!_.isEmpty(formData.CVV)) {
      const selOption = _.filter(CVV, function (x) { if (_.includes(formData.CVV._id, x.value)) { return x } })
      setSelectedCVS(selOption)
    }
  }, [CVVTransactionlists])

  const handleChangeCVS = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCVS(selectedOption)
      setFormData(values => ({ ...values, CVV: selectedOption.value }))
    }
  }

  const getDefaultMerchantIdOption = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].companyName, value: rawData[item]._id })
    }
    return defaultOptions
  }

  const MerchantIDListData = MerchantIDList && MerchantIDList.data ? MerchantIDList.data :'--'
  useEffect(() => {
    const MerchantValue = getDefaultMerchantIdOption(MerchantIDListData)
    setMerchantIdMethod(MerchantValue)
    if (!_.isEmpty(formData.merchantId)) {
      const selOption = _.filter(MerchantValue, function (x) { if (_.includes(formData.merchantId._id, x.value)) { return x } })
      setSelectedMerchantId(selOption)
    }
  }, [MerchantIDListData])

  const handleChangeMerchantId = selectedOption => {
    if (selectedOption !== null) {
      setSelectedMerchantId(selectedOption)
      setFormData(values => ({ ...values, merchantId: selectedOption.value }))
    }
  }
  return (
    <>

      <div
        className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed overflow-hidden'
        style={{
          backgroundImage: `url(${toAbsoluteUrl(
            '/media/illustrations/sketchy-1/14.png'
          )})`,
          backgroundColor: colors.oxfordBlue
        }}
      >
        {/* begin::Wrapper */}
        <div className='row mb-4'>
          {
            !show
              ? (
                <>
                  {/* begin::Content */}
                  <div className='d-flex flex-center flex-column flex-column-fluid p-10'>
                    {/* begin::Logo */}
                    <a href='#' className='mb-12'>
                      <img
                        alt='Logo'
                        // src={toAbsoluteUrl('/media/loginImage/MicrosoftTeams-image.png')}
                        src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
                        className='h-65px'
                      />
                    </a>
                  </div>
                  {/* end::Logo */}
                  <div className='card card-xl-stretch'>
                    <div className='row mt-10'>
                      <div className='col-lg-4'>
                        <h1 className='text-dark mb-3 d-flex justify-content-center'>Order Details</h1>
                      </div>
                      <div className='col-lg-4'>
                        <h1 className='text-dark mb-3 d-flex justify-content-center'>Billing Details</h1>
                      </div>
                      <div className='col-lg-4'>
                        <h1 className='text-dark mb-3 d-flex justify-content-center'>Shipping Details</h1>
                      </div>
                    </div>
                    {/* <div className='card-header border-0 d-flext  mt-4'>
                    </div> */}
                    <div className='card-body pt-0'>
                      <div className='row'>
                        <div className='col-lg-4 col-md-3 col-sm-3 d-flex justify-content-end'>
                          <div className='w-lg-450px bg-white rounded p-10 p-lg-15'>
                            <>
                              {/* begin::Heading */}
                              {/* <div className='text-center mb-10'>
                                  <h1 className='text-dark mb-3'>Business Information</h1>
                                </div> */}
                              {/* end::Heading */}

                              {/* begin::Form group */}
                              <>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>First Name</label>
                                  <input
                                    placeholder='First Name'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.firstName && errors.firstName },
                                      {
                                        'is-valid': formData.firstName && !errors.firstName
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    onKeyPress={(e) => {
                                      if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    type='text'
                                    name='firstName'
                                    autoComplete='off'
                                    maxLength={10}
                                  />
                                  {errors.firstName && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.firstName}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Last Name</label>
                                  <input
                                    placeholder='Last Name'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.lastName && errors.lastName },
                                      {
                                        'is-valid': formData.lastName && !errors.lastName
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    onKeyPress={(e) => {
                                      if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    type='text'
                                    name='lastName'
                                    autoComplete='off'
                                    maxLength={10}
                                  />
                                  {errors.lastName && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.lastName}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10 react-tel-input'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Phone</label>
                                  <PhoneInput
                                    inputProps={{
                                      required: true,
                                      autoFocus: true
                                    }}
                                    country={'in'}
                                    className='react-tel-input'
                                    // value={formData.individualPhone || ''}
                                    onChange={(e) => handleOnChange(e)}
                                    searchPlaceholder='Phone Number'
                                  />
                                  {errors.phone && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.phone}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Personal Email</label>
                                  <input
                                    placeholder='Personal Email'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.emailAddress && errors.emailAddress },
                                      {
                                        'is-valid': formData.emailAddress && !errors.emailAddress
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='emailAddress'
                                    name='emailAddress'
                                    autoComplete='off'
                                  />
                                  {errors.emailAddress && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.emailAddress}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Description</label>
                                  <input
                                    placeholder='Description'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.description && errors.description },
                                      {
                                        'is-valid': formData.description && !errors.description
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='description'
                                    autoComplete='off'
                                  />
                                  {errors.description && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.description}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>IP Address</label>
                                  <textarea
                                    placeholder='Ip Address'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.ipAddress && errors.ipAddress },
                                      {
                                        'is-valid': formData.ipAddress && !errors.ipAddress
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='ipAddress'
                                    name='ipAddress'
                                    autoComplete='off'
                                  // onKeyPress={(e) => {
                                  //   if (!/[0-9{1-3}.]/.test(e.key)) {
                                  //     e.preventDefault()
                                  //   }
                                  // }}
                                  />
                                  {errors.ipAddress && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.ipAddress}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Currency</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='AppUserId'
                                    className='select2'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeCurrency}
                                    options={transactionDropDown}
                                    value={selectedtransactionDropDown}
                                    isDisabled={!transactionDropDown}
                                  />
                                  {errors.currency && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.currency}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Total Amount</label>
                                  <input
                                    placeholder='Total Amount'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.totalAmount && errors.totalAmount },
                                      {
                                        'is-valid': formData.totalAmount && !errors.totalAmount
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    type='text'
                                    name='totalAmount'
                                    autoComplete='off'
                                    maxLength={10}
                                  />
                                  {errors.totalAmount && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.totalAmount}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Department</label>
                                  <input
                                    placeholder='Department'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.department && errors.department },
                                      {
                                        'is-valid': formData.department && !errors.department
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='department'
                                    name='department'
                                    autoComplete='off'
                                  />
                                  {errors.department && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.department}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Payment Method</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='paymentMethod'
                                    // className={clsx(
                                    //   'form-control form-control-lg form-control-solid',
                                    //   { 'is-invalid': formData.paymentMethod && errors.paymentMethod },
                                    //   {
                                    //     'is-valid': formData.paymentMethod && !errors.paymentMethod
                                    //   }
                                    // )}
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangePayment}
                                    options={PaymentMethod}
                                    value={selectedPaymentMethod}
                                    isDisabled={!PaymentMethod}
                                  />
                                  {errors.paymentMethod && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.paymentMethod}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Credit Card Number</label>
                                  <input
                                    placeholder='Credit Card Number'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.creditCardNumber && errors.creditCardNumber },
                                      {
                                        'is-valid': formData.creditCardNumber && !errors.creditCardNumber
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    name='creditCardNumber'
                                    autoComplete='off'
                                  />
                                  {errors.creditCardNumber && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.creditCardNumber}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Quantity</label>
                                  <input
                                    placeholder='Quantity'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.quantity && errors.quantity },
                                      {
                                        'is-valid': formData.quantity && !errors.quantity
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='quantity'
                                    autoComplete='off'
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                  />
                                  {errors.quantity && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.quantity}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>AVS</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='AVS'
                                    // className={clsx(
                                    //   'form-control form-control-lg form-control-solid',
                                    //   { 'is-invalid': formData.paymentMethod && errors.paymentMethod },
                                    //   {
                                    //     'is-valid': formData.paymentMethod && !errors.paymentMethod
                                    //   }
                                    // )}
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeAVS}
                                    options={AVSMethod}
                                    value={selectedAVS}
                                    isDisabled={!AVSMethod}
                                  />
                                  {errors.AVS && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.AVS}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>CVV</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='CVV'
                                    // className={clsx(
                                    //   'form-control form-control-lg form-control-solid',
                                    //   { 'is-invalid': formData.paymentMethod && errors.paymentMethod },
                                    //   {
                                    //     'is-valid': formData.paymentMethod && !errors.paymentMethod
                                    //   }
                                    // )}
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeCVS}
                                    options={CVSMethod}
                                    value={selectedCVS}
                                    isDisabled={!CVSMethod}
                                  />
                                  {errors.CVV && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.CVV}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Promo Code</label>
                                  <input
                                    placeholder='Promo Code'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.promoCode && errors.promoCode },
                                      {
                                        'is-valid': formData.promoCode && !errors.promoCode
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='promoCode'
                                    autoComplete='off'
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                  />
                                  {errors.promoCode && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.promoCode}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Bin Country</label>
                                  <input
                                    placeholder='Bin Country'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.binCountry && errors.binCountry },
                                      {
                                        'is-valid': formData.binCountry && !errors.binCountry
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='binCountry'
                                    autoComplete='off'
                                  />
                                  {errors.binCountry && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.binCountry}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>CC Issuer</label>
                                  <input
                                    placeholder='CC Issuer'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.ccIssuer && errors.ccIssuer },
                                      {
                                        'is-valid': formData.ccIssuer && !errors.ccIssuer
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='ccIssuer'
                                    autoComplete='off'
                                  />
                                  {errors.ccIssuer && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.ccIssuer}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>CC Type</label>
                                  <input
                                    placeholder='CC Type'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.ccType && errors.ccType },
                                      {
                                        'is-valid': formData.ccType && !errors.ccType
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='ccType'
                                    autoComplete='off'
                                  />
                                  {errors.ccType && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.ccType}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>CC LastFourDigits</label>
                                  <input
                                    placeholder='CC LastFourDigits'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.ccLastFourDigits && errors.ccLastFourDigits },
                                      {
                                        'is-valid': formData.ccLastFourDigits && !errors.ccLastFourDigits
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='ccLastFourDigits'
                                    autoComplete='off'
                                    maxLength={4}
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                  />
                                  {errors.ccLastFourDigits && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.ccLastFourDigits}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Bin Number</label>
                                  <input
                                    placeholder='Bin Number'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.binNumber && errors.binNumber },
                                      {
                                        'is-valid': formData.binNumber && !errors.binNumber
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='binNumber'
                                    autoComplete='off'
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                  />
                                  {errors.binNumber && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.binNumber}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Payment Gateway</label>
                                  <input
                                    placeholder='Payment Gateway'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.paymentGateway && errors.paymentGateway },
                                      {
                                        'is-valid': formData.paymentGateway && !errors.paymentGateway
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='paymentGateway'
                                    autoComplete='off'
                                  />
                                  {errors.paymentGateway && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.paymentGateway}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Order Amount</label>
                                  <input
                                    placeholder='Order Amount'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.OrderAmount && errors.OrderAmount },
                                      {
                                        'is-valid': formData.OrderAmount && !errors.OrderAmount
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='OrderAmount'
                                    autoComplete='off'
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                  />
                                  {errors.OrderAmount && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.OrderAmount}</span>
                                    </div>
                                  )}
                                </div>
                                {/* end::Action */}
                              </>
                            </>
                          </div>
                        </div>
                        <div className='col-lg-4 d-flex justify-content-end'>
                          <div className='w-lg-450px bg-white rounded p-10 p-lg-15'>
                            <>
                              {/* begin::Banner */}
                              {/* end::Banner */}
                              <>
                                {/* begin::Heading */}
                                {/* <div className='text-center mb-10'>
                                  <h1 className='text-dark mb-3'>Business Information</h1>
                                </div> */}
                                {/* end::Heading */}

                                {/* begin::Form group */}
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Billing Address</label>
                                  <textarea
                                    placeholder='Billing Address'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.billingAddress && errors.billingAddress },
                                      {
                                        'is-valid': formData.billingAddress && !errors.billingAddress
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    // onKeyPress={(e) => {
                                    //   if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                    //     e.preventDefault()
                                    //   }
                                    // }}
                                    type='text'
                                    name='billingAddress'
                                    autoComplete='off'
                                  />
                                  {errors.billingAddress && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.billingAddress}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Billing Country</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='country'
                                    className='basic-single'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeCountry}
                                    options={countryOption}
                                    value={selectedCountryOption}
                                    isDisabled={!countryOption}
                                  />
                                  {errors.billingCountry && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.billingCountry}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Billing State</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='state'
                                    className='basic-single'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeState}
                                    options={stateOption}
                                    value={selectedStateOption}
                                    isDisabled={!stateOption}
                                  />
                                  {errors.billingState && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.billingState}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Billing City</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='city'
                                    className='basic-single'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeCity}
                                    options={cityOptions}
                                    value={selectedCityOption}
                                    isDisabled={!cityOptions}
                                  />

                                  {errors.billingCity && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.billingCity}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Billing Zip Code</label>
                                  <input
                                    placeholder='Billing Zip Code'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.billingZipCode && errors.billingZipCode },
                                      {
                                        'is-valid': formData.billingZipCode && !errors.billingZipCode
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    type='text'
                                    name='billingZipCode'
                                    autoComplete='off'
                                  />
                                  {errors.billingZipCode && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.billingZipCode}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Company</label>
                                  <input
                                    placeholder='Shipping Company'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shippingCompany && errors.shippingCompany },
                                      {
                                        'is-valid': formData.shippingCompany && !errors.shippingCompany
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shippingCompany'
                                    autoComplete='off'
                                  />
                                  {errors.shippingCompany && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingCompany}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Method</label>
                                  <input
                                    placeholder='Shipping Method'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shippingMethod && errors.shippingMethod },
                                      {
                                        'is-valid': formData.shippingMethod && !errors.shippingMethod
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shippingMethod'
                                    autoComplete='off'
                                  />
                                  {errors.shippingMethod && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingMethod}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Price</label>
                                  <input
                                    placeholder='Shipping Price'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shippingPrice && errors.shippingPrice },
                                      {
                                        'is-valid': formData.shippingPrice && !errors.shippingPrice
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    onKeyPress={(e) => {
                                      if (!/[0-9{1-3}.]/.test(e.key)) {
                                        e.preventDefault()
                                      }
                                    }}
                                    type='text'
                                    name='shippingPrice'
                                    autoComplete='off'
                                  />
                                  {errors.shippingPrice && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingPrice}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Merchant ID</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='merchantId'
                                    // className={clsx(
                                    //   'form-control form-control-lg form-control-solid',
                                    //   { 'is-invalid': formData.paymentMethod && errors.paymentMethod },
                                    //   {
                                    //     'is-valid': formData.paymentMethod && !errors.paymentMethod
                                    //   }
                                    // )}
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeMerchantId}
                                    options={MerchantIdMethod}
                                    value={selectedMerchantId}
                                    isDisabled={!MerchantIdMethod}
                                  />
                                  {errors.merchantId && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.merchantId}</span>
                                    </div>
                                  )}
                                </div>



                                {/* end::Action */}
                              </>
                            </>
                          </div>
                        </div>
                        <div className='col-lg-4 d-flex justify-content-start'>
                          <div className='w-lg-500px bg-white rounded p-10 p-lg-15'>

                            <>
                              {/* begin::Banner */}
                              {showBanner &&
                                <div className='mb-10 bg-light-info p-8 rounded'>
                                  <div className='text-center text-danger'>
                                    {loginData.message}
                                  </div>
                                </div>}
                              {/* end::Banner */}
                              <>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Ship To First Name</label>
                                  <input
                                    placeholder='Ship To First Name'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shipToFirstName && errors.shipToFirstName },
                                      {
                                        'is-valid': formData.shipToFirstName && !errors.shipToFirstName
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shipToFirstName'
                                    autoComplete='off'
                                  />
                                  {errors.shipToFirstName && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shipToFirstName}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Ship To Last Name</label>
                                  <input
                                    placeholder='Ship To Last Name'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shipToLastName && errors.shipToLastName },
                                      {
                                        'is-valid': formData.shipToLastName && !errors.shipToLastName
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shipToLastName'
                                    autoComplete='off'
                                  />
                                  {errors.shipToLastName && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shipToLastName}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Address</label>
                                  <textarea
                                    placeholder='Shipping Address'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shippingAddress && errors.shippingAddress },
                                      {
                                        'is-valid': formData.shippingAddress && !errors.shippingAddress
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shippingAddress'
                                    autoComplete='off'
                                  />
                                  {errors.shippingAddress && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingAddress}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Country</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='shippingCountry'
                                    className='basic-single'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleChangeShippingCountry}
                                    options={shippingCountryOption}
                                    value={selectedShippingCountryOption}
                                    isDisabled={!shippingCountryOption}
                                  />
                                  {errors.shippingCountry && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingCountry}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping State</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='shippingState'
                                    className='basic-single'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleShippingChangeState}
                                    options={shippingStateOption}
                                    value={selectedShippingStateOption}
                                    isDisabled={!shippingStateOption}
                                  />
                                  {errors.shippingState && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingState}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping City</label>
                                  <ReactSelect
                                    styles={customStyles}
                                    isMulti={false}
                                    name='shippingCountry'
                                    className='basic-single'
                                    classNamePrefix='select'
                                    handleChangeReactSelect={handleShippingChangeCity}
                                    options={shippingCityOptions}
                                    value={selectedShippingCityOptions}
                                    isDisabled={!shippingCityOptions}
                                  />
                                  {errors.shippingCity && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingCity}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Zip Code</label>
                                  <input
                                    placeholder='Shipping Zip Code'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shippingZipCode && errors.shippingZipCode },
                                      {
                                        'is-valid': formData.shippingZipCode && !errors.shippingZipCode
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shippingZipCode'
                                    autoComplete='off'
                                  />
                                  {errors.shippingZipCode && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingZipCode}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Shipping Charge</label>
                                  <input
                                    placeholder='Shipping Charge'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.shippingCharge && errors.shippingCharge },
                                      {
                                        'is-valid': formData.shippingCharge && !errors.shippingCharge
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='shippingCharge'
                                    autoComplete='off'
                                  />
                                  {errors.shippingCharge && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.shippingCharge}</span>
                                    </div>
                                  )}
                                </div>
                                <div className='fv-row mb-10'>
                                  <label className='form-label fs-6 fw-bolder text-dark'>Order Discount</label>
                                  <input
                                    placeholder='Order Discount'
                                    className={clsx(
                                      'form-control form-control-lg form-control-solid',
                                      { 'is-invalid': formData.orderDiscount && errors.orderDiscount },
                                      {
                                        'is-valid': formData.orderDiscount && !errors.orderDiscount
                                      }
                                    )}
                                    onChange={(e) => handleChange(e)}
                                    type='text'
                                    name='orderDiscount'
                                    autoComplete='off'
                                  />
                                  {/* {errors.orderDiscount && (
                                    <div className='fv-plugins-message-container text-danger'>
                                      <span role='alert text-danger'>{errors.orderDiscount}</span>
                                    </div>
                                  )} */}
                                </div>
                              </>
                            </>
                          </div>
                        </div>
                        <div className='form-group row mb-4'>
                          <div>
                            <h1 className='text-dark mb-3 d-flex justify-content-center'>Item</h1>
                          </div>
                          {
                            formData.items && formData.items.map((item, i) => (
                              <div className='row mb-8' key={"kk_" + i}>
                                <div className='col-lg-2'>
                                  <div className='col-md-10 mx-10 '>
                                    <label className='col-form-label col-md-4 text-lg-start'>Name:</label>
                                    <input
                                      onChange={(e) => handleChange(e, i)}
                                      type="text"
                                      name="name"
                                      value={item.name || ''}
                                      className="form-control form-control-solid"
                                      placeholder="Name"
                                      onKeyPress={(e) => {
                                        if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                                          e.preventDefault()
                                        }
                                      }}
                                    />
                                    {/* {errors && errors.name && (
                                      <div className='fv-plugins-message-container text-danger'>
                                        <div className='fv-help-block'>
                                          <span role='alert'>{errors && errors.name}</span>
                                        </div>
                                      </div>
                                    )} */}
                                  </div>
                                </div>
                                <div className='col-lg-2'>
                                  <div className='col-md-10 mx-10 '>
                                    <label className='col-form-label col-md-4 text-lg-start'>Description:</label>
                                    <input
                                      onChange={(e) => handleChange(e, i)}
                                      type="text"
                                      name="itemDescription"
                                      value={item.itemDescription || ''}
                                      className="form-control form-control-solid"
                                      placeholder="Description"
                                    />
                                    {/* {errors && errors.description && (
                                      <div className='fv-plugins-message-container text-danger'>
                                        <div className='fv-help-block'>
                                          <span role='alert'>{errors && errors.description}</span>
                                        </div>
                                      </div>
                                    )} */}
                                  </div>
                                </div>
                                <div className='col-lg-2'>
                                  <div className='col-md-10 mx-10 '>
                                    <label className='col-form-label col-md-4 text-lg-start'>Quantity:</label>
                                    <input
                                      onChange={(e) => handleChange(e, i)}
                                      type="text"
                                      name="itemQuantity"
                                      value={item.itemQuantity || ''}
                                      className="form-control form-control-solid"
                                      placeholder="Quantity"
                                      onKeyPress={(e) => {
                                        if (!/[0-9{1-3}.]/.test(e.key)) {
                                          e.preventDefault()
                                        }
                                      }}
                                    />
                                    {/* {errors && errors.itemQuantity && (
                                      <div className='fv-plugins-message-container text-danger'>
                                        <div className='fv-help-block'>
                                          <span role='alert'>{errors && errors.itemQuantity}</span>
                                        </div>
                                      </div>
                                    )} */}
                                  </div>
                                </div>
                                <div className='col-lg-2'>
                                  <div className='col-md-10 mx-10 '>
                                    <label className='col-form-label col-md-4 text-lg-start'>Price:</label>
                                    <input
                                      onChange={(e) => handleChange(e, i)}
                                      type="text"
                                      name="price"
                                      value={item.price || ''}
                                      className="form-control form-control-solid"
                                      placeholder="Price"
                                      onKeyPress={(e) => {
                                        if (!/[0-9{1-3}.]/.test(e.key)) {
                                          e.preventDefault()
                                        }
                                      }}
                                    />
                                    {/* {errors && errors.price && (
                                      <div className='fv-plugins-message-container text-danger'>
                                        <div className='fv-help-block'>
                                          <span role='alert'>{errors && errors.price}</span>
                                        </div>
                                      </div>
                                    )} */}
                                  </div>
                                </div>
                                <div className='col-lg-2'>
                                  <div className='col-md-10 mx-10 '>
                                    <label className='col-form-label col-md-4 text-lg-start'>Discount:</label>
                                    <input
                                      onChange={(e) => handleChange(e, i)}
                                      type="text"
                                      name="discount"
                                      value={item.discount || ''}
                                      className="form-control form-control-solid"
                                      placeholder="Discount"
                                    />
                                    {/* {errors && errors.discount && (
                                      <div className='fv-plugins-message-container text-danger'>
                                        <div className='fv-help-block'>
                                          <span role='alert'>{errors && errors.discount}</span>
                                        </div>
                                      </div>
                                    )} */}
                                  </div>
                                </div>
                                <div className='col-lg-2'>
                                  {
                                    i === 0 ? (
                                      <div className='col-md-10 mx-10 '>
                                        <button
                                          type='button'
                                          className='btn btn-sm btn-light-primary mt-14 text-lg-start '
                                          onClick={() => handleAddItem()}
                                        >
                                          <span className='indicator-label'>Add Item</span>
                                        </button>
                                      </div>
                                    ) : (
                                      <div className='col-md-10 mx-10 '>
                                        <button
                                          type='button'
                                          className='btn btn-sm btn-warning mt-14 text-lg-start '
                                          onClick={() => handleRemoveClick(i)}
                                        >
                                          <span className='indicator-label'>Remove Item</span>
                                        </button>
                                      </div>
                                    )
                                  }
                                </div>
                              </div>
                            ))
                          }
                          <div className='col-lg-6' />
                          <div className='col-lg-6'>
                            <div className='col-lg-11'>
                              <button
                                type='button'
                                className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                                onClick={(e) => handleSubmit(e)}
                                disabled={loading}
                              >
                                {!loading && <span className='indicator-label'>Submit</span>}
                                {loading && (
                                  <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                              <Link
                                to='/transactions'
                                disabled={loading}
                                className='btn btn-sm btn-light-danger m-2 fa-pull-right close'

                              >
                                Back To Transaction List
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className='row mt-50 m-top-7'>
                    <div className='col-lg-5' />
                    <div className='col-lg-6 ' style={{ marginTop: '14%' }}>
                      <div className='card w-450px '
                      >
                        <div className='text-center text-success fw-bolder fs-3 mb-4 mt-4'>
                          Thanks For Submitting The Request. We Are Processing your Request.
                        </div>
                        <div className='text-center mb-4'>
                          <div className='row'>
                            <div className='col-sm-4 col-md-4 col-lg-4' />
                            <div className='col-sm-3 col-md-3 col-lg-3'>
                              <button
                                type='button'
                                id='kt_sign_in_submit'
                                className='btn btn-sm btn-info w-100'
                                onClick={() => showSubmit()}
                                disabled={loading}
                              >
                                {!loading && <span className='indicator-label'>Back</span>}
                                {loading && (
                                  <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </>
              )
          }
        </div>
        {/* end::Wrapper */}
        {/* end::Content */}
      </div>

    </>
  )
}

const mapStateToProps = state => {
  const { merchantLoginStore, ownerlistStore, transactionlistStore, MonthlylistStore, McclistStore,
    CountrylistStore, StatelistStore, CitylistStore, TransactionAddStore, DropDownTransactionStore, AVSTransactionStore,
    CVVTransactionStore, PaymentTransactionStore, MerchantIDStore
  } = state

  return {
    loginData: merchantLoginStore && merchantLoginStore.login ? merchantLoginStore.login : {},
    ownerlistsData: ownerlistStore && ownerlistStore.ownerlists ? ownerlistStore.ownerlists : '',
    transactionListData: transactionlistStore && transactionlistStore.transactionList ? transactionlistStore.transactionList : '',
    monthlyListData: MonthlylistStore && MonthlylistStore.MonthlyList ? MonthlylistStore.MonthlyList : '',
    mccListData: McclistStore && McclistStore.MccList ? McclistStore.MccList : '',
    loading: TransactionAddStore && TransactionAddStore.loading ? TransactionAddStore.loading : false,

    getCountrys: CountrylistStore && CountrylistStore.Countrylists ? CountrylistStore.Countrylists : {},
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    getTransacionData: TransactionAddStore && TransactionAddStore.TransactionAdd ? TransactionAddStore.TransactionAdd : {},
    CurrencyTransacionData: DropDownTransactionStore && DropDownTransactionStore.DropDownTransactionlists ? DropDownTransactionStore.DropDownTransactionlists : '',
    AVSTransactionlists: AVSTransactionStore && AVSTransactionStore.AVSTransactionlists ? AVSTransactionStore.AVSTransactionlists : '',
    CVVTransactionlists: CVVTransactionStore && CVVTransactionStore.CVVTransactionlists ? CVVTransactionStore.CVVTransactionlists : '',
    PaymentTransactionlists: PaymentTransactionStore && PaymentTransactionStore.PaymentTransactionlists ? PaymentTransactionStore.PaymentTransactionlists : '',
    MerchantIDList: MerchantIDStore && MerchantIDStore.MerchantID ? MerchantIDStore.MerchantID : '',

  }
}

const mapDispatchToProps = dispatch => ({
  loginDispatch: (data) => dispatch(MerchantLoginAction.login(data)),
  getOwnerDispatch: () => dispatch(ownerActions.getOwnerlist()),
  getTransactionDispatch: () => dispatch(transactionActions.gettransactionlist()),
  getMonthlyDispatch: () => dispatch(MonthlyActions.getMonthlylist()),
  getMccDispatch: () => dispatch(MccActions.getMcclist()),
  clearLogin: () => dispatch(LoginActions.clearLogin()),



  getCountryDispatch: (params) => dispatch(CountryActions.getCountrylist(params)),
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  getAreaDispatch: () => dispatch(AreaActions.getArealist()),
  addTransactionDispatch: (data) => dispatch(TransactionAddAction.TransactionAdd(data)),
  clearAddTransaction: () => dispatch(TransactionAddAction.clearTransaction()),
  dropDownTransaction: () => dispatch(DropdownTransactionActions.getDropDownTransactionlist()),
  AVSTransaction: () => dispatch(AVSTransactionActions.getAVSTransactionlist()),
  CVVTransaction: () => dispatch(CVVTransactionActions.getCVVTransactionlist()),
  PyamentTransaction: () => dispatch(PaymentTransactionActions.getPaymentTransactionlist()),
  getMerchantIdDispatch: () => dispatch(merchantIdActions.getMerchantIdDetails()),

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddTransactions)
