import React, { useEffect, useState, Fragment } from 'react'
import { connect } from 'react-redux'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import { DATE, AVSSTATUS, CVSSTATUS, USER_ERROR, REGEX, STATUS_RESPONSE, SESSION, SWEET_ALERT_MSG, RISKSTATUSCOLOR, RISKSTATUS } from '../../utils/constants'
import { Link, useLocation } from 'react-router-dom'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Nav from 'react-bootstrap/Nav'
import ProgressBar from 'react-bootstrap/ProgressBar'
import Table from 'react-bootstrap/Table'
import Accordion from 'react-bootstrap/Accordion'
import { toAbsoluteUrl } from '../../theme/helpers'
import {
  merchantDetailsActions,
  TransactionGetByIdActions,
  dashboardDetailsActions,
  riskSummaryActions,
  LinkAnlyticsTransactionActions,
  ApproveTXnActions,
  TransactionActions
} from '../../store/actions'
import './index.scss'
import TransactionDashboard from './transactionDashboard'
import NetworkGraph from './Netwok'
import Icofont from 'react-icofont';
import { KTSVG } from '../../theme/helpers'
import clsx from 'clsx'
import _ from 'lodash'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import ChangeStatus from '../amlQueue/TxnStatusChange'

function RiskDetails(props) {
  const {
    getRiskSummarys,
    dashboardDetails,
    merchantIddetails,
    TxngetbyIdDispatch,
    TransactionGetById,
    loading,
    riskmgmtlistdetails,
    getRiskManagementlistDispatch,
    ApprovePost,
    linkAnalytics,
    LinkAnlyticslists,
    TransactionDashboardIdDetail,
    approveResponceData,
    getTransactionDispatch,
    clearApproveTXN
  } = props


  const [key, setKey] = useState('overview');
  const [active, setActive] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [rejectValue, setRejectValue] = useState()
  const [formData, setFormData] = useState({
    message: ''
  })
  const [holdFormData, setHoldFormData] = useState({
    riskStatus: 'HOLD',
    reason: ''
  })
  const [rejectFormData, setRejectFormData] = useState({
    riskStatus: 'REJECTED',
    reason: '',
    rejectType: '',
    rejectMoreValue: ''

  })
  const [errors, setErrors] = useState({
    reason: '',
  })

  const [approveFormData, setApproveFormData] = useState({
    riskStatus: 'APPROVED',
  })

  useEffect(() => {
    if (currentId) {
      TxngetbyIdDispatch(currentId)
    }
    const params = {
      caseId: currentId,
    }
    linkAnalytics(params)
  }, [currentId])

  useEffect(() => {
    if (approveResponceData && approveResponceData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        approveResponceData && approveResponceData.message,
        'success',
      )
      getTransactionDispatch()
      TxngetbyIdDispatch(currentId)
      clearApproveTXN()
    }
  }, [approveResponceData])

  useEffect(() => {
    if (approveResponceData && approveResponceData.message === 'Record Status Approved Successfully') {
      const approvemodalBtn = document.getElementById('approveModal')
      approvemodalBtn.click()
    }
    if (approveResponceData && approveResponceData.message === 'Record Status Rejected Successfully') {
      const rejectmodalBtn = document.getElementById('reject-model')
      rejectmodalBtn.click()
    }
    if (approveResponceData && approveResponceData.message === 'Record Status Changed as Hold') {
      const modalBtn = document.getElementById('hold-model')
      modalBtn.click()
    }
  }, [approveResponceData])

  const approveSubmit = () => {
    ApprovePost(currentId, approveFormData)
  }

  const onConfirmHold = () => {
    ApprovePost(currentId, holdFormData)
  }

  const holdSubmit = () => {
    const errors = {}
    if (_.isEmpty(holdFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.HOLD,
        'warning',
        'Yes',
        'No',
        () => { onConfirmHold() },
        () => { }
      )
    }
  }

  const onConfirmReject = () => {
    ApprovePost(currentId, rejectFormData)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        'warning',
        'Yes',
        'No',
        () => { onConfirmReject() },
        () => { }
      )
    }
  }

  const handleChange = (e) => {
    setHoldFormData({ ...holdFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })

  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
    setRejectValue(e.target.value)
  }

  const IDIP_Address = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.ipAddress && dashboardDetails.data.dashboardData.ipAddress.data[0] ? dashboardDetails.data.dashboardData.ipAddress.data[0] : '--'
  const viewData = getRiskSummarys && getRiskSummarys && getRiskSummarys.data ? getRiskSummarys.data : []
  const getData = viewData.filter(o => (o ? o : null))
  const viewIpAddress = getData && getData[0] && getData[0].ipCheck ? getData[0].ipCheck : '--'
  const merchantIdDetails = merchantIddetails && merchantIddetails.data ? merchantIddetails.data : '--'
  const TransactionIdDetail = TransactionGetById && TransactionGetById.data ? TransactionGetById.data : '--'
  const dashboardDetail = TransactionDashboardIdDetail && TransactionDashboardIdDetail.data ? TransactionDashboardIdDetail.data : '--'
  const dashboarDatas = dashboardDetail && dashboardDetail.dashboardData ? dashboardDetail.dashboardData : '--'
  const allApiDatas = dashboardDetail && dashboardDetail.allApiData ? dashboardDetail.allApiData : []
  const ApiDataOption = _.filter(allApiDatas, function (x) { return x })
  const IpGeoOption = ApiDataOption && ApiDataOption[0] ? ApiDataOption[0] : '--'
  const scoreData = dashboardDetail && dashboardDetail.transactionScoreData &&
    dashboardDetail.transactionScoreData
    ? dashboardDetail.transactionScoreData : '--'
  const allApiData = dashboardDetail && dashboardDetail.allApiData && dashboardDetail.allApiData.map((item, i) => { return item.ipGeo })

  const containerStyle = {
    width: '15%',
    height: '21%'
  }

  return (
    <>
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="hold-model"
        data-target='#holdModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#rejectModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="approve-model"
        data-target='#approveModal'
        onClick={() => { }}
      />
      <div
        className='modal fade'
        id='holdModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'cornflowerblue' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Hold
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 '>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={holdFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => holdSubmit()}
                              disabled={loading}
                            >
                              <span className='indicator-label'>Submit</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='approveModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Are You Sure Want to Approve This Users Risk Analytics Report ?</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'cornflowerblue' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-9 col-md-8 col-sm-8 mb-3'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-warning m-2 fa-pull-right close'
                          data-dismiss='modal'
                        >
                          No
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                          onClick={() => approveSubmit()}
                          data-dismiss='modal'
                        >
                          Yes
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='rejectModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              // onClick={() => { setShow(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'cornflowerblue' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Reject :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => rejectChange(e)}
                            autoComplete='off'
                            value={rejectFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Type :
                        </label>
                        <div className='col-lg-8 col-md-8 col-sm-8'>
                          <select
                            className='form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => rejectChange(e)}
                            value={rejectFormData.rejectType || ''}
                            name='rejectType'
                          >
                            <option value='Suspect Fraud'>Suspect Fraud</option>
                            <option value=' Fraud'>Fraud</option>
                            <option value='Spam'>Spam</option>
                            <option value='Fake'>Fake</option>
                            <option value='Funneling'>Funneling</option>
                            <option value='Other'>Other</option>
                          </select>
                          {
                            rejectFormData.rejectType === 'More' ? (
                              <>
                                <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label mt-2'>
                                  please enter your message :
                                </label>
                                <textarea
                                  name='rejectMoreValue'
                                  type='text'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.rejectMoreValue && errors.rejectMoreValue },
                                    {
                                      'is-valid': formData.rejectMoreValue && !errors.rejectMoreValue
                                    }
                                  )}
                                  placeholder='Message'
                                  onChange={(e) => rejectChange(e)}
                                  autoComplete='off'
                                  value={rejectFormData.rejectMoreValue || ''}
                                />
                              </>
                            ) : rejectValue === 'Spam' || rejectValue === 'Suspect Fraud' || rejectValue === 'Fraud' || rejectValue === 'Fake' || rejectValue === 'Funneling' ?
                              null
                              : null
                          }
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => rejectSubmit()}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="row mb-6">
        <div className='col-md-12'>
          <div className="row">
            <div className='col-md-12'>
              <div className="card card-custom card-stretch gutter-b p-8 pb-0">
                <div className="d-flex align-items-left">
                  <div style={{ marginRight: "10px" }}>
                    <h1 className={`text-black ${RISKSTATUSCOLOR[TransactionIdDetail && TransactionIdDetail.riskStatus]}`}
                      style={{ marginTop: '10px', padding: "10px" }}
                    >
                      {scoreData && scoreData.transactionScore ? scoreData.transactionScore : '0'}
                    </h1>
                  </div>
                  <div>
                    <div className="d-flex align-items-left">
                      <h4 style={{ marginRight: "10px" }}>
                        {TransactionIdDetail && TransactionIdDetail.firstName ? TransactionIdDetail.firstName : '--'} {TransactionIdDetail && TransactionIdDetail.lastName ? TransactionIdDetail.lastName : '--'}
                      </h4>
                    </div>
                    <div className="d-flex align-items-left text-muted">
                      <h5 className="text-muted" style={{ marginRight: "10px" }}>
                        TXN{TransactionIdDetail && TransactionIdDetail.transactionId ? TransactionIdDetail.transactionId : '--'}
                      </h5>
                    </div>
                    <div className="d-flex align-items-left">
                      <h5 style={{ marginRight: "10px" }}>
                        First Seen
                      </h5>
                      <span className="text-muted">
                        {moment(dashboardDetail && dashboardDetail.firstSeen ? dashboardDetail.firstSeen : "--").format('DD/MM/YYYY')}
                      </span>
                    </div>
                    <div className="d-flex align-items-left">
                      <h5 style={{ marginRight: "10px" }}>
                        Age
                      </h5>
                      <span className="text-muted">
                        {dashboardDetail && dashboardDetail.ageOfCustomer ? dashboardDetail.ageOfCustomer : '--'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab mb-3">
            <Tab eventKey="overview" title="Overview">
              <div className="row">
                <div className='col-md-12'>
                  <div className="card card-custom card-stretch gutter-b p-8 mb-8">
                    <div className="row">
                      <div className='col-md-3'>
                        <div className='card card-custom  card-stretch gutter-b'>
                          <div className='card-header no-border cst-header mt-2'>
                            <h4><span><i className="bi bi-geo-alt-fill me-2 txt-danger"></i></span>Identity</h4>
                            <div className="h-6px w-100 bg-light-danger rounded">
                              <div className="bg-danger rounded h-6px" role="progressbar" style={{ width: '20%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                          <div className='card-body pt-0'>
                            <div className='row mb-8 mt-8'>
                              <div className='col-md-12'>
                                {
                                  dashboardDetail &&
                                  dashboardDetail.positive &&
                                  dashboardDetail.positive.map((datas, index) => {
                                    return (
                                        <div className='row' key={"F1_" + index}>
                                          <div className='col-lg-12 col-md-12 col-sm-12'>
                                            <span className="bullet bg-primary me-3" />
                                            {datas.message ? datas.message : '--'}
                                          </div>
                                        </div>
                                    )
                                  })
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-3'>
                        <div className='card'>
                          <div className='card-header no-border cst-header mt-2'>
                            <h4><span><i className="bi bi-display me-2 txt-primary"></i></span>Order</h4>
                            <div className="h-6px w-100 bg-light-primary rounded">
                              {
                                TransactionIdDetail && TransactionIdDetail.billingAddress === TransactionIdDetail.shippingAddress ?
                                  (
                                    <>
                                      <div className="bg-success rounded h-6px" role="progressbar" style={{ width: '76%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" />
                                    </>
                                  ) : (
                                    <>
                                      <div className="bg-primary rounded h-6px" role="progressbar" style={{ width: '76%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" />
                                    </>
                                  )
                              }
                            </div>
                          </div>
                          <div className='card-body pt-0'>
                            <div className='row mb-8 mt-8'>
                              <div className='col-md-12'>
                                <div className="d-flex align-items-center">
                                  {
                                    TransactionIdDetail && TransactionIdDetail.billingAddress === TransactionIdDetail.shippingAddress ?
                                      (
                                        <>
                                          <span className="bullet bg-primary me-3" />
                                          Billing / Shipping match
                                        </>
                                      ) : (
                                        <>
                                          <span className="bullet bg-primary me-3" />
                                          Billing / Shipping mismatch
                                        </>
                                      )
                                  }
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-3'>
                        <div className='card'>
                          <div className='card-header no-border cst-header mt-2'>
                            <h4><span><i className="bi bi-envelope me-2 txt-info"></i></span> Devices</h4>
                            <div className="h-6px w-100 bg-light-info rounded">
                              <div className="bg-info rounded h-6px" role="progressbar" style={{ width: '42%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                          <div className='card-body pt-0'>
                            <div className='row mb-8 mt-8'>
                              <div className='col-md-12'>
                                <div className='row'>
                                  <div className='col-lg-12 col-md-12 col-sm-12'>
                                    <span className="bullet bg-primary me-3" />
                                    {IpGeoOption && IpGeoOption.ipCheck && IpGeoOption.ipCheck.message ? IpGeoOption.ipCheck.message : '--'}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-3'>
                        <div className='card'>
                          <div className='card-header no-border cst-header mt-2'>
                            <h4><span><i className="bi bi-display me-2 txt-success"></i></span>Behavior</h4>
                            <div className="h-6px w-100 bg-light-success rounded">
                              <div className="bg-success rounded h-6px" role="progressbar" style={{ width: '90%' }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                          <div className='card-body pt-0'>
                            <div className='row mb-8 mt-8'>
                              <div className='col-md-12'>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  {
                                    TransactionIdDetail && TransactionIdDetail.behaviour && TransactionIdDetail.behaviour.behaviour ? TransactionIdDetail.behaviour.behaviour : '--'
                                  }
                                </div>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  {
                                    TransactionIdDetail && TransactionIdDetail.behaviour && TransactionIdDetail.behaviour.sevenDays ? TransactionIdDetail.behaviour.sevenDays : '--'
                                  }
                                </div>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  {
                                    TransactionIdDetail && TransactionIdDetail.behaviour && TransactionIdDetail.behaviour.thirtyDays ? TransactionIdDetail.behaviour.thirtyDays : '--'
                                  }
                                </div>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  {
                                    TransactionIdDetail && TransactionIdDetail.behaviour && TransactionIdDetail.behaviour.twentyFourHours ? TransactionIdDetail.behaviour.twentyFourHours : '--'
                                  }
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className='col-md-9'>
                  <div className="card card-custom card-stretch gutter-b p-8 mb-8">
                    <div className="row">
                      <div className='col-md-12'>
                        <h3>Order Summary</h3>
                      </div>
                      <div className='col-md-4 mb-8'>
                        <div className="card card-flush h-md-100">
                          <div className="card-header no-padding">
                            <div className="card-title">
                              <h4>Customer</h4>
                            </div>
                          </div>
                          <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
                          <div className="card-body pt-1">
                            <div className="row">
                              <div className='col-md-11'>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.shipToFirstName ? TransactionIdDetail.shipToFirstName : '--'} {TransactionIdDetail && TransactionIdDetail.shipToLastName ? TransactionIdDetail.shipToLastName : '--'}

                                </div>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.emailAddress ? TransactionIdDetail.emailAddress : '--'}
                                </div>
                                <div className="d-flex align-items-center">
                                  #CR10001
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-4 mb-8'>
                        <div className="card card-flush h-md-100">
                          <div className="card-header no-padding">
                            <div className="card-title">
                              <h4>Device</h4>
                            </div>
                          </div>
                          <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
                          <div className="card-body pt-1">
                            <div className="row">
                              <div className='col-md-11'>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.ipAddress ? TransactionIdDetail.ipAddress : '--'} {TransactionIdDetail && TransactionIdDetail.shipToLastName ? TransactionIdDetail.shipToLastName : '--'}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-4'>
                        <div className="card card-flush h-md-100">
                          <div className="card-header no-padding">
                            <div className="card-title">
                              <h4>IP GeoLocation</h4>
                            </div>
                          </div>
                          <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
                          <div className="card-body pt-1">
                            <div className="row">
                              <div className='col-md-11'>
                                {
                                  !_.isEmpty(allApiData) ? (
                                  allApiData && allApiData.map((api, a) => {
                                    return (
                                      <Fragment key={"kl" + a}>
                                        <div className="d-flex align-items-center">
                                        Country  - {api && api.location && api.location.country}
                                        </div>
                                        <div className="d-flex align-items-center">
                                        City - {api && api.location && api.location.city}
                                        </div>
                                        <div className="d-flex align-items-center">
                                          Region - {api && api.location && api.location.region}
                                        </div>
                                        <div className="d-flex align-items-center">
                                          PostalCode - {api && api.location && api.location.postalCode}
                                        </div>
                                        <div className="d-flex align-items-center">
                                        Lat - {api && api.location && api.location.lat}
                                        </div>
                                        <div className="d-flex align-items-center">
                                        Lng - {api && api.location && api.location.lng}
                                        </div>
                                      </Fragment>
                                    )
                                  })
                                  ):null
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-4'>
                        <div className="card card-flush h-md-100">
                          <div className="card-header no-padding">
                            <div className="card-title">
                              <h4>Shipping</h4>
                            </div>
                          </div>
                          <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
                          <div className="card-body pt-1">
                            <div className="row">
                              <div className='col-md-11'>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.shipToFirstName ? TransactionIdDetail.shipToFirstName : '--'} {TransactionIdDetail && TransactionIdDetail.shipToLastName ? TransactionIdDetail.shipToLastName : '--'}
                                </div>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.shippingAddress ? TransactionIdDetail.shippingAddress : '--'}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-4 mb-8'>
                        <div className="card card-flush h-md-100">
                          <div className="card-header no-padding">
                            <div className="card-title">
                              <h4>Billing</h4>
                            </div>
                          </div>
                          <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
                          <div className="card-body pt-1">
                            <div className="row">
                              <div className='col-md-11'>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.shipToFirstName ? TransactionIdDetail.shipToFirstName : '--'} {TransactionIdDetail && TransactionIdDetail.shipToLastName ? TransactionIdDetail.shipToLastName : '--'}
                                </div>
                                <div className="d-flex align-items-center">
                                  {TransactionIdDetail && TransactionIdDetail.billingAddress ? TransactionIdDetail.billingAddress : '--'}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-4 mb-8'>
                        <div className="card card-flush h-md-100">
                          <div className="card-header no-padding">
                            <div className="card-title">
                              <h4>Delivery</h4>
                            </div>
                          </div>
                          <div className="separator mb-2" style={{ borderBottomColor: '#eff2f5' }} />
                          <div className="card-body pt-1">
                            <div className="row">
                              <div className='col-md-11'>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  Shipping Company - Fedex
                                </div>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  Shipping Method - Next Day
                                </div>
                                <div className="d-flex align-items-center">
                                  <span className="bullet bg-primary me-3" />
                                  Shipping Price - $45
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className='col-md-12'>
                        <h3>Order Items</h3>
                        <div className="table-responsive">
                          <table className="table table-borderless table-vertical-center">
                            <thead>
                              <tr>
                                <th className="p-2 text-muted" style={{ minWidth: "70px" }}>Item</th>
                                <th className="p-2 text-muted" style={{ minWidth: "90px" }}>Description</th>
                                <th className="p-2 text-muted" style={{ minWidth: "90px" }}>Quantity</th>
                                <th className="p-2 text-muted" style={{ minWidth: "90px" }}>Discount</th>
                                <th className="p-2 text-muted" style={{ minWidth: "80px" }}>Amount</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr style={{ borderColor: '#eee' }}>
                                {
                                  TransactionIdDetail &&
                                  TransactionIdDetail.item &&
                                  TransactionIdDetail.item.map((datas, index) => {
                                    return (
                                      <Fragment key={"F2_" + index}>
                                        <td className="p-2">{datas.name}</td>
                                        <td className="p-2" style={{ width: "231px" }}>{datas.description}</td>
                                        <td className="p-2">${datas.quantity}</td>
                                        <td className="p-2">{datas.discount}</td>
                                        <td className="p-2">{datas.price}</td>
                                      </Fragment>
                                    )
                                  })
                                }
                              </tr>
                            </tbody>
                          </table>
                          <div className="separator col-sm-12 col-md-12 col-lg-12 mb-2" />
                          <div className='row'>
                            <div className='col-sm-6 col-md-6 col-lg-6'>
                              <h6>Promos Used</h6>
                              <p className='text-muted'>
                                {TransactionIdDetail && TransactionIdDetail.promoCode ? TransactionIdDetail.promoCode : '--'}
                              </p>
                            </div>
                            <div className='col-sm-6 col-md-6 col-lg-6 '>
                              <div className='d-flex justify-content-end'>
                                <h6 className='me-2'>Order Discount : </h6>
                                <p className='fs-6'>
                                  {TransactionIdDetail.orderDiscount ? TransactionIdDetail.orderDiscount : "--"}
                                </p>
                              </div>
                              <div className='d-flex justify-content-end'>
                                <h6 className='me-2'>Total :</h6>
                                <p className='fs-6'>
                                  {TransactionIdDetail.totalAmount ? TransactionIdDetail.totalAmount : "--"}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='col-md-3 mb-8'>
                  <div className="row">
                    <div className='col-md-12'>
                      <div className={`card card-custom card-stretch gutter-b p-5 w-100 ${RISKSTATUSCOLOR[TransactionIdDetail && TransactionIdDetail.riskStatus]}`} >
                        <div className="card-body d-flex align-items-end pt-0">
                          <div className="d-flex align-items-left flex-column w-100">
                            <div className=' col-md-12 card mb-5 mb-xl-10' style={{ backgroundColor: 'transparent' }}>
                              <div className='card-body pt-9 pb-0'>
                                <div className='d-flex flex-wrap flex-sm-nowrap mb-3'>
                                  <div className='flex-grow-1'>
                                    <div className='d-flex justify-content-between align-items-start flex-wrap mb-2'>
                                      <div className='row'>
                                        <div className='col-lg-12'>
                                          <div className="card-toolbar d-flex mt-5">
                                            {
                                              TransactionIdDetail.riskStatus === 'PENDING' ?
                                                (
                                                  <ul className="nav">
                                                    <li className="nav-item">
                                                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                                                        data-toggle='modal'
                                                        data-target='#approveModal'
                                                        onClick={() => setActive(true)}
                                                      >
                                                        Approve
                                                      </a>
                                                    </li>
                                                    <li className="nav-item">
                                                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                                                        data-toggle='modal'
                                                        data-target='#rejectModal'
                                                        onClick={() => setActive(true)}
                                                      >
                                                        Reject
                                                      </a>
                                                    </li>
                                                    <li className="nav-item">
                                                      <a className="nav-link btn btn-sm fw-bolder px-4 me-2 btn-warning"
                                                        data-toggle='modal'
                                                        data-target='#holdModal'
                                                        onClick={() => setActive(true)}
                                                      >
                                                        Hold
                                                      </a>
                                                    </li>
                                                  </ul>
                                                )
                                                : (
                                                  TransactionIdDetail.riskStatus === 'HOLD' ? (
                                                    <ul className="nav">
                                                      <li className="nav-item">
                                                        <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                                                          data-toggle='modal'
                                                          data-target='#approveModal'
                                                          onClick={() => { setActive(true) }}
                                                        >
                                                          Approve
                                                        </a>
                                                      </li>
                                                      <li className="nav-item">
                                                        <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                                                          data-toggle='modal'
                                                          data-target='#rejectModal'
                                                          onClick={() => { setActive(true) }}
                                                        >
                                                          Reject
                                                        </a>
                                                      </li>
                                                    </ul>
                                                  ) :
                                                    (
                                                      TransactionIdDetail.riskStatus === 'APPROVED' ? (
                                                        null
                                                      ) :
                                                        (
                                                          TransactionIdDetail.riskStatus === 'REJECTED' ? (
                                                            null
                                                          ) : (null)
                                                        )
                                                    )

                                                )
                                            }
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className={`d-flex align-items-left widgetfy ${RISKSTATUSCOLOR[TransactionIdDetail && TransactionIdDetail.riskStatus]}`}>
                              <div>
                                <h4 className='text-black'>{TransactionIdDetail.riskStatus}</h4>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row mt-8 mb-0">
                    <div className='col-md-12'>
                      <div className="card card-custom card-stretch gutter-b pt-2 p-8 h-md-100">
                        <div className="card-header cst-header">
                          <div className="card-title">
                            <h4>Payment Details</h4>
                          </div>
                        </div>
                        <div className="card-body pt-1 one-db">
                          <ul style={{ textAlign: "right", marginTop: "15px" }}>
                            <li>
                              <span className={`badge badge-square badge-lg me-3 ${AVSSTATUS[TransactionIdDetail && TransactionIdDetail.AVS]}`} style={{ padding: '10px' }}>AVS</span>
                              <span className={`badge badge-square badge-lg me-3 ${CVSSTATUS[TransactionIdDetail && TransactionIdDetail.CVV]}`} style={{ padding: '10px' }}>CVV</span>
                            </li>
                            <li>
                              <h6>Payment Method</h6>
                              <p className='text-muted'>Credit Card</p>
                            </li>
                            <li>
                              <h6>CC issuer</h6>
                              <p className='text-muted'>VISA</p>
                            </li>
                            <li>
                              <h6>CC Type</h6>
                              <p className='text-muted'>CC</p>
                            </li>
                            <li>
                              <h6>CC Last 4 Digits</h6>
                              <p className='text-muted'>1234</p>
                            </li>
                            <li>
                              <h6>Payment GateWay</h6>
                              <p className='text-muted'>RazorPay</p>
                            </li>
                            <li>
                              <h6>BIN Number</h6>
                              <p className='text-muted'>56856</p>
                            </li>
                            <li>
                              <h6>BIN Country</h6>
                              <p className='text-muted'>Franse</p>
                            </li>
                            <li>
                              <ChangeStatus />
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className='col-md-12'>
                  <div className="card card-custom card-stretch gutter-b p-8 mb-8">
                    <div className="row">
                      <div className='col-md-12'>
                        <h3>Order History</h3>
                        <div className="table-responsive">
                          <table className="table">
                            <thead>
                              <tr className="fw-bolder fs-6 text-gray-800">
                                <th className="p-2 text-muted" style={{ minWidth: "90px" }}>Order Date</th>
                                <th className="p-2 text-muted" style={{ minWidth: "130px" }}>Order Ref No</th>
                                <th className="p-2 text-muted" style={{ minWidth: "80px" }}>Order Amount</th>
                                <th className="p-2 text-muted" style={{ minWidth: "80px" }}>Currency</th>
                                <th className="p-2 text-muted" style={{ minWidth: "90px" }}>Quantity</th>
                                <th className="p-2 text-muted" style={{ minWidth: "130px" }}>Status </th>
                              </tr>
                            </thead>
                            <tbody>
                            {
                                  TransactionIdDetail &&
                                  TransactionIdDetail.orderHistory &&
                                  TransactionIdDetail.orderHistory.map((item, i) => {
                                    return (
                                      <Fragment key={"F3_" + i}>
                                      <tr>
                                        <td key={i}>
                                          {moment(item.updatedAt ? item.updatedAt : "--").format('DD/MM/YYYY')}
                                        </td>
                                        <td>
                                          <Link to={`/transaction-dashboard/${item.caseId}`}>
                                            TXN{item.transactionId ? item.transactionId : "--"}
                                          </Link>
                                        </td>
                                        <td>
                                          {item.totalAmount ? item.totalAmount : "--"}
                                        </td>
                                        <td>
                                          {item.currency ? item.currency : "--"}
                                        </td>
                                        <td>
                                          {item.quantity ? item.quantity : "--"}
                                        </td>
                                        <td >
                                          <span className={`badge ${RISKSTATUS[item.riskStatus && item.riskStatus]}`}>
                                            {item.riskStatus ? item.riskStatus : "--"}
                                          </span>
                                        </td>
                                        </tr>
                                      </Fragment>
                                    )
                                  })
                                }
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className='col-md-12'>
                  <TransactionDashboard />
                </div>
              </div>
            </Tab>
            <Tab eventKey="NetWork" title="NetWork">
              <NetworkGraph networkData={LinkAnlyticslists} TransactionIdDetail={TransactionIdDetail} />
            </Tab>
          </Tabs>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { 
    dashboardStore, 
    riskManagementlistStore, 
    editMerchantStore, 
    TransactionGetByIdStore, 
    LinkAnlyticsTransactionStore, 
    ApproveTXnStore,
    TransactionDashboardStore
  } = state

  return {
    dashboardDetails: dashboardStore && dashboardStore.dashboardDetails ? dashboardStore.dashboardDetails : {},
    riskmgmtlistdetails: riskManagementlistStore && riskManagementlistStore.riskmgmtlists ? riskManagementlistStore.riskmgmtlists : null,
    getRiskSummarys: state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    merchantIddetails: editMerchantStore && editMerchantStore.merchantIddetail ? editMerchantStore.merchantIddetail : {},
    TransactionGetById: TransactionGetByIdStore && TransactionGetByIdStore.TransactionGetById ? TransactionGetByIdStore.TransactionGetById : {},
    riskmgmtlistdetails: riskManagementlistStore && riskManagementlistStore.riskmgmtlists ? riskManagementlistStore.riskmgmtlists : {},
    approveResponceData: ApproveTXnStore && ApproveTXnStore.ApproveTXnResponce ? ApproveTXnStore.ApproveTXnResponce : {},
    loading: ApproveTXnStore && ApproveTXnStore.loading ? ApproveTXnStore.loading : false,
    TransactionDashboardIdDetail: TransactionDashboardStore && TransactionDashboardStore.TransactionDashboardIdDetail ? TransactionDashboardStore.TransactionDashboardIdDetail : {},
    LinkAnlyticslists: LinkAnlyticsTransactionStore && LinkAnlyticsTransactionStore.LinkAnlyticslists ? LinkAnlyticsTransactionStore.LinkAnlyticslists : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  TxngetbyIdDispatch: (id) => dispatch(TransactionGetByIdActions.getGetByIdDetails(id)),
  linkAnalytics: (params) => dispatch(LinkAnlyticsTransactionActions.getLinkAnlyticsTransactionlist(params)),
  ApprovePost: (id, params) => dispatch(ApproveTXnActions.ApproveTXn(id, params)),
  clearApproveTXN: () => dispatch(ApproveTXnActions.clearApproveTXn()),
  getTransactionDispatch: (params) => dispatch(TransactionActions.getTransactionlist(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiskDetails)