import { USER_MANAGEMENT_ERROR, REGEX } from "../../utils/constants";
import _ from 'lodash'

export const transactionValidation = (values, setErrors) => {
  const errors = {};
  if (!values.firstName) {
    errors.firstName = 'First Name field is required.'
  }
  if (!values.lastName) {
    errors.lastName = 'Last Name field is required.'
  }
  if (!values.currency) {
    errors.currency = 'Currency field is required.'
  }
  if (!values.totalAmount) {
    errors.totalAmount = 'Total Amount field is required.'
  }
  if (!values.quantity) {
    errors.quantity = 'Quantity field is required.'
  }
  if (!values.department) {
    errors.department = 'Department field is required.'
  }
  if (!values.paymentMethod) {
    errors.paymentMethod = 'Payment Method field is required.'
  }
  if (!values.creditCardNumber) {
    errors.creditCardNumber = 'Credit Card Number field is required.'
  }
  if (!values.billingAddress) {
    errors.billingAddress = 'Billing Address field is required.'
  }
  if (!values.billingCity) {
    errors.billingCity = 'Billing City field is required.'
  }
  if (!values.billingState) {
    errors.billingState = 'Billing State field is required.'
  }
  if (!values.billingCountry) {
    errors.billingCountry = 'Billing Country field is required.'
  }
  if (!values.billingZipCode) {
    errors.billingZipCode = 'Billing Zip Code field is required.'
  }
  if (!values.AVS) {
    errors.AVS = 'AVS field is required.'
  }
  if (!values.CVV) {
    errors.CVV = 'CVV field is required.'
  }
  if (!values.shipToFirstName) {
    errors.shipToFirstName = 'Ship To FirstName field is required.'
  }
  if (!values.shipToLastName) {
    errors.shipToLastName = 'Ship To LastName field is required.'
  }
  if (!values.shippingAddress) {
    errors.shippingAddress = 'Shipping Address field is required.'
  }
  if (!values.shippingCity) {
    errors.shippingCity = 'Shipping City field is required.'
  }
  if (!values.shippingState) {
    errors.shippingState = 'Shipping State field is required.'
  }
  if (!values.shippingCountry) {
    errors.shippingCountry = 'Shipping Country field is required.'
  }
  if (!values.shippingZipCode) {
    errors.shippingZipCode = 'Shipping Zip Code field is required.'
  }
  if (!values.ipAddress) {
    errors.ipAddress = 'IpAddress field is required.'
  }
  if (!values.description) {
    errors.description = 'Description field is required.'
  }
  if (!values.emailAddress) {
    errors.emailAddress = 'emailAddress field is required.'
  }
  if (!values.phone) {
    errors.phone = 'Phone field is required.'
  }
  if (!values.shippingCharge) {
    errors.shippingCharge = 'ShippingCharge field is required.'
  }
  setErrors(errors); 
  return errors;
};