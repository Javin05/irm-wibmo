import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import {
  userGetDetailsActions,
  editUserActions,
  userRolesActions,
  addMerchantUploadActions,
  userTypesActions
} from '../../store/actions'
import { useLocation, Link, useHistory } from 'react-router-dom'
import { STATUS_RESPONSE, REGEX, API_URL, FILE_FORMAT_IMAGES, DROPZONE_MESSAGES } from '../../utils/constants'
import color from '../../utils/colors'
import _ from 'lodash'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import { setUserData, setUserPassData } from './formData'
import { userEditValidation, userEdiPasstValidation } from './validation'
import { confirmAlert, warningAlert } from '../../utils/alerts'
import userPlaceholder from '../../assets/userPlaceholder.png'

const ViewUser = (props) => {
  const {
    getUserDetailsDispatch,
    clearUserDetailsDispatch,
    statusGUD,
    loadingGUD,
    UserGetDetails,
    editUserDispatch,
    statusEUD,
    messageEUD,
    cleareditUserDispatch,
    userRoleData,
    getUserroleDispatch,
    loadingEUD,
    addMerchantUploadDispatch,
    clearaddMerchantUploadDispatch,
    dataAMUpload,
    loadingAMUpload,
    statusAMUpload,
    messageAMUpload,
    getUserTypeDispatch,
    getDataUserType,
    loadingGetUR
  } = props

  const url = useLocation().pathname
  const fields = url && url.split('/update-user')
  const id = fields && fields[1]
  const history = useHistory()
  const [errors, setErrors] = useState({})
  const [editForm, setEditForm] = useState(false)
  const [editPasswordForm, setEditPasswordForm] = useState(false)
  const [selectedUserTypeOption, setSelectedUserTypeOption] = useState('')
  const [userTypeOption, setUserTypeOption] = useState()
  const [selectedUserRoleOption, setSelectedUserRoleOption] = useState('')
  const [userRoleOption, setUserRoleOption] = useState()
  const [updateUpload, setUpdateUpload] = useState(false)
  const [passUpdate, setPassUpdate] = useState(false)
  const [initTypeUpdate, setInitTypeUpdate] = useState(false)
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    userTypeId: '',
    roleId: '',
    password: '',
    cPassword: '',
    image: '',
    tag: "IRM"
  })

  useEffect(() => {
    const params = {
      tag: "IRM"
    }
    getUserroleDispatch(params)
    getUserTypeDispatch(params)
  }, [])

  const handleChange = (e) => {
    e.persist()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleEdit = () => {
    setEditForm(val => !val)
  }

  const handlePasswordEdit = () => {
    setEditPasswordForm(val => !val)
  }

  const handleSubmit = () => {
    const errorMsg = userEditValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const payload = setUserData(formData)
      editUserDispatch(id, payload)
    }
  }

  const handlePassSubmit = () => {
    const errorMsg = userEdiPasstValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const payload = setUserPassData(formData)
      editUserDispatch(id, payload)
      setPassUpdate(true)
    }
  }

  const onConfirm = () => {
    if (!updateUpload && !passUpdate) {
      history.push('/user-management')
    }
    setUpdateUpload(false)
    setPassUpdate(false)
    setEditPasswordForm(false)
    setFormData({
      password: '',
      cPassword: ''
    })

  }

  useEffect(() => {
    if (statusEUD === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'success',
        messageEUD,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() },
      )
      cleareditUserDispatch()
    } else if (statusEUD === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageEUD,
        '',
        'Ok'
      )
      cleareditUserDispatch()
    }
  }, [statusEUD])

  useEffect(() => {
    if (id) {
      getUserDetailsDispatch(id)
    }
  }, [id])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? _.startCase(item[name]) : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    if (initTypeUpdate) {
      const data = getDefaultOptions(userRoleData, 'role')
      setUserRoleOption(data)
    }
  }, [userRoleData])

  const handleChangeUserRole = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserRoleOption(selectedOption)
      setFormData((values) => ({
        ...values,
        roleId: selectedOption.value
      }))
      setErrors((values) => ({ ...values, roleId: '' }))
    } else {
      setSelectedUserRoleOption()
      setFormData((values) => ({ ...values, roleId: '' }))
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getDataUserType, 'userType')
    setUserTypeOption(data)
  }, [getDataUserType])

  const handleChangeUserType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserTypeOption(selectedOption)
      setFormData((values) => ({
        ...values,
        userTypeId: selectedOption.value
      }))
      setInitTypeUpdate(true)
      const roleParams = {
        skipPagination: true,
        userTypeId: selectedOption.value,
        tag: "IRM"
      }
      getUserroleDispatch(roleParams)
      setErrors((values) => ({ ...values, userTypeId: '' }))
    } else {
      setSelectedUserTypeOption()
      setFormData((values) => ({ ...values, userTypeId: '', roleId: '' }))
    }
  }

  useEffect(() => {
    if (statusGUD === STATUS_RESPONSE.SUCCESS_MSG) {
      const setData = setUserData(UserGetDetails)
      setFormData(setData)
      if (UserGetDetails.roleId) {
        const data = getDefaultOptions(userRoleData, 'role')
        const selOption = _.filter(data, function (x) {
          if (_.includes(UserGetDetails.roleId._id, x.value)) {
            return x
          }
        })
        setSelectedUserRoleOption(selOption)
      }
      if (UserGetDetails.userTypeId) {
        const data = getDefaultOptions(getDataUserType, 'userType')
        const selOption = _.filter(data, function (x) {
          if (_.includes(UserGetDetails.userTypeId, x.value)) {
            return x
          }
        })
        setSelectedUserTypeOption(selOption)
      }
      clearUserDetailsDispatch()
    } else if (statusGUD === STATUS_RESPONSE.ERROR_MSG) {
      clearUserDetailsDispatch()
    }
  }, [statusGUD])

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_IMAGES, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', "user")
        data.append('file_to_upload', files)
        addMerchantUploadDispatch(data)
        setErrors((values) => ({ ...values, image: "" }));
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID });
    }
  };

  useEffect(() => {
    if (statusAMUpload === STATUS_RESPONSE.SUCCESS_MSG) {
      if (dataAMUpload && dataAMUpload.path) {
        setFormData((values) => ({ ...values, image: dataAMUpload.path }))
        const payloadImage = {
          ...setFormData,
          image: dataAMUpload.path
        }
        editUserDispatch(id, payloadImage)
        setUpdateUpload(true)
      }
      clearaddMerchantUploadDispatch()
    } else if (statusAMUpload === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAMUpload,
        '',
        'Ok'
      )
      clearaddMerchantUploadDispatch()
    }
  }, [statusAMUpload])

  useEffect(() => {
    return () => {
      setSelectedUserRoleOption()
      setSelectedUserTypeOption()
    }
  }, [])

  return (
    <>
      <div className='card-header bg-skyBlue py-5'>
        {loadingGUD
          ? (
            <div className='d-flex justify-content-center py-5'>
              <div className='spinner-border text-primary m-5' role='status' />
            </div>
          )
          : (
            <>
              <div className="card-title d-flex row">
                <div className='col-lg-2 justify-content-start px-8'>
                  <div class="card-header ribbon ribbon-start ribbon-clip">
                    <div class="ribbon-label">
                      <span className="bi bi-person-badge"> User</span>
                      <span class="ribbon-inner bg-dark"></span>
                    </div>
                    <div class="card-title d-flex justify-content-start mt-2 px-15">
                      {UserGetDetails.firstName ? _.startCase(UserGetDetails.firstName) : '--'}&nbsp;
                      {UserGetDetails.lastName ? _.startCase(UserGetDetails.lastName) : '--'}
                    </div>
                  </div>
                </div>
                <div className='col-lg-10 justify-content-end'>
                  <Link to='/user-management' className='btn btn-darkRed fa-pull-right' onClick={() => { clearUserDetailsDispatch() }}>
                    Back
                  </Link>
                </div>
              </div>
              <div className='card-body'>
                <div className='row'>
                  <div className='col-lg-4'>
                    <div className='card'>
                      <div className="flex-column flex-lg-row-auto w-lg-350px w-xl-350px mb-8 px-15">
                        <div className="d-flex flex-center flex-column py-5">
                          <div className="image-input image-input-outline image-input-empty" data-kt-image-input="true">
                            <div className="image-input-wrapper w-125px h-125px">
                              <img
                                src={`${API_URL}/uploads/${formData.image}`}
                                alt="your image" style={{ width: 120, height: 120 }}
                                onError={(e) => { e.target.src = userPlaceholder }}
                              />
                            </div>
                            <label className="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="" data-bs-original-title="Change avatar" >
                              <i className="bi bi-pencil-fill fs-7"></i>
                              <input
                                type="file"
                                name="image"
                                id="file_to_upload"
                                accept=".png, .jpg, .jpeg"
                                multiple={false}
                                onChange={handleFileChange}
                              />
                              <input type="hidden" name="avatar_remove" />
                            </label>
                          </div>
                          {loadingAMUpload ? (
                            <div className="d-flex justify-content-start align-items-start py-3">
                              <div className="d-flex justify-content-start">
                                <span className="spinner-grow spinner-grow-sm" role="status">
                                  <span className="visually-hidden" />
                                </span>
                                <span className="spinner-grow spinner-grow-sm mx-1" role="status">
                                  <span className="visually-hidden" />
                                </span>
                              </div>
                              <div className="mx-2">Uploading</div>
                            </div>
                          ) :
                            null
                          }
                          <a href="#" className="fs-3 text-gray-800 text-hover-primary fw-bolder mb-3">
                            {UserGetDetails.firstName ? _.startCase(UserGetDetails.firstName) : '--'}&nbsp;{UserGetDetails.lastName ? _.startCase(UserGetDetails.lastName) : '--'}
                          </a>
                          <div className="mb-9">
                            <div className="badge badge-lg badge-light-primary d-inline">
                              {UserGetDetails && UserGetDetails.roleId
                                ? _.startCase(UserGetDetails.roleId.role)
                                : '--'}
                            </div>
                          </div>
                          {/* <div className="fw-bolder mb-3">Assigned Tickets
                    <i className="fas fa-exclamation-circle ms-2 fs-7" />
                  </div>  */}
                          {/* <div className="d-flex flex-wrap flex-center">
                    <div className="border border-gray-300 border-dashed rounded py-3 px-3 mb-3">
                      <div className="fs-4 fw-bolder text-gray-700">
                        <span className="w-75px">243</span>
                        <span className="svg-icon svg-icon-3 svg-icon-success">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor"></rect>
                            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor"></path>
                          </svg>
                        </span>
                      </div>
                      <div className="fw-bold text-muted">Total</div>
                    </div>
                    <div className="border border-gray-300 border-dashed rounded py-3 px-3 mx-4 mb-3">
                      <div className="fs-4 fw-bolder text-gray-700">
                        <span className="w-50px">56</span>
                        <span className="svg-icon svg-icon-3 svg-icon-danger">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="11" y="18" width="13" height="2" rx="1" transform="rotate(-90 11 18)" fill="currentColor"></rect>
                            <path d="M11.4343 15.4343L7.25 11.25C6.83579 10.8358 6.16421 10.8358 5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75L11.2929 18.2929C11.6834 18.6834 12.3166 18.6834 12.7071 18.2929L18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25C17.8358 10.8358 17.1642 10.8358 16.75 11.25L12.5657 15.4343C12.2533 15.7467 11.7467 15.7467 11.4343 15.4343Z" fill="currentColor"></path>
                          </svg>
                        </span>
                      </div>
                      <div className="fw-bold text-muted">Solved</div>
                    </div>
                    <div className="border border-gray-300 border-dashed rounded py-3 px-3 mb-3">
                      <div className="fs-4 fw-bolder text-gray-700">
                        <span className="w-50px">188</span>
                        <span className="svg-icon svg-icon-3 svg-icon-success">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor"></rect>
                            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor"></path>
                          </svg>
                        </span>
                      </div>
                      <div className="fw-bold text-muted">Open</div>
                    </div>
                  </div> */}
                        </div>
                        <div className="card-title d-flex">
                          <h4 className="d-flex justify-content-start">Change Password</h4>
                          <td className="d-flex justify-content-end mx-2">
                            <button
                              type="button"
                              className="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                              disabled={loadingAMUpload || loadingEUD}
                              onClick={() => {
                                handlePasswordEdit()
                                setFormData({
                                  password: '',
                                  cPassword: ''
                                })
                              }}
                            >
                              <span className="svg-icon svg-icon-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                  <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="currentColor"></path>
                                  <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="currentColor"></path>
                                </svg>
                              </span>
                            </button>
                          </td>
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Password</label>
                          <input
                            name='password'
                            type='password'
                            className='form-control'
                            id='basic-url'
                            placeholder='Password'
                            aria-describedby='basic-addon3'
                            onChange={(e) => handleChange(e)}
                            value={formData.password || ''}
                            autoComplete='off'
                            disabled={!editPasswordForm}
                          />
                          {errors && errors.password && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.password}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Confirm Password</label>
                          <input
                            name='cPassword'
                            type='password'
                            className='form-control'
                            id='basic-url'
                            placeholder='Confirm Password'
                            aria-describedby='basic-addon3'
                            onChange={(e) => handleChange(e)}
                            value={formData.cPassword || ''}
                            autoComplete='off'
                            disabled={!editPasswordForm}
                          />
                          {errors && errors.cPassword && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.cPassword}
                            </div>
                          )}
                        </div>
                        {
                          editPasswordForm
                            ? (
                              <div className='form-group row mt-4'>
                                <div className='col-lg-4' />
                                <div className='col-lg-8'>
                                  <div className='col-lg-12'>
                                    <button
                                      className='btn btn-blue mt-7 fa-pull-right'
                                      onClick={(event) => {
                                        handlePassSubmit(event)
                                      }}
                                    >
                                      {loadingEUD
                                        ? (
                                          <span
                                            className='spinner-border spinner-border-sm mx-3'
                                            role='status'
                                            aria-hidden='true'
                                          />
                                        )
                                        : (
                                          'Submit'
                                        )}
                                    </button>
                                  </div>
                                </div>
                              </div>
                            )
                            : null
                        }
                      </div>
                    </div>
                  </div>
                  <div className='col-lg-8'>
                    <div className="card pt-4 mb-6 mb-xl-12">
                      <div className="card-header border-0 ">
                        <div className="card-title">
                          <h2>Profile</h2>
                        </div>
                        <td className="text-end mt-10">
                          <button
                            type="button"
                            className="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                            disabled={loadingAMUpload || loadingEUD}
                            onClick={() => { handleEdit() }}
                          >
                            <span className="svg-icon svg-icon-3">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="currentColor"></path>
                                <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="currentColor"></path>
                              </svg>
                            </span>
                          </button>
                        </td>
                      </div>
                      <div className="card-body pt-0 pb-5">
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">First Name</label>
                          <input
                            name='firstName'
                            type='text'
                            className='form-control form-control-solid'
                            placeholder='First Name'
                            value={formData.firstName || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            disabled={!editForm}
                          />
                          {errors && errors.firstName && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.firstName}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Last Name</label>
                          <input
                            name='lastName'
                            type='text'
                            className='form-control form-control-solid'
                            placeholder='Last Name'
                            value={formData.lastName || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            disabled={!editForm}
                          />
                          {errors && errors.lastName && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.lastName}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Email</label>
                          <input
                            name='email'
                            type='text'
                            className='form-control form-control-solid'
                            placeholder='Email'
                            value={formData.email || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            disabled={!editForm}
                          />
                          {errors && errors.email && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.email}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Phone Number</label>
                          <input
                            name='mobile'
                            type='text'
                            className='form-control'
                            placeholder='Phone Number'
                            value={formData.mobile || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            maxLength={10}
                            disabled={!editForm}
                            onKeyPress={(e) => {
                              if (!REGEX.NUMERIC.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors && errors.mobile && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.mobile}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">User Type</label>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='userTypeId'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeUserType}
                            options={userTypeOption}
                            value={selectedUserTypeOption}
                            isDisabled={!editForm}
                          />
                          {errors && errors.userTypeId && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.userTypeId}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Role</label>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='roleId'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeUserRole}
                            options={userRoleOption}
                            value={selectedUserRoleOption}
                            isLoading={loadingGetUR}
                            isDisabled={!editForm}
                          />
                          {errors && errors.roleId && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.roleId}
                            </div>
                          )}
                        </div>
                        {
                          editForm
                            ? (
                              <div className='form-group row mt-4'>
                                <div className='col-lg-5' />
                                <div className='col-lg-7'>
                                  <div className='col-lg-12'>
                                    <button
                                      className='btn btn-blue mt-7 fa-pull-right'
                                      onClick={(event) => {
                                        handleSubmit(event)
                                      }}
                                    >
                                      {loadingEUD
                                        ? (
                                          <span
                                            className='spinner-border spinner-border-sm mx-3'
                                            role='status'
                                            aria-hidden='true'
                                          />
                                        )
                                        : (
                                          'Submit'
                                        )}
                                    </button>
                                  </div>
                                </div>
                              </div>
                            )
                            : null
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { userStore, editUserStore, userroleStore, addMerchantUploadStore, usertypeStore } = state
  return {
    UserGetDetails: userStore && userStore.userGetDetails ? userStore.userGetDetails : {},
    statusGUD: userStore && userStore.statusGUD ? userStore.statusGUD : '',
    messagesGUD: userStore && userStore.messagesGUD ? userStore.messagesGUD : '',
    loadingGUD: userStore && userStore.loadingGUD ? userStore.loadingGUD : false,
    editUser: editUserStore && editUserStore.dataEUD ? editUserStore.dataEUD : [],
    statusEUD: editUserStore && editUserStore.statusEUD ? editUserStore.statusEUD : '',
    messageEUD: editUserStore && editUserStore.messageEUD ? editUserStore.messageEUD : '',
    loadingEUD: editUserStore && editUserStore.loadingEUD ? editUserStore.loadingEUD : false,
    userRoleData: userroleStore && userroleStore.userRoleData ? userroleStore.userRoleData : {},
    loadingGetUR: state && state.userroleStore && state.userroleStore.loadingGetUR,
    loadingAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.loadingAMUpload
        ? addMerchantUploadStore.loadingAMUpload
        : false,
    dataAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.dataAMUpload
        ? addMerchantUploadStore.dataAMUpload
        : {},
    statusAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.statusAMUpload
        ? addMerchantUploadStore.statusAMUpload
        : '',
    messageAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.messageAMUpload
        ? addMerchantUploadStore.messageAMUpload
        : '',
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
  }
}

const mapDispatchToProps = dispatch => ({
  getUserDetailsDispatch: (id) =>
    dispatch(userGetDetailsActions.getUserDetails(id)),
  clearUserDetailsDispatch: () =>
    dispatch(userGetDetailsActions.clearUserDetails()),
  editUserDispatch: (id, params) =>
    dispatch(editUserActions.editUser(id, params)),
  cleareditUserDispatch: () =>
    dispatch(editUserActions.cleareditUser()),
  getUserroleDispatch: (params) => dispatch(userRolesActions.getUserrole(params)),
  addMerchantUploadDispatch: (data) =>
    dispatch(addMerchantUploadActions.addMerchantUpload(data)),
  clearaddMerchantUploadDispatch: () =>
    dispatch(addMerchantUploadActions.clearaddMerchantUpload()),
  getUserTypeDispatch: (params) =>
    dispatch(userTypesActions.getUserType(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewUser)