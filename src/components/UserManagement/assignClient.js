import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { useLocation, Link } from 'react-router-dom'
import color from '../../utils/colors'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import _ from 'lodash'
import {
  partnerActions,
  clientActions,
  userGetDetailsActions,
  addAssignUserActions,
  assignUserActions,
  deleteAssignUserActions
} from '../../store/actions'
import { KTSVG } from '../../theme/helpers'
import { clientValidation } from './validation'
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import {
  warningAlert,
  confirmAlert,
  confirmationAlert
} from '../../utils/alerts'

function AssignClient(props) {
  const {
    getPartnerDispatch,
    getPartner,
    getClientDispatch,
    getClient,
    loadingClient,
    getUserDetailsDispatch,
    clearUserDetailsDispatch,
    loadingGUD,
    UserGetDetails,
    className,
    addAssignUserDispatch,
    clearaddAssignUserDispatch,
    statusAAU,
    messageAAU,
    loadingAAU,
    getAssignUserDispatch,
    getAssignUser,
    deleteAssignUserDispatch,
    clearDeleteAssignUserDispatch,
    statusDeleteAssignUser,
    messageDeleteAssignUser,
    loadingGAU
  } = props
  const url = useLocation().pathname
  const fields = url && url.split('assign-client/')
  const id = fields && fields[1]
  const [errors, setErrors] = useState({})
  const [typingTimeout, setTypingTimeout] = useState(0)
  const [selectedPartnerOption, setSelectedPartnerOption] = useState()
  const [partnerOption, setPartnerOption] = useState()
  const [selectedClientOption, setSelectedClientOption] = useState('')
  const [clientOption, setClientOption] = useState()
  const [formData, setFormData] = useState({
    userId: id,
    partnerId: '',
    clientId: '',
    tag: "IRM"
  })

  useEffect(() => {
    const param ={
      tag: "IRM"
    }
    getPartnerDispatch(param)
    getClientDispatch(param)
    const params = {
      clientId: id,
      tag: "IRM"
    }
    getAssignUserDispatch(params)
  }, [])

  useEffect(() => {
    if (id) {
      getUserDetailsDispatch(id)
    }
  }, [id])

  const handleSubmit = () => {
    const errorMsg = clientValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      addAssignUserDispatch(formData)
    }
  }

  const onConfirm = () => {
    setSelectedPartnerOption()
    setSelectedClientOption()
    const params = {
      clientId: id,
      tag: "IRM"
    }
    getAssignUserDispatch(params)
  }

  useEffect(() => {
    if (statusAAU === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAAU,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() },
      )
      clearaddAssignUserDispatch()
    } else if (statusAAU === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAAU,
        'error',
        'Close',
        'Ok',
        () => {
          onConfirm()
        },
        () => { }
      )
      clearaddAssignUserDispatch()
    }
  }, [statusAAU])

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_PARTNER,
      'warning',
      'Yes',
      'No',
      () => {
        deleteAssignUserDispatch(id)
      },
      () => { }
    )
  }

  useEffect(() => {
    if (statusDeleteAssignUser === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageDeleteAssignUser,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() },
      )
      clearDeleteAssignUserDispatch()
    } else if (statusDeleteAssignUser === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageDeleteAssignUser,
        'error',
        'Close',
        'Ok',
        () => { onConfirm() },
        () => { }
      )
      clearDeleteAssignUserDispatch()
    }
  }, [statusDeleteAssignUser])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultOption = (data) => {
    const defaultOptions = []
    if (!_.isEmpty(getPartner)) {
      if (!_.isEmpty(data)) {
        data.map((item) => (
          defaultOptions.push({
            label: `${item.partnerFirstName ? item.partnerFirstName : ''} ${item.partnerLastName
              ? item.partnerLastName
              : ''}`,
            value: item._id
          })
        ))
      }
      return defaultOptions
    }
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    const data = getDefaultOption(getPartner)
    setPartnerOption(data)
  }, [getPartner])

  useEffect(() => {
    const data = getDefaultOptions(getClient && getClient.result, 'company')
    setClientOption(data)
  }, [getClient])

  const handleChangePartner = selectedOption => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          const clientParams = {
            skipPagination: true,
            partnerId: selectedOption.value,
            tag: "IRM"
          }
          getClientDispatch(clientParams)
        }, 1500)
      )
      setSelectedPartnerOption(selectedOption)
      setFormData(values => ({ ...values, partnerId: selectedOption.value }))
      setErrors({ ...errors, partnerId: '' })
    } else {
      setSelectedPartnerOption()
      setSelectedClientOption()
      setFormData(values => ({ ...values, partnerId: '', clientId: '' }))
    }
  }

  const handleChangeClient = selectedOption => {
    if (selectedOption !== null) {
      setSelectedClientOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value }))
      setErrors({ ...errors, clientId: '' })
    } else {
      setSelectedClientOption()
      setFormData(values => ({ ...values, clientId: '' }))
    }
  }

  return (
    <>
      <div className='card-header bg-skyBlue py-5'>
        {loadingGUD
          ? (
            <div className='d-flex justify-content-center py-5'>
              <div className='spinner-border text-primary m-5' role='status' />
            </div>
          )
          : (
            <>
              <div className="card-title d-flex row">
                <div className='col-lg-12 justify-content-end'>
                  <Link to='/user-management' className='btn btn-darkRed fa-pull-right' onClick={() => { clearUserDetailsDispatch() }}>
                    Back
                  </Link>
                </div>
              </div>
              <div className="row g-5 g-xl-6 mb-4 mt-2">
                <div className="col-xl-6 col-sm-10 col-md-10  mx-auto">
                  <div
                    className='card min-h-250px'
                  >
                    <div className="card-header border-0 pt-5">
                      <div className="card-title align-items-start flex-column text-primary fw-bolder">
                        User Details
                      </div>
                    </div>
                    <div className="mx-7 mb-5">
                      <div className="row">
                        <div className="col-lg-6 px-8">
                          <div className="mb-4">
                            <div className="text-gray-700 fw-bolder fs-6 pl-3">
                              First Name :
                            </div>
                            <div className="fw-bolder text-muted fs-7 w-120px mx-3">
                              {UserGetDetails.firstName ? _.startCase(UserGetDetails.firstName) : '--'}
                            </div>
                          </div>
                          <div className="mb-4">
                            <div className="text-gray-700 fw-bolder fs-6 pl-3">
                              Last Name :
                            </div>
                            <div className="fw-bolder text-muted fs-7 w-120px mx-3">
                              {UserGetDetails.lastName ? _.startCase(UserGetDetails.lastName) : '--'}
                            </div>
                          </div>
                          <div className="mb-4">
                            <div className="text-gray-700 fw-bolder fs-6 pl-3">
                              Email :
                            </div>
                            <div className="fw-bolder text-muted fs-7 w-120px mx-3">
                              {UserGetDetails.email ? _.startCase(UserGetDetails.email) : '--'}
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6 px-8">
                          <div className="mb-4">
                            <div className="text-gray-700 fw-bolder fs-6 pl-3">
                              Phone Number :
                            </div>
                            <div className="fw-bolder text-muted fs-7 w-120px mx-3">
                              {UserGetDetails.mobile ? _.startCase(UserGetDetails.mobile) : '--'}
                            </div>
                          </div>
                          <div className="mb-4">
                            <div className="text-gray-700 fw-bolder fs-6 pl-3">
                              User Type :
                            </div>
                            <div className="fw-bolder text-muted fs-7 w-120px mx-3">
                              {UserGetDetails && UserGetDetails.userTypeId
                                ? _.startCase(UserGetDetails.userTypeId.userType)
                                : '--'}
                            </div>
                          </div>
                          <div className="mb-4">
                            <div className="text-gray-700 fw-bolder fs-6 pl-3">
                              User Role :
                            </div>
                            <div className="fw-bolder text-muted fs-7 w-120px mx-3">
                              {UserGetDetails && UserGetDetails.roleId
                                ? _.startCase(UserGetDetails.roleId.role)
                                : '--'}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-sm-10 col-md-10  mx-auto">
                  <div
                    className='card min-h-250px'
                  >
                    <div className="card-header border-0">
                      <div className="card-title align-items-start flex-column text-primary fw-bolder">
                        Assign Partner
                      </div>
                    </div>
                    <div className='col-lg-5 pr-3 mx-auto'>
                      <h6 className='mb-3'>Partner</h6>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='partnerId'
                        className='basic-single'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangePartner}
                        options={partnerOption}
                        value={selectedPartnerOption}
                      />
                      {errors && errors.partnerId && (
                        <div className='rr mt-1'>
                          <style>{'.rr{color:red}'}</style>
                          {errors.partnerId}
                        </div>
                      )}
                    </div>
                    <div className='col-lg-5 pr-3 mx-auto mt-5'>
                      <h6 className='mb-3'>Client</h6>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='clientId'
                        className='basic-single'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeClient}
                        options={clientOption}
                        value={selectedClientOption}
                        isLoading={loadingClient}
                      />
                      {errors && errors.clientId && (
                        <div className='rr mt-1'>
                          <style>{'.rr{color:red}'}</style>
                          {errors.clientId}
                        </div>
                      )}
                    </div>
                    <div className='col-lg-11 justify-content-end mb-6'>
                      <button
                        className='btn btn-blue mt-7 fa-pull-right'
                        onClick={(event) => {
                          handleSubmit(event)
                        }}
                      >
                        {loadingAAU
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true'
                            />
                          )
                          : (
                            'Submit'
                          )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-4">
                <div className={`card ${className}`}>
                  <div className="card-body py-3">
                    <div className="table-responsive">
                      <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                        <thead>
                          <tr className="fw-bold fs-6 text-gray-800">
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>S.No</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Partner Name</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Client Name</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Mobile Number</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Email</span>
                              </div>
                            </th>
                            <th className="min-w-80px text-start">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            !loadingGAU
                              ? (
                                getAssignUser &&
                                  getAssignUser.length > 0
                                  ? (
                                    getAssignUser.map((item, i) => {
                                      return (
                                        <tr
                                          key={i}
                                          style={
                                            i === 0
                                              ? { borderColor: "black" }
                                              : { borderColor: "white" }
                                          }
                                        >
                                          <td className="pb-0 pt-5  text-start">
                                            {i + 1}
                                          </td>
                                          <td className="pb-0 pt-5  text-start">
                                            {item.partnerId && item.partnerId.partnerFirstName ? item.partnerId.partnerFirstName : '--'} &nbsp;
                                            {item.partnerId && item.partnerId.partnerLastName ? item.partnerId.partnerLastName : '--'}
                                          </td>
                                          <td className="pb-0 pt-5  text-start">
                                            {item.clientId && item.clientId.company ? item.clientId.company : '--'}
                                          </td>
                                          <td className="pb-0 pt-5  text-start">
                                            {item.clientId && item.clientId.clientPhoneNumber ? item.clientId.clientPhoneNumber : '--'}
                                          </td>
                                          <td className="pb-0 pt-5  text-start">
                                            {item.clientId && item.clientId.clientEmail ? item.clientId.clientEmail : '--'}
                                          </td>
                                          <td className="pb-0 pt-3 text-start">
                                            <div className="my-auto d-flex">
                                              <button
                                                className='btn btn-icon btn-bg-light btn-active-color-danger btn-sm'
                                                onClick={() => {
                                                  onDeleteItem(item._id)
                                                }}
                                              >
                                                {/* eslint-disable */}
                                                <KTSVG
                                                  path="/media/icons/duotune/general/gen027.svg"
                                                  className="svg-icon-3"
                                                />
                                                {/* eslint-enable */}
                                              </button>
                                            </div>
                                          </td>
                                        </tr>
                                      )
                                    })
                                  )
                                  : (
                                    <tr className='text-center py-3'>
                                      <td colSpan='100%'>No record(s) found</td>
                                    </tr>
                                  )
                              )
                              : (
                                <tr>
                                  <td colSpan='100%' className='text-center'>
                                    <div className='spinner-border text-primary m-5' role='status' />
                                  </td>
                                </tr>
                              )
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { userStore, assignUserStore, deleteAssignUserStore } = state
  return {
    getPartner: state && state.partnerStore && state.partnerStore.getPartner,
    loading: state && state.partnerStore && state.partnerStore.loading,
    getClient: state && state.clientStore && state.clientStore.getClient,
    loadingClient: state && state.clientStore && state.clientStore.loading,
    UserGetDetails: userStore && userStore.userGetDetails ? userStore.userGetDetails : {},
    statusGUD: userStore && userStore.statusGUD ? userStore.statusGUD : '',
    messagesGUD: userStore && userStore.messagesGUD ? userStore.messagesGUD : '',
    loadingGUD: userStore && userStore.loadingGUD ? userStore.loadingGUD : false,
    loadingAAU:
      state && state.addAssignUserStore && state.addAssignUserStore.loadingAAU,
    statusAAU:
      state && state.addAssignUserStore && state.addAssignUserStore.statusAAU,
    messageAAU:
      state && state.addAssignUserStore && state.addAssignUserStore.messageAAU,
    dataAAU: state && state.addAssignUserStore && state.addAssignUserStore.dataAAU,
    getAssignUser:
      assignUserStore && assignUserStore.getAssignUser ? assignUserStore.getAssignUser : {},
    loadingGAU: assignUserStore && assignUserStore.loading ? assignUserStore.loading : false,
    statusDeleteAssignUser:
      deleteAssignUserStore &&
        deleteAssignUserStore.statusDeleteAssignUser ?
        deleteAssignUserStore.statusDeleteAssignUser : '',
    messageDeleteAssignUser:
      deleteAssignUserStore &&
        deleteAssignUserStore.messageDeleteAssignUser ?
        deleteAssignUserStore.messageDeleteAssignUser : ''
  }
}

const mapDispatchToProps = dispatch => ({
  getPartnerDispatch: () =>
    dispatch(partnerActions.getPartner()),
  partnerActions: (data) =>
    dispatch(partnerActions.getPartner(data)),
  getClientDispatch: (params) =>
    dispatch(clientActions.getClient(params)),
  clientActions: (data) =>
    dispatch(clientActions.getClient(data)),
  addAssignUserDispatch: (data) =>
    dispatch(addAssignUserActions.add(data)),
  clearaddAssignUserDispatch: () =>
    dispatch(addAssignUserActions.clear()),
  getUserDetailsDispatch: (id) =>
    dispatch(userGetDetailsActions.getUserDetails(id)),
  clearUserDetailsDispatch: () =>
    dispatch(userGetDetailsActions.clearUserDetails()),
  getAssignUserDispatch: (params) =>
    dispatch(assignUserActions.getAssignUser(params)),
  deleteAssignUserDispatch: (params) =>
    dispatch(deleteAssignUserActions.delete(params)),
  clearDeleteAssignUserDispatch: (params) =>
    dispatch(deleteAssignUserActions.clear(params))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssignClient)
