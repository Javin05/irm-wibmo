import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { userActions, deleteUsersActions } from '../../store/actions'
import { exportActions } from '../../store/actions/export'
import { connect } from 'react-redux'
import { getUserPermissions } from '../../utils/helper'
import { Can } from '../../theme/layout/components/can'
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  DATE,
  API_URL,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  DELETE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from '../../utils/constants'
import { warningAlert, confirmAlert, confirmationAlert } from '../../utils/alerts'
import ReactPaginate from 'react-paginate'
import userPlaceholder from '../../assets/userPlaceholder.png'
import AddUser from './AddUser'
import moment from 'moment'
import SearchList from './SearchList'

const UserList = (props) => {
  const {
    className,
    getUserDispatch,
    getUser,
    loading,
    messageDeleteUsers,
    statusDeleteUsers,
    deleteUserDispatch,
    clearDeleteUserDispatch
  } = props

  const url = useLocation().pathname
  const getUsersPermissions = getUserPermissions(url)
  const [show, setShow] = useState(false)
  const [searchParams, setSearchParams] = useState()
  const [editMode, setEditMode] = useState(false)
  const [limit, setLimit] = useState(25)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sorting, setSorting] = useState({
    firstName: false,
    lastName: false,
    email: false,
    mobile: false,
    roleId: false,
    password: false,
    image: false,
    lastLoggedAt: false
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
      ...searchParams,
      // tag: "IRM"
    }
    getUserDispatch(params)
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)

    const params = {
      limit: value,
      page: activePageNumber,
      ...searchParams,
      tag: "IRM"
    }
    getUserDispatch(params)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_USER,
      'warning',
      'Yes',
      'No',
      () => {
        deleteUserDispatch(id)
      },
      () => { }
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const paginationParams = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      tag: "IRM"
    }
    setActivePageNumber(pageNumber)
    getUserDispatch(paginationParams)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC',
        tag: "IRM"
      }
      getUserDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC',
        tag: "IRM"
      }
      getUserDispatch(params)
    }
  }

  const totalPages =
    getUser && getUser.count
      ? Math.ceil(parseInt(getUser && getUser.count) / limit)
      : 1

  const onConfirm = () => {
    const params = {
      limit: limit,
      page: 1,
      tag: "IRM"
    }
    getUserDispatch(params)
  }

  useEffect(() => {
    if (statusDeleteUsers === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageDeleteUsers,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearDeleteUserDispatch()
    } else if (statusDeleteUsers === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageDeleteUsers,
        '',
        'Ok'
      )
      clearDeleteUserDispatch()
    }
  }, [statusDeleteUsers])

  return (
    <>
      <AddUser setShow={setShow} show={show} editMode={editMode} />
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {getUser && getUser.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {getUser.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto'>
              <div className='my-auto me-5'>
                <SearchList
                  setSearchParams={setSearchParams}
                  limit={limit}
                  activePageNumber={activePageNumber}
                  setActivePageNumber={setActivePageNumber}
                />
              </div>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <div className='my-auto me-3'>
                  <button
                    type='button'
                    className='btn btn-sm btn-light-primary font-5vw pull-right'
                    data-toggle='modal'
                    data-target='#addModal'
                    onClick={() => {
                      setShow(true)
                      setEditMode(false)
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    {/* eslint-disable */}
                    Add Users
                  </button>
                </div>
              </Can>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border">
              <thead className="text-start text-muted fw-bolder fs-7 gs-0">
                <tr>
                  <th>
                    <div className='d-flex'>
                      <span>User</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('firstName')}
                        >
                          <i
                            className={`bi ${sorting.firstName
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-green`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Phone Number</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('mobile')}
                        >
                          <i
                            className={`bi ${sorting.mobile
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-green`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Email</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('email')}
                        >
                          <i
                            className={`bi ${sorting.email
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-green`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>User Type</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('userTypeId')}
                        >
                          <i
                            className={`bi ${sorting.userTypeId
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-green`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Role</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('roleId')}
                        >
                          <i
                            className={`bi ${sorting.roleId
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-green`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Last Login</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('lastLoggedAt')}
                        >
                          <i
                            className={`bi ${sorting.lastLoggedAt
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-green`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Status</span>
                    </div>
                  </th>
                  <th>
                  </th>
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={UPDATE_DELETE_PERMISSION}
                  >
                    <th>
                      <div className="min-w-70px">
                        <span>Action</span>
                      </div>
                    </th>
                  </Can>
                </tr>
              </thead>
              <tbody className="text-start text-gray-700 fw-bolder fs-7 gs-0">
                {!loading
                  ? (
                    getUser && getUser.result && getUser.result
                      ? (
                        getUser.result.map((user, _id) => {
                          return (
                            <tr
                              key={_id}
                              style={
                                _id === 0
                                  ? { borderColor: 'black' }
                                  : { borderColor: 'white' }
                              }
                            >
                              <td className="d-flex align-items-center">
                                <div className="symbol symbol-circle symbol-25px overflow-hidden me-3">
                                  <div>
                                    <img
                                      className='h-50px w-50px'
                                      src={`${API_URL}/uploads/${user.image}`}
                                      onError={(e) => { e.target.src = userPlaceholder }}
                                    />
                                  </div>
                                </div>
                                <div className="d-flex flex-column">
                                  <a className="text-gray-400 fs-6 fw-bolder text-hover-primary mb-1">
                                    {user.firstName
                                      ? _.startCase(user.firstName)
                                      : '--'} &nbsp;
                                    {user.lastName
                                      ? _.startCase(user.lastName)
                                      : '--'}
                                  </a>
                                </div>
                              </td>
                              <td className='ellipsis'>
                                {user.mobile
                                  ? user.mobile
                                  : '--'}
                              </td>
                              <td className='ellipsis'>
                                {user.email
                                  ? user.email
                                  : '--'}
                              </td>
                              <td className='ellipsis'>
                                {user && user.userTypeId
                                  ? _.startCase(user.userTypeId.userType)
                                  : '--'}
                              </td>
                              <td className='ellipsis'>
                                {user && user.roleId
                                  ? _.startCase(user.roleId.role)
                                  : '--'}
                              </td>
                              <td className='ellipsis'>
                                {user && user.lastLoggedAt
                                  ? moment(user.lastLoggedAt).format(
                                    DATE.DATE_FORMAT_MINS
                                  )
                                  : '--'}
                              </td>
                              {user.status === 'ACTIVE'
                                ? (
                                  <td>
                                    <span className='badge badge-success'>
                                      {user.status ? user.status : '--'}
                                    </span>
                                  </td>
                                )
                                : user.status === 'INACTIVE'
                                  ? (
                                    <span className='badge badge-warning'>
                                      {user.status ? user.status : '--'}
                                    </span>
                                  )
                                  : (
                                    ''
                                  )}
                              <td className='min-width-150px'>
                                <div className='d-flex'>
                                  <Link
                                    to={`/assign-partner/${user._id}`}
                                    class="btn btn-sm btn-secondary m-1"
                                    data-toggle="tooltip"
                                    data-placement="right"
                                    title="Assign Partner"
                                  >
                                    <i className="bi bi-check-circle-fill" />
                                  </Link> &nbsp;
                                  <Link
                                    to={`/assign-client/${user._id}`}
                                    class="btn btn-sm btn-secondary m-1"
                                    data-toggle="tooltip"
                                    data-placement="right"
                                    title="Assign Client"
                                  >
                                    <i className="bi bi-person-check-fill" />
                                  </Link>
                                </div>
                              </td>
                              <Can
                                permissons={getUsersPermissions}
                                componentPermissions={UPDATE_DELETE_PERMISSION}
                              >
                                <td className='min-width-150px'>
                                  <div>
                                    <Can
                                      permissons={getUsersPermissions}
                                      componentPermissions={UPDATE_PERMISSION}
                                    >
                                      <Link
                                        to={`/user-management/update-user/${user._id}`}
                                        className='btn btn-icon btn-icon-primary btn-sm w-10px h-10px '
                                      >
                                        <i className='bi bi-eye-fill' />
                                      </Link>
                                    </Can> &nbsp;
                                    <Can
                                      permissons={getUsersPermissions}
                                      componentPermissions={DELETE_PERMISSION}
                                    >
                                      <button
                                        className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm'
                                        onClick={() => {
                                          onDeleteItem(user._id)
                                        }}
                                      >
                                        {/* eslint-disable */}
                                        <KTSVG
                                          path="/media/icons/duotune/general/gen027.svg"
                                          className="svg-icon-3"
                                        />
                                        {/* eslint-enable */}
                                      </button>
                                    </Can>
                                  </div>
                                </td>
                              </Can>
                            </tr>
                          )
                        })
                      )
                      : (
                        <tr className='text-center py-3'>
                          <td colSpan='100%'>No record(s) found</td>
                        </tr>
                      )
                  )
                  : (
                    <tr>
                      <td colSpan='100%' className='text-center'>
                        <div className='spinner-border text-primary m-5' role='status' />
                      </td>
                    </tr>
                  )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  forcePage={activePageNumber - 1}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { deleteUsersStore, userStore } = state
  return {
    loading: state && state.userStore && state.userStore.loading,
    getUser:
      userStore && userStore.getUser ? userStore.getUser : {},
    count:
      userStore && userStore.count ? userStore.count : 0,
    statusDeleteUsers:
      deleteUsersStore && deleteUsersStore.statusDeleteUsers ? deleteUsersStore.statusDeleteUsers : '',
    messageDeleteUsers:
      deleteUsersStore && deleteUsersStore.messageDeleteUsers ? deleteUsersStore.messageDeleteUsers : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  clearPostsDispatch: () => dispatch(exportActions.clearPosts()),
  getUserDispatch: (params) => dispatch(userActions.getUser(params)),
  deleteUserDispatch: (params) => dispatch(deleteUsersActions.delete(params)),
  clearDeleteUserDispatch: () => dispatch(deleteUsersActions.clear())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
