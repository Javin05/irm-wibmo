import { USER_MANAGEMENT_ERROR, REGEX, USER_ERROR } from '../../utils/constants'
import { setDynamicPasswordRegex } from '../../utils/helper'
// import { searchDefaultCols } from '../IssuerBankAlerts/fromData/showSearcFields'

export const userValidation = (values, setErrors, configData) => {
  const errors = {}
  if (!values.firstName) {
    errors.firstName = USER_MANAGEMENT_ERROR.USER_FNAME
  }
  if (!values.lastName) {
    errors.lastName = USER_MANAGEMENT_ERROR.USER_LNAME
  }
  if (!values.email) {
    errors.email = USER_MANAGEMENT_ERROR.USER_EMAIL
  }
  if (values.email && !REGEX.EMAIL.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('/')
    const emailName = getEmailName && getEmailName[1]
    if (REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
  }
  if (!values.mobile) {
    errors.mobile = USER_MANAGEMENT_ERROR.USER_PHONE
  }
  if (!values.userTypeId) {
    errors.userTypeId = USER_MANAGEMENT_ERROR.USER_TYPE_REQUIRED
  }
  if (!values.roleId) {
    errors.roleId = USER_MANAGEMENT_ERROR.USER_ROLE_REQUIRED
  }
  if (!values.password) {
    errors.password = USER_ERROR.PASSWORD_REQUIRED
  } else if (!REGEX.PASSWORD_MIN_MAX_LENGTH.test(values.password)) {
    errors.password = USER_ERROR.PASSWORD_MIN_MAX_LENGTH
  }
  const getPasswordRegex = setDynamicPasswordRegex(configData && configData.passwordMinChar, configData && configData.passwordMaxChar)

  // if (!values.password) {
  //   errors.password = USER_ERROR.PASSWORD_REQUIRED
  // } else if (!getPasswordRegex.test(values.password)) {
  //   errors.password = USER_ERROR.PASSWORD_MIN_MAX_LENGTH
  // }
  if (!values.cPassword) {
    errors.cPassword = USER_ERROR.CONFIRM_PASSWORD_REQUIRED
  }
  if (values.password !== values.cPassword) {
    errors.cPassword = USER_ERROR.PASSWORD_SAME
  }
  setErrors(errors)
  return errors
}

export const userEditValidation = (values, setErrors) => {
  const errors = {}
  if (!values.firstName) {
    errors.firstName = USER_MANAGEMENT_ERROR.USER_FNAME
  }
  if (!values.lastName) {
    errors.lastName = USER_MANAGEMENT_ERROR.USER_LNAME
  }
  if (!values.email) {
    errors.email = USER_MANAGEMENT_ERROR.USER_EMAIL
  }
  if (values.email && !REGEX.EMAIL.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('/')
    const emailName = getEmailName && getEmailName[1]
    if (REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
  }
  if (!values.mobile) {
    errors.mobile = USER_MANAGEMENT_ERROR.USER_PHONE
  }
  if (!values.roleId) {
    errors.roleId = USER_MANAGEMENT_ERROR.USER_ROLE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const userEdiPasstValidation = (values, setErrors) => {
  const errors = {}
  if (!values.password) {
    errors.password = USER_ERROR.PASSWORD_REQUIRED
  } else if (!REGEX.PASSWORD_MIN_MAX_LENGTH.test(values.password)) {
    errors.password = USER_ERROR.PASSWORD_MIN_MAX_LENGTH
  }
  if (!values.cPassword) {
    errors.cPassword = USER_ERROR.CONFIRM_PASSWORD_REQUIRED
  }
  if (values.password !== values.cPassword) {
    errors.cPassword = USER_ERROR.PASSWORD_SAME
  }
  setErrors(errors)
  return errors
}

export const partnerValidation = (values, setErrors) => {
  const errors = {}
  if (!values.partnerId) {
    errors.partnerId = USER_ERROR.PARTNER_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const clientValidation = (values, setErrors) => {
  const errors = {}
  if (!values.clientId) {
    errors.clientId = USER_ERROR.CLIENT_REQUIRED
  }
  setErrors(errors)
  return errors
}
