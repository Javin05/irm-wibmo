import _ from 'lodash'

export const setUserData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      mobile: data.mobile,
      userTypeId: data.userTypeId,
      roleId: data.roleId,
      image: data.image
    }
  }
}

export const setUserPassData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      password: data.password,
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      mobile: data.mobile,
      userTypeId: data.userTypeId,
      roleId: data.roleId,
      image: data.image
    }
  }
}
