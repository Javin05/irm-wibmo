import React, { useState, useEffect } from 'react'
import { KTSVG } from '../../theme/helpers'
import { connect } from 'react-redux'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import color from '../../utils/colors'
import _ from 'lodash'
import {
  userRolesActions,
  userActions,
  userTypesActions
} from '../../store/actions'
import { REGEX } from '../../utils/constants'

function SearchList(props) {
  const {
    getDataUserType,
    userRoleData,
    getUserroleDispatch,
    getUserDispatch,
    setSearchParams,
    activePageNumber,
    setActivePageNumber,
    limit,
    getUserTypeDispatch,
    loadingGetUR
  } = props
  const [, setShow] = useState(false)
  const [selectedUserTypeOption, setSelectedUserTypeOption] = useState('')
  const [userTypeOption, setUserTypeOption] = useState()
  const [selectedUserRoleOption, setSelectedUserRoleOption] = useState('')
  const [initTypeUpdate, setInitTypeUpdate] = useState(false)
  const [userRoleOption, setUserRoleOption] = useState()
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    userTypeId: '',
    roleId: '',
    tag: "IRM"
  })

  const handleChange = (e) => {
    e.persist()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
  }

  const resetState = () => {
    setFormData({
      firstName: '',
      lastName: '',
      email: '',
      mobile: '',
      userTypeId: '',
      roleId: ''
    })
    setSelectedUserRoleOption()
  }

  const handleSearch = () => {
    const params = {
      limit: limit,
      page: 1,
      tag: "IRM"
    }
    setActivePageNumber(1)
    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key] !== '') {
        params[key] = formData[key]
      }
    }
    const result = _.omit(params, ['limit', 'page']);
    setSearchParams(_.pickBy(result))
    setShow(false)
    getUserDispatch(params)
    resetState()
  }

  const handleReset = () => {
    resetState()
    setSearchParams()
    const params = {
      limit: limit,
      page: activePageNumber,
      tag: "IRM"
    }
    getUserDispatch(params)
  }

  useEffect(() => {
    const params ={
      tag: "IRM"
    }
    getUserTypeDispatch(params)
  }, [])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? _.startCase(item[name]) : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    if (initTypeUpdate) {
      const data = getDefaultOptions(userRoleData, 'role')
      setUserRoleOption(data)
    }
  }, [userRoleData])

  const handleChangeRole = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserRoleOption(selectedOption)
      setFormData((values) => ({
        ...values,
        roleId: selectedOption.value
      }))
    } else {
      setSelectedUserRoleOption()
      setFormData((values) => ({ ...values, roleId: '' }))
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getDataUserType, 'userType')
    setUserTypeOption(data)
  }, [getDataUserType])

  const handleChangeUserType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserTypeOption(selectedOption)
      setFormData((values) => ({
        ...values,
        userTypeId: selectedOption.value
      }))
      setInitTypeUpdate(true)
      const roleParams = {
        skipPagination: true,
        userTypeId: selectedOption.value,
        tag: "IRM"
      }
      getUserroleDispatch(roleParams)
    } else {
      setSelectedUserTypeOption()
      setFormData((values) => ({ ...values, userTypeId: '', roleId: '' }))
    }
  }

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  return (
    <>
      <div className='d-flex bd-highlight'>
        <button
          type='button'
          className='btn btn-sm btn-light-primary font-5vw pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>
      <div
        className='modal fade'
        id='searchModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1000px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Search</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => {
                  setShow(false)
                  resetState()
                }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header bg-lightBlue'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs  font-weight-bold mb-2 form-label'>
                          First Name:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='firstName'
                            type='text'
                            className='form-control'
                            placeholder='First Name'
                            value={formData.firstName || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            onKeyPress={(e) => {
                              if (!REGEX.TEXT.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-2'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Last Name:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='lastName'
                            type='text'
                            className='form-control'
                            placeholder='Last Name'
                            value={formData.lastName || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            onKeyPress={(e) => {
                              if (!REGEX.TEXT.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Email:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='email'
                            type='email'
                            className='form-control'
                            placeholder='Email'
                            value={formData.email || ''}
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Phone Number:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='mobile'
                            type='text'
                            className='form-control'
                            placeholder='Phone Number'
                            value={formData.mobile || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            maxLength={10}
                            onKeyPress={(e) => {
                              if (!REGEX.NUMERIC.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs  font-weight-bold mb-2 required form-label'>User Type:</label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='userTypeId'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeUserType}
                            options={userTypeOption}
                            value={selectedUserTypeOption}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs  font-weight-bold mb-2 required form-label'>User Role:</label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name="roleId"
                            className="basic-single"
                            classNamePrefix="select"
                            handleChangeReactSelect={handleChangeRole}
                            options={userRoleOption}
                            value={selectedUserRoleOption}
                            isLoading={loadingGetUR}
                            noOptionsMessage={() => 'Please select User Type!'}
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mt-4'>
                      <div className='col-lg-6' />
                      <div className='col-lg-6'>
                        <div className='col-lg-12'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                            onClick={() => handleSearch()}
                            data-dismiss='modal'
                          >
                            Search
                          </button>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                            onClick={() => handleReset()}
                          >
                            Reset
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div >
    </>
  )
}

const mapStateToProps = (state) => {
  const { userroleStore, usertypeStore } = state
  return {
    userRoleData:
      userroleStore && userroleStore.userRoleData
        ? userroleStore.userRoleData
        : {},
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
    loadingGetUR: state && state.userroleStore
      && state.userroleStore.loadingGetUR
  }
}

const mapDispatchToProps = dispatch => ({
  getUserroleDispatch: (params) =>
    dispatch(userRolesActions.getUserrole(params)),
  getUserDispatch: (params) => dispatch(userActions.getUser(params)),
  getUserTypeDispatch: (params) =>
    dispatch(userTypesActions.getUserType(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)
