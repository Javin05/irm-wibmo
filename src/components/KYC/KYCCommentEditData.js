import { KYC_COMMENT_ERROR } from "../../utils/constants";
import _ from 'lodash'

export const kycCommentEditValidation = (values, setErrors) => {
    const errors = {}
    if (!values.kycStatus) {
        errors.kycStatus = KYC_COMMENT_ERROR.KYC_STATUS
    }
    if (!values.comments) {
        errors.comments = KYC_COMMENT_ERROR.KYC_COMMENT_REQUIRED
    }
    setErrors(errors)
    return errors
}