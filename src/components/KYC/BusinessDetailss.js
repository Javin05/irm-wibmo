
import React, { useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import moment from 'moment'
import { KycContext } from '../kycDashboard/StaticComponent'

export default function BusinessData() {
  const userData = useContext(KycContext)
  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <h2> Business Details</h2>{" "}
          <i class="fa fa-times-circle-o" aria-hidden="true"></i>
        </Accordion.Header>
        <Accordion.Body>
          <Card>
            <Card.Body>
              <div
                className="row block_div"
              >
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  {/* <legend class="float-none w-auto fs-3 p-2 h6 font-weight-600 ms-2 mt-4">
                    Business Information
                  </legend> */}
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Name :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userData && userData.businessName ? userData.businessName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Type :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userData && userData.entityName ? userData.entityName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Cateogry :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userData && userData.businessCategory ? userData.businessCategory : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Website :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userData && userData.website ? userData.website : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Address :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userData && userData.businessAddress && userData.businessAddress.address ? userData.businessAddress.address : '--'},{userData && userData.businessAddress && userData.businessAddress.city},{userData && userData.businessAddress && userData.businessAddress.state},{userData && userData.businessAddress && userData.businessAddress.pincode},
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Business Description :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userData && userData.businessDescription ? userData.businessDescription : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
