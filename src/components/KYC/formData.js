import _ from 'lodash'

export const setKYCCommentData = (data) => {
    if (!_.isEmpty(data)) {
        return {
            kycStatus: data && data.kycStatus ? data.kycStatus : '',
            comments: data && data.comments ? data.comments : ''
        }
    }
}