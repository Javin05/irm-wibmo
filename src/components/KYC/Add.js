import React, { useState, useEffect } from "react";
import { Modal } from "../../theme/layout/components/modal/Modal";
import { connect } from "react-redux";
import { KTSVG } from "../../theme/helpers";
import { VerticalStepper } from "../../theme/layout/components/stepper/verticalStepper";
import {
  setLocalStorage,
  getLocalStorage,
  removeLocalStorage,
} from "../../utils/helper";
import { KYC_FORM } from "../../utils/constants";
import ShowFields from "./ShowFields";
import { useLocation } from "react-router-dom";
import ContactInfo from "./ContactInfo";
import BusinessForm from "./Business";
import BusinessDetailsForm from "./BusinessDetails";
import { Scrollbars } from "react-custom-scrollbars";
import { StateActions } from "../../store/actions";
import BankAccount from "./BankAccount";
import DocumentVerification from "./DocumentVerification";

function KYCAdd(props) {
  const { setShow, show, loading, getStateDispatch } = props;
  const [activeStep, setActiveStep] = useState(0);
  const [completedSteps, setCompletedSteps] = useState([-1]);
  const [array, setArray] = useState([]);
  const [clientDetails, setClientDetails] = useState({});
  const [summary, setSummary] = useState({});
  const [editMode, setEditMode] = useState(false);
  const active = getLocalStorage(KYC_FORM.ACTIVE_STEP);

  useEffect(() => {
    const params = {
      skipPagination: "true",
    };
    getStateDispatch(params);
  }, []);

  const stepperArray = [
    {
      stepperLabel: "Contact Info",
      Optional: "",
    },
    {
      stepperLabel: "Business Overview",
      Optional: "",
    },
    {
      stepperLabel: "Business Details",
      Optional: "",
    },
    {
      stepperLabel: "Bank Account",
      Optional: "",
    },
    {
      stepperLabel: "Document Verification",
      Optional: "",
    },
  ];
  const onClickNext = (currentId) => {
    const id = currentId - 1;
    setActiveStep(currentId);
    if (completedSteps.length === currentId) {
      if (completedSteps && !completedSteps.includes(id)) {
        setCompletedSteps((values) => [...values, id]);
        setLocalStorage(KYC_FORM.ACTIVE_STEP, currentId);
      }
    }
  };

  const goBack = (id) => {
    setActiveStep(id);
  };

  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update");
  const id = url && url[1];
  const urlName = pathName && pathName.split("update/");
  const clientId = urlName && urlName[1];

  const getDescendants = (arr, step) => {
    const val = [];
    arr.forEach((item, i) => {
      if (step > i) {
        val.push(i);
      }
    });
    return val;
  };
  const d = getDescendants(array, active);

  const editstepper = getDescendants(array, array.length);

  useEffect(() => {
    if (id) {
      setEditMode(true);
      // setArray(stepperEditArray)
      // clientDetailsDispatch(id)
    } else {
      setEditMode(false);
      setArray(stepperArray);
    }
  }, [id]);

  useEffect(() => {
    if (active) {
      setActiveStep(Number(active));
      setCompletedSteps([-1, ...d]);
    } else {
      setActiveStep(0);
      setCompletedSteps([-1]);
    }
  }, [active]);

  return (
    <>
      <Modal showModal={show} modalWidth={1000}>
        <div className="" id="userModal">
          <div>
            <div
              className="modal-dialog modal-dialog-centered mw-1000px"
              style={{ backgroundColor: "transparent" }}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h2 className="me-8">
                    {editMode ? "Update" : "Add"} Profile
                  </h2>
                  <button
                    type="button"
                    className="btn btn-lg btn-icon btn-active-light-danger close"
                    data-dismiss="modal"
                    onClick={() => {
                      setShow(false);
                      removeLocalStorage("CONTACT_DETAILS");
                      removeLocalStorage("BUSINESS");
                      removeLocalStorage("BUSINESS_DETAILS");
                      removeLocalStorage("BANK_ACCOUNT");
                      removeLocalStorage("VALIDNAME");
                      removeLocalStorage("BUSINESSTYPE");
                      // removeLocalStorage('USER_MENU_DETAILS')
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG
                      path="/media/icons/duotune/arrows/arr061.svg"
                      className="svg-icon-1"
                    />
                    {/* eslint-disable */}
                  </button>
                </div>
                <div className="container-fixed">
                  <div className="card-header">
                    <div className="card-body">
                      <div
                        className="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid"
                        id="kt_modal_create_app_stepper"
                      >
                        <div className="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                          <VerticalStepper
                            activeStep={activeStep}
                            setActiveStep={setActiveStep}
                            completedSteps={completedSteps}
                            stepperArr={array}
                            setCompletedSteps={setCompletedSteps}
                            onStepperClick={onClickNext}
                          />
                        </div>
                        <div className="flex-row-fluid py-lg-5 px-lg-15">
                          <ShowFields>
                            <Scrollbars
                              style={{ width: "100%", height: "400px" }}
                            >
                              {activeStep === 0 ? (
                                <ContactInfo
                                  onClickNext={onClickNext}
                                  goBack={goBack}
                                  setClientDetails={setClientDetails}
                                  setSummary={setSummary}
                                />
                              ) : null}
                              {activeStep === 1 ? (
                                <BusinessForm
                                  onClickNext={onClickNext}
                                  goBack={goBack}
                                  setClientDetails={setClientDetails}
                                  setSummary={setSummary}
                                />
                              ) : null}
                              {activeStep === 2 ? (
                                <BusinessDetailsForm
                                  onClickNext={onClickNext}
                                  goBack={goBack}
                                  setClientDetails={setClientDetails}
                                  setSummary={setSummary}
                                  clientDetails={clientDetails}
                                />
                              ) : null}
                              {activeStep === 3 ? (
                                <BankAccount
                                  onClickNext={onClickNext}
                                  goBack={goBack}
                                  setClientDetails={setClientDetails}
                                  setSummary={setSummary}
                                />
                              ) : null}
                              {activeStep === 4 ? (
                                <DocumentVerification
                                  onClickNext={onClickNext}
                                  goBack={goBack}
                                  setClientDetails={setClientDetails}
                                  setSummary={setSummary}
                                  clientDetails={clientDetails}
                                />
                              ) : null}
                            </Scrollbars>
                          </ShowFields>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
}
const mapStateToProps = (state) => {
  const {
    usertypeStore,
    addUsertypeStore,
    editUsertypeStore,
  } = state;
  return {
    getDataUserType:
    usertypeStore && usertypeStore.userTypeData ? usertypeStore.userTypeData : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(KYCAdd);
