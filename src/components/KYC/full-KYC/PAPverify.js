import React, { useState, useEffect, useCallback } from "react";
import { connect } from "react-redux";
import Dropzone, { useDropzone } from "react-dropzone";
import _ from "lodash";
import {
  DROPZONE_IMAGE_NAME_TYPES,
  DELETE_THUMBNAIL_IMAGE,
  EMPTY_DROPZONE,
  DROPZONE_MESSAGES,
  ENV,
} from "../../../constants/index";
import { UploadActions, UploadDocumentActions } from '../../../utils/dropzone/redux/action'
import { STATUS_RESPONSE } from '../../../utils/constants'
import { warningAlert } from "../../../utils/alerts"

function PAPverify(props) {

  const {
    defaultImages,
    multiple,
    type,
    name,
    flag,
    maxFileSize,
    formatType,
    dropzoneImage,
    postUploadDispatch,
    proofType,
    DocumentUploadDispatch,
    dropzoneDocument
  } = props;

  const [totalSize, setTotalSize] = useState(0);
  const [status, setStatus] = useState(
    "Upload"
  );
  const [show, setShow] = useState(false);
  const [error, setError] = useState(false);
  const isJsonFormat = _.includes(formatType, "application/json");

  const setFormatTypeError = (type, setStatus) => {
    switch (type) {
      case DROPZONE_IMAGE_NAME_TYPES.IMAGE:
        return setStatus('Image is InValid Type')
      case DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS:
        return setStatus('Document is Invalid Type')
    }
  };

  const onDrop = useCallback((acceptedFiles) => {
    setError(false);
    setStatus("Uploading...");
    let isValidFileFormat = false;
    const formData = new FormData();
    acceptedFiles.forEach((item) => {
      const fileType = item && item.type;
      const uploadedFileSize = item && item.size;
      isValidFileFormat = _.includes(formatType, fileType);
      const fileSize = Number(maxFileSize) * 1024 * 1024;
      setTotalSize(uploadedFileSize);
      if (isValidFileFormat) {
        if (uploadedFileSize < fileSize) {
          formData.append('type', `${proofType}`)
          formData.append('file_to_upload', item)
          if(proofType === 'pan'){
            postUploadDispatch(formData)
          }else if (proofType === 'cin') {
            DocumentUploadDispatch(formData)
          }
        } else {
          setError(true);
          setStatus(
            `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`
          );
        }
      } else {
        setError(true);
        setFormatTypeError(type, setStatus);
      }
    })
  }, [])

  const { getRootProps, getInputProps } = useDropzone({ onDrop })
  useEffect(() => {
    if(dropzoneImage && dropzoneImage.status === 'ok'){
      setShow(true)
      setStatus('Uploaded')
    } else if (dropzoneImage && dropzoneImage.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        dropzoneImage && dropzoneImage.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
    else{
      setShow(false)
    }
  },[dropzoneImage])

  return (
    <>
      <div
        {...getRootProps({
          className: `dropzone ${error && "dropzone-error"}`,
        })}
      >
        <input {...getInputProps({ multiple })} />
        <button className= {`${!show ? `btn btn-sm btn-light-primary` : 'btn btn-sm btn-success'}`}>
          {status}
        </button>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { dropzoneStore } = state;
  return {
    dropzoneImage: dropzoneStore && dropzoneStore.dropzoneImage ? dropzoneStore.dropzoneImage : {},
    dropzoneStatus: dropzoneStore && dropzoneStore.status ? dropzoneStore.status : "",
    dropzoneDocument: dropzoneStore && dropzoneStore.dropzoneDocument ? dropzoneStore.dropzoneDocument : {}
  };
};

const mapDispatchToProps = (dispatch) => ({
  postUploadDispatch: (params) => dispatch(UploadActions.uploadImage(params)),
  DocumentUploadDispatch: (params) => dispatch(UploadDocumentActions.uploadDocument(params)),
})
export default connect(mapStateToProps, mapDispatchToProps)(PAPverify);