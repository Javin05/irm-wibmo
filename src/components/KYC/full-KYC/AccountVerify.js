
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { contactInfoValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { DOCUMENT_URL, REGEX, STATUS_RESPONSE, DROPZONE_MESSAGES } from '../../../utils/constants'
import _, { values } from 'lodash'
import {
  KYCaccountAction,
  AccountUploadAction,
  chequeDetailsDocAction,
  FullKycValueAction
} from '../../../store/actions'
import { warningAlert, successAlert } from "../../../utils/alerts"
import {
  DROPZONE_IMAGE_NAME_TYPES,
  RESTRICTED_FILE_FORMAT_TYPE
} from "../../../constants/index";

function AccountVerification(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    kycAccountDispatch,
    getaccountDetail,
    accountDetailLoading,
    clearAccountDetailDispatch,
    setFullKycDetails,
    AccountUploadDocDispatch,
    AccountUploadRes,
    AccountUploadResLoading,
    clearAccountUploadDocDispatch,
    chequeDetailsDocDispatch,
    kycAllDataSaved,
    chequeUploadRes,
    clearChequeDetailsDocDispatch,
    chequeUploadLoading,
    fullKycDispatch,
    clearFullKycValueDispatch,
    FullKycResData,
    FullKycLoading
  } = props

  const cancelledFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [showForm, setShowForm] = useState(true)
  const [Data, setData] = useState(true)
  const [cancelledChequeUpload, setcancelledChequeUpload] = useState('Upload')
  const [showchecque, setShowchecque] = useState(false)
  const [formData, setFormData] = useState({
    accountDetails: '',
    ifscCode: '',
    accountHolderName: '',
    cancelledCheque: ''
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.accountDetails)) {
      errors.accountDetails = 'Account Number Is Required'
    }
    if (_.isEmpty(formData.ifscCode)) {
      errors.ifscCode = 'IFSC Is Required'
    }
    if (_.isEmpty(errors)) {
      kycAccountDispatch(formData)
      setLocalStorage('AccountVerify', JSON.stringify(formData))
      setData(formData)
      setFullKycDetails((values) => ({ ...values, Account: formData }))
    }
    setErrors(errors)
  }

  const proccedData = () => {
    const errors = {}
    if (_.isEmpty(formData.accountDetails)) {
      errors.accountDetails = 'Account Number Is Required'
    }else if (formData.accountDetails && !REGEX.ACCOUNT_NUMBER.test(formData.accountDetails)) {
      errors.accountDetails = 'Account Number Is Invalid'
    }
    if (_.isEmpty(formData.ifscCode)) {
      errors.ifscCode = 'IFSC Is Required'
    }else if (formData.ifscCode && !REGEX.IFSC_NUMBER.test(formData.ifscCode)) {
      errors.ifscCode = 'IFSC Is Invalid'
    } if (_.isEmpty(formData.cancelledCheque)) {
      errors.cancelledCheque = 'CancelledCheque Is Required'
    }
    if (_.isEmpty(errors)) {
      setLocalStorage('AccountVerify', JSON.stringify(formData))
      setData(formData)
      setFullKycDetails((values) => ({ ...values, Account: formData }))
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (getaccountDetail && getaccountDetail.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        `${getaccountDetail && getaccountDetail.message} ${getaccountDetail && getaccountDetail.name}`,
        'success',
      )
      onClickNext(4)
      clearAccountDetailDispatch()
    } else if (getaccountDetail && getaccountDetail.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        getaccountDetail && getaccountDetail.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearAccountDetailDispatch()
    }
  }, [getaccountDetail])

  const cancelledFilePanClick = (event) => {
    cancelledFilesInput.current.click(event);
  }

  const FileChangeHandler = (e) => {
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', 'cancelledCheque')
        data.append('file_to_upload', files)
        AccountUploadDocDispatch(data)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
        setShowchecque(false)
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID })
      setShowchecque(false)
    }
  }

  useEffect(() => {
    if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = AccountUploadRes && AccountUploadRes.data && AccountUploadRes.data.path
      setFormData((values) => ({ ...values, cancelledCheque: `${DOCUMENT_URL}${data}` }))
      const params = {
        cancelledCheque: `${DOCUMENT_URL}${data}`
      }
      chequeDetailsDocDispatch(kycAllDataSaved && kycAllDataSaved.id, params)
      setcancelledChequeUpload('Uploaded')
      setShowchecque(true)
      clearAccountUploadDocDispatch()
    } else if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, cancelledCheque: '' }))
      clearAccountUploadDocDispatch()
      setcancelledChequeUpload('Upload')
      setShowchecque(false)
    }
  }, [AccountUploadRes])

  useEffect(() => {
    if (chequeUploadRes && chequeUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setErrors({ ...errors, cancelledCheque: '' })
      fullKycDispatch(kycAllDataSaved && kycAllDataSaved.id, formData)
      // onClickNext(4)
      clearFullKycValueDispatch()
    } else if (chequeUploadRes && chequeUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setcancelledChequeUpload('Upload')
      setShowchecque(false)
      warningAlert(
        'error',
        chequeUploadRes && chequeUploadRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearChequeDetailsDocDispatch()
    }
  }, [chequeUploadRes])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(4)
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  useEffect(() => {
    return () => {
      setShowchecque(false)
      setFormData({
        accountDetails: '',
        ifscCode: '',
        accountHolderName: '',
        cancelledCheque: ''
      })
    }
  }, [])

  return (
    <>
      {
        <div className={`${chequeUploadLoading ? 'opacity-25 text-primary m-5 text-center' : 'opacity-100'}`} >
          {chequeUploadLoading && (
            <div>
              <span class="spinner-border text-primary opacity-100" role="status"></span>
              <span class="text-gray-800 fs-6 fw-semibold mt-5 ms-4 opacity-100">Loading...</span>
            </div>
          )}

          <div className='current' data-kt-stepper-element='content'>
            <div className='w-100'>
              <div className='fv-row mb-10'>
                <div className='row mb-4'>
                  <div className='col-lg-12'>
                    <label className='d-flex align-items-center fw-bold mb-2'>
                      <span className='required fw-bold fs-4'>Add bank account details linked to {``}</span>
                    </label>
                  </div>
                  <div className='col-lg-12'>
                    <label className='d-flex align-items-center mb-2'>
                      <span className='text-muted fs-6 fw-bold'>Payment from your customers will be transfered to this account.</span>
                      <i
                        className='fas fa-exclamation-circle ms-2 fs-7'
                        data-bs-toggle='tooltip'
                        title='Individual Pan'
                      />
                    </label>
                  </div>
                </div>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  name='accountDetails'
                  placeholder='Account Number'
                  onChange={(e) => handleChange(e)}
                  value={formData.accountDetails || ''}
                  maxLength={18}
                  onKeyPress={(e) => {
                    if (!REGEX.NUMERIC.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.accountDetails && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.accountDetails}
                  </div>
                )}
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid mt-4'
                  name='ifscCode'
                  placeholder='IFSC Code'
                  onChange={(e) => handleChange(e)}
                  value={formData.ifscCode || ''}
                  maxLength={11}
                />
                {errors && errors.ifscCode && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.ifscCode}
                  </div>
                )}

                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid mt-4'
                  name='accountHolderName'
                  placeholder='Account Holder Name'
                  onChange={(e) => handleChange(e)}
                  value={formData.accountHolderName || ''}
                  maxLength={30}
                  onKeyPress={(e) => {
                    if (!REGEX.TEXT.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.accountHolderName && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.accountHolderName}
                  </div>
                )}

                <input
                  type="file"
                  className="d-none mt-4"
                  name="cancelledCheque"
                  id="cancelledCheque"
                  multiple={false}
                  ref={cancelledFilesInput}
                  onChange={FileChangeHandler} />
                <span className='me-4 fs-5 fw-bolder'>
                  Cancelled Cheque :
                </span>
                <button type="button"
                  className={`${!showchecque ? 'btn btn-light-primary btn-sm mt-4' : 'btn btn-sm btn-success mt-4'}`}
                  onClick={cancelledFilePanClick}>
                  {AccountUploadResLoading
                    ? (
                      'Uploading...'
                    )
                    : (
                      <>
                        {cancelledChequeUpload}
                      </>
                    )}
                </button>

                {errors && errors.cancelledCheque && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.cancelledCheque}
                  </div>
                )}
              </div>
              <div className='fv-row mb-4'>
                <div className='d-flex flex-stack pt-10 justify-content-end'>
                  <div>
                    {/* {
                  showForm ? (
                    <button type='submit' className={`btn btn-sm  btn-light-primary`}
                      onClick={() => { handleSubmit() }}
                      disabled={accountDetailLoading}
                    >
                      {!accountDetailLoading &&
                        <span className='indicator-label'>
                          <i className={`bi bi-bank`} />
                          Next
                        </span>
                      }
                      {accountDetailLoading && (
                        <span className='indicator-progress' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  ) :
                    null
                } */}

                    <button type='submit' className='btn btn-sm btn-light-primary ms-2'
                      onClick={() => { proccedData() }}
                    >
                      <span className='indicator-label'>
                        {!FullKycLoading &&
                          <span className='indicator-label'>
                            <i className='bi bi-person-fill' />
                            Proceed
                          </span>
                        }
                        {FullKycLoading && (
                          <span className='indicator-progress' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        // )
      }
    </>

  )
}

const mapStateToProps = (state) => ({
  getaccountDetail: state && state.accountDetailStore && state.accountDetailStore.accountDetail,
  accountDetailLoading: state && state.accountDetailStore && state.accountDetailStore.loading,
  AccountUploadResLoading: state && state.AccountUploadStore && state.AccountUploadStore.loading,
  AccountUploadRes: state && state.AccountUploadStore && state.AccountUploadStore.AccountUploadRes,
  chequeUploadRes: state && state.chequeUploadStore && state.chequeUploadStore.chequeUpload,
  chequeUploadLoading: state && state.chequeUploadStore && state.chequeUploadStore.loading,
  FullKycResData: state && state.FullKycValueStore && state.FullKycValueStore.FullKycValue,
  FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
})

const mapDispatchToProps = (dispatch) => ({
  kycAccountDispatch: (params) => dispatch(KYCaccountAction.KYCaccountDetail(params)),
  clearAccountDetailDispatch: (params) => dispatch(KYCaccountAction.clearKYCaccountDetail(params)),
  AccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.AccountUploadDoc(params)),
  clearAccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.clearAccountUploadDoc(params)),
  chequeDetailsDocDispatch: (id, params) => dispatch(chequeDetailsDocAction.chequeDetailsDoc(id, params)),
  clearChequeDetailsDocDispatch: () => dispatch(chequeDetailsDocAction.clearchequeDetailsDoc()),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountVerification)