
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import _, { values } from 'lodash'
import {
  CityActions,
  KYCAdharNumberAction
} from '../../../store/actions'
import { REGEX, STATUS_RESPONSE } from '../../../utils/constants'
import { warningAlert } from "../../../utils/alerts"

function AdharNumber(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    KycAadharDispatch,
    AadharVerifyRes,
    Aadharloading,
    clearKYCAdhaarDispatch
  } = props

  const [errors, setErrors] = useState({})
  const [showForm, setShowForm] = useState(true)
  const [formData, setFormData] = useState({
    aadharNumber: ''
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.aadharNumber)) {
      errors.aadharNumber = 'Aadhaar Number Is Required'
    }
    if (_.isEmpty(errors)) {
      KycAadharDispatch(formData)
      setClientDetails((values) => ({ ...values, aadharNumber: formData }))
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (AadharVerifyRes && AadharVerifyRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(4)
      clearKYCAdhaarDispatch()
    }
    else if (AadharVerifyRes && AadharVerifyRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        AadharVerifyRes && AadharVerifyRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCAdhaarDispatch()
    }
  }, [AadharVerifyRes])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Add an identify Proof for {`${``}`}</span>
                  </label>
                </div>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>We require this information for taxation and compliance.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='AadharNumber'
                    />
                  </label>
                </div>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='aadharNumber'
                placeholder='Adhaar Number'
                onChange={(e) => handleChange(e)}
                value={formData.aadharNumber || ''}
                onKeyPress={(e) => {
                  if (!REGEX.NUMERIC.test(e.key)) {
                    e.preventDefault()
                  }
                }}
              />
              {errors && errors.aadharNumber && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.aadharNumber}
                </div>
              )}
            </div>
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2'>
                  <button
                    onClick={() => { goBack(0) }}
                    type='button'
                    className='btn btn-sm btn-light-primary me-3'
                  >
                    <KTSVG
                      path='/media/icons/duotune/arrows/arr063.svg'
                      className='svg-icon-4 me-1'
                    />
                    Back
                  </button>
                </div>
                <div>
                  {
                    showForm ? (
                      <>
                        <button type='submit' className='btn btn-sm btn-primary me-3'
                          onClick={() => {
                            handleSubmit()
                          }}
                        >
                          <span className='indicator-label'
                          >
                            <i className={`bi bi-person-check-fill`} />
                          </span>
                          {Aadharloading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Next'
                            )}
                        </button>
                        <button type='submit' className='btn btn-sm btn-light-danger ms-2'
                          onClick={() => { onClickNext(4) }}
                        >
                          <span className='indicator-label'>
                            <KTSVG
                              path='/media/icons/duotune/files/fil007.svg'
                              className='svg-icon-3 me-2'
                            />
                            Skip
                          </span>
                        </button>
                      </>
                    ) :
                      null
                  }
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, AdharNumberStore } = state

  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    AadharVerifyRes: AdharNumberStore && AdharNumberStore.AadharVerify ? AdharNumberStore.AadharVerify : {},
    Aadharloading: AdharNumberStore && AdharNumberStore.loading ? AdharNumberStore.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  KycAadharDispatch: (params) => dispatch(KYCAdharNumberAction.KYCAdhaarNumber(params)),
  clearKYCAdhaarDispatch: (params) => dispatch(KYCAdharNumberAction.clearKYCAdhaarNumber(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(AdharNumber)