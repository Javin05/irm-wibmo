import React, { useState, useCallback, useEffect, useRef } from "react"
import Accordion from "react-bootstrap/Accordion"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import Card from "react-bootstrap/Card"
import { toAbsoluteUrl } from "../../theme/helpers"
import { Row, Col } from "react-bootstrap-v5"
import { AADHAR_MATCH } from '../../utils/constants'
import { Modal } from "../../theme/layout/components/modal/Modal"
import { KTSVG } from "../../theme/helpers"

import AllPagesPDFViewer from "./all-pages";

export default function VideoKYCdashboard(props) {
  const {
    UserDetails,
    DashboardAadharRes,
    DashboardPANres,
    DashboardCINres,
    VideoKycRes
  } = props
  const [currentImage, setCurrentImage] = useState('')
  const [isViewerOpen, setIsViewerOpen] = useState(false)

  const backsideMatch = VideoKycRes && VideoKycRes.data && VideoKycRes.data.details && VideoKycRes.data.details.backSideMatcting ? VideoKycRes.data.details.backSideMatcting : '--'
  const faceRegonize = VideoKycRes && VideoKycRes.data && VideoKycRes.data.details && VideoKycRes.data.details.faceRecognization ? VideoKycRes.data.details.faceRecognization : '--'
  const pageFound = VideoKycRes && VideoKycRes.data && VideoKycRes.data.details && VideoKycRes.data.details.header ? VideoKycRes.data.details.header : '--'

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index)
    setIsViewerOpen(true)
  }, [])


  return (
    <>
      <Modal showModal={isViewerOpen} modalWidth={1000}>
        <div
          className="modal-dialog modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="me-8">
              </h2>
              <button
                type="button"
                className="btn btn-lg btn-icon btn-active-light-danger close me-2 mt-2"
                data-dismiss="modal"
                onClick={() => {
                  setIsViewerOpen(false)
                }}
              >
                {/* eslint-disable */}
                <KTSVG
                  path="/media/icons/duotune/arrows/arr061.svg"
                  className="svg-icon-1"
                />
                {/* eslint-disable */}
              </button>
            </div>
            <div className="container-fixed">
              <div className="card-header">
                <div className="card-body mt-4 mb-8">
                  <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-10">
                      <div className="card card-custom overlay overflow-hidden">
                        <div className="card-body p-0">
                          <div className="overlay-wrapper">
                            <img
                              src={currentImage}
                              alt=""
                              className="w-100 rounded"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-1" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <h2>Video KYC</h2>{" "}
            <i class="fa fa-times-circle-o" aria-hidden="true"></i>
          </Accordion.Header>
          <Accordion.Body>
            <Card>
              <Tabs variant="pills">
                <Tab eventKey="document">
                  <Card.Body>
                    <div
                      className="row block_div"
                    >
                      {
                        pageFound === "Data Not Found" ? (pageFound) : (
                          <>
                            <div className="col-lg-5">
                              <h3 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                {backsideMatch && backsideMatch.faceSidename}
                              </h3>
                              <div className="row mt-4"
                                style={{
                                  backgroundColor: 'aliceblue',
                                  borderRadius: '10px'
                                }}
                              >
                                <div className="col-lg-11 ms-4">
                                  <div className="card card-custom overlay overflow-hidden">
                                    <div className="card-body p-0">
                                      <div className="overlay-wrapper">
                                        <img
                                          src={
                                            backsideMatch && backsideMatch.faceSide ? backsideMatch.faceSide : toAbsoluteUrl('/media/logos/no-image.png')}
                                          className="w-100 rounded"
                                        />
                                      </div>
                                      <div className="overlay-layer bg-dark bg-opacity-10">
                                        <a className="btn btn-light-primary btn-shadow"
                                          onClick={() => openImageViewer(backsideMatch && backsideMatch.faceSide ? backsideMatch.faceSide : '--')}
                                        >
                                          View
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="block_div col-lg-2" />
                            <div className="block_div col-lg-5">
                              <h3 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                {backsideMatch && backsideMatch.frontSideName ? backsideMatch.frontSideName : '--'}
                              </h3>
                              <div className="row mt-4"
                                style={{
                                  backgroundColor: 'aliceblue',
                                  borderRadius: '10px'
                                }}
                              >
                                <div className="col-lg-11 ms-5">
                                  <div className="card card-custom overlay overflow-hidden">
                                    <div className="card-body p-0">
                                      <div className="overlay-wrapper">
                                        <img
                                          src={backsideMatch && backsideMatch.frontSide ? backsideMatch.frontSide : toAbsoluteUrl('/media/logos/no-image.png')}
                                          className="w-100 rounded"
                                        />
                                      </div>
                                      <div className="overlay-layer bg-dark bg-opacity-10">
                                        <a className="btn btn-light-primary btn-shadow"
                                          onClick={() => openImageViewer(backsideMatch && backsideMatch.frontSide ? backsideMatch.frontSide : '--')}
                                        >
                                          View
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mt-8">
                              <h4 className="d-flex justify-content-center">{backsideMatch && backsideMatch.matchingCases ? backsideMatch.matchingCases : '--'}</h4>
                            </div>
                            <div className="separator separator-dashed border-secondary mt-4 mb-4" />

                            <div className="col-lg-5">
                              <h3 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                {faceRegonize && faceRegonize.faceSidename}
                              </h3>
                              <div className="row mt-4"
                                style={{
                                  backgroundColor: 'aliceblue',
                                  borderRadius: '10px'
                                }}
                              >
                                <div className="col-lg-11 ms-4">
                                  <div className="card card-custom overlay overflow-hidden">
                                    <div className="card-body p-0">
                                      <div className="overlay-wrapper">
                                        <img
                                          src={faceRegonize && faceRegonize.faceSide ? faceRegonize.faceSide : toAbsoluteUrl('/media/logos/no-image.png')}
                                          className="w-100 rounded"
                                        />
                                      </div>
                                      <div className="overlay-layer bg-dark bg-opacity-10">
                                        <a className="btn btn-light-primary btn-shadow"
                                          onClick={() => openImageViewer(faceRegonize && faceRegonize.faceSide ? faceRegonize.faceSide : '--')}
                                        >
                                          View
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="block_div col-lg-2" />
                            <div className="block_div col-lg-5">
                              <h3 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                {faceRegonize && faceRegonize.frontSideName ? faceRegonize.frontSideName : '--'}
                              </h3>
                              <div className="row mt-4"
                                style={{
                                  backgroundColor: 'aliceblue',
                                  borderRadius: '10px'
                                }}
                              >
                                <div className="col-lg-11 ms-5">
                                  <div className="card card-custom overlay overflow-hidden">
                                    <div className="card-body p-0">
                                      <div className="overlay-wrapper">
                                        <img
                                          src={faceRegonize && faceRegonize.frontSide ? faceRegonize.frontSide : toAbsoluteUrl('/media/logos/no-image.png')}
                                          className="w-100 rounded"
                                        />
                                      </div>
                                      <div className="overlay-layer bg-dark bg-opacity-10">
                                        <a className="btn btn-light-primary btn-shadow"
                                          onClick={() => openImageViewer(faceRegonize && faceRegonize.frontSide ? faceRegonize.frontSide : '--')}
                                        >
                                          View
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mt-8">
                              <h4 className="d-flex justify-content-center">{faceRegonize && faceRegonize.matchingCases ? faceRegonize.matchingCases : '--'}</h4>
                            </div>
                          </>
                        )
                      }
                    </div>
                  </Card.Body>
                </Tab>
              </Tabs>
            </Card>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  )
}
