
import React, { useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../theme/helpers'
import { connect } from 'react-redux'
import { businessValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../utils/helper'
import { KYC_FORM } from '../../utils/constants'
import _ from 'lodash'
function BusinessForm(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    loading,
    goBack
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)

  const [formData, setFormData] = useState({
    businessType: '',
    businessCategory: '',
    subCategory: '',
    businessDiscrition: '',
    websiteApp: '',
    acceptPaymentApp: '',
    acceptPaymentWeb: ''
  })


  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = () => {
    const errorMsg = businessValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      setClientDetails((values) => ({ ...values, business: formData }))
      // setSummary((values) => ({
      //   ...values,
      //   user: {
      //     roleId: getRolelabel && getRolelabel.label
      //   }
      // }))
      setLocalStorage(KYC_FORM.BUSINESS, JSON.stringify(formData))
    }
  }

  const handleNextClick = () => {
    const errorMsg = businessValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      onClickNext(2)
    }
  }

  return (
    <>
    <div>
    <h3 className='mb-4 d-flex justify-content-center'>Business Overview</h3>
      <div className='current' data-kt-stepper-element='content'>
        <div className='w-100'>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Business Type</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Business Type'
              />
            </label>
            <select
              className='form-select'
              name='businessType'
              data-control='select'
              data-placeholder='Select an option'
              data-allow-clear='true'
              onChange={(e) => handleChange(e)}
              value={formData.businessType || ''}
            >
              <option value=''>Select</option>
              <option value='proprietorShip'>Proprietor Ship</option>
              <option value='privatedLimited'>Privated Limited</option>
              <option value='publicLimited'>Public Limited</option>
              <option value='LLPIN'>LLPIN</option>
            </select>
            {errors && errors.businessType && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.businessType}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Business Category</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Business Category'
              />
            </label>
            <select
              className='form-select'
              data-control='select'
              data-placeholder='Select an option'
              data-allow-clear='true'
              onChange={(e) => handleChange(e)}
              value={formData.businessCategory || ''}
              name='businessCategory'

            >
              <option value=''>Select</option>
              <option value='IT and SoftWare'>IT and SoftWare</option>
              <option value='Production'>Production</option>
            </select>
            {errors && errors.businessCategory && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.businessCategory}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Sub Category</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Sub Category'
              />
            </label>
            <select
              className='form-select'
              data-control='select'
              data-placeholder='Select an option'
              data-allow-clear='true'
              onChange={(e) => handleChange(e)}
              value={formData.subCategory || ''}
              name='subCategory'
            >
              <option value=''>Select</option>
              <option value='Sas(SoftWare Service)'>Sas(SoftWare Service)</option>
              <option value='Production'>Production</option>
            </select>
            {errors && errors.subCategory && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.subCategory}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Business Discrition</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Business Discrition'
              ></i>
            </label>
            <textarea
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='businessDiscrition'
              placeholder='Business Discrition'
              onChange={(e) => handleChange(e)}
              value={formData.businessDiscrition || ''}
            />
            {errors && errors.businessDiscrition && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.businessDiscrition}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>How do You Wish to accept Payment</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='How do You Wish to accept Payment'
              ></i>
            </label>
            <div className="form-check form-check-custom form-check-solid mb-4">
              <label className='d-flex flex-stack mb-5 cursor-pointer'>
                <span className='form-check form-check-custom form-check-solid me-2'>
                  <input
                    className='form-check-input'
                    type='radio'
                    onChange={(e) => handleChange(e)}
                    value='Withoutwebsite/app'
                    name='websiteApp'
                  />
                </span>
                <span className='d-flex flex-column'>
                  <span className='fs-7 text-muted'>
                    Without website/app
                  </span>
                </span>
              </label>
              <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                <span className='form-check form-check-custom form-check-solid me-2'>
                  <input
                    className='form-check-input'
                    type='radio'
                    name='websiteApp'
                    onChange={(e) => handleChange(e)}
                    value='Onmywebsite/app'
                  />
                </span>
                <span className='d-flex flex-column'>
                  <span className='fs-7 text-muted'>
                    On my website/app
                  </span>
                </span>
              </label>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm">
              <input 
              className="form-check-input" 
              type="checkbox" 
              value="acceptPaymentWebsite" 
              id="flexRadioLg" 
              name='acceptPaymentWeb'
              onChange={(e) => handleChange(e)}
              />
              <label className="form-check-label text-muted" for="flexRadioLg">
               Accept Payments On Website
              </label>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
              <input 
              className="form-check-input" 
              type="checkbox" 
              value="acceptPaymentApp" 
              id="flexRadioLg" 
              name='acceptPaymentApp'
              onChange={(e) => handleChange(e)}
              />
              <label className="form-check-label text-muted" for="flexRadioLg">
               Accept Payments On App
              </label>
            </div>
          </div>
          <div className='fv-row'>
            <div className='d-flex flex-stack pt-10'>
              <div className='me-2'>
                <button
                  onClick={() => { goBack(0) }}
                  type='button'
                  className='btn btn-sm btn-light-primary me-3'
                >
                  <KTSVG
                    path='/media/icons/duotune/arrows/arr063.svg'
                    className='svg-icon-4 me-1'
                  />
                  Back
                </button>
              </div>
              <div>
                {
                  showForm ? (
                    <button type='submit' className='btn btn-sm btn-primary me-3'
                      onClick={(event) => {
                        handleSubmit(event)
                        handleNextClick()
                      }}
                    >
                      <span className='indicator-label'
                      >
                        <KTSVG
                          path='/media/icons/duotune/arrows/arr064.svg'
                          className='svg-icon-3 ms-2 me-0'
                        />
                      </span>
                      {loading
                        ? (
                          <span
                            className='spinner-border spinner-border-sm mx-3'
                            role='status'
                            aria-hidden='true'
                          />
                        )
                        : (
                          'Next'
                        )}
                    </button>
                  ) :
                    null
                }
              </div>
            </div>

          </div>
        </div>
      </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists
})

const mapDispatchToProps = (dispatch) => ({
  // getCountryDispatch: () => dispatch(CountryActions.getCountrys()),
})

export default connect(mapStateToProps, mapDispatchToProps)(BusinessForm)