import React, { useState, useCallback, useEffect, useRef, useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import Card from "react-bootstrap/Card"
import { toAbsoluteUrl } from "./../../theme/helpers"
import {
  UPDATE_PERMISSION,
  STATUS_RESPONSE,
  DROPZONE_MESSAGES,
  DOCUMENT_URL,
  RISKSTATUS
} from '../../utils/constants'
import Status from './StatusChange'
import {
  form80UploadAction,
  FullKycValueAction,
  DashComUploadAction,
  KYCUserAction,
  DashReUploadAction,
  uploadViewAction,
  KYCDocumentdeleteActions,
  CommomFileAction
} from '../../store/actions'
import {
  RESTRICTED_FILE_FORMAT_TYPE,
  PDF
} from "../../constants/index"
import _ from 'lodash'
import { connect } from "react-redux"
import { useLocation } from "react-router-dom"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts"
import { pdfjs } from 'react-pdf';
import Modal from 'react-bootstrap/Modal'
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'
import { Document, Page } from 'react-pdf';
import { KycContext } from '../kycDashboard/StaticComponent'
pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/legacy/build/pdf.worker.min.js`;

function DocumentView(props) {
  const {
    DashboardAadharRes,
    DashboardPANres,
    DashboardCINres,
    UENdashboardRes,
    formUploadDispatch,
    FormRes,
    clearformUploadDispatch,
    FormResLoadig,
    FullKycResData,
    clearFullKycValueDispatch,
    fullKycDispatch,
    DashComUploadDocDispatch,
    DashCommonUploadLoadig,
    DashCommonUploadRes,
    clearDashComUploadDocDispatch,
    getKYCUserDetailsDispatch,
    DashReUploadDocDispatch,
    DashReUploadRes,
    clearDashReUploadDocDispatch,
    DashReUploadResLoadig,
    viewUploadDocDispatch,
    uploadViewResLoadig,
    uploadViewRes,
    clearviewUploadDocDispatch,
    DeleteKYCDocumentData,
    deleteKYCDocumentDispatch,
    clearDeleteKYCDocumentDispatch,
    ClearCommonFileDispatch,
    CommonFileDispatch,
    CommonFileDocument,
    CommonFileLoading,
    FullKycResDataLoading
  } = props

  const userData = useContext(KycContext)
  const AdditionalDocumentFilesInput1 = useRef(null)
  const AdditionalDocumentFilesInput2 = useRef(null)
  const AdditionalDocumentFilesInput3 = useRef(null)
  const AdditionalDocumentFilesInput4 = useRef(null)

  const pathName = '/KYC'
  const getUsersPermissions = getUserPermissions(pathName, true)
  const hiddenFileInput = useRef(null)
  const commonFileInput = useRef(null)
  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [currentImage, setCurrentImage] = useState('')
  const [isViewerOpen, setIsViewerOpen] = useState(false)
  const [formData, setFormData] = useState({
    uploadDocument: '',
    documentName: '',
    documentNumber: '',
    documentId: '',
    First: '',
    documentType: '',
    id: '',
    companyName: ''
  })
  const [additionalDocumentFormData1, setAdditionalDocumentFormData1] = useState({
    label: '',
    document: ''
  })
  const [additionalDocumentFormData2, setAdditionalDocumentFormData2] = useState({
    label: '',
    document: ''
  })
  const [additionalDocumentFormData3, setAdditionalDocumentFormData3] = useState({
    label: '',
    document: ''
  })
  const [additionalDocumentFormData4, setAdditionalDocumentFormData4] = useState({
    label: '',
    document: ''
  })
  const [errors, setErrors] = useState({})
  const [fileGName, setFileGName] = useState('Upload')
  const [showformg, setShowformg] = useState(false)
  const [showCommon, setShowCommon] = useState(false)
  const [showComUp, setShowComUp] = useState(false)
  const [data, setData] = useState()
  const [currentIndex, setCurrentIndex] = useState(undefined)
  const [callApi, setcallApi] = useState(false)
  const [statusData, setStatusData] = useState(false)
  const [FirstOption, setFirstOption] = useState()
  const [selectedFirstOption, setSelectedFirstOption] = useState('')
  const [show, setShow] = useState(false)

  const [showAdditionalDocument1, setShowAdditionalDocument1] = useState(false)
  const [showAdditionalDocument2, setShowAdditionalDocument2] = useState(false)
  const [showAdditionalDocument3, setShowAdditionalDocument3] = useState(false)
  const [showAdditionalDocument4, setShowAdditionalDocument4] = useState(false)
  const [numPages, setNumPages] = useState()
  const [pageNumber, setPageNumber] = useState()
  const [pageNumber3, setPageNumber3] = useState()
  const [pageNumber4, setPageNumber4] = useState()

  const [additionalDocument1Loading, setAdditionalDocument1Loading] = useState(false)
  const [additionalDocument2Loading, setAdditionalDocument2Loading] = useState(false)
  const [additionalDocument3Loading, setAdditionalDocument3Loading] = useState(false)
  const [additionalDocument4Loading, setAdditionalDocument4Loading] = useState(false)
  const [currentPage, setCurrentPage] = useState(1)
  const [currentPages, setCurrentPages] = useState(1)
  const [currentPages3, setCurrentPages3] = useState(1)
  const [currentPages4, setCurrentPages4] = useState(1)
  const [editShow, setEditShow] = useState(false)
  const [showAdditionalDoc1, setShowAdditionalDoc1] = useState(false)

  const [editShow2, setEditShow2] = useState(false)
  const [showAdditionalDoc2, setShowAdditionalDoc2] = useState(false)

  const [editShow3, setEditShow3] = useState(false)
  const [showAdditionalDoc3, setShowAdditionalDoc3] = useState(false)

  const [editShow4, setEditShow4] = useState(false)
  const [showAdditionalDoc4, setShowAdditionalDoc4] = useState(false)

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  function onDocumentLoadSuccess2({ numPages }) {
    setPageNumber(numPages);
  }

  function onDocumentLoadSuccess3({ numPages }) {
    setPageNumber3(numPages);
  }

  function onDocumentLoadSuccess4({ numPages }) {
    setPageNumber4(numPages);
  }

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index)
    setIsViewerOpen(true)
  }, [])

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const closeImageViewer = () => {
    setCurrentImage(0)
    setIsViewerOpen(false)
  }

  const downloadFiles = (data, fileName) => {
    const link = document.createElement("a")
    const url = window.URL || window.webkitURL
    const revokeUrlAfterSec = 1000000
    link.href = data
    link.target = "_blank"
    link.download = fileName
    document.body.append(link)
    link.click()
    link.remove()
    setTimeout(() => url.revokeObjectURL(link.download), revokeUrlAfterSec)
  }

  const FileChangeHandler = (e) => {
    const { name } = e.target
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', `dashboardImg`)
        data.append('file_to_upload', files)
        formUploadDispatch(data)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
        setShowformg(false)
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID })
      setShowformg(false)
    }
  }

  const FileChangeHandlers = (e) => {
    const { name } = e.target
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', `dashboardImg`)
        data.append('file_to_upload', files)
        formUploadDispatch(data)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID });
    }
  }

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  }

  const handleComonClick = (event) => {
    const errors = {}
    if (_.isEmpty(formData.First)) {
      errors.First = 'Please Select The Value'
    }
    if (_.isEmpty(errors)) {
      commonFileInput.current.click(event)
      setShowComUp(true)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (FormRes && FormRes.status === STATUS_RESPONSE.SUCCESS_MSG && showComUp === true) {
      const data = FormRes && FormRes.data && FormRes.data.path
      setFileGName('Uploaded')
      setShowCommon(true)
      setFormData((values) => ({ ...values, uploadDocument: data }))
      setErrors({ ...errors, uploadDocument: "" })
    } else if (FormRes && FormRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = FormRes && FormRes.data && FormRes.data.path
      if (!_.isEmpty(data)) {
        setShowformg(true)
      }
      setFormData((values) => ({ ...values, uploadDocument: data }))
      setErrors({ ...errors, uploadDocument: "" })
      clearformUploadDispatch()
    }
    else if (FormRes && FormRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, uploadDocument: '' }))
      clearformUploadDispatch()
      setFileGName('Upload')
      setShowformg(false)
    }
  }, [FormRes])


  const documentSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.documentNumber)) {
      errors.documentNumber = 'This Field is Required'
    }
    if (_.isEmpty(formData.uploadDocument)) {
      errors.uploadDocument = 'Document is Required'
    }
    if (_.isEmpty(formData.First)) {
      errors.First = 'Entity is Required'
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        "documents": {
          [data && data.documentType]: [
            {
              uploadDocument: `${DOCUMENT_URL}${formData.uploadDocument}`,
              documentName: formData && formData.First,
              documentNumber: formData && formData.documentNumber,
              documentId: data && data.documentId,
            }
          ]
        }
      }
      DashReUploadDocDispatch(id, params)
    }
  }

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShowformg(false)
      setcallApi(false)
      setShow(false)
      clearFullKycValueDispatch()
      clearformUploadDispatch()
      setFormData({
        uploadDocument: '',
        documentName: '',
        documentNumber: '',
        documentId: '',
        documentType: ''
      })
      getKYCUserDetailsDispatch(id)
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      setShowformg(false)
      clearFullKycValueDispatch()
      clearformUploadDispatch()
    }
  }, [FullKycResData])


  const handleChangeFirst = selectedOption => {
    if (selectedOption !== null) {
      setSelectedFirstOption(selectedOption)
      setFormData(values => ({
        ...values,
        First: selectedOption.label
      }))
      setErrors({ ...errors, First: '' })
    } else {
      setSelectedFirstOption()
      setFormData(values => ({ ...values, First: '' }))
    }
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({
        label: rawData[item].name,
        value: rawData[item].name
      })
    }
    return defaultOptions
  }

  useEffect(() => {
    const defaultValue = []
    const businessProof = _.filter(data && data.entityDocument, function (o) {
      defaultValue.push({
        name: o,
      })
    })
    if (defaultValue) {
      const First = getDefaultOptions(defaultValue)
      setFirstOption(First)
      if (!_.isEmpty(formData.First)) {
        const selOption = _.filter(First, function (x) { if (_.includes(formData.First._id, x.value)) { return x } })
        setSelectedFirstOption(selOption)
      }
    }
  }, [data])

  const handleSubmit = () => {
    const params = {
      "documents": {
        [formData && formData.companyName]: [
          {
            uploadDocument: `${DOCUMENT_URL}${formData.uploadDocument}`,
            documentName: formData && formData.First,
            documentId: formData && formData.id,
            documentNumber: formData && formData.documentNumber
          }
        ]
      }
    }
    DashComUploadDocDispatch(id, params)
  }

  const handleSubmitAddtionalDocument = () => {
    if (_.isEmpty(additionalDocumentFormData1.document)) {
      setErrors({ ...errors, additionalDocument: 'Please upload the document' })
    }
    else {
      setErrors({ ...errors, additionalDocument: '' })
      const params = {
        "additionalDocument1": additionalDocumentFormData1,
      }
      fullKycDispatch(id, params)
      setEditShow(false)
      setShowAdditionalDoc1(false)
    }
  }

  const handleSubmitAddtionalDocument2 = () => {
    if (_.isEmpty(additionalDocumentFormData2.document)) {
      setErrors({ ...errors, additionalDocument: 'Please upload the document' })
    }
    else {
      setErrors({ ...errors, additionalDocument: '' })
      const params = {
        "additionalDocument2": additionalDocumentFormData2
      }
      fullKycDispatch(id, params)
      setEditShow2(false)
      setShowAdditionalDoc2(false)
    }
  }

  const handleSubmitAddtionalDocument3 = () => {
    if (_.isEmpty(additionalDocumentFormData3.document)) {
      setErrors({ ...errors, additionalDocument: 'Please upload the document' })
    }
    else {
      setErrors({ ...errors, additionalDocument: '' })
      const params = {
        "additionalDocument3": additionalDocumentFormData3
      }
      fullKycDispatch(id, params)
      setEditShow3(false)
      setShowAdditionalDoc3(false)
    }
  }

  const handleSubmitAddtionalDocument4 = () => {
    if (_.isEmpty(additionalDocumentFormData4.document)) {
      setErrors({ ...errors, additionalDocument: 'Please upload the document' })
    }
    else {
      setErrors({ ...errors, additionalDocument: '' })
      const params = {
        "additionalDocument4": additionalDocumentFormData4
      }
      fullKycDispatch(id, params)
      setEditShow4(false)
      setShowAdditionalDoc4(false)
    }
  }

  useEffect(() => {
    if (DashReUploadRes && DashReUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(DashReUploadRes && DashReUploadRes.message, "success")
      setFileGName('Upload')
      setShow(false)
      setShowCommon(false)
      setSelectedFirstOption('')
      clearDashReUploadDocDispatch()
      clearformUploadDispatch()
      getKYCUserDetailsDispatch(id)
      setShowformg(false)
      setFormData({
        uploadDocument: '',
        documentNumber: ''
      })
    } else if (DashReUploadRes && DashReUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setShowformg(false)
      setFileGName('Upload')
      setShowCommon(false)
      clearDashReUploadDocDispatch()
      clearformUploadDispatch()
    }
  }, [DashReUploadRes])

  useEffect(() => {
    if (DashCommonUploadRes && DashCommonUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(DashCommonUploadRes && DashCommonUploadRes.message, "success")
      setFileGName('Upload')
      setShow(false)
      setShowCommon(false)
      setSelectedFirstOption('')
      clearDashComUploadDocDispatch()
      clearformUploadDispatch()
      getKYCUserDetailsDispatch(id)
    } else if (DashCommonUploadRes && DashCommonUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setShowformg(false)
      setFileGName('Upload')
      setShowCommon(false)
      clearDashComUploadDocDispatch()
      clearformUploadDispatch()
    }
  }, [DashCommonUploadRes])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(FullKycResData && FullKycResData.message, "success")
      setSelectedFirstOption('')
      clearFullKycValueDispatch()
      getKYCUserDetailsDispatch(id)
      setFormData({
        uploadDocument: '',
        documentNumber: ''
      })
      setShowAdditionalDocument1(false)
      setShowAdditionalDocument2(false)
      setShowAdditionalDocument3(false)
      setShowAdditionalDocument4(false)
      setAdditionalDocumentFormData1({
        label: '',
        document: ''
      })
      setAdditionalDocumentFormData2({
        label: '',
        document: ''
      })
      setAdditionalDocumentFormData3({
        label: '',
        document: ''
      })
      setAdditionalDocumentFormData4({
        label: '',
        document: ''
      })
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
      clearformUploadDispatch()
    }
  }, [FullKycResData])

  useEffect(() => {
    if (uploadViewRes && uploadViewRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      getKYCUserDetailsDispatch(id)
      clearviewUploadDocDispatch()
    } else if (uploadViewRes && uploadViewRes.status === STATUS_RESPONSE.ERROR_MSG) {
      clearviewUploadDocDispatch()
    }
  }, [uploadViewRes])

  const clear = () => {
    setShowformg(false)
    setShow(false)
    setFormData({
      uploadDocument: '',
      documentNumber: ''
    })
    setShowCommon(false)
    setSelectedFirstOption('')
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      "Are you sure want to delete this document,",
      "If you delete the document, can't revert back?",
      'warning',
      'Yes',
      'No',
      () => {
        deleteKYCDocumentDispatch(id)
      },
      () => { { } }
    )
  }

  useEffect(() => {
    if (DeleteKYCDocumentData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        DeleteKYCDocumentData && DeleteKYCDocumentData.message,
        'success'
      )
      getKYCUserDetailsDispatch(id)
      clearDeleteKYCDocumentDispatch()
    } else if (DeleteKYCDocumentData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        DeleteKYCDocumentData.message,
        '',
        'Ok'
      )
    }
    clearDeleteKYCDocumentDispatch()
  }, [DeleteKYCDocumentData])

  useEffect(() => {
    const typeData = CommonFileDocument && CommonFileDocument.data
    if (typeData && typeData.type === "additionalDocument1" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAdditionalDocumentFormData1({
          ...additionalDocumentFormData1,
          document: `${DOCUMENT_URL}${data}`
        })
      }
      setShowAdditionalDocument1(true)
      setAdditionalDocument1Loading(false)
      setErrors({ ...errors, additionalDocument: '' })
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "additionalDocument2" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAdditionalDocumentFormData2({
          ...additionalDocumentFormData2,
          document: `${DOCUMENT_URL}${data}`
        })
      }
      setShowAdditionalDocument2(true)
      setAdditionalDocument2Loading(false)
      setErrors({ ...errors, additionalDocument: '' })
      ClearCommonFileDispatch()
    } else if (typeData && typeData.type === "additionalDocument3" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAdditionalDocumentFormData3({
          ...additionalDocumentFormData3,
          document: `${DOCUMENT_URL}${data}`
        })
      }
      setShowAdditionalDocument3(true)
      setAdditionalDocument3Loading(false)
      setErrors({ ...errors, additionalDocument: '' })
      ClearCommonFileDispatch()
    } else if (typeData && typeData.type === "additionalDocument4" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAdditionalDocumentFormData4({
          ...additionalDocumentFormData4,
          document: `${DOCUMENT_URL}${data}`
        })
      }
      setShowAdditionalDocument4(true)
      setAdditionalDocument4Loading(false)
      setErrors({ ...errors, additionalDocument: '' })
      ClearCommonFileDispatch()
    }else if (CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        CommonFileDocument && CommonFileDocument.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      ClearCommonFileDispatch()
    }
  }, [CommonFileDocument])

  const AddtionalFileChangeHandler = (e) => {
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(PDF, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        if (name === 'additionalDocument1') {
          setAdditionalDocument1Loading(true)
          const data = new FormData()
          data.append('type', 'additionalDocument1')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'additionalDocument2') {
          setAdditionalDocument2Loading(true)
          const data = new FormData()
          data.append('type', 'additionalDocument2')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'additionalDocument3') {
          setAdditionalDocument3Loading(true)
          const data = new FormData()
          data.append('type', 'additionalDocument3')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        }  else if (name === 'additionalDocument4') {
          setAdditionalDocument4Loading(true)
          const data = new FormData()
          data.append('type', 'additionalDocument4')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        }
      } else {
        setErrors({
          ...errors,
          [name]:
            warningAlert(
              'error',
              `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
              '',
              'Try again',
              '',
              () => { { } }
            )
        })
      }
    } else {
      setErrors({
        ...errors, [name]:
          warningAlert(
            'error',
            DROPZONE_MESSAGES.PDF_INVALID,
            '',
            'Try again',
            '',
            () => { { } }
          )
      })
    }
  }

  const AdditionalDocumentHandleChange = (e) => {
    const { name, value } = e.target
    if (name === "additionalDocument1label") {
      setAdditionalDocumentFormData1({ ...additionalDocumentFormData1, label: value })
    } else if (name === "additionalDocument2label") {
      setAdditionalDocumentFormData2({ ...additionalDocumentFormData2, label: value })
    } else if (name === "additionalDocument3label") {
      setAdditionalDocumentFormData3({ ...additionalDocumentFormData3, label: value })
    }else {
      setAdditionalDocumentFormData4({ ...additionalDocumentFormData4, label: value })
    }
  }

  const AdditionalDocumentClick1 = (event) => {
    AdditionalDocumentFilesInput1.current.click(event)
  }

  const AdditionalDocumentClick2 = (event) => {
    AdditionalDocumentFilesInput2.current.click(event)
  }

  const AdditionalDocumentClick3 = (event) => {
    AdditionalDocumentFilesInput3.current.click(event)
  }

  const AdditionalDocumentClick4 = (event) => {
    AdditionalDocumentFilesInput4.current.click(event)
  }

  const goToPreviousPage = () => {
    setCurrentPage(prevPage => Math.max(prevPage - 1, 1));
  };

  const goToNextPage = () => {
    setCurrentPage(prevPage => Math.min(prevPage + 1, numPages));
  };

  const goToPreviousPages = () => {
    setCurrentPages(prevPage => Math.max(prevPage - 1, 1));
  };

  const goToNextPages = () => {
    setCurrentPages(prevPage => Math.min(prevPage + 1, pageNumber));
  };

  const goToPreviousPages3 = () => {
    setCurrentPages3(prevPage => Math.max(prevPage - 1, 1));
  }

  const goToNextPages3 = () => {
    setCurrentPages3(prevPage => Math.min(prevPage + 1, pageNumber3));
  }

  const goToPreviousPages4 = () => {
    setCurrentPages4(prevPage => Math.max(prevPage - 1, 1));
  }

  const goToNextPages4 = () => {
    setCurrentPages4(prevPage => Math.min(prevPage + 1, pageNumber4));
  }

  useEffect(() => {
    if (userData && userData.additionalDocument1 && userData.additionalDocument1.document) {
      setEditShow(true)
      setShowAdditionalDocument1(true)
    } else {
      setEditShow(false)
      setShowAdditionalDocument1(false)
    }
    if (userData && userData.additionalDocument2 && userData.additionalDocument2.document) {
      setEditShow2(true)
      setShowAdditionalDocument2(true)
    } else {
      setEditShow2(false)
      setShowAdditionalDocument2(false)
    }
    if (userData && userData.additionalDocument3 && userData.additionalDocument3.document) {
      setEditShow3(true)
      setShowAdditionalDocument3(true)
    } else {
      setEditShow3(false)
      setShowAdditionalDocument3(false)
    }
    if (userData && userData.additionalDocument4 && userData.additionalDocument4.document) {
      setEditShow4(true)
      setShowAdditionalDocument4(true)
    } else {
      setEditShow4(false)
      setShowAdditionalDocument4(false)
    }
  }, [userData])

  const EditData1 = () => {
    setShowAdditionalDoc1(true)
    setAdditionalDocumentFormData1({
      ...additionalDocumentFormData1,
      label: userData && userData.additionalDocument1 && userData.additionalDocument1.label,
      document: userData && userData.additionalDocument1 && userData.additionalDocument1.document
    })
  }

  const EditData2 = () => {
    setShowAdditionalDoc2(true)
    setAdditionalDocumentFormData2({
      ...additionalDocumentFormData2,
      label: userData && userData.additionalDocument2 && userData.additionalDocument2.label,
      document: userData && userData.additionalDocument2 && userData.additionalDocument2.document
    })
  }

  const EditData3 = () => {
    setShowAdditionalDoc3(true)
    setAdditionalDocumentFormData3({
      ...additionalDocumentFormData3,
      label: userData && userData.additionalDocument3 && userData.additionalDocument3.label,
      document: userData && userData.additionalDocument3 && userData.additionalDocument3.document
    })
  }

  const EditData4 = () => {
    setShowAdditionalDoc4(true)
    setAdditionalDocumentFormData4({
      ...additionalDocumentFormData4,
      label: userData && userData.additionalDocument4 && userData.additionalDocument4.label,
      document: userData && userData.additionalDocument4 && userData.additionalDocument4.document
    })
  }


  const AddDocclear1 = () => {
    setEditShow(false)
    setShowAdditionalDoc1(false)
  }

  const AddDocclear2 = () => {
    setEditShow2(false)
    setShowAdditionalDoc2(false)
  }

  const AddDocclear3 = () => {
    setEditShow3(false)
    setShowAdditionalDoc3(false)
  }

  const AddDocclear4 = () => {
    setEditShow4(false)
    setShowAdditionalDoc4(false)
  }

  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clear()}>
        <Modal.Header
          closeButton={() => clear()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Re Upload
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className="card-header">
              <div className="card-body mt-10">
                <div className="form-group row mb-4">
                  <div className="row mb-4">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Entity :
                      </label>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6 mb-3'>
                      <ReactSelect
                        isClearable
                        styles={customStyles}
                        isMulti={false}
                        name='First'
                        placeholder="Select..."
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeFirst}
                        options={FirstOption}
                        value={selectedFirstOption}
                        isDisabled={!FirstOption}
                      />
                      {errors.First && (
                        <div className='fv-plugins-message-container text-danger'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors.First}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Document Number :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <input
                        name="documentNumber"
                        type="text"
                        className='form-control form-control-lg form-control-solid'
                        placeholder="Document Number"
                        onChange={(e) => handleChange(e)}
                        autoComplete="off"
                        value={formData.documentNumber || ""}
                      />
                      {errors.documentNumber && (
                        <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                          <span role="alert text-danger">
                            {errors.documentNumber}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Document :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <div className="d-flex justify-content-start">
                        <input
                          type="file"
                          className="d-none"
                          name="uploadDocument"
                          id="uploadDocument"
                          multiple={false}
                          ref={hiddenFileInput}
                          onChange={(e) => {
                            FileChangeHandler(e)
                          }} />
                        <a
                          className={showformg ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}
                          onClick={() => {
                            handleClick()
                          }}
                        >
                          {FormResLoadig
                            ? (
                              'Uploading...'
                            )
                            : (
                              showformg ? (
                                'Uploaded'
                              ) : (
                                'Re Upload'
                              )
                            )}
                        </a>
                      </div>
                      {errors.uploadDocument && (
                        <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                          <span role="alert text-danger">
                            {errors.uploadDocument}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => documentSubmit()}
                          disabled={DashReUploadResLoadig}
                        >
                          {
                            DashReUploadResLoadig ? (
                              'please wait...'
                            ) : 'Submit'
                          }
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={isViewerOpen}
        size="lg"
        centered
        onHide={() => setIsViewerOpen(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => setIsViewerOpen(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-1" />
            <div className="col-lg-10">
              <div className="card card-custom overlay overflow-hidden">
                <div className="card-body p-0">
                  <div className="overlay-wrapper">
                    <img
                      src={currentImage}
                      alt=""
                      className="w-100 rounded"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-1" />
          </div>
        </Modal.Body>
      </Modal>

      <Modal
        show={showAdditionalDoc1}
        size="lg"
        centered
        onHide={() => AddDocclear1()}>
        <Modal.Header
          closeButton={() => AddDocclear1()}
          style={{
            backgroundColor: "#5e6278"
          }}
        >
          <Modal.Title
            className="text-white"
          >
            {userData && !_.isEmpty(userData.additionalDocument1 && userData.additionalDocument1.document) ? 'Re-upload' : 'Upload'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className="card-header">
              <div className="card-body mt-10">
                <div className="form-group row mb-4">
                  <div className="row mb-4">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Label :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <input
                        className="form-control form-control-lg form-control-solid"
                        type="text"
                        id="additionalDocument1label"
                        name='additionalDocument1label'
                        onChange={(e) => AdditionalDocumentHandleChange(e)}
                        value={additionalDocumentFormData1.label || ''}
                        autoComplete="off"
                        placeholder="Label"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Document :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <div className="d-flex justify-content-start">
                        <input
                          type="file"
                          className="d-none"
                          name="additionalDocument1"
                          id="additionalDocument1"
                          accept=".pdf"
                          multiple={false}
                          ref={AdditionalDocumentFilesInput1}
                          onChange={(e) => {
                            AddtionalFileChangeHandler(e)
                            e.target.value = null
                          }}
                        />
                        <a
                          className={showAdditionalDocument1 ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}
                          disabled={CommonFileLoading}
                          onClick={(e) => { AdditionalDocumentClick1(e) }}
                        >
                          {CommonFileLoading && additionalDocument1Loading
                            ? (
                              <span className='spinner-border spinner-border-sm align-middle text-white ms-2' />
                            )
                            : (
                              showAdditionalDocument1 ? (
                                'Uploaded'
                              ) : (
                                'Upload'
                              )
                            )}
                        </a>
                      </div>
                      {errors.additionalDocument && (
                        <div className='fv-plugins-message-container text-danger ms-3 mt-1'>
                          <span role="alert text-danger">
                            {errors.additionalDocument}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button className="btn btn-sm btn-light-primary m-2 fa-pull-right close" type="button"
                        onClick={handleSubmitAddtionalDocument}
                        disabled={FullKycResDataLoading}
                      >
                        {
                          FullKycResDataLoading ?
                            (
                              'loading...'
                            ) : ('Submit')
                        }
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={showAdditionalDoc2}
        size="lg"
        centered
        onHide={() => AddDocclear2()}>
        <Modal.Header
          closeButton={() => AddDocclear2()}
          style={{
            backgroundColor: "#5e6278"
          }}
        >
          <Modal.Title
            className="text-white"
          >
            {userData && !_.isEmpty(userData.additionalDocument2 && userData.additionalDocument2.document) ? 'Re-upload' : 'Upload'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className="card-header">
              <div className="card-body mt-10">
                <div className="form-group row mb-4">
                  <div className="row mb-4">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Label :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <input
                        className="form-control form-control-lg form-control-solid"
                        type="text"
                        id="additionalDocument2label"
                        name='additionalDocument2label'
                        onChange={(e) => AdditionalDocumentHandleChange(e)}
                        value={additionalDocumentFormData2.label || ''}
                        autoComplete="off"
                        placeholder="Label"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Document :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <div className="d-flex justify-content-start">
                        <input
                          type="file"
                          className="d-none"
                          name="additionalDocument2"
                          id="additionalDocument2"
                          multiple={false}
                          ref={AdditionalDocumentFilesInput2}
                          accept=".pdf"
                          onChange={(e) => {
                            AddtionalFileChangeHandler(e)
                            e.target.value = null
                          }}
                        />
                        <a
                          className={showAdditionalDocument2 ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}
                          disabled={CommonFileLoading}
                          onClick={(e) => { AdditionalDocumentClick2(e) }}
                        >
                          {CommonFileLoading && additionalDocument2Loading
                            ? (
                              <span className='spinner-border spinner-border-sm align-middle text-white ms-2' />
                            )
                            : (
                              showAdditionalDocument2 ? (
                                'Uploaded'
                              ) : (
                                'Upload'
                              )
                            )}
                        </a>
                      </div>
                      {errors.additionalDocument && (
                        <div className='fv-plugins-message-container text-danger ms-3 mt-1'>
                          <span role="alert text-danger">
                            {errors.additionalDocument}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button className="btn btn-sm btn-light-primary m-2 fa-pull-right close" type="button"
                        onClick={handleSubmitAddtionalDocument2}
                        disabled={FullKycResDataLoading}
                      >
                        {
                          FullKycResDataLoading ?
                            (
                              'loading...'
                            ) : ('Submit')
                        }
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={showAdditionalDoc3}
        size="lg"
        centered
        onHide={() => AddDocclear3()}>
        <Modal.Header
          closeButton={() => AddDocclear3()}
          style={{
            backgroundColor: "#5e6278"
          }}>
          <Modal.Title
            className="text-white"
          >
            {userData && !_.isEmpty(userData.additionalDocument3 && userData.additionalDocument3.document) ? 'Re-upload' : 'Upload'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className="card-header">
              <div className="card-body mt-10">
                <div className="form-group row mb-4">
                  <div className="row mb-4">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Label :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <input
                        className="form-control form-control-lg form-control-solid"
                        type="text"
                        id="additionalDocument3label"
                        name='additionalDocument3label'
                        onChange={(e) => AdditionalDocumentHandleChange(e)}
                        value={additionalDocumentFormData3.label || ''}
                        autoComplete="off"
                        placeholder="Label"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Document :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <div className="d-flex justify-content-start">
                        <input
                          type="file"
                          className="d-none"
                          name="additionalDocument3"
                          id="additionalDocument3"
                          multiple={false}
                          ref={AdditionalDocumentFilesInput3}
                          accept=".pdf"
                          onChange={(e) => {
                            AddtionalFileChangeHandler(e)
                            e.target.value = null
                          }}
                        />
                        <a
                          className={showAdditionalDocument3 ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}
                          disabled={CommonFileLoading}
                          onClick={(e) => { AdditionalDocumentClick3(e) }}
                        >
                          {CommonFileLoading && additionalDocument3Loading
                            ? (
                              <span className='spinner-border spinner-border-sm align-middle text-white ms-2' />
                            )
                            : (
                              showAdditionalDocument3 ? (
                                'Uploaded'
                              ) : (
                                'Upload'
                              )
                            )}
                        </a>
                      </div>
                      {errors.additionalDocument && (
                        <div className='fv-plugins-message-container text-danger ms-3 mt-1'>
                          <span role="alert text-danger">
                            {errors.additionalDocument}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button className="btn btn-sm btn-light-primary m-2 fa-pull-right close" type="button"
                        onClick={handleSubmitAddtionalDocument3}
                        disabled={FullKycResDataLoading}
                      >
                        {
                          FullKycResDataLoading ?
                            (
                              'loading...'
                            ) : ('Submit')
                        }
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={showAdditionalDoc4}
        size="lg"
        centered
        onHide={() => AddDocclear4()}>
        <Modal.Header
          closeButton={() => AddDocclear4()}
          style={{
            backgroundColor: "#5e6278"
          }}>
          <Modal.Title
            className="text-white"
          >
            {userData && !_.isEmpty(userData.additionalDocument4 && userData.additionalDocument4.document) ? 'Re-upload' : 'Upload'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className="card-header">
              <div className="card-body mt-10">
                <div className="form-group row mb-4">
                  <div className="row mb-4">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Label :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <input
                        className="form-control form-control-lg form-control-solid"
                        type="text"
                        id="additionalDocument4label"
                        name='additionalDocument4label'
                        onChange={(e) => AdditionalDocumentHandleChange(e)}
                        value={additionalDocumentFormData4.label || ''}
                        autoComplete="off"
                        placeholder="Label"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-5 col-md-5 col-sm-5 mb-3">
                      <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label ms-4">
                        Document :
                      </label>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                      <div className="d-flex justify-content-start">
                        <input
                          type="file"
                          className="d-none"
                          name="additionalDocument4"
                          id="additionalDocument4"
                          multiple={false}
                          ref={AdditionalDocumentFilesInput4}
                          accept=".pdf"
                          onChange={(e) => {
                            AddtionalFileChangeHandler(e)
                            e.target.value = null
                          }}
                        />
                        <a
                          className={showAdditionalDocument4 ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}
                          disabled={CommonFileLoading}
                          onClick={(e) => { AdditionalDocumentClick4(e) }}
                        >
                          {CommonFileLoading && additionalDocument4Loading
                            ? (
                              <span className='spinner-border spinner-border-sm align-middle text-white ms-2' />
                            )
                            : (
                              showAdditionalDocument4 ? (
                                'Uploaded'
                              ) : (
                                'Upload'
                              )
                            )}
                        </a>
                      </div>
                      {errors.additionalDocument && (
                        <div className='fv-plugins-message-container text-danger ms-3 mt-1'>
                          <span role="alert text-danger">
                            {errors.additionalDocument}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button className="btn btn-sm btn-light-primary m-2 fa-pull-right close" type="button"
                        onClick={handleSubmitAddtionalDocument4}
                        disabled={FullKycResDataLoading}
                      >
                        {
                          FullKycResDataLoading ?
                            (
                              'loading...'
                            ) : ('Submit')
                        }
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <h2> Documents</h2>{" "}
            <i class="fa fa-times-circle-o" aria-hidden="true"></i>
          </Accordion.Header>
          <Accordion.Body>
            <Card>
              <Tabs variant="pills">
                <Tab eventKey="document">
                  <Card.Body>
                    <div
                      className="row block_div"
                    >
                      {
                        userData && userData.documents && _.isEmpty(userData.documents.identityProof) &&
                          _.isEmpty(userData.documents.addressProof) &&
                          _.isEmpty(userData.documents.businessProof) ? (
                          <div className='row'>
                            <div className='col-lg-12'>
                              <div className="d-flex justify-content-end">
                                <button
                                  className="btn btn-sm btn-light-primary "
                                  onClick={() => { viewUploadDocDispatch(id) }}
                                  disabled={uploadViewResLoadig}
                                >
                                  {!uploadViewResLoadig && <span className='indicator-label'>Create Kyc Document</span>}
                                  {uploadViewResLoadig && (
                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                      Please wait...
                                      <span className='spinner-border spinner-border-sm align-middle text-primary ms-2' />
                                    </span>
                                  )}
                                </button>
                              </div>
                            </div>
                          </div>
                        ) : null
                      }

                      {
                        userData && userData.documents && _.isArray(userData.documents.identityProof) && !_.isEmpty(userData.documents.identityProof) ? (
                          <div className='row'>
                            <div className='col-lg-12'>
                              <div className='card'>
                                <div className='card-body'>
                                  <div className="row">
                                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                      <div className="fs-3 h6 font-weight-600">
                                        Identity Proof
                                      </div>
                                    </div>
                                  </div>
                                  {
                                    userData && userData.documents && userData.documents.identityProof.map((item, i) => {
                                      const ocrLabel = !_.isEmpty(item && item.ocrFields) ? Object.keys(item && item.ocrFields) : '--'
                                      const ocrValue = !_.isEmpty(item && item.ocrFields) ? Object.values(item && item.ocrFields) : '--'

                                      const mrzLabel = !_.isEmpty(item && item.mrzFields) ? Object.keys(item && item.mrzFields) : '--'
                                      const mrzValue = !_.isEmpty(item && item.mrzFields) ? Object.values(item && item.mrzFields) : '--'

                                      let ocrLabelData = []
                                      let mrzLabelData = []

                                      _.isArray(ocrLabel) && ocrLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        ocrLabelData.push({ label: formattedLabel, value: ocrValue[index] });
                                      });

                                      _.isArray(mrzLabel) && mrzLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        mrzLabelData.push({ label: formattedLabel, value: mrzValue[index] });
                                      });

                                      return (
                                        <>
                                          <div className="row" key={i}>
                                            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                              <h4 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                                {
                                                  item && item.documentName
                                                }
                                                <span className={`badge ${RISKSTATUS[item.documentStatus && item.documentStatus]} ms-4`}>
                                                  {item.documentStatus ? item.documentStatus : "--"}
                                                </span>
                                              </h4>
                                            </div>
                                            <Can
                                              permissons={getUsersPermissions}
                                              componentPermissions={UPDATE_PERMISSION}
                                            >
                                              {
                                                item && item.updatedDocument === 'No' || _.isEmpty(item && item.uploadDocument) ? (
                                                  <>
                                                    <div className='col-xs-5 col-sm-5 col-md-5 col-lg-5 mt-5'>
                                                      <div className="d-flex justify-content-end">
                                                        <button
                                                          className="btn btn-sm btn-light-primary "
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Upload
                                                        </button>
                                                      </div>
                                                    </div>
                                                  </>
                                                ) : (
                                                  !_.isEmpty(item && item.uploadDocument) ?
                                                    <>
                                                      {
                                                        item && item.documentStatus === "PENDING" ? (
                                                          <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                            <div className="d-flex justify-content-end ms-8"
                                                            >
                                                              <span
                                                                onClick={() => {
                                                                  setStatusData({
                                                                    documentId: item && item._id,
                                                                    documentType: item && item.documentType,
                                                                    documentName: item && item.documentName,
                                                                    entityDocument: item && item.entityDocument
                                                                  })
                                                                }}
                                                              >
                                                                <Status
                                                                  statusData={statusData}
                                                                  FullKycResData={FullKycResData}
                                                                />
                                                              </span>
                                                            </div>
                                                          </div>
                                                        ) : null
                                                      }
                                                      <div className='d-flex align-items-center col-xs-3 col-sm-3 col-md-3 col-lg-3 mt-5'>
                                                        <button
                                                          className="btn btn-sm btn-light-primary px-2"
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Reupload
                                                        </button>

                                                        <button
                                                          className="btn btn-sm btn-light-danger px-2 ms-2"
                                                          onClick={() => onDeleteItem(item && item._id)}
                                                        >
                                                          Delete
                                                        </button>
                                                      </div>
                                                    </>
                                                    : null
                                                )
                                              }
                                            </Can>
                                          </div>
                                          <div className="row mt-4"
                                          >
                                            <div className="col-lg-3" />
                                            <div className="col-lg-6 ms-4"
                                              style={{
                                                backgroundColor: 'aliceblue',
                                                borderRadius: '10px'
                                              }}
                                            >
                                              <div className="d-flex justify-content-start">
                                                {
                                                  item && item.updatedDocument === 'Yes' && _.isEmpty(item && item.uploadDocument) ? (
                                                    <div className="d-flex justify-content-center">
                                                      <h3 className="text-primary">Processing...</h3>
                                                    </div>
                                                  ) : (
                                                    <div className="card card-custom overlay overflow-hidden"
                                                    >
                                                      <div className="card-body p-0">
                                                        <>
                                                          <div className="overlay-wrapper">
                                                            <img
                                                              src={
                                                                item && item.uploadDocument ? item.uploadDocument : toAbsoluteUrl('/media/logos/no-image.png')
                                                              }
                                                              className="w-100 rounded"
                                                            />
                                                          </div>
                                                          <div className="overlay-layer bg-dark bg-opacity-10">
                                                            <a className="btn btn-light-primary btn-shadow"
                                                              onClick={() => openImageViewer(item && item.uploadDocument ? item.uploadDocument : '--')}
                                                            >
                                                              View
                                                            </a>
                                                          </div>
                                                        </>
                                                      </div>
                                                    </div>
                                                  )
                                                }
                                              </div>
                                            </div>
                                            <div className="col-lg-3" />
                                          </div>
                                          {
                                            !_.isEmpty(item && item.mrzFields) || !_.isEmpty(item && item.ocrFields) ? (
                                              <div className="row">
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>OCR Field</h4>
                                                  {
                                                    _.isArray(ocrLabelData) ? ocrLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>MRZ Field</h4>
                                                  {
                                                    _.isArray(mrzLabelData) ? mrzLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                              </div>
                                            ) : null
                                          }
                                          <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                                        </>
                                      )
                                    })
                                  }
                                </div >
                              </div>
                            </div>
                          </div>
                        ) : null
                      }

                      {
                        userData && userData.documents && _.isArray(userData.documents.otherDocuments) && !_.isEmpty(userData.documents.otherDocuments) ? (
                          <div className='row'>
                            <div className='col-lg-12'>
                              <div className='card'>
                                <div className='card-body'>
                                  <div className="row">
                                    <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                      <div className="fs-3 h6 font-weight-600">
                                        Other Documents Proof
                                      </div>
                                    </div>
                                  </div>
                                  {
                                    userData && userData.documents && userData.documents.otherDocuments.map((item, i) => {

                                      const ocrLabel = !_.isEmpty(item && item.ocrFields) ? Object.keys(item && item.ocrFields) : '--'
                                      const ocrValue = !_.isEmpty(item && item.ocrFields) ? Object.values(item && item.ocrFields) : '--'

                                      const mrzLabel = !_.isEmpty(item && item.mrzFields) ? Object.keys(item && item.mrzFields) : '--'
                                      const mrzValue = !_.isEmpty(item && item.mrzFields) ? Object.values(item && item.mrzFields) : '--'

                                      let ocrLabelData = []
                                      let mrzLabelData = []

                                      _.isArray(ocrLabel) && ocrLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        ocrLabelData.push({ label: formattedLabel, value: ocrValue[index] });
                                      });

                                      _.isArray(mrzLabel) && mrzLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        mrzLabelData.push({ label: formattedLabel, value: mrzValue[index] });
                                      });

                                      return (
                                        <>
                                          <div className="row">
                                            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                              <h4 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                                {
                                                  item && item.documentName
                                                }
                                                <span className={`badge ${RISKSTATUS[item.documentStatus && item.documentStatus]} ms-4`}>
                                                  {item.documentStatus ? item.documentStatus : "--"}
                                                </span>
                                              </h4>
                                            </div>
                                            <Can
                                              permissons={getUsersPermissions}
                                              componentPermissions={UPDATE_PERMISSION}
                                            >
                                              {
                                                item && item.updatedDocument === 'No' || _.isEmpty(item && item.uploadDocument) ? (
                                                  <>
                                                    <div className='col-xs-5 col-sm-5 col-md-5 col-lg-5 mt-5'>
                                                      <div className="d-flex justify-content-end">
                                                        <button
                                                          className="btn btn-sm btn-light-primary "
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Upload
                                                        </button>
                                                      </div>
                                                    </div>
                                                  </>
                                                ) : (
                                                  !_.isEmpty(item && item.uploadDocument) ?
                                                    <>
                                                      {
                                                        item && item.documentStatus === "PENDING" ? (
                                                          <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                            <div className="d-flex justify-content-end ms-8"
                                                            >
                                                              <span
                                                                onClick={() => {
                                                                  setStatusData({
                                                                    documentId: item && item._id,
                                                                    documentType: item && item.documentType,
                                                                    documentName: item && item.documentName,
                                                                    entityDocument: item && item.entityDocument
                                                                  })
                                                                }}
                                                              >
                                                                <Status
                                                                  statusData={statusData}
                                                                  FullKycResData={FullKycResData}
                                                                />
                                                              </span>
                                                            </div>
                                                          </div>
                                                        ) : null
                                                      }
                                                      <div className='col-xs-3 col-sm-3 col-md-3 col-lg-3 mt-5'>
                                                        <button
                                                          className="btn btn-sm btn-light-primary"
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Re Upload
                                                        </button>
                                                      </div>
                                                    </>
                                                    : null
                                                )
                                              }
                                            </Can>
                                          </div>
                                          <div className="row mt-4"
                                          >
                                            <div className="col-lg-3" />
                                            <div className="col-lg-6 ms-4"
                                              style={{
                                                backgroundColor: 'aliceblue',
                                                borderRadius: '10px'
                                              }}
                                            >
                                              <div className="d-flex justify-content-start">
                                                <div className="card card-custom overlay overflow-hidden">
                                                  <div className="card-body p-0">
                                                    {
                                                      item && item.updatedDocument === 'Yes' && _.isEmpty(item && item.uploadDocument) ? (
                                                        <h3 className="text-primary">Processing...</h3>
                                                      ) : (
                                                        <>
                                                          <div className="overlay-wrapper">
                                                            <img
                                                              src={
                                                                item && item.uploadDocument ? item.uploadDocument : toAbsoluteUrl('/media/logos/no-image.png')
                                                              }
                                                              className="w-100 rounded"
                                                            />
                                                          </div>
                                                          <div className="overlay-layer bg-dark bg-opacity-10">
                                                            <a className="btn btn-light-primary btn-shadow"
                                                              onClick={() => openImageViewer(item && item.uploadDocument ? item.uploadDocument : '--')}
                                                            >
                                                              View
                                                            </a>
                                                          </div>
                                                        </>
                                                      )
                                                    }
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div className="col-lg-3" />
                                          </div>
                                          {
                                            !_.isEmpty(item && item.mrzFields) || !_.isEmpty(item && item.ocrFields) ? (
                                              <div className="row">
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>OCR Field</h4>
                                                  {
                                                    _.isArray(ocrLabelData) ? ocrLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>MRZ Field</h4>
                                                  {
                                                    _.isArray(mrzLabelData) ? mrzLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                              </div>
                                            ) : null
                                          }
                                          <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                                        </>
                                      )
                                    })
                                  }
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : null
                      }

                      {
                        userData && userData.documents && _.isArray(userData.documents.businessProof) && !_.isEmpty(userData.documents.businessProof) ? (
                          <div className='row'>
                            <div className='col-lg-12'>
                              <div className='card'>
                                <div className='card-body'>
                                  <div className="row">
                                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                      <div className="fs-3 h6 font-weight-600">
                                        Business Proof
                                      </div>
                                    </div>
                                  </div>
                                  {
                                    userData && userData.documents && userData.documents.businessProof.map((item, i) => {
                                      const ocrLabel = !_.isEmpty(item && item.ocrFields) ? Object.keys(item && item.ocrFields) : '--'
                                      const ocrValue = !_.isEmpty(item && item.ocrFields) ? Object.values(item && item.ocrFields) : '--'

                                      const mrzLabel = !_.isEmpty(item && item.mrzFields) ? Object.keys(item && item.mrzFields) : '--'
                                      const mrzValue = !_.isEmpty(item && item.mrzFields) ? Object.values(item && item.mrzFields) : '--'

                                      let ocrLabelData = []
                                      let mrzLabelData = []

                                      _.isArray(ocrLabel) && ocrLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        ocrLabelData.push({ label: formattedLabel, value: ocrValue[index] });
                                      });

                                      _.isArray(mrzLabel) && mrzLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        mrzLabelData.push({ label: formattedLabel, value: mrzValue[index] });
                                      });

                                      return (
                                        <>
                                          <div className="row">
                                            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                              <h4 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                                {
                                                  item && item.documentName
                                                }
                                                <span className={`badge ${RISKSTATUS[item.documentStatus && item.documentStatus]} ms-4`}>
                                                  {item.documentStatus ? item.documentStatus : "--"}
                                                </span>
                                              </h4>
                                            </div>
                                            <Can
                                              permissons={getUsersPermissions}
                                              componentPermissions={UPDATE_PERMISSION}
                                            >
                                              {
                                                item && item.updatedDocument === 'No' || _.isEmpty(item && item.uploadDocument) ? (
                                                  <>
                                                    <div className='col-xs-5 col-sm-5 col-md-5 col-lg-5 mt-5'>
                                                      <div className="d-flex justify-content-end">
                                                        <button
                                                          className="btn btn-sm btn-light-primary "
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Upload
                                                        </button>
                                                      </div>
                                                    </div>
                                                  </>
                                                ) : (
                                                  !_.isEmpty(item && item.uploadDocument) ?
                                                    <>
                                                      {
                                                        item && item.documentStatus === "PENDING" ? (
                                                          <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                            <div className="d-flex justify-content-end ms-8"
                                                            >
                                                              <span
                                                                onClick={() => {
                                                                  setStatusData({
                                                                    documentId: item && item._id,
                                                                    documentType: item && item.documentType,
                                                                    documentName: item && item.documentName,
                                                                    entityDocument: item && item.entityDocument
                                                                  })
                                                                }}
                                                              >
                                                                <Status
                                                                  statusData={statusData}
                                                                  FullKycResData={FullKycResData}
                                                                />
                                                              </span>
                                                            </div>
                                                          </div>
                                                        ) : null
                                                      }

                                                      <div className='col-xs-3 col-sm-3 col-md-3 col-lg-3 mt-5'>
                                                        <button
                                                          className="btn btn-sm btn-light-primary"
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Re Upload
                                                        </button>
                                                      </div>
                                                    </>
                                                    : null
                                                )
                                              }
                                            </Can>
                                          </div>
                                          <div className="row mt-4"
                                          >
                                            <div className="col-lg-3" />
                                            <div className="col-lg-6 ms-4"
                                              style={{
                                                backgroundColor: 'aliceblue',
                                                borderRadius: '10px'
                                              }}
                                            >
                                              <div className="d-flex justify-content-start">
                                                <div className="card card-custom overlay overflow-hidden">
                                                  <div className="card-body p-0">
                                                    {
                                                      item && item.updatedDocument === 'Yes' && _.isEmpty(item && item.uploadDocument) ? (
                                                        <h3 className="text-primary">Processing...</h3>
                                                      ) : (
                                                        <>
                                                          <div className="overlay-wrapper">
                                                            <img
                                                              src={
                                                                item && item.uploadDocument ? item.uploadDocument : toAbsoluteUrl('/media/logos/no-image.png')
                                                              }
                                                              className="w-100 rounded"
                                                            />
                                                          </div>
                                                          <div className="overlay-layer bg-dark bg-opacity-10">
                                                            <a className="btn btn-light-primary btn-shadow"
                                                              onClick={() => openImageViewer(item && item.uploadDocument ? item.uploadDocument : '--')}
                                                            >
                                                              View
                                                            </a>
                                                          </div>
                                                        </>
                                                      )
                                                    }
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div className="col-lg-3" />
                                          </div>
                                          {
                                            !_.isEmpty(item && item.mrzFields) || !_.isEmpty(item && item.ocrFields) ? (
                                              <div className="row">
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>OCR Field</h4>
                                                  {
                                                    _.isArray(ocrLabelData) ? ocrLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>MRZ Field</h4>
                                                  {
                                                    _.isArray(mrzLabelData) ? mrzLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                              </div>
                                            ) : null
                                          }
                                          <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                                        </>
                                      )
                                    })
                                  }
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : null
                      }


                      {
                        userData && userData.documents && _.isArray(userData.documents.categoryProof) && !_.isEmpty(userData.documents.categoryProof) ? (
                          <div className='row'>
                            <div className='col-lg-12'>
                              <div className='card'>
                                <div className='card-body'>
                                  <div className="row">
                                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                      <div className="fs-3 h6 font-weight-600">
                                        Category Proof
                                      </div>
                                    </div>
                                  </div>
                                  {
                                    userData && userData.documents && userData.documents.categoryProof.map((item, i) => {
                                      const ocrLabel = !_.isEmpty(item && item.ocrFields) ? Object.keys(item && item.ocrFields) : '--'
                                      const ocrValue = !_.isEmpty(item && item.ocrFields) ? Object.values(item && item.ocrFields) : '--'

                                      const mrzLabel = !_.isEmpty(item && item.mrzFields) ? Object.keys(item && item.mrzFields) : '--'
                                      const mrzValue = !_.isEmpty(item && item.mrzFields) ? Object.values(item && item.mrzFields) : '--'

                                      let ocrLabelData = []
                                      let mrzLabelData = []

                                      _.isArray(ocrLabel) && ocrLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        ocrLabelData.push({ label: formattedLabel, value: ocrValue[index] });
                                      });

                                      _.isArray(mrzLabel) && mrzLabel.forEach((label, index) => {
                                        const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
                                        mrzLabelData.push({ label: formattedLabel, value: mrzValue[index] });
                                      });

                                      return (
                                        <>
                                          <div className="row">
                                            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                              <h4 style={{ marginTop: "20px", marginBottom: "20px" }}>
                                                {
                                                  item && item.documentName
                                                }
                                                <span className={`badge ${RISKSTATUS[item.documentStatus && item.documentStatus]} ms-4`}>
                                                  {item.documentStatus ? item.documentStatus : "--"}
                                                </span>
                                              </h4>
                                            </div>
                                            <Can
                                              permissons={getUsersPermissions}
                                              componentPermissions={UPDATE_PERMISSION}
                                            >
                                              {
                                                item && item.updatedDocument === 'No' || _.isEmpty(item && item.uploadDocument) ? (
                                                  <>
                                                    <div className='col-xs-5 col-sm-5 col-md-5 col-lg-5 mt-5'>
                                                      <div className="d-flex justify-content-end">
                                                        <button
                                                          className="btn btn-sm btn-light-primary "
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Upload
                                                        </button>
                                                      </div>
                                                    </div>
                                                  </>
                                                ) : (
                                                  !_.isEmpty(item && item.uploadDocument) ?
                                                    <>
                                                      {
                                                        item && item.documentStatus === "PENDING" ? (
                                                          <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                                            <div className="d-flex justify-content-end ms-8"
                                                            >
                                                              <span
                                                                onClick={() => {
                                                                  setStatusData({
                                                                    documentId: item && item._id,
                                                                    documentType: item && item.documentType,
                                                                    documentName: item && item.documentName,
                                                                    entityDocument: item && item.entityDocument
                                                                  })
                                                                }}
                                                              >
                                                                <Status
                                                                  statusData={statusData}
                                                                  FullKycResData={FullKycResData}
                                                                />
                                                              </span>
                                                            </div>
                                                          </div>
                                                        ) : null
                                                      }

                                                      <div className='col-xs-3 col-sm-3 col-md-3 col-lg-3 mt-5'>
                                                        <button
                                                          className="btn btn-sm btn-light-primary"
                                                          onClick={() => {
                                                            setShow(true)
                                                            setData({
                                                              documentName: item && item.documentName,
                                                              documentNumber: item && item.documentNumber,
                                                              documentId: item && item._id,
                                                              documentType: item && item.documentType,
                                                              entityDocument: item && item.entityDocument
                                                            })
                                                            setCurrentIndex(i)
                                                          }}
                                                        >
                                                          Re Upload
                                                        </button>
                                                      </div>
                                                    </>
                                                    : null
                                                )
                                              }
                                            </Can>
                                          </div>
                                          <div className="row mt-4"
                                          >
                                            <div className="col-lg-3" />
                                            <div className="col-lg-6 ms-4"
                                              style={{
                                                backgroundColor: 'aliceblue',
                                                borderRadius: '10px'
                                              }}
                                            >
                                              <div className="d-flex justify-content-start">
                                                <div className="card card-custom overlay overflow-hidden">
                                                  <div className="card-body p-0">
                                                    {
                                                      item && item.updatedDocument === 'Yes' && _.isEmpty(item && item.uploadDocument) ? (
                                                        <h3 className="text-primary">Processing...</h3>
                                                      ) : (
                                                        <>
                                                          <div className="overlay-wrapper">
                                                            <img
                                                              src={
                                                                item && item.uploadDocument ? item.uploadDocument : toAbsoluteUrl('/media/logos/no-image.png')
                                                              }
                                                              className="w-100 rounded"
                                                            />
                                                          </div>
                                                          <div className="overlay-layer bg-dark bg-opacity-10">
                                                            <a className="btn btn-light-primary btn-shadow"
                                                              onClick={() => openImageViewer(item && item.uploadDocument ? item.uploadDocument : '--')}
                                                            >
                                                              View
                                                            </a>
                                                          </div>
                                                        </>
                                                      )
                                                    }
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div className="col-lg-3" />
                                          </div>
                                          {
                                            !_.isEmpty(item && item.mrzFields) || !_.isEmpty(item && item.ocrFields) ? (
                                              <div className="row">
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>OCR Field</h4>
                                                  {
                                                    _.isArray(ocrLabelData) ? ocrLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                                  <h4>MRZ Field</h4>
                                                  {
                                                    _.isArray(mrzLabelData) ? mrzLabelData.map((data) => {
                                                      return (
                                                        <div className="row">
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100">
                                                              {
                                                                data && data.label ? `${data.label} :` : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                              {
                                                                data && data.value ? data.value.toString() : '--'
                                                              }
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )
                                                    }) : null
                                                  }
                                                </div>
                                              </div>
                                            ) : null
                                          }
                                          <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                                        </>
                                      )
                                    })
                                  }
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : null
                      }
                      <div className="row mt-4">
                        <div className="my-3">
                          <div className="d-flex justify-content-between align-items-center">
                            <div className="fs-3 h6 font-weight-600 mb-4">
                              {
                                userData && userData.additionalDocument1 && !_.isEmpty(userData.additionalDocument1.label) ? userData.additionalDocument1.label : 'Additional Document 1'
                              }
                            </div>
                            <div className="d-flex justify-content-end mb-4">
                              <button
                                className="btn btn-sm btn-light-primary"
                                onClick={() => {
                                  {
                                    editShow ? EditData1() : setShowAdditionalDoc1(true)
                                  }
                                }}
                              >
                                {userData && !_.isEmpty(userData.additionalDocument1 && userData.additionalDocument1.document) ? 'Re-upload' : 'Upload'}
                              </button>
                            </div>
                          </div>
                          {userData && !_.isEmpty(userData.additionalDocument1 && userData.additionalDocument1.document) ?
                            <>
                              <div className='text-center p-5 bg-secondary'>
                                <div>
                                  <Document
                                    file={userData && userData.additionalDocument1 && userData.additionalDocument1.document}
                                    onLoadSuccess={onDocumentLoadSuccess}
                                  >
                                    <Page pageNumber={currentPage} renderTextLayer={false} renderAnnotationLayer={false} />
                                  </Document>
                                  <p>
                                    Page {currentPage} of {numPages}
                                  </p>
                                  <button className="btn btn-sm btn-light-danger" onClick={goToPreviousPage} disabled={currentPage <= 1}>Previous</button>
                                  <button className="btn btn-sm btn-light-primary ms-2" onClick={goToNextPage} disabled={numPages === 1}>Next</button>
                                </div>
                              </div>
                            </> :
                            null
                          }
                        </div>

                        <div className="my-3">
                          <div className="d-flex justify-content-between align-items-center">
                            <div className="fs-3 h6 font-weight-600 mb-4">
                              {
                                userData && userData.additionalDocument2 && !_.isEmpty(userData.additionalDocument2.label) ? userData.additionalDocument2.label : 'Additional Document 2'
                              }
                            </div>
                            <div className="d-flex justify-content-end mb-4">
                              <button
                                className="btn btn-sm btn-light-primary "
                                onClick={() => {
                                  {
                                    editShow2 ? EditData2() : setShowAdditionalDoc2(true)
                                  }
                                }}
                              >
                                {userData && !_.isEmpty(userData.additionalDocument2 && userData.additionalDocument2.document) ? 'Re-upload' : 'Upload'}
                              </button>
                            </div>
                          </div>
                          {userData && !_.isEmpty(userData.additionalDocument2 && userData.additionalDocument2.document) ?
                            <>
                              <div className='text-center p-5 bg-secondary'>
                                <div>
                                  <Document
                                    file={userData && userData.additionalDocument2 && userData.additionalDocument2.document}
                                    onLoadSuccess={onDocumentLoadSuccess2}
                                  >
                                    <Page pageNumber={currentPages} renderTextLayer={false} renderAnnotationLayer={false} />
                                  </Document>
                                  <p>
                                    Page {currentPages} of {pageNumber}
                                  </p>
                                  <button className="btn btn-sm btn-light-danger" onClick={goToPreviousPages} disabled={currentPages <= 1}>Previous</button>
                                  <button className="btn btn-sm btn-light-primary ms-2" onClick={goToNextPages} disabled={pageNumber === 1}>Next</button>
                                </div>
                              </div>
                            </> :
                            null
                          }
                        </div>

                        <div className="my-3">
                          <div className="d-flex justify-content-between align-items-center">
                            <div className="fs-3 h6 font-weight-600 mb-4">
                            {
                           userData && userData.additionalDocument3 && !_.isEmpty(userData.additionalDocument3.label) ? userData.additionalDocument3.label : 'Additional Document 3'
                            }
                            </div>
                            <div className="d-flex justify-content-end mb-4">
                              <button
                                className="btn btn-sm btn-light-primary "
                                onClick={() => {
                                  {
                                    editShow3 ? EditData3() : setShowAdditionalDoc3(true)
                                  }
                                }}
                              >
                                {userData && !_.isEmpty(userData.additionalDocument3 && userData.additionalDocument3.document) ? 'Re-upload' : 'Upload'}
                              </button>
                            </div>
                          </div>
                          {userData && !_.isEmpty(userData.additionalDocument3 && userData.additionalDocument3.document) ?
                            <>
                              <div className='text-center p-5 bg-secondary'>
                                <div>
                                  <Document
                                    file={userData && userData.additionalDocument3 && userData.additionalDocument3.document}
                                    onLoadSuccess={onDocumentLoadSuccess3}
                                  >
                                    <Page pageNumber={currentPages3} renderTextLayer={false} renderAnnotationLayer={false} />
                                  </Document>
                                  <p>
                                    Page {currentPages3} of {pageNumber3}
                                  </p>
                                  <button className="btn btn-sm btn-light-danger" onClick={goToPreviousPages3} disabled={currentPages3 <= 1}>Previous</button>
                                  <button className="btn btn-sm btn-light-primary ms-2" onClick={goToNextPages3} disabled={pageNumber3 === 1}>Next</button>
                                </div>
                              </div>
                            </> :
                            null
                          }
                        </div>


                        <div className="my-3">
                          <div className="d-flex justify-content-between align-items-center">
                            <div className="fs-3 h6 font-weight-600 mb-4">
                            {
                           userData && userData.additionalDocument4 && !_.isEmpty(userData.additionalDocument4.label) ? userData.additionalDocument4.label : 'Additional Document 4'
                            }
                            </div>
                            <div className="d-flex justify-content-end mb-4">
                              <button
                                className="btn btn-sm btn-light-primary "
                                onClick={() => {
                                  {
                                    editShow4 ? EditData4() : setShowAdditionalDoc4(true)
                                  }
                                }}
                              >
                                {userData && !_.isEmpty(userData.additionalDocument4 && userData.additionalDocument4.document) ? 'Re-upload' : 'Upload'}
                              </button>
                            </div>
                          </div>
                          {userData && !_.isEmpty(userData.additionalDocument4 && userData.additionalDocument4.document) ?
                            <>
                              <div className='text-center p-5 bg-secondary'>
                                <div>
                                  <Document
                                    file={userData && userData.additionalDocument4 && userData.additionalDocument4.document}
                                    onLoadSuccess={onDocumentLoadSuccess4}
                                  >
                                    <Page pageNumber={currentPages4} renderTextLayer={false} renderAnnotationLayer={false} />
                                  </Document>
                                  <p>
                                    Page {currentPages4} of {pageNumber4}
                                  </p>
                                  <button className="btn btn-sm btn-light-danger" onClick={goToPreviousPages4} disabled={currentPages4 <= 1}>Previous</button>
                                  <button className="btn btn-sm btn-light-primary ms-2" onClick={goToNextPages4} disabled={pageNumber4 === 1}>Next</button>
                                </div>
                              </div>
                            </> :
                            null
                          }
                        </div>
                      </div>
                      {/* {
                        FirstOption && !_.isEmpty(FirstOption) ? (

                          <div className='row'>
                            <h4>
                              Upload Document
                            </h4>
                            <div className='col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4'>
                              <ReactSelect
                                isClearable
                                styles={customStyles}
                                isMulti={false}
                                name='First'
                                placeholder="Select..."
                                className="basic-single"
                                classNamePrefix="select"
                                handleChangeReactSelect={handleChangeFirst}
                                options={FirstOption}
                                value={selectedFirstOption}
                                isDisabled={!FirstOption}
                              />
                              {errors.First && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <div className='fv-help-block'>
                                    <span role='alert'>{errors.First}</span>
                                  </div>
                                </div>
                              )}
                            </div>
                            <div className='col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-4'>
                              <input
                                type="file"
                                className="d-none"
                                name="dashboardImg"
                                id="dashboardImg"
                                multiple={false}
                                ref={commonFileInput}
                                onChange={FileChangeHandlers} />
                              <button type="button"
                                className={`${!showCommon ? 'btn btn-light-primary btn-sm' : 'btn btn-sm btn-success'}`}
                                onClick={handleComonClick}>
                                {FormResLoadig
                                  ? (
                                    'Uploading...'
                                  )
                                  : (
                                    <>
                                      {fileGName}
                                    </>
                                  )}
                              </button>
                              {errors && errors.ImageData && (
                                <div className="rr mt-1">
                                  <style>
                                    {
                                      ".rr{color:red}"
                                    }
                                  </style>
                                  {errors.ImageData}
                                </div>
                              )}
                              {
                                showCommon ? (
                                  <button
                                    className="btn btn-sm btn-light-primary ms-2"
                                    onClick={handleSubmit}
                                  >
                                    {
                                      DashCommonUploadLoadig ?
                                        (
                                          'loading...'
                                        ) : ('Submit')
                                    }

                                  </button>
                                ) : (
                                  null
                                )
                              }
                            </div>
                          </div>
                        ) : null
                      } */}
                    </div>
                  </Card.Body>
                </Tab>
              </Tabs>
            </Card>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  )
}


const mapStateToProps = state => {
  const { Form80Store, FullKycValueStore, DashCommonUploadStore, DashReUploadStore, uploadViewTypeStore, KYCDocumentdeleteStore, CommonFileStore } = state
  return {
    FormRes: Form80Store && Form80Store.Form80Res ? Form80Store.Form80Res : {},
    FormResLoadig: Form80Store && Form80Store.loading ? Form80Store.loading : false,
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    FullKycResDataLoading: FullKycValueStore && FullKycValueStore.loading ? FullKycValueStore.loading : false,
    DashCommonUploadLoadig: DashCommonUploadStore && DashCommonUploadStore.loading ? DashCommonUploadStore.loading : false,
    DashCommonUploadRes: DashCommonUploadStore && DashCommonUploadStore.DashCommonUploadRes ? DashCommonUploadStore.DashCommonUploadRes : {},
    DashReUploadRes: DashReUploadStore && DashReUploadStore.DashReUploadRes ? DashReUploadStore.DashReUploadRes : {},
    DashReUploadResLoadig: DashReUploadStore && DashReUploadStore.loading ? DashReUploadStore.loading : false,
    uploadViewRes: uploadViewTypeStore && uploadViewTypeStore.uploadViewRes ? uploadViewTypeStore.uploadViewRes : {},
    uploadViewResLoadig: uploadViewTypeStore && uploadViewTypeStore.loading ? uploadViewTypeStore.loading : false,
    DeleteKYCDocumentData: KYCDocumentdeleteStore && KYCDocumentdeleteStore.DeleteKYCDocumentData ? KYCDocumentdeleteStore.DeleteKYCDocumentData : '',
    CommonFileDocument: CommonFileStore && CommonFileStore.commonFileRes ? CommonFileStore.commonFileRes : {},
    CommonFileLoading: CommonFileStore && CommonFileStore.loading ? CommonFileStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  formUploadDispatch: (params) => dispatch(form80UploadAction.form80Upload(params)),
  clearformUploadDispatch: (params) => dispatch(form80UploadAction.clearform80Upload(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params)),
  DashComUploadDocDispatch: (id, params) => dispatch(DashComUploadAction.DashComUploadDoc(id, params)),
  clearDashComUploadDocDispatch: (id, params) => dispatch(DashComUploadAction.clearDashComUploadDoc(id, params)),
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
  DashReUploadDocDispatch: (id, params) => dispatch(DashReUploadAction.DashReUploadDoc(id, params)),
  clearDashReUploadDocDispatch: (id) => dispatch(DashReUploadAction.clearDashReUploadDoc(id)),
  viewUploadDocDispatch: (id) => dispatch(uploadViewAction.viewUploadDoc(id)),
  clearviewUploadDocDispatch: (id) => dispatch(uploadViewAction.clearviewUploadDoc(id)),
  deleteKYCDocumentDispatch: (params) => dispatch(KYCDocumentdeleteActions.delete(params)),
  clearDeleteKYCDocumentDispatch: () => dispatch(KYCDocumentdeleteActions.clear()),
  CommonFileDispatch: (params) => dispatch(CommomFileAction.CommonFile(params)),
  ClearCommonFileDispatch: (params) => dispatch(CommomFileAction.clearCommonFile(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DocumentView)