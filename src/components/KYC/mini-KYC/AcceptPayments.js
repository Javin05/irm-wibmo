import React, { useEffect, useRef, useState } from 'react'

function AcceptPayments() {
  return (
    <div>
          <h3 className='mb-8 d-flex justify-content-center'>Where Do You Want Accept Payment ?</h3>
      <div className="form-check form-check-custom form-check-solid form-check-sm">
        <input
          className="form-check-input"
          type="checkbox"
          value="Website"
          id="flexRadioLg"
          name='acceptPaymentWeb'
        //   onChange={(e) => handleChange(e)}
        />
        <label className="form-check-label text-muted" for="flexRadioLg">
         Website
        </label>
      </div>
      {/* <span className='border border-gray' /> */}
      <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
        <input
          className="form-check-input"
          type="checkbox"
          value="AndroidApp"
          id="flexRadioLg"
          name='acceptPaymentApp'
        //   onChange={(e) => handleChange(e)}
        />
        <label className="form-check-label text-muted" for="flexRadioLg">
          Android App
        </label>
      </div>
      <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
        <input
          className="form-check-input"
          type="checkbox"
          value="acceptPaymentApp"
          id="flexRadioLg"
          name='acceptPaymentApp'
          // onChange={(e) => handleChange(e)}
        />
        <label className="form-check-label text-muted" for="flexRadioLg">
         ios App
        </label>
      </div>
      <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
        <input
          className="form-check-input"
          type="checkbox"
          value="acceptPaymentApp"
          id="flexRadioLg"
          name='acceptPaymentApp'
        //   onChange={(e) => handleChange(e)}
        />
        <label className="form-check-label text-muted" for="flexRadioLg">
          Social Media (like whatsapp, Facebook, Instagram)
        </label>
      </div>
      
    </div>
  )
}
export default AcceptPayments