
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businesDetailValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { KYC_FORM, REGEX, STATUS_RESPONSE } from '../../../utils/constants'
import _, { upperCase } from 'lodash'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import {
  CityActions,
  KYCpanAction,
  KYCcinAction,
  KYCgstinAction,
  KYCPersonalpanAction
} from '../../../store/actions'
import color from '../../../utils/colors'
import { warningAlert } from "../../../utils/alerts"

function BusinessDetailsForm(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
    clientDetails,
    KYCbusinessPanLoading,
    KycPanVerifyDispatch,
    KybusinessPanRes,
    KYCbusinessCinLoading,
    KycCinVerifyDispatch,
    KYCcinResponse,
    KYCgstinLoading,
    KycGstinVerifyDispatch,
    KYCgstinResponse,
    PersonalPanDispatch,
    PersonalPanResponse,
    PersonalPanLoading
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [showVerify, setshowVerify] = useState(false)
  const [cinVerify, setCinVerify] = useState(false)
  const [gstinVerify, setGstinVerify] = useState(false)
  const [personalPanVerify, setPersonalPanVerify] = useState(false)
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [formData, setFormData] = useState({
    businessName: '',
    panNumber: '',
    panOwnerName: '',
    address: '',
    pinCode: '',
    state: '',
    city: '',
    businessPan: ''
  })

  const validaName = clientDetails && clientDetails.contactInfo && clientDetails.contactInfo.contactName
  const businessType = clientDetails && clientDetails.business && clientDetails.business.businessType
  setLocalStorage('VALIDNAME', validaName)
  setLocalStorage('BUSINESSTYPE', businessType)

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = () => {
    const errorMsg = businesDetailValidation(formData, setErrors, validaName)
    if (_.isEmpty(errorMsg)) {
      setClientDetails((values) => ({ ...values, businessDetails: formData }))
      // setSummary((values) => ({
      //   ...values,
      //   user: {
      //     roleId: getRolelabel && getRolelabel.label
      //   }
      // }))
      setLocalStorage(KYC_FORM.BUSINESS_DETAILS, JSON.stringify(formData))
    }
  }

  const handleNextClick = () => {
    onClickNext(3)
    // const errorMsg = businesDetailValidation(formData, setErrors)
    // if (_.isEmpty(errorMsg)) {
    //   onClickNext(3)
    // }
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.state)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.state._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  const handleChangeState = selectedOption => {
    if (selectedOption !== null) {
      setSelectedStateOption(selectedOption)
      setFormData(values => ({ ...values, state: selectedOption.value, city: '' }))
      if (selectedOption.value) {
        const params = {
          stateId: selectedOption.value,
          skipPagination: 'true'
        }
        setTimeout(() => {
          getCityDispatch(params)
        }, 1500)
      }
      setSelectedCityOption()
    } else {
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData(values => ({ ...values, state: '', city: '' }))
    }
    setErrors({ ...errors, state: '' })
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const handleChangeCity = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData(values => ({ ...values, city: selectedOption.value }))
    } else {
      setSelectedCityOption()
      setFormData(values => ({ ...values, city: '', area: '', address: '' }))
    }
    setErrors({ ...errors, city: '' })
  }

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.city)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.city._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  const businessPanVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.businessPan)) {
      errors.businessPan = 'BusinessPan Is Required'
    } else if (formData.businessPan && !REGEX.PAN.test(formData.businessPan)) {
      errors.businessPan = 'BusinessPan Number Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        businessPan: formData.businessPan
      }
      KycPanVerifyDispatch(data)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (KybusinessPanRes && KybusinessPanRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setshowVerify(true)
    } else if (KybusinessPanRes && KybusinessPanRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KybusinessPanRes && KybusinessPanRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KybusinessPanRes])


  const PersonalPanVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.panNumber)) {
      errors.panNumber = 'PanNumber Is Required'
    } else if (formData.panNumber && !REGEX.PAN.test(formData.panNumber)) {
      errors.panNumber = 'PanNumber Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        panNumber: formData.panNumber
      }
      PersonalPanDispatch(data)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (PersonalPanResponse && PersonalPanResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setPersonalPanVerify(true)
    } else if (PersonalPanResponse && PersonalPanResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        PersonalPanResponse && PersonalPanResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [PersonalPanResponse])

  return (
    <>
      <div>
        <h3 className='mb-4 d-flex justify-content-center'>Business Details</h3>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>

            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                    <span className='required'>Business Pan</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Business Pan'
                    ></i>
                  </label>
                </div>
                <div className='col-lg-6'>
                  <button type='submit' className={`btn btn-sm ${!showVerify ? `btn-light-primary` : `btn-success`}`}
                    onClick={() => { businessPanVerify() }}
                    disabled={KYCbusinessPanLoading}
                  >
                    {!KYCbusinessPanLoading &&
                      <span className='indicator-label'>
                        <i className={`${!showVerify ? `bi bi-person-check-fill` : `bi bi-person-check-fill`}`} />
                        {!showVerify ? `Verify` : `Verified`}
                      </span>
                    }
                    {KYCbusinessPanLoading && (
                      <span className='indicator-progress' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                </div>
              </div>
              <input
                type='text'
                // style={{textTransform:"upperCase"}}
                className='form-control form-control-lg form-control-solid'
                name='businessPan'
                placeholder='Business Pan'
                onChange={(e) => handleChange(e)}
                value={formData.businessPan || ''}
                maxLength={10}
              />
              {errors && errors.businessPan && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.businessPan}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Business Name</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Business Name'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='businessName'
                placeholder='Business Name'
                onChange={(e) => handleChange(e)}
                value={formData.businessName || ''}
              />
              {errors && errors.businessName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.businessName}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                    <span className='required'>Authorized Signature Pan</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Authorized Signature Pan'
                    ></i>
                  </label>
                </div>
                <div className='col-lg-6'>
                  <button type='submit' className={`btn btn-sm ${!personalPanVerify ? `btn-light-primary` : `btn-success`}`}
                    onClick={() => { PersonalPanVerify() }}
                    disabled={PersonalPanLoading}
                  >
                    {!PersonalPanLoading &&
                      <span className='indicator-label'>
                        <i className={`${!personalPanVerify ? `bi bi-person-check-fill` : `bi bi-person-check-fill`}`} />
                        {!personalPanVerify ? `Verify` : `Verified`}
                      </span>
                    }
                    {PersonalPanLoading && (
                      <span className='indicator-progress' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                </div>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='panNumber'
                placeholder='Authorized Signature Pan'
                onChange={(e) => handleChange(e)}
                value={formData.panNumber || ''}
              />
              {errors && errors.panNumber && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.panNumber}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Pan Owners Name</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Pan Owners Name'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='panOwnerName'
                placeholder='Pan Owners Name'
                onChange={(e) => handleChange(e)}
                value={formData.panOwnerName || ''}
                onKeyPress={(e) => {
                  if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                    e.preventDefault()
                  }
                }}
              />
              {errors && errors.panOwnerName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.panOwnerName}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Address</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Address'
                ></i>
              </label>
              <textarea
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='address'
                placeholder='Address'
                onChange={(e) => handleChange(e)}
                value={formData.address || ''}
              />
              {errors && errors.address && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.address}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Pin Code</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Pin Code'
                ></i>
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='pinCode'
                placeholder='Pin Code'
                onChange={(e) => handleChange(e)}
                value={formData.pinCode || ''}
                maxLength={6}
                onKeyPress={(e) => {
                  if (!/[0-9{1-3}.]/.test(e.key)) {
                    e.preventDefault()
                  }
                }}
              />
              {errors && errors.pinCode && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.pinCode}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>State</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='City'
                ></i>
              </label>
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name='state'
                className='basic-single'
                classNamePrefix='select'
                handleChangeReactSelect={handleChangeState}
                options={stateOption}
                value={selectedStateOption}
                isDisabled={!stateOption}
              />
              {errors.state && (
                <div className='fv-plugins-message-container text-danger'>
                  <span role='alert text-danger'>{errors.state}</span>
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>City</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='City'
                ></i>
              </label>
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name='city'
                className='basic-single'
                classNamePrefix='select'
                handleChangeReactSelect={handleChangeCity}
                options={cityOptions}
                value={selectedCityOption}
                isDisabled={!cityOptions}
              />
              {errors.city && (
                <div className='fv-plugins-message-container text-danger'>
                  <span role='alert text-danger'>{errors.city}</span>
                </div>
              )}
            </div>
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2'>
                  <button
                    onClick={() => { goBack(1) }}
                    type='button'
                    className='btn btn-sm btn-light-primary me-3'
                  >
                    <KTSVG
                      path='/media/icons/duotune/arrows/arr063.svg'
                      className='svg-icon-4 me-1'
                    />
                    Back
                  </button>
                </div>
                <div>
                  {
                    showForm ? (
                      <button type='submit' className='btn btn-sm btn-primary me-3'
                        onClick={(event) => {
                          handleSubmit(event)
                          handleNextClick()
                        }}
                      >
                        <span className='indicator-label'
                        >
                          <KTSVG
                            path='/media/icons/duotune/arrows/arr064.svg'
                            className='svg-icon-3 ms-2 me-0'
                          />
                        </span>
                        {loading
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true'
                            />
                          )
                          : (
                            'Submit'
                          )}
                      </button>
                    ) :
                      null
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, KYCbusinessPanStore, KYCcinStore, KYCgstinStore, PersonalPanStore } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    KYCbusinessPanLoading: KYCbusinessPanStore && KYCbusinessPanStore.loading ? KYCbusinessPanStore.loading : false,
    KybusinessPanRes: KYCbusinessPanStore && KYCbusinessPanStore.KybusinessPan ? KYCbusinessPanStore.KybusinessPan : {},
    KYCbusinessCinLoading: KYCcinStore && KYCcinStore.loading ? KYCcinStore.loading : false,
    KYCcinResponse: KYCcinStore && KYCcinStore.KycCIN ? KYCcinStore.KycCIN : {},
    KYCgstinLoading: KYCgstinStore && KYCgstinStore.loading ? KYCgstinStore.loading : false,
    PersonalPanLoading: PersonalPanStore && PersonalPanStore.loading ? PersonalPanStore.loading : false,
    PersonalPanResponse: PersonalPanStore && PersonalPanStore.personalPan ? PersonalPanStore.personalPan : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycPanVerifyDispatch: (params) => dispatch(KYCpanAction.KYCpanVerify(params)),
  KycCinVerifyDispatch: (params) => dispatch(KYCcinAction.KYCcinVerify(params)),
  PersonalPanDispatch: (params) => dispatch(KYCPersonalpanAction.KYCpersonalpanVerify(params))

})

export default connect(mapStateToProps, mapDispatchToProps)(BusinessDetailsForm)