import React, { useState, useEffect } from "react"
import { Modal } from "../../../theme/layout/components/modal/Modal"
import { connect } from "react-redux"
import { KTSVG } from "../../../theme/helpers"
import { VerticalStepper } from "./verticalStepper"
import {
  setLocalStorage,
  getLocalStorage,
  removeLocalStorage,
} from "../../../utils/helper"
import { KYC_FORM } from "../../../utils/constants"
import ShowFields from "../ShowFields"
import { useLocation } from "react-router-dom"
import ContactInfo from "./ContactInfo"
import BusinessForm from "./Business"
import ComptedPage from "./completed"
import EmailInfo from "./Email"
import { StateActions, KYCAddAction, FullKycValueAction } from "../../../store/actions"
import AccountVerification from "../full-KYC/AccountVerify"
import Address from "../full-KYC/Address"
import FullKyc from "../full-KYC/completeKyc"
import FullKycCompletedPage from "../full-KYC/completed"
import IdentityVerification from '../full-KYC/IdentifyVerificaion'
import { confirmationAlert } from "../../../utils/alerts"
import NewVideoKyc from "./NewVideoKyc"

function KYCminiAdd(props) {
  const { setShow, show, loading, getStateDispatch, clearPhoneDispatch, activeKyc, setActiveKyc, clearFullKycValueDispatch, fullKycDispatch, FullKycResData, FullKycLoading } =
    props
  const [activeStep, setActiveStep] = useState(0)
  const [completedSteps, setCompletedSteps] = useState([-1])
  const [array, setArray] = useState([])
  const [clientDetails, setClientDetails] = useState({})
  const [fullKycDetails, setFullKycDetails] = useState({})
  const [summary, setSummary] = useState({})
  const [editMode, setEditMode] = useState(false)
  const active = getLocalStorage(KYC_FORM.ACTIVE_STEP)
  const [kycShow, setKycShow] = useState(false)
  const organName = clientDetails && clientDetails.business && clientDetails.business.entiryName ? clientDetails.business.entiryName : ''

  useEffect(() => {
    const params = {
      skipPagination: "true",
    }
    getStateDispatch(params)
  }, [])

  const stepperArray = [
    {
      stepperLabel: "Mobile Verification",
      Optional: "",
    },
    {
      stepperLabel: "Email",
      Optional: "",
    },
    {
      stepperLabel: "Business Information ",
      Optional: "",
    },
    {
      stepperLabel: "Bank Account Details",
      Optional: "",
    },
    {
      stepperLabel: "Document Section",
      Optional: "",
    },
    {
      stepperLabel: "Business Address ",
      Optional: "",
    }
  ]

  const kycStepperArray = [
    {
      stepperLabel: "Bank Account Details",
      Optional: "",
    },
    {
      stepperLabel: "Document Section",
      Optional: "",
    },
    {
      stepperLabel: "Business Address ",
      Optional: "",
    },
    {
      stepperLabel: "Complete",
      Optional: "",
    },
  ]

  const onClickNext = (currentId) => {
    const id = currentId - 1
    setActiveStep(currentId)
    if (completedSteps.length === currentId) {
      if (completedSteps && !completedSteps.includes(id)) {
        setCompletedSteps((values) => [...values, id])
        setLocalStorage(KYC_FORM.ACTIVE_STEP, currentId)
      }
    }
  }

  const goBack = (id) => {
    setActiveStep(id)
  }

  const pathName = useLocation().pathname
  const url = pathName && pathName.split("update")
  const id = url && url[1]
  const urlName = pathName && pathName.split("update/")
  const clientId = urlName && urlName[1]

  const getDescendants = (arr, step) => {
    const val = []
    arr.forEach((item, i) => {
      if (step > i) {
        val.push(i)
      }
    })
    return val
  }
  const d = getDescendants(array, active)
  const editstepper = getDescendants(array, array.length)

  useEffect(() => {
    if (kycShow) {
      setArray(kycStepperArray)
      setActiveStep(0)
      setCompletedSteps([-1])
    } else {
      setArray(stepperArray)
      setCompletedSteps([-1])
      setActiveStep(0)
    }
  }, [kycShow])

  useEffect(() => {
    return () => {
      setKycShow(false)
      removeLocalStorage("ADDRESSVERIFY")
      removeLocalStorage("CONTACT_DETAILS")
      removeLocalStorage("BUSINESS")
      removeLocalStorage("BUSINESS_DETAILS")
      removeLocalStorage("BANK_ACCOUNT")
      removeLocalStorage("VALIDNAME")
      removeLocalStorage("BUSINESSTYPE")
      removeLocalStorage("ADDRESSVERIFY")
      removeLocalStorage("AccountVerify")
      removeLocalStorage("CINandGSTIN")
    }
  }, [])

  useEffect(() => {
    if (active) {
      setActiveStep(Number(active))
      setCompletedSteps([-1, ...d])
    } else {
      setActiveStep(0)
      setCompletedSteps([-1])
    }
  }, [active])

  useEffect(() => {
    onClickNext(0)
  }, [])

  useEffect(() => {
    setLocalStorage(KYC_FORM.ORGANIZATIONS, JSON.stringify(organName))
    if (activeKyc && activeKyc.kycData && activeKyc.kycData.kycName === 'CONTINUE WITH FULL KYC') {
      setKycShow(true)
      setShow(true)
    } else {
      setShow(false)
      setKycShow(false)
    }
  }, [activeKyc])

  const onConfirm = () => {
    setShow(false)
    setKycShow(false)
  }

  const showMessage = () => {
    confirmationAlert(
      'Are you sure you want to cancel the application. You will lose data of current application?',
      '',
      'warning',
      'Yes',
      'No',
      () => { onConfirm() },
      () => { { } }
    )
  }

  return (
    <>
      <Modal showModal={show} modalWidth={1200}>
        <div className="" id="userModal">
          <div>
            <div
              className="modal-dialog modal-dialog-centered mw-1200px"
              style={{ backgroundColor: "transparent" }}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h2 className="me-8 mt-4 mb-4 ms-4">{!kycShow ? "Merchant Details" : "Business Information"}</h2>
                  <button
                    type="button"
                    className="btn btn-lg btn-icon btn-active-light-danger me-4"
                    data-dismiss="modal"
                    onClick={() => {
                      showMessage()
                      onClickNext(0)
                      setClientDetails(null)
                      clearPhoneDispatch()
                      removeLocalStorage("CONTACT_DETAILS")
                      removeLocalStorage("BUSINESS")
                      removeLocalStorage("BUSINESS_DETAILS")
                      removeLocalStorage("BANK_ACCOUNT")
                      removeLocalStorage("VALIDNAME")
                      removeLocalStorage("BUSINESSTYPE")
                      removeLocalStorage("ADDRESSVERIFY")
                      removeLocalStorage("AccountVerify")
                      removeLocalStorage("CINandGSTIN")
                      setActiveKyc('')
                      setCompletedSteps([-1])
                      setActiveStep(0)
                    }}
                  >
                    <KTSVG
                      path="/media/icons/duotune/arrows/arr061.svg"
                      className="svg-icon-1"
                    />
                  </button>
                </div>
                <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                <div className="container-fixed">
                  <div className="card-header">
                    <div className="card-body">
                      <div
                        className="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid"
                        id="kt_modal_create_app_stepper"
                      >
                        <div className="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                          <VerticalStepper
                            activeStep={activeStep}
                            setActiveStep={setActiveStep}
                            completedSteps={completedSteps}
                            stepperArr={array}
                            setCompletedSteps={setCompletedSteps}
                            onStepperClick={onClickNext}
                            kycShow={kycShow}
                          />
                        </div>
                        <div
                          className="flex-row-fluid py-lg-5 px-lg-15 -bs-gray-100 mt-10"
                          style={{
                            backgroundoutline: "#f1faff",
                          }}
                        >
                          <ShowFields>
                            <div className={activeStep === 7 ? "px-5": "scroll h-400px px-5"}>
                              <>
                                {activeStep === 0 ? (
                                  <ContactInfo
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                  />
                                ) : null}
                                {activeStep === 1 ? (
                                  <EmailInfo
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    kycAllDataSaved={clientDetails}
                                  />
                                ) : null}

                                {activeStep === 2 ? (
                                  <BusinessForm
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    setCompletedSteps={setCompletedSteps}
                                    setActiveStep={setActiveStep}
                                    kycAllDataSaved={clientDetails}
                                  />
                                ) : null}
                                {activeStep === 3 ? (
                                  <AccountVerification
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    setFullKycDetails={setFullKycDetails}
                                    kycAllDataSaved={clientDetails}
                                  />
                                ) : null}
                                {activeStep === 4 ? (
                                  <IdentityVerification
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    clientDetails={clientDetails}
                                    setFullKycDetails={setFullKycDetails}
                                    organName={organName}
                                    kycAllDataSaved={clientDetails}
                                  />
                                ) : null}
                                {activeStep === 5 ? (
                                  <Address
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    clientDetails={clientDetails}
                                    setFullKycDetails={setFullKycDetails}
                                    kycAllDataSaved={clientDetails}
                                  />
                                ) : null}
                                {activeStep === 6 ? (
                                  <FullKycCompletedPage
                                    onClickNext={onClickNext}
                                    goBack={goBack}
                                    setClientDetails={setClientDetails}
                                    setSummary={setSummary}
                                    clientDetails={clientDetails}
                                    setKycShow={setKycShow}
                                    kycAllDataSaved={clientDetails}
                                    fullKycDetails={fullKycDetails}
                                    setShow={setShow}
                                    setCompletedSteps={setCompletedSteps}
                                    setActiveStep={setActiveStep}
                                  />
                                ) : null}
                              </>
                            </div>
                          </ShowFields>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}
const mapStateToProps = (state) => {
  const {
    usertypeStore,
    addUsertypeStore,
    editUsertypeStore,
  } = state
  return {
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData ? usertypeStore.userTypeData : {},
      FullKycResData: state && state.FullKycValueStore && state.FullKycValueStore.FullKycValue,
      FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  clearPhoneDispatch: () => dispatch(KYCAddAction.clearKYC()),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(KYCminiAdd)
