
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import _ from 'lodash'
import { KYCphoneVerify } from '../../../store/actions'
import "toastify-js/src/toastify.css"

function ContactName(props) {
  const {
    onClickNext,
    setClientDetails
  } = props

  const [errors, setErrors] = useState({})
  const [showOtp, setShowOtp] = useState(false)
  const [showValidateOtp, setShowValidateOtp] = useState(false)
  const [formData, setFormData] = useState({
    contactName: '',
  })

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.contactName)) {
      errors.contactName = "Contact Name is required"
    }
    if (_.isEmpty(errors)) {
      onClickNext(2)
      setClientDetails((values) => ({...values, contactName : formData.contactName}))
    }
    setErrors(errors)
  }


  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
                    <div className='row mb-4'>
                      <div className='col-lg-6'>
                        <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                          <span className='required'>What's your name?</span>
                          <i
                            className='fas fa-exclamation-circle ms-2 fs-7'
                            data-bs-toggle='tooltip'
                            title='Contact Name'
                          ></i>
                        </label>
                      </div>
                    </div>
                    <input
                      type='text'
                      className='form-control form-control-lg form-control-solid mb-4'
                      name='contactName'
                      placeholder='Contact Name'
                      onChange={(e) => handleChange(e)}
                      value={formData.contactName || ''}
                      onKeyPress={(e) => {
                        if (!/^[a-zA-Z_ ]+$/.test(e.key)) {
                          e.preventDefault()
                        }
                      }}
                    />
                    {errors && errors.contactName && (
                      <div className='rr mt-1'>
                        <style>{'.rr{color:red;}'}</style>
                        {errors.contactName}
                      </div>
                    )}
            </div>
            <div className='fv-row mb-4'>
              <div className='d-flex align-items-center pt-10 justify-content-end'>
                <div>
                  <button type='submit' className='btn btn-sm btn-light-primary'
                    onClick={() => { handleSubmit() }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/arrows/arr064.svg'
                        className='svg-icon-3 ms-2 me-0'
                      />
                      Next
                    </span>
                  </button>
                  <button type='submit' className='btn btn-sm btn-light-danger ms-2'
                    onClick={() => { onClickNext(2) }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Skip
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists
})

const mapDispatchToProps = (dispatch) => ({
  KycPhoneVerifyDispatch: (data) => dispatch(KYCphoneVerify.KYCphoneVerifyList(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactName)