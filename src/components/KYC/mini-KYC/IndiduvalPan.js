
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businesDetailValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { KYC_FORM, REGEX, STATUS_RESPONSE } from '../../../utils/constants'
import _, { upperCase } from 'lodash'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import {
  CityActions,
  KYCpanAction,
  KYCcinAction,
  KYCgstinAction,
  KYCPersonalpanAction
} from '../../../store/actions'
import color from '../../../utils/colors'
import { warningAlert, confirmationAlert } from "../../../utils/alerts"

function IndividualPanPage(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
    clientDetails,
    KYCbusinessPanLoading,
    KycPanVerifyDispatch,
    KybusinessPanRes,
    KYCbusinessCinLoading,
    KycCinVerifyDispatch,
    KYCcinResponse,
    KYCgstinLoading,
    KycGstinVerifyDispatch,
    KYCgstinResponse,
    PersonalPanDispatch,
    PersonalPanResponse,
    PersonalPanLoading,
    PersonalPanClear
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [showVerify, setshowVerify] = useState(false)
  const [cinVerify, setCinVerify] = useState(false)
  const [gstinVerify, setGstinVerify] = useState(false)
  const [personalPanVerify, setPersonalPanVerify] = useState(false)
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [formData, setFormData] = useState({
    individualPan: '',
    individualPanName: '',
  })

  const validaName = clientDetails && clientDetails.contactInfo && clientDetails.contactInfo.contactName
  const businessType = clientDetails && clientDetails.business && clientDetails.business.businessType
  setLocalStorage('VALIDNAME', validaName)
  setLocalStorage('BUSINESSTYPE', businessType)

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = () => {
    // const errorMsg = businesDetailValidation(formData, setErrors, validaName)
    const errors = {}
    if (_.isEmpty(formData.individualPanName)) {
      errors.individualPanName = 'IndividualPanName Is Required'
    }
    if (_.isEmpty(errors)) {
      onClickNext(5)
      setClientDetails((values) => ({ ...values, individualPanData: formData }))
    }
    setErrors(errors)

    // if (_.isEmpty(errorMsg)) {
    //   setClientDetails((values) => ({ ...values, businessDetails: formData }))
    //   // setSummary((values) => ({
    //   //   ...values,
    //   //   user: {
    //   //     roleId: getRolelabel && getRolelabel.label
    //   //   }
    //   // }))
    //   setLocalStorage(KYC_FORM.BUSINESS_DETAILS, JSON.stringify(formData))
    // }
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.state)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.state._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  const handleChangeState = selectedOption => {
    if (selectedOption !== null) {
      setSelectedStateOption(selectedOption)
      setFormData(values => ({ ...values, state: selectedOption.value, city: '' }))
      if (selectedOption.value) {
        const params = {
          stateId: selectedOption.value,
          skipPagination: 'true'
        }
        setTimeout(() => {
          getCityDispatch(params)
        }, 1500)
      }
      setSelectedCityOption()
    } else {
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData(values => ({ ...values, state: '', city: '' }))
    }
    setErrors({ ...errors, state: '' })
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const handleChangeCity = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData(values => ({ ...values, city: selectedOption.value }))
    } else {
      setSelectedCityOption()
      setFormData(values => ({ ...values, city: '', area: '', address: '' }))
    }
    setErrors({ ...errors, city: '' })
  }

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.city)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.city._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  const businessPanVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.businessPan)) {
      errors.businessPan = 'BusinessPan Is Required'
    } else if (formData.businessPan && !REGEX.PAN.test(formData.businessPan)) {
      errors.businessPan = 'BusinessPan Number Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        businessPan: formData.businessPan
      }
      KycPanVerifyDispatch(data)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (KybusinessPanRes && KybusinessPanRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setshowVerify(true)
    } else if (KybusinessPanRes && KybusinessPanRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KybusinessPanRes && KybusinessPanRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KybusinessPanRes])

  const CinVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.cinIn)) {
      errors.cinIn = 'CIN Is Required'
    } else if (formData.cinIn && !REGEX.CIN.test(formData.cinIn)) {
      errors.cinIn = 'CIN Number Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        cinIn: formData.cinIn
      }
      KycCinVerifyDispatch(data)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (KYCcinResponse && KYCcinResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setCinVerify(true)
    } else if (KYCcinResponse && KYCcinResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KYCcinResponse && KYCcinResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KYCcinResponse])

  const GstinVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.gstIn)) {
      errors.gstIn = 'GSTIN Is Required'
    } else if (formData.gstIn && !REGEX.GSTIN_REGEX.test(formData.gstIn)) {
      errors.gstIn = 'GSTIN Number Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        gstIn: formData.gstIn,
        businessName: formData.businessName
      }
      KycGstinVerifyDispatch(data)
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (KYCgstinResponse && KYCgstinResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setGstinVerify(true)
    } else if (KYCgstinResponse && KYCgstinResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KYCgstinResponse && KYCgstinResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KYCgstinResponse])

  const PersonalPanVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.individualPan)) {
      errors.individualPan = 'IndividualPan Is Required'
    } else if (formData.individualPan && !REGEX.PAN.test(formData.individualPan)) {
      errors.individualPan = 'IndividualPan Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        individualPan: formData.individualPan
      }
      PersonalPanDispatch(data)
    }
    setErrors(errors)
  }

  const onConfirm = (data) => {
    setFormData({
      individualPanName: data,
      individualPan: formData.individualPan
    })
  }
  useEffect(() => {
    if (PersonalPanResponse && PersonalPanResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setPersonalPanVerify(true)
      confirmationAlert(
        'Confirm Your Name is',
        `${PersonalPanResponse && PersonalPanResponse.name}?`,
        'warning',
        'Yes,confirm',
        'No',
        () => { onConfirm(PersonalPanResponse && PersonalPanResponse.name) },
        () => { { } }
      )
      PersonalPanClear()
    } else if (PersonalPanResponse && PersonalPanResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        PersonalPanResponse && PersonalPanResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      PersonalPanClear()
    }
  }, [PersonalPanResponse])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            {
              !personalPanVerify ? (
                <div className='fv-row mb-10'>
                  <div className='row mb-4'>
                    <div className='col-lg-12'>
                      <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                        <span className='required fw-bold'>What's the PAN number of the authorized Signatory?</span>
                      </label>
                    </div>
                    <div className='col-lg-12'>
                      <label className='d-flex align-items-center mb-2'>
                        <span className='text-muted fs-6 fw-bold'>We require this to verify your identify</span>
                        <i
                          className='fas fa-exclamation-circle ms-2 fs-7'
                          data-bs-toggle='tooltip'
                          title='Individual Pan'
                        ></i>
                      </label>
                    </div>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid'
                    name='individualPan'
                    placeholder='Authorized Signature PAN'
                    onChange={(e) => handleChange(e)}
                    value={formData.individualPan || ''}
                  />
                  {errors && errors.individualPan && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.individualPan}
                    </div>
                  )}
                </div>
              ) : (
                <div className='fv-row mb-10'>
                  <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                    <span className='required'>Individual Pan Owner Name</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Individual Pan Owner Name'
                    />
                  </label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid'
                    name='individualPanName'
                    placeholder='Pan Owners Name'
                    onChange={(e) => handleChange(e)}
                    value={formData.individualPanName || ''}
                    onKeyPress={(e) => {
                      if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                        e.preventDefault()
                      }
                    }}
                  />
                  {errors && errors.individualPanName && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.individualPanName}
                    </div>
                  )}
                </div>

              )
            }
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10 justify-content-end'>
                <div className='me-2'>
                  {
                    !personalPanVerify ? (

                      <button type='submit' className={`btn btn-sm ${!personalPanVerify ? `btn-light-primary` : `btn-success`}`}
                        onClick={() => { PersonalPanVerify() }}
                        disabled={PersonalPanLoading}
                      >
                        {!PersonalPanLoading &&
                          <span className='indicator-label'>
                            <i className={`${!personalPanVerify ? `bi bi-person-check-fill` : `bi bi-person-check-fill`}`} />
                            {!personalPanVerify ? `Continue` : `Verified`}
                          </span>
                        }
                        {PersonalPanLoading && (
                          <span className='indicator-progress' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    ) : (
                      <button type='submit' className={`btn btn-sm ${!personalPanVerify ? `btn-light-primary` : `btn-primary`}`}
                        onClick={() => { handleSubmit() }}
                      >
                        <KTSVG
                          path='/media/icons/duotune/arrows/arr064.svg'
                          className='svg-icon-3 ms-2 me-0'
                        />
                        Next
                      </button>
                    )
                  }
                  <button type='submit' className='btn btn-sm btn-light-danger ms-2'
                    onClick={() => { onClickNext(5) }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Skip
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, KYCbusinessPanStore, KYCcinStore, KYCgstinStore, PersonalPanStore } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    KYCbusinessPanLoading: KYCbusinessPanStore && KYCbusinessPanStore.loading ? KYCbusinessPanStore.loading : false,
    KybusinessPanRes: KYCbusinessPanStore && KYCbusinessPanStore.KybusinessPan ? KYCbusinessPanStore.KybusinessPan : {},
    KYCbusinessCinLoading: KYCcinStore && KYCcinStore.loading ? KYCcinStore.loading : false,
    KYCcinResponse: KYCcinStore && KYCcinStore.KycCIN ? KYCcinStore.KycCIN : {},
    KYCgstinLoading: KYCgstinStore && KYCgstinStore.loading ? KYCgstinStore.loading : false,
    KYCgstinResponse: KYCgstinStore && KYCgstinStore.Kycgstin ? KYCgstinStore.Kycgstin : {},
    PersonalPanLoading: PersonalPanStore && PersonalPanStore.loading ? PersonalPanStore.loading : false,
    PersonalPanResponse: PersonalPanStore && PersonalPanStore.personalPan ? PersonalPanStore.personalPan : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycPanVerifyDispatch: (params) => dispatch(KYCpanAction.KYCpanVerify(params)),
  KycCinVerifyDispatch: (params) => dispatch(KYCcinAction.KYCcinVerify(params)),
  KycGstinVerifyDispatch: (params) => dispatch(KYCgstinAction.KYCgstinVerify(params)),
  PersonalPanDispatch: (params) => dispatch(KYCPersonalpanAction.KYCpersonalpanVerify(params)),
  PersonalPanClear: (params) => dispatch(KYCPersonalpanAction.clearKYCpersonalpanVerify(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(IndividualPanPage)