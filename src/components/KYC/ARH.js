import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  SWEET_ALERT_MSG,
} from "../../utils/constants"
import {
  KycStatusChangeActions,
  KycUpdateQueueAction
} from "../../store/actions"
import { useLocation } from "react-router-dom"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../utils/alerts"
import clsx from "clsx"
import Modal from 'react-bootstrap/Modal'

function Status(props) {
  const {
    loading,
    KycStatusDispatch,
    KycStatusChangeResponce,
    clearKycStatusDispatch,
    formData,
    setFormData,
    KycUpdateQueue,
    clearKycUpdateQueue,
    KycUpdateQueueResponse
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const [rejectValue, setRejectValue] = useState()
  const [show, setShow] = useState(false)
  const [updateQueue, setUpdateQueue] = useState(false)
  const [queueData, setQueueData] = useState({})
  const [rejectShow, setRejectShow] = useState(false)
  const [errors, setErrors] = useState({
    reason: "",
    updateQueue:""
  })

  const [rejectFormData, setRejectFormData] = useState({
    status: "REJECTED",
    reason: "",
    rejectType: "",
    rejectMoreValue: "",
  })
  const [approveFormData, setApproveFormData] = useState({
    status: "APPROVED",
    reason: "",
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setQueueData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleQueue = () => {
    const errors = {}
    if (_.isEmpty(queueData.updateQueue)) {
      errors.updateQueue = USER_ERROR.UPDATE_QUEUE
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        ids:formData,
        queueName: queueData && queueData.updateQueue,
      }
      KycUpdateQueue(params)
    }
  }

  const approveSubmit = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        ids: formData,
        comments: approveFormData.reason,
        kycStatus: "APPROVED"
      }
      KycStatusDispatch(params)
    }
  }

  const onConfirmReject = () => {
    const params = {
      ids: formData,
      comments: rejectFormData.reason,
      kycStatus: rejectFormData.status
    }
    KycStatusDispatch(params)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmReject()
        },
        () => { }
      )
    }
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
    setRejectValue(e.target.value)
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  useEffect(() => {
    if (KycUpdateQueueResponse && KycUpdateQueueResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        KycUpdateQueueResponse && KycUpdateQueueResponse.message,
        'success'
      )
      setUpdateQueue(false)
      setQueueData('')
      clearKycUpdateQueue()
      setFormData([])
    } else if (KycUpdateQueueResponse && KycUpdateQueueResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KycUpdateQueueResponse && KycUpdateQueueResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearKycUpdateQueue()
    }
  }, [KycUpdateQueueResponse])

  useEffect(() => {
    if (KycStatusChangeResponce && KycStatusChangeResponce.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        KycStatusChangeResponce && KycStatusChangeResponce.message,
        'success'
      )
      setShow(false)
      setRejectShow(false)
      clearKycStatusDispatch()
      setFormData([])
    } else if (KycStatusChangeResponce && KycStatusChangeResponce.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KycStatusChangeResponce && KycStatusChangeResponce.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearKycStatusDispatch()
    }
  }, [KycStatusChangeResponce])


  const clearPopup = () => {
    setShow(false)
    setRejectShow(false)
    setUpdateQueue(false)
    setRejectFormData({
      reason: "",
      rejectType: "",
      rejectMoreValue: ""
    })
    setApproveFormData({
      comment: '',
      reason: "",
    })
    setErrors({})
  }

  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to Approve This Users Risk Analytics Report ?

          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Reason For Approved
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 ">
                    <textarea
                      name="reason"
                      type="text"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            approveFormData.reason && errors.reason,
                        },
                        {
                          "is-valid":
                            approveFormData.reason && !errors.reason,
                        }
                      )}
                      placeholder="Message"
                      onChange={(e) => approveChange(e)}
                      autoComplete="off"
                      value={approveFormData.reason || ""}
                    />
                    {errors.reason && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.reason}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                      type="submit"
                      className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                      onClick={(event) => {
                        approveSubmit(event)
                      }}
                      disabled={loading}
                    >
                      {!loading && (
                        "Submit"
                      )}
                      {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                      )}
                    </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      <Modal
        show={rejectShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Status
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Reason For Reject :
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                    <textarea
                      name="reason"
                      type="text"
                      // className='form-control'
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid": rejectFormData.reason && errors.reason,
                        },
                        {
                          "is-valid": rejectFormData.reason && !errors.reason,
                        }
                      )}
                      placeholder="Message"
                      onChange={(e) => rejectChange(e)}
                      autoComplete="off"
                      value={rejectFormData.reason || ""}
                    />
                    {errors.reason && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.reason}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                      type="submit"
                      className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                      onClick={(event) => {
                        rejectSubmit(event)
                      }}
                      disabled={loading}
                    >
                      {!loading && (
                        "Submit"
                      )}
                      {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                      )}
                    </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      <Modal
        show={updateQueue}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to Update This Users Risk Analytics Report ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Update Queue
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 ">
                    <select
                      name='updateQueue'
                      className='form-select form-select-solid mb-4'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                    >
                      <option value=''>Select...</option>
                      <option value='SUSPECT_KYC_Q'>SUSPECT_KYC_Q</option>
                      <option value='KYC_Q'>KYC_Q</option>
                    </select>
                    {errors.updateQueue && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.updateQueue}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                        type="submit"
                        className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                        onClick={(event) => {
                          handleQueue(event)
                        }}
                        disabled={loading}
                      >
                        {!loading && (
                          "Submit"
                        )}
                        {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className="card-toolbar d-flex">
        <>
          <ul className="nav">
            <li className="nav-item">
              <a
                className="btn btn-sm btn-responsive fw-bold font-7vw me-1 btn-light-success ms-2"
                onClick={() => {
                  setUpdateQueue(true)
                }}
              >
                Update Queues
              </a>
            </li>
            <li className="nav-item">
              <a
                className="btn btn-sm fw-bold font-7vw me-1 btn-light-success ms-2 btn-responsive "
                onClick={() => {
                  setShow(true)
                }}
              >
                Approve
              </a>
            </li>
            <li className="nav-item">
              <a
                className="btn btn-sm fw-bold font-7vw me-1 btn-light-danger btn-responsive "
                onClick={() => {
                  setRejectShow(true)
                }}
              >
                Reject
              </a>
            </li>
          </ul>
        </>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    kycStatusARStore,
    KycUpdateQueueStore
  } = state
  return {
    KycStatusChangeResponce: kycStatusARStore && kycStatusARStore.KycStatusChangeResponce ? kycStatusARStore.KycStatusChangeResponce : {},
    loading: kycStatusARStore && kycStatusARStore.loading ? kycStatusARStore.loading : false,
    KycUpdateQueueResponse: KycUpdateQueueStore && KycUpdateQueueStore.KycUpdateQueueRes ? KycUpdateQueueStore.KycUpdateQueueRes :{},
  }
}

const mapDispatchToProps = (dispatch) => ({
  KycStatusDispatch: (params) => dispatch(KycStatusChangeActions.KycStatusChange(params)),
  clearKycStatusDispatch: () => dispatch(KycStatusChangeActions.clearKycStatusChange()),
  KycUpdateQueue: (params) => dispatch(KycUpdateQueueAction.KycUpdateQueueDoc(params)),
  clearKycUpdateQueue: () => dispatch(KycUpdateQueueAction.clearKycUpdateQueueDoc()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Status)