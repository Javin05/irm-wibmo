
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG } from '../../theme/helpers'
import { connect } from 'react-redux'
import { AccountValidation } from './Validation'
import { setLocalStorage } from '../../utils/helper'
import { KYC_FORM } from '../../utils/constants'
import _ from 'lodash'

function BankAccount(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)

  const [formData, setFormData] = useState({
    beneficiaryName: '',
    reEnterAccountNumber:'',
    IFSCcode: '',
    accountNumber: ''
  })


  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = AccountValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      setClientDetails((values) => ({ ...values, bannkAccount: formData }))
      // setSummary((values) => ({
      //   ...values,
      //   user: {
      //     roleId: getRolelabel && getRolelabel.label
      //   }
      // }))
      setLocalStorage(KYC_FORM.BANK_ACCOUNT, JSON.stringify(formData))
    }
  }

  const handleNextClick = () => {
    const errorMsg = AccountValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      onClickNext(4)
    }
  }

  return (
    <>
    <div>
    <h3 className='mb-4 d-flex justify-content-center'>Bank Account</h3>
      <div className='current' data-kt-stepper-element='content'>
        <div className='w-100'>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Beneficiary Name</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Beneficiary Name'
              />
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='beneficiaryName'
              placeholder='Beneficiary Name'
              onChange={(e) => handleChange(e)}
              value={formData.beneficiaryName || ''}
              onKeyPress={(e) => {
                if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
            {errors && errors.beneficiaryName && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.beneficiaryName}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Branch IFSC Code</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Branch IFSC Code'
              />
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='IFSCcode'
              placeholder='Branch IFSC Code'
              onChange={(e) => handleChange(e)}
              value={formData.IFSCcode || ''}
              maxLength={11}
            />
            {errors && errors.IFSCcode && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.IFSCcode}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Account Number</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Account Number'
              />
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='accountNumber'
              placeholder='Account Number'
              onChange={(e) => handleChange(e)}
              value={formData.accountNumber || ''}
              maxLength={16}
              onKeyPress={(e) => {
                if (!/[0-9{1-3}.]/.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
            {errors && errors.accountNumber && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.accountNumber}
              </div>
            )}
          </div>
          
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Re-Enter Account Number</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Re-Enter Account Number'
              />
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='reEnterAccountNumber'
              placeholder='Re-Enter Account Number'
              onChange={(e) => handleChange(e)}
              value={formData.reEnterAccountNumber || ''}
              maxLength={16}
              onKeyPress={(e) => {
                if (!/[0-9{1-3}.]/.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
            {errors && errors.reEnterAccountNumber && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.reEnterAccountNumber}
              </div>
            )}
          </div>
          <div className='fv-row'>
            <div className='d-flex flex-stack pt-10'>
              <div className='me-2'>
                <button
                  onClick={() => { goBack(1) }}
                  type='button'
                  className='btn btn-sm btn-light-primary me-3'
                >
                  <KTSVG
                    path='/media/icons/duotune/arrows/arr063.svg'
                    className='svg-icon-4 me-1'
                  />
                  Back
                </button>
              </div>
              <div>
                {
                  showForm ? (
                    <button type='submit' className='btn btn-sm btn-primary me-3'
                      onClick={(event) => {
                        handleSubmit(event)
                        handleNextClick()
                      }}
                    >
                      <span className='indicator-label'
                      >
                        <KTSVG
                          path='/media/icons/duotune/arrows/arr064.svg'
                          className='svg-icon-3 ms-2 me-0'
                        />
                      </span>
                      {loading
                        ? (
                          <span
                            className='spinner-border spinner-border-sm mx-3'
                            role='status'
                            aria-hidden='true'
                          />
                        )
                        : (
                          'Next'
                        )}
                    </button>
                  ) :
                    null
                }
              </div>
            </div>

          </div>
        </div>
      </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(BankAccount)