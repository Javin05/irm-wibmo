
import React, { useState, useCallback, useEffect, useRef, useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import {
  SWEET_ALERT_MSG,
  STATUS_RESPONSE,
  UPDATE_PERMISSION,
  RISKSTATUS,
  DROPZONE_MESSAGES,
  DOCUMENT_URL,
  USER_ERROR
} from "../../utils/constants"
import Modal from 'react-bootstrap/Modal'
import { toAbsoluteUrl } from "./../../theme/helpers"
import _ from 'lodash'
import { useLocation } from "react-router-dom"
import { FullKycValueAction, AccountUploadAction } from "../../store/actions"
import { connect } from "react-redux"
import { confirmationAlert } from "../../utils/alerts"
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'
import {
  DROPZONE_IMAGE_NAME_TYPES,
  RESTRICTED_FILE_FORMAT_TYPE
} from "../../constants/index";
import clsx from "clsx"
import { KycContext } from '../kycDashboard/StaticComponent'

function BankVerifyData(props) {
  const {
    fullKycDispatch,
    FullKycResData,
    clearAccountUploadDocDispatch,
    AccountUploadDocDispatch,
    AccountUploadRes,
    AccountUploadResLoading
  } = props

  const userData = useContext(KycContext)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const documentUrl = DOCUMENT_URL
  const pathNames = '/KYC'
  const getUsersPermissions = getUserPermissions(pathNames, true)

  const [currentImage, setCurrentImage] = useState('')
  const [isViewerOpen, setIsViewerOpen] = useState(false)
  const [cancelChequeShow, setCancelChequeShow] = useState(false)
  const [show, setShow] = useState(false)
  const [rejectShow, setRejectShow] = useState(false)
  const openImageViewer = useCallback((index) => {
    setCurrentImage(index)
    setIsViewerOpen(true)
  }, [])
  const [cancelledChequeUpload, setcancelledChequeUpload] = useState('Upload')
  const [showchecque, setShowchecque] = useState(false)
  const cancelledFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [formData, setFormData] = useState({
    cancelledCheque: ''
  })
  const [rejectFormData, setRejectFormData] = useState({
    status: "REJECTED",
    reason: "",
    rejectType: "",
    rejectMoreValue: "",
  })
  const [approveFormData, setApproveFormData] = useState({
    status: "APPROVED",
    reason: "",
  })

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const ocrLabel = !_.isEmpty(userData.chequeOcrDetails) ? Object.keys(userData.chequeOcrDetails) : '--'
  const ocrValue = !_.isEmpty(userData.chequeOcrDetails) ? Object.values(userData.chequeOcrDetails) : '--'
  let ocrLabelData = []
  const ocrLabelUC = _.find(ocrLabel, function (o) {
    const data = o.replaceAll("_", " ")
    ocrLabelData.push(
      data
        .toLowerCase()
        .split(' ')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')
    )
  })

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.cancelledCheque)) {
      errors.cancelledCheque = 'Upload Is Required'
    }
    if (_.isEmpty(errors)) {
      const params = {
        cancelledCheque: `${documentUrl}${formData.cancelledCheque}`,
      }
      fullKycDispatch(id, params)
      setCancelChequeShow(false)
    }
    setErrors(errors)
  }

  const FileChangeHandler = (e) => {
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', 'cancelledCheque')
        data.append('file_to_upload', files)
        AccountUploadDocDispatch(data)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
        setShowchecque(false)
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID })
      setShowchecque(false)
    }
  }

  const cancelledFilePanClick = (event) => {
    cancelledFilesInput.current.click(event);
  }

  useEffect(() => {
    if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = AccountUploadRes && AccountUploadRes.data && AccountUploadRes.data.path
      setFormData((values) => ({ ...values, cancelledCheque: data }))
      setcancelledChequeUpload('Uploaded')
      setShowchecque(true)
      clearAccountUploadDocDispatch()
    } else if (AccountUploadRes && AccountUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, cancelledCheque: '' }))
      clearAccountUploadDocDispatch()
      setcancelledChequeUpload('Upload')
      setShowchecque(false)
    }
  }, [AccountUploadRes])

  useEffect(() => {
    if (!cancelChequeShow) {
      setShowchecque(false)
      setFormData({
        cancelledCheque: ''
      })
    }
  }, [cancelChequeShow])

  const approveSubmit = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        bankVerificationStatus: 'APPROVED',
      }
      fullKycDispatch(id, params)
      setShow(false)
    }
  }

  const onConfirmReject = () => {
    const params = {
      bankVerificationStatus: 'REJECTED',
    }
    fullKycDispatch(id, params)
    setRejectShow(false)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmReject()
        },
        () => { }
      )
    }
  }

  const clearPopup = () => {
    setShow(false)
    setRejectShow(false)
  }

  return (
    <>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Are You Sure Want to Approve ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Approved
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 ">
                      <textarea
                        name="reason"
                        type="text"
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              approveFormData.reason && errors.reason,
                          },
                          {
                            "is-valid":
                              approveFormData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => approveChange(e)}
                        autoComplete="off"
                        value={approveFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => approveSubmit()}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={rejectShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Are You Sure Want to Reject ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Reject :
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                      <textarea
                        name="reason"
                        type="text"
                        // className='form-control'
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid": formData.reason && errors.reason,
                          },
                          {
                            "is-valid": formData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => rejectChange(e)}
                        autoComplete="off"
                        value={rejectFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => rejectSubmit()}
                        >
                          <span className="indicator-label">
                            Submit
                          </span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>




      <Modal
        show={cancelChequeShow}
        size="lg"
        centered
        onHide={() => setCancelChequeShow(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => setCancelChequeShow(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Cancelled Cheque
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="card-body p-0">
              <div className="col-lg-6">
                <input
                  type="file"
                  className="d-none mt-4"
                  name="cancelledCheque"
                  id="cancelledCheque"
                  multiple={false}
                  ref={cancelledFilesInput}
                  onChange={FileChangeHandler} />
                <span className='me-4 fs-5 fw-bolder'>
                  Cancelled Cheque :
                </span>
                <button type="button"
                  className={`${!showchecque ? 'btn btn-light-primary btn-sm mt-4' : 'btn btn-sm btn-success mt-4'}`}
                  onClick={cancelledFilePanClick}>
                  {AccountUploadResLoading
                    ? (
                      'Uploading...'
                    )
                    : (
                      <>
                        {cancelledChequeUpload}
                      </>
                    )}
                </button>
                {errors && errors.cancelledCheque && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red;}'}</style>
                    {errors.cancelledCheque}
                  </div>
                )}
              </div>
              <div className="col-lg-6">
                <div className="d-flex justify-content-end">
                  <button
                    type="submit"
                    className="btn btn-sm btn-light-primary"
                    onClick={handleSubmit}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={isViewerOpen}
        size="lg"
        centered
        onHide={() => setIsViewerOpen(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => setIsViewerOpen(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-1" />
            <div className="col-lg-10">
              <div className="card card-custom overlay overflow-hidden">
                <div className="card-body p-0">
                  <div className="overlay-wrapper">
                    <img
                      src={currentImage}
                      alt=""
                      className="w-100 rounded"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-1" />
          </div>
        </Modal.Body>
      </Modal>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <h2> Bank Verification</h2>{" "}
            <i class="fa fa-times-circle-o" aria-hidden="true"></i>
          </Accordion.Header>
          <Accordion.Body>
            <Card>
              <Card.Body>
                <div className="row">
                  <div className="row mt-4"
                  >
                    <div className="mb-4 row">
                      <div className="col-lg-6">
                        <div className="row">
                          <div className="col-lg-5">
                            <h4>Cancelled Cheque</h4>
                          </div>
                          <div className="col-lg-2">
                            <span className={`badge ${RISKSTATUS[userData.bankVerificationStatus && userData.bankVerificationStatus]} ms-4`}>
                              {userData.bankVerificationStatus ? userData.bankVerificationStatus : "--"}
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <Can
                          permissons={getUsersPermissions}
                          componentPermissions={UPDATE_PERMISSION}
                        >
                          <div className="d-flex justify-content-end">
                            <ul className="nav">
                              {
                                userData && userData.bankVerificationStatus === "PENDING" && !_.isEmpty(userData && userData.cancelledCheque) ? (
                                  <>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                                        onClick={() => {
                                          setShow(true)
                                        }}
                                      >
                                        Approve
                                      </a>
                                    </li>
                                    <li className="nav-item">
                                      <a
                                        className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                                        onClick={() => {
                                          setRejectShow(true)
                                        }}
                                      >
                                        Reject
                                      </a>
                                    </li>
                                  </>
                                ) : null
                              }
                              <li className="nav-item">
                                <a
                                  className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-primary ms-2"
                                  onClick={() => {
                                    setCancelChequeShow(true)
                                  }}
                                >
                                  ReUpload
                                </a>
                              </li>
                            </ul>
                          </div>
                        </Can>
                      </div>
                    </div>
                    <div className="col-lg-3" />
                    <div className="col-lg-6 ms-4"
                      style={{
                        backgroundColor: 'aliceblue',
                        borderRadius: '10px'
                      }}
                    >
                      <div className="d-flex justify-content-start">
                        <div className="card card-custom overlay overflow-hidden">
                          <div className="card-body p-0">
                            <div className="overlay-wrapper">
                              <img
                                src={
                                  userData && userData.cancelledCheque ? userData.cancelledCheque : toAbsoluteUrl('/media/logos/no-image.png')
                                }
                                className="w-100 rounded"
                              />
                            </div>
                            <div className="overlay-layer bg-dark bg-opacity-10">
                              <a className="btn btn-light-primary btn-shadow"
                                onClick={() => openImageViewer(userData && userData.cancelledCheque ? userData.cancelledCheque : '--')}
                              >
                                View
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3" />
                  </div>
                  <div className="row mt-8">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      {
                        userData && !_.isEmpty(userData.chequeOcrDetails) ?
                          <div>
                            <h4>OCR Field</h4>
                            <div className="row">
                              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                {
                                  ocrLabelData && ocrLabelData.map((data) => {
                                    return (
                                      <div className="fs-6 fw-bold mb-2 w-100">
                                        {
                                          data ? data.replaceAll("_", " ") : '--'
                                        }
                                      </div>
                                    )
                                  })
                                }
                              </div>
                              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                {
                                  ocrValue.map((data) => {
                                    return (
                                      <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                        {
                                          data ? data : '--'
                                        }
                                      </div>
                                    )
                                  })
                                }
                              </div>
                            </div>
                          </div>
                          : null
                      }
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <legend class="float-none w-auto fs-3 p-2 h6 font-weight-600 ms-2 mt-4">
                        Account Information
                        <span className={`badge ${userData.bankVerification === true ? 'badge-success' : 'badge-danger'} ms-4`}>
                          {userData.bankVerification === true ? 'Verified' : 'Unverified'}
                        </span>
                      </legend>
                      <div className="row">
                        <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                          <div className="ms-4 text-gray-600 fw-bold fs-5">
                            Account Holder Name :
                          </div>
                        </div>
                        <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                          <div className="fw-bolder fs-6 ellipsis">
                            {userData && userData.accountHolderName ? userData.accountHolderName : '--'}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                          <div className="ms-4 text-gray-600 fw-bold fs-5">
                            Account Number :
                          </div>
                        </div>
                        <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                          <div className="fw-bolder fs-6 ellipsis">
                            {userData && userData.bankAccountNumber ? userData.bankAccountNumber : '--'}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                          <div className="ms-4 text-gray-600 fw-bold fs-5">
                            IFSC Code :
                          </div>
                        </div>
                        <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                          <div className="fw-bolder fs-6 ellipsis">
                            {userData && userData.ifscCode ? userData.ifscCode : '--'}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Card.Body>
            </Card>
          </Accordion.Body>
        </Accordion.Item >
      </Accordion >
    </>
  )
}


const mapStateToProps = (state) => {
  const { FullKycValueStore } = state

  return {
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    AccountUploadResLoading: state && state.AccountUploadStore && state.AccountUploadStore.loading,
    AccountUploadRes: state && state.AccountUploadStore && state.AccountUploadStore.AccountUploadRes,
  }
}

const mapDispatchToProps = (dispatch) => ({
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  AccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.AccountUploadDoc(params)),
  clearAccountUploadDocDispatch: (params) => dispatch(AccountUploadAction.clearAccountUploadDoc(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(BankVerifyData)