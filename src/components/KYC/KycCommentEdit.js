import React, { useState, useEffect, Fragment } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import Modal from 'react-bootstrap/Modal'
import clsx from "clsx"
import { warningAlert, successAlert } from "../../utils/alerts"
import { KYCCommentActions, updateKYCCommentActions } from "../../store/actions"
import { kycCommentEditValidation } from "./KYCCommentEditData"
import { setKYCCommentData } from "./formData"
import { STATUS_RESPONSE } from '../../utils/constants'

function CommentEdit(props) {
    const {
        id,
        editMode,
        setEditMode,
        userId,
        setUserId,
        getKYCCommentDispatch,
        UpdateKYCComments,
        updateKYCCommentDispatch,
        clearUpdateDispatch
    } = props
    const [formData, setFormData] = useState({
        kycStatus: "",
        comments: "",
    })
    const [errors, setErrors] = useState({})

    const handleChanges = (e) => {
        e.preventDefault()
        setFormData((values) => ({
            ...values,
            [e.target.name]: e.target.value
        }))
        setErrors({ ...errors, [e.target.name]: '' })
    }

    const clearPopup = () => {
        setEditMode(false)
        setFormData({
            comments: "",
            kycStatus: ""
        })
        setUserId('')
    }

    useEffect(() => {
        const data = setKYCCommentData(userId)
        setFormData(data)
    }, [userId])

    const handleSubmit = () => {
        const errorMsg = kycCommentEditValidation(formData, setErrors)
        const updateValue = {
            comments: formData.comments,
            kycStatus: formData.kycStatus,
        }
        if (_.isEmpty(errorMsg)) {
            const editId = userId && userId._id
            updateKYCCommentDispatch(editId, updateValue)
        }
    }

    useEffect(() => {
        if (UpdateKYCComments.status === STATUS_RESPONSE.SUCCESS_MSG) {
            const params = {
                kycId: id
            }
            successAlert(
                UpdateKYCComments && UpdateKYCComments.message,
                'success'
            )
            getKYCCommentDispatch(params)
            clearUpdateDispatch()
            clearPopup()
        } else if (UpdateKYCComments.status === STATUS_RESPONSE.ERROR_MSG) {
            warningAlert(
                'Error',
                UpdateKYCComments && UpdateKYCComments.message,
                '',
                'Ok',
            )
            clearUpdateDispatch()
        }
    }, [UpdateKYCComments])

    return (
        <>
            <Modal
                show={editMode}
                size="lg"
                centered
                onHide={() => clearPopup()}>
                <Modal.Header
                    style={{ backgroundColor: 'rgb(126 126 219)' }}
                    closeButton={() => clearPopup()}>
                    <Modal.Title
                        style={{
                            color: 'white'
                        }}
                    >
                        Update Comment
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="card card-custom card-stretch gutter-b p-8">
                        <div className="row mb-8">
                            <div className='col-md-4'>
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                    KYC Status :
                                </label>
                            </div>
                            <div className='col-md-8'>
                                <select
                                    name='kycStatus'
                                    className='form-select form-select-solid'
                                    data-control='select'
                                    data-placeholder='Select an option'
                                    data-allow-clear='true'
                                    onChange={(e) => handleChanges(e)}
                                    value={formData && formData.kycStatus || ''}
                                    disabled
                                >
                                    <option value=''>Select...</option>
                                    <option value='APPROVED'>APPROVED</option>
                                    <option value='REJECTED'>REJECTED</option>
                                </select>
                            </div>
                        </div>
                        <div className="row mb-8">
                            <div className='col-md-4'>
                                <label className="font-size-xs font-weight-bold mb-3 form-label">
                                    Comments :
                                </label>
                            </div>
                            <div className='col-md-8'>
                                <textarea
                                    name="comments"
                                    type="text"
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                    )}
                                    placeholder="Comment"
                                    onChange={(e) => handleChanges(e)}
                                    autoComplete="off"
                                    value={formData && formData.comments || ""}
                                />
                                {errors && errors.comments && (
                                    <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.comments}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="row">
                            <div className='col-md-4'>
                            </div>
                            <div className='col-md-8'>
                                <button
                                    className='btn btn-light-primary m-1 mt-8 font-5vw '
                                    onClick={handleSubmit}
                                >
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )

}

const mapStateToProps = (state) => {
    const { updateKYCCommentStore } = state
    return {
        UpdateKYCComments: updateKYCCommentStore && updateKYCCommentStore.saveupdateKYCCommentResponse ? updateKYCCommentStore.saveupdateKYCCommentResponse : ''
    }
}

const mapDispatchToProps = (dispatch) => ({
    getKYCCommentDispatch: (params) => dispatch(KYCCommentActions.getKYCCommentlist(params)),
    updateKYCCommentDispatch: (id, params) => dispatch(updateKYCCommentActions.updateKYCComment(id, params)),
    clearUpdateDispatch: () => dispatch(updateKYCCommentActions.clearupdateKYCComment()),
})

export default connect(mapStateToProps, mapDispatchToProps)(CommentEdit)