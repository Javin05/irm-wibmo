import React, { useEffect, useState, useContext } from "react"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import Card from "react-bootstrap/Card"
import ShowFields from "./ShowFields"
import DocumentView from "./DocumentView"
import Address from "./Address"
import Contacts from "./Contacts"
import ProfileKycDasboard from "./ProfileKycDasboard"
import { Link, useLocation, useParams } from "react-router-dom"
import _ from "lodash"
import { KTSVG } from "../../theme/helpers"
import { KYCUserAction } from "../../store/actions"
import { connect } from "react-redux"
import { get } from "http"
import CrossVerify from './CrossVerify'
import { RISKSTATUS, KYC_SCORE_STATUS, STATUS_RESPONSE } from "../../utils/constants"
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts"
import BankVerifyData from './BankVerify'
import BusinessData from './BusinessDetailss'
import KycVideo from "./KycVideo"
import AdditionalDocument from "./AdditionalDocument"
import { KycContext } from '../kycDashboard/StaticComponent'

function UserProfileKYC(props) {
  const {
    isLoading,
    DistanceRes,
    DashboardAadharRes,
    DashboardPANres,
    DashboardCINres,
    KycScoreres,
    VideoKycRes,
    UENdashboardRes,
    KycStatusres,
    getKYCUserDetailsDispatch,
    UpdateKycDispatch,
    FullKycResData,
    FullKycResDataLoading
  } = props

  const userData = useContext(KycContext)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]

  useEffect(() => {
    if (KycStatusres && KycStatusres.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(KycStatusres && KycStatusres.message, "success")
      getKYCUserDetailsDispatch(id)
    } else if (KycStatusres && KycStatusres.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KycStatusres && KycStatusres.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KycStatusres])


  return (
    <>
      <div className="row mx-1 bg-white">
        <div className="col-sm-12 col-md-12 col-lg-12">
          <Card>
            {userData &&
              typeof userData === "object" &&
              typeof userData !== "undefined" &&
              userData ? (
              <div class="row">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <legend class="float-none w-auto fs-3 p-2 h6 font-weight-600 ms-2 mt-4">
                    Contact Information
                  </legend>
                  <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Primary Contact Name :
                      </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="fw-bolder fs-6 ">
                        {userData && userData.primaryContactName ? userData.primaryContactName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Phone Number :
                      </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="fw-bolder fs-6 ">
                        {userData && userData.phoneNumber && userData.phoneNumber.number ? userData.phoneNumber.number : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="ms-4 text-gray-600 fw-bold fs-5 ">
                        Email :
                      </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="fw-bolder fs-6 ">
                        {userData && userData.emailId && userData.emailId.emailId ? userData.emailId.emailId : '--'}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-4">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div className="row">
                      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div className="ms-4 text-gray-600 fw-bold fs-5">
                          KYC Id : <span className="text-black fw-bolder fs-6">#KYC{userData && userData.kycId}</span>
                        </div>
                      </div>
                      {/* <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <div className="fw-bolder fs-6">
                          #KYC{userData && userData.kycId}
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 mt-4 mb-4">
                      <span className="ms-4 fs-5 fw-bolder">
                        eKYC
                      </span>
                      <span className="ms-4 fs-7 fw-bolder">
                        <i className={` me-1 ${KYC_SCORE_STATUS[userData && userData.emailId && userData.emailId.verified]}`} />
                        Email
                      </span>
                      <span className="ms-4 fs-7 fw-bolder">
                        <i className={`me-1 ${KYC_SCORE_STATUS[userData && userData.phoneNumber && userData.phoneNumber.verified]}`} />
                        Phone
                      </span>
                      <span className="ms-4 fs-7 fw-bolder">
                        {
                          userData && userData.documentVerification === true ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Document
                      </span>
                      <span className="ms-4 fs-7 fw-bolder">
                        {
                          userData && userData.businessAddress && userData.businessAddress.addressVerification === true ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Business Address
                      </span>
                      <span className="ms-4 fs-7">
                        {
                          userData && userData.bankVerificationStatus === "APPROVED" ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Bank Details
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className="d-flex justify-content-center">
                <div className=" spinner-border text-primary m-5" role="status" />
              </div>
            )}
          </Card>
        </div>
      </div>
      <div className='row mt-8'>
        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <div className="row">
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <DocumentView
                DashboardAadharRes={DashboardAadharRes}
                DashboardPANres={DashboardPANres}
                DashboardCINres={DashboardCINres}
                UENdashboardRes={UENdashboardRes}
              />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <CrossVerify DashboardPANres={DashboardPANres} />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <Address DistanceRes={DistanceRes} />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <BankVerifyData />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
            <BusinessData />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <Contacts />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <AdditionalDocument objectId={id} UpdateKycDispatch={UpdateKycDispatch} FullKycResData={FullKycResData} FullKycResDataLoading={FullKycResDataLoading} />
            </div>
          </div>
        </div>
        <div className="col-sm-4 col-md-4 col-lg-4">
          <ProfileKycDasboard KycScoreres={KycScoreres} />
        </div>
      </div>
      {/* ) : (
        <div className="d-flex justify-content-center py-5">
          <div className="spinner-border text-primary m-5" role="status" />
        </div>
      )} */}
    </>
  )
}

const mapStateToProps = (state) => {
  const { KycScoreStore, KycStatusStore } = state

  return {
    KycScoreres: KycScoreStore && KycScoreStore.KycScoreres ? KycScoreStore.KycScoreres : {},
    KycStatusres: KycStatusStore && KycStatusStore.KycStatusres ? KycStatusStore.KycStatusres : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileKYC)
