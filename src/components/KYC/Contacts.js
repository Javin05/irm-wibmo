
import React, { useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import { KYC_VERIFY, KYC_VERIFY_DATA } from "../../utils/constants"
import { KycContext } from '../kycDashboard/StaticComponent'

export default function Contacts() {
  const userData = useContext(KycContext)
  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <h2> Contacts</h2>{" "}
          <i class="fa fa-times-circle-o" aria-hidden="true"></i>
        </Accordion.Header>
        <Accordion.Body>
          <Card>
            <Card.Body>
              <div
                className="row block_div"
              >
                <>
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th className="fs-4 fw-bold">Phone</th>
                        <td>
                          {userData && userData.phoneNumber
                            && userData.phoneNumber.number ? userData.phoneNumber.number : '--'}
                          <span
                            className={`ms-2 badge badge-sm ${KYC_VERIFY[userData && userData.phoneNumber
                              && userData.phoneNumber.verified]}`}
                          >
                            {KYC_VERIFY_DATA[userData && userData.phoneNumber
                              && userData.phoneNumber.verified]}
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <th className="fs-4 fw-bold">Email</th>
                        <td>
                          {userData && userData.emailId
                            && userData.emailId.emailId ? userData.emailId.emailId : '--'}
                          <span
                            className={`ms-2 badge badge-sm ${KYC_VERIFY[userData && userData.emailId
                              && userData.emailId.verified]}`}
                          >
                            {KYC_VERIFY_DATA[userData && userData.emailId
                              && userData.emailId.verified]}
                          </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </>
              </div>
            </Card.Body>
          </Card>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
