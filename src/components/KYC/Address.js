import React, { useContext } from "react"
import Map from "../../components/merchant/subComponent/map"
// import Map from "srccomponentsmerchantsubComponentmap.js"
import Accordion from "react-bootstrap/Accordion"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import Card from "react-bootstrap/Card"
import { Row, Col } from "react-bootstrap-v5"
import _ from "lodash"
import MapGoogle from "../kycDashboard/MapGoogle"
import StreetView from '../kycDashboard/StreetView'
import { KycContext } from '../kycDashboard/StaticComponent'

export default function Address(props) {
  const {
    DistanceRes
  } = props

  const userData = useContext(KycContext)
  const DistanceMapData = userData && userData.businessAddress
  const viweLat = DistanceMapData && DistanceMapData.lattitude
  const viweLng = DistanceMapData && DistanceMapData.longitude

  let allMarkers = []
  if (DistanceMapData) {
    allMarkers.push({
      lat: DistanceMapData && DistanceMapData.lattitude,
      lng: DistanceMapData && DistanceMapData.longitude,
      area: "BUSINESS ADDRESS"
    })
  }

  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <h2> Address </h2>{" "}
          <i class="fa fa-times-circle-o" aria-hidden="true"></i>
        </Accordion.Header>
        <Accordion.Body>
          <Card>
            <div className="separator separator-dashed" />

            <Card.Body>
              <div className="row">
                <div className="d-flex justify-content-end mt-4">
                  <span
                    className={`badge badge-sm ${userData && userData.businessAddress && userData.businessAddress.addressVerification === true ? 'badge-success' : 'badge-danger'}`}
                  >
                    {
                      userData && userData.businessAddress && userData.businessAddress.addressVerification === true ? 'Verified' : 'Unverified'
                    }
                  </span>
                </div>
                <div className="col">
                  <>
                    <table class="table table-striped">
                      <tbody>
                        <tr>
                          <th className="fs-4 fw-bold">Company Name</th>
                          <td>{userData.businessName ? userData.businessName : '--'}</td>
                        </tr>
                        <tr>
                          <th className="fs-4 fw-bold">Address</th>
                          <td>{userData.businessAddress && userData.businessAddress.address ? userData.businessAddress.address : '--'}</td>
                        </tr>
                        <tr>
                          <th className="fs-4 fw-bold">State</th>
                          <td>{userData.businessAddress && userData.businessAddress.state ? userData.businessAddress.state : '--'}</td>                        </tr><tr>
                          <th className="fs-4 fw-bold">City</th>
                          <td>{userData.businessAddress && userData.businessAddress.city ? userData.businessAddress.city : '--'}</td>
                        </tr><tr>
                          <th className="fs-4 fw-bold">Pincode</th>
                          <td>{userData.businessAddress && userData.businessAddress.pincode ? userData.businessAddress.pincode : '--'}</td>
                        </tr>
                      </tbody>
                    </table>
                  </>
                  {
                    _.isNumber(viweLat && viweLng) ? (
                      <span className="fw-bolder text-muted fs-6 w-120px mr-2 mt-4 mb-4">
                        ADDRESS VERFICATION
                      </span>
                    ) : (
                      null
                    )
                  }
                </div>
                {
                  _.isNumber(viweLat && viweLng) ? (
                    <div className="row">
                      <div className='col-lg-6'>
                        <MapGoogle mapMarkers={allMarkers} viweLat={viweLat} viweLng={viweLng} />
                      </div>
                      <div className="col-lg-6">
                        <div
                        >
                          {<StreetView splitData={DistanceMapData} />}
                        </div>
                      </div>
                    </div>
                  ) : null
                }
              </div>
            </Card.Body>
          </Card>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  )
}
