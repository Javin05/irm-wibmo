import React, { useState, useCallback, useEffect, useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import { toAbsoluteUrl } from "../../theme/helpers"
import { KYC_VERIFY, KYC_VERIFY_DATA, RISKSTATUS } from "../../utils/constants"
import Modal from 'react-bootstrap/Modal'
import _ from 'lodash'
import KycStatus from "./KycStatus"
import VideoKyc from "../kycDashboard/DashboardVideoKYC"
import { KycContext } from '../kycDashboard/StaticComponent'

export default function KycVideo(props) {
  const { objectId } = props
  const userData = useContext(KycContext)
  const [currentImage, setCurrentImage] = useState('')
  const [isViewerOpen, setIsViewerOpen] = useState(false)
  const [videoOpen, setvideoOpen] = useState(false)
  const [videoSHow, setVideoSHow] = useState(false)

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index)
    setIsViewerOpen(true)
    setvideoOpen(false)
  }, [])

  const OpenVideo = () => {
    setvideoOpen(true)
    setVideoSHow(false)
  }

  useEffect(() =>{
    if(userData && userData.videoKycImage){
      setVideoSHow(false)
    }else{
      setVideoSHow(true)
    }
  },[userData])

  return (
    <>
      <Modal
        show={isViewerOpen}
        size="lg"
        centered
        onHide={() => setIsViewerOpen(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => setIsViewerOpen(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
             Liveness Check Image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-1" />
            <div className="col-lg-10">
              <div className="card card-custom overlay overflow-hidden">
                <div className="card-body p-0">
                  <div className="overlay-wrapper">
                    <img
                      src={currentImage}
                      alt=""
                      className="w-100 rounded"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-1" />
          </div>
        </Modal.Body>
      </Modal>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <h2>Liveness Check</h2>{" "}
            <i class="fa fa-times-circle-o" aria-hidden="true"></i>
          </Accordion.Header>
          <Accordion.Body>
            <Card>
              <Card.Body>
                <div className="row mt-4"
                >
                  {
                    userData &&
                      // !_.isEmpty(userData && userData.videoKycImage) && 
                      userData.videoKycApprovalStatus === "PENDING" ? (
                      <div className="row mb-4">
                        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8" />
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <KycStatus FullKycResData={userData} />
                        </div>
                      </div>

                    ) : (
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end">
                        <span className={`badge ${RISKSTATUS[userData.videoKycApprovalStatus && userData.videoKycApprovalStatus]}`}>
                          {userData.videoKycApprovalStatus ? userData.videoKycApprovalStatus : "--"}
                        </span>
                      </div>
                    )
                  }
                  {
                    videoSHow ? (
                      <div>
                      <button className="btn btn-sm btn-light-dark"
                        onClick={OpenVideo}
                      >
                        Add VideoKYC
                      </button>
                    </div>
                    ):null
                  }
                  {
                    videoOpen ? (
                      <div>
                        <div>
                          <button className="btn btn-sm btn-light-danger"
                            onClick={() => { 
                              setvideoOpen(false)
                              setVideoSHow(true)
                             }}
                          >
                            Cancel
                          </button>
                        </div>
                        <VideoKyc objectId={objectId} setvideoOpen={setvideoOpen} />
                      </div>
                    ) : (
                      null
                    )
                  }
                  <div className="col-lg-3" />
                  {
                    !videoOpen ? (
                      <div className="col-lg-6 ms-4"
                        style={{
                          backgroundColor: 'aliceblue',
                          borderRadius: '10px'
                        }}
                      >
                        <div className="d-flex justify-content-start">
                          {
                            userData && userData.videoKycImage && _.isEmpty(userData && userData.videoKycImage) ? (
                              <div className="d-flex justify-content-center">
                                <h3 className="text-primary">Processing...</h3>
                              </div>
                            ) : (
                              <>
                                <div className="card card-custom overlay overflow-hidden"
                                >
                                  <div className="card-body p-0">
                                    <div>
                                      <div className="overlay-wrapper">
                                        <img
                                          src={
                                            userData && userData.videoKycImage ? userData.videoKycImage : toAbsoluteUrl('/media/logos/no-image.png')
                                          } userData
                                          className="w-100 rounded"
                                        />
                                      </div>
                                      <div className="overlay-layer bg-dark bg-opacity-10">
                                        <a className="btn btn-light-primary btn-shadow"
                                          onClick={() => openImageViewer(userData && userData.videoKycImage ? userData.videoKycImage : '--')}
                                        >
                                          View
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </>
                            )
                          }
                        </div>
                      </div>
                    ) : null
                  }
                  <div className="col-lg-3" />
                </div>
                {
                  userData && userData.videoKycDetails && !_.isEmpty(userData.videoKycDetails) ?
                    <>
                      {userData.videoKycDetails.map((item) =>
                        <div className="row mt-4">
                          <div className="col-lg-5 ms-4">
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Eyes Open:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.are_eyes_open) ? item.result.are_eyes_open ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Confidence:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.confidence ? item.result.confidence.toString() : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Face Detected:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.face_detected) ? item.result.face_detected ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Face Mask Detected:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.face_mask_detected) ? item.result.face_mask_detected ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Face Cropped:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.is_face_cropped) ? item.result.is_face_cropped ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Front Facing:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.is_front_facing ? item.result.is_front_facing.toString() : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Live:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.is_live) ? item.result.is_live ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Not Safe For Work:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.is_nsfw) ? item.result.is_nsfw ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Multiple Faces Detected:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.multiple_faces_detected) ? item.result.multiple_faces_detected ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Review Required:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && _.isEmpty(item.result.review_required) ? item.result.review_required ? 'True' : 'False' : "--"}
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-1" />
                          <div className="col-lg-5 ms-4">
                            <h4>Face Box</h4>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Height:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_box && item.result.face_box.height ? item.result.face_box.height.toString() : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Left:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_box && item.result.face_box.left ? item.result.face_box.left.toString() : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Top:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_box && item.result.face_box.top ? item.result.face_box.top.toString() : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Width:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_box && item.result.face_box.width ? item.result.face_box.width.toString() : '--'}
                              </div>
                            </div>
                            <h4 className="mt-6">Face Coverage</h4>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Message:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_coverage && item.result.face_coverage.message ? item.result.face_coverage.message : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Percentage:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_coverage && item.result.face_coverage.percentage ? item.result.face_coverage.percentage.toString() : '--'}
                              </div>
                            </div>
                            <div className="row mt-2">
                              <div className="col-lg-6 fs-6 fw-bold">
                                Status:
                              </div>
                              <div className="col-lg-6 fs-6 fw-bold text-muted">
                                {item && item.result && item.result.face_coverage && item.result.face_coverage.status ? item.result.face_coverage.status : '--'}
                              </div>
                            </div>
                            {/* <h4 className="mt-6">Face Quality</h4>
                      <div className="row mt-2">
                        <div className="col-lg-6 fs-6 fw-bold">
                          Message :
                        </div>
                        <div className="col-lg-6 fs-6 fw-bold text-muted">
                          {item && item.result && item.result.face_quality && item.result.face_quality.message ? item.result.face_quality.message : '--'}
                        </div>
                      </div>
                      <div className="row mt-2">
                        <div className="col-lg-6 fs-6 fw-bold">
                          Status :
                        </div>
                        <div className="col-lg-6 fs-6 fw-bold text-muted">
                          {item && item.result && item.result.face_quality && item.result.face_quality.status ? item.result.face_quality.status : '--'}
                        </div>
                      </div> */}
                          </div>
                        </div>)}</> : null
                }
              </Card.Body>
            </Card>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  );
}