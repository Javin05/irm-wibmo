import React, { useEffect } from "react";
import {
  KYCActions,
  FullKycValueAction,
  KYCUserAction} from '../../../store/actions'
import { connect } from 'react-redux'
import { STATUS_RESPONSE } from '../../../utils/constants'

function FullKycCompletedPage(props) {
  const {
    setKycShow,
    getKYClistDispatch,
    setActiveStep,
    setCompletedSteps,
    setClientDetails,
    fullKycDispatch,
    UserDetails,
    setEditMode,
    clearFullKycValueDispatch,
    FullKycResData,
    setuserId,
    clearKYCUserDispatch,
    onClickNext
  } = props

  useEffect(() => {
    const params = {
      kycSubmitStatus: 'FULL'
    }
    fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id, params)

  }, [])

  console.log("UserDetails", UserDetails)

  const Finsh = () => {
    setEditMode(false)
    setCompletedSteps([-1])
    setActiveStep(0)
    setKycShow(false)
    setClientDetails(null)
    clearKYCUserDispatch()
    clearFullKycValueDispatch()
  }

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      clearFullKycValueDispatch()
      setuserId('')
      // clearKYCUserDispatch()
    }
  }, [FullKycResData])

  return (
    <>
      <div>
        <div class="card card-flush"
          style={{
            backgroundColor: '#1a1d31'
          }}
        >
          <div class="card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"
          >
            <div className="d-flex justify-content-center">
              <i class="bi bi-check-circle-fill"
                style={{ color: '#208f20', backgroundColor: 'transparent', fontSize: '2.75rem' }}
              />
            </div>
            <div class="fs-1hx fw-bold text-white text-center mb-10 mt-5">
              Your Details Submitted Successfully
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-4">
          <button class="btn btn-light-dark"
            onClick={
              Finsh
            }
          >Back to List</button>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => {
  const { FullKycValueStore } = state
  return {
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params)),
  clearKYCUserDispatch: (id) => dispatch(KYCUserAction.KYCUser_CLEAR(id)),

})

export default connect(mapStateToProps, mapDispatchToProps)(FullKycCompletedPage)
