import React, { useEffect, useState } from "react"
import _, { map } from 'lodash'
import {
  CityActions,
  FullKycValueAction
} from '../../../store/actions'
import { connect } from 'react-redux'
import { STATUS_RESPONSE, KYC_URL, AADHAAR_FRONT, AADHAAR_BACK, DOCUMENT_URL } from "../../../utils/constants"
import { warningAlert } from "../../../utils/alerts"
import {
  COMMON_PROOF_TYPE
} from "../../../constants/index"
import { getLocalStorage } from '../../../utils/helper'

function FullKyc(props) {
  const {
    setKycShow,
    onClickNext,
    fullKycDispatch,
    FullKycResData,
    kycAllDataSaved,
    fullKycDetails,
    FullKycLoading,
    activeKyc,
    clearFullKycValueDispatch,
    UseData
  } = props
  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const [nameChange] = useState(COMMON_PROOF_TYPE[organization])
  const [frontName] = useState(AADHAAR_FRONT[fullKycDetails && fullKycDetails.AddressData && fullKycDetails.AddressData.identityProof])
  const [backName] = useState(AADHAAR_BACK[fullKycDetails && fullKycDetails.AddressData && fullKycDetails.AddressData.identityProof])
  const url = DOCUMENT_URL

  const handleSubmit = () => {
    const AddressData = fullKycDetails && fullKycDetails.AddressData

    const identity = fullKycDetails && !_.isEmpty(fullKycDetails.indidual) ? fullKycDetails.indidual.map((o) => {
      const data = {
        documentName: _.isEmpty(o.uploadDocument) ? '' : o.documentName,
        documentNumber: o.documentNumber,
        uploadDocument: _.isEmpty(o.uploadDocument) ? '' : `${url}${o.uploadDocument}`
      }
      return data
    }): []
    const address = fullKycDetails && !_.isEmpty(fullKycDetails.adress) ? fullKycDetails.adress.map((o) => {
      const data = {
        documentName: _.isEmpty(o.uploadDocument) ? '' : o.documentName,
        documentNumber: o.documentNumber,
        uploadDocument: _.isEmpty(o.uploadDocument) ? '' : `${url}${o.uploadDocument}`
      }
      return data
    }): []
    const business = fullKycDetails && !_.isEmpty(fullKycDetails.business) ? fullKycDetails.business.map((o) => {
      const data = {
        documentName: _.isEmpty(o.uploadDocument) ? '' : o.documentName,
        documentNumber: o.documentNumber,
        uploadDocument: _.isEmpty(o.uploadDocument) ? '' : `${url}${o.uploadDocument}`
      }
      return data
    }): []
    const id = UseData && UseData._id
    const data = {
      "bankAccountNumber": fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.accountDetails,
      "ifscCode": fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.ifscCode,
      "accountHolderName": fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.accountHolderName,
      "cancelledCheque": `${fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.cancelledCheque}` === `${UseData && UseData.cancelledCheque}` ? UseData && UseData.cancelledCheque : `${url}${fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.cancelledCheque}`,
      "documents": {
        "identityProof": identity,
        "otherDocuments": address,
        "businessProof": business
      },
      "businessAddress": {
        "address": AddressData && AddressData.Address,
        "city": AddressData && AddressData.city,
        "state": AddressData && AddressData.state,
        "pincode": AddressData && AddressData.pinCode
      },
      phoneNumber: kycAllDataSaved && kycAllDataSaved.phoneNumber,
      countryCode: kycAllDataSaved && kycAllDataSaved.countryCode,
      emailId: kycAllDataSaved && kycAllDataSaved.Email && kycAllDataSaved.Email.contactEmail,
      businessCategory: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessCategory,
      businessName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessName,
      entityName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.entiryName,
      entityId: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.entityId,
      primaryContactName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.primaryContactName,
      website: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.website,
      businessDescription: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessDescription
    }
    fullKycDispatch(id, data)
  }

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(7)
      clearFullKycValueDispatch()
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [FullKycResData])

  return (
    <>
      {
        !FullKycLoading ? (

          <div>
            <div className="row">
              <div className='col-lg-12'>
                <label className='d-flex align-items-center mb-2'>
                  <span className='text-muted fs-6 fw-bold'>Submit Your All Details</span>
                  <i
                    className='fas fa-exclamation-circle ms-2 fs-7'
                    data-bs-toggle='tooltip'
                    title='AadharNumber'
                  />
                </label>
              </div>
            </div>
            <div className="d-flex justify-content-center mt-4">
              <button type='submit' className='btn btn-sm btn-light-dark me-3'
                onClick={() => {
                  handleSubmit()
                }}
              >
                <i className={`bi bi-person-check-fill`} />
                {FullKycLoading
                  ? (
                    <span
                      className='spinner-border spinner-border-sm mx-3'
                      role='status'
                      aria-hidden='true'
                    />
                  )
                  : (
                    'Submit'
                  )}
              </button>
            </div>
          </div>
        ) : (
          <>
            <div
              className='d-flex justify-content-center mt-20'
            >
              <h4 className='text-muted me-4'>Processing</h4>
              <span
                className='spinner-grow text-primary me-4'
                role='status'
              />
              <span
                className='spinner-grow text-danger me-4'
                role='status'
              />
              <span
                className='spinner-grow text-warning me-4'
                role='status'
              />
              <span
                className='spinner-grow text-info me-4'
                role='status'
              />
            </div>
          </>
        )
      }
    </>
  )
}

const mapStateToProps = state => {
  const { FullKycValueStore } = state
  return {
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    FullKycLoading: FullKycValueStore && FullKycValueStore.loading ? FullKycValueStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(FullKyc)