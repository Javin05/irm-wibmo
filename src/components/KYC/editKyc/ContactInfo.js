import React, { FC, useEffect, useRef, useState } from "react"
import { KTSVG, toAbsoluteUrl } from "../../../theme/helpers"
import { connect } from "react-redux"
import { contactInfoValidation, phoneOtp } from "./Validation"
import { setLocalStorage, getLocalStorage } from "../../../utils/helper"
import { KYC_FORM, REGEX, STATUS_RESPONSE } from "../../../utils/constants"
import _, { values } from "lodash"
import {
  KYCphoneVerify,
  KYCphoneOtpAction,
  KYCAddAction,
  KYCAddALLAction,
  clientIdLIstActions,
  FullKycValueAction
} from "../../../store/actions"
import Toastify from "toastify-js"
import "toastify-js/src/toastify.css"
import { warningAlert } from "../../../utils/alerts"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import { setPhoneData } from './formData'

function ContactInfo(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    KycPhoneVerifyDispatch,
    KycPhoneOtpDispatch,
    KycPhoneVerifyResponse,
    KycPhoneOtp,
    phoneVerifyloading,
    ClearPhoneVerifyDispatch,
    phoneOtpVerifyLoading,
    KYCAddDispatch,
    KYCAddResponse,
    ClearKYCDispatch,
    clearKYCphoneOTP,
    clientIdDispatch,
    clinetIdLists,
    UserDetails,
    clearFullKycValueDispatch,
    FullKycResData,
    FullKycLoading,
    fullKycDispatch
  } = props

  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [errors, setErrors] = useState({})
  const [error, setError] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [Data, setData] = useState(true)
  const [showOtp, setShowOtp] = useState(false)
  const [showSuccess, setShowSuccess] = useState(false)
  const [showValidateOtp, setShowValidateOtp] = useState(false)
  const [contactedit, setContactedit] = useState(false)
  const [updateedit, setUpdateedit] = useState(false)
  const [clientEdit, setClientEdit] = useState(false)
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [formData, setFormData] = useState({
    contactNumber: "",
    clientId: "",
  })
  const [otpData, setOtpData] = useState({
    code: "",
  })

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
  }, [])

  const otpChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setOtpData((values) => ({ ...values, [name]: value }))
    setError({ ...error, [name]: "" })
  }

  const otpSubmit = () => {
    const error = {}
    if (_.isEmpty(otpData.code)) {
      error.code = "Phone Otp is required"
    }
    setError(error)
    if (_.isEmpty(error)) {
      const data = {
        otp: otpData.code,
        phoneNumber: formData.contactNumber.slice(2,),
        countryCode: formData.contactNumber.slice(0, 2),
        clientId: formData.clientId
      }
      KycPhoneOtpDispatch(data)
      setLocalStorage(KYC_FORM.OTP, JSON.stringify(data))
    }
  }

  const phoneVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.contactNumber)) {
      errors.contactNumber = "Phone Number is required"
    }
    // else if (formData.contactNumber.slice(0, 2) === '91' && formData.contactNumber.length !== 12) {
    //   errors.contactNumber = "Phone Number is Invalid"
    // }
    // if (_.isEmpty(formData.clientId)) {
    //   errors.clientId = "Client is required"
    // }
    setError(errors)
    // if (clientEdit) {
    //   const params = {
    //     ...formData, clientId: formData.clientId
    //   }
    //   fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id, params)
    // } else 
    if (!updateedit) {
      if (_.isEmpty(errors)) {
        const data = {
          phoneNumber: formData.contactNumber.slice(2,),
          countryCode: formData.contactNumber.slice(0, 2),
        }
        KycPhoneVerifyDispatch(data)
        setClientDetails((values) => ({
          ...values,
          phoneNumber: formData.contactNumber.slice(2,),
          countryCode: formData.contactNumber.slice(0, 2),
        }))
      }
    } else {
      onClickNext(1)
    }
    setErrors(errors)
  }

  const proccedStore = () => {
    const errors = {}
    if (_.isEmpty(formData.contactNumber)) {
      errors.contactNumber = "Phone Number is required"
    } else if (formData.contactNumber.slice(0, 2) === '91' && formData.contactNumber.length !== 12) {
      errors.contactNumber = "Phone Number is Invalid"
    }
    // if (_.isEmpty(formData.clientId)) {
    //   errors.clientId = "Client is required"
    // }
    setError(errors)
    if (_.isEmpty(errors)) {
      onClickNext(1)
      setClientDetails((values) => ({
        ...values,
        phoneNumber: formData.contactNumber.slice(2,),
        countryCode: formData.contactNumber.slice(0, 2),
        clientId: formData.clientId
      }))
    }
    setErrors(errors)
  }

  const resendOtp = () => {
    const data = {
      contactNumber: formData.contactNumber,
      resend: "1",
    }
    KycPhoneVerifyDispatch(data)
  }

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: "" })
  }

  const conatactFrom = JSON.parse(getLocalStorage(KYC_FORM.CONTACT_DETAILS))
  const OtpData = JSON.parse(getLocalStorage(KYC_FORM.OTP))
  useEffect(() => {
    setFormData({
      contactName: conatactFrom.contactName,
      contactNumber: conatactFrom.contactNumber,
      contactEmail: conatactFrom.contactEmail,
    })
    setOtpData({
      contactNumber: OtpData.contactNumber,
      code: OtpData.code,
    })
  }, [Data])

  useEffect(() => {
    if (showValidateOtp) {
      Toastify({
        text: "Please Verify Your Number",
        duration: 3000,
        newWindow: true,
        close: true,
        gravity: "bottom",
        position: "center",
        stopOnFocus: true,
        style: {
          background:
            "linear-gradient(to right, rgb(176 0 32), rgb(201 87 61))",
        },
      }).showToast()
    }
  }, [showValidateOtp])

  useEffect(() => {
    if (
      KycPhoneVerifyResponse &&
      KycPhoneVerifyResponse.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      Toastify({
        text: "OTP Send Successfully",
        duration: 4000,
        newWindow: true,
        close: true,
        gravity: "top",
        position: "right",
        stopOnFocus: true,
        offset: {
          x: 50,
          y: 10,
        },
        className: "info",
      }).showToast()
      setShowOtp(true)
      ClearPhoneVerifyDispatch()
    } else if (
      KycPhoneVerifyResponse &&
      KycPhoneVerifyResponse.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        KycPhoneVerifyResponse && KycPhoneVerifyResponse.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      )
      ClearPhoneVerifyDispatch()
    }
  }, [KycPhoneVerifyResponse])

  useEffect(() => {
    if (KycPhoneOtp && KycPhoneOtp.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShowOtp(true)
      onClickNext(1)
      const params = {
        ...formData,
        phoneNumber: formData.contactNumber.slice(2,),
        countryCode: formData.contactNumber.slice(0, 2),
      }
      fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id, params)
      setContactedit(true)
      setUpdateedit(false)
      setClientDetails((values) => ({
        ...values,
        phoneNumber: formData.contactNumber.slice(2,),
        countryCode: formData.contactNumber.slice(0, 2),
        clientId: formData.clientId
      }))
    } else if (
      KycPhoneOtp &&
      KycPhoneOtp.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        KycPhoneOtp && KycPhoneOtp.message,
        "",
        "Try again",
        "",
        () => { { } }
      )
    }
  }, [KycPhoneOtp])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(1)
      clearFullKycValueDispatch()
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  useEffect(() => {
    return () => {
      ClearPhoneVerifyDispatch()
      clearKYCphoneOTP()
      ClearKYCDispatch()
      setFormData({
        contactNumber: "",
      })
    }
  }, [])

  const handleBusinessChange = value => {
    setFormData((values) => ({ ...values, contactNumber: value }))
  }
  const handleAlterNativesChange = value => {
    setFormData((values) => ({ ...values, alterNativeNumbar: value }))
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: "" })
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  useEffect(() => {
    if (UserDetails) {
      const data = setPhoneData(UserDetails && UserDetails.data)
      const id = UserDetails && UserDetails.data && UserDetails.data.clientId && UserDetails.data.clientId._id
      const assignee = getDefaultOption(AsigneesNames)
      const selOption = _.find(assignee, function (x) {
        if (_.includes(id, x.value)) {
          setAsignees(x.label)
          return x
        }
      })
      setSelectedAsigneesOption(selOption)
      setFormData(data)
    }
  }, [UserDetails, AsigneesNames])

  useEffect(() => {
    if (UserDetails && UserDetails.data && UserDetails.data.phoneNumber && UserDetails.data.phoneNumber.number && !_.isEmpty(UserDetails.data.phoneNumber.number)) {
      setContactedit(true)
    } else {
      setContactedit(false)
    }
  }, [UserDetails])

  useEffect(() => {
    if(contactedit && UserDetails && UserDetails.data && UserDetails.data.clientId && !_.isEmpty(UserDetails.data.clientId._id)) {
      setUpdateedit(true)
    } else {
      setUpdateedit(false)
      // setClientEdit(true)
    }
  }, [SelectedAsigneesOption, contactedit])

  return (
    <>
      <div>
        <div className="current" data-kt-stepper-element="content">
          <div className="w-100">
            <div className="fv-row mb-10">

              {
                !showOtp ? (
                  <>
                    {
                      Role === 'Admin' ? (
                        <>
                          <div className="d-flex align-items-center fs-5 fw-bold mb-2">
                            Client :
                          </div>
                          <div className='col-md-8'>
                            <ReactSelect
                              styles={customStyles}
                              isMulti={false}
                              name='AppUserId'
                              className='select2'
                              classNamePrefix='select'
                              handleChangeReactSelect={handleChangeAsignees}
                              options={AsigneesOption}
                              value={SelectedAsigneesOption}
                              // isDisabled={SelectedAsigneesOption ? true : false}
                            />
                            {errors && errors.clientId && (
                              <div className="rr mt-1">
                                <style>{".rr{color:red}"}</style>
                                {errors.clientId}
                              </div>
                            )}
                          </div>
                        </>
                      ) : (
                        null
                      )
                    }
                    <div className="row mb-4 mt-8">
                      <div className="col-lg-6">
                        <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                          <span className="required">
                            What's your mobile numer?
                          </span>
                          <i
                            className="fas fa-exclamation-circle ms-2 fs-7"
                            data-bs-toggle="tooltip"
                            title="Contact Number"
                          ></i>
                        </label>
                      </div>
                    </div>
                    <div className="col-lg-6 position-relative align-items-center">
                      <span className='position-absolute ms-4 fs-5 d-flex align-items-center' style={{top:'25.5%'}}><img src={toAbsoluteUrl('/media/svg/flags/india.svg')} height='16' className="contactImg-width me-2"/>+91 
                      </span>
                      <input
                        placeholder='Mobile Number'
                        className='form-control ps-20 form-control-lg fw-normal'
                        disabled={contactedit}
                        type='text'
                        name='phoneNo'
                        value={formData.contactNumber || ''}
                        autoComplete='off'
                        onKeyPress={(e) => {
                          const key = e.key;
                          const currentValue = e.target.value;
                          const regex = REGEX.NUMERIC_DIGITS
                          const updatedValue = currentValue + key;
                          if (!regex.test(updatedValue) || updatedValue.length > 10) {
                            e.preventDefault();
                          }
                        }}
                      />
                      </div>
                    {/* <PhoneInput
                      country={'in'}
                      enableAreaCodes={true}
                      value={formData.contactNumber || ''}
                      onChange={phone => handleBusinessChange(phone)}
                      disabled={contactedit}
                    /> */}
                    {errors && errors.contactNumber && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.contactNumber}
                      </div>
                    )}
                    {/* <div className="row mb-4 mt-8">
                      <div className="col-lg-6">
                        <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                          <span className="required">
                            Alternate MobileNumer
                          </span>
                          <i
                            className="fas fa-exclamation-circle ms-2 fs-7"
                            data-bs-toggle="tooltip"
                            title="Contact Number"
                          ></i>
                        </label>
                      </div>
                    </div>
                    <PhoneInput
                      country={'in'}
                      enableAreaCodes={true}
                      value={formData.alterNativeNumbar || ''}
                      onChange={phone => handleAlterNativesChange(phone)}
                    />
                    {errors && errors.alterNativeNumbar && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.alterNativeNumbar}
                      </div>
                    )} */}
                  </>
                ) : (
                  <>
                    <div className="row">
                      <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                        <span className="required">
                          {`Enter the OTP sent to +`}
                          <text className="text-danger">
                            {formData.contactNumber}
                          </text>
                        </span>
                        <i
                          className="fas fa-exclamation-circle ms-2 fs-7"
                          data-bs-toggle="tooltip"
                          title="Otp"
                        ></i>
                      </label>
                      <div className="col-lg-6">
                        <input
                          type="text"
                          className="form-control form-control-lg form-control-solid"
                          name="code"
                          placeholder="OTP"
                          onChange={(e) => otpChange(e)}
                          value={otpData.code || ""}
                          maxLength={5}
                          onKeyPress={(e) => {
                            if (!/^[0-9 .]+$/.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                        {error && error.code && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.code}
                          </div>
                        )}
                      </div>
                      <div className="col-lg-6">
                        <button
                          type="submit"
                          className="btn btn-sm btn-light-primary"
                          onClick={() => {
                            phoneVerify()
                          }}
                          disabled={phoneVerifyloading}
                        >
                          {!phoneVerifyloading && (
                            <span className="indicator-label">
                              <i className="bi bi-person-fill" />
                              Resend OTP
                            </span>
                          )}
                          {phoneVerifyloading && (
                            <span
                              className="indicator-progress"
                              style={{ display: "block" }}
                            >
                              Please wait...
                              <span className="spinner-border spinner-border-sm align-middle ms-2" />
                            </span>
                          )}
                        </button>
                      </div>
                    </div>
                  </>
                )}
            </div>
            <div className="fv-row mb-4">
              <div className="d-flex pt-10 justify-content-end">
                <div>
                  {!showOtp ? (
                    <button
                      type="submit"
                      className="btn btn-sm btn-light-primary"
                      onClick={() => {
                        phoneVerify()
                      }}
                      disabled={phoneVerifyloading}
                    >
                      {clientEdit ? <span className='indicator-label'>
                        Proceed
                      </span> : updateedit ? <span className='indicator-label'>
                        Proceed
                      </span> :<>
                      {!phoneVerifyloading && (
                        <span className="indicator-label">
                          <i className="bi bi-person-fill" />
                          Send OTP
                        </span>
                      )}
                      {phoneVerifyloading && (
                        <span
                          className="indicator-progress"
                          style={{ display: "block" }}
                        >
                          Please wait...
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        </span>
                      )}</>}
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      onClick={(event) => {
                        otpSubmit(event)
                      }}
                    >
                      {!phoneOtpVerifyLoading && (
                        <span className="indicator-label">
                          <i className="bi bi-person-check-fill" />
                          Verify
                        </span>
                      )}
                      {phoneOtpVerifyLoading && (
                        <span
                          className="indicator-progress"
                          style={{ display: "block" }}
                        >
                          Please wait...
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        </span>
                      )}
                    </button>
                  )}
                  {/* <button type='submit' className='btn btn-sm btn-light-primary ms-2'
                    onClick={() => { proccedStore() }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Proceed
                    </span>
                  </button> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  getCountrys:
    state && state.CountrylistStore && state.CountrylistStore.Countrylists,
  KycPhoneVerifyResponse:
    state && state.KYCphoneStore && state.KYCphoneStore.KycPhoneVerify,
  phoneVerifyloading:
    state && state.KYCphoneStore && state.KYCphoneStore.loading,
  KycPhoneOtp:
    state && state.KYCphoneOtpStore && state.KYCphoneOtpStore.KycPhoneOtp,
  phoneOtpVerifyLoading:
    state && state.KYCphoneOtpStore && state.KYCphoneOtpStore.loading,
  KYCAddResponse:
    state && state.KYCAddStore && state.KYCAddStore.KYCAddResponse,
  clinetIdLists: state && state.clinetListStore && state.clinetListStore.clinetIdLists ? state.clinetListStore.clinetIdLists : '',
  FullKycResData: state && state.FullKycValueStore && state.FullKycValueStore.FullKycValue,
})

const mapDispatchToProps = (dispatch) => ({
  KycPhoneVerifyDispatch: (data) =>
    dispatch(KYCphoneVerify.KYCphoneVerifyList(data)),
  ClearPhoneVerifyDispatch: () =>
    dispatch(KYCphoneVerify.clearKYCphoneVerify()),
  KycPhoneOtpDispatch: (data) => dispatch(KYCphoneOtpAction.KYCphoneOTP(data)),
  clearKYCphoneOTP: (data) =>
    dispatch(KYCphoneOtpAction.clearKYCphoneOTP(data)),
  KYCAddDispatch: (data) => dispatch(KYCAddAction.KYCAdd(data)),
  ClearKYCDispatch: (data) => dispatch(KYCAddAction.clearKYC(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactInfo)
