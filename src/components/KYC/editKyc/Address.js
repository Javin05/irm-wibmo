
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businesDetailValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { DROPZONE_IMAGE_NAME_TYPES, REGEX, STATUS_RESPONSE, DROPZONE_MESSAGES, AADHAAR_BACK, AADHAAR_FRONT, IDPROFF, FRONT_TYPE_VERIFY } from '../../../utils/constants'
import _, { values } from 'lodash'
import {
  CityActions,
  KYCAdharNumberAction,
  AadhaarFrontAction,
  AadhaarBackAction,
  StateActions,
  PinCodeValueAction,
  FullKycValueAction
} from '../../../store/actions'
import { warningAlert } from "../../../utils/alerts"
import {
  FILE_FORMAT_TYPE,
  FILE_FORMAT_TYPE_DOCUMEN_IMAGE
} from "../../../constants/index"
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import { setAddressData } from './formData'

function Address(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
    clientDetails,
    KycAadharDispatch,
    AadharVerifyRes,
    Aadharloading,
    clearKYCAdhaarDispatch,
    setFullKycDetails,
    AadhaarFrontDispatch,
    AadhaarBackDispatch,
    AadharFrontValue,
    AadharBackValue,
    AadharFrontLoading,
    AadharBackLoading,
    ClearAadhaarBackDispatch,
    ClearAadhaarFrontDispatch,
    UserDetails,
    pincodeDataRes,
    getPincodeDispatch,
    FullKycResData,
    fullKycDispatch,
    FullKycLoading,
    clearFullKycValueDispatch,
    getKYCUserDetailsDispatch,
    userId
  } = props

  const hiddenFileInput = useRef(null)
  const hiddenFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [showAadhar, setShowAadhar] = useState(0)
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [Data, setData] = useState(true)
  const [pincode, setPincode] = useState('')
  const [formData, setFormData] = useState({
    Address: '',
    pinCode: '',
    state: '',
    city: ''
  })
  const [update, setUpdate] = useState(false)

  const handleChange = (e) => {
    e.persist()
    setUpdate(true)
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  setLocalStorage('AADHARTYPE', JSON.stringify(formData.identityProof ? formData.identityProof : 'null'))
  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const handleSubmit = () => {
    const errors = {}
    if (organization === 'Partnership') {
      if (_.isEmpty(formData.Address)) {
        errors.Address = 'Address Number Is Required'
      }
      if (_.isEmpty(formData.pinCode)) {
        errors.pinCode = 'pinCode Is Required'
      }
      if (_.isEmpty(formData.businessIpAddress)) {
        errors.businessIpAddress = 'Business Ip Address Is Required.'
      }
      if (_.isEmpty(formData.aadharNumber)) {
        errors.aadharNumber = 'Aadhar Number Is Required'
      }
      if (_.isEmpty(formData.identityProof)) {
        errors.identityProof = 'PLease Select Idetity Proof'
      }
      if (_.isEmpty(formData.AadhaarFront)) {
        errors.AadhaarFront = `${AADHAAR_FRONT[formData.identityProof]} Is Required`
      }
      if (_.isEmpty(formData.AadhaarBack)) {
        errors.AadhaarBack = `${AADHAAR_BACK[formData.identityProof]} Is Required`
      }
      if (_.isEmpty(errors)) {
        setLocalStorage('ADDRESSVERIFY', JSON.stringify(formData))
        setFullKycDetails((values) => ({ ...values, AddressData: formData }))
        setData(formData)
        const params = {
          aadharNumber: formData.aadharNumber
        }
        KycAadharDispatch(params)
      }
    } else {
      if (_.isEmpty(formData.Address)) {
        errors.Address = 'Address Number Is Required'
      }
      if (_.isEmpty(formData.pinCode)) {
        errors.pinCode = 'pinCode Is Required'
      }
      if (_.isEmpty(errors)) {
        fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id , formData)
        // setLocalStorage('ADDRESSVERIFY', JSON.stringify(formData))
        // setFullKycDetails((values) => ({ ...values, AddressData: formData }))
        // setData(formData)
      }
    }
    setErrors(errors)
  }

  const addressVerify = JSON.parse(getLocalStorage('ADDRESSVERIFY'))
  useEffect(() => {
    setFormData({
      Address: addressVerify.Address,
      pinCode: addressVerify.pinCode,
      state: addressVerify.state,
      city: addressVerify.city,
      aadharNumber: addressVerify.aadharNumber,
      businessIpAddress: addressVerify.businessIpAddress
    })
  }, [Data])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.state)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.state._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.city)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.city._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  const handlePincode = (e) => {
    setPincode(e.target.value.trim())
    setFormData({
      ...formData,
      pinCode: e.target.value.trim()
    })
    setUpdate(true)
  }
  useEffect(() => {
    if (pincode && pincode.length === 6) {
      const payload = {
        pincode: pincode
      }
      getPincodeDispatch(payload)
    } else if (pincode.length > 6) {
      const errors = {}
      errors.pincode = 'Pincode is InValid'
    }
  }, [pincode])

  useEffect(() => {
    if (UserDetails) {
      const data = setAddressData(UserDetails && UserDetails.data)
      setPincode(UserDetails && UserDetails.data && UserDetails.data.businessAddress && UserDetails.data.businessAddress.pincode ? UserDetails.data.businessAddress.pincode :'')
      if (UserDetails && UserDetails.data &&  UserDetails.data._id) {
        const state = getDefaultOptions(getStates)
        if (!_.isEmpty(data && data.state)) {
          const selOption = _.filter(state, function (x) {
            if (_.includes(data && data.state, x.label)) {
              setStateOption(x.label)
              return x
            }
          })
          setSelectedStateOption(selOption)
        }
        
        const city = getDefaultOptions(getCitys)
        if (!_.isEmpty(data && data.city)) {
          const selOption = _.filter(city, function (x) {
            if (_.includes(data && data.city, x.label)) {
              setCityOptions(x.label)
              return x
            }
          })
          setSelectedCityOption(selOption)
        }
      }
      setFormData(data)
    }
  }, [UserDetails])

  useEffect(() => {
    if (pincodeDataRes && pincodeDataRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setFormData({
        ...formData,
        state: pincodeDataRes && pincodeDataRes.data && pincodeDataRes.data.state ? pincodeDataRes.data.state : '',
        city: pincodeDataRes && pincodeDataRes.data && pincodeDataRes.data.city ? pincodeDataRes.data.city : '',
      })
    } else if (pincodeDataRes && pincodeDataRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        pincodeDataRes && pincodeDataRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [pincodeDataRes])


  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(6)
      clearFullKycValueDispatch()
      setPincode('')
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [FullKycResData])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG && update) {
      getKYCUserDetailsDispatch(userId)
    }
  },[update,FullKycResData])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
          <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Add your registered business address</span>
                  </label>
                </div>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>We'll verify these details using your given business documents.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Add your pinCode and Address'
                    />
                  </label>
                </div>
              </div>
              <div className='row mb-4 col-lg-12 mt-4 mb-4'>
                <div>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text fs-4 fw-bolder required'>PinCode</span>
                  </label>
                </div>
                <div className='col-lg-6'>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mt-4'
                    name='pinCode'
                    placeholder='pincode'
                    onChange={handlePincode}
                    value={pincode || ''}
                    maxLength={6}
                    onKeyPress={(e) => {
                      if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                        e.preventDefault()
                      }
                    }}
                  />
                  {errors && errors.pinCode && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red}'}</style>
                      {errors.pinCode}
                    </div>
                  )}
                </div>
              </div>
              <div className='row mb-4 col-lg-12 mt-4 mb-4'>
                <div className='col-lg-6'>
                  <div>
                    <label className='d-flex align-items-center mb-2'>
                      <span className='text-muted fs-4 fw-bolder required'>State</span>
                    </label>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-4'
                    name='state'
                    placeholder='State'
                    value={formData.state || ''}
                    disabled
                  />
                  {errors && errors.state && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.state}
                    </div>
                  )}
                </div>
                <div className='col-lg-6'>
                  <div>
                    <label className='d-flex align-items-center mb-2'>
                      <span className='text-muted fs-4 fw-bolder required'>City</span>
                    </label>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-4'
                    name='city'
                    placeholder='City'
                    value={formData.city || ''}
                    disabled
                  />
                  {errors && errors.city && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.city}
                    </div>
                  )}
                </div>
              </div>
              <div>
                <label className='d-flex align-items-center mb-2'>
                  <span className='text fs-4 fw-bolder required'>Address</span>
                </label>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='Address'
                placeholder='Address'
                onChange={(e) => handleChange(e)}
                value={formData.Address || ''}
              />
              {errors && errors.Address && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red}'}</style>
                  {errors.Address}
                </div>
              )}
            </div>
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2' />
                <div>
                <button type='submit' className='btn btn-sm btn-primary me-3'
                    onClick={() => {
                      handleSubmit()
                    }}
                    disabled={FullKycLoading}
                  >
                    {!FullKycLoading &&
                      <span className='indicator-label'>
                        <i className='bi bi-person-fill' />
                        Proceed
                      </span>
                    }
                    {FullKycLoading && (
                      <span className='indicator-progress' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, AdharNumberStore, AAdhaarUploadStore, AAdhaarBackUploadStore } = state

  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    AadharVerifyRes: AdharNumberStore && AdharNumberStore.AadharVerify ? AdharNumberStore.AadharVerify : {},
    Aadharloading: AdharNumberStore && AdharNumberStore.loading ? AdharNumberStore.loading : false,
    AadharFrontValue: AAdhaarUploadStore && AAdhaarUploadStore.AadharFrontValue ? AAdhaarUploadStore.AadharFrontValue : {},
    AadharFrontLoading: AAdhaarUploadStore && AAdhaarUploadStore.loading ? AAdhaarUploadStore.loading : false,
    AadharBackValue: AAdhaarBackUploadStore && AAdhaarBackUploadStore.AadharBackValue ? AAdhaarBackUploadStore.AadharBackValue : {},
    AadharBackLoading: AAdhaarBackUploadStore && AAdhaarBackUploadStore.loading ? AAdhaarBackUploadStore.loading : false,
    pincodeDataRes: state && state.PinCodeValueTypeStore && state.PinCodeValueTypeStore.pincodeData,  FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
    FullKycResData: state && state.FullKycValueStore && state.FullKycValueStore.FullKycValue,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycAadharDispatch: (params) => dispatch(KYCAdharNumberAction.KYCAdhaarNumber(params)),
  clearKYCAdhaarDispatch: (params) => dispatch(KYCAdharNumberAction.clearKYCAdhaarNumber(params)),
  AadhaarFrontDispatch: (params) => dispatch(AadhaarFrontAction.AadhaarFrontValue(params)),
  AadhaarBackDispatch: (params) => dispatch(AadhaarBackAction.AadhaarBackValue(params)),
  ClearAadhaarBackDispatch: (params) => dispatch(AadhaarBackAction.clearAadhaarBackValue(params)),
  ClearAadhaarFrontDispatch: (params) => dispatch(AadhaarFrontAction.clearAadhaarFrontValue(params)),
  getPincodeDispatch: (payload) => dispatch(PinCodeValueAction.PinCodeValue(payload)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Address)