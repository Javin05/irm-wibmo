import _ from "lodash";
import moment from "moment";

export const setPhoneData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      contactNumber: data && data.phoneNumber && data.phoneNumber.number,
      // contactNumber: `${data && data.phoneNumber && data.phoneNumber.number}`,
      clientId: data && data.clientId && data.clientId._id
    }
  }
}

export const setEmailData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      contactEmail: data && data.emailId && data.emailId.emailId,
      alternateEmail: data && data.alternateEmailId
    }
  }
}

export const setBusinessData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      brandName: data && data.brandName,
      businessStartDate: data && moment(data.businessStartDate).toDate(),
      annualTurnOverRange:  data && data.annualTurnOverRange,
      actualAnnualTurnOver: data && data.actualAnnualTurnOver,
      actualAnnualTurnOverRange: data && data.actualAnnualTurnOverRange,
      exceptedMonthlyCardturnOver: data && data.exceptedMonthlyCardturnOver,
      exceptedMonthlyCardturnOverRange: data && data.exceptedMonthlyCardturnOverRange,
      exceptedMonthlyTransaction: data && data.exceptedMonthlyTransaction,
      exceptedMonthlyTransactionRange: data && data.exceptedMonthlyTransactionRange,
      website: data && data.website,
      businessName: data && data.businessName,
      primaryContactName: data && data.primaryContactName,
      entityName: data && data.entityName,
      businessDescription: data && data.businessDescription,
      entityId: data && data.entityId,
      businessSubCategory: data && data.businessSubCategory,
      businessCategory: data && data.businessCategory,
      category_id: data && data.category_id,
      subcategory_id: data && data.subcategory_id,
    }
  }
}

export const setAccountData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      bankAccountNumber: data && data.bankAccountNumber,
      ifscCode: data && data.ifscCode,
      accountHolderName: data && data.accountHolderName,
      cancelledCheque: data && data.cancelledCheque
    }
  }
}

export const setAddressData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      Address: data && data.businessAddress && data.businessAddress.address,
      pinCode: data && data.businessAddress && data.businessAddress.pincode,
      state: data && data.businessAddress && data.businessAddress.state,
      city: data && data.businessAddress && data.businessAddress.city
    }
  }
}