import React, { useEffect } from "react"

function FullKycCompletedPage(props) {
  const {
    setActiveStep,
    setCompletedSteps,
    setClientDetails
  } = props
  
  return (
    <>
      <div>
        <div class="card card-flush"
          style={{
            backgroundColor: '#1a1d31'
          }}
        >
          <div class="card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"
          >
            <div className="d-flex justify-content-center">
              <i class="bi bi-check-circle-fill"
                style={{ color: '#208f20', backgroundColor: 'transparent', fontSize: '2.75rem' }}
              />
            </div>
            <div class="fs-1hx fw-bold text-white text-center mb-10 mt-5">
              Your Details Submitted Successfully
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-4">
        <button class="btn btn-light-dark"
          onClick={() => { 
            setCompletedSteps([-1])
            setActiveStep(0)
            setClientDetails(null)
          }}
          >Add Another KYC</button>
        </div>
      </div>
    </>
  )
}

export default FullKycCompletedPage