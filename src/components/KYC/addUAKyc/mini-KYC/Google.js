import React, { useState, useEffect } from "react"
import { Beforeunload } from 'react-beforeunload'
import KYCminiAdd from './Add'

function GoogleTab() {
  return (
    <>
      <Beforeunload 
      onBeforeunload={(event) => {
      event.preventDefault()
     return 'Hello world'
      }} 
      // useBeforeunload={() => 'You’ll lose your data!'}
      // useBeforeunload(() => 'You’ll lose your data!')
      >
        <KYCminiAdd />
      </Beforeunload>
    </>
  )

}
export default GoogleTab