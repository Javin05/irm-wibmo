
import React, { useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../../theme/helpers'
import { connect } from 'react-redux'
import { businessValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../../utils/helper'
import { KYC_FORM, REGEX } from '../../../../utils/constants'
import _ from 'lodash'
import { EntityAction } from "../../../../store/actions"

function BusinessForm(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    loading,
    goBack,
    setCompletedSteps,
    setActiveStep,
    getEntityDispatch,
    getEntityRes
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [showWebsite, setShowWebsite] = useState(false)
  const [websiteCheked, setWebsiteCheked] = useState()
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [Data, setData] = useState(true)
  const [tabDefault, setTabdefault] = useState('');
  const [formData, setFormData] = useState({
    businessCategory: '',
    website: '',
    entity_id: '',
    businessIpAddress: '',
    businessName: '',
    primaryContactName: '',
    entiryName: '',
    businessDescription:''
  })

  useEffect(() => {
    getEntityDispatch()
  }, [])

  const handleChange = (e) => {
    e.persist()
    const { value, name, checked } = e.target
    setWebsiteCheked(checked)
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = (e) => {
    e.preventDefault()
    const errors = {}
    if (_.isEmpty(formData.businessName)) {
      errors.businessName = 'Please Enter Business Name.'
    } if (_.isEmpty(formData.primaryContactName)) {
      errors.primaryContactName = 'Please Enter Primary Contact Name.'
    }
    if (_.isEmpty(formData.entity_id)) {
      errors.entity_id = 'Please Select entity Or Not Registered.'
    }
    if (_.isEmpty(formData.businessCategory)) {
      errors.businessCategory = 'Please Select One Payment.'
    }
    if (_.isEmpty(formData.website)) {
      errors.website = 'Website is required.'
    } else if (formData.website && !REGEX.WEBSITE_URLS.test(formData.website)) {
      errors.website = 'Website is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        entityId: formData.entity_id,
        entiryName: formData.entiryName,
        businessName: formData.businessName,
        primaryContactName: formData.primaryContactName,
        businessCategory: formData.businessCategory,
        website: formData.website,
        businessDescription:formData.businessDescription
      }
      onClickNext(3)
      setData(formData)
      setLocalStorage(KYC_FORM.BUSINESS, JSON.stringify(data))
      setClientDetails((values) => ({ ...values, business: data }))
    }
    setErrors(errors)
  }

  const ToggleTab = (tab, name) => {
    setFormData((values) => ({ ...values, entiryName: name }))
    setFormData((values) => ({ ...values, entity_id: tab }))
    setTabdefault(tab)
  }

  return (
    <>
      <div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Business name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='businessName'
            placeholder='Business name'
            onChange={(e) => handleChange(e)}
            value={formData.businessName || ''}
            maxLength={50}
          />
          {errors && errors.businessName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.businessName}
            </div>
          )}
        </div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Primary Contact Name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='primaryContactName'
            placeholder='Primary Contact Name'
            onChange={(e) => handleChange(e)}
            value={formData.primaryContactName || ''}
            maxLength={50}
          />
          {errors && errors.primaryContactName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.primaryContactName}
            </div>
          )}
        </div>

        <div className='row mt-6'>
          <h3 className='mb-8 d-flex justify-content-center'>Pick only one that suits to your business </h3>
          <div className='col-sm-12 col-md-12 col-lg-12 mb-4' >
            {
              getEntityRes && getEntityRes.data && getEntityRes.data.map((item, i) => {
                return (
                  <a
                    onClick={() =>
                      ToggleTab(item && item._id, item && item.kyc_entity)
                    }
                    className={tabDefault === `${item && item._id}` ? "active btn btn-primary me-2 btn-sm mt-2" : "btn btn-outline btn-outline-primary me-2 btn-sm mt-2"}>
                    {
                      item && item.kyc_entity
                    }
                  </a>
                )
              })
            }
          </div>
          {errors && errors.entity_id && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.entity_id}
            </div>
          )}
        </div>
        <div className='current mt-4' data-kt-stepper-element='content'>
          <div className='w-100'>
            <h3 className='mb-8 d-flex justify-content-center'>Business Category</h3>
            <div className="form-check form-check-custom form-check-solid form-check-sm mb-2">
              <label>
                <input
                className="form-check-input"
                type="radio"
                value="Ecommerce/Online Store/MarketPlace"
                id="flexRadioLg"
                name='businessCategory'
                onChange={(e) => handleChange(e)}
                />
                <span className='form-check-label text-muted'>
                Ecommerce/Online Store/MarketPlace
                </span>
              </label>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm mb-2">
              <label>
                <input
                className="form-check-input"
                type="radio"
                id="flexRadioLg"
                name='businessCategory'
                value='Restaurant/QSR'
                onChange={(e) => handleChange(e)}
                />
                <span className='form-check-label text-muted'>
                Restaurant/QSR
                </span>
              </label>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm mb-2">
              <label>
                <input
                className="form-check-input"
                type="radio"
                value="Financial service provider"
                id="flexRadioLg"
                name='businessCategory'
                onChange={(e) => handleChange(e)}
                />
                <span className='form-check-label text-muted'>
                Financial service provider
                </span>
              </label>
            </div>

            {errors && errors.businessCategory && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.businessCategory}
              </div>
            )}
            <div className='fv-row mb-10 mt-8'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Add your website link</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Website'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='website'
                placeholder='Website'
                onChange={(e) => handleChange(e)}
                value={formData.website || ''}
              />
              {errors && errors.website && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.website}
                </div>
              )}
            </div>
            <div className='fv-row mb-10 mt-8'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'> Business Description</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Business Description'
                />
              </label>
              <textarea
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='businessDescription'
                placeholder='Business Description'
                onChange={(e) => handleChange(e)}
                value={formData.businessDescription || ''}
                maxLength={200}
              />
              {errors && errors.businessDescription && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.businessDescription}
                </div>
              )}
            </div>
          </div>

        </div>
        <div className='fv-row mt-4 mb-6'>
          <div className='d-flex justify-content-end'>
            <button type='submit' className='btn btn-sm btn-primary me-3'
              onClick={(e) => {
                handleSubmit(e)
              }}
            >
              <span className='indicator-label'
              >
                <KTSVG
                  path='/media/icons/duotune/arrows/arr064.svg'
                  className='svg-icon-3 ms-2 me-0'
                />
              </span>
              Proceed
            </button>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists,
  getEntityRes: state && state.EntityStore && state.EntityStore.EntityRes

})

const mapDispatchToProps = (dispatch) => ({
  getEntityDispatch: () => dispatch(EntityAction.Entity()),
})

export default connect(mapStateToProps, mapDispatchToProps)(BusinessForm)