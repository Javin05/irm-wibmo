import React, { useState, useEffect } from "react"
import { Modal } from "../../../../theme/layout/components/modal/Modal"
import { connect } from "react-redux"
import { toAbsoluteUrl, KTSVG } from '../../../../theme/helpers'
import { VerticalStepper } from "./verticalStepper"
import {
  setLocalStorage,
  getLocalStorage,
  removeLocalStorage,
} from "../../../../utils/helper"
import { KYC_FORM } from "../../../../utils/constants"
import ShowFields from "../../ShowFields"
import { useLocation } from "react-router-dom"
import ContactInfo from "./ContactInfo"
import BusinessForm from "./Business"
import ComptedPage from "./completed"
import EmailInfo from "./Email"
import { StateActions, KYCAddAction } from "../../../../store/actions"
import AccountVerification from "../full-KYC/AccountVerify"
import Address from "../full-KYC/Address"
import FullKyc from "../full-KYC/completeKyc"
import FullKycCompletedPage from "../full-KYC/completed"
import IdentityVerification from '../full-KYC/IdentifyVerificaion'

function KYCminiAdd(props) {
  const { show, loading, getStateDispatch, clearPhoneDispatch, activeKyc, setActiveKyc } =
    props
  const [activeStep, setActiveStep] = useState(0)
  const [completedSteps, setCompletedSteps] = useState([-1])
  const [array, setArray] = useState([])
  const [clientDetails, setClientDetails] = useState({})
  const [fullKycDetails, setFullKycDetails] = useState({})
  const [summary, setSummary] = useState({})
  const [editMode, setEditMode] = useState(false)
  const active = getLocalStorage(KYC_FORM.ACTIVE_STEP)
  const [kycShow, setKycShow] = useState(false)
  const organName = clientDetails && clientDetails.business && clientDetails.business.entiryName ? clientDetails.business.entiryName : ''
  const [value, setValue] = useState(true)

  useEffect(() => {
    const params = {
      skipPagination: "true",
    }
    getStateDispatch(params)
    setValue(true)
  }, [])

  const stepperArray = [
    {
      stepperLabel: "Mobile Verification",
      Optional: "",
    },
    {
      stepperLabel: "Email",
      Optional: "",
    },
    {
      stepperLabel: "Business Information ",
      Optional: "",
    },
    {
      stepperLabel: "Bank Account Details",
      Optional: "",
    },
    {
      stepperLabel: "Document Section",
      Optional: "",
    },
    {
      stepperLabel: "Business Address ",
      Optional: "",
    },
    {
      stepperLabel: "Complete",
      Optional: "",
    }
  ]

  const kycStepperArray = [
    {
      stepperLabel: "Bank Account Details",
      Optional: "",
    },
    {
      stepperLabel: "Document Section",
      Optional: "",
    },
    {
      stepperLabel: "Business Address ",
      Optional: "",
    },
    {
      stepperLabel: "Complete",
      Optional: "",
    },
  ]

  const onClickNext = (currentId) => {
    const id = currentId - 1
    setActiveStep(currentId)
    if (completedSteps.length === currentId) {
      if (completedSteps && !completedSteps.includes(id)) {
        setCompletedSteps((values) => [...values, id])
        setLocalStorage(KYC_FORM.ACTIVE_STEP, currentId)
      }
    }
  }

  const goBack = (id) => {
    setActiveStep(id)
  }

  const pathName = useLocation().pathname
  const url = pathName && pathName.split("update")
  const id = url && url[1]
  const urlName = pathName && pathName.split("update/")
  const clientId = urlName && urlName[1]

  const getDescendants = (arr, step) => {
    const val = []
    arr.forEach((item, i) => {
      if (step > i) {
        val.push(i)
      }
    })
    return val
  }
  const d = getDescendants(array, active)
  const editstepper = getDescendants(array, array.length)

  useEffect(() => {
    if (kycShow) {
      setArray(kycStepperArray)
      setActiveStep(0)
      setCompletedSteps([-1])
    } else {
      setArray(stepperArray)
      setCompletedSteps([-1])
      setActiveStep(0)
    }
  }, [kycShow])

  useEffect(() => {
    return () => {
      setKycShow(false)
      removeLocalStorage("ADDRESSVERIFY")
      removeLocalStorage("CONTACT_DETAILS")
      removeLocalStorage("BUSINESS")
      removeLocalStorage("BUSINESS_DETAILS")
      removeLocalStorage("BANK_ACCOUNT")
      removeLocalStorage("VALIDNAME")
      removeLocalStorage("BUSINESSTYPE")
      removeLocalStorage("ADDRESSVERIFY")
      removeLocalStorage("AccountVerify")
      removeLocalStorage("CINandGSTIN")
    }
  }, [])

  useEffect(() => {
    if (active) {
      setActiveStep(Number(active))
      setCompletedSteps([-1, ...d])
    } else {
      setActiveStep(0)
      setCompletedSteps([-1])
    }
  }, [active])

  useEffect(() => {
    onClickNext(0)
  }, [])

  useEffect(() => {
    setLocalStorage(KYC_FORM.ORGANIZATIONS, JSON.stringify(organName))
    if (activeKyc && activeKyc.kycData && activeKyc.kycData.kycName === 'CONTINUE WITH FULL KYC') {
      setKycShow(true)
    } else {
      setKycShow(false)
    }
  }, [activeKyc])


  useEffect(() => {
    window.addEventListener('beforeunload', onbeforeunload);
    function onbeforeunload(e) {
      console.log('>>>> onbeforeunload called');
      e.returnValue = "false"
      window.open('logout.php');
    };
  }, [value])

  return (

    <>
      <div
        className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed overflow-hidden'
        style={{
          backgroundImage: `url(${toAbsoluteUrl(
            '/media/illustrations/sketchy-1/14.png'
          )})`,
          // backgroundColor: colors.oxfordBlue
        }}
      >
        <div className='row mb-4 mt-20'>
          <div className='card card-xl-stretch'>
            <div className='row mt-10 mb-8'>
              <div className='col-lg-12'>
                <h1 className='text-dark mb-3 d-flex justify-content-center'>{!kycShow ? "Merchant Details" : "Business Information"}</h1>
              </div>
            </div>
            <div className='card-body pt-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <form className='container-fixed'>
                    <div className='card-header'>
                      <div className='card-body'>
                        <div className='form-group row mb-4'>
                          <div className="row">
                            <div className="col-lg-2 col-md-2 col-sm-2" />
                            <div className="col-lg-8 col-md-8 col-sm-8">
                              <div
                                className="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid"
                                id="kt_modal_create_app_stepper"
                              >
                                <div className="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                                  <VerticalStepper
                                    activeStep={activeStep}
                                    setActiveStep={setActiveStep}
                                    completedSteps={completedSteps}
                                    stepperArr={array}
                                    setCompletedSteps={setCompletedSteps}
                                    onStepperClick={onClickNext}
                                    kycShow={kycShow}
                                  />
                                </div>
                                <div
                                  className="flex-row-fluid py-lg-5 px-lg-15 -bs-gray-100 mt-10"
                                  style={{
                                    backgroundoutline: "#f1faff",
                                  }}
                                >
                                  <ShowFields>
                                    {/* <div className="scroll h-400px px-5"> */}
                                    <>
                                      {activeStep === 0 ? (
                                        <ContactInfo
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                        />
                                      ) : null}
                                      {activeStep === 1 ? (
                                        <EmailInfo
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          kycAllDataSaved={clientDetails}
                                        />
                                      ) : null}

                                      {activeStep === 2 ? (
                                        <BusinessForm
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setCompletedSteps={setCompletedSteps}
                                          setActiveStep={setActiveStep}
                                        />
                                      ) : null}
                                      {activeStep === 3 ? (
                                        <ComptedPage
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setKycShow={setKycShow}
                                          kycAllDataSaved={clientDetails}
                                          setCompletedSteps={setCompletedSteps}
                                          setActiveStep={setActiveStep}
                                        />
                                      ) : null}
                                      {activeStep === 4 ? (
                                        <AccountVerification
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          setFullKycDetails={setFullKycDetails}
                                          clientDetails={clientDetails}
                                        />
                                      ) : null}
                                      {activeStep === 5 ? (
                                        <IdentityVerification
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setFullKycDetails={setFullKycDetails}
                                          organName={organName}
                                        />
                                      ) : null}
                                      {activeStep === 6 ? (
                                        <Address
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setFullKycDetails={setFullKycDetails}
                                        />
                                      ) : null}
                                      {activeStep === 7 ? (
                                        <FullKyc
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setKycShow={setKycShow}
                                          kycAllDataSaved={clientDetails}
                                          fullKycDetails={fullKycDetails}
                                          activeKyc={activeKyc}
                                        />
                                      ) : null}
                                      {activeStep === 8 ? (
                                        <FullKycCompletedPage
                                          onClickNext={onClickNext}
                                          goBack={goBack}
                                          setClientDetails={setClientDetails}
                                          setSummary={setSummary}
                                          clientDetails={clientDetails}
                                          setKycShow={setKycShow}
                                          kycAllDataSaved={clientDetails}
                                          fullKycDetails={fullKycDetails}
                                          setCompletedSteps={setCompletedSteps}
                                          setActiveStep={setActiveStep}
                                        />
                                      ) : null}
                                    </>
                                    {/* </div> */}
                                  </ShowFields>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-2 col-md-2 col-sm-2" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* end::Wrapper */}
        {/* end::Content */}
      </div>

    </>
  )
}
const mapStateToProps = (state) => {
  const {
    usertypeStore,
    addUsertypeStore,
    editUsertypeStore,
  } = state
  return {
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData ? usertypeStore.userTypeData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  clearPhoneDispatch: () => dispatch(KYCAddAction.clearKYC()),
})

export default connect(mapStateToProps, mapDispatchToProps)(KYCminiAdd)
