import React, { FC, useEffect, useRef, useState } from "react"
import { connect } from "react-redux"
import { contactInfoValidation, phoneOtp } from "./Validation"
import { setLocalStorage, getLocalStorage } from "../../../../utils/helper"
import { KYC_FORM, REGEX, STATUS_RESPONSE } from "../../../../utils/constants"
import _, { values } from "lodash"
import {
  KYCphoneVerify,
  KYCphoneOtpAction,
  KYCAddAction,
  KYCAddALLAction,
  clientIdLIstActions
} from "../../../../store/actions"
import Toastify from "toastify-js"
import "toastify-js/src/toastify.css"
import { warningAlert, confirmationAlert } from "../../../../utils/alerts"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import ReactSelect from "../../../../theme/layout/components/ReactSelect"
import color from "../../../../utils/colors"

function ContactInfo(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    KycPhoneVerifyDispatch,
    KycPhoneOtpDispatch,
    KycPhoneVerifyResponse,
    KycPhoneOtp,
    phoneVerifyloading,
    ClearPhoneVerifyDispatch,
    phoneOtpVerifyLoading,
    KYCAddDispatch,
    KYCAddResponse,
    ClearKYCDispatch,
    clearKYCphoneOTP,
    clientIdDispatch,
    clinetIdLists
  } = props

  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [errors, setErrors] = useState({})
  const [error, setError] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [Data, setData] = useState(true)
  const [showOtp, setShowOtp] = useState(false)
  const [showSuccess, setShowSuccess] = useState(false)
  const [showValidateOtp, setShowValidateOtp] = useState(false)
  const [countryNumber, setCountryNumber] = useState()
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [formData, setFormData] = useState({
    contactNumber: "",
    clientId: "",
    alterNativeNumbar: ""
  })
  const [otpData, setOtpData] = useState({
    code: "",
  })

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
  }, [])

  const otpChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setOtpData((values) => ({ ...values, [name]: value }))
    setError({ ...error, [name]: "" })
  }

  const otpSubmit = (e) => {
    e.preventDefault()
    const error = {}
    if (_.isEmpty(otpData.code)) {
      error.code = "Phone Otp is required"
    }
    setError(error)
    if (_.isEmpty(error)) {
      const data = {
        otp: otpData.code,
        phoneNumber: formData.contactNumber.slice(2,),
        countryCode: formData.contactNumber.slice(0, 2),
        clientId: formData.clientId
      }
      KycPhoneOtpDispatch(data)
      setLocalStorage(KYC_FORM.OTP, JSON.stringify(data))
    }
  }

  const phoneVerify = (e) => {
    e.preventDefault()
    const errors = {}
    if (Role === 'Admin') {
      if (_.isEmpty(formData.contactNumber)) {
        errors.contactNumber = "Phone Number is required"
      } else if (formData.contactNumber.slice(0, 2) === '91' && formData.contactNumber.length !== 12) {
        errors.contactNumber = "Phone Number is Invalid"
      }
      setError(errors)
      if (_.isEmpty(errors)) {
        const data = {
          phoneNumber: formData.contactNumber.slice(2,),
          alterNativeNumbar: !_.isEmpty(formData.alterNativeNumbar) ? formData.alterNativeNumbar.slice(2,) : '',
          countryCode: formData.contactNumber.slice(0, 2),
        }
        KycPhoneVerifyDispatch(data)
        setClientDetails((values) => ({
          ...values,
          phoneNumber: formData.contactNumber.slice(2,),
          alterNativeNumbar: !_.isEmpty(formData.alterNativeNumbar) ? formData.alterNativeNumbar.slice(2,) : '',
          countryCode: formData.contactNumber.slice(0, 2),
        }))
      }
    } else {
      if (_.isEmpty(formData.contactNumber)) {
        errors.contactNumber = "Phone Number is required"
      } else if (formData.contactNumber.slice(0, 2) === '91' && formData.contactNumber.length !== 12) {
        errors.contactNumber = "Phone Number is Invalid"
      }
      setError(errors)
      if (_.isEmpty(errors)) {
        const data = {
          phoneNumber: formData.contactNumber.slice(2,),
          alterNativeNumbar: !_.isEmpty(formData.alterNativeNumbar) ? formData.alterNativeNumbar.slice(2,) : '',
          countryCode: formData.contactNumber.slice(0, 2),
        }
        KycPhoneVerifyDispatch(data)
        setClientDetails((values) => ({
          ...values,
          phoneNumber: formData.contactNumber.slice(2,),
          alterNativeNumbar: !_.isEmpty(formData.alterNativeNumbar) ? formData.alterNativeNumbar.slice(2,) : '',
          countryCode: formData.contactNumber.slice(0, 2),
        }))
      }
    }
    setErrors(errors)
  }

  const onConfirm = () => {
    onClickNext(1)
    setClientDetails((values) => ({
      ...values,
      phoneNumber: formData.contactNumber.slice(2,),
      alterNativeNumbar: !_.isEmpty(formData.alterNativeNumbar) ? formData.alterNativeNumbar.slice(2,) : '',
      countryCode: formData.contactNumber.slice(0, 2),
      clientId: formData.clientId
    }))
  }

  const proccedStore = () => {
    onClickNext(1)
    const errors = {}
    if (Role === 'Admin') {
      if (_.isEmpty(formData.contactNumber)) {
        errors.contactNumber = "Phone Number is required"
      } else if (formData.contactNumber.slice(0, 2) === '91' && formData.contactNumber.length !== 12) {
        errors.contactNumber = "Phone Number is Invalid"
      }
    } else {
      if (_.isEmpty(formData.contactNumber)) {
        errors.contactNumber = "Phone Number is required"
      } else if (formData.contactNumber.slice(0, 2) === '91' && formData.contactNumber.length !== 12) {
        errors.contactNumber = "Phone Number is Invalid"
      }
    }
    setError(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        'We could not verify your phone - Do you still want to Proceed? ',
        '',
        'warning',
        'Yes',
        'No',
        () => { onConfirm() },
        () => { { } }
      )
    }
    setErrors(errors)
  }

  const resendOtp = () => {
    const data = {
      contactNumber: formData.contactNumber,
      resend: "1",
    }
    KycPhoneVerifyDispatch(data)
  }

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: "" })
  }

  const conatactFrom = JSON.parse(getLocalStorage(KYC_FORM.CONTACT_DETAILS))
  const OtpData = JSON.parse(getLocalStorage(KYC_FORM.OTP))
  useEffect(() => {
    setFormData({
      contactName: conatactFrom.contactName,
      contactNumber: conatactFrom.contactNumber,
      contactEmail: conatactFrom.contactEmail,
    })
    setOtpData({
      contactNumber: OtpData.contactNumber,
      code: OtpData.code,
    })
  }, [Data])

  useEffect(() => {
    if (showValidateOtp) {
      Toastify({
        text: "Please Verify Your Number",
        duration: 3000,
        newWindow: true,
        close: true,
        gravity: "bottom",
        position: "center",
        stopOnFocus: true,
        style: {
          background:
            "linear-gradient(to right, rgb(176 0 32), rgb(201 87 61))",
        },
      }).showToast()
    }
  }, [showValidateOtp])

  useEffect(() => {
    if (
      KycPhoneVerifyResponse &&
      KycPhoneVerifyResponse.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      Toastify({
        text: "OTP Sent Successfully",
        duration: 4000,
        newWindow: true,
        close: true,
        gravity: "top",
        position: "right",
        stopOnFocus: true,
        offset: {
          x: 50,
          y: 10,
        },
        className: "info",
      }).showToast()
      setShowOtp(true)
      ClearPhoneVerifyDispatch()
    } else if (
      KycPhoneVerifyResponse &&
      KycPhoneVerifyResponse.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        KycPhoneVerifyResponse && KycPhoneVerifyResponse.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      )
      ClearPhoneVerifyDispatch()
    }
  }, [KycPhoneVerifyResponse])

  useEffect(() => {
    if (KycPhoneOtp && KycPhoneOtp.status === STATUS_RESPONSE.SUCCESS_MSG) {
      // setShowOtp(true)
      onClickNext(1)
      setClientDetails((values) => ({
        ...values,
        phoneNumber: formData.contactNumber.slice(2,),
        countryCode: formData.contactNumber.slice(0, 2),
        clientId: formData.clientId
      }))
    } else if (
      KycPhoneOtp &&
      KycPhoneOtp.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        KycPhoneOtp && KycPhoneOtp.message,
        "",
        "Try again",
        "",
        () => { { } }
      )
    }
  }, [KycPhoneOtp])


  useEffect(() => {
    return () => {
      ClearPhoneVerifyDispatch()
      clearKYCphoneOTP()
      ClearKYCDispatch()
      setFormData({
        contactNumber: "",
      })
    }
  }, [])

  const handleBusinessChange = value => {
    setFormData((values) => ({ ...values, contactNumber: value }))
  }
  const handleAlterNativesChange = value => {
    setFormData((values) => ({ ...values, alterNativeNumbar: value }))
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: "" })
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  return (
    <>
      <div>
        <div className="current" data-kt-stepper-element="content">
          <div className="w-100">
            <div className="fv-row mb-10">
              {
                !showOtp ? (
                  <>
                    {/* {
                      Role === 'Admin' ? (
                        <>
                          <div className="d-flex align-items-center fs-5 fw-bold mb-2">
                            Client :
                          </div>
                          <div className='col-md-8'>
                            <ReactSelect
                              styles={customStyles}
                              isMulti={false}
                              name='AppUserId'
                              className='select2'
                              classNamePrefix='select'
                              handleChangeReactSelect={handleChangeAsignees}
                              options={AsigneesOption}
                              value={SelectedAsigneesOption}
                              isDisabled={!AsigneesOption}
                            />
                            {errors && errors.clientId && (
                              <div className="rr mt-1">
                                <style>{".rr{color:red}"}</style>
                                {errors.clientId}
                              </div>
                            )}
                          </div>
                        </>
                      ) : (
                        null
                      )
                    } */}
                    <div className="row mb-4 mt-8">
                      <div className="col-lg-6">
                        <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                          <span className="required">
                            What's your Mobile Number?
                          </span>
                          <i
                            className="fas fa-exclamation-circle ms-2 fs-7"
                            data-bs-toggle="tooltip"
                            title="Contact Number"
                          ></i>
                        </label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6">
                        <PhoneInput
                          country={'in'}
                          enableAreaCodes={true}
                          value={formData.contactNumber || ''}
                          onChange={phone => handleBusinessChange(phone)}
                          style={{

                          }}
                        />
                        {errors && errors.contactNumber && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {errors.contactNumber}
                          </div>
                        )}
                      </div>
                      <div className="col-lg-6">
                        <div className="d-flex justify-content-end">
                          <div>
                            <button
                              type="submit"
                              className="btn btn-sm btn-light-primary"
                              onClick={(e) => {
                                phoneVerify(e)
                              }}
                              disabled={phoneVerifyloading}
                            >
                              {!phoneVerifyloading && (
                                <span className="indicator-label">
                                  <i className="bi bi-person-fill" />
                                  Send OTP
                                </span>
                              )}
                              {phoneVerifyloading && (
                                <span
                                  className="indicator-progress"
                                  style={{ display: "block" }}
                                >
                                  Please wait...
                                  <span className="spinner-border spinner-border-sm align-middle ms-2" />
                                </span>
                              )}
                            </button>
                            {/* <button
                              type="submit"
                              className="btn btn-sm btn-light-primary ms-2"
                              onClick={() => {
                                proccedStore()
                              }}
                            >
                              Proceed
                            </button> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    <div className="row">
                      <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                        <span className="required">
                          {`Enter the OTP sent to +`}
                          <text className="text-danger">
                            {formData.contactNumber}
                          </text>
                        </span>
                        <i
                          className="fas fa-exclamation-circle ms-2 fs-7"
                          data-bs-toggle="tooltip"
                          title="Otp"
                        ></i>
                      </label>
                      <div className="col-lg-6">
                        <input
                          type="text"
                          className="form-control form-control-lg form-control-solid"
                          name="code"
                          placeholder="OTP"
                          onChange={(e) => otpChange(e)}
                          value={otpData.code || ""}
                          maxLength={6}
                          onKeyPress={(e) => {
                            if (!/^[0-9 .]+$/.test(e.key)) {
                              e.preventDefault()
                            }
                          }}
                        />
                        {error && error.code && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.code}
                          </div>
                        )}
                      </div>
                      <div className="col-lg-6">
                        <button
                          type="submit"
                          className="btn btn-sm btn-light-primary"
                          onClick={() => {
                            phoneVerify()
                          }}
                          disabled={phoneVerifyloading}
                        >
                          {!phoneVerifyloading && (
                            <span className="indicator-label">
                              <i className="bi bi-person-fill" />
                              Resend OTP
                            </span>
                          )}
                          {phoneVerifyloading && (
                            <span
                              className="indicator-progress"
                              style={{ display: "block" }}
                            >
                              Please wait...
                              <span className="spinner-border spinner-border-sm align-middle ms-2" />
                            </span>
                          )}
                        </button>
                      </div>
                    </div>
                    <div className="row mt-8">
                      <div className="d-flex justify-content-end">
                        <div>
                          <button
                            type="submit"
                            className="btn btn-sm btn-primary"
                            onClick={(event) => {
                              otpSubmit(event)
                            }}
                            disabled={phoneOtpVerifyLoading}
                          >
                            {!phoneOtpVerifyLoading && (
                              <span className="indicator-label">
                                <i className="bi bi-person-check-fill" />
                                Verify
                              </span>
                            )}
                            {phoneOtpVerifyLoading && (
                              <span
                                className="indicator-progress"
                                style={{ display: "block" }}
                              >
                                Please wait...
                                <span className="spinner-border spinner-border-sm align-middle ms-2" />
                              </span>
                            )}
                          </button>
                        </div>
                      </div>
                    </div>
                  </>
                )}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  getCountrys:
    state && state.CountrylistStore && state.CountrylistStore.Countrylists,
  KycPhoneVerifyResponse:
    state && state.KYCphoneStore && state.KYCphoneStore.KycPhoneVerify,
  phoneVerifyloading:
    state && state.KYCphoneStore && state.KYCphoneStore.loading,
  KycPhoneOtp:
    state && state.KYCphoneOtpStore && state.KYCphoneOtpStore.KycPhoneOtp,
  phoneOtpVerifyLoading:
    state && state.KYCphoneOtpStore && state.KYCphoneOtpStore.loading,
  KYCAddResponse:
    state && state.KYCAddStore && state.KYCAddStore.KYCAddResponse,
  clinetIdLists: state && state.clinetListStore && state.clinetListStore.clinetIdLists ? state.clinetListStore.clinetIdLists : '',
})

const mapDispatchToProps = (dispatch) => ({
  KycPhoneVerifyDispatch: (data) =>
    dispatch(KYCphoneVerify.KYCphoneVerifyList(data)),
  ClearPhoneVerifyDispatch: () =>
    dispatch(KYCphoneVerify.clearKYCphoneVerify()),
  KycPhoneOtpDispatch: (data) => dispatch(KYCphoneOtpAction.KYCphoneOTP(data)),
  clearKYCphoneOTP: (data) =>
    dispatch(KYCphoneOtpAction.clearKYCphoneOTP(data)),
  KYCAddDispatch: (data) => dispatch(KYCAddAction.KYCAdd(data)),
  ClearKYCDispatch: (data) => dispatch(KYCAddAction.clearKYC(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactInfo)
