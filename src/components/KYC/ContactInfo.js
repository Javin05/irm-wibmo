
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../theme/helpers'
import { connect } from 'react-redux'
import { contactInfoValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../utils/helper'
import { KYC_FORM, REGEX, STATUS_RESPONSE, currencyOptions } from '../../utils/constants'
import _ from 'lodash'
function ContactInfo(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [Data, setData] = useState(true)
  const [formData, setFormData] = useState({
    contactName: '',
    contactNumber: '',
    contactEmail: ''
  })


  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    // !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = contactInfoValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        // const gePayload = getCompanyDetailsUpdatePayload(formData)
        // editClientsDispatch(id, gePayload)
      } else {
        onClickNext(1)
        // setSummary((values) => ({
        //   ...values,
        //   client: {
        //     country: getCountryValue && getCountryValue.label,
        //     state: getStateValue && getStateValue.label,
        //     city: gerCityValue && gerCityValue.label,
        //     industry: getIndValue && getIndValue.label
        //   }
        // }))
        setClientDetails((values) => ({ ...values, contactInfo: formData }))
        setLocalStorage(KYC_FORM.CONTACT_DETAILS, JSON.stringify(formData))
        setData(formData)
      }
    }
  }

  const conatactFrom = JSON.parse(getLocalStorage(KYC_FORM.CONTACT_DETAILS))
  useEffect(() => {
    setFormData({
      contactName: conatactFrom.contactName,
      contactNumber: conatactFrom.contactNumber,
      contactEmail: conatactFrom.contactEmail
    })
  },[Data])


  return (
    <>
    <div>
    <h3 className='mb-4 d-flex justify-content-center'>Contact Info</h3>
      <div className='current' data-kt-stepper-element='content'>
        <div className='w-100'>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Contact Name</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Contact Name'
              ></i>
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='contactName'
              placeholder='Contact Name'
              onChange={(e) => handleChange(e)}
              value={formData.contactName || ''}
              maxLength={42}
              onKeyPress={(e) => {
                if (!REGEX.TEXT.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
            {errors && errors.contactName && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.contactName}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Contact Number</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Contact Number'
              ></i>
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='contactNumber'
              placeholder='Contact Number'
              onChange={(e) => handleChange(e)}
              value={formData.contactNumber || ''}
              maxLength={10}
              onKeyPress={(e) => {
                if (!/^[0-9 .]+$/.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
            {errors && errors.contactNumber && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.contactNumber}
              </div>
            )}
          </div>
          <div className='fv-row mb-10'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Contact Email</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='Contact Email'
              ></i>
            </label>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid'
              name='contactEmail'
              placeholder='Contact Email'
              onChange={(e) => handleChange(e)}
              value={formData.contactEmail || ''}
            />
            {errors && errors.contactEmail && (
              <div className='rr mt-1'>
                <style>{'.rr{color:red;}'}</style>
                {errors.contactEmail}
              </div>
            )}
          </div>
          <div className='fv-row mb-4'>
            <div className='d-flex flex-stack pt-10'>
              <div>
                {
                  showForm ? (
                    <button type='submit' className='btn btn-sm btn-primary me-3'
                      onClick={(event) => {
                        handleSubmit(event)
                      }}
                    >
                      <span className='indicator-label'
                      >
                        <KTSVG
                          path='/media/icons/duotune/arrows/arr064.svg'
                          className='svg-icon-3 ms-2 me-0'
                        />
                      </span>
                      Next
                    </button>
                  ) :
                    null
                }
              </div>
            </div>

          </div>
        </div>
      </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists
})

const mapDispatchToProps = (dispatch) => ({
  // getCountryDispatch: () => dispatch(CountryActions.getCountrys()),
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactInfo)