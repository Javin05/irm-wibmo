import React, { useState, useEffect, Fragment } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import _ from 'lodash'

function KycTable(props) {
    const { tableData, statusChange } = props;

    const options = {
        sizePerPageList: [5, 10, 15, 20],
        sizePerPage: 15,
        page: 1,
        noDataText: "Data not available for this search ....",
    };

    function getCaret(direction) {
        if (direction === "asc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-up-circle-fill text-primary"></i>
                </span>
            );
        }
        if (direction === "desc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-down-circle-fill text-primary"></i>
                </span>
            );
        }
        return (
            <span className="ml-2">
                <i className="bi bi-arrow-down-up"></i>
            </span>
        );
    }
    function kycIdFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return cell && `${'KYC'}${cell}`;
    }
    function phoneNumberFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return cell && cell.number;
    }
    function emailIdFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return cell && cell.emailId;
    }
    return (
        <Fragment>
            {tableData && (
                <Fragment>
                    <BootstrapTable
                        options={options}
                        data={tableData}
                        pagination={true}
                        table
                        //striped
                        hover
                    >
                        <TableHeaderColumn
                            width="80"
                            dataField="kycId"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={kycIdFormatter}
                            isKey
                        >
                            Case ID
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="businessName"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Business Name
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="phoneNumber"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={phoneNumberFormatter}
                        >
                            Phone
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="emailId"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={emailIdFormatter}
                        >
                            Email
                        </TableHeaderColumn>
                    </BootstrapTable>
                </Fragment>
            )}
        </Fragment>
    );
}
export default KycTable;
