import { Row, Col, Form } from 'react-bootstrap'
import FloatingLabel from 'react-bootstrap/FloatingLabel'
import { Field } from 'formik'

function KycFilter(props) {
    const { clearSearch, formInitials } = props
    return (
        <Row className="g-0">
            <Col md={3}>
                <FloatingLabel controlId="riskId" label="Case ID">
                    <Field
                        id="riskId"
                        name="riskId"
                        className={formInitials.riskId !== null ? "form-control active-control" : "form-control"}
                        placeholder="Case ID" />
                </FloatingLabel>
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="phone" label="Phone">
                    <Field
                        id="phone"
                        name="phone"
                        className={formInitials.phone !== null ? "form-control active-control" : "form-control"}
                        placeholder="Phone" />
                </FloatingLabel>
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="personalEmail" label="Email">
                    <Field
                        id="personalEmail"
                        name="personalEmail"
                        className={formInitials.personalEmail !== null ? "form-control active-control" : "form-control"}
                        placeholder="Email" />
                </FloatingLabel>
            </Col>
            <Col md={12} className="text-end">
                <button
                    type="clear"
                    className="btn btn-sm btn-light mr-2"
                    onClick={(e) => clearSearch(e)}>
                    CLEAR
                </button>
                <button
                    type="submit"
                    className="btn btn-sm btn-primary">
                    FILTER
                </button>
            </Col>
        </Row>
    )
}
export default KycFilter