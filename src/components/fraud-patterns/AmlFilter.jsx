import {Row, Col, Form} from 'react-bootstrap'
import FloatingLabel from 'react-bootstrap/FloatingLabel'
import { Field } from 'formik'

function AmlFilter (props) {
    const {clearSearch, formInitials} = props
	return (
		<Row className="g-0">
			<Col md={3}>
                <FloatingLabel controlId="riskId" label="Case ID">
                    <Field 
                        id="riskId" 
                        name="riskId" 
                        className={formInitials.riskId !== null ? "form-control active-control" : "form-control"}                         
                        placeholder="Case ID" />
                </FloatingLabel>
                <FloatingLabel controlId="website" label="Website">
                    <Field 
                        id="website" 
                        name="website" 
                        className={formInitials.website !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Website" />
                </FloatingLabel>
                <FloatingLabel controlId="companyName" label="Bussiness Name">
                    <Field 
                        id="companyName" 
                        name="companyName" 
                        className={formInitials.companyName !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Bussiness Name" />
                </FloatingLabel>                
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="deviceID" label="Device ID">
                    <Field 
                        id="deviceID" 
                        name="deviceID" 
                        className={formInitials.deviceID !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Device ID" />
                </FloatingLabel>                
                <FloatingLabel controlId="phone" label="Phone">
                    <Field 
                        id="phone" 
                        name="phone" 
                        className={formInitials.phone !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Phone" />
                </FloatingLabel>
                <FloatingLabel controlId="businessPhone" label="Bussiness Phone">
                    <Field 
                        id="businessPhone" 
                        name="businessPhone" 
                        className={formInitials.businessPhone !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Bussines Phone" />
                </FloatingLabel>
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="firstName" label="First Name">
                    <Field 
                        id="firstName" 
                        name="firstName" 
                        className={formInitials.firstName !== null ? "form-control active-control" : "form-control"} 
                        placeholder="First Name" />
                </FloatingLabel>                
                <FloatingLabel controlId="personalEmail" label="Email">
                    <Field 
                        id="personalEmail" 
                        name="personalEmail" 
                        className={formInitials.personalEmail !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Email" />
                </FloatingLabel>
                <FloatingLabel controlId="businessEmail" label="Bussiness Email">
                    <Field 
                        id="businessEmail" 
                        name="businessEmail" 
                        className={formInitials.businessEmail !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Bussiness Email" />
                </FloatingLabel>
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="lastName" label="Last Name">
                    <Field 
                        id="lastName" 
                        name="lastName" 
                        className={formInitials.lastName !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Last Name" />  
                </FloatingLabel>
                <FloatingLabel controlId="address" label="Address">
                    <Field 
                        id="address" 
                        name="address" 
                        className={formInitials.address !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Address" />
                </FloatingLabel>
                <FloatingLabel controlId="businessAddress" label="Bussiness Address">
                    <Field 
                        id="businessAddress" 
                        name="businessAddress" 
                        className={formInitials.businessAddress !== null ? "form-control active-control" : "form-control"} 
                        placeholder="Bussiness Address" />
                </FloatingLabel>
            </Col>
            <Col md={12} className="text-end">
                <button 
                    type="clear" 
                    className="btn btn-sm btn-light mr-2"
                    onClick={(e)=>clearSearch(e)}>
                    CLEAR
                </button> 
                <button 
                    type="submit" 
                    className="btn btn-sm btn-primary">
                    FILTER
                </button>  
            </Col>
		</Row>
	)
}
export default AmlFilter