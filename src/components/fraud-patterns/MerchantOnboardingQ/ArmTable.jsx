import React, { useState, useEffect, Fragment } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import _ from 'lodash'

function ArmTable(props) {
    const { tableData, statusChange } = props;

    const options = {
        sizePerPageList: [5, 10, 15, 20],
        sizePerPage: 15,
        page: 1,
        noDataText: "Data not available for this search ....",
    };

    function getCaret(direction) {
        if (direction === "asc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-up-circle-fill text-primary"></i>
                </span>
            );
        }
        if (direction === "desc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-down-circle-fill text-primary"></i>
                </span>
            );
        }
        return (
            <span className="ml-2">
                <i className="bi bi-arrow-down-up"></i>
            </span>
        );
    }
    function riskIdFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return cell && `${'IRM'}${cell}`;
    }

    return (
        <Fragment>
            {tableData && (
                <Fragment>
                    <BootstrapTable
                        options={options}
                        data={tableData}
                        pagination={true}
                        table
                        //striped
                        hover
                    >
                        <TableHeaderColumn
                            width="80"
                            dataField="riskId"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={riskIdFormatter}
                            isKey
                        >
                            Case ID
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="firstName"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            First Name
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="lastName"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Last Name
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="phone"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Phone
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="personalEmail"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Personal Email
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="businessName"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Business Name
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="businessPhone"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Business Phone
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="website"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Website
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="address"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Address
                        </TableHeaderColumn>
                    </BootstrapTable>
                </Fragment>
            )}
        </Fragment>
    );
}
export default ArmTable;
