import React, { useState, useEffect, Fragment } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import _ from 'lodash'
import moment from "moment"

function OGMTable(props) {
    const { tableData, statusChange } = props;

    const options = {
        sizePerPageList: [5, 10, 15, 20],
        sizePerPage: 15,
        page: 1,
        noDataText: "Data not available for this search ....",
    };

    function getCaret(direction) {
        if (direction === "asc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-up-circle-fill text-primary"></i>
                </span>
            );
        }
        if (direction === "desc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-down-circle-fill text-primary"></i>
                </span>
            );
        }
        return (
            <span className="ml-2">
                <i className="bi bi-arrow-down-up"></i>
            </span>
        );
    }

    function dateFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return `${moment(cell).format("DD-MM-YYYY") ? moment(cell).format("DD-MM-YYYY") : moment(cell).format("DD-MM-YYYY")}`;
    }
    function ogmIdFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return cell && `${'OGM'}${cell}`;
    }

    return (
        <Fragment>
            {tableData && (
                <Fragment>
                    <BootstrapTable
                        options={options}
                        data={tableData}
                        pagination={true}
                        table
                        //striped
                        hover
                    >
                        <TableHeaderColumn
                            width="80"
                            dataField="ogm_Id"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={ogmIdFormatter}
                            isKey
                        >
                            OGM ID
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="batchId"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Batch ID
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="website"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Website
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="acquirer"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Acquirer
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="ogm_start_date"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={dateFormatter}
                        >
                            Onboarding date
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="ogm_last_run_date"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={dateFormatter}
                        >
                            Last scan date
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="ogm_next_run_date"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={dateFormatter}
                        >
                            Next scan date
                        </TableHeaderColumn>
                    </BootstrapTable>
                </Fragment>
            )}
        </Fragment>
    );
}
export default OGMTable;
