import { Row, Col, Form } from 'react-bootstrap'
import FloatingLabel from 'react-bootstrap/FloatingLabel'
import { Field } from 'formik'

function OGMFilter(props) {
    const { clearSearch, formInitials } = props
    return (
        <Row className="g-0">
            <Col md={3}>
                <FloatingLabel controlId="acquirer" label="Acquirer">
                    <Field
                        id="acquirer"
                        name="acquirer"
                        className={formInitials.acquirer !== null ? "form-control active-control" : "form-control"}
                        placeholder="Acquirer" />
                </FloatingLabel>
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="batchId" label="Batch ID">
                    <Field
                        id="batchId"
                        name="batchId"
                        className={formInitials.batchId !== null ? "form-control active-control" : "form-control"}
                        placeholder="Batch ID" />
                </FloatingLabel>
            </Col>
            <Col md={3}>
                <FloatingLabel controlId="website" label="Website">
                    <Field
                        id="website"
                        name="website"
                        className={formInitials.website !== null ? "form-control active-control" : "form-control"}
                        placeholder="Website" />
                </FloatingLabel>
            </Col>
            <Col md={12} className="text-end">
                <button
                    type="clear"
                    className="btn btn-sm btn-light mr-2"
                    onClick={(e) => clearSearch(e)}>
                    CLEAR
                </button>
                <button
                    type="submit"
                    className="btn btn-sm btn-primary">
                    FILTER
                </button>
            </Col>
        </Row>
    )
}
export default OGMFilter