import { Row, Col, Form} from 'react-bootstrap'
import FloatingLabel from 'react-bootstrap/FloatingLabel'
import Select from 'react-select'
import { Field } from 'formik'

function TransactionFilter (props) {
	const {
		countryList, 
		stateList,
		cityList,
		clearSearch, 
		countryChange, 
		stateChange,
		formInitials
	} = props
	return (
		<Row className="g-0">
			<Col md={6}>
				<Row className="g-0">
					<Col md={4}>
		                <FloatingLabel controlId="caseId" label="Case ID">
		                    <Field 
		                        id="caseId" 
		                        name="caseId" 
		                        className={formInitials.caseId !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Case ID" />
		                </FloatingLabel>
		                <FloatingLabel controlId="firstName" label="Billing Firstname">
		                    <Field 
		                        id="firstName" 
		                        name="firstName" 
		                        className={formInitials.firstName !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Billing Firstname" 
		                        />
		                </FloatingLabel>
		                <FloatingLabel controlId="shipping_first_name" label="Shipping Firstname">
		                    <Field 
		                        id="shipping_first_name" 
		                        name="shipping_first_name" 
		                        className={formInitials.shipping_first_name !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Shipping Firstname" 
		                        disabled/>
		                </FloatingLabel>
		                <FloatingLabel controlId="cvv" label="CVV">
		                    <Field 
		                        id="cvv" 
		                        name="cvv" 
		                        className={formInitials.cvv !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Case ID" 
		                        disabled/>
		                </FloatingLabel>
		            </Col>
		            <Col md={4}>
		                <FloatingLabel controlId="emailAddress" label="Email">
		                    <Field 
		                        id="emailAddress" 
		                        name="emailAddress" 
		                        className={formInitials.emailAddress !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Email" />
		                </FloatingLabel>
		                <FloatingLabel controlId="lastName" label="Billing Lastname">
		                    <Field 
		                        id="lastName" 
		                        name="lastName" 
		                        className={formInitials.lastName !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Billing Lastname" 
		                        />
		                </FloatingLabel>
		                <FloatingLabel controlId="shipping_last_name" label="Shipping Lastname">
		                    <Field 
		                        id="shipping_last_name" 
		                        name="shipping_last_name" 
		                        className={formInitials.shipping_last_name !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Shipping Lastname" 
		                        disabled/>
		                </FloatingLabel>
		                <FloatingLabel controlId="billing_zip" label="Billing Zipcode">
		                    <Field 
		                        id="billing_zip" 
		                        name="billing_zip" 
		                        className={formInitials.billing_zip !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Billing Zipcode" 
		                        disabled/>
		                </FloatingLabel>
		            </Col>
		            <Col md={4}>
		                <FloatingLabel controlId="phone" label="Phone">
		                    <Field 
		                        id="phone" 
		                        name="phone" 
		                        className={formInitials.phone !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Phone" />
		                </FloatingLabel>
		                <FloatingLabel controlId="billingAddress" label="Billing Address">
		                    <Field 
		                        id="billingAddress" 
		                        name="billingAddress" 
		                        className={formInitials.billingAddress !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Billing Address" />
		                </FloatingLabel>
		                <FloatingLabel controlId="shippingAddress" label="Shipping Address">
		                    <Field 
		                        id="shippingAddress" 
		                        name="shippingAddress" 
		                        className={formInitials.shippingAddress !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Shipping Address" />
		                </FloatingLabel>
		                <FloatingLabel controlId="shipping_zip" label="Shipping Zipcode">
		                    <Field 
		                        id="shipping_zip" 
		                        name="shipping_zip" 
		                        className={formInitials.shipping_zip !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Shipping Zipcode" 
		                        disabled/>
		                </FloatingLabel>
		            </Col>
				</Row>
			</Col>
			<Col md={6}>
				<Row className="g-0">
					<Col md={4}>
		                <FloatingLabel controlId="payment_method" label="Payment Method">
		                    <Field 
		                        id="payment_method" 
		                        name="payment_method" 
		                        className={formInitials.payment_method !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Payment Method" 
		                        disabled/>
		                </FloatingLabel>
		                <FloatingLabel controlId="billing_city" label="">
		                	<Select 
                                placeholder="Country" 
                                options={countryList} 
                                onChange={(e)=>countryChange(e)}                               
                                style={{width:"100%"}}
                                //className={formInitials.billing_city !== null ? "form-control active-control" : "form-control"} 
                                isDisabled
                                />		                    
		                </FloatingLabel>
		                <FloatingLabel controlId="shipping_city" label="">
		                    <Select 
                                placeholder="Country" 
                                options={countryList} 
                                onChange={(e)=>countryChange(e)}                               
                                style={{width:"100%"}}
                                //className={formInitials.shipping_city !== null ? "form-control active-control" : "form-control"} 
                                isDisabled
                                />
		                </FloatingLabel>
		            </Col>
		            <Col md={4}>
		                <FloatingLabel controlId="credit_card_number" label="Credit Card Number">
		                    <Field 
		                        id="credit_card_number" 
		                        name="credit_card_number" 
		                        className={formInitials.credit_card_number !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="Credit Card Number" 
		                        disabled/>
		                </FloatingLabel>
		                <FloatingLabel controlId="billing_state" label="">
		                    <Select 
                                placeholder="State" 
                                options={stateList} 
                                onChange={(e)=>stateChange(e)} 
                                //className={formInitials.billing_state !== null ? "form-control active-control" : "form-control"}                               
                                style={{width:"100%"}}
                                isDisabled
                                />
		                </FloatingLabel>
		                <FloatingLabel controlId="shipping_state" label="">
		                    <Select 
                                placeholder="State" 
                                options={stateList} 
                                onChange={(e)=>stateChange(e)} 
                                //className={formInitials.shipping_state !== null ? "form-control active-control" : "form-control"}                               
                                style={{width:"100%"}}
                                isDisabled
                                />
		                </FloatingLabel>
		            </Col>
		            <Col md={4}>
		                <FloatingLabel controlId="avs" label="AVS">
		                    <Field 
		                        id="avs" 
		                        name="avs" 
		                        className={formInitials.avs !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="AVS" 
		                        disabled/>
		                </FloatingLabel>
		                <FloatingLabel controlId="billing_city" label="">
		                	<Select 
                                placeholder="City" 
                                options={cityList}                                                       
                                style={{width:"100%"}}
                                //className={formInitials.billing_city !== null ? "form-control active-control" : "form-control"} 
                                isDisabled/>
		                </FloatingLabel>
		                <FloatingLabel controlId="shipping_city" label="">
		                    <Select 
                                placeholder="City" 
                                options={cityList}                                                       
                                style={{width:"100%"}}
                                //className={formInitials.shipping_city !== null ? "form-control active-control" : "form-control"} 
                                isDisabled/>
		                </FloatingLabel>
		            </Col>
		            <Col md={4}>
		                <FloatingLabel controlId="ipAddress" label="IP Address">
		                    <Field 
		                        id="ipAddress" 
		                        name="ipAddress" 
		                        className={formInitials.ipAddress !== null ? "form-control active-control" : "form-control"} 
		                        placeholder="IP Address" 
		                        />
		                </FloatingLabel>
		            </Col>		            
				</Row>
			</Col>
			<Col md={12} className="text-end mt-2">
                <button 
                    type="clear" 
                    className="btn btn-sm btn-light mr-2"
                    onClick={(e)=>clearSearch(e)}>
                    CLEAR
                </button> 
                <button 
                    type="submit" 
                    className="btn btn-sm btn-primary">
                    FILTER
                </button>  
            </Col>
		</Row>
	)
}
export default TransactionFilter