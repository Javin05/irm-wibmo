import React, { useState, useEffect, Fragment } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { confirmationAlert } from "../../utils/alerts";
import RejectReasonModal from "./RejectReasonModal";
import HoldReasonModal from "./HoldReasonModal";
import ApproveReasonModal from "./ApproveReasonModel";
import _ from 'lodash'

function TransactionTable(props) {
  const { tableData, statusChange } = props;
  const [allow, setAllow] = useState(false);
  const [rowids, setRowids] = useState([]);
  const [rejectModal, setRejectModal] = useState(false);
  const [holdModal, setHoldModal] = useState(false);
  const [approveModal, setPpproveModal] = useState(false);

  const options = {
    sizePerPageList: [5, 10, 15, 20],
    sizePerPage: 15,
    page: 1,
    noDataText: "Data not available for this search ....",
  };
  const onRowSelect = (row, isSelected, e) => {
    setAllow(isSelected);
    if (!rowids.some((item) => item === row._id)) {
      setRowids((rowids) => [...rowids, row._id]);
    } else {
      setRowids(rowids.filter((item) => item !== row._id));
    }
  };
  const onSelectAll = (isSelected) => {
    if (isSelected) {
      setAllow(true);
      let selectedIds = tableData.map((row) => row._id);
      setRowids(selectedIds);
      return selectedIds;
    } else {
      setAllow(false);
      setRowids(null);
      return [];
    }
  };
  const onStatusToggle = (e, type) => {
    e.preventDefault();
    if (type === "approve") {
      // confirmationAlert(
      //     'success', 'Approve this list',
      //     'warning',
      //     'YES',
      //     'NO',
      //     () => {

      //         statusChange(rowids, {
      //             riskStatus: 'APPROVED',
      //             reason: ''
      //         })
      //     },
      //     () => {
      //         //Nothing
      //     }
      // )
      if (!approveModal) {
        setPpproveModal(true);
      }
    } else if (type === "reject") {
      if (!rejectModal) {
        setRejectModal(true);
      }
    } else if (type === "hold") {
      if (!holdModal) {
        setHoldModal(true);
      }
    }
  };
  const onStatusSubmit = (formData) => {
    statusChange(rowids, formData);
  };
  function getCaret(direction) {
    if (direction === "asc") {
      return (
        <span className="ml-2">
          <i className="bi bi-arrow-up-circle-fill text-primary"></i>
        </span>
      );
    }
    if (direction === "desc") {
      return (
        <span className="ml-2">
          <i className="bi bi-arrow-down-circle-fill text-primary"></i>
        </span>
      );
    }
    return (
      <span className="ml-2">
        <i className="bi bi-arrow-down-up"></i>
      </span>
    );
  }
  const selectRowProp = {
    mode: "checkbox",
    clickToSelect: false,
    columnWidth: "60px",
    bgColor: "#d6edfa",
    onSelect: onRowSelect,
    onSelectAll: onSelectAll,
  };

  function TxnIdFormatter(cell) {
    if (!cell) {
      return "--";
    }
    return cell && `${'TXN'}${cell}`;
  }

  return (
    <Fragment>
      {tableData && (
        <Fragment>
          {/* <ul className="status-ul">
            <li
              className={allow ? "approve" : "approve disabled"}
              onClick={(e) => onStatusToggle(e, "approve")}
            >
              Approve
            </li>
            <li
              className={allow ? "reject" : "reject disabled"}
              onClick={(e) => onStatusToggle(e, "reject")}
            >
              Reject
            </li>
            <li
              className={allow ? "hold" : "hold disabled"}
              onClick={(e) => onStatusToggle(e, "hold")}
            >
              Hold
            </li>
          </ul> */}
          <BootstrapTable
            options={options}
            data={tableData}
            // selectRow={selectRowProp}
            pagination={true}
            table
            //striped
            hover
          >
            <TableHeaderColumn dataField="_id" isKey>
              ID
            </TableHeaderColumn>
            <TableHeaderColumn
              width="80"
              dataField="transactionId"
              dataSort={true}
              caretRender={getCaret}
              dataFormat={TxnIdFormatter}
            >
              Case ID
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="firstName"
              dataSort={true}
              caretRender={getCaret}
            >
              Firstname
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="lastName"
              dataSort={true}
              caretRender={getCaret}
            >
              Lastname
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="phone"
              dataSort={true}
              caretRender={getCaret}
            >
              Phone
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="emailAddress"
              dataSort={true}
              caretRender={getCaret}
            >
              Email
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="paymentMethod"
              dataSort={true}
              caretRender={getCaret}
            >
              Payment Method
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="creditCardNumber"
              dataSort={true}
              caretRender={getCaret}
            >
              Credit Card
            </TableHeaderColumn>
            <TableHeaderColumn width="300" dataField="billingAddress">
              Billing Address
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="billingCity"
              dataSort={true}
              caretRender={getCaret}
            >
              Billing City
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="billingState"
              dataSort={true}
              caretRender={getCaret}
            >
              Billing State
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="billingCountry"
              dataSort={true}
              caretRender={getCaret}
            >
              Billing Country
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="billingZipCode"
              dataSort={true}
              caretRender={getCaret}
            >
              Billing Zipcode
            </TableHeaderColumn>
            <TableHeaderColumn
              width="50"
              dataField="AVS"
              dataSort={true}
              caretRender={getCaret}
            >
              AVS
            </TableHeaderColumn>
            <TableHeaderColumn
              width="50"
              dataField="CVV"
              dataSort={true}
              caretRender={getCaret}
            >
              CVV
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="shipToFirstName"
              dataSort={true}
              caretRender={getCaret}
            >
              Shipping Firstname
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="shipToLastName"
              dataSort={true}
              caretRender={getCaret}
            >
              Shipping Lastname
            </TableHeaderColumn>
            <TableHeaderColumn width="300" dataField="shippingAddress">
              Shipping Address
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="shippingCity"
              dataSort={true}
              caretRender={getCaret}
            >
              Shipping City
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="shippingState"
              dataSort={true}
              caretRender={getCaret}
            >
              Shipping State
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="shippingCountry"
              dataSort={true}
              caretRender={getCaret}
            >
              Shipping Country
            </TableHeaderColumn>
            <TableHeaderColumn
              width="120"
              dataField="shippingZipCode"
              dataSort={true}
              caretRender={getCaret}
            >
              Shipping ZipCode
            </TableHeaderColumn>
          </BootstrapTable>
        </Fragment>
      )}
      <RejectReasonModal
        openModal={rejectModal}
        onSubmit={onStatusSubmit}
        onClose={() => {
          setRejectModal(false);
        }}
      />
      <HoldReasonModal
        openModal={holdModal}
        onSubmit={onStatusSubmit}
        onClose={() => {
          setHoldModal(false);
        }}
      />
      <ApproveReasonModal
        openModal={approveModal}
        onSubmit={onStatusSubmit}
        onClose={() => {
          setPpproveModal(false);
        }}
      />
    </Fragment>
  );
}
export default TransactionTable;
