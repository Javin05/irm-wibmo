import React, { useState, useEffect } from "react";
import { Switch, Route, useLocation, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { Formik, Field, Form as Formikform, ErrorMessage } from "formik";
import { Row, Col, Form, Table } from "react-bootstrap";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Select, { components } from "react-select";
import Icofont from "react-icofont";
import * as Yup from "yup";
import {
  queuesActions,
  CountryActions,
  riskManagementActions,
  TransactionActions,
  StateActions,
  CityActions,
  AMLqueueActions,
  ApproveActions,
  FraudActions,
  clientCredFilterActions,
  WrmRiskManagementActions,
  MonitorActions,
  KYCActions,
} from "../../store/actions";

import AmlFilter from "./AmlFilter";
import TxnTable from "./TxnTable";
import AmlTable from "./AmlTable";
import WrmTable from "./WrmTableFileQ/WrmTable";
import WrmFilter from "./WrmTableFileQ/WrmFilter";
import ArmTable from "./MerchantOnboardingQ/ArmTable";
import ArmFilter from "./MerchantOnboardingQ/ArmFilter";
import OGMFilter from "./OngoingMonitorQ/OGMFilter";
import OGMTable from "./OngoingMonitorQ/OGMTable";
import KycFilter from "./KYC_Q/KYCFilter";
import KycTable from "./KYC_Q/KYCTable";
import TransactionFilter from "./TransactionFilter";
import TransactionTable from "./TransactionTable";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";
import { getLocalStorage } from "../../utils/helper";
import { SET_FILTER } from "../../utils/constants";
import './index.css'

function FraudPatterns(props) {
  const {
    fetchQueueslistDispatch,
    fetchCountryDispatch,
    fetchStateDispatch,
    fetchCityDispatch,
    fetchAmlDispatch,
    getWrmRiskManagementlistDispatch,
    getMonitorlistDispatch,
    getKYClistDispatch,
    patternActionsDispatch,
    patternActionsClearDispatch,
    fetchFraudPatternFilter,
    fetchFraudPatternTransactionFilter,
    QueuesList,
    CountriesList,
    StateList,
    CityList,
    FraudPatternList,
    FraudPatternTransactionList,
    AmlList,
    PatternActionsResponse,
    setFilterFunction,
    setFilterFunctionDispatch,
    WrmRiskManagementList,
    MonitorData,
    KYClists,
  } = props;

  const history = useHistory();
  const search = useLocation().search;
  const riskId = new URLSearchParams(search).get("riskId");
  const website = new URLSearchParams(search).get("website");
  const businessPhone = new URLSearchParams(search).get("businessPhone");
  const deviceID = new URLSearchParams(search).get("deviceID");
  const companyName = new URLSearchParams(search).get("companyName");
  const personalEmail = new URLSearchParams(search).get("personalEmail");
  const address = new URLSearchParams(search).get("address");
  const businessEmail = new URLSearchParams(search).get("businessEmail");
  const businessAddress = new URLSearchParams(search).get("businessAddress");

  const firstName = new URLSearchParams(search).get("firstName");
  const lastName = new URLSearchParams(search).get("lastName");
  const phone = new URLSearchParams(search).get("phone");

  const caseId = new URLSearchParams(search).get("caseId");
  const merchantId = new URLSearchParams(search).get("merchantId");
  const transactionStatus = new URLSearchParams(search).get(
    "transactionStatus"
  );
  const emailAddress = new URLSearchParams(search).get("emailAddress");
  const ipAddress = new URLSearchParams(search).get("ipAddress");
  const shippingAddress = new URLSearchParams(search).get("shippingAddress");
  const billingAddress = new URLSearchParams(search).get("billingAddress");
  const status = new URLSearchParams(search).get("status");

  //Wrm-risk management 
  const acquirer = new URLSearchParams(search).get("acquirer");

  //OGM - management
  const batchId = new URLSearchParams(search).get("batchId");

  const [loading, setLoading] = useState(false);
  const [formLoading, setFormLoading] = useState(false);
  const [queueOptions, setQueueOptions] = useState(null);
  const [countryOptions, setCountryOptions] = useState(null);
  const [stateOptions, setStateOptions] = useState(null);
  const [cityOptions, setCityOptions] = useState(null);
  const [defaultQueue, setDefaultQueue] = useState(null);
  const [reload, setReload] = useState(false);
  const [queue, setQueue] = useState(
    localStorage.getItem("QueuePattern") !== null
      ? localStorage.getItem("QueuePattern")
      : "624fc708ae69dc1e03f47ec9"
  );
  const [client_id, setClientID] = useState("");
  const amlInitials = {
    riskId: riskId,
    deviceID: deviceID,
    website: website,
    businessPhone: businessPhone,
    firstName: firstName,
    lastName: lastName,
    companyName: companyName,
    phone: phone,
    personalEmail: personalEmail,
    address: address,
    businessEmail: businessEmail,
    businessAddress: businessAddress,
  };
  const wrmInitials = {
    riskId: riskId,
    website: website,
    acquirer: acquirer,
  };
  const armInitials = {
    riskId: riskId,
    phone: phone,
    personalEmail: personalEmail,
    address: address,
  };
  const ogmInitials = {
    acquirer: acquirer,
    batchId: batchId,
    website: website,
  };
  const kycInitials = {
    phone: phone,
    personalEmail: personalEmail,
    riskId: riskId,
  };
  const transactionsInitials = {
    firstName: firstName,
    lastName: lastName,
    phone: phone,
    caseId: caseId,
    merchantId: merchantId,
    transactionStatus: transactionStatus,
    emailAddress: emailAddress,
    ipAddress: ipAddress,
    shippingAddress: shippingAddress,
    billingAddress: billingAddress,
    status: status,

    shipping_first_name: null,
    cvv: null,
    shipping_last_name: null,
    billing_zip: null,
    shipping_zip: null,
    payment_method: null,
    credit_card_number: null,
    avs: null,
  };

  const FilterIcon = () => {
    return <Icofont style={{ color: "#009ef7" }} icon="filter" />;
  };
  const DropdownIndicator = (props) => {
    return (
      <components.DropdownIndicator {...props}>
        <FilterIcon />
      </components.DropdownIndicator>
    );
  };
  const TriggerDispatches = (dispatchType) => {
    if (dispatchType === "country") {
      /*
            Fetch Country for dropdown
            */
      const params = {
        skipPagination: "true",
      };
      fetchCountryDispatch(params);
    } else if (dispatchType === "state") {
      /*
            Fetch State for dropdown
            */
    } else if (dispatchType === "city") {
      /*
            Fetch City for dropdown
            */
    }
  };
  const filterHandler = (queue) => {
    setFormLoading(true);
    setDefaultQueue(queue);
    localStorage.setItem("QueuePattern", queue.value);

    history.push({
      pathname: "fraud-patterns",
    });
  };
  const onCountryChange = (e) => {
    const params = {
      country: e.value,
      skipPagination: "true",
    };

    fetchStateDispatch(params);
  };
  const onStateChange = (e) => {
    const params = {
      state: e.value,
      skipPagination: "true",
    };

    fetchCityDispatch(params);
  };
  const FilterForm = (props) => {
    const { queue, countryList, clearSearch } = props;
    return (
      <Formik
        initialValues={
          queue && queue.value === "624fc6b3ae69dc1e03f47ec0"
            ? transactionsInitials
            : queue && queue.value === "624fc708ae69dc1e03f47ec9" ? amlInitials : queue && queue.value === "647d93fb3d1076a9e9a1cc76" ? wrmInitials : queue && queue.value === "647d93a83d107660c4a1ca4a" ? armInitials : queue && queue.value === "647d941d3d1076444aa1cd55" ? ogmInitials : kycInitials
        }
        onSubmit={onSubmit}
      >
        <Formikform className="overlay-loader-wrapper">
          {(() => {
            if (queue && queue.value === "624fc708ae69dc1e03f47ec9") {
              // Aml - AML_Q
              return (
                <AmlFilter
                  clearSearch={clearSearch}
                  formInitials={amlInitials}
                />
              );
            } else if (queue && queue.value === "647d93fb3d1076a9e9a1cc76") {
              // Wrm - WEB_RISK_MONITORING_Q
              return (
                <WrmFilter
                  clearSearch={clearSearch}
                  formInitials={wrmInitials}
                />
              );
            } else if (queue && queue.value === "647d93a83d107660c4a1ca4a") {
              // merchant - MERCHANT_ONBOARDING_Q
              return (
                <ArmFilter
                  clearSearch={clearSearch}
                  formInitials={armInitials}
                />
              );
            } else if (queue && queue.value === "647d941d3d1076444aa1cd55") {
              // merchant - MERCHANT_ONBOARDING_Q
              return (
                <OGMFilter
                  clearSearch={clearSearch}
                  formInitials={ogmInitials}
                />
              );
            } else if (queue && queue.value === "624fc6f0ae69dc1e03f47ec6") {
              // KYC - KYC_Q
              return (
                <KycFilter
                  clearSearch={clearSearch}
                  formInitials={kycInitials}
                />
              );
            } else if (queue && queue.value === "624fc6b3ae69dc1e03f47ec0") {
              // Transction - SUSPECT_TXN_Q
              return (
                <TransactionFilter
                  countryList={countryList}
                  stateList={stateOptions}
                  cityList={cityOptions}
                  clearSearch={clearSearch}
                  countryChange={onCountryChange}
                  stateChange={onStateChange}
                  formInitials={transactionsInitials}
                />
              );
            } else {
              return (
                <Row className="g-0">
                  <Col md={12}>Not available</Col>
                </Row>
              );
            }
          })()}
        </Formikform>
      </Formik>
    );
  };
  const statusHandler = (rowIds, formData) => {
    formData["riskanalyticsId"] = rowIds;
    patternActionsDispatch(formData);
  };
  const FilterTables = (props) => {
    const { queue, amlData, wrmData, txnData, merchantData, statusChange, ogmData, kycData } = props;
    return (
      <>
        {(() => {
          if (queue && queue.value === "624fc708ae69dc1e03f47ec9") {
            // Aml - AML_Q
            return <AmlTable tableData={amlData} statusChange={statusChange} />;
          } else if (queue && queue.value === "647d93fb3d1076a9e9a1cc76") {
            // Wrm - WEB_RISK_MONITORING_Q
            return <WrmTable tableData={wrmData} statusChange={statusChange} />;
          } else if (queue && queue.value === "647d93a83d107660c4a1ca4a") {
            // merchant - SUSPECT_ACCOUNTS_Q
            return <ArmTable tableData={txnData} statusChange={statusChange} />;
          } else if (queue && queue.value === "647d941d3d1076444aa1cd55") {
            // OGM - ONGOING_MONITORING_Q
            return <OGMTable tableData={ogmData} statusChange={statusChange} />;
          } else if (queue && queue.value === "624fc6f0ae69dc1e03f47ec6") {
            // KFC - KFC_Q
            return <KycTable tableData={kycData} statusChange={statusChange} />;
          } else if (queue && queue.value === "624fc6b3ae69dc1e03f47ec0") {
            // Transction - SUSPECT_TXN_Q
            return (
              <TransactionTable
                tableData={merchantData}
                statusChange={statusChange}
              />
            );
          } else {
            return null;
          }
        })()}
      </>
    );
  };
  const parseQueryString = function (queryString) {
    var params = {},
      queries,
      temp,
      i,
      l;
    // Split into key/value pairs
    queries = queryString.split("&");
    // Convert the array of strings into an object
    for (i = 0, l = queries.length; i < l; i++) {
      temp = queries[i].split("=");
      params[temp[0]] = temp[1];
    }
    return params;
  };
  const clearSearch = (e) => {
    e.preventDefault();
    history.push({
      pathname: "fraud-patterns",
    });
  };
  const pageLoadFetch = () => {
    // Preapare to send filter params via url as queryParams
    const queryString = new URLSearchParams(search).toString();
    var params = parseQueryString(queryString);
    params["limit"] = 100;
    params["page"] = 1;

    if (queue === "624fc708ae69dc1e03f47ec9") {
      fetchAmlDispatch(params);
    } else if (queue === "647d93fb3d1076a9e9a1cc76") {
      getWrmRiskManagementlistDispatch(params);
    } else if (queue === "647d93a83d107660c4a1ca4a") {
      fetchFraudPatternFilter(params);
    } else if (queue === "647d941d3d1076444aa1cd55") {
      getMonitorlistDispatch(params);
    } else if (queue === "624fc6f0ae69dc1e03f47ec6") {
      getKYClistDispatch(params);
    } else if (queue === "624fc6b3ae69dc1e03f47ec0") {
      TriggerDispatches("country");
      fetchFraudPatternTransactionFilter(params);
    }
  };
  function onSubmit(formfields, { setStatus, setSubmitting, resetForm }) {
    const params = {};
    for (const key in formfields) {
      if (
        Object.prototype.hasOwnProperty.call(formfields, key) &&
        formfields[key] !== "" &&
        formfields[key] !== null
      ) {
        params[key] = formfields[key];
      }
    }

    const queryString = new URLSearchParams(params).toString();
    history.push({
      pathname: "fraud-patterns",
      search: `?${queryString}`,
    });
  }

  /*
    Fetch Country Data
    */
  useEffect(() => {
    let countryTemp = [];
    if (CountriesList) {
      Object.keys(CountriesList).forEach(function (key) {
        countryTemp.push({
          value: CountriesList[key]._id,
          label: CountriesList[key].name,
          code: CountriesList[key].code,
        });
      });

      setCountryOptions(countryTemp);
    }
  }, [CountriesList]);

  /*
    Fetch State Data
    */
  useEffect(() => {
    let stateTemp = [];
    if (StateList) {
      Object.keys(StateList).forEach(function (key) {
        stateTemp.push({
          value: StateList[key]._id,
          label: StateList[key].name,
          code: StateList[key].statecode,
        });
      });

      setStateOptions(stateTemp);
    }
  }, [StateList]);

  /*
    Fetch City Data
    */
  useEffect(() => {
    let cityTemp = [];
    if (CityList) {
      Object.keys(CityList).forEach(function (key) {
        cityTemp.push({
          value: CityList[key]._id,
          label: CityList[key].name,
        });
      });

      setCityOptions(cityTemp);
    }
  }, [CityList]);

  /*
    Fetch Queue Data for Dropdown
    */
  useEffect(() => {
    let queueTemp = [];
    if (QueuesList) {
      Object.keys(QueuesList).forEach(function (key) {
        queueTemp.push({
          value: QueuesList[key]._id,
          label: QueuesList[key].queueName,
        });
      });

      // set dropdown data
      setQueueOptions(queueTemp);

      // set selected option ... from url queryParams
      if (queue) {
        let oneQueue = queueTemp.filter((q) => q.value === queue);
        setDefaultQueue(oneQueue[0]);
      } else {
        setDefaultQueue(queueTemp[0]);
      }
    }
  }, [QueuesList]);

  /*
    Dispatches
    */
  useEffect(() => {
    // Dropdown Dispatch
    fetchQueueslistDispatch();
    pageLoadFetch();
  }, [queue]);

  /*
    Update Success
    */
  useEffect(() => {
    if (PatternActionsResponse && PatternActionsResponse.status === "ok") {
      confirmationAlert(
        "success",
        PatternActionsResponse && PatternActionsResponse.message,
        "success",
        "Back to Fraud Patterns",
        "Ok",
        () => {
          pageLoadFetch();
          patternActionsClearDispatch();
        },
        () => {
          pageLoadFetch();
          patternActionsClearDispatch();
        }
      );
    }
  }, [PatternActionsResponse]);

  // filter
  useEffect(() => {
    if (setFilterFunction) {
      const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER);
      setClientID(credBasedClientValue);
      const params = {
        clientId: credBasedClientValue ? credBasedClientValue : "",
        limit: 25,
        page: 1,
      };
      if (queue === "624fc708ae69dc1e03f47ec9") {
        fetchAmlDispatch(params);
      } else if (queue === "647d93fb3d1076a9e9a1cc76") {
        getWrmRiskManagementlistDispatch(params);
      } else if (queue === "647d93a83d107660c4a1ca4a") {
        fetchFraudPatternFilter(params);
      } else if (queue === "647d941d3d1076444aa1cd55") {
        getMonitorlistDispatch(params);
      } else if (queue === "624fc6f0ae69dc1e03f47ec6") {
        getKYClistDispatch(params);
      } else if (queue === "624fc6b3ae69dc1e03f47ec0") {
        fetchFraudPatternTransactionFilter(params);
      }
      setFilterFunctionDispatch(false);
    }
  }, [setFilterFunction]);

  return (
    <div className={`card card-custom card-stretch gutter-b`}>
      <div className="card-header border-0 pt-5 pb-5">
        <div className="card-title align-items-start flex-column">
          <span className="card-label font-weight-bolder text-dark">
            Fraud Patterns
          </span>
          <span className="text-muted mt-3 font-weight-bold font-size-sm">
            More than 2+ phone numbers, and emails
          </span>
        </div>
        <div className="card-toolbar">
          <div className="ml-2">
            <Select
              placeholder="Queue"
              components={{ DropdownIndicator }}
              options={queueOptions}
              value={defaultQueue}
              onChange={(e) => filterHandler(e)}
              style={{ width: "150px" }}
            />
          </div>
        </div>
      </div>
      <div className="card-body pt-2 pb-0 pl-5 pr-5">
        <section className="fraud-pattern-filter">
          <FilterForm
            queue={defaultQueue}
            countryList={countryOptions}
            clearSearch={clearSearch}
          />
          {formLoading ? (
            <div className="overlay-loader">
              <div className="overlay-content">
                <div className="overlay-spinner"></div>
              </div>
            </div>
          ) : null}
        </section>
        <div className="tabify-wrapper">
          <Row className="g-0">
            <Col md={12} className="overlay-loader-wrapper">
              <FilterTables
                queue={defaultQueue}
                amlData={AmlList === null ? [] : AmlList}
                wrmData={WrmRiskManagementList === null ? [] : WrmRiskManagementList}
                ogmData={MonitorData === null ? [] : MonitorData}
                kycData={KYClists === null ? [] : KYClists}
                txnData={FraudPatternList === null ? [] : FraudPatternList}
                merchantData={
                  FraudPatternTransactionList === null
                    ? []
                    : FraudPatternTransactionList
                }
                statusChange={statusHandler}
              />
              {formLoading ? (
                <div className="overlay-loader">
                  <div className="overlay-content">
                    <div className="overlay-spinner"></div>
                  </div>
                </div>
              ) : null}
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  const {
    WatchListStore,
    queueslistStore,
    CountrylistStore,
    StatelistStore,
    CitylistStore,
    riskManagementlistStore,
    TransactionlistStore,
    AMLgetStore,
    ApproveStore,
    FraudActionStore,
    WrmRiskManagementStore,
    MonitorStore,
    KYClistStore,
  } = state;

  return {
    loading: state && state.WatchListStore && state.WatchListStore.loading,
    CountriesList:
      CountrylistStore && CountrylistStore.Countrylists
        ? CountrylistStore.Countrylists
        : null,
    StateList:
      StatelistStore && StatelistStore.Statelists
        ? StatelistStore.Statelists
        : null,
    CityList:
      CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : null,
    QueuesList:
      queueslistStore && queueslistStore.queueslists?.data
        ? queueslistStore.queueslists?.data
        : null,
    FraudPatternList:
      riskManagementlistStore && riskManagementlistStore.riskmgmtlists?.data
        ? riskManagementlistStore.riskmgmtlists?.data.result
        : null,
    FraudPatternTransactionList:
      TransactionlistStore && TransactionlistStore.Transactionlists?.data
        ? TransactionlistStore.Transactionlists?.data
        : null,
    AmlList:
      AMLgetStore && AMLgetStore.AMLgetList?.data
        ? AMLgetStore.AMLgetList?.data.result
        : null,
    WrmRiskManagementList: WrmRiskManagementStore && WrmRiskManagementStore.WrmRiskManagement?.data ? WrmRiskManagementStore.WrmRiskManagement?.data.result : null,
    PatternActionsResponse:
      FraudActionStore && FraudActionStore.fraudStatus
        ? FraudActionStore.fraudStatus
        : {},
    setFilterFunction:
      state &&
        state.clientCrudFilterStore &&
        state.clientCrudFilterStore.setFilterFunction &&
        state.clientCrudFilterStore.setFilterFunction
        ? state.clientCrudFilterStore.setFilterFunction
        : false,
    MonitorData: MonitorStore && MonitorStore.MonitorData?.data ? MonitorStore.MonitorData?.data.result : null,
    KYClists: KYClistStore && KYClistStore.KYClists?.data ? KYClistStore.KYClists?.data : null,
  };
};
const mapDispatchToProps = (dispatch) => ({
  fetchCountryDispatch: (params) =>
    dispatch(CountryActions.getCountrylist(params)),
  fetchStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  fetchCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  fetchQueueslistDispatch: () => dispatch(queuesActions.getqueueslist()),
  fetchAmlDispatch: (params) =>
    dispatch(AMLqueueActions.getAMLqueuelist(params)),
  fetchFraudPatternFilter: (data) =>
    dispatch(riskManagementActions.getRiskManagementlist(data)),
  fetchFraudPatternTransactionFilter: (params) =>
    dispatch(TransactionActions.getTransactionlist(params)),
  patternActionsDispatch: (formData) =>
    dispatch(FraudActions.ChangeFruadStatusInit(formData)),
  patternActionsClearDispatch: () =>
    dispatch(FraudActions.ChangeFruadStatusInitClear()),
  setFilterFunctionDispatch: (data) =>
    dispatch(clientCredFilterActions.setFilterFunction(data)),
  getWrmRiskManagementlistDispatch: (params) => dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params)),
  getMonitorlistDispatch: (params) => dispatch(MonitorActions.getMonitorlist(params)),
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
});
export default connect(mapStateToProps, mapDispatchToProps)(FraudPatterns);
