import React, { useState, useEffect, Fragment } from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {confirmationAlert} from "../../utils/alerts"
import RejectReasonModal from './RejectReasonModal'
import HoldReasonModal from './HoldReasonModal'
import ApproveReasonModal from './ApproveReasonModel'
import _ from 'lodash'

function TxnTable (props) {
	const {tableData, statusChange} = props
    const [allow, setAllow] = useState(false)
    const [rowids, setRowids] = useState([])
    const [rejectModal, setRejectModal] = useState(false)
    const [holdModal, setHoldModal] = useState(false)
    const [approveModal, setPpproveModal] = useState(false)

    const options = {
        sizePerPageList: [ 5, 10, 15, 20 ],
        sizePerPage: 15,
        page: 1,
        noDataText: 'Data not available for this search ....',
    }
    let dataDemo = []

    const onRowSelect = (row, isSelected, e) =>{
        setAllow(isSelected)
        if (!(rowids.some(item => item === row._id))) {
            setRowids(rowids => [...rowids, row._id]);
        } else {
            setRowids(rowids.filter(item => item !== row._id));
        }
    }
    const onSelectAll = (isSelected) =>{
        if (isSelected) {
            setAllow(true)
            let selectedIds = tableData.map(row => row._id);
            setRowids(selectedIds)
            return selectedIds
        } else {
            setAllow(false)
            setRowids(null)
            return [];
        }
    }  
    const onStatusToggle = (e, type) =>{
        e.preventDefault()
        if(type==='approve'){
            // confirmationAlert(
            //     'success', 'Approve this list',
            //     'warning',
            //     'YES',
            //     'NO',
            //     () => { 
            //         statusChange(rowids, {
            //             riskStatus: 'APPROVED',
            //             reason: ''
            //         })
            //     },
            //     () => { 
            //         //Nothing
            //     }
            // )     
            if(!approveModal){
                setPpproveModal(true)
            }
            
        }
        else if(type==='reject'){
            if(!rejectModal){
                setRejectModal(true)
            }           
        } 
        else if(type==='hold'){
            if(!holdModal){
                setHoldModal(true)
            }      
        }
    }
    const onStatusSubmit = (formData) =>{
        statusChange(rowids, formData) 
    }   
    function getCaret(direction) {
        if (direction === 'asc') {
            return (
                <span className="ml-2"><i className="bi bi-arrow-up-circle-fill text-primary"></i></span>
            )
        }
        if (direction === 'desc') {
            return (
                <span className="ml-2"><i className="bi bi-arrow-down-circle-fill text-primary"></i></span>
            )
        }
        return (
            <span className="ml-2"><i className="bi bi-arrow-down-up"></i></span>
        )
    }
    const selectRowProp = {
        mode: 'checkbox',
        clickToSelect: false,
        columnWidth: '60px',
        bgColor: '#d6edfa',
        onSelect: onRowSelect,
        onSelectAll: onSelectAll
    }
	return (
        <Fragment>
            { tableData && (
                <Fragment>
                    <ul className="status-ul">
                        <li className={allow? "approve" : "approve disabled" } onClick={(e)=>onStatusToggle(e, 'approve')}>Approve</li>
                        <li className={allow? "reject" : "reject disabled" } onClick={(e)=>onStatusToggle(e, 'reject')}>Reject</li>
                        <li className={allow? "hold" : "hold disabled" } onClick={(e)=>onStatusToggle(e, 'hold')}>Hold</li>
                    </ul>
                    <BootstrapTable 
                        options={options}
                        data={tableData}
                        selectRow={ selectRowProp }
                        pagination={true} 
                        table
                        //striped 
                        hover >
                        <TableHeaderColumn dataField='_id' isKey>ID</TableHeaderColumn>
                        <TableHeaderColumn width='80' dataField='riskId' dataSort={true} caretRender={ getCaret }>Case ID</TableHeaderColumn>                                    
                        <TableHeaderColumn width='150' dataField='firstName' dataSort={true} caretRender={ getCaret }>Firstname</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='lastName' dataSort={true} caretRender={ getCaret }>Lastname</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='phone' dataSort={true} caretRender={ getCaret }>Phone</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='personalEmail' dataSort={true} caretRender={ getCaret }>Email</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='ipAddress'>Ip Address</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='address'>Address</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='deviceID' dataSort={true} caretRender={ getCaret }>Device Id</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='companyName'>Company Name</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='businessPhone' dataSort={true} caretRender={ getCaret }>Company Phone</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='businessEmail' dataSort={true} caretRender={ getCaret }>Company Email</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='businessAddress'>Company Address</TableHeaderColumn>
                        <TableHeaderColumn width='150' dataField='website'>Website</TableHeaderColumn>
                    </BootstrapTable> 
                </Fragment>           
            )}
            <RejectReasonModal 
                openModal={rejectModal}
                onSubmit={onStatusSubmit}
                onClose={()=>{
                    setRejectModal(false)
                }}/>
            <HoldReasonModal 
                openModal={holdModal}
                onSubmit={onStatusSubmit}
                onClose={()=>{
                    setHoldModal(false)
                }}/>
                <ApproveReasonModal
                openModal={approveModal}
                onSubmit={onStatusSubmit}
                onClose={()=>{
                    setPpproveModal(false)
                }}
                />
        </Fragment>
	)
}
export default TxnTable