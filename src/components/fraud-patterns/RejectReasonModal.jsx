import React, {useState, useEffect} from 'react';
import { Modal, Row, Col, Form } from 'react-bootstrap'
import { Formik, Field, Form as Formikform, ErrorMessage } from 'formik'
import Select from "react-select";
import * as Yup from 'yup';

const RejectReasonModal=(props)=> {
    const {
        openModal,
        onSubmit,
        onClose
    } = props

    const [defOption, setDefOption] = useState(null)
    const options = [
        { label:"Suspect Fraud", value: "Suspect Fraud"},
        { label:"Fraud", value: "Fraud"},
        { label:"Spam", value: "Spam"},
        { label:"Fake", value: "Fake"},
        { label:"Funneling", value: "Funneling"},
        { label:"More", value: "More"}
    ]
    const initialValues = {
        riskStatus:"REJECTED",
        reason:"",
        rejectType:"",
        rejectMoreValue:""
    }
    const validationSchema = Yup.object().shape({
        reason: Yup.string()
            .required('Reason is required'),
        rejectType: Yup.string()
            .required('Reject Type is required')
    })

    function onSubmitRejection(fields, { setStatus, setSubmitting }) {
        setStatus();
        onSubmit(fields)
        setSubmitting(false)
    }

    return (
        <Modal
            show={openModal}
            onHide={onClose}
            onSubmit={onSubmitRejection}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            className="custom-modal"
            centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                   Reject Status
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Formik 
                    initialValues={initialValues} 
                    validationSchema={validationSchema} 
                    onSubmit={onSubmit}>
                    {({ errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm, handleChange, handleBlur }) => {                    
                        return (
                            <Formikform 
                                id="rejectForm" 
                                className="rejectForm" 
                                noValidate>
                                <Form.Group as={Row} className="mb-3" controlId="reason">
                                    <Form.Label>Reason For Reject</Form.Label>
                                    <Field 
                                        name="reason" 
                                        as="textarea"
                                        rows={4} 
                                        className={'form-control' + (errors.reason && touched.reason ? ' is-invalid' : '')} />
                                        <ErrorMessage name="reason" component="div" className="invalid-feedback" />                                    
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3" controlId="rejectType">
                                    <Form.Label>Type</Form.Label>    
                                    <Select
                                        style={{width:"100%"}}
                                        name="rejectType"
                                        onBlur={() => setFieldTouched("rejectType", true)}
                                        onChange={(opt, e) => {
                                            setFieldValue("rejectType", opt.label);
                                            setDefOption(opt)
                                        }}
                                        value={defOption}
                                        options={options}
                                        error={errors.rejectType}
                                        touched={touched.rejectType}
                                    />
                                    <ErrorMessage name="rejectType" component="div" className="invalid-feedback" />                 
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3" controlId="submitBtn">
                                    <Col sm="12">
                                        <button 
                                            type="submit" 
                                            disabled={isSubmitting} 
                                            className="btn btn-primary">
                                            {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            Submit
                                        </button>    
                                    </Col>
                                </Form.Group>
                            </Formikform>
                        );
                    }}
                </Formik>
            </Modal.Body>
        </Modal>
    );
}
 
export default RejectReasonModal;