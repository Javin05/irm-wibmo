import React, {useState, useEffect} from 'react';
import { Modal, Row, Col, Form } from 'react-bootstrap'
import { Formik, Field, Form as Formikform, ErrorMessage } from 'formik'
import Select from "react-select";
import * as Yup from 'yup';

const HoldReasonModal=(props)=> {
    const {
        openModal,
        onSubmit,
        onClose
    } = props

    const initialValues = {
        riskStatus:"HOLD",
        reason:""
    }
    const validationSchema = Yup.object().shape({
        reason: Yup.string()
            .required('Reason is required')
    })

    function onSubmitHold(fields, { setStatus, setSubmitting }) {
        setStatus();
        onSubmit(fields)
        setSubmitting(false)
    }

    return (
        <Modal
            show={openModal}
            onHide={onClose}
            onSubmit={onSubmit}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            className="custom-modal"
            centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                   Hold Status
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Formik 
                    initialValues={initialValues} 
                    validationSchema={validationSchema} 
                    onSubmit={onSubmitHold}>
                    {({ errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm }) => {                    
                        return (
                            <Formikform 
                                id="signUpForm" 
                                className="signinupForm" 
                                noValidate>
                                <Form.Group as={Row} className="mb-3" controlId="reason">
                                    <Form.Label>Reason For Hold</Form.Label>
                                    <Field 
                                        name="reason" 
                                        as="textarea"
                                        rows={4} 
                                        className={'form-control' + (errors.reason && touched.reason ? ' is-invalid' : '')} />
                                        <ErrorMessage name="reason" component="div" className="invalid-feedback" />                                    
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3" controlId="submitBtn">
                                    <Col sm="12">
                                        <button 
                                            type="submit" 
                                            disabled={isSubmitting} 
                                            className="btn btn-primary">
                                            {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            Submit
                                        </button>    
                                    </Col>
                                </Form.Group>
                            </Formikform>
                        );
                    }}
                </Formik>
            </Modal.Body>
        </Modal>
    );
}
 
export default HoldReasonModal;