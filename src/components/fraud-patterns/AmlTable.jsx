import React, { useState, useEffect, Fragment } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import _ from 'lodash'

function AmlTable(props) {
  const { tableData, statusChange } = props;

  const options = {
    sizePerPageList: [5, 10, 15, 20],
    sizePerPage: 15,
    page: 1,
    noDataText: "Data not available for this search ....",
  };

  function getCaret(direction) {
    if (direction === "asc") {
      return (
        <span className="ml-2">
          <i className="bi bi-arrow-up-circle-fill text-primary"></i>
        </span>
      );
    }
    if (direction === "desc") {
      return (
        <span className="ml-2">
          <i className="bi bi-arrow-down-circle-fill text-primary"></i>
        </span>
      );
    }
    return (
      <span className="ml-2">
        <i className="bi bi-arrow-down-up"></i>
      </span>
    );
  }

  function amlIdFormatter(cell) {
    if (!cell) {
      return "--";
    }
    return cell && `${'IRM'}${cell}`;
  }

  return (
    <Fragment>
      {tableData && (
        <Fragment>
          <BootstrapTable
            options={options}
            data={tableData}
            pagination={true}
            table
            //striped
            hover
          >
            <TableHeaderColumn dataField="_id" isKey>
              ID
            </TableHeaderColumn>
            <TableHeaderColumn
              width="80"
              dataField="riskId"
              dataSort={true}
              caretRender={getCaret}
              dataFormat={amlIdFormatter}
            >
              Case ID
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="firstName"
              dataSort={true}
              caretRender={getCaret}
            >
              Firstname
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="lastName"
              dataSort={true}
              caretRender={getCaret}
            >
              Lastname
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="phone"
              dataSort={true}
              caretRender={getCaret}
            >
              Phone
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="personalEmail"
              dataSort={true}
              caretRender={getCaret}
            >
              Email
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="ipAddress">
              Ip Address
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="address">
              Address
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="deviceID"
              dataSort={true}
              caretRender={getCaret}
            >
              Device Id
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="companyName">
              Business Name
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="businessPhone"
              dataSort={true}
              caretRender={getCaret}
            >
              Business Phone
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="businessEmail"
              dataSort={true}
              caretRender={getCaret}
            >
              Business Email
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="businessAddress">
              Business Address
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="website">
              Website
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="clinetId">
              Client ID
            </TableHeaderColumn>
          </BootstrapTable>
        </Fragment>
      )}
    </Fragment>
  );
}
export default AmlTable;
