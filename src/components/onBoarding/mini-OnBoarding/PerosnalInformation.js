
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import _ from 'lodash'
import { REGEX } from '../../../utils/constants'
import { 
  KYCphoneVerify,
  CityActions,
  StateActions } from '../../../store/actions'
import "toastify-js/src/toastify.css"
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import color from "../../../utils/colors"


function PersonalInformation(props) {
  const {
    onClickNext,
    setClientDetails,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
  } = props
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [errors, setErrors] = useState({})
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    personalAddress: '',
    pinCode: '',
    state: '',
    city: '',
  })

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.firstName)) {
      errors.firstName = "First Name is required"
    }
    if (_.isEmpty(formData.lastName)) {
      errors.lastName = "Last Name is required"
    }
    if (_.isEmpty(formData.personalAddress)) {
      errors.personalAddress = "Personal Address is required"
    }
    if (_.isEmpty(formData.pinCode)) {
      errors.pinCode = 'pinCode Is Required'
    }
    if (_.isEmpty(errors)) {
      onClickNext(3)
      setClientDetails((values) => ({...values, personalInformation : formData}))
    }
    setErrors(errors)
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.state)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.state._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  const handleChangeState = selectedOption => {
    if (selectedOption !== null) {
      setSelectedStateOption(selectedOption)
      setFormData(values => ({ ...values, state: selectedOption.label, city: '' }))
      if (selectedOption.value) {
        const params = {
          stateId: selectedOption.value,
          skipPagination: 'true'
        }
        getCityDispatch(params)
      }
      setSelectedCityOption()
    } else {
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData(values => ({ ...values, state: '', city: '' }))
    }
    setErrors({ ...errors, state: '' })
  }

  const handleChangeCity = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData(values => ({ ...values, city: selectedOption.label }))
    } else {
      setSelectedCityOption()
      setFormData(values => ({ ...values, city: '', address: '' }))
    }
    setErrors({ ...errors, city: '' })
  }

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.city)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.city._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>First Name</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='First Name'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='firstName'
                placeholder='First Name'
                onChange={(e) => handleChange(e)}
                value={formData.firstName || ''}
              />
              {errors && errors.firstName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.firstName}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Last Name</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Last Name'
                />
              </label>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='lastName'
                placeholder='Last Name'
                onChange={(e) => handleChange(e)}
                value={formData.lastName || ''}
              />
              {errors && errors.lastName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.lastName}
                </div>
              )}
            </div>
            <div className='fv-row mb-10'>
              <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                <span className='required'>Personal Address</span>
                <i
                  className='fas fa-exclamation-circle ms-2 fs-7'
                  data-bs-toggle='tooltip'
                  title='Personal Address'
                />
              </label>
              <textarea
                type='text'
                className='form-control form-control-lg form-control-solid'
                name='personalAddress'
                placeholder='Personal Address'
                onChange={(e) => handleChange(e)}
                value={formData.personalAddress || ''}
              />
              {errors && errors.personalAddress && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.personalAddress}
                </div>
              )}
            </div>
            <div className='row mb-4 col-lg-12 mt-4 mb-4'>
              <div className='col-lg-6'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='required fw-bold fs-4'>State</span>
                </label>
                <ReactSelect
                  isClearable
                  styles={customStyles}
                  isMulti={false}
                  name='state'
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeState}
                  options={stateOption}
                  value={selectedStateOption}
                  isDisabled={!stateOption}
                />
                {errors && errors.state && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red}'}</style>
                    {errors.state}
                  </div>
                )}
              </div>
              <div className='col-lg-6'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='required fw-bold fs-4'>City</span>
                </label>
                <ReactSelect
                  isClearable
                  styles={customStyles}
                  isMulti={false}
                  name='city'
                  className='basic-single'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeCity}
                  options={cityOptions}
                  value={selectedCityOption}
                  isDisabled={!cityOptions}
                />
                {errors && errors.city && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red}'}</style>
                    {errors.city}
                  </div>
                )}
              </div>
              <div className='col-lg-6 mt-5'>
                <label className='d-flex align-items-center fw-bold mb-2'>
                  <span className='required fw-bold fs-4'>PinCode</span>
                </label>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  name='pinCode'
                  placeholder='PinCode'
                  onChange={(e) => handleChange(e)}
                  value={formData.pinCode || ''}
                  maxLength={6}
                  onKeyPress={(e) => {
                    if (!REGEX.NUMERIC.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.pinCode && (
                  <div className='rr mt-1'>
                    <style>{'.rr{color:red}'}</style>
                    {errors.pinCode}
                  </div>
                )}
              </div>
            </div>
            <div className='fv-row mb-4'>
              <div className='d-flex align-items-center pt-10 justify-content-end'>
                <div>
                  <button type='submit' className='btn btn-sm btn-light-primary'
                    onClick={() => { handleSubmit() }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/arrows/arr064.svg'
                        className='svg-icon-3 ms-2 me-0'
                      />
                      Proceed
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists,
  getStates: state && state.StatelistStore && state.StatelistStore.Statelists ? state.StatelistStore.Statelists : {},
  getCitys: state && state.CitylistStore && state.CitylistStore.Citylists ? state.CitylistStore.Citylists : {},
})

const mapDispatchToProps = (dispatch) => ({
  KycPhoneVerifyDispatch: (data) => dispatch(KYCphoneVerify.KYCphoneVerifyList(data)),
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInformation)