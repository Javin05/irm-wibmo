
import React, { FC, useEffect, useRef, useState } from 'react'
import { connect } from 'react-redux'
import { KYC_FORM, REGEX, STATUS_RESPONSE, USER_ERROR } from '../../../utils/constants'
import _, { isEmpty, values } from 'lodash'
import { KYCemailAction, KYCemailOtpAction, KYCAddALLAction, KYCActions, KYCAddAction } from '../../../store/actions'
import Toastify from 'toastify-js'
import "toastify-js/src/toastify.css"
import { warningAlert, confirmationAlert } from "../../../utils/alerts"
import CryptoJS from "crypto-js"

function EmailInfo(props) {
  const {
    onClickNext,
    setClientDetails,
    emialVerifyloading,
    emialVerifyResponse,
    emialOtpVerifyloading,
    emialVerifyOtpResponse,
    EmailVerifyOtpDispatch,
    EmailVerifyDispatch,
    clearKYCemailOtpVerify,
    clearKYCemailVerify,
    kycAllDataSaved,
  } = props

  const [errors, setErrors] = useState({})
  const [error, setError] = useState({})
  const [showOtp, setShowOtp] = useState(false)
  const [formData, setFormData] = useState({
    contactEmail: '',
    alternateEmail: '',
  })
  const [otpData, setOtpData] = useState({
    otp: '',
  })

  const otpChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setOtpData((values) => ({ ...values, [name]: value }))
    setError({ ...error, [name]: '' })
  }

  const otpSubmit = () => {
    const error = {}
    if (_.isEmpty(otpData.otp)) {
      error.otp = "Email Otp is required"
    }
    setError(error)
    if (_.isEmpty(error)) {
      var digits = "0123456789";
      let OTP = "";
      let otp_length = 4
      for (let i = 0; i < otp_length; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
      }
      const cryptoForm = `${otpData.otp}-${formData.contactEmail}-${OTP}`
      const cryptoToken = CryptoJS.AES.encrypt(cryptoForm, 'I4M2OBW').toString()
      const data = {
        otp: CryptoJS.AES.encrypt(otpData.otp, 'I4M2OBW').toString(),
        emailId: formData.contactEmail,
        clientId: kycAllDataSaved && kycAllDataSaved.clientId,
        token: cryptoToken
      }
      EmailVerifyOtpDispatch(data)
    }
  }

  const emailVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.contactEmail)) {
      errors.contactEmail = 'Email Is Required'
    } else if (formData.contactEmail && !REGEX.EMAIL.test(formData.contactEmail)) {
      errors.contactEmail = USER_ERROR.EMAIL_INVALID
    }
    setError(errors)
    if (_.isEmpty(errors)) {
      var digits = "0123456789";
      let OTP = "";
      let otp_length = 4
      for (let i = 0; i < otp_length; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
      }
      const cryptoForm = `${formData.contactEmail}-${OTP}`
      const cryptoToken = CryptoJS.AES.encrypt(cryptoForm, 'I4M2OBW').toString()
      const data = {
        emailId: formData.contactEmail,
        alternateEmailId: formData.alternateEmail,
        clientId: kycAllDataSaved && kycAllDataSaved.clientId, 
        token: cryptoToken
      }
      EmailVerifyDispatch(data)
      setClientDetails((values) => ({ ...values, Email: formData }))
    }
    setErrors(errors)
  }

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  useEffect(() => {
    if (emialVerifyResponse && emialVerifyResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      Toastify({
        text: "Otp Sent Successfully In Your Email",
        duration: 4000,
        newWindow: true,
        close: true,
        gravity: "top",
        position: "right",
        stopOnFocus: true,
        offset: {
          x: 50,
          y: 10
        },
        className: "info"
      }).showToast()
      setShowOtp(true)
      clearKYCemailVerify()
    } else if (emialVerifyResponse && emialVerifyResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        emialVerifyResponse && emialVerifyResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCemailVerify()
    }
  }, [emialVerifyResponse])

  useEffect(() => {
    if (emialVerifyOtpResponse && emialVerifyOtpResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(2)
      clearKYCemailOtpVerify()
    } else if (emialVerifyOtpResponse && emialVerifyOtpResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        emialVerifyOtpResponse && emialVerifyOtpResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCemailOtpVerify()
    }
  }, [emialVerifyOtpResponse])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <>
              <div className='fv-row mb-10'>
                {
                  !showOtp ? (
                    <>
                      <div className='row mb-4'>
                        <div className='col-lg-12'>
                          <label className='d-flex align-items-center mb-2'>
                            <span className='text-dark fs-6 fw-bold required'>Add your email address to receive account update</span>
                            <i
                              className='fas fa-exclamation-circle ms-2 fs-7'
                              data-bs-toggle='tooltip'
                              title='Email'
                            ></i>
                          </label>
                        </div>
                      </div>
                      <div className='row mb-4'>
                        <div className='col-lg-7'>
                          <input
                            type='text'
                            className='form-control form-control-lg form-control-solid mb-4'
                            name='contactEmail'
                            placeholder='Contact Email'
                            onChange={(e) => handleChange(e)}
                            value={formData.contactEmail || ''}
                          />
                          {errors && errors.contactEmail && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red;}'}</style>
                              {errors.contactEmail}
                            </div>
                          )}
                        </div>
                        <div className='col-lg-5'>
                          <div className='d-flex justify-content-end'>
                            <div>
                              <button type='submit' className='btn btn-sm btn-light-primary'
                                onClick={() => { emailVerify() }}
                                disabled={emialVerifyloading}
                              >
                                {!emialVerifyloading &&
                                  <span className='indicator-label'>
                                    <i className='bi bi-person-fill' />
                                    Send OTP
                                  </span>
                                }
                                {emialVerifyloading && (
                                  <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className='row'>
                        <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                          <span className='required'>
                            {
                              `Enter the OTP sent to `
                            }
                            <text className='text-danger'>
                              {formData.contactEmail}
                            </text>
                          </span>
                          <i
                            className='fas fa-exclamation-circle ms-2 fs-7'
                            data-bs-toggle='tooltip'
                            title='Otp'
                          ></i>
                        </label>
                        <div className='col-lg-6'>
                          <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            name='otp'
                            placeholder='OTP'
                            onChange={(e) => otpChange(e)}
                            value={otpData.otp || ''}
                            maxLength={6}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {error && error.otp && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red;}'}</style>
                              {error.otp}
                            </div>
                          )}
                        </div>
                        <div className='col-lg-6'>
                          <button type='submit' className='btn btn-sm btn-light-primary'
                            onClick={() => { emailVerify() }}
                            disabled={emialVerifyloading}
                          >
                            {!emialVerifyloading &&
                              <span className='indicator-label'>
                                <i className='bi bi-person-fill' />
                                Resend OTP
                              </span>
                            }
                            {emialVerifyloading && (
                              <span className='indicator-progress' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-lg-12'>
                          <div className='d-flex justify-content-end'>
                            <button type='submit' className='btn btn-sm btn-primary'
                              onClick={(event) => {
                                otpSubmit(event)
                              }}
                            >
                              {!emialOtpVerifyloading &&
                                <span className='indicator-label'>
                                  <i className='bi bi-person-check-fill' />
                                  verify
                                </span>
                              }
                              {emialOtpVerifyloading && (
                                <span className='indicator-progress' style={{ display: 'block' }}>
                                  Please wait...
                                  <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                </span>
                              )}
                            </button>
                          </div>
                        </div>
                      </div>
                    </>
                  )
                }
              </div>
            </>
          </div>
        </div>
      </div >
    </>
  )
}

const mapStateToProps = (state) => ({
  emialVerifyloading: state && state.EmailVerifyStore && state.EmailVerifyStore.loading,
  emialVerifyResponse: state && state.EmailVerifyStore && state.EmailVerifyStore.emialVerify,
  emialOtpVerifyloading: state && state.EmailVerifyOtpStore && state.EmailVerifyOtpStore.loading,
  emialVerifyOtpResponse: state && state.EmailVerifyOtpStore && state.EmailVerifyOtpStore.emialOtpVerify,
  MiniKycAllDataRes: state && state.KYCAddStore && state.KYCAddStore.KYCAddResponse,
  MiniKycAllDataResLoading: state && state.KYCAddStore && state.KYCAddStore.loading,
})

const mapDispatchToProps = (dispatch) => ({
  EmailVerifyDispatch: (data) => dispatch(KYCemailAction.KYCemailVerify(data)),
  EmailVerifyOtpDispatch: (data) => dispatch(KYCemailOtpAction.KYCemailOtpVerify(data)),
  clearKYCemailOtpVerify: (data) => dispatch(KYCemailOtpAction.clearKYCemailOtpVerify(data)),
  clearKYCemailVerify: (data) => dispatch(KYCemailAction.clearKYCemailVerify(data)),
  AddAllKycDataDispatch: (id, data) => dispatch(KYCAddALLAction.KYCAddAllData(id, data)),
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  KYCAddDispatch: (data) => dispatch(KYCAddAction.KYCAdd(data)),
  ClearKYCDispatch: (data) => dispatch(KYCAddAction.clearKYC(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(EmailInfo)