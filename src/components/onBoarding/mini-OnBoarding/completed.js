import React, { useEffect, useState } from "react"
import { connect } from 'react-redux'
import { STATUS_RESPONSE, USER_ERROR } from '../../../utils/constants'
import { OnBoardingAddAction, onBoardingActions } from '../../../store/actions'
import { warningAlert } from "../../../utils/alerts"
import _, { isEmpty, values } from 'lodash'

function ComptedPage(props) {
  const {
    setKycShow,
    setShow,
    OnBoardingDataResLoading,
    OnBoardingDataRes,
    kycAllDataSaved,
    OnBoardingAddDispatch,
    ClearOnBoardingDispatch,
    setClientDetails,
    getOnBoardinglistDispatch,
    setCompletedSteps,
    setActiveStep,
    onClickNext
  } = props

  const [show, SetShow] = useState(false)

  useEffect(() => {
    const data = {
      phoneNumber: kycAllDataSaved && kycAllDataSaved.phoneNumber,
      alternatePhoneNumber: kycAllDataSaved && kycAllDataSaved.alterNativeNumbar,
      countryCode: kycAllDataSaved && kycAllDataSaved.countryCode,
      emailId: kycAllDataSaved && kycAllDataSaved.Email && kycAllDataSaved.Email.contactEmail,
      alternateEmailId: kycAllDataSaved && kycAllDataSaved.Email && kycAllDataSaved.Email.alternateEmail,
      clientId: kycAllDataSaved && kycAllDataSaved.clientId,
      businessCategory: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessCategory,
      businessName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessName,
      entityName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.entiryName,
      entityId: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.entityId,
      primaryContactName: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.primaryContactName,
      website: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.website,
      businessDescription: kycAllDataSaved && kycAllDataSaved.business && kycAllDataSaved.business.businessDescription,
      firstName: kycAllDataSaved && kycAllDataSaved.personalInformation && kycAllDataSaved.personalInformation.firstName,
      lastName: kycAllDataSaved && kycAllDataSaved.personalInformation && kycAllDataSaved.personalInformation.lastName,
      address: {
        address: kycAllDataSaved && kycAllDataSaved.personalInformation && kycAllDataSaved.personalInformation.personalAddress,
        city: kycAllDataSaved && kycAllDataSaved.personalInformation && kycAllDataSaved.personalInformation.city,
        state: kycAllDataSaved && kycAllDataSaved.personalInformation && kycAllDataSaved.personalInformation.state,
        pincode: kycAllDataSaved && kycAllDataSaved.personalInformation && kycAllDataSaved.personalInformation.pinCode,
      }
    }
    if (!_.isEmpty(data)) {
      OnBoardingAddDispatch(data)
    }
  }, [])    

  useEffect(() => {
    if (OnBoardingDataRes && OnBoardingDataRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setClientDetails((values) => ({
        ...values,
        PhoneNUmberId:
        OnBoardingDataRes && OnBoardingDataRes.data && OnBoardingDataRes.data._id,
        allData: OnBoardingDataRes && OnBoardingDataRes.data
      }))
      getOnBoardinglistDispatch()
      onClickNext(5)
    } else if (OnBoardingDataRes && OnBoardingDataRes.status === STATUS_RESPONSE.ERROR_MSG) {
      SetShow(true)
      warningAlert(
        'error',
        OnBoardingDataRes && OnBoardingDataRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      ClearOnBoardingDispatch()
    }
  }, [OnBoardingDataRes])

  return (
    <>
      <div>
        {
          show ? (
            <>
              <div>
                <div class="card card-flush bg-danger"
                >
                  <div class="card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"
                  >
                    <div className="d-flex justify-content-center">
                    </div>
                    <div class="fs-1hx fw-bold text-white text-center mb-10 mt-5">
                      Your Data Not Updated
                    </div>
                  </div>
                </div>
                <div class="text-center mt-4">
                  <a class=" ms-4 btn btn-light-danger"
                    onClick={() => {
                      setShow(false)
                      setCompletedSteps([0])
                      setActiveStep(0)
                    }}
                  >Back to List</a>
                </div>
              </div>
            </>
          ) : (
            <>
              {
                !OnBoardingDataResLoading ? (
                  null
                ) : (
                  <>
                    <div
                      className='d-flex justify-content-center mt-20'
                    >
                      <h4 className='text-muted me-4'>Processing</h4>
                      <span
                        className='spinner-grow text-primary me-4'
                        role='status'
                      />
                      <span
                        className='spinner-grow text-danger me-4'
                        role='status'
                      />
                      <span
                        className='spinner-grow text-warning me-4'
                        role='status'
                      />
                      <span
                        className='spinner-grow text-info me-4'
                        role='status'
                      />
                    </div>
                  </>
                )
              }
            </>
          )
        }
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  OnBoardingDataRes: state && state.OnBoardingAddStore && state.OnBoardingAddStore.OnBoardingAddResponse,
  OnBoardingDataResLoading: state && state.OnBoardingAddStore && state.OnBoardingAddStore.loading,
})

const mapDispatchToProps = (dispatch) => ({
  OnBoardingAddDispatch: (data) => dispatch(OnBoardingAddAction.OnBoardingAdd(data)),
  ClearOnBoardingDispatch: (data) => dispatch(OnBoardingAddAction.clearOnBoarding(data)),
  getOnBoardinglistDispatch: (params) => dispatch(onBoardingActions.getOnBoardinglist(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ComptedPage)