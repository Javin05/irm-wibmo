import React, { useEffect } from "react";
import {
  onBoardingActions
} from '../../../store/actions'
import { connect } from 'react-redux'

function FullKycCompletedPage(props) {
  const {
    setKycShow,
    setShow,
    getOnBoardinglistDispatch,
    setActiveStep,
    setCompletedSteps,
    setClientDetails
  } = props
  
  useEffect(() => {
    getOnBoardinglistDispatch()
  },[])
  
  return (
    <>
      <div>
        <div className="card card-flush"
          style={{
            backgroundColor: '#1a1d31'
          }}
        >
          <div className="card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"
          >
            <div className="d-flex justify-content-center">
              <i class="bi bi-check-circle-fill"
                style={{ color: '#208f20', backgroundColor: 'transparent', fontSize: '2.75rem' }}
              />
            </div>
            <div className="fs-1hx fw-bold text-white text-center mb-10 mt-5">
              Your Details Submitted Successfully
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-4">
        <a className="btn btn-light-dark"
          onClick={() => { 
            setShow(false)
            setCompletedSteps([-1])
            setActiveStep(0)
            setKycShow(false)
            setClientDetails(null)
          }}
          >Back to List</a>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => {
  const { FullKycValueStore } = state
  return {
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getOnBoardinglistDispatch: (params) => dispatch(onBoardingActions.getOnBoardinglist(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FullKycCompletedPage)
