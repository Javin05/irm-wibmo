import React, { useEffect, useState } from "react"
import _, { map } from 'lodash'
import {
  CityActions,
  OnBoardingPutAction
} from '../../../store/actions'
import { connect } from 'react-redux'
import { STATUS_RESPONSE, KYC_URL, AADHAAR_FRONT, AADHAAR_BACK, DOCUMENT_URL } from "../../../utils/constants"
import { warningAlert } from "../../../utils/alerts"
import {
  COMMON_PROOF_TYPE
} from "../../../constants/index"
import { getLocalStorage } from '../../../utils/helper'

function FullOnBoarding(props) {
  const {
    onClickNext,
    OnBoardingUpdateDispatch,
    OnBoardingUpdate,
    kycAllDataSaved,
    fullKycDetails,
    OnBoardingUpdateLoading,
    clearOnBoardingUpdateDispatch, 
    setSummary
  } = props
  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const [nameChange] = useState(COMMON_PROOF_TYPE[organization])
  const [frontName] = useState(AADHAAR_FRONT[fullKycDetails && fullKycDetails.AddressData && fullKycDetails.AddressData.identityProof])
  const [backName] = useState(AADHAAR_BACK[fullKycDetails && fullKycDetails.AddressData && fullKycDetails.AddressData.identityProof])
  const url = DOCUMENT_URL

  console.log("kycAllDataSaved", kycAllDataSaved)

  const handleSubmit = () => {
    if (setSummary === 'NO') {
      const AddressData = fullKycDetails && fullKycDetails.AddressData
      const id = kycAllDataSaved && kycAllDataSaved.PhoneNUmberId
      const data = {
        "address": AddressData && AddressData.Address,
        "city": AddressData && AddressData.city,
        "state": AddressData && AddressData.state,
        "pincode": AddressData && AddressData.pinCode,
        "phone": AddressData && AddressData.Phone.slice(2,),
        "email": AddressData && AddressData.Email
      }
      OnBoardingUpdateDispatch(id, data)
    } else {
      const gstin = fullKycDetails && fullKycDetails.gstinData
      const AddressData = fullKycDetails && fullKycDetails.AddressData
      const uploadsData = fullKycDetails && fullKycDetails.uploadsData
      const solePropData = fullKycDetails && !_.isEmpty(fullKycDetails.solePropData) ? fullKycDetails.solePropData : ''

      const identity = fullKycDetails && fullKycDetails.indidual.map((o) => {
        const data = {
          documentName: _.isEmpty(o.uploadDocument) ? '' : o.documentName,
          documentNumber: o.documentNumber,
          uploadDocument: _.isEmpty(o.uploadDocument) ? '' : `${url}${o.uploadDocument}`
        }
        return data
      })
      const address = fullKycDetails && fullKycDetails.adress.map((o) => {
        const data = {
          documentName: _.isEmpty(o.uploadDocument) ? '' : o.documentName,
          documentNumber: o.documentNumber,
          uploadDocument: _.isEmpty(o.uploadDocument) ? '' : `${url}${o.uploadDocument}`
        }
        return data
      })
      const business = fullKycDetails && fullKycDetails.business.map((o) => {
        const data = {
          documentName: _.isEmpty(o.uploadDocument) ? '' : o.documentName,
          documentNumber: o.documentNumber,
          uploadDocument: _.isEmpty(o.uploadDocument) ? '' : `${url}${o.uploadDocument}`
        }
        return data
      })
      const id = kycAllDataSaved && kycAllDataSaved.PhoneNUmberId
      const data = {
        "bankAccountNumber": fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.accountDetails,
        "ifscCode": fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.ifscCode,
        "accountHolderName": fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.accountHolderName,
        "cancelledCheque": `${url}${fullKycDetails && fullKycDetails.Account && fullKycDetails.Account.cancelledCheque}`,
        "documents": {
          "identityProof": identity,
          "otherDocuments": address,
          "businessProof": business
        },
        "gstDetails": {
          "country": gstin && gstin.country,
          "gstInNumber": gstin && gstin.gstNumber,
          "gstIn": "YES"
        },
        "address": AddressData && AddressData.Address,
        "city": AddressData && AddressData.city,
        "state": AddressData && AddressData.state,
        "pincode": AddressData && AddressData.pinCode,
        "phone": AddressData && AddressData.Phone.slice(2,),
        "email": AddressData && AddressData.Email
      }
      OnBoardingUpdateDispatch(id, data)
    }
  }

  useEffect(() => {
    if (OnBoardingUpdate && OnBoardingUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      if (setSummary === 'NO') {
      onClickNext(7)
      } else {
        onClickNext(9)
      }
      clearOnBoardingUpdateDispatch()
    } else if (OnBoardingUpdate && OnBoardingUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        OnBoardingUpdate && OnBoardingUpdate.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [OnBoardingUpdate])

  return (
    <>
      {
        !OnBoardingUpdateLoading ? (

          <div>
            <div className="row">
              <div className='col-lg-12'>
                <label className='d-flex align-items-center mb-2'>
                  <span className='text-muted fs-6 fw-bold'>Submit Your All Details</span>
                  <i
                    className='fas fa-exclamation-circle ms-2 fs-7'
                    data-bs-toggle='tooltip'
                    title='AadharNumber'
                  />
                </label>
              </div>
            </div>
            <div className="d-flex justify-content-center mt-4">
              <button type='submit' className='btn btn-sm btn-light-dark me-3'
                onClick={() => {
                  handleSubmit()
                }}
              >
                <i className={`bi bi-person-check-fill`} />
                {OnBoardingUpdateLoading
                  ? (
                    <span
                      className='spinner-border spinner-border-sm mx-3'
                      role='status'
                      aria-hidden='true'
                    />
                  )
                  : (
                    'Submit'
                  )}
              </button>
            </div>
          </div>
        ) : (
          <>
            <div
              className='d-flex justify-content-center mt-20'
            >
              <h4 className='text-muted me-4'>Processing</h4>
              <span
                className='spinner-grow text-primary me-4'
                role='status'
              />
              <span
                className='spinner-grow text-danger me-4'
                role='status'
              />
              <span
                className='spinner-grow text-warning me-4'
                role='status'
              />
              <span
                className='spinner-grow text-info me-4'
                role='status'
              />
            </div>
          </>
        )
      }
    </>
  )
}

const mapStateToProps = state => {
  const { OnBoardingUpdateStore } = state
  return {
    OnBoardingUpdate: OnBoardingUpdateStore && OnBoardingUpdateStore.OnBoardingUpdateResponse ? OnBoardingUpdateStore.OnBoardingUpdateResponse : {},
    OnBoardingUpdateLoading: OnBoardingUpdateStore && OnBoardingUpdateStore.loading ? OnBoardingUpdateStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  OnBoardingUpdateDispatch: (id, params) => dispatch(OnBoardingPutAction.OnBoardingUpdate(id, params)),
  clearOnBoardingUpdateDispatch: (id, params) => dispatch(OnBoardingPutAction.clearOnBoardingUpdate(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(FullOnBoarding)