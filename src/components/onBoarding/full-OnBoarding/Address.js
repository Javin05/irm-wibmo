
import React, { FC, useEffect, useRef, useState } from 'react'
import { connect } from 'react-redux'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { DROPZONE_IMAGE_NAME_TYPES, REGEX, STATUS_RESPONSE, DROPZONE_MESSAGES, AADHAAR_BACK, AADHAAR_FRONT, IDPROFF, FRONT_TYPE_VERIFY } from '../../../utils/constants'
import _, { values } from 'lodash'
import {
  CityActions,
  KYCAdharNumberAction,
  AadhaarFrontAction,
  AadhaarBackAction,
  StateActions
} from '../../../store/actions'
import { warningAlert } from "../../../utils/alerts"
import {
  FILE_FORMAT_TYPE,
  FILE_FORMAT_TYPE_DOCUMEN_IMAGE
} from "../../../constants/index"
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'

function Address(props) {
  const {
    onClickNext,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
    KycAadharDispatch,
    AadharVerifyRes,
    Aadharloading,
    clearKYCAdhaarDispatch,
    setFullKycDetails,
    AadharFrontValue,
    AadharBackValue,
    ClearAadhaarBackDispatch,
    ClearAadhaarFrontDispatch,
    setSummary
  } = props

  const [errors, setErrors] = useState({})
  const [showForm, setShowForm] = useState(true)
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [Data, setData] = useState(true)
  const [frontendfileName, setFrontendfileName] = useState('Upload')
  const [backfileName, setbackfileName] = useState('Upload')
  const [showfrontAadhaar, setShowfrontAadhaar] = useState(false)
  const [showbackAadhaar, setShowbackAadhaar] = useState(false)
  const [formData, setFormData] = useState({
    Phone: '',
    Email: '',
    Address: '',
    pinCode: '',
    state: '',
    city: '',
    aadharNumber: '',
    identityProof: '',
    AadhaarFront: '',
    AadhaarBack: '',
    Aadhartype: '',
    businessIpAddress: ''
  })

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    getStateDispatch(params)
  }, [])

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  setLocalStorage('AADHARTYPE', JSON.stringify(formData.identityProof ? formData.identityProof : 'null'))
  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const handleSubmit = () => {
    const errors = {}
    if (organization === 'Partnership') {
      if (_.isEmpty(formData.Phone)) {
        errors.Phone = 'Phone Number Is Required'
      }else if (formData.Phone.slice(0, 2) === '91' && formData.Phone.length !== 12) {
        errors.Phone = "Phone Number Is Invalid"
      }
      if (_.isEmpty(formData.Email)) {
        errors.Email = 'Email Id Is Required'
      }
      if (_.isEmpty(formData.Address)) {
        errors.Address = 'Address Number Is Required'
      }
      if (_.isEmpty(formData.pinCode)) {
        errors.pinCode = 'pinCode Is Required'
      }
      if (_.isEmpty(formData.businessIpAddress)) {
        errors.businessIpAddress = 'Business Ip Address Is Required.'
      }
      if (_.isEmpty(formData.aadharNumber)) {
        errors.aadharNumber = 'Aadhar Number Is Required'
      }
      if (_.isEmpty(formData.identityProof)) {
        errors.identityProof = 'PLease Select Idetity Proof'
      }
      if (_.isEmpty(formData.AadhaarFront)) {
        errors.AadhaarFront = `${AADHAAR_FRONT[formData.identityProof]} Is Required`
      }
      if (_.isEmpty(formData.AadhaarBack)) {
        errors.AadhaarBack = `${AADHAAR_BACK[formData.identityProof]} Is Required`
      }
      if (_.isEmpty(errors)) {
        setLocalStorage('ADDRESSVERIFY', JSON.stringify(formData))
        setFullKycDetails((values) => ({ ...values, AddressData: formData }))
        setData(formData)
        const params = {
          aadharNumber: formData.aadharNumber
        }
        KycAadharDispatch(params)
      }
    } else {
      if (_.isEmpty(formData.Phone)) {
        errors.Phone = 'Phone Number Is Required'
      }else if (formData.Phone.slice(0, 2) === '91' && formData.Phone.length !== 12) {
        errors.Phone = "Phone Number Is Invalid"
      }
      if (_.isEmpty(formData.Email)) {
        errors.Email = 'Email Id Is Required'
      }
      if (_.isEmpty(formData.Address)) {
        errors.Address = 'Address Number Is Required'
      }
      if (_.isEmpty(formData.pinCode)) {
        errors.pinCode = 'pinCode Is Required'
      }
      if (_.isEmpty(errors)) {
        setLocalStorage('ADDRESSVERIFY', JSON.stringify(formData))
        setFullKycDetails((values) => ({ ...values, AddressData: formData }))
        setData(formData)
        if (setSummary === 'NO') {
        onClickNext(6)
        } else {
          onClickNext(8)
        }
      }
    }
    setErrors(errors)
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.state)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.state._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  const handleChangeState = selectedOption => {
    if (selectedOption !== null) {
      setSelectedStateOption(selectedOption)
      setFormData(values => ({ ...values, state: selectedOption.label, city: '' }))
      if (selectedOption.value) {
        const params = {
          stateId: selectedOption.value,
          skipPagination: 'true'
        }
        getCityDispatch(params)
      }
      setSelectedCityOption()
    } else {
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData(values => ({ ...values, state: '', city: '' }))
    }
    setErrors({ ...errors, state: '' })
  }

  const handleChangeCity = selectedOption => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData(values => ({ ...values, city: selectedOption.label }))
    } else {
      setSelectedCityOption()
      setFormData(values => ({ ...values, city: '', area: '', address: '' }))
    }
    setErrors({ ...errors, city: '' })
  }

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.city)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.city._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])
  useEffect(() => {
    if (AadharVerifyRes && AadharVerifyRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      if (setSummary === 'NO') {
      onClickNext(6)
      } else {
        onClickNext(7)
      }
      clearKYCAdhaarDispatch()
    }
    else if (AadharVerifyRes && AadharVerifyRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        AadharVerifyRes && AadharVerifyRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCAdhaarDispatch()
    }
  }, [AadharVerifyRes])

  useEffect(() => {
    if (AadharFrontValue && AadharFrontValue.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setFrontendfileName('Uploaded')
      setShowfrontAadhaar(true)
      setErrors(true)
      const data = AadharFrontValue && AadharFrontValue.data && AadharFrontValue.data.path
      const dataType = AadharFrontValue && AadharFrontValue.data && AadharFrontValue.data.type
      setFormData({
        AadhaarBack: formData.businessPanImage,
        AadhaarFront: data,
        Phone: formData.Phone,
        Email: formData.Email,
        Address: formData.Address,
        aadharNumber: formData.aadharNumber,
        identityProof: formData.identityProof,
        Address: formData.Address,
        Aadhartype: dataType,
        pinCode: formData.pinCode,
        state: formData.state,
        city: formData.city,
        businessIpAddress: formData.businessIpAddress
      })
      ClearAadhaarFrontDispatch()

    } else if (AadharFrontValue && AadharFrontValue.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        AadharFrontValue && AadharFrontValue.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      ClearAadhaarFrontDispatch()
    }
  }, [AadharFrontValue])

  useEffect(() => {
    if (AadharBackValue && AadharBackValue.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setbackfileName('Uploaded')
      setShowbackAadhaar(true)
      setErrors(true)
      const data = AadharBackValue && AadharBackValue.data && AadharBackValue.data.path
      setFormData({
        AadhaarBack: data,
        AadhaarFront: formData.AadhaarFront,
        Phone: formData.Phone,
        Email: formData.Email,
        Address: formData.Address,
        aadharNumber: formData.aadharNumber,
        identityProof: formData.identityProof,
        Address: formData.Address,
        pinCode: formData.pinCode,
        state: formData.state,
        city: formData.city,
        businessIpAddress: formData.businessIpAddress
      })
      ClearAadhaarBackDispatch()
    } else if (AadharBackValue && AadharBackValue.status === STATUS_RESPONSE.ERROR_MSG) {
      const data = AadharBackValue && AadharBackValue.data && AadharBackValue.data.path
      setFormData({
        AadhaarBack: data,
        AadhaarFront: formData.AadhaarFront,
        Phone: formData.Phone,
        Email: formData.Email,
        Address: formData.Address,
        aadharNumber: formData.aadharNumber,
        identityProof: formData.identityProof,
        Address: formData.Address,
        pinCode: formData.pinCode,
        state: formData.state,
        city: formData.city,
        businessIpAddress: formData.businessIpAddress
      })
      ClearAadhaarBackDispatch()
    }
  }, [AadharBackValue])

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  useEffect(() => {
    return () => {
      setFormData({
        Phone: '',
        Email: '',
        Address: '',
        pinCode: '',
        state: '',
        city: '',
        aadharNumber: '',
        identityProof: '',
        AadhaarFront: '',
        AadhaarBack: '',
        Aadhartype: '',
        businessIpAddress: ''
      })
    }
  }, [])

  const handleBusinessChange = value => {
    setFormData((values) => ({ ...values, Phone: value }))
  }

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
            <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Business Phone</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Add your pinCode and Address'
                    />
                  </label>
                </div>
              </div>
               <PhoneInput
                  country={'in'}
                  enableAreaCodes={true}
                  value={formData.Phone || ''}
                  onChange={phone => handleBusinessChange(phone)}
              />
              {errors && errors.Phone && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red}'}</style>
                  {errors.Phone}
                </div>
              )}

              <div className='row mb-4 mt-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Business Email</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Add your pinCode and Address'
                    />
                  </label>
                </div>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='Email'
                placeholder='Email'
                onChange={(e) => handleChange(e)}
                value={formData.Email || ''}
              />
              {errors && errors.Email && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red}'}</style>
                  {errors.Email}
                </div>
              )}

              <div className='row mb-4 mt-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Add your registered business address</span>
                  </label>
                </div>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>We'll verify these details using your given business documents.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Add your pinCode and Address'
                    />
                  </label>
                </div>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='Address'
                placeholder='Address'
                onChange={(e) => handleChange(e)}
                value={formData.Address || ''}
              />
              {errors && errors.Address && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red}'}</style>
                  {errors.Address}
                </div>
              )}
              <div className='row mb-4 col-lg-12 mt-4 mb-4'>
                <div className='col-lg-6'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>State</span>
                  </label>
                  <ReactSelect
                    isClearable
                    styles={customStyles}
                    isMulti={false}
                    name='state'
                    placeholder="Select..."
                    className="basic-single"
                    classNamePrefix="select"
                    handleChangeReactSelect={handleChangeState}
                    options={stateOption}
                    value={selectedStateOption}
                    isDisabled={!stateOption}
                  />
                  {errors && errors.state && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red}'}</style>
                      {errors.state}
                    </div>
                  )}
                </div>
                <div className='col-lg-6'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>City</span>
                  </label>
                  <ReactSelect
                    isClearable
                    styles={customStyles}
                    isMulti={false}
                    name='city'
                    className='basic-single'
                    classNamePrefix='select'
                    handleChangeReactSelect={handleChangeCity}
                    options={cityOptions}
                    value={selectedCityOption}
                    isDisabled={!cityOptions}
                  />
                  {errors && errors.city && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red}'}</style>
                      {errors.city}
                    </div>
                  )}
                </div>
                <div className='col-lg-6 mt-5'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>PinCode</span>
                  </label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid'
                    name='pinCode'
                    placeholder='PinCode'
                    onChange={(e) => handleChange(e)}
                    value={formData.pinCode || ''}
                    maxLength={6}
                    onKeyPress={(e) => {
                      if (!REGEX.NUMERIC.test(e.key)) {
                        e.preventDefault()
                      }
                    }}
                  />
                  {errors && errors.pinCode && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red}'}</style>
                      {errors.pinCode}
                    </div>
                  )}
                </div>
              </div>
            </div>
            {
              organization === 'Partnership' ? (
                <>
                  <div className='fv-row mb-10'>
                    <div className='row mb-4'>
                      <div className='col-lg-12'>
                        <label className='d-flex align-items-center fw-bold mb-2'>
                          <span className='required fw-bold fs-4'>Add an identify Proof for {`${``}`}</span>
                        </label>
                      </div>
                      <div className='col-lg-12'>
                        <label className='d-flex align-items-center mb-2'>
                          <span className='text-muted fs-6 fw-bold'>We require this information for taxation and compliance.</span>
                          <i
                            className='fas fa-exclamation-circle ms-2 fs-7'
                            data-bs-toggle='tooltip'
                            title='AadharNumber'
                          />
                        </label>
                      </div>
                    </div>
                    <input
                      type='text'
                      className='form-control form-control-lg form-control-solid mt-4'
                      name='aadharNumber'
                      placeholder='Adhaar Number'
                      onChange={(e) => handleChange(e)}
                      value={formData.aadharNumber || ''}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC.test(e.key)) {
                          e.preventDefault()
                        }
                      }}
                    />
                    {errors && errors.aadharNumber && (
                      <div className='rr mt-1'>
                        <style>{'.rr{color:red}'}</style>
                        {errors.aadharNumber}
                      </div>
                    )}
                  </div>
                </>
              ) : (
                null
              )
            }
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2' />
                <div className='d-flex'>
                  {
                    showForm ? (
                      <button type='submit' className='btn btn-sm btn-primary me-3'
                        onClick={() => {
                          handleSubmit()
                        }}
                      >
                        <span className='indicator-label'
                        >
                          <i className={`bi bi-person-check-fill`} />
                        </span>
                        {Aadharloading
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true'
                            />
                          )
                          : (
                            'Proceed'
                          )}
                      </button>
                    ) :
                      null
                  }
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, AdharNumberStore, AAdhaarUploadStore, AAdhaarBackUploadStore } = state

  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    AadharVerifyRes: AdharNumberStore && AdharNumberStore.AadharVerify ? AdharNumberStore.AadharVerify : {},
    Aadharloading: AdharNumberStore && AdharNumberStore.loading ? AdharNumberStore.loading : false,
    AadharFrontValue: AAdhaarUploadStore && AAdhaarUploadStore.AadharFrontValue ? AAdhaarUploadStore.AadharFrontValue : {},
    AadharFrontLoading: AAdhaarUploadStore && AAdhaarUploadStore.loading ? AAdhaarUploadStore.loading : false,
    AadharBackValue: AAdhaarBackUploadStore && AAdhaarBackUploadStore.AadharBackValue ? AAdhaarBackUploadStore.AadharBackValue : {},
    AadharBackLoading: AAdhaarBackUploadStore && AAdhaarBackUploadStore.loading ? AAdhaarBackUploadStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycAadharDispatch: (params) => dispatch(KYCAdharNumberAction.KYCAdhaarNumber(params)),
  clearKYCAdhaarDispatch: (params) => dispatch(KYCAdharNumberAction.clearKYCAdhaarNumber(params)),
  AadhaarFrontDispatch: (params) => dispatch(AadhaarFrontAction.AadhaarFrontValue(params)),
  AadhaarBackDispatch: (params) => dispatch(AadhaarBackAction.AadhaarBackValue(params)),
  ClearAadhaarBackDispatch: (params) => dispatch(AadhaarBackAction.clearAadhaarBackValue(params)),
  ClearAadhaarFrontDispatch: (params) => dispatch(AadhaarFrontAction.clearAadhaarFrontValue(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Address)