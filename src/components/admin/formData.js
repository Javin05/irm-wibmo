import _ from "lodash";

export const setAdminData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      Ssn: data.Ssn,
      firstName: data.firstName,
      lastName: data.lastName,
      dob: data.dob,
      address: data.address,
      city: data.city,
      state: data.state,
      zip: data.zip,
      homePhone: data.homePhone,
      cellPhone: data.cellPhone,
      employeName: data.employeName,
      employePhone: data.employePhone,
      notes: data.notes
    };
  }
}
