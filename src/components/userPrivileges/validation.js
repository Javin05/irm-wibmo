import { USER_MANAGEMENT_ERROR } from '../../utils/constants'

export const userRoleValidation = (values, setErrors) => {
  const errors = {}
  && values.userPrivileges[0] && values.userPrivileges[0].permissions
  && values.userPrivileges[0].permissions.length > 0
  if (!values.roleId) {
    errors.roleId = USER_MANAGEMENT_ERROR.USER_ROLE_REQUIRED
  }
  if (!values.usertypeId) {
    errors.usertypeId = USER_MANAGEMENT_ERROR.USER_TYPE_REQUIRED
  }
  setErrors(errors)
  return errors
}
