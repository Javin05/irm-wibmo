import _ from 'lodash'

export const setPrevilegeData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      roleId: data[0].roleId,
      usertypeId: data[0].userTypeId,
      tag:"IRM"
    }
  }
}
