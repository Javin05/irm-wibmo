import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  deletePrivilegesActions,
  getPrivilegesActions
} from '../../store/actions'
import { connect } from 'react-redux'
import { getUserPermissions } from '../../utils/helper'
import { Can } from '../../theme/layout/components/can'
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  DELETE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from '../../utils/constants'
import { warningAlert, confirmationAlert, confirmAlert } from '../../utils/alerts'
import color from "../../utils/colors";
import ReactPaginate from 'react-paginate'
import AddPrevileges from './add/AddPrevileges'

const UserPrivileges = (props) => {
  const {
    className,
    getPrivilegesDispatch,
    getPrivileges,
    loadingGP,
    count,
    messageDeletePrivi,
    statusDeletePrivi,
    deletePrivilegesDispatch,
    clearDeletePrivilegesDispatch
  } = props

  const url = useLocation().pathname
  const getUsersPermissions = getUserPermissions(url)
  const [editMode, setEditMode] = useState(false)
  const [limit, setLimit] = useState(25)
  const [show, setShow] = useState(false)
  const [updateId, setUpdateId] = useState({
    userTypeId: "",
    roleId: "",
  })
  const [activePageNumber, setActivePageNumber] = useState(1)

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1,
      tag:"IRM"
    }
    getPrivilegesDispatch(params)
  }, [])

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1,
      tag:"IRM"
    }
    getPrivilegesDispatch(params)
  }, [limit])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  const onDeleteItem = (item) => {
    const userTypeId = item && item.usertypeId &&
    item.usertypeId._id ? item.usertypeId._id : ''
    const roleId = item && item.roleId &&
    item.roleId._id ? item.roleId._id : ''
    const paramString = `${userTypeId}/${roleId}`
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_USER_PRIVILEGES,
      'warning',
      'Yes',
      'No',
      () => {
        deletePrivilegesDispatch(paramString)
      },
      () => { }
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      tag:"IRM"
    }
    setActivePageNumber(pageNumber)
    getPrivilegesDispatch(params)
  }

  const totalPages = count ? Math.ceil(parseInt(count) / limit) : 1

  const onConfirm = () => {
    const params = {
      limit: limit,
      page: activePageNumber,
      tag:"IRM"
    }
    getPrivilegesDispatch(params)
  }

  useEffect(() => {
    if (statusDeletePrivi === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageDeletePrivi,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() },
      )
      clearDeletePrivilegesDispatch()
    } else if (statusDeletePrivi === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageDeletePrivi,
        'error',
        'Close',
        'Ok',
        () => { onConfirm() },
        () => { }
      )
      clearDeletePrivilegesDispatch()
    }
  }, [statusDeletePrivi])

  return (
    <>
      <AddPrevileges
        setShow={setShow}
        show={show}
        updateId={updateId}
        editMode={editMode}
        setEditMode={setEditMode}
        setUpdateId={setUpdateId}
      />
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {getPrivileges && getPrivileges
                  ? (
                    <span className='text-muted fw-bold d-flex fs-5 mt-2 ms-4'>
                      Total: &nbsp;{' '}
                      <span className='text-gray-700 fw-bolder text-hover-primary fs-5'>
                        {getPrivileges && getPrivileges.length}
                      </span>
                    </span>
                  )
                  : null}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto mt-4'>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <div className='my-auto me-3'>
                  <button
                    type='button'
                    className='btn btn-sm btn-light-primary font-5vw pull-right'
                    data-toggle='modal'
                    data-target='#addModal'
                    onClick={() => { setShow(true) }}
                  >
                    {/* eslint-disable */}
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    {/* eslint-disable */}
                    Add
                  </button>
                </div>
              </Can>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={UPDATE_DELETE_PERMISSION}
                  >
                    <th>
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                  </Can>
                  <th>
                    <div className="d-flex">
                      <span>User Role</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>User Type</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loadingGP
                    ? (
                      getPrivileges &&
                        getPrivileges.length > 0
                        ? (
                          getPrivileges.map((item, i) => {
                            return (
                              <tr
                                key={i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <Can
                                  permissons={getUsersPermissions}
                                  componentPermissions={UPDATE_DELETE_PERMISSION}
                                >
                                  <td className='pb-0 pt-3 text-start'>
                                    <div className='my-auto d-flex'>
                                      <Can
                                        permissons={getUsersPermissions}
                                        componentPermissions={UPDATE_PERMISSION}
                                      >
                                        <button
                                          className='btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm'
                                          style={
                                            i % 2 === 0
                                              ? { backgroundColor: color.bgGray }
                                              : { backgroundColor: color.white }
                                          }
                                          onClick={() => {
                                            setShow(true)
                                            setUpdateId({
                                              userTypeId: { label: item.usertypeId && item.usertypeId.userType, value: item.usertypeId && item.usertypeId._id },
                                              roleId: { label: item.roleId && item.roleId.role, value: item.roleId && item.roleId._id }
                                            })
                                            setEditMode(true)
                                          }}
                                        >
                                          {/* eslint-disable */}
                                          <KTSVG
                                            path="/media/icons/duotune/art/art005.svg"
                                            className="svg-icon-3"
                                          />
                                          {/* eslint-enable */}
                                        </button>
                                      </Can>
                                      <Can
                                        permissons={getUsersPermissions}
                                        componentPermissions={DELETE_PERMISSION}
                                      >
                                        <button
                                          className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm ms-2'
                                          style={
                                            i % 2 === 0
                                              ? { backgroundColor: color.bgGray }
                                              : { backgroundColor: color.white }
                                          }
                                          onClick={() => {
                                            onDeleteItem(item)
                                          }}
                                        >
                                          {/* eslint-disable */}
                                          <KTSVG
                                            path="/media/icons/duotune/general/gen027.svg"
                                            className="svg-icon-3"
                                          />
                                          {/* eslint-enable */}
                                        </button>
                                      </Can>
                                    </div>
                                  </td>
                                </Can>
                                <td className='ellipsis'>
                                  {item && item.roleId && item.roleId.role ? item.roleId.role : '--'}
                                </td>
                                <td className='ellipsis'>
                                  {item && item.usertypeId && item.usertypeId.userType ? item.usertypeId.userType : '--'}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div className='spinner-border text-primary m-5' role='status' />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { getPrivilegesStore, deletePrivilegesStore } = state
  return {
    loadingGP:
      getPrivilegesStore && getPrivilegesStore.loadingGP ? getPrivilegesStore.loadingGP : false,
    getPrivileges:
      getPrivilegesStore && getPrivilegesStore.getPrivileges ? getPrivilegesStore.getPrivileges : [],
    count:
      getPrivilegesStore && getPrivilegesStore.count ? getPrivilegesStore.count : 0,
    statusDeletePrivi:
      deletePrivilegesStore && deletePrivilegesStore.statusDeletePrivi ? deletePrivilegesStore.statusDeletePrivi : '',
    messageDeletePrivi:
      deletePrivilegesStore && deletePrivilegesStore.messageDeletePrivi ? deletePrivilegesStore.messageDeletePrivi : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPrivilegesDispatch: (params) => dispatch(getPrivilegesActions.get(params)),
  deletePrivilegesDispatch: (params) => dispatch(deletePrivilegesActions.delete(params)),
  clearDeletePrivilegesDispatch: () => dispatch(deletePrivilegesActions.clear())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserPrivileges)
