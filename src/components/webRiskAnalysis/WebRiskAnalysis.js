import React, { useEffect, useState, useRef } from 'react'
import {
  WebAnalysisActions,
  getWebAnalysisActions,
  ManualWebAnalysisActions,
  DeleteWebAnalysisActions,
  EditWebAnalysisActions,
  updateWebAnalysisActions
} from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { STATUS_RESPONSE, SWEET_ALERT_MSG, DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT, STATUS_BADGE } from '../../utils/constants'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import './styles.css';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Modal from 'react-bootstrap/Modal'
import { userValidation, manualValidation } from './validation'
import { setWebAnalysisData } from './formData'
import clsx from 'clsx'
import SearchList from './searchList'
import ReactHTMLTableToExcel from "react-html-table-to-excel";

function WebRiskAnalysis(props) {
  const {
    className,
    loading,
    getWebAnalysisDispatch,
    BlockListType,
    WebAnalysisDispatch,
    getWebAnalysis,
    postCSVWebAnalysis,
    cleargetWebAnalysislistDispatch,
    postManualWebAnalysisDispatch,
    postManualWebAnalysis,
    clearManualWebAnalysisDispatch,
    DeleteWebAnalysisDispatch,
    deleteWebAnalysis,
    DeleteClearDispatch,
    EditWebAnalysisDispatch,
    EditWebAnalysis,
    UpdateWebAnalysisDispatch,
    EditclearWebAnalysis,
    UpdateClearDispatch,
    UpdateWebAnalysis
  } = props

  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [errors, setErrors] = useState({});
  const [error, setError] = useState({})
  const [fileName, setFileName] = useState()
  const [show, setShow] = useState(false)
  const [edit, setEdit] = useState(false)
  const [key, setKey] = useState('Manuval');
  const [editMode, setEditMode] = useState(false)
  const [currentId, setcurrentId] = useState()

  const [formData, setFormData] = useState({
    file: '',
    tag: ''
  })
  const [manualFormData, setManualFormData] = useState({
    website: '',
    tag: '',
  })

  const [sorting, setSorting] = useState({
    tag: false
  })

  useEffect(() => {
    getWebAnalysisDispatch()
  }, [])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getWebAnalysisDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getWebAnalysisDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getWebAnalysisDispatch(params)
    }
  }
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const hiddenFileInput = useRef(null);
  const handleChange = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setFormData((values) => ({
          ...values,
          file: files,
        }));
        setErrors((values) => ({ ...values, file: "" }));
        setFileName(files && files.name);
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  };

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  };

  const OnSubmit = () => {
    const errorMsg = userValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("file", formData.file)
      data.append("tag", formData.tag)
      WebAnalysisDispatch(data)
    }
  }

  const onConfirmUpdate = (currentId) => {
    UpdateWebAnalysisDispatch(currentId, manualFormData)
  }

  const handelSubmit = () => {
    const errorMsg = manualValidation(manualFormData, setError)
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        confirmationAlert(
          SWEET_ALERT_MSG.CONFIRMATION_TEXT,
          SWEET_ALERT_MSG.UPDATE_WEB,
          'warning',
          'Yes',
          'No',
          () => { onConfirmUpdate(currentId) },
          () => { }
        )
      } else {
        postManualWebAnalysisDispatch(manualFormData)
      }
    }
  }

  const clearPopup = () => {
    EditclearWebAnalysis()
    setEditMode(false)
    setShow(false)
    setFormData(values => ({
      ...values,
      file: '',
      tag: ''
    }))
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  const onConfirm = () => {
    setShow(false)
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    setFormData(values => ({
      ...values,
      file: '',
      tag: ''
    }))
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  const clear = () => {
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    setManualFormData({
      website: '',
      tag: ''
    })
    setFormData(values => ({
      ...values,
      file: '',
      tag: ''
    }))
  }

  useEffect(() => {
    if (postCSVWebAnalysis && postCSVWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        'success',
        'Back to Web RisK Analysis',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      getWebAnalysisDispatch()
      cleargetWebAnalysislistDispatch()
    } else if (postCSVWebAnalysis && postCSVWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      cleargetWebAnalysislistDispatch()
    }
  }, [postCSVWebAnalysis])

  useEffect(() => {
    if (postManualWebAnalysis && postManualWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        postManualWebAnalysis && postManualWebAnalysis.message,
        'success',
        'Back to Web RisK Analysis',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      getWebAnalysisDispatch()
      clearManualWebAnalysisDispatch()
    } else if (postManualWebAnalysis && postManualWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postManualWebAnalysis && postManualWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      clearManualWebAnalysisDispatch()
    }
  }, [postManualWebAnalysis])

  const onConfirmDelete = (id) => {
    DeleteWebAnalysisDispatch(id)
  }
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_WEB,
      'warning',
      'Yes',
      'No',
      () => { onConfirmDelete(id) },
      () => { }
    )
  }

  useEffect(() => {
    if (deleteWebAnalysis && deleteWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        deleteWebAnalysis && deleteWebAnalysis.message,
        'success'
      )
      getWebAnalysisDispatch()
      DeleteClearDispatch()
    } else if (deleteWebAnalysis && deleteWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        deleteWebAnalysis && deleteWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      DeleteClearDispatch()
    }
  }, [deleteWebAnalysis])

  useEffect(() => {
    if (EditWebAnalysis && EditWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const getByIdData = EditWebAnalysis && EditWebAnalysis.data
      const data = setWebAnalysisData(getByIdData)
      setManualFormData(data)
    }
  }, [EditWebAnalysis])

  useEffect(() => {
    if (UpdateWebAnalysis && UpdateWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        // UpdateWebAnalysis && UpdateWebAnalysis.message,
        'Updated Successfully',
        'success'
      )
      getWebAnalysisDispatch()
      EditclearWebAnalysis()
      UpdateClearDispatch()
      setShow(false)
    } else if (UpdateWebAnalysis && UpdateWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        UpdateWebAnalysis && UpdateWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      DeleteClearDispatch()
    }
  }, [UpdateWebAnalysis])

  const totalPages =
    BlockListType && BlockListType.count
      ? Math.ceil(parseInt(BlockListType && BlockListType.count) / limit)
      : 1

  return (
    <>
      <div className={`card p-7 ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex px-2'>
            <div className='d-flex justify-content-start col-md-6 col-lg-6 col-sm-6'>
            </div>
            <div className='d-flex justify-content-end my-auto col-md-6 col-sm-6 col-lg-6'>
              <div className='my-auto me-3'>
                <SearchList />
              </div>
              <div className='my-auto me-3'>
                <button
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
                  onClick={() => {
                    setShow(true)
                    setEditMode(false)
                  }}
                >
                  <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                  Add Web
                </button>
              </div>
            </div>
          </div>
          <div className='d-flex justify-content-end col-md-12 col-lg-12 col-sm-12 mt-4'>
            <div className="d-flex flex-stack">
              <div  className="me-3 xl-sheet"
              >
                <ReactHTMLTableToExcel
                  id="test-table-xls-button"
                  className="download-table-xls-button"
                  table="table-to-xls"
                  filename="tablexls"
                  sheet="tablexls"
                  buttonText="Export"
                />
                <i class="bi bi-filetype-xls text-success fs-1x"
                 />
              </div>
            </div>
          </div>

        

      <div class="table-responsive" style={{
        display: "none"
      }}>
 <table class="table" id="table-to-xls">
  <thead>
   <tr class="fw-bolder fs-6 text-gray-800">
    <th>Name</th>
    <th>Position</th>
    <th>Office</th>
    <th>Age</th>
    <th>Start date</th>
    <th>Salary</th>
   </tr>
  </thead>
  <tbody>
   <tr>
    <td>Tiger Nixon</td>
    <td>System Architect</td>
    <td>Edinburgh</td>
    <td>61</td>
    <td>2011/04/25</td>
    <td>$320,800</td>
   </tr>
   <tr>
    <td>Garrett Winters</td>
    <td>Accountant</td>
    <td>Tokyo</td>
    <td>63</td>
    <td>2011/07/25</td>
    <td>$170,750</td>
   </tr>
  </tbody>
 </table>
</div>



          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6"
            // id="table-to-xls"
            >
              <thead>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className="text-start">
                    <div className="d-flex">
                      <span>Web Risk No</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("webRiskNo")}
                        >
                          <i
                            className={`bi ${sorting.webRiskNo
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("website")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Tag</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                  !loading
                    ? (
                      getWebAnalysis &&
                        getWebAnalysis.data && getWebAnalysis.data.result && getWebAnalysis.data.result.length !== 0
                        ? (
                          getWebAnalysis.data.result && getWebAnalysis.data.result.map((item, id) => {
                            return (
                              <tr
                                key={id}
                                style={
                                  id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="pb-0 pt-5  text-start">
                                  {id + 1}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  <a
                                    href={item && item.website ? item.website : '--'}
                                  >
                                    {item && item.website ? item.website : "--"}
                                  </a>
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item && item.tag ? item.tag : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  <span className={`badge ${STATUS_BADGE[item.status]}`}>
                                    {item && item.status ? item.status : "--"}
                                  </span>
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  <button
                                    className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
                                    onClick={() => {
                                      setShow(true)
                                      setEditMode(true)
                                      EditWebAnalysisDispatch(item._id)
                                      setcurrentId(item._id)
                                    }}                                  >
                                    <KTSVG
                                      path='/media/icons/duotune/art/art005.svg'
                                      className='svg-icon-3'
                                      title="Edit BlockList"
                                    />
                                  </button>
                                  <button
                                    className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px me-4'
                                    onClick={() => onDeleteItem(item._id)}
                                    title="Delete BlockList"
                                  >
                                    <KTSVG
                                      path='/media/icons/duotune/general/gen027.svg'
                                      className='svg-icon-3'
                                    />
                                  </button>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"} Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab ftab"
          >
            <Tab eventKey="Manuval" title="Manual">
              <div className="card card-custom card-stretch gutter-b p-8">
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.tag && error.tag },
                        {
                          'is-valid': manualFormData.tag && !error.tag
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      onKeyPress={(e) => {
                        if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                          e.preventDefault()
                        }
                      }}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={manualFormData.tag || ''}
                    />
                    {error && error.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.tag}
                      </div>
                    )}
                  </div>
                </div>

                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Website :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Website'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.website && error.website },
                        {
                          'is-valid': manualFormData.website && !error.website
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='website'
                      autoComplete='off'
                      value={manualFormData.website || ''}
                    />
                    {error && error.website && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.website}
                      </div>
                    )}
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      className='btn btn-light-primary m-1 mt-8 font-5vw '
                      onClick={handelSubmit}>
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="bulkUpload" title="bulkUpload">
              <div className="card card-custom card-stretch gutter-b p-8">
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': formData.tag && errors.tag },
                        {
                          'is-valid': formData.tag && !errors.tag
                        }
                      )}
                      onChange={(e) => handleChange(e)}
                      onKeyPress={(e) => {
                        if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                          e.preventDefault()
                        }
                      }}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={formData.tag || ''}
                    />
                    {errors && errors.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.tag}
                      </div>
                    )}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Upload Document :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      type="file"
                      className="d-none"
                      name="file"
                      id="file"
                      multiple={true}
                      ref={hiddenFileInput}
                      onChange={handleFileChange}
                    />
                    <button
                      type="button"
                      style={{
                        width: "100%",
                      }}
                      className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                      onClick={handleClick}
                    >
                      <i class="bi bi-filetype-csv text-info" />
                      Upload Document
                    </button>
                    {errors && errors.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.file}
                      </div>
                    )}
                    {fileName && fileName}
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      className='btn btn-light-primary font-5vw  m-1 mt-8'
                      onClick={OnSubmit}>
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    rulesStore,
    BlockListlistStore,
    BlockListTypeStore,
    AddEmailToBlacklistKeysone,
    queueslistStore,
    BlockListUploadlistStore,
    BlockListEditlistStore,
    BlockListDeletelStore,
    BlockListUpdatelistStore,

    getWebAnalysisStore,
    WebAnalysisStore,
    ManualWebAnalysisStore,
    DeleteWebAnalysisStore,
    EditWebAnalysisStore,
    UpdateWebAnalysisStore
  } = state
  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : '',
    BlockListlists: BlockListlistStore && BlockListlistStore.BlockListlists ? BlockListlistStore.BlockListlists : {},
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType : {},
    BlockListUpdateSuccess: BlockListUpdatelistStore && BlockListUpdatelistStore.BlockListUpdatelists ? BlockListUpdatelistStore.BlockListUpdatelists : {},
    BlockEmailSuccess: AddEmailToBlacklistKeysone && AddEmailToBlacklistKeysone.emailSuccess ? AddEmailToBlacklistKeysone.emailSuccess : {},
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    BlocklistsUploads: BlockListUploadlistStore && BlockListUploadlistStore.BlocklistsUploads ? BlockListUploadlistStore.BlocklistsUploads : '',
    BlockListEditlists: BlockListEditlistStore && BlockListEditlistStore.BlockListEditlists ? BlockListEditlistStore.BlockListEditlists : '',
    DelteBlockList: BlockListDeletelStore && BlockListDeletelStore.DelteBlockList ? BlockListDeletelStore.DelteBlockList : '',

    loading: getWebAnalysisStore && getWebAnalysisStore.loading && getWebAnalysisStore.loading,
    getWebAnalysis: getWebAnalysisStore && getWebAnalysisStore.getWebAnalysis ? getWebAnalysisStore.getWebAnalysis : '',
    postCSVWebAnalysis: WebAnalysisStore && WebAnalysisStore.postCSVWebAnalysis ? WebAnalysisStore.postCSVWebAnalysis : '',
    postManualWebAnalysis: ManualWebAnalysisStore && ManualWebAnalysisStore.postManualWebAnalysis ? ManualWebAnalysisStore.postManualWebAnalysis : '',
    deleteWebAnalysis: DeleteWebAnalysisStore && DeleteWebAnalysisStore.deleteWebAnalysis ? DeleteWebAnalysisStore.deleteWebAnalysis : '',
    EditWebAnalysis: EditWebAnalysisStore && EditWebAnalysisStore.EditWebAnalysis ? EditWebAnalysisStore.EditWebAnalysis : '',
    UpdateWebAnalysis: UpdateWebAnalysisStore && UpdateWebAnalysisStore.UpdateWebAnalysis ? UpdateWebAnalysisStore.UpdateWebAnalysis : '',
  }
}

const mapDispatchToProps = dispatch => ({
  WebAnalysisDispatch: (data) => dispatch(WebAnalysisActions.getWebAnalysis(data)),
  getWebAnalysisDispatch: (data) => dispatch(getWebAnalysisActions.getgetWebAnalysislist(data)),
  cleargetWebAnalysislistDispatch: (data) => dispatch(getWebAnalysisActions.cleargetWebAnalysislist(data)),
  postManualWebAnalysisDispatch: (data) => dispatch(ManualWebAnalysisActions.postWebAnalysis(data)),
  clearManualWebAnalysisDispatch: (data) => dispatch(ManualWebAnalysisActions.clearManualWebAnalysis(data)),
  DeleteWebAnalysisDispatch: (data) => dispatch(DeleteWebAnalysisActions.deleteWebAnalysisDetails(data)),
  DeleteClearDispatch: (data) => dispatch(DeleteWebAnalysisActions.clearWebAnalysisDetails(data)),
  EditWebAnalysisDispatch: (id) => dispatch(EditWebAnalysisActions.EditWebAnalysisDetails(id)),
  EditclearWebAnalysis: () => dispatch(EditWebAnalysisActions.EditclearWebAnalysisDetails()),
  UpdateWebAnalysisDispatch: (data) => dispatch(updateWebAnalysisActions.updateWebAnalysisDetails(data)),
  UpdateClearDispatch: (data) => dispatch(updateWebAnalysisActions.updateclearWebAnalysisDetails(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WebRiskAnalysis);