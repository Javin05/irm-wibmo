import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { getWebAnalysisActions } from '../../store/actions'
import './styles.css';
import Modal from 'react-bootstrap/Modal'
import { searchValidation } from './validation'
import clsx from 'clsx'

function SearchList(props) {
  const {
    getRiskMgmtColumns,
    getWebAnalysisDispatch
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [key, setKey] = useState('Manuval');

  const [manualFormData, setManualFormData] = useState({
    website: '',
    tag: ''
  })

  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const handleSearch = () => {
    const errorMsg = searchValidation(manualFormData, setError)
    if (_.isEmpty(errorMsg)) {
      const params = {
        website: manualFormData.website,
        tag: manualFormData.tag
      }
      getWebAnalysisDispatch(params)
      setShow(false)
    }
  }

  const clearPopup = () => {
    setShow(false)
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Tag :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Tag'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.tag && error.tag },
                    {
                      'is-valid': manualFormData.tag && !error.tag
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  onKeyPress={(e) => {
                    if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                      e.preventDefault()
                    }
                  }}
                  type='text'
                  name='tag'
                  autoComplete='off'
                  value={manualFormData.tag || ''}
                />
                {error && error.tag && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.tag}
                  </div>
                )}
              </div>
            </div>

            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Website :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Website'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.website && error.website },
                    {
                      'is-valid': manualFormData.website && !error.website
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type='text'
                  name='website'
                  autoComplete='off'
                  value={manualFormData.website || ''}
                />
                {error && error.website && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.website}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  className='btn btn-light-primary m-1 mt-8 font-5vw '
                  onClick={handleSearch}>
                  Search
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}


const mapStateToProps = state => ({
  getRiskManagementlist: state && state.riskManagementlistStore && state.riskManagementlistStore.getRiskManagementlist,
  loading: state && state.riskManagementlistStore && state.riskManagementlistStore.loading,
})

const mapDispatchToProps = dispatch => ({
  getWebAnalysisDispatch: (params) => dispatch(getWebAnalysisActions.getgetWebAnalysislist(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)