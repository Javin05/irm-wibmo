import _ from "lodash";

export const setWebAnalysisData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      website: data.website,
      tag: data.tag,
    }
  }
}