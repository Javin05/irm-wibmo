/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * components (e.g: `src/app/modules/Auth/pages/AuthPage`, `src/app/BasePage`).
 */
import React from 'react'
import { Switch, Route, useLocation } from 'react-router-dom'
import { MasterLayout } from '../theme/layout/MasterLayout'
import { PrivateRoutes } from './PrivateRoutes'
import { PublicRoutes } from './PublicRoutes'
import { MasterInit } from '../theme/layout/MasterInit'
import { UnderConstruction, ErrorsPage } from '../containers/error'
import { PageTitle } from '../theme/layout/core'
import { SESSION } from '../utils/constants'
import { getLocalStorage } from '../utils/helper'
import FraudAnalyzer from '../containers/merchantLogin/index'
import MerchantDetails from '../containers/auth/merchantDetails'
import AddQueue from '../containers/auth/addQueues'
import AddTransactions from '../components/transaction/AddTransaction'
import AddAML from '../components/amlQueue/AddAML'
import Logout from '../components/auth/Logout'
import SessionTimeout from '../components/SessionTimeout/SessionTimeout'
import SDKCBForm from '../components/wrmManagement/SDK/sdkForm'
import SdkManagement from '../components/wrmManagement/SDK/SdkRiskManangement'
// import SdkDashboard from '../components/wrmManagement/SDK/sdkDashboard'

const Routes = () => {
  const isAuthorized = getLocalStorage(SESSION.TOKEN)
  const pathName = useLocation().pathname
  const search = useLocation().search
  const query = new URLSearchParams(search)
  const paramField = query.get('bank_id')
  const fields = pathName && pathName.split("/")
  const sdkId = fields && fields[3]
  const urlEndPoints = [
    '/session-expired',
    '/session-timeout',
    '/merchant-login',
    '/add-transaction',
    '/add-aml',
    '/wrm-riskmmanagement-sdk',
    `/sdk-summary/update/${sdkId}`
  ]
  return (
    <>
      <Switch>
        {isAuthorized
          ? (
            <>
              {
                urlEndPoints.includes(pathName)
                  ? (
                    <>
                      <Route path='/merchant-login' component={MerchantDetails} />
                      <Route path='/add-transaction' component={AddTransactions} />
                      <Route path='/add-aml' component={AddAML} />
                      <Route path='/session-timeout' component={SessionTimeout} />
                      <Route path='/session-expired' component={Logout} />
                      <Route path='/wrm-riskmmanagement-sdk' component={SdkManagement} />
                      {/* <Route path='/sdk-summary/update/:id' component={SdkDashboard} /> */}
                    </>
                    )
                  : (
                    <MasterLayout>
                       <Route path='/upcoming'>
                        <PageTitle breadcrumbs={[]}>Under Construction</PageTitle>
                        <UnderConstruction />
                      </Route>
                      <PrivateRoutes />
                    </MasterLayout>
                    )
              }
            </>
            )
          : (
            <>
              <PublicRoutes />
            </>
            )}
      </Switch>

      <MasterInit />
    </>
  )
}

export { Routes }
