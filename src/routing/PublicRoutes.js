import React, { useEffect } from 'react'
import { Route, Switch, useLocation } from 'react-router-dom'
import { AuthPage } from '../containers/auth'
import { LoginPage } from '../containers/auth/login'
import { MerchantDetails } from '../containers/auth/merchantDetails'
import SDKCBForm from '../components/wrmManagement/SDK/sdkForm'
import SdkManagement from '../components/wrmManagement/SDK/SdkRiskManangement'
// import SdkDashboard from '../components/wrmManagement/SDK/sdkDashboard'
import KYCminiAdd from '../components/KYC/addUAKyc/mini-KYC/Add'

export function PublicRoutes () {
  const pathName = useLocation().pathname
  useEffect(() => {
    localStorage.removeItem('isAuthorized')
  }, [])
  const fields = pathName && pathName.split("/")
  const sdkId = fields && fields[3]
  console.log('pathName', pathName)

  return (
    <Switch>
      {
        pathName === '/forgotPassword' || 
        pathName === '/registration' ||
        pathName === '/wrm-riskmmanagement-sdk' ||
        pathName === '/add-kyc' || 
        pathName === `/sdk-summary/update/${sdkId}`
         ? (
          <>
          <Route path='/forgotPassword' component={AuthPage} />
          <Route path='/registration' component={AuthPage} />
          <Route path='/wrm-riskmmanagement-sdk' component={SdkManagement} />
          {/* <Route path='/sdk-summary/update/:id' component={SdkDashboard} /> */}
          </>
        ):(
          <Route path='/' component={LoginPage} />
        )
      }
    </Switch>
  )
}
