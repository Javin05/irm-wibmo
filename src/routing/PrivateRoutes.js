import React, { Suspense, lazy } from "react"
import { Route, Switch, Redirect, useLocation } from "react-router-dom"
import { FallbackView } from "../theme/partials"
import routeConfig from "./routeConfig"
import { getUserMatrixSlugs, getLocalStorage } from "../utils/helper"
import { SET_STORAGE } from "../utils/constants"
import _ from "lodash"
import OnBoardingSummary from "../components/OnBoardingSummary/OnBoardingSummary"


export function PrivateRoutes() {
  const Merchant = lazy(() => import("../containers/merchant/index"))
  const DashboardPage = lazy(() => import("../containers/dashboard"))
  const RiskManagement = lazy(() =>
    import("../containers/riskManagement/index")
  )
  const MerchantDemo = lazy(() =>
    import("../containers/merchant/merchantDemo")
  )
  const RiskSummaryPage = lazy(() => import("../containers/riskSummary"))
  const StaticSummaryPage = lazy(() => import("../containers/staticSummary"))
  const Home = lazy(() => import("../components/home/Home"))
  const Reports = lazy(() => import("../containers/reports/index"))
  const WebriskSummary = lazy(() => import('../containers/webriskSummary/index'))
  const APIUsageSummary = lazy(() => import('../containers/apiUsageSummary/index'))
  const APIUsageBillingSummary =lazy(()=>import("../containers/apiUsageBillingSummary/index")) 
  const WRMSlaMonitorSummary = lazy(() => import('../containers/wrmSlaMonitorySummary/index'))
  const WRMSimplBillingSummary = lazy(() => import('../containers/wrmSimplBillingSummary/index'))
  const BillingSummary = lazy(() => import('../containers/billingsummary/index'))
  const ManageQues = lazy(() => import("../containers/manageQueues/index"))
  const AddQueues = lazy(() => import("../containers/queues/add"))
  const Rules = lazy(() => import("../containers/rules/index"))
  const RulesForm = lazy(() => import("../containers/rules/index"))
  const UpdateRulesForm = lazy(() => import("../containers/rules"))
  const UpdateQueues = lazy(() => import("../containers/queues/edit"))
  const Admin = lazy(() => import("../containers/admin/index"))
  const OGMAnalysisSummary = lazy(()=>import("../containers/OGMAnalysisDashboard/index"))
  const OGMAuditSummary = lazy(()=>import("../containers/OGMAuditDashboard/index"))
  const addAdmin = lazy(() => import("../components/admin/AdminForm"))
  const editAdmin = lazy(() => import("../containers/admin/edit"))
  const Transaction = lazy(() => import("../containers/transaction/index"))
  const WrmRiskOperationManagementList = lazy(() => import("../containers/wrmOperationManagmement/index"))
  const TransactionSearch = lazy(() =>
    import("../containers/transaction/index")
  )
  const TransactionDashboard = lazy(() =>
    import("../containers/transactionDashboard/index")
  )
  const BlackList = lazy(() => import("../containers/blackList"))
  const MainDashboard = lazy(() => import("../containers/main-dashboard"))
  const AMLQueue = lazy(() => import("../containers/amlQueue/index"))
  const AMLQueueSearch = lazy(() => import("../containers/amlQueue/index"))
  const QueueReport = lazy(() => import("../containers/queueReports/index"))
  const ClientMerchant = lazy(() =>
    import("../containers/clientMerchant/index")
  )
  const Components = lazy(() => import("../containers/components"))
  const UsersContainer = lazy(() => import("../containers/UsersContainer"))
  const UsersType = lazy(() => import("../containers/UserType"))
  const UsersPrivileges = lazy(() => import("../containers/userPrivileges"))
  const DemoDashboard = lazy(() => import("../containers/demo-dashboard"))
  const Whitelist = lazy(() => import("../containers/white-list"))
  const Watchlist = lazy(() => import("../containers/watch-list"))
  const KYCPage = lazy(() => import("../containers/KYC/index"))
  const FraudPatterns = lazy(() => import("../containers/fraud-patterns"))
  const WebRiskAnalysis = lazy(() => import("../containers/webRiskAnalysis"))
  const UsersManagement = lazy(() => import("../containers/UserManagement"))
  const SiteConfig = lazy(() => import("../containers/SiteConfig/index"))
  const AssignClient = lazy(() => import("../containers/UserManagement/index"))
  const TagSummary = lazy(() => import("../containers/tagSummary/index"))
  const UserProfileKYC = lazy(() => import("../containers/UserProfileKYC/index"))
  const WrmRiskManagementList = lazy(() => import("../containers/WrmRiskManagement/index"))
  const MonitorList = lazy(() => import("../containers/monitor/index"))
  const TransactionLaundring = lazy(() => import("../containers/tansactionLaundring/index"))
  const UaeKyc = lazy(() => import("../containers/uaeKyc/index"))
  const WebReportSetting = lazy(() => import("../containers/webReportSetting/index"))
  const PlayStoreReportSetting = lazy(() => import("../containers/playStoreReportSetting/index"))
  const TemplateContainer = lazy(() => import('../containers/Settings/EmailTemplate/index'))
  const OGMSummary = lazy(() => import('../containers/OGMsummary/index'))
  const AccountRiskSummary = lazy(() => import('../containers/riskManagement/index'))
  const OnBoarding = lazy(() => import('../containers/onBoarding/index'))
  const onBoardingUpdate = lazy(() => import('../containers/onBoarding/index'))
  const WrmOperationManagementList = lazy(() => import("../containers/wrmOperationManagmement/index"))
  const WRMOperationSummary = lazy(() => import('../containers/wrmOperationSummary/index'))
  const OgmOperationManagementList = lazy(() => import('../containers/ogmOperationManagmement/index'))


  const pathName = useLocation().pathname
  const getSlugs = getUserMatrixSlugs()
  const slugToRedirect =
    getLocalStorage(SET_STORAGE.IS_FIRST_LOGIN) === "true"
      ? "/session-expired"
      : "/auth-failed"

  const checkSlugsMatched = () => {
    if (pathName.includes("update") || pathName.includes("assign")) {
      let check = false
      _.forEach(getSlugs && getSlugs, (ele) => {
        if (ele.includes(":id")) {
          const currentUrl = ele.split("/:id")
          if (ele.includes(currentUrl && currentUrl[0])) {
            check = true
          }
        }
      })
      return check
    } else if (getLocalStorage(SET_STORAGE.IS_FIRST_LOGIN) === "true") {
      return false
    } else if (getSlugs && getSlugs.includes(pathName)) {
      return true
    }
    return false
  }

  return (
    <Suspense fallback={<FallbackView />}>
      <Switch>
      <Route
          path={routeConfig.onBoardingUpdate}
          render={(props) => checkSlugsMatched()
            ? <OnBoardingSummary {...props} />
            : <Redirect to={slugToRedirect} />}
        />
      <Route
          path={routeConfig.onBoarding}
          render={(props) => checkSlugsMatched()
            ? <OnBoarding {...props} />
            : <Redirect to={slugToRedirect} />}
        />
      <Route
          path={routeConfig.accountRisk}
          render={(props) => checkSlugsMatched()
            ? <AccountRiskSummary {...props} />
            : <Redirect to={slugToRedirect} />}
        />
      <Route
          path={routeConfig.ogmSummary}
          render={(props) => checkSlugsMatched()
            ? <OGMSummary {...props} />
            : <Redirect to={slugToRedirect} />}
        />
      <Route
          path={routeConfig.emailTemplates}
          render={(props) => checkSlugsMatched()
            ? <TemplateContainer {...props} />
            : <Redirect to={slugToRedirect} />}
        />
        <Route
          path={routeConfig.PlayStoreReportSetting}
          render={(props) =>
            checkSlugsMatched() ? (
              <PlayStoreReportSetting {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.webReportSetting}
          render={(props) =>
            checkSlugsMatched() ? (
              <WebReportSetting {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.uaeKyc}
          render={(props) =>
            checkSlugsMatched() ? (
              <UaeKyc {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.transactionLundering}
          render={(props) =>
            checkSlugsMatched() ? (
              <TransactionLaundring {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.monitarDashboard}
          render={(props) =>
            checkSlugsMatched() ? (
              <MonitorList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.monitarSearch}
          render={(props) =>
            checkSlugsMatched() ? (
              <MonitorList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.monitar}
          render={(props) =>
            checkSlugsMatched() ? (
              <MonitorList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.wrmSDK}
          render={(props) =>
            checkSlugsMatched() ? (
              <WrmRiskManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.wrmRiskmanagement}
          render={(props) =>
            checkSlugsMatched() ? (
              <WrmRiskManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.dashboard}
          render={(props) =>
            checkSlugsMatched() ? (
              <DashboardPage {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.wrmOperationManagementList}
          render={(props) =>
            checkSlugsMatched() ? (
              <WrmRiskOperationManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.wrmOperationManagementUpdate}
          render={(props) =>
            checkSlugsMatched() ? (
              <WrmRiskOperationManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.accountsRiskmanagement}
          render={(props) =>
            checkSlugsMatched() ? (
              <RiskManagement {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.riskManagementSearch}
          render={(props) =>
            checkSlugsMatched() ? (
              <RiskManagement {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.merchant}
          render={(props) =>
            checkSlugsMatched() ? (
              <Merchant {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.merchantIdDemo}
          render={(props) =>
            checkSlugsMatched() ? (
              <MerchantDemo {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.wrmoperation}
          component={WRMOperationSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <WRMOperationSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.wrmOperationManagementList}
          render={(props) =>
            checkSlugsMatched() ? (
              <WrmOperationManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.wrmOperationManagementUpdate}
          render={(props) =>
            checkSlugsMatched() ? (
              <WrmOperationManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.ogmOperationManagementList}
          render={(props) =>
            checkSlugsMatched() ? (
              <OgmOperationManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.ogmOperationManagementUpdate}
          render={(props) =>
            checkSlugsMatched() ? (
              <OgmOperationManagementList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.riskSummary}
          component={RiskSummaryPage}
          render={(props) =>
            checkSlugsMatched() ? (
              <RiskSummaryPage {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.riskSummaryNew}
          component={RiskSummaryPage}
          render={(props) =>
            checkSlugsMatched() ? (
              <RiskSummaryPage {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.staticSummary}
          component={StaticSummaryPage}
          render={(props) =>
            checkSlugsMatched() ? (
              <StaticSummaryPage {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.home}
          component={Home}
          render={(props) =>
            checkSlugsMatched() ? (
              <Home {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.reports}
          component={Reports}
          render={(props) =>
            checkSlugsMatched() ? (
              <Reports {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.webrisksummary}
          component={WebriskSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <WebriskSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.billingsummary}
          component={BillingSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <BillingSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
         <Route
          path={routeConfig.apiusagesummary}
          component={APIUsageSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <APIUsageSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
          <Route
          path={routeConfig.apiusageBillingsummary}
          component={APIUsageBillingSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <APIUsageBillingSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.ogmanalysisdashboard}
          component={OGMAnalysisSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <OGMAnalysisSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
         <Route
          path={routeConfig.ogmauditdashboard}
          component={OGMAuditSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <OGMAuditSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
            <Route
          path={routeConfig.wrmslamonitorysummary}
          component={WRMSlaMonitorSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <WRMSlaMonitorSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.wrmsimplbiling}
          component={WRMSimplBillingSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <WRMSimplBillingSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.manageQueues}
          render={(props) =>
            checkSlugsMatched() ? (
              <ManageQues {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.addQueues}
          render={(props) =>
            checkSlugsMatched() ? (
              <AddQueues {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.rules}
          render={(props) =>
            checkSlugsMatched() ? (
              <Rules {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.rulesForm}
          render={(props) =>
            checkSlugsMatched() ? (
              <RulesForm {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.updateRule}
          render={(props) =>
            checkSlugsMatched() ? (
              <UpdateRulesForm {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.upadteQueues}
          render={(props) =>
            checkSlugsMatched() ? (
              <UpdateQueues {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.admin}
          render={(props) =>
            checkSlugsMatched() ? (
              <Admin {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.addAdmin}
          render={(props) =>
            checkSlugsMatched() ? (
              <addAdmin {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.editAdmin}
          render={(props) =>
            checkSlugsMatched() ? (
              <editAdmin {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.transaction}
          render={(props) =>
            checkSlugsMatched() ? (
              <Transaction {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.transactionSearch}
          render={(props) =>
            checkSlugsMatched() ? (
              <TransactionSearch {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.transactionDashboard}
          render={(props) =>
            checkSlugsMatched() ? (
              <TransactionDashboard {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.blacklist}
          render={(props) =>
            checkSlugsMatched() ? (
              <BlackList {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.mainDashboard}
          render={(props) =>
            checkSlugsMatched() ? (
              <MainDashboard {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.amlQueue}
          render={(props) =>
            checkSlugsMatched() ? (
              <AMLQueue {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.addAmlQueue}
          render={(props) =>
            checkSlugsMatched() ? (
              <AMLQueue {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.addAml}
          render={(props) =>
            checkSlugsMatched() ? (
              <AMLQueue {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.amlQueueSearch}
          render={(props) =>
            checkSlugsMatched() ? (
              <AMLQueueSearch {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.queueReports}
          render={(props) =>
            checkSlugsMatched() ? (
              <QueueReport {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.demoDashboard}
          render={(props) =>
            checkSlugsMatched() ? (
              <DemoDashboard {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.clientManagement}
          render={(props) =>
            checkSlugsMatched() ? (
              <ClientMerchant {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.clientOnboarding}
          render={(props) =>
            checkSlugsMatched() ? (
              <ClientMerchant {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.editClientOnboarding}
          render={(props) =>
            checkSlugsMatched() ? (
              <ClientMerchant {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.userComponents}
          render={(props) =>
            checkSlugsMatched() ? (
              <Components {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.addUserComponents}
          render={(props) =>
            checkSlugsMatched() ? (
              <Components {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.updateUserComponents}
          render={(props) =>
            checkSlugsMatched() ? (
              <Components {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.userRole}
          render={(props) =>
            checkSlugsMatched() ? (
              <UsersContainer {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.addUserRole}
          render={(props) =>
            checkSlugsMatched() ? (
              <UsersContainer {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.updateUserRole}
          render={(props) =>
            checkSlugsMatched() ? (
              <UsersContainer {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.assignClient}
          render={(props) =>
            checkSlugsMatched() ? (
              <AssignClient {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.userType}
          render={(props) =>
            checkSlugsMatched() ? (
              <UsersType {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.userPrivileges}
          render={(props) =>
            checkSlugsMatched() ? (
              <UsersPrivileges {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.demoDashboard}
          render={(props) =>
            checkSlugsMatched() ? (
              <DemoDashboard {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.whitelist}
          render={(props) =>
            checkSlugsMatched() ? (
              <Whitelist {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.watchlist}
          render={(props) =>
            checkSlugsMatched() ? (
              <Watchlist {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.KYC}
          render={(props) =>
            checkSlugsMatched() ? (
              <KYCPage {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        {/* newchange */}
        <Route
          path={routeConfig.UserProfileKYC}
          render={(props) =>
            checkSlugsMatched() ? (
              <UserProfileKYC {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.FraudPatterns}
          render={(props) =>
            checkSlugsMatched() ? (
              <FraudPatterns {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.WebRisk}
          render={(props) =>
            checkSlugsMatched() ? (
              <WebRiskAnalysis {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.manageUsers}
          render={(props) =>
            checkSlugsMatched() ? (
              <UsersManagement {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.siteconfiguration}
          render={(props) =>
            checkSlugsMatched() ? (
              <SiteConfig {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
        <Route
          path={routeConfig.tagSummary}
          render={(props) =>
            checkSlugsMatched() ? (
              <TagSummary {...props} />
            ) : (
              <Redirect to={slugToRedirect} />
            )
          }
        />
      </Switch>
    </Suspense>
  )
}
